package net.vicemice.game.bedwars.shop;

import net.vicemice.game.bedwars.shop.specialitems.Warper;
import lombok.Getter;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class ItemTimeouts {
    @Getter
    private static HashMap<Player, Long> rettungsPlatFormTimeout = new HashMap<>();
    @Getter
    private static HashMap<Player, Warper> warpers = new HashMap<>();
}
