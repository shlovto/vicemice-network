package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.config.map.vote.VoteState;
import net.vicemice.game.bedwars.game.VotingManager;
import net.vicemice.game.bedwars.player.board.lobby.LobbyScoreboardManager;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.party.PartyManager;
import net.vicemice.game.bedwars.player.spectator.SpectatorManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.gameapi.util.EloRank;
import net.vicemice.hector.gameapi.util.Locations;
import net.vicemice.hector.packets.types.player.party.PartyMemberPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.utils.ItemCreator;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PlayerJoinListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        IUser user = UserManager.getUser(event.getPlayer());
        user.getBukkitPlayer().setGameMode(GameMode.ADVENTURE);
        event.setJoinMessage(null);
        PlayerManager.resetPlayer(user);
        if (GameManager.getGameState() == GameState.LOBBY) {
            if (BedWars.getGameConfig().isRANKED()) {
                long elo = (user.getStatistic( "bw_elo", StatisticPeriod.MONTH) >= 1 ? user.getStatistic("bw_elo", StatisticPeriod.MONTH) : 0);
                EloRank rank = EloRank.rankByElo(elo);
                if (rank == null) rank = EloRank.UNRANKED;
                user.sendMessage("ranked-server", "BedWars", ((rank.getMax()+1)-elo));
            } else {
                user.sendMessage("unranked-server", "BedWars");
            }

            user.teleport(Locations.getLobbyLocation());
            String playerName = (user.isNicked() ? "§9"+user.getNickName() : user.getRank().getRankColor() + user.getName());
            UserManager.getConnectedUsers().values().forEach(p -> p.sendMessage("game-joined", playerName));
            GoServer.getLocaleManager().getLocales().forEach((k, v) -> GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "game-joined", playerName), k));

            user.getBukkitPlayer().getInventory().setItem(0, new ItemCreator().material(Material.BED).displayName(user.translate("item-team-selector")).build());

            if (user.getRank().isHigherEqualsLevel(Rank.VIP)) {
                if (VotingManager.getVoteState() == VoteState.ON) {
                    user.getBukkitPlayer().getInventory().setItem(4, new ItemCreator().material(Material.CHEST).displayName(user.translate("item-force-map")).build());
                }
            }

            if (VotingManager.getVoteState() == VoteState.ON) {
                if (BedWars.getGameConfig().isVOTING()) {
                    user.getBukkitPlayer().getInventory().setItem(1, new ItemCreator().material(Material.PAPER).displayName(user.translate("item-voting")).build());
                }
            }

            if (PartyManager.getPartyMembers().containsKey(user.getUniqueId())) {
                PartyMemberPacket partyMemberPacket = PartyManager.getPartyMembers().get(user.getUniqueId());
                List<IUser> onlinePartyPlayers = new ArrayList<>();
                for (UUID member : partyMemberPacket.getMembers()) {
                    if (UserManager.isConnected(member)) {
                        IUser getPlayer = UserManager.getUser(member);
                        onlinePartyPlayers.add(getPlayer);
                    }
                }
                if (onlinePartyPlayers.size() == partyMemberPacket.getMembers().size()) {
                    PartyManager.allPlayersOnline(onlinePartyPlayers);
                }
            }

            user.sendMessage("game-teaming-with-other-teams-forbidden", user.translate("bw-prefix"));

            LobbyScoreboardManager.createBoard(user);
        } else if (GameManager.getGameState() == GameState.RESTART) {
            user.teleport(Locations.getLobbyLocation());
        } else {
            SpectatorManager.spectatorPlayer(user);
        }
    }
}
