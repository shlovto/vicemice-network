package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.game.bedwars.player.spectator.SpectatorManager;
import java.util.Arrays;
import java.util.List;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.gameapi.util.EloRank;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.player.permissions.listener.CloudChatEvent;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class CloudChatListener implements Listener {
    private static final List<String> allPrefix = Arrays.asList("@all ", "@all", "@a ", "@a");

    @EventHandler
    public void onCloudChat(CloudChatEvent event) {
        IUser user = event.getUser();

        if (GameManager.getGameState() == GameState.LOBBY && BedWars.getGameConfig().isRANKED()) {
            long elo = (user.getStatistic("bw_elo", StatisticPeriod.MONTH) >= 1 ? user.getStatistic("bw_elo", StatisticPeriod.MONTH) : 0);
            EloRank rank = EloRank.rankByElo(elo);
            if (rank == null) rank = EloRank.UNRANKED;
            event.setMessage("§7(§e§l"+rank.getColor()+rank.getName()+"§7) "+event.getMessage());
        }

        if (GameManager.getGameState() == GameState.INGAME) {
            event.setCancelled(true);

            if (SpectatorManager.getSpectators().contains(UserManager.getUser(user.getUniqueId()))) {
                spectatorChat(UserManager.getUser(user.getUniqueId()), event.getRawMessage());
                return;
            }

            boolean global = false;
            for (String prefix : allPrefix) {
                if (event.getRawMessage().toLowerCase().startsWith(prefix)) {
                    sendGlobalMessage(UserManager.getUser(user.getUniqueId()), event.getRawMessage().substring(prefix.length()));
                    global = true;
                    break;
                }
            }
            if (!global) {
                sendTeamMessage(UserManager.getUser(user.getUniqueId()), event.getRawMessage());
            }
        }
    }

    public void spectatorChat(IUser user, String message) {
        for (IUser spectator : SpectatorManager.getSpectators()) {
            spectator.sendRawMessage("§7[§4X§7] " + user.getNickName() + "§8: §f" + message);
        }
    }

    public void sendGlobalMessage(IUser user, String message) {
        GameTeam gameTeam = PlayerManager.getPlayerDatas().get(user).getGameTeam();
        for (IUser onlinePlayer : UserManager.getConnectedUsers().values()) {
            onlinePlayer.sendRawMessage("§8[§7@all§8] "+gameTeam.getColor()+user.getNickName()+"§8: §f"+message);
        }

    }

    public void sendTeamMessage(IUser user, String message) {
        GameTeam gameTeam = PlayerManager.getPlayerDatas().get(user).getGameTeam();
        for (IUser teamPlayer : gameTeam.getPlayers()) {
            teamPlayer.sendRawMessage(gameTeam.getColor() + user.getNickName() + "§8: §f" + message);
        }
    }
}
