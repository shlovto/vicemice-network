package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.player.PlayerData;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.game.bedwars.util.ItemStackHelper;
import net.vicemice.game.bedwars.player.spectator.SpectatorManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreakListener implements Listener {

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        IUser user = UserManager.getUser(event.getPlayer());
        if (BedWars.getBuildPlayers().contains(user.getBukkitPlayer())) {
            return;
        }
        if (GameManager.getGameState() != GameState.INGAME) {
            event.setCancelled(true);
            return;
        }
        if (SpectatorManager.getSpectators().contains(user)) {
            event.setCancelled(true);
            return;
        }
        PlayerData playerData = PlayerManager.getPlayerDatas().get(user);
        if (playerData == null) {
            event.setCancelled(true);
            return;
        }
        if (event.getBlock().getType() == Material.BED_BLOCK) {
            if (BedWars.getGame().getBedWarsMap().getTeamByBed(event.getBlock().getLocation()) != null) {
                GameTeam bedTeam = BedWars.getGame().getBedWarsMap().getTeamByBed(event.getBlock().getLocation());
                if (bedTeam != playerData.getGameTeam()) {
                    if (!bedTeam.isPlayed()) {
                        user.sendMessage("team-not-played");
                        event.setCancelled(true);
                        return;
                    }
                    bedTeam.destroyBed(user);
                } else {
                    event.setCancelled(true);
                    user.sendMessage("own-bed");
                }
            }
            return;
        }
        if (!BedWars.getGame().getPlacedBlocks().contains(event.getBlock().getLocation()) && !BedWars.getWhiteListMaterials().contains(event.getBlock().getType())) {
            event.setCancelled(true);
            user.sendMessage("not-breakable");
            return;
        }
        if (event.getBlock().getType() == Material.ENDER_CHEST && BedWars.getGame().getTeamChests().containsKey(event.getBlock().getLocation())) {
            event.setCancelled(true);
            event.getBlock().setType(Material.AIR);
            event.getBlock().getLocation().getWorld().dropItemNaturally(event.getBlock().getLocation(), ItemStackHelper.createItemStack(Material.ENDER_CHEST, 0, "§6Team Chest"));
            GameTeam gameTeam = BedWars.getGame().getTeamChests().get(event.getBlock().getLocation());
            BedWars.getGame().getTeamChests().remove(event.getBlock().getLocation());
        }
    }
}
