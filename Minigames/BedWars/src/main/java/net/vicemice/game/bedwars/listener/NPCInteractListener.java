package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.player.PlayerData;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.shop.types.BlockShopInventory;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.api.internal.fakemobs.event.PlayerInteractFakeMobEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/*
 * Class created at 22:56 - 13.04.2020
 * Copyright (C) elrobtossohn
 */
public class NPCInteractListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractFakeMobEvent event) {
        PlayerData playerData = PlayerManager.getPlayerDatas().get(UserManager.getUser(event.getPlayer()));
        if (playerData == null) return;
        new BlockShopInventory().create(UserManager.getUser(event.getPlayer())).open();
    }
}
