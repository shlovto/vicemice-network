package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.game.scoreboard.ScoreboardManager;
import net.vicemice.game.bedwars.player.teams.TeamManager;
import net.vicemice.game.bedwars.player.teams.lobby.LobbyTeamManager;
import net.vicemice.game.bedwars.player.spectator.SpectatorManager;
import net.vicemice.hector.events.*;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class NickListener implements Listener {

	@EventHandler
	public void onPlayerNick(PlayerNickEvent event) {
		IUser user = event.getUser();
		if (GameManager.getGameState() == GameState.INGAME) {
			if (SpectatorManager.getSpectators().contains(user)) {
				SpectatorManager.getSpectatorTeam().addEntry(event.getNickName());
			} else {
				ScoreboardManager.getScoreboardTeams().get(TeamManager.getPlayersTeam(user)).addEntry(event.getNickName());
			}
		} else if (GameManager.getGameState() == GameState.LOBBY) {
			LobbyTeamManager.updateSelectInventory();
		} else if (SpectatorManager.getSpectators().contains(user)) {
			Bukkit.getOnlinePlayers().forEach(all -> all.hidePlayer(user.getBukkitPlayer()));
			user.getBukkitPlayer().setAllowFlight(true);
			user.getBukkitPlayer().setFlying(true);
			user.getBukkitPlayer().spigot().setCollidesWithEntities(false);
			user.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));
		}
	}

	@EventHandler
	public void onPlayerUnnick(PlayerUnnickEvent event) {
		IUser user = event.getUser();
		if (GameManager.getGameState() == GameState.INGAME) {
			if (SpectatorManager.getSpectators().contains(user)) {
				SpectatorManager.getSpectatorTeam().removeEntry(event.getNickName());
			} else {
				ScoreboardManager.getScoreboardTeams().get(TeamManager.getPlayersTeam(user)).removeEntry(event.getNickName());
			}
		} else if (SpectatorManager.getSpectators().contains(user)) {
			Bukkit.getOnlinePlayers().forEach(all -> all.hidePlayer(user.getBukkitPlayer()));
			user.getBukkitPlayer().setAllowFlight(true);
			user.getBukkitPlayer().setFlying(true);
			user.getBukkitPlayer().spigot().setCollidesWithEntities(false);
			user.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));
		}
	}

	@EventHandler
	public void onPlayerNicked(PlayerNickedEvent event) {
		IUser user = event.getUser();
		if (!SpectatorManager.getSpectators().contains(user)) {
			for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
				if (!SpectatorManager.getSpectators().contains(user)) {
					onlinePlayer.hidePlayer(user.getBukkitPlayer());
				}
			}
		} else if (SpectatorManager.getSpectators().contains(user)) {
			Bukkit.getOnlinePlayers().forEach(all -> all.hidePlayer(user.getBukkitPlayer()));
			user.getBukkitPlayer().setAllowFlight(true);
			user.getBukkitPlayer().setFlying(true);
			user.getBukkitPlayer().spigot().setCollidesWithEntities(false);
			user.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));
		}
	}

	@EventHandler
	public void onPlayerUnnicked(PlayerUnnickedEvent event) {
		IUser user = event.getUser();
		if (SpectatorManager.getSpectators().contains(user)) {
			for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
				if (!SpectatorManager.getSpectators().contains(UserManager.getUser(onlinePlayer))) {
					onlinePlayer.hidePlayer(user.getBukkitPlayer());
				}
			}
		}
	}
}
