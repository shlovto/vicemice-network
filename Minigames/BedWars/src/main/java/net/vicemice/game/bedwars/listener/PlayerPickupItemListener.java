package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.player.PlayerData;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.util.ColorManager;
import net.vicemice.game.bedwars.shop.GameType;
import net.vicemice.game.bedwars.player.spectator.SpectatorManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.actionbat.ActionBarAPI;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

public class PlayerPickupItemListener implements Listener {

    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        IUser user = UserManager.getUser(event.getPlayer());
        if (BedWars.getBuildPlayers().contains(user.getBukkitPlayer())) {
            return;
        }
        if (GameManager.getGameState() != GameState.INGAME) {
            event.setCancelled(true);
            return;
        }
        if (SpectatorManager.getSpectators().contains(user)) {
            event.setCancelled(true);
            return;
        }
        Material type = event.getItem().getItemStack().getType();
        if (type == Material.STAINED_CLAY) {
            event.setCancelled(true);
            PlayerData playerData = PlayerManager.getPlayerDatas().get(user);
            if (playerData == null) return;
            ItemStack cloneItemStack = event.getItem().getItemStack().clone();
            DyeColor dyeColor = ColorManager.getDyeColor(playerData.getGameTeam());
            cloneItemStack.setData(new MaterialData(dyeColor.getWoolData()));
            cloneItemStack.setDurability(dyeColor.getWoolData());
            if (user.getBukkitPlayer().getInventory().addItem(cloneItemStack).size() == 0) {
                event.getItem().remove();
                user.playSound(Sound.ITEM_PICKUP, 40, 1);
            }
        }
        if (BedWars.getGame().getGameType() == GameType.CLASSIC) return;
        if (!(type == Material.CLAY_BRICK || type == Material.GOLD_INGOT || type == Material.IRON_INGOT)) return;
        event.setCancelled(true);
        event.getItem().remove();
        int amount = 0;
        if (type == Material.CLAY_BRICK) {
            amount = 2 * event.getItem().getItemStack().getAmount();
        } else if (type == Material.GOLD_INGOT) {
            amount = 40 * event.getItem().getItemStack().getAmount();
        } else if (type == Material.IRON_INGOT) {
            amount = 20 * event.getItem().getItemStack().getAmount();
        }
        if (amount != 0) {
            user.sendActionBarMessage("§a+ §7" + amount + " "+ user.translate("points"));
            user.playSound(Sound.ITEM_PICKUP, 40, 1);
            user.getBukkitPlayer().setLevel(user.getBukkitPlayer().getLevel() + amount);
        }
    }
}
