package net.vicemice.game.bedwars.player.shop;

import net.vicemice.game.bedwars.player.shop.types.*;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/*
 * Class created at 09:45 - 06.04.2020
 * Copyright (C) elrobtossohn
 */
public class DefaultShopInventory {

    public GUI create(IUser user, ShopType shopType) {
        GUI gui = user.createGUI("Loading...", 5);
        if (shopType != null)
            gui = gui.rename(user.translate(shopType.name().toLowerCase()));

        gui.fill(0, 9, user.getUIColor());

        gui.fill(18, 28, user.getUIColor());
        gui.fill(35, 45, user.getUIColor());


        gui.setItem(10, ItemBuilder.create(Material.COBBLESTONE).name("§f"+user.translate("blocks")).glow((shopType == ShopType.BLOCKS)).build(), e -> {
            if (shopType == ShopType.BLOCKS) return;
            new BlockShopInventory().create(user).open();
        });
        gui.setItem(11, ItemBuilder.create(Material.DIAMOND_PICKAXE).name("§f"+user.translate("tools")).glow((shopType == ShopType.TOOLS)).build(), e -> {
            if (shopType == ShopType.TOOLS) return;
            new ToolShopInventory().create(user).open();
        });
        gui.setItem(12, ItemBuilder.create(Material.IRON_CHESTPLATE).name("§f"+user.translate("armor")).glow((shopType == ShopType.ARMOR)).build(), e -> {
            if (shopType == ShopType.ARMOR) return;
            new ArmorShopInventory().create(user).open();
        });
        gui.setItem(13, ItemBuilder.create(Material.GOLD_SWORD).name("§f"+user.translate("weapons")).glow((shopType == ShopType.WEAPONS)).build(), e -> {
            if (shopType == ShopType.WEAPONS) return;
            new WeaponShopInventory().create(user).open();
        });
        gui.setItem(14, ItemBuilder.create(Material.COOKED_BEEF).name("§f"+user.translate("food")).glow((shopType == ShopType.FOOD)).build(), e -> {
            if (shopType == ShopType.FOOD) return;
            new FoodShopInventory().create(user).open();
        });
        gui.setItem(15, ItemBuilder.create(Material.CHEST).name("§f"+user.translate("chests")).glow((shopType == ShopType.CHESTS)).build(), e -> {
            if (shopType == ShopType.CHESTS) return;
            new ChestShopInventory().create(user).open();
        });
        gui.setItem(16, ItemBuilder.create(Material.TNT).name("§f"+user.translate("special")).glow((shopType == ShopType.SPECIAL)).build(), e -> {
            if (shopType == ShopType.SPECIAL) return;
            new SpecialShopInventory().create(user).open();
        });

        return gui;
    }

    public enum ShopType {
        BLOCKS,
        TOOLS,
        ARMOR,
        WEAPONS,
        FOOD,
        CHESTS,
        SPECIAL
    }
}
