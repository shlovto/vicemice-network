package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.BedWars;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;

public class ItemSpawnListener implements Listener {
    @EventHandler
    public void onItemSpawn(ItemSpawnEvent event) {
        if (event.getEntity().getItemStack().getType() == Material.BED) {
            event.setCancelled(true);
        }
        if (BedWars.getDontSpawnMaterials().contains(event.getEntity().getItemStack().getType())) {
            event.setCancelled(true);
        }
    }
}
