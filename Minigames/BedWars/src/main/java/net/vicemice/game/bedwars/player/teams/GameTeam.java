package net.vicemice.game.bedwars.player.teams;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.config.game.GameImporter;
import net.vicemice.game.bedwars.player.PlayerData;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.board.ingame.GameScoreboardManager;
import net.vicemice.game.bedwars.player.teams.lobby.LobbyTeamManager;
import net.vicemice.game.bedwars.util.ColorManager;
import net.vicemice.game.bedwars.util.ParticleEffect;
import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.gameapi.util.Title;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class GameTeam {
    @Getter
    private String colorName;
    private String rightName;
    @Getter
    private ChatColor color;
    @Getter
    private List<IUser> players = new ArrayList<>();
    @Getter
    @Setter
    private boolean bedAlive = true;
    @Getter
    @Setter
    private boolean played = false;
    @Getter
    private Inventory teamInventory;
    @Getter
    private ParticleEffect.OrdinaryColor ordinaryColor;
    @Getter
    private DyeColor dyeColor;

    @Getter
    @Setter
    private int tabPriority = 0;

    private static int tabPriorityCounter = 100;

    public GameTeam(String colorName, String rightName, ChatColor color) {
        this.colorName = colorName;
        this.rightName = rightName;
        this.color = color;
        this.tabPriority = tabPriorityCounter++;
        this.dyeColor = ColorManager.getDyeColor(this);
        this.teamInventory = Bukkit.getServer().createInventory(null, 27, "Team Chest");
        this.ordinaryColor = new ParticleEffect.OrdinaryColor(dyeColor.getColor().getRed(), dyeColor.getColor().getGreen(), dyeColor.getColor().getBlue());
    }

    public void join(IUser user, boolean opJoin) {
        if (players.size() >= BedWars.getGameConfig().getPER_TEAM() && !opJoin) {
            user.sendMessage("team-full", user.translate("bw-prefix"), getColor()+getRightName(user));
            return;
        }
        if (players.contains(user)) {
            user.sendMessage("already-in-team", user.translate("bw-prefix"));
            return;
        }
        for (String teamName : GameImporter.getTeams().keySet()) {
            GameTeam gameTeam = GameImporter.getTeams().get(teamName);
            if (gameTeam.getPlayers().contains(user)) {
                gameTeam.getPlayers().remove(user);
            }
        }
        players.add(user);
        LobbyTeamManager.updateSelectInventory();

        user.sendMessage("team-joined", user.translate("bw-prefix"), getColor()+getRightName(user));
    }

    public void destroyBed(IUser user) {
        PlayerData playerData = PlayerManager.getPlayerDatas().get(user);
        if (bedAlive) {
            bedAlive = false;
            for (IUser onlinePlayer : UserManager.getConnectedUsers().values()) {
                onlinePlayer.playSound(onlinePlayer.getLocation(), Sound.WITHER_DEATH, 40, 1);
                if (players.contains(onlinePlayer)) {
                    new Title("§0", onlinePlayer.translate("bed-destroyed-title"), 0, 2, 1).send(onlinePlayer.getBukkitPlayer());
                    onlinePlayer.sendMessage(onlinePlayer.translate("bed-destroyed"));
                    PlayerManager.getPlayerDatas().get(onlinePlayer).receiveDamageFrom(user);
                }
            }
            playerData.addDestroyedBed();
            UserManager.getConnectedUsers().values().forEach(o -> o.sendMessage(o.translate("bed-destroyed-all", color+getRightName(o), playerData.getGameTeam().getColor()+user.getNickName())));
            for (Locale locale : GoServer.getLocaleManager().getLocales().keySet()) {
                GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(locale, "bed-destroyed-all", colorName+getRightName(locale), playerData.getGameTeam().getColor()+user.getNickName()), locale);
            }
        }
        GameScoreboardManager.updateAllTeams();
    }

    public boolean canRespawn() {
        return bedAlive;
    }

    public String getRightName() {
        return GoServer.getLocaleManager().getMessage(Locale.GERMANY, rightName);
    }

    public String getRightName(Locale locale) {
        return GoServer.getLocaleManager().getMessage(locale, rightName);
    }

    public String getRightName(IUser user) {
        return user.translate(rightName);
    }

    public int getTeamColor() {
        int teamColor = 0;
        if (this.getColor() == ChatColor.RED)
            teamColor = 14;
        if (this.getColor() == ChatColor.BLUE)
            teamColor = 11;
        if (this.getColor() == ChatColor.YELLOW)
            teamColor = 4;
        if (this.getColor() == ChatColor.GREEN)
            teamColor = 5;
        if (this.getColor() == ChatColor.LIGHT_PURPLE)
            teamColor = 6;
        if (this.getColor() == ChatColor.AQUA)
            teamColor = 3;
        if (this.getColor() == ChatColor.GOLD)
            teamColor = 1;
        if (this.getColor() == ChatColor.DARK_PURPLE)
            teamColor = 10;
        return teamColor;
    }
}
