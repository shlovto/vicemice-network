package net.vicemice.game.bedwars.game.tasks;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.game.VotingManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.gameapi.events.GameLobbyEvent;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.actionbat.ActionBarAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/*
 * Class created at 18:08 - 02.04.2020
 * Copyright (C) elrobtossohn
 */
public class GameLobbyListener implements Listener {



    @EventHandler
    public void onLobby(GameLobbyEvent event) {
        if (!(Bukkit.getOnlinePlayers().size() >= BedWars.getGameConfig().getTO_START())) {
            for (IUser user : UserManager.getConnectedUsers().values()) {
                user.getBukkitPlayer().setExp(0);
                user.getBukkitPlayer().setLevel(0);

                int need = BedWars.getGameConfig().getTO_START() - Bukkit.getOnlinePlayers().size();
                if (need == 1) {
                    user.sendActionBarMessage(user.translate("game-start-more-player"));
                } else {
                    user.sendActionBarMessage(user.translate("game-start-more-players", need));
                }
            }
        } else {
            if (GameManager.getLobbyCount() > 20 && Bukkit.getServer().getOnlinePlayers().size() == GoServer.getService().getGoConfig().getMAX_PLAYERS()) {
                GameManager.setLobbyCount(20);
            }
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.setLevel(GameManager.getLobbyCount());
                player.setExp(GameManager.getLobbyCount() * 0.0166666666666667F);
            }
            if (GameManager.getLobbyCount() == 60 || GameManager.getLobbyCount() == 45 || GameManager.getLobbyCount() == 30 || GameManager.getLobbyCount() == 20 || GameManager.getLobbyCount() == 15 || GameManager.getLobbyCount() == 10 || GameManager.getLobbyCount() <= 5) {
                if (GameManager.getLobbyCount() == 1) {
                    GameManager.startGame();
                    UserManager.getConnectedUsers().values().forEach((o) -> {
                        o.sendMessage("game-lobby-last-msg", o.translate("bw-prefix"));
                    });
                    GoServer.getLocaleManager().getLocales().forEach((k, v) -> {
                        GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "game-lobby-last-msg", GoServer.getLocaleManager().getPrivateMessage(k, "bw-prefix")), k);
                    });
                } else {
                    UserManager.getConnectedUsers().values().forEach((o) -> {
                        o.sendMessage("game-lobby-msg", o.translate("bw-prefix"), GameManager.getLobbyCount());
                    });
                    GoServer.getLocaleManager().getLocales().forEach((k, v) -> {
                        GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "game-lobby-msg", GoServer.getLocaleManager().getPrivateMessage(k, "bw-prefix"), GameManager.getLobbyCount()), k);
                    });
                }

                if (GameManager.getLobbyCount() == 10) {
                    VotingManager.endVoting();
                    GameManager.sendTitle("§cBed§fWars", "§e" + BedWars.getGame().getBedWarsMap().getDisplayName());
                }
            }

            if (GameManager.getLobbyCount() > 0) GameManager.setLobbyCount(GameManager.getLobbyCount() - 1);
        }
    }
}