package net.vicemice.game.bedwars.player.shop.types;

import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.shop.DefaultShopInventory;
import net.vicemice.game.bedwars.player.shop.NeedResourceType;
import net.vicemice.game.bedwars.player.shop.ShopManager;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

/*
 * Class created at 13:15 - 06.04.2020
 * Copyright (C) elrobtossohn
 */
public class ArmorShopInventory {

    public GUI create(IUser user) {
        GUI gui = new DefaultShopInventory().create(user, DefaultShopInventory.ShopType.ARMOR);

        String bronze = user.translate("bronze");
        String iron = user.translate("iron");
        String gold = user.translate("gold");
        GameTeam gameTeam = PlayerManager.getPlayerDatas().get(user).getGameTeam();

        gui.setItem(28, ItemBuilder.create(Material.LEATHER_HELMET).amount(1).name("&e"+user.translate("shop-armor-helmet")).lore(user.translate("price", "&c1 "+bronze)).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).enchant(Enchantment.DURABILITY, 3).build(gameTeam.getDyeColor().getColor()), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 1, ItemBuilder.create(Material.LEATHER_HELMET).amount(1).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).enchant(Enchantment.DURABILITY, 3).build(gameTeam.getDyeColor().getColor()));
        });

        gui.setItem(29, ItemBuilder.create(Material.LEATHER_CHESTPLATE).amount(1).name("&e"+user.translate("shop-armor-chestplate")).lore(user.translate("price", "&c1 "+bronze)).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).enchant(Enchantment.DURABILITY, 3).build(gameTeam.getDyeColor().getColor()), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 1, ItemBuilder.create(Material.LEATHER_CHESTPLATE).amount(1).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).enchant(Enchantment.DURABILITY, 3).build(gameTeam.getDyeColor().getColor()));
        });

        gui.setItem(30, ItemBuilder.create(Material.LEATHER_LEGGINGS).amount(1).name("&e"+user.translate("shop-armor-leggings")).lore(user.translate("price", "&c1 "+bronze)).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).enchant(Enchantment.DURABILITY, 3).build(gameTeam.getDyeColor().getColor()), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 1, ItemBuilder.create(Material.LEATHER_LEGGINGS).amount(1).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).enchant(Enchantment.DURABILITY, 3).build(gameTeam.getDyeColor().getColor()));
        });

        gui.setItem(31, ItemBuilder.create(Material.LEATHER_BOOTS).amount(1).name("&e"+user.translate("shop-armor-boots")).lore(user.translate("price", "&c1 "+bronze)).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).enchant(Enchantment.DURABILITY, 3).build(gameTeam.getDyeColor().getColor()), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 1, ItemBuilder.create(Material.LEATHER_BOOTS).amount(1).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).enchant(Enchantment.DURABILITY, 3).build(gameTeam.getDyeColor().getColor()));
        });

        gui.setItem(32, ItemBuilder.create(Material.CHAINMAIL_CHESTPLATE).amount(1).name("&e"+user.translate("shop-armor-chainmail-chestplate")).lore(user.translate("price", "&71 "+iron)).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).enchant(Enchantment.DURABILITY, 3).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.IRON, 1, ItemBuilder.create(Material.CHAINMAIL_CHESTPLATE).amount(1).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).enchant(Enchantment.DURABILITY, 3).build());
        });

        gui.setItem(33, ItemBuilder.create(Material.IRON_CHESTPLATE).amount(1).name("&e"+user.translate("shop-armor-iron-chestplate")).lore(user.translate("price", "&75 "+iron)).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).enchant(Enchantment.DURABILITY, 3).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.IRON, 5, ItemBuilder.create(Material.IRON_CHESTPLATE).amount(1).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).enchant(Enchantment.DURABILITY, 3).build());
        });

        gui.setItem(34, ItemBuilder.create(Material.DIAMOND_CHESTPLATE).amount(1).name("&e"+user.translate("shop-armor-diamond-chestplate")).lore(user.translate("price", "&63 "+gold)).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).enchant(Enchantment.DURABILITY, 3).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.GOLD, 3, ItemBuilder.create(Material.DIAMOND_CHESTPLATE).amount(1).enchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).enchant(Enchantment.DURABILITY, 3).build());
        });

        return gui;
    }
}
