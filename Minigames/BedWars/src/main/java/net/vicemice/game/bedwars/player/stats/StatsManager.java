package net.vicemice.game.bedwars.player.stats;

import lombok.Getter;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class StatsManager {
    @Getter
    private static HashMap<String, HashMap<String, Long>> statsValues = new HashMap<>();

    public static long getPlayerStats(String playerName, String key) {
        if (statsValues.containsKey(playerName.toLowerCase())) {
            HashMap<String, Long> statsMap = statsValues.get(playerName.toLowerCase());
            if (statsMap == null) return -1;
            if (statsMap.containsKey(key)) {
                return statsMap.get(key);
            }
        }
        return 0;
    }
}