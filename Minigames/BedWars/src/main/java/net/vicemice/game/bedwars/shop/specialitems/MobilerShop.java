package net.vicemice.game.bedwars.shop.specialitems;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.hector.utils.players.api.internal.SkullChanger;
import org.bukkit.*;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.EulerAngle;

public class MobilerShop {
	private static String[] skinNames = {"PremiumPlus", "elrobtossohn", "Folge"};
    private int yaw, despawn;
    private int bukkitTask;
    private Location location;

    public MobilerShop(Location location) {
        this.location = location;
    }

    public void start() {
        ItemStack head = SkullChanger.getSkull(Bukkit.getPlayer(skinNames[(int) (System.currentTimeMillis() % skinNames.length)]));
        Location spawnLocation = location.clone();
        spawnLocation.add(0.5, 0, 0.5);

        spawnLocation.add(0, 1, 0);
        Entity armorEntity = spawnLocation.getWorld().spawnEntity(spawnLocation, EntityType.ARMOR_STAND);
        ArmorStand armorStand = (ArmorStand) armorEntity;
        armorStand.setHelmet(head);
        armorStand.setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
        armorStand.setVisible(false);
        armorStand.setGravity(false);
        armorStand.setCustomNameVisible(true);
        armorStand.setCustomName("§a15 §6Sekunden");

        yaw = 0;
        despawn = 30;
        bukkitTask = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(BedWars.getInstance(), () -> {
            if (despawn == 0) {
                Bukkit.getServer().getScheduler().cancelTask(bukkitTask);
                spawnLocation.getWorld().playSound(spawnLocation, Sound.ENDERMAN_TELEPORT, 2, 1);
                spawnLocation.getWorld().playEffect(spawnLocation, Effect.ENDER_SIGNAL, 10);
                armorStand.remove();
            } else {
                armorStand.setCustomName("§a" + despawn / 2 + " §6Sekunden");
                despawn--;


                if (yaw == -1) {
                    yaw = 0;
                } else {
                    if (yaw < 0) {
                        yaw--;
                    } else {
                        if (yaw >= 180) {
                            yaw = -180;
                        } else {
                            yaw++;
                        }
                    }
                }
                armorStand.setHeadPose(new EulerAngle(0, yaw, 0));
            }
        }, 10L, 10L);
    }
}
