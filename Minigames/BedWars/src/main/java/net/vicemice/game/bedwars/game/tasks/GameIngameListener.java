package net.vicemice.game.bedwars.game.tasks;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.board.ingame.GameScoreboardManager;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.game.bedwars.player.teams.TeamManager;
import net.vicemice.game.bedwars.spawner.SpawnerManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.events.GameIngameEvent;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.actionbat.ActionBarAPI;
import net.vicemice.mctv.MCTV;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/*
 * Class created at 13:12 - 03.04.2020
 * Copyright (C) elrobtossohn
 */
public class GameIngameListener implements Listener {
    @Getter
    @Setter
    private static boolean fullIngame = false;
    private static int toCheck = 10;

    @EventHandler
    public void onIngame(GameIngameEvent event) {
        if (Bukkit.getOnlinePlayers().size() == 0) {
            Bukkit.getServer().shutdown();
        }
        if (GameManager.getReplayStartSeconds() == 0) {
            GameManager.setReplayStartSeconds(MCTV.getInstance().getApi().getRecordedSeconds());
        }

        UserManager.getConnectedUsers().values().forEach(user -> {
            GameScoreboardManager.createBoard(user);
            if (PlayerManager.getPlayerDatas().containsKey(user)) {
                GameTeam gameTeam = PlayerManager.getPlayerDatas().get(user).getGameTeam();
                user.sendActionBarMessage(gameTeam.getColor()+user.translate("action-team", gameTeam.getRightName(user)));
            } else {
                user.sendActionBarMessage(user.translate("action-spectator"));
            }
        });
        GameScoreboardManager.updateAllTeams();
        SpawnerManager.spawnItems();
        if (isFullIngame()) {
            if (toCheck == 0) {
                toCheck = 10;
                try {
                    for (GameTeam gameTeam : TeamManager.getAliveTeams()) {
                        for (IUser user : gameTeam.getPlayers()) {
                            if (!PlayerManager.getPlayerDatas().containsKey(user)) {
                                TeamManager.playerQuit(user);
                            }
                        }
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
            } else {
                toCheck--;
            }
        }
    }
}