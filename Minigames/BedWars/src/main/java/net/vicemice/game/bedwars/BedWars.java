package net.vicemice.game.bedwars;

import net.vicemice.game.bedwars.commands.Command_BWAdmin;
import net.vicemice.game.bedwars.config.game.GameConfig;
import net.vicemice.game.bedwars.config.game.GameConfiguration;
import net.vicemice.game.bedwars.game.Game;
import net.vicemice.game.bedwars.game.MapManager;
import net.vicemice.game.bedwars.game.StartupManager;
import net.vicemice.game.bedwars.player.party.PartyPacketListener;
import net.vicemice.game.bedwars.commands.Command_Start;
import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.Register;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BedWars extends JavaPlugin {
    @Getter
    private static BedWars instance;
    @Getter
    private static List<Material> whiteListMaterials = new ArrayList<>();
    @Getter
    private static List<Material> dontSpawnMaterials = new ArrayList<>();
    @Getter
    @Setter
    private static boolean testMode = false;
    @Getter
    @Setter
    private static boolean event = false;
    @Getter
    @Setter
    private static boolean open = false;
    @Getter
    private static List<Player> buildPlayers = new ArrayList<>();
    @Getter
    public static Game game;
    @Getter
    public static MapManager mapManager;
    @Getter
    public static GameConfig gameConfig;
    @Getter
    public static String statsPrefix;

    @Override
    public void onEnable() {
        instance = this;
        game = new Game();
        mapManager = new MapManager();
        GoServer.getLocaleManager().addPaths(new ArrayList<>(Arrays.asList("cloud/locales/game", "cloud/locales/bedwars", "cloud/locales/bedwars-shop")));
        Register.registerListener(this, "net.vicemice.game.bedwars.listener");
        Register.registerListener(this, "net.vicemice.game.bedwars.game.tasks");

        GameConfiguration gameConfiguration = new GameConfiguration();
        if (!gameConfiguration.exists()) {
            GameConfig config = new GameConfig();
            gameConfiguration.writeConfiguration(config);
            System.out.println("Game Config File not found and will be created...");
            System.out.println("Game Config File was created!");
        }
        gameConfiguration.readConfiguration();
        gameConfig = gameConfiguration.getGameConfig();

        //Init BedWars
        GameAPI.initGameAPI();
        StartupManager.startupBedWars();
        getCommand("bwadmin").setExecutor(new Command_BWAdmin());
        getCommand("start").setExecutor(new Command_Start());
        //getCommand("build").setExecutor(new Command_Build());
        GoServer.getService().getGoAPI().getNettyClient().addGetter(new PartyPacketListener());

        whiteListMaterials.add(Material.RED_ROSE);
        whiteListMaterials.add(Material.YELLOW_FLOWER);
        whiteListMaterials.add(Material.SAPLING);
        whiteListMaterials.add(Material.CROPS);
        whiteListMaterials.add(Material.POTATO);
        whiteListMaterials.add(Material.CARROT);
        whiteListMaterials.add(Material.LONG_GRASS);
        whiteListMaterials.add(Material.DOUBLE_PLANT);
        whiteListMaterials.add(Material.BROWN_MUSHROOM);
        whiteListMaterials.add(Material.RED_MUSHROOM);
        whiteListMaterials.add(Material.WEB);

        dontSpawnMaterials.add(Material.RED_ROSE);
        dontSpawnMaterials.add(Material.STRING);
        dontSpawnMaterials.add(Material.YELLOW_FLOWER);
        dontSpawnMaterials.add(Material.SAPLING);
        dontSpawnMaterials.add(Material.WHEAT);
        dontSpawnMaterials.add(Material.SEEDS);
        dontSpawnMaterials.add(Material.POTATO_ITEM);
        dontSpawnMaterials.add(Material.CARROT_ITEM);
        dontSpawnMaterials.add(Material.DOUBLE_PLANT);
        dontSpawnMaterials.add(Material.RED_MUSHROOM);
        dontSpawnMaterials.add(Material.BROWN_MUSHROOM);

        if (gameConfig.isRANKED()) {
            statsPrefix = "bw_";
        } else {
            statsPrefix = "ubw_";
        }
    }

    public static boolean isEventMode() {
        if (GoServer.getService().getServerName().contains("Event")) {
            return true;
        }
        return false;
    }
}
