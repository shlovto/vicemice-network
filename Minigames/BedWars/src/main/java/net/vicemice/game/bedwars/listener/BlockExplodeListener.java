package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.BedWars;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.util.Vector;

import java.util.Iterator;

public class BlockExplodeListener implements Listener {
    @EventHandler
    public void onBlockExplode(BlockExplodeEvent event) {
        Iterator<Block> blockIterator = event.blockList().iterator();
        while (blockIterator.hasNext()) {
            Block block = blockIterator.next();
            Location location = block.getLocation();
            if (!BedWars.getGame().getPlacedBlocks().contains(location)) {
                blockIterator.remove();
            } else {
                BedWars.getGame().getPlacedBlocks().remove(location);
                float x = (float) (Math.random() * 0.4D);
                float y = 2 + (float) (Math.random() * 1.0D);
                float z = (float) (Math.random() * 0.4D);
                FallingBlock fallingblock = block.getWorld().spawnFallingBlock(block.getLocation(), block.getType(), block.getData());
                fallingblock.setDropItem(false);
                fallingblock.setVelocity(new Vector(x, y, z).normalize());
            }
        }
    }
}
