package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class LoginListener implements Listener {

    @EventHandler
    public void onLogin(PlayerLoginEvent playerLoginEvent) {
        IUser user = UserManager.getUser(playerLoginEvent.getPlayer());
        if (BedWars.isEventMode() && !BedWars.isOpen() && user.getRank().isLowerLevel(Rank.CONTENT)) {
            playerLoginEvent.disallow(PlayerLoginEvent.Result.KICK_OTHER, user.translate("event-not-available"));
        }
    }
}