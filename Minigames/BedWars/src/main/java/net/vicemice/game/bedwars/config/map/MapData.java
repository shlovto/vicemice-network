package net.vicemice.game.bedwars.config.map;

import lombok.Getter;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.config.game.GameImporter;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.LocationSerializer;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/*
 * Class created at 16:16 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class MapData {

    @Getter
    private final String name;
    @Getter
    private final String displayName;
    @Getter
    private final String creator;
    @Getter
    private final String world;
    @Getter
    private String displayItem;

    @Getter
    private final List<String> spawns,bronzeSpawner, ironSpawner,goldSpawner,beds,villager;
    @Getter
    private final ItemStack itemStack;


    public MapData(String name, String displayName, String creator, String world, String displayItem, List<String> spawns, List<String> bronzeSpawner, List<String> ironSpawner, List<String> goldSpawner, List<String> beds, List<String> villager) {
        this.name = name;
        this.displayName = displayName;
        this.creator = creator;
        this.world = world;
        this.displayItem = displayItem;
        this.spawns = spawns;
        this.bronzeSpawner = bronzeSpawner;
        this.ironSpawner = ironSpawner;
        this.goldSpawner = goldSpawner;
        this.beds = beds;
        this.villager = villager;

        if (this.displayItem == null) {
            this.displayItem = "2;0";
        }

        String[] array = this.displayItem.split(";");
        Material id = Material.valueOf(array[0]);
        int data = Integer.parseInt(array[1]);

        this.itemStack = new ItemStack(id, 1, (short) data);
        ItemMeta meta = this.itemStack.getItemMeta();
        meta.setDisplayName("§6" + this.displayName);
        this.itemStack.setItemMeta(meta);
    }

    public BedWarsMap getCreatedMap() {
        try {

            WorldCreator worldCreator = new WorldCreator(this.world);
            Bukkit.getServer().createWorld(worldCreator);

            World world = Bukkit.getWorld(this.world);
            world.setGameRuleValue("mobGriefing", "false");
            world.setGameRuleValue("doDaylightCycle", "false");
            world.setAutoSave(false);
            world.setDifficulty(Difficulty.EASY);
            world.setTime(1000L);
            world.setThundering(false);
            world.setStorm(false);
            world.setPVP(true);

            BedWarsMap map = new BedWarsMap(world, this.name, this.displayName, this.creator);

            map.setVillager(LocationSerializer.fromList(this.getVillager()));
            map.setBronzeSpawner(LocationSerializer.fromListWithoutYawAndPitch(this.getBronzeSpawner()));
            map.setIronSpawner(LocationSerializer.fromListWithoutYawAndPitch(this.getIronSpawner()));
            map.setGoldSpawner(LocationSerializer.fromListWithoutYawAndPitch(this.getGoldSpawner()));

            for (String spawn : getSpawns()) {
                String[] array = spawn.split(";");
                GameTeam gameTeam = GameImporter.getTeams().get(array[0].toLowerCase());
                Location spawnLocation = new Location(world, Double.parseDouble(array[1]), Double.parseDouble(array[2]), Double.parseDouble(array[3]) + 0.5, Float.parseFloat(array[4]), Float.parseFloat(array[5]));
                map.getSpawns().put(gameTeam, spawnLocation);
                BedWars.getGame().getSpawnLocations().add(spawnLocation.clone().subtract(0, 1, 0));
            }
            for (Location spawnerLocation : map.getBronzeSpawner()) {
                spawnerLocation.add(0.5, 1, 0.5);
            }
            for (Location spawnerLocation : map.getIronSpawner()) {
                spawnerLocation.add(0.5, 1, 0.5);
            }
            for (Location spawnerLocation : map.getGoldSpawner()) {
                spawnerLocation.add(0.5, 1, 0.5);
            }
            for (String bedData : getBeds()) {
                String[] array = bedData.split(";");
                Location bedLocation = new Location(world, Double.parseDouble(array[1]), Double.parseDouble(array[2]), Double.parseDouble(array[3]));
                Location bedHalf = findBedAround(bedLocation);
                map.getBeds().put(GameImporter.getTeams().get(array[0].toLowerCase()), Arrays.asList(bedLocation, bedHalf));
            }

            map.saveBeds();

            for (Location location : map.getVillager()) {
                //TruenoNPCApi.createNPC(BedWars.getInstance(), "§eShop", location, TruenoNPCSkinBuilder.fromPlayer(BedWars.getInstance()));

                /*Villager v = (Villager) world.spawnEntity(location, EntityType.VILLAGER);
                v.setCustomName("§eShop");
                v.setCustomNameVisible(true);
                v.setCanPickupItems(false);
                v.setRemoveWhenFarAway(false);
                noai(v);*/

                GoServer.getService().getFakeMobs().getSkinQueue().addToQueue(GoServer.getService().getFakeMobs().spawnPlayer(location, "§eShop"), this.getRandomName());
            }

            return map;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Location findBedAround(Location location) {
        Location test1 = location.clone().add(1, 0, 0);
        if (test1.getBlock().getType() == Material.BED_BLOCK) {
            return test1;
        }
        Location test2 = location.clone().add(-1, 0, 0);
        if (test2.getBlock().getType() == Material.BED_BLOCK) {
            return test2;
        }
        Location test3 = location.clone().add(0, 0, 1);
        if (test3.getBlock().getType() == Material.BED_BLOCK) {
            return test3;
        }
        Location test4 = location.clone().add(0, 0, -1);
        if (test4.getBlock().getType() == Material.BED_BLOCK) {
            return test4;
        }
        return null;
    }

    public static void noai(Entity v) throws IllegalArgumentException, SecurityException {
        net.minecraft.server.v1_8_R3.Entity nmsEn = ((CraftEntity) v).getHandle();
        NBTTagCompound compound = new NBTTagCompound();
        nmsEn.c(compound);
        compound.setByte("NoAI", (byte) 1);
        nmsEn.f(compound);
    }



    private String getRandomName() {
        Random random = new Random();
        int percent = random.nextInt(100);
        if (percent == 100) return "elrobtossohn";
        String name = "Folge";;
        if (percent >= 50) name = "elrobtossohn";
        if (percent < 50) name = "PremiumPlus";
        return name;
    }
}