package net.vicemice.game.bedwars.game;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.config.map.MapData;
import net.vicemice.game.bedwars.config.map.vote.VoteMap;
import net.vicemice.game.bedwars.config.map.vote.VoteState;
import net.vicemice.game.bedwars.player.board.lobby.LobbyScoreboardManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.utils.ItemCreator;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

/*
 * Class created at 16:14 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class VotingManager {

    @Getter
    private static List<VoteMap> votemaps = new ArrayList<>();
    @Getter
    private static List<MapData> maps = new ArrayList<>();
    @Getter
    private static HashMap<VoteMap, PlayerRankData> forceRank = new HashMap<>();
    @Getter
    @Setter
    private static VoteState voteState = VoteState.ON;
    @Getter
    @Setter
    private static VoteMap forceMap;

    public static void createVoting() {
        for (String name : BedWars.getMapManager().getMapDatas().keySet()) {
            MapData mapData = BedWars.getMapManager().getMapDatas().get(name);
            maps.add(mapData);
        }
        while (maps.size() != 0 && votemaps.size() < 3) {
            int random = Math.abs(new Random().nextInt()) % maps.size();
            MapData mapData = maps.get(random);
            ItemStack voteItem = mapData.getItemStack();
            votemaps.add(new VoteMap(mapData, voteItem));
            maps.remove(mapData);
        }
    }

    public static void removePlayerVotes(IUser user) {
        for (VoteMap voteMap : votemaps) {
            voteMap.getVotes().remove(user);
        }
    }

    public static void addPlayerVote(IUser user, int slot) {
        VoteMap voteMap = votemaps.get(slot);
        if (voteMap == null)
            return;
        if (!voteMap.getVotes().contains(user)) {
            removePlayerVotes(user);
            voteMap.getVotes().add(user);
            user.sendMessage("vote-success", user.translate("bw-prefix"), voteMap.getMapData().getDisplayName());
        } else {
            user.sendMessage("already-voted", user.translate("bw-prefix"));
        }
    }

    public static void endVoting() {
        if (!BedWars.getGameConfig().isVOTING()) return;
        voteState = VoteState.OFF;
        try {
            VoteMap bestMap = null;
            for (VoteMap voteMap : votemaps) {
                if (bestMap == null || voteMap.getVotes().size() > bestMap.getVotes().size()) {
                    bestMap = voteMap;
                }
            }
            if (getForceMap() != null) {
                bestMap = getForceMap();
            }

            for (Player player : Bukkit.getOnlinePlayers()) {
                player.playSound(player.getLocation(), Sound.ANVIL_BREAK, 10L, 10L);
            }

            Bukkit.broadcastMessage(" ");
            VoteMap finalBestMap = bestMap;
            UserManager.getConnectedUsers().values().forEach(o -> {
                o.sendMessage("vote-ends", o.translate("bw-prefix"), finalBestMap.getMapData().getDisplayName());
            });
            GoServer.getLocaleManager().getLocales().forEach((k, v) -> {
                GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "vote-ends", GoServer.getLocaleManager().getMessage(k, "bw-prefix"), finalBestMap.getMapData().getDisplayName()), k);
            });

            Bukkit.broadcastMessage(" ");

            BedWars.getGame().setBedWarsMap(bestMap.getMapData().getCreatedMap());
            GoAPI.getServerAPI().changeServer(ServerState.LOBBY, bestMap.getMapData().getDisplayName()+" - "+ BedWars.getGameConfig().getTYPE());
            LobbyScoreboardManager.updateAllMap();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
