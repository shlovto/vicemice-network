package net.vicemice.game.bedwars.util;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.player.PlayerData;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.spectator.SpectatorManager;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.game.bedwars.player.teams.TeamManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.gameapi.util.Title;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/*
 * Class created at 13:21 - 05.04.2020
 * Copyright (C) elrobtossohn
 */
public class DeathManager {

    public static void death(IUser user) {
        PlayerData playerData = PlayerManager.getPlayerDatas().get(user);
        if (playerData == null) return;
        playerData.setKeepLevel(user.getBukkitPlayer().getLevel());
        IUser killer = playerData.getLastHitted();
        GameTeam playerTeam = playerData.getGameTeam();
        if (killer == null) {
            MessageUtils.sendMessage("player-died", (playerTeam.getColor()+user.getNickName()));
        } else {
            PlayerData killerData = PlayerManager.getPlayerDatas().get(killer);
            if (killerData != null) {
                UserManager.getConnectedUsers().values().forEach(o -> o.sendMessage( "game-player-killed", o.translate("bw-prefix"), (playerTeam.getColor()+user.getNickName()), (killerData.getGameTeam().getColor()+killer.getNickName())));
                GoServer.getLocaleManager().getLocales().forEach((k, v) -> GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "game-player-killed", GoServer.getLocaleManager().getPrivateMessage(k, "bw-prefix"), (playerTeam.getColor()+user.getNickName()), (killerData.getGameTeam().getColor()+killer.getNickName())), k));
            } else {
                UserManager.getConnectedUsers().values().forEach(o -> o.sendMessage("game-player-died", o.translate("bw-prefix"), (playerTeam.getColor()+user.getNickName())));
                GoServer.getLocaleManager().getLocales().forEach((k, v) -> GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "game-player-died", GoServer.getLocaleManager().getPrivateMessage(k, "bw-prefix"), (playerTeam.getColor()+user.getNickName())), k));
            }

            if (!playerData.getGameTeam().canRespawn()) {
                playKillSound(killer);
                killerData.addKill();
            }
            playerData.setLastHitted(null);
        }
        if (!playerTeam.canRespawn()) {
            playerData.addDeath();
            playerData.saveStats();
            SpectatorManager.spectatorPlayer(user);
            TeamManager.playerDeath(user);
            PlayerManager.getPlayerDatas().remove(user);
            new Title(user.translate("game-you-died"), " ", 0, 2, 1).send(user.getBukkitPlayer());
            SpectatorManager.updateSpectatorInventory();
            return;
        }
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(BedWars.getInstance(), () -> {
            if (user.isOnline()) user.getBukkitPlayer().spigot().respawn();
        }, 10L);
    }

    public static void playKillSound(IUser user) {
        user.playSound(Sound.LEVEL_UP, 40, 1);
    }
}