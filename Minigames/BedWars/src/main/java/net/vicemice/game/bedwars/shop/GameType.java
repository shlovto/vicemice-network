package net.vicemice.game.bedwars.shop;

public enum GameType {
    MODERN, CLASSIC
}
