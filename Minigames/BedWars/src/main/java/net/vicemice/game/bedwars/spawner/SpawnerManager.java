package net.vicemice.game.bedwars.spawner;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.shop.GameType;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class SpawnerManager {
    private static int goldSpawn = 0;
    private static int ironSpawn = 0;

    public static void spawnItems() {
        for (Location location : BedWars.getGame().getBedWarsMap().getBronzeSpawner()) {
            if (BedWars.getGame().getGameType() == GameType.CLASSIC) {
                location.getWorld().dropItem(location, new ItemStack(Material.CLAY_BRICK));
                location.getWorld().dropItem(location, new ItemStack(Material.CLAY_BRICK));
            } else {
                location.getWorld().dropItem(location, new ItemStack(Material.CLAY_BRICK));
            }
        }
        if (goldSpawn == 0) {
            goldSpawn = 35;
            for (Location location : BedWars.getGame().getBedWarsMap().getGoldSpawner()) {
                location.getWorld().dropItem(location, new ItemStack(Material.GOLD_INGOT));
            }
        } else {
            goldSpawn--;
        }
        if (ironSpawn == 0) {
            ironSpawn = 8;
            if (BedWars.getGameConfig().getTYPE().endsWith("x1")) {
                ironSpawn = 15;
            }
            for (Location location : BedWars.getGame().getBedWarsMap().getIronSpawner()) {
                location.getWorld().dropItem(location, new ItemStack(Material.IRON_INGOT));
            }
        } else {
            ironSpawn--;
        }
    }
}
