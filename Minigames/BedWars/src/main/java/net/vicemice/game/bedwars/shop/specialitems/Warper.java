package net.vicemice.game.bedwars.shop.specialitems;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.player.PlayerData;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.util.ParticleEffect;
import net.vicemice.game.bedwars.shop.ItemTimeouts;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

public class Warper {
    @Getter
    private Player player;
    private int levelBefore;
    private int count = 5;
    private BukkitTask bukkitTask;
    private ItemStack itemStack;
    private int startX, startY, startZ;

    public Warper(Player player, ItemStack itemStack) {
        this.player = player;
        this.itemStack = itemStack;
    }

    public void start() {
        startX = player.getLocation().getBlockX();
        startY = player.getLocation().getBlockY();
        startZ = player.getLocation().getBlockZ();
        levelBefore = player.getLevel();
        bukkitTask = Bukkit.getServer().getScheduler().runTaskTimer(BedWars.getInstance(), () -> {
            Location playerLocation = player.getLocation();
            if (playerLocation.getBlockX() != startX || playerLocation.getBlockY() != startY || playerLocation.getBlockZ() != startZ) {
                bukkitTask.cancel();
                endTeleport(false);
                player.playSound(playerLocation, Sound.ITEM_BREAK, 40, 1);
                return;
            }
            player.getWorld().playSound(player.getLocation(), Sound.NOTE_PLING, 2, 1);
            ParticleEffect.PORTAL.display(0.3F, 1, 0.3F, 0.0001F, 30, player.getLocation(), 20);
            if (count == 0) {
                bukkitTask.cancel();
                endTeleport(true);
            } else {
                player.setLevel(count);
                player.setExp((float) 0.2 * count);
                count--;
            }
        }, 0L, 20L);
    }

    public void endTeleport(boolean teleport) {
        player.setLevel(levelBefore);
        player.setExp(0);
        ItemTimeouts.getWarpers().remove(player);
        for (ItemStack itemStack : player.getInventory().getContents()) {
            if (itemStack != null) {
                if (itemStack.getType() == Material.GLOWSTONE_DUST) {
                    itemStack.setType(Material.SULPHUR);
                }
            }
        }
        if (teleport) {
            PlayerData playerData = PlayerManager.getPlayerDatas().get(player);
            if (playerData != null) {
                player.teleport(BedWars.getGame().getBedWarsMap().getSpawns().get(playerData.getGameTeam()));
                player.getInventory().removeItem(itemStack);
            }
        }
    }
}
