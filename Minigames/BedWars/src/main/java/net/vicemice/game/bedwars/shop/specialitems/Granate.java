package net.vicemice.game.bedwars.shop.specialitems;

import net.vicemice.game.bedwars.BedWars;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.scheduler.BukkitTask;

public class Granate {
    @Getter
    private BukkitTask bukkitTask;
    private int currentStep, nextSound, nextSoundTemplate;

    public Granate(Location location) {
        nextSoundTemplate = 10;
        bukkitTask = Bukkit.getServer().getScheduler().runTaskTimer(BedWars.getInstance(), () -> {
            try {
                if (currentStep == 70) {
                    bukkitTask.cancel();
                    location.getWorld().createExplosion(location, 10);
                } else {
                    if (nextSound == 0) {
                        nextSound = nextSoundTemplate;
                        if (nextSoundTemplate >= 0) {
                            nextSoundTemplate--;
                        }
                        location.getWorld().playSound(location, Sound.NOTE_PLING, 1, 1);
                    } else {
                        nextSound--;
                    }
                    currentStep++;
                }
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }, 1L, 1L);
    }
}
