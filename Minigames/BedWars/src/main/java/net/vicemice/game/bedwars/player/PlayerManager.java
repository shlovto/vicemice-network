package net.vicemice.game.bedwars.player;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.config.game.GameImporter;
import net.vicemice.game.bedwars.game.scoreboard.ScoreboardManager;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.game.bedwars.player.teams.TeamManager;
import net.vicemice.game.bedwars.player.spectator.SpectatorManager;
import lombok.Getter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scoreboard.Team;
import java.util.HashMap;
import java.util.Random;

public class PlayerManager {
    @Getter
    private static HashMap<IUser, PlayerData> playerDatas = new HashMap<>();

    public static void startGameFor(IUser user, boolean manAdd) {
        user.getBukkitPlayer().spigot().setCollidesWithEntities(true);
        user.getBukkitPlayer().setGameMode(GameMode.SURVIVAL);
        PlayerData playerData = new PlayerData(user);
        playerDatas.put(user, playerData);
        GameTeam gameTeam = TeamManager.getPlayersTeam(user);
        if (gameTeam == null || gameTeam == TeamManager.getDefaultTeam()) {
            GameTeam bestTeam = getRandomTeam();
            while (bestTeam != null && (bestTeam.getPlayers().size() == BedWars.getGameConfig().getPER_TEAM())) {
                bestTeam = getRandomTeam();
            }
            /*for (String teamName : GameImporter.getTeams().keySet()) {
                GameTeam getTeam = GameImporter.getTeams().get(teamName);

                if (bestTeam == null || (getTeam.getPlayers().size() < bestTeam.getPlayers().size() && getTeam.getPlayers().size() <= BedWars.getGameConfig().getPER_TEAM())) {
                    bestTeam = getTeam;
                }
            }*/
            if (bestTeam != null) {
                gameTeam = bestTeam;
                bestTeam.join(user, false);
                user.sendMessage("game-team", user.translate("bw-prefix"), (gameTeam.getColor()+gameTeam.getRightName(user)));
            } else {
                user.sendMessage("game-no-team-found", user.translate("bw-prefix"));
                SpectatorManager.spectatorPlayer(user);
                return;
            }
        } else {
            user.sendMessage("game-team", user.translate("bw-prefix"), (gameTeam.getColor()+gameTeam.getRightName(user)));
        }
        playerData.setGameTeam(gameTeam);
        if (!ScoreboardManager.getScoreboardTeams().containsKey(gameTeam)) {
            Team team = ScoreboardManager.getScoreboard().registerNewTeam(gameTeam.getColorName());
            team.setCanSeeFriendlyInvisibles(true);
            team.setPrefix(gameTeam.getColor() + "");
            ScoreboardManager.getScoreboardTeams().put(gameTeam, team);
        }
        user.getBukkitPlayer().setScoreboard(ScoreboardManager.getScoreboard());
        ScoreboardManager.getScoreboardTeams().get(gameTeam).addEntry(user.getName());
        if (user.isNicked()) {
            ScoreboardManager.getScoreboardTeams().get(gameTeam).addEntry(user.getNickName());
        }
        if (!manAdd) {
            for (IUser onlinePlayer : UserManager.getConnectedUsers().values()) {
                user.getBukkitPlayer().hidePlayer(onlinePlayer.getBukkitPlayer());
                user.getBukkitPlayer().showPlayer(onlinePlayer.getBukkitPlayer());
                onlinePlayer.getBukkitPlayer().hidePlayer(user.getBukkitPlayer());
                onlinePlayer.getBukkitPlayer().showPlayer(user.getBukkitPlayer());
            }
        }
        if (!TeamManager.getAliveTeams().contains(gameTeam)) {
            TeamManager.getAliveTeams().add(gameTeam);
        }
        gameTeam.setPlayed(true);
        playerData.addPlayedGame();
        resetPlayer(user);

        if (BedWars.getGame().getBedWarsMap() != null) {
            if (BedWars.getGame().getBedWarsMap().getSpawns().containsKey(gameTeam)) {
                user.teleport(BedWars.getGame().getBedWarsMap().getSpawns().get(gameTeam));
            } else {
                user.sendRawMessage("§cNo Spawnpoint");
                System.out.println("Exception no Spawnpoint " + BedWars.getGame().getBedWarsMap().getName() + " " + gameTeam.getRightName());
            }
        } else {
            user.sendRawMessage("§cMap null");
        }

        user.sendMessage("game-teaming-with-other-teams-forbidden", user.translate("bw-prefix"));
    }

    public static void resetPlayer(IUser user) {
        user.getBukkitPlayer().getInventory().clear();
        user.getBukkitPlayer().getInventory().setArmorContents(null);
        user.getBukkitPlayer().setHealth(20);
        user.getBukkitPlayer().setFoodLevel(20);
        user.getBukkitPlayer().setExp(0);
        user.getBukkitPlayer().setLevel(0);
        user.getBukkitPlayer().setAllowFlight(false);
        for (PotionEffect potionEffect : user.getBukkitPlayer().getActivePotionEffects()) {
            user.getBukkitPlayer().removePotionEffect(potionEffect.getType());
        }
    }

    private static GameTeam getRandomTeam() {
        Random generator = new Random();
        Object[] values = GameImporter.getTeams().values().toArray();
        return (GameTeam) values[generator.nextInt(values.length)];
    }
}
