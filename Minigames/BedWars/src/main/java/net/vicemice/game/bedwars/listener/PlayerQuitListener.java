package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.game.VotingManager;
import net.vicemice.game.bedwars.player.PlayerData;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.party.PartyManager;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.game.bedwars.player.teams.TeamManager;
import net.vicemice.game.bedwars.util.DeathManager;
import net.vicemice.game.bedwars.player.teams.lobby.LobbyTeamManager;
import net.vicemice.game.bedwars.player.spectator.SpectatorManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerQuit(PlayerQuitEvent event) {
		event.setQuitMessage("");
		IUser user = UserManager.getUser(event.getPlayer());
		PlayerData playerData = PlayerManager.getPlayerDatas().get(user);
		String playerName = (user.isNicked() ? "§9"+user.getNickName() : user.getRank().getRankColor() + user.getName());
		if (GameManager.getGameState() == GameState.LOBBY) {
			VotingManager.removePlayerVotes(user);
			TeamManager.removePlayerFromTeams(user);
			LobbyTeamManager.updateSelectInventory();
			UserManager.getConnectedUsers().values().forEach(p -> p.sendMessage("game-quit", playerName));
			GoServer.getLocaleManager().getLocales().forEach((k, v) -> GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "game-quit", playerName), k));
			PartyManager.getPartyMembers().remove(user.getName().toLowerCase());
		} else if (GameManager.getGameState() == GameState.INGAME) {
			if (PlayerManager.getPlayerDatas().containsKey(user)) {
				GameTeam gameTeam = PlayerManager.getPlayerDatas().get(user).getGameTeam();
				DeathManager.death(user);
				UserManager.getConnectedUsers().values().forEach(p -> p.sendMessage("game-quit-ingame", p.translate("bw-prefix"), playerName, gameTeam.getColor()+p.translate("action-team", gameTeam.getColorName()), gameTeam.getPlayers().size()));
				GoServer.getLocaleManager().getLocales().forEach((k, v) -> GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "game-quit-ingame", GoServer.getLocaleManager().getMessage(k, "bw-prefix"), playerName, GoServer.getLocaleManager().getMessage(k, "action-team", gameTeam.getColorName()), gameTeam.getPlayers().size()), k));
			} else {
				System.out.println(user.getName() + " leaved as spectator!");
				SpectatorManager.getSpectators().remove(user);
			}
		}
		if (playerData != null) {
			playerData.saveStats();
			PlayerManager.getPlayerDatas().remove(user);
			SpectatorManager.updateSpectatorInventory();
		}
	}
}
