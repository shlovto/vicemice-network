package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.shop.specialitems.Granate;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Egg;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.util.BlockIterator;

public class ProjectileHitListener implements Listener {

    @EventHandler
    public void onProjectileHit(ProjectileHitEvent event) {
        if (event.getEntity() instanceof Egg) {
            BlockIterator blockIterator = new BlockIterator(event.getEntity().getWorld(), event.getEntity().getLocation().toVector(), event.getEntity().getVelocity().normalize(), 0.00D, 4);
            Block hitBlock = null;
            while (blockIterator.hasNext()) {
                Block getBlock = blockIterator.next();
                if (getBlock.getType() != Material.AIR) {
                    hitBlock = getBlock;
                    break;
                }
            }
            if (hitBlock != null) {
                new Granate(hitBlock.getLocation());
            }
        }
    }
}