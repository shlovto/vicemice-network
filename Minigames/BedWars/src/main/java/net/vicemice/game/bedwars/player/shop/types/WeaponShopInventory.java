package net.vicemice.game.bedwars.player.shop.types;

import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.shop.DefaultShopInventory;
import net.vicemice.game.bedwars.player.shop.NeedResourceType;
import net.vicemice.game.bedwars.player.shop.ShopManager;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

/*
 * Class created at 13:26 - 06.04.2020
 * Copyright (C) elrobtossohn
 */
public class WeaponShopInventory {

    public GUI create(IUser user) {
        GUI gui = new DefaultShopInventory().create(user, DefaultShopInventory.ShopType.WEAPONS);

        String bronze = user.translate("bronze");
        String iron = user.translate("iron");
        String gold = user.translate("gold");
        GameTeam gameTeam = PlayerManager.getPlayerDatas().get(user).getGameTeam();

        gui.setItem(28, ItemBuilder.create(Material.WOOD_SWORD).amount(1).name("&e"+user.translate("shop-weapons-wood-sword") + " &f[&7Lvl. &8I&f]").lore(user.translate("price", "&c10 "+bronze)).enchant(Enchantment.DURABILITY, 3).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 10, ItemBuilder.create(Material.WOOD_SWORD).amount(1).enchant(Enchantment.DURABILITY, 3).build());
        });

        gui.setItem(29, ItemBuilder.create(Material.WOOD_SWORD).amount(1).name("&e"+user.translate("shop-weapons-wood-sword") + " &f[&7Lvl. &8II&f]").lore(user.translate("price", "&c32 "+bronze)).enchant(Enchantment.DAMAGE_ALL, 1).enchant(Enchantment.DURABILITY, 3).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 32, ItemBuilder.create(Material.WOOD_SWORD).amount(1).enchant(Enchantment.DAMAGE_ALL, 1).enchant(Enchantment.DURABILITY, 3).build());
        });

        gui.setItem(30, ItemBuilder.create(Material.WOOD_SWORD).amount(1).name("&e"+user.translate("shop-weapons-wood-sword") + " &f[&7Lvl. &8III&f]").lore(user.translate("price", "&72 "+iron)).enchant(Enchantment.DAMAGE_ALL, 2).enchant(Enchantment.DURABILITY, 3).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.IRON, 2, ItemBuilder.create(Material.WOOD_SWORD).amount(1).enchant(Enchantment.DAMAGE_ALL, 2).enchant(Enchantment.DURABILITY, 3).build());
        });

        gui.setItem(31, ItemBuilder.create(Material.STONE_SWORD).amount(1).name("&e"+user.translate("shop-weapons-stone-sword")).lore(user.translate("price", "&75 "+iron)).enchant(Enchantment.DAMAGE_ALL, 2).enchant(Enchantment.DURABILITY, 3).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.IRON, 5, ItemBuilder.create(Material.STONE_SWORD).amount(1).enchant(Enchantment.DAMAGE_ALL, 2).enchant(Enchantment.DURABILITY, 3).build());
        });

        gui.setItem(32, ItemBuilder.create(Material.IRON_SWORD).amount(1).name("&e"+user.translate("shop-weapons-iron-sword")).lore(user.translate("price", "&63 "+gold)).enchant(Enchantment.DAMAGE_ALL, 2).enchant(Enchantment.DURABILITY, 3).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.GOLD, 3, ItemBuilder.create(Material.IRON_SWORD).amount(1).enchant(Enchantment.DAMAGE_ALL, 2).enchant(Enchantment.DURABILITY, 3).build());
        });

        gui.setItem(33, ItemBuilder.create(Material.DIAMOND_SWORD).amount(1).name("&e"+user.translate("shop-weapons-diamond-sword")).lore(user.translate("price", "&67 "+gold)).enchant(Enchantment.DAMAGE_ALL, 2).enchant(Enchantment.DURABILITY, 3).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.GOLD, 7, ItemBuilder.create(Material.DIAMOND_SWORD).amount(1).enchant(Enchantment.DAMAGE_ALL, 2).enchant(Enchantment.DURABILITY, 3).build());
        });

        return gui;
    }
}
