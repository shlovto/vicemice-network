package net.vicemice.game.bedwars.player.party;

import io.netty.channel.Channel;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.party.PartyMemberPacket;

public class PartyPacketListener extends PacketGetter {

    @Override
    public void receivePacket(PacketHolder initPacket) {
        if (initPacket.getKey().equals(PacketType.PARTY_MEMBER_PACKET.getKey())) {
            PartyMemberPacket partyMemberPacket = (PartyMemberPacket) initPacket.getValue();
            PartyManager.receivePartyPacket(partyMemberPacket);
        }
    }

    @Override
    public void channelActive(Channel channel) {
    }
}
