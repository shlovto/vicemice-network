package net.vicemice.game.bedwars.util;

import net.vicemice.hector.GoAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Locale;

/*
 * Class created at 18:20 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class MessageUtils {

    public static void sendMessage(String string) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), string));
        }
    }

    public static void sendMessage(String string, Object... objects) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), string, objects));
        }
    }

    public static void sendMessage(String message, Locale locale) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (GoAPI.getUserAPI().getLocale(player.getUniqueId()) == locale) {
                player.sendMessage(message);
            }
        }
    }
}
