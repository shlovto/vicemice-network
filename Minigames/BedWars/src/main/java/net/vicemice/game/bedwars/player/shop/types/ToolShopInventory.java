package net.vicemice.game.bedwars.player.shop.types;

import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.shop.DefaultShopInventory;
import net.vicemice.game.bedwars.player.shop.NeedResourceType;
import net.vicemice.game.bedwars.player.shop.ShopManager;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

/*
 * Class created at 10:39 - 06.04.2020
 * Copyright (C) elrobtossohn
 */
public class ToolShopInventory {

    public GUI create(IUser user) {
        GUI gui = new DefaultShopInventory().create(user, DefaultShopInventory.ShopType.TOOLS);

        String bronze = user.translate("bronze");
        String iron = user.translate("iron");
        String gold = user.translate("gold");
        GameTeam gameTeam = PlayerManager.getPlayerDatas().get(user).getGameTeam();

        gui.setItem(28, ItemBuilder.create(Material.WOOD_PICKAXE).amount(1).name("&e"+user.translate("shop-tools-woodpickaxe")).lore(user.translate("price", "&c8 "+bronze)).enchant(Enchantment.DIG_SPEED, 1).enchant(Enchantment.DURABILITY, 3).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 8, ItemBuilder.create(Material.WOOD_PICKAXE).amount(1).enchant(Enchantment.DIG_SPEED, 1).enchant(Enchantment.DURABILITY, 3).build());
        });

        gui.setItem(29, ItemBuilder.create(Material.IRON_PICKAXE).amount(1).name("&e"+user.translate("shop-tools-ironpickaxe")).lore(user.translate("price", "&75 "+iron)).enchant(Enchantment.DIG_SPEED, 1).enchant(Enchantment.DURABILITY, 3).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.IRON, 5, ItemBuilder.create(Material.IRON_PICKAXE).amount(1).enchant(Enchantment.DIG_SPEED, 1).enchant(Enchantment.DURABILITY, 3).build());
        });

        gui.setItem(30, ItemBuilder.create(Material.DIAMOND_PICKAXE).amount(1).name("&e"+user.translate("shop-tools-diamondpickaxe")).lore(user.translate("price", "&62 "+gold)).enchant(Enchantment.DIG_SPEED, 1).enchant(Enchantment.DURABILITY, 3).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.GOLD, 2, ItemBuilder.create(Material.DIAMOND_PICKAXE).amount(1).enchant(Enchantment.DIG_SPEED, 1).enchant(Enchantment.DURABILITY, 3).build());
        });

        gui.setItem(31, ItemBuilder.create(Material.WOOD_AXE).amount(1).name("&e"+user.translate("shop-tools-woodaxe")).lore(user.translate("price", "&c8 "+bronze)).enchant(Enchantment.DIG_SPEED, 1).enchant(Enchantment.DURABILITY, 3).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 8, ItemBuilder.create(Material.WOOD_AXE).amount(1).enchant(Enchantment.DIG_SPEED, 1).enchant(Enchantment.DURABILITY, 3).build());
        });

        gui.setItem(32, ItemBuilder.create(Material.IRON_AXE).amount(1).name("&e"+user.translate("shop-tools-ironaxe")).lore(user.translate("price", "&72 "+iron)).enchant(Enchantment.DIG_SPEED, 1).enchant(Enchantment.DURABILITY, 3).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.IRON, 2, ItemBuilder.create(Material.IRON_AXE).amount(1).enchant(Enchantment.DIG_SPEED, 1).enchant(Enchantment.DURABILITY, 3).build());
        });

        gui.setItem(33, ItemBuilder.create(Material.DIAMOND_AXE).amount(1).name("&e"+user.translate("shop-tools-diamondaxe")).lore(user.translate("price", "&61 "+gold)).enchant(Enchantment.DIG_SPEED, 1).enchant(Enchantment.DURABILITY, 3).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.GOLD, 1, ItemBuilder.create(Material.DIAMOND_AXE).amount(1).enchant(Enchantment.DIG_SPEED, 1).enchant(Enchantment.DURABILITY, 3).build());
        });

        gui.setItem(34, ItemBuilder.create(Material.SHEARS).amount(1).name("&e"+user.translate("shop-tools-shear")).lore(user.translate("price", "&71 "+iron)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.IRON, 1, ItemBuilder.create(Material.SHEARS).amount(1).build());
        });

        return gui;
    }
}
