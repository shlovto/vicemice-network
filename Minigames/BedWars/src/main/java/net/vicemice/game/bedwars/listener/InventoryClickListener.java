package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.player.teams.lobby.LobbyTeamManager;
import net.vicemice.game.bedwars.player.spectator.SpectatorManager;
import net.md_5.bungee.api.ChatColor;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class InventoryClickListener implements Listener {
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        IUser user = UserManager.getUser((Player) event.getWhoClicked());
        if (BedWars.getBuildPlayers().contains(user)) {
            return;
        }

        ItemStack current = event.getCurrentItem();
        if (GameManager.getGameState() == GameState.LOBBY) {
            event.setCancelled(true);
            if (event.getClickedInventory() != null && event.getClickedInventory().getTitle() != null) {
                if (event.getClickedInventory().getTitle().equalsIgnoreCase("§6Teams")) {
                    int slot = event.getSlot();
                    if (LobbyTeamManager.getTeams().containsKey(slot)) {
                        LobbyTeamManager.getTeams().get(slot).join(user, false);
                    }
                }
            }
        } else if (GameManager.getGameState() == GameState.INGAME) {
            if (SpectatorManager.getSpectators().contains(user)) {
                event.setCancelled(true);
                if (event.getClickedInventory() != null && event.getClickedInventory().getTitle() != null && event.getClickedInventory().getTitle().equals("§6Teleporter")) {
                    event.setCancelled(true);
                    if (current != null) {
                        if (current.getType() == Material.SKULL_ITEM) {
                            Player getPlayer = Bukkit.getServer().getPlayer(ChatColor.stripColor(current.getItemMeta().getDisplayName()));
                            if (getPlayer != null)
                                user.teleport(getPlayer);
                        }
                    }
                }
            }
            /*if (event.getClickedInventory() != null && event.getClickedInventory().getTitle() != null) {
                if (event.getClickedInventory().getTitle().equalsIgnoreCase("§fShop")) {
                    event.setCancelled(true);
                    PlayerData playerData = PlayerManager.getPlayerDatas().get(player);
                    if (playerData == null) {
                        return;
                    }
                    if (current != null) {
                        if (current.getType().equals(Material.DOUBLE_PLANT) && BedWars.getGame().getGameType().equals(GameType.CLASSIC)) {
                            player.playSound(player.getLocation(), Sound.NOTE_BASS, 35L, 1L);
                            player.sendMessage("§cNot available");
                            return;
                        }
                        if (ShopManager.getShopCategoryItemStack().containsKey(current)) {
                            // Clicked Shop Category
                            event.setCancelled(true);
                            ShopCategory getCategory = ShopManager.getShopCategoryItemStack().get(current);
                            if (getCategory != playerData.getPlayerShop().getCurrentCategory()) {
                                playerData.getPlayerShop().prepareShopFor(getCategory);
                                playClick(player);
                            }
                        }

                        if (playerData.getPlayerShop().getShopItemMap().containsKey(current)) {
                            // Clicked Item
                            event.setCancelled(true);
                            ShopItem shopItem = playerData.getPlayerShop().getShopItemMap().get(current);
                            if (BedWars.getGame().getGameType() == GameType.CLASSIC) {
                                if (player.getInventory().containsAtLeast(shopItem.getNeededItems(), shopItem.getNeededItems().getAmount())) {
                                    if (event.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY) {
                                        int buyedSize = 0;
                                        int buyedAmount = 0;
                                        for (; ; ) {
                                            if (buyedAmount >= shopItem.getItem().getMaxStackSize()) break;
                                            if (player.getInventory().containsAtLeast(shopItem.getNeededItems(), shopItem.getNeededItems().getAmount())) {
                                                if (player.getInventory().addItem(shopItem.getItem()).isEmpty()) {
                                                    player.getInventory().removeItem(shopItem.getNeededItems());
                                                    if (buyedSize == 0)
                                                        player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 1, -10);
                                                    buyedSize++;
                                                    buyedAmount += shopItem.getItem().getAmount();
                                                } else {
                                                    if (buyedSize == 0) {
                                                        player.sendMessage(GoAPI.getUserAPI().translate(player.getUniqueId(), "not-enough-space"));
                                                        playFail(player);
                                                    }
                                                    break;
                                                }
                                            } else {
                                                break;
                                            }
                                        }
                                    } else {
                                        if (player.getInventory().addItem(shopItem.getItem()).isEmpty()) {
                                            player.getInventory().removeItem(shopItem.getNeededItems());
                                            player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 1, -10);
                                        } else {
                                            player.sendMessage(GoAPI.getUserAPI().translate(player.getUniqueId(), "not-enough-space"));
                                            playFail(player);
                                        }
                                    }
                                } else {
                                    player.sendMessage(GoAPI.getUserAPI().translate(player.getUniqueId(), "not-enough-resources"));
                                    playFail(player);
                                }
                            } else {
                                if (player.getLevel() >= shopItem.getPrice()) {
                                    if (event.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY) {
                                        int buyedSize = 0;
                                        int buyedAmount = 0;
                                        for (; ; ) {
                                            if (buyedAmount >= shopItem.getItem().getMaxStackSize()) break;
                                            if (player.getLevel() >= shopItem.getPrice()) {
                                                if (player.getInventory().addItem(shopItem.getItem()).isEmpty()) {
                                                    player.setLevel(player.getLevel() - shopItem.getPrice());
                                                    if (buyedSize == 0)
                                                        player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 1, -10);
                                                    buyedSize++;
                                                    buyedAmount += shopItem.getItem().getAmount();
                                                } else {
                                                    if (buyedSize == 0) {
                                                        player.sendMessage(GoAPI.getUserAPI().translate(player.getUniqueId(), "not-enough-space"));
                                                        playFail(player);
                                                    }
                                                    break;
                                                }
                                            } else {
                                                break;
                                            }
                                        }
                                    } else {
                                        if (player.getInventory().addItem(shopItem.getItem()).isEmpty()) {
                                            player.setLevel(player.getLevel() - shopItem.getPrice());
                                            player.playSound(player.getLocation(), Sound.CHICKEN_EGG_POP, 1, -10);
                                        } else {
                                            player.sendMessage(GoAPI.getUserAPI().translate(player.getUniqueId(), "not-enough-space"));
                                            playFail(player);
                                        }
                                    }
                                } else {
                                    player.sendMessage(GoAPI.getUserAPI().translate(player.getUniqueId(), "not-enough-points"));
                                    playFail(player);
                                }
                            }
                        }
                    }
                } else {
                    if (current != null && current.getType() == Material.STAINED_CLAY) {
                        PlayerData playerData = PlayerManager.getPlayerDatas().get(player);
                        if (playerData == null) return;
                        DyeColor dyeColor = ColorManager.getDyeColor(playerData.getGameTeam());
                        current.setData(new MaterialData(dyeColor.getWoolData()));
                        current.setDurability(dyeColor.getWoolData());
                    }
                }
            }*/
        }
    }
}