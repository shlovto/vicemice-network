package net.vicemice.game.bedwars.player;

import net.vicemice.game.bedwars.util.ItemStackHelper;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public enum LobbyItems {
    MAPVOTING(ItemStackHelper.createItemStack(Material.PAPER, 0, "§6Kartenauswahl §8(§eRechtsklick§8)")),
    FORCEMAP(ItemStackHelper.createItemStack(Material.CHEST, 0, "§6Forcemap §8(§eRechtsklick§8)")),
    TRACKER(ItemStackHelper.createItemStack(Material.COMPASS, 0, "§7Teleporter §8(§eRechtsklick§8)"));
    ItemStack itemStack;

    LobbyItems(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public ItemStack get() {
        return itemStack;
    }
}
