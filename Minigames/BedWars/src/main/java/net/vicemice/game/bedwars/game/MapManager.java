package net.vicemice.game.bedwars.game;

import lombok.Getter;
import net.vicemice.game.bedwars.config.map.BedWarsMap;
import net.vicemice.game.bedwars.config.map.MapData;
import org.bukkit.Location;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Class created at 15:56 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class MapManager {

    @Getter
    private final Map<String, MapData> mapDatas;
    @Getter
    private final Map<String, BedWarsMap> maps;
    @Getter
    private final List<Location> spawnPoints;

    public MapManager() {
        this.mapDatas = new HashMap<>();
        this.maps = new HashMap<>();
        this.spawnPoints = new ArrayList<>();
    }
}
