package net.vicemice.game.bedwars.player.teams;

import net.vicemice.game.bedwars.config.game.GameImporter;
import net.vicemice.game.bedwars.game.scoreboard.ScoreboardManager;
import net.vicemice.game.bedwars.game.tasks.GameEndGameListener;
import lombok.Getter;
import lombok.Setter;
import net.vicemice.game.bedwars.player.board.ingame.GameScoreboardManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Player;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TeamManager {
    @Getter
    @Setter
    private static GameTeam defaultTeam;
    @Getter
    private static List<GameTeam> aliveTeams = new ArrayList<>();

    public static GameTeam getPlayersTeam(IUser user) {
        for (String teamName : GameImporter.getTeams().keySet()) {
            GameTeam gameTeam = GameImporter.getTeams().get(teamName);
            if (gameTeam.getPlayers().contains(user)) {
                return gameTeam;
            }
        }
        return defaultTeam;
    }

    public static void removePlayerFromTeams(IUser user) {
        for (String teamName : GameImporter.getTeams().keySet()) {
            GameTeam gameTeam = GameImporter.getTeams().get(teamName);
            if (gameTeam.getPlayers().contains(user)) {
                gameTeam.getPlayers().remove(user);
                if (ScoreboardManager.getScoreboardTeams().containsKey(gameTeam)) {
                    ScoreboardManager.getScoreboardTeams().get(gameTeam).removeEntry(user.getName());
                }
                if (user.isNicked()) {
                    if (ScoreboardManager.getScoreboardTeams().containsKey(gameTeam)) {
                        ScoreboardManager.getScoreboardTeams().get(gameTeam).removeEntry(user.getNickName());
                    }
                }
            }
        }
    }

    public static void playerQuit(IUser user) {
        removePlayerFromTeams(user);
        checkAliveTeams();
    }

    public static void playerDeath(IUser user) {
        GameTeam gameTeam = getPlayersTeam(user);
        if (gameTeam != null) {
            if (!gameTeam.isBedAlive()) {
                gameTeam.getPlayers().remove(user);
                if (gameTeam.getPlayers().size() == 0) {
                    checkAliveTeams();
                }
            }
        }
        GameScoreboardManager.updateAllTeams();
    }

    public static void checkAliveTeams() {
        Iterator<GameTeam> bedwarsTeamIterator = getAliveTeams().iterator();
        while (bedwarsTeamIterator.hasNext()) {
            GameTeam gameTeam = bedwarsTeamIterator.next();
            if (gameTeam.getPlayers().size() == 0) {
                //TODO: MessageUtil.sendMessageToAll(BedWars.getPrefix() + "§7Team " + gameTeam.getColor() + gameTeam.getRightName() + " §7was destroyed!", BedWars.getPrefix() + "§7Team " + gameTeam.getColor() + gameTeam.getRightName() + " §7wurde vernichtet!");
                bedwarsTeamIterator.remove();
            }
        }
        if (aliveTeams.size() == 1) {
            GameEndGameListener.setWinnerTeam(aliveTeams.get(0));
            GameManager.endGame();
        }
    }
}
