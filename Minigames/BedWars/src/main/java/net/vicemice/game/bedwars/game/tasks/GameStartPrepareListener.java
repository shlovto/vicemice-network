package net.vicemice.game.bedwars.game.tasks;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.config.game.GameImporter;
import net.vicemice.game.bedwars.game.scoreboard.ScoreboardManager;
import net.vicemice.game.bedwars.player.board.ingame.GameScoreboardManager;
import net.vicemice.game.bedwars.player.board.lobby.LobbyScoreboardManager;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.spectator.SpectatorManager;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.game.bedwars.player.teams.TeamManager;
import net.vicemice.game.bedwars.util.TeamChestTimer;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.events.GameStartPrepareEvent;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.replay.ReplayDataPacket;
import net.vicemice.hector.packets.types.player.replay.ReplayServerPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/*
 * Class created at 22:27 - 05.04.2020
 * Copyright (C) elrobtossohn
 */
public class GameStartPrepareListener implements Listener {

    @EventHandler
    public void onPrepareStart(GameStartPrepareEvent event) {
        if (BedWars.getGame().getBedWarsMap() == null) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.sendMessage("§cAn error occurred while starting the game");
                player.kickPlayer("lobby");
            }
            Bukkit.getServer().shutdown();
            return;
        }
        GameManager.setStartTime(System.currentTimeMillis());
        GameManager.setGameState(GameState.INGAME);

        TeamChestTimer.start();
        GoAPI.getServerAPI().changeServer(ServerState.INGAME, BedWars.getGame().getBedWarsMap().getDisplayName());
        GoAPI.getServerAPI().setScoreboard(ScoreboardManager.getScoreboard());
        GoAPI.getServerAPI().setKick(false);

        GameIngameListener.setFullIngame(true);
        LobbyScoreboardManager.removeAllBoards();
        for (IUser user : UserManager.getConnectedUsers().values()) {
            GameScoreboardManager.createBoard(user);
            GameManager.getReplayPlayers().add(user.getUniqueId());
            GameManager.getIngamePlayers().add(user.getUniqueId());
            user.playSound(Sound.NOTE_PLING, 40, 1);
            PlayerManager.startGameFor(user, false);
        }

        GameScoreboardManager.updateAllTeams();
        SpectatorManager.updateSpectatorInventory();
        for (String color : GameImporter.getTeams().keySet()) {
            GameTeam gameTeam = GameImporter.getTeams().get(color);
            if (TeamManager.getAliveTeams().contains(gameTeam)) {
                gameTeam.setPlayed(true);
            }
        }

        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.REPLAY_SERVER_PACKET, new ReplayServerPacket(new ReplayDataPacket(GoAPI.getGameIDAPI().getGameUID(), GoAPI.getGameIDAPI().getGameID(), "BedWars", BedWars.getGame().getBedWarsMap().getDisplayName(), true), true)), API.PacketReceiver.SLAVE);
    }
}