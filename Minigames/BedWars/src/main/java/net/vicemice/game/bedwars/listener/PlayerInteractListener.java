package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.player.PlayerData;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.game.bedwars.player.teams.lobby.LobbyTeamManager;
import net.vicemice.game.bedwars.shop.ItemTimeouts;
import net.vicemice.game.bedwars.shop.specialitems.MobilerShop;
import net.vicemice.game.bedwars.shop.specialitems.Warper;
import net.vicemice.game.bedwars.player.spectator.SpectatorManager;
import net.vicemice.game.bedwars.util.map.ForceMapInventory;
import net.vicemice.game.bedwars.util.map.MapVotingInventory;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerInteractListener implements Listener {

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        IUser user = UserManager.getUser(event.getPlayer());
        if (BedWars.getBuildPlayers().contains(user.getBukkitPlayer())) {
            return;
        }
        ItemStack hand = user.getBukkitPlayer().getItemInHand();
        if (GameManager.getGameState() == GameState.LOBBY) {
            if (hand != null) {
                event.setCancelled(true);
                if (hand.getType() == Material.BED) {
                    user.getBukkitPlayer().openInventory(LobbyTeamManager.getSelectInventory());
                } else if (hand.getType() == Material.CHEST) {
                    new ForceMapInventory().create(user, 1).open();
                } else if (hand.getType() == Material.PAPER) {
                    new MapVotingInventory().create(user).open();
                }
            }
        } else if (GameManager.getGameState() == GameState.INGAME) {
            if (SpectatorManager.getSpectators().contains(user)) {
                event.setCancelled(true);
                if (hand != null) {
                    if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
                        if (hand.getType() == Material.COMPASS) {
                            user.getBukkitPlayer().openInventory(SpectatorManager.getSpectatorInventory());
                        } else if (hand.getType() == Material.WATCH) {
                            user.getBukkitPlayer().kickPlayer("lobby");
                        }
                    }
                }
                return;
            }
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                if (event.getClickedBlock().getType() == Material.ANVIL) {
                    event.setCancelled(true);
                    return;
                } else if (event.getClickedBlock().getType() == Material.BED_BLOCK) {
                    event.setCancelled(true);
                    return;
                }
            }
            PlayerData playerData = PlayerManager.getPlayerDatas().get(user);
            if (playerData == null) return;
            if ((event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) && (hand != null && hand.getType() == Material.BLAZE_ROD)) {
                Block under = user.getLocation().subtract(0, 1, 0).getBlock();
                if (under.getType() == Material.AIR) {
                    if (ItemTimeouts.getRettungsPlatFormTimeout().containsKey(user.getBukkitPlayer())) {
                        long time = ItemTimeouts.getRettungsPlatFormTimeout().get(user.getBukkitPlayer());
                        if (time > System.currentTimeMillis()) {
                            long diff = time - System.currentTimeMillis();
                            diff = diff / 1000;
                            if (diff > 0) {
                                user.sendMessage("rescue-platform-reuse");
                                return;
                            } else {
                                ItemTimeouts.getRettungsPlatFormTimeout().remove(user.getBukkitPlayer());
                            }
                        } else {
                            ItemTimeouts.getRettungsPlatFormTimeout().remove(user.getBukkitPlayer());
                        }
                    }
                    ItemTimeouts.getRettungsPlatFormTimeout().put(user.getBukkitPlayer(), System.currentTimeMillis() + 5000);
                    ItemStack remove = user.getBukkitPlayer().getItemInHand().clone();
                    remove.setAmount(1);
                    user.getBukkitPlayer().getInventory().removeItem(remove);
                    int x = user.getLocation().getBlockX();
                    int y = user.getLocation().getBlockY();
                    int z = user.getLocation().getBlockZ();
                    for (int xPoint = x - 1; xPoint <= x + 1; xPoint++) {
                        for (int zPoint = z - 1; zPoint <= z + 1; zPoint++) {
                            Block block = user.getBukkitPlayer().getWorld().getBlockAt(xPoint, y - 3, zPoint);
                            if (block.getType() == Material.AIR) {
                                block.setType(Material.STAINED_GLASS);
                                block.setData(playerData.getGameTeam().getDyeColor().getWoolData());
                                BedWars.getGame().getPlacedBlocks().add(block.getLocation());
                            }
                        }
                    }
                } else {
                    user.sendMessage("use-rescue-platform-fall");
                }
            } else if ((event.getAction() == Action.RIGHT_CLICK_BLOCK) && (hand != null && hand.getType() == Material.ARMOR_STAND)) {
                event.setCancelled(true);
                ItemStack remove = user.getBukkitPlayer().getItemInHand().clone();
                remove.setAmount(1);
                user.getBukkitPlayer().getInventory().removeItem(remove);
                new MobilerShop(event.getClickedBlock().getLocation()).start();
            } else if ((event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) && (hand != null && hand.getType() == Material.SULPHUR)) {
                event.setCancelled(true);
                if (ItemTimeouts.getWarpers().containsKey(user)) {
                    user.sendMessage("baseteleporter-already-running");
                    return;
                }
                ItemStack remove = user.getBukkitPlayer().getItemInHand().clone();
                remove.setAmount(1);
                Warper warper = new Warper(user.getBukkitPlayer(), remove);
                ItemTimeouts.getWarpers().put(user.getBukkitPlayer(), warper);
                user.getBukkitPlayer().getItemInHand().setType(Material.GLOWSTONE_DUST);
                warper.start();
            } else if ((event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) && (hand != null && hand.getType() == Material.COMPASS)) {
                PlayerData trackerData = PlayerManager.getPlayerDatas().get(user);
                IUser finalPlayer = null;
                int distance = 0;
                GameTeam finalTeam = null;
                for (IUser trackedPlayer : PlayerManager.getPlayerDatas().keySet()) {
                    PlayerData trackedData = PlayerManager.getPlayerDatas().get(trackedPlayer);
                    if (trackerData.getGameTeam() != trackedData.getGameTeam() && (finalPlayer == null || user.getLocation().distance(trackedPlayer.getLocation()) < finalPlayer.getLocation().distance(user.getLocation()))) {
                        finalPlayer = trackedPlayer;
                        distance = (int) user.getLocation().distance(trackedPlayer.getLocation());
                        finalTeam = trackedData.getGameTeam();
                    }
                }
                if (finalPlayer != null) {
                    user.getBukkitPlayer().setCompassTarget(finalPlayer.getLocation());
                    //player.sendMessage(BedWars.getPrefix() + "§6Getrackt: " + finalTeam.getColor() + CloudAPI.getNickAPI().getNickName(finalPlayer) /*+ " §aDistanz: §6" + distance + " Blöcke"*/);
                }
            }
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                if (event.getClickedBlock().getType() == Material.ENDER_CHEST) {
                    event.setCancelled(true);
                    if (BedWars.getGame().getTeamChests().containsKey(event.getClickedBlock().getLocation())) {
                        GameTeam gameTeam = BedWars.getGame().getTeamChests().get(event.getClickedBlock().getLocation());
                        if (gameTeam == playerData.getGameTeam()) {
                            user.getBukkitPlayer().openInventory(gameTeam.getTeamInventory());
                        } else {
                            user.sendMessage("not-your-teamchest");
                        }
                    }
                } else if (event.getClickedBlock().getType() == Material.CHEST) {
                    if (!BedWars.getGame().getPlacedBlocks().contains(event.getClickedBlock().getLocation())) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }
}
