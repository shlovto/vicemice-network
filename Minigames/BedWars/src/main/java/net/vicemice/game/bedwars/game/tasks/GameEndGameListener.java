package net.vicemice.game.bedwars.game.tasks;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.player.PlayerData;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.board.ingame.GameScoreboardManager;
import net.vicemice.game.bedwars.player.spectator.SpectatorManager;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.gameapi.events.GameEndGameEvent;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.gameapi.util.Locations;
import net.vicemice.hector.gameapi.util.Title;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.UUID;

/*
 * Class created at 13:16 - 03.04.2020
 * Copyright (C) elrobtossohn
 */
public class GameEndGameListener implements Listener {

    @Getter
    @Setter
    private static GameTeam winnerTeam;

    @EventHandler
    public void onEndGame(GameEndGameEvent event) {
        ArrayList<String> uniqueIds = new ArrayList<>();
        for (UUID uniqueId : GameManager.getReplayPlayers()) {
            uniqueIds.add(String.valueOf(uniqueId));
        }

        GameManager.getReplayPlayers().forEach((o) -> {
            GoAPI.getUserAPI().addRecordedGame(o, GoAPI.getGameIDAPI().getGameID(), GoAPI.getGameIDAPI().getGameUID(), GameManager.getReplayStartSeconds(), GameManager.getStartTime(), GameManager.getLength(), uniqueIds, "BedWars", BedWars.getGameConfig().getTYPE(), BedWars.getGame().getBedWarsMap().getDisplayName(), GameManager.getSaveReplayPlayers().contains(o));
        });
        GameManager.setGameState(GameState.END);

        GoAPI.getServerAPI().setScoreboard(null);
        for (IUser specPlayer : SpectatorManager.getSpectators()) {
            specPlayer.getBukkitPlayer().setAllowFlight(false);
            specPlayer.getBukkitPlayer().setFlying(false);
        }

        GameScoreboardManager.removeAllBoards();
        for (IUser user : UserManager.getConnectedUsers().values()) {
            new Title(user.translate("team-won-title", (winnerTeam.getColor()+winnerTeam.getRightName(user))), user.translate("team-won-subtitle"), 0, 2, 1).send(user.getBukkitPlayer());

            for (IUser specPlayer : SpectatorManager.getSpectators()) {
                user.getBukkitPlayer().showPlayer(specPlayer.getBukkitPlayer());
            }

            PlayerData playerData = PlayerManager.getPlayerDatas().get(user);
            if (playerData != null) {
                if (playerData.getGameTeam() == winnerTeam) {
                    playerData.addWin();
                }
                playerData.saveStats();
            }
            user.sendMessage("team-won", user.translate("bw-prefix"), winnerTeam.getColor()+winnerTeam.getRightName(user));

            GoServer.getLocaleManager().getLocales().forEach((k, v) -> {
                GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "team-won", GoServer.getLocaleManager().getPrivateMessage(k, "bw-prefix"), winnerTeam.getColor()+winnerTeam.getRightName(k)), k);
            });

            PlayerManager.resetPlayer(user);
            user.teleport(Locations.getLobbyLocation());
        }

        GoAPI.getServerAPI().changeServer(ServerState.NO_STATE, "Restarting Server");
    }
}