package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.player.PlayerData;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.teams.TeamManager;
import net.vicemice.game.bedwars.player.spectator.SpectatorManager;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class EntityDamageByEntityListener implements Listener {
    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (GameManager.getGameState() != GameState.INGAME) {
            event.setCancelled(true);
            return;
        }
        if (event.getEntity() instanceof Player) {
            IUser user = UserManager.getUser((Player) event.getEntity());
            if (SpectatorManager.getSpectators().contains(user)) {
                event.setCancelled(true);
                return;
            }
            PlayerData playerData = PlayerManager.getPlayerDatas().get(user);
            if (playerData == null) {
                event.setCancelled(true);
                return;
            }
            if (event.getDamager() instanceof Player) {
                IUser damager = UserManager.getUser((Player) event.getDamager());
                if (BedWars.getBuildPlayers().contains(damager.getBukkitPlayer())) return;
                if (SpectatorManager.getSpectators().contains(damager)) {
                    event.setCancelled(true);
                    return;
                }
                if (TeamManager.getPlayersTeam(user) == TeamManager.getPlayersTeam(damager)) {
                    event.setCancelled(true);
                    return;
                }
                playerData.receiveDamageFrom(damager);
            } else if (event.getDamager() instanceof Arrow) {
                Arrow arrow = (Arrow) event.getDamager();
                if (arrow.getShooter() instanceof Player) {
                    IUser damager = UserManager.getUser((Player) arrow.getShooter());
                    if (BedWars.getBuildPlayers().contains(damager.getBukkitPlayer())) return;
                    if (SpectatorManager.getSpectators().contains(damager)) {
                        event.setCancelled(true);
                        return;
                    }
                    if (TeamManager.getPlayersTeam(user) == TeamManager.getPlayersTeam(damager)) {
                        event.setCancelled(true);
                        return;
                    }
                    playerData.receiveDamageFrom(damager);
                }
            }
        }
    }
}
