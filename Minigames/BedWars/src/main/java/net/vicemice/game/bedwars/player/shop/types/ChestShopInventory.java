package net.vicemice.game.bedwars.player.shop.types;

import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.shop.DefaultShopInventory;
import net.vicemice.game.bedwars.player.shop.NeedResourceType;
import net.vicemice.game.bedwars.player.shop.ShopManager;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/*
 * Class created at 13:56 - 06.04.2020
 * Copyright (C) elrobtossohn
 */
public class ChestShopInventory {

    public GUI create(IUser user) {
        GUI gui = new DefaultShopInventory().create(user, DefaultShopInventory.ShopType.CHESTS);

        String bronze = user.translate("bronze");
        String iron = user.translate("iron");
        String gold = user.translate("gold");
        GameTeam gameTeam = PlayerManager.getPlayerDatas().get(user).getGameTeam();

        gui.setItem(28, ItemBuilder.create(Material.CHEST).amount(1).name("&e"+user.translate("shop-chest-chest")).lore(user.translate("price", "&71 "+iron)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.IRON, 1, ItemBuilder.create(Material.CHEST).amount(1).build());
        });

        gui.setItem(29, ItemBuilder.create(Material.ENDER_CHEST).amount(1).name("&e"+user.translate("shop-chest-team-chest")).lore(user.translate("price", "&61 "+gold)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.GOLD, 1, ItemBuilder.create(Material.ENDER_CHEST).amount(1).build());
        });

        return gui;
    }
}