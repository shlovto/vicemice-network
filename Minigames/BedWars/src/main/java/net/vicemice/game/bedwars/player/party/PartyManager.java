package net.vicemice.game.bedwars.player.party;

import net.vicemice.game.bedwars.config.game.GameImporter;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.game.bedwars.BedWars;
import lombok.Getter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.types.player.party.PartyMemberPacket;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class PartyManager {
    @Getter
    private static HashMap<UUID, PartyMemberPacket> partyMembers = new HashMap<>();

    public static void receivePartyPacket(PartyMemberPacket partyMemberPacket) {
        Bukkit.getServer().getScheduler().runTask(BedWars.getInstance(), () -> {
            List<IUser> onlinePlayers = new ArrayList<>();
            for (UUID member : partyMemberPacket.getMembers()) {
                if (UserManager.isConnected(member)) {
                    IUser player = UserManager.getUser(member);
                    onlinePlayers.add(player);
                }
                partyMembers.put(member, partyMemberPacket);
            }
            if (onlinePlayers.size() == partyMemberPacket.getMembers().size()) {
                allPlayersOnline(onlinePlayers);
            }
        });
    }

    public static void allPlayersOnline(List<IUser> onlinePartyPlayers) {
        GameTeam gameTeam = null;
        for (String color : GameImporter.getTeams().keySet()) {
            GameTeam getTeam = GameImporter.getTeams().get(color);
            int space = BedWars.getGameConfig().getPER_TEAM() - getTeam.getPlayers().size();
            if (space >= onlinePartyPlayers.size()) {
                gameTeam = getTeam;
            }
        }
        for (IUser user : onlinePartyPlayers) {
            partyMembers.remove(user.getUniqueId());
            if (gameTeam != null) {
                gameTeam.join(user, false);
            } else {
                user.sendMessage("party-no-team-found");
            }
        }
    }
}
