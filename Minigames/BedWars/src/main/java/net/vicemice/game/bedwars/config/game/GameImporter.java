package net.vicemice.game.bedwars.config.game;

import lombok.Getter;
import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.config.map.MapData;
import net.vicemice.game.bedwars.config.map.vote.VoteMap;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.game.bedwars.util.ColorManager;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.MongoManager;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.*;

/*
 * Class created at 22:20 - 05.04.2020
 * Copyright (C) elrobtossohn
 */
public class GameImporter {

    @Getter
    private static Map<Integer, VoteMap> forceMaps = new HashMap<>();
    @Getter
    private static HashMap<String, GameTeam> teams = new HashMap<>();

    public static void importConfiguration() {
        Document configuration = GoServer.getService().getMongoManager().getDocument(MongoManager.Database.GAMES, "configuration", new Document("game", "BedWars").append("type", BedWars.getGameConfig().getTYPE()));

        List<String> teamList = configuration.getList("teamList", String.class);

        for (String team : teamList) {
            teams.put(team.toLowerCase(), new GameTeam(team, ColorManager.getRightName(team), ChatColor.valueOf(team.toUpperCase())));
        }
    }

    public static void importMaps() {
        List<Document> maps = GoServer.getService().getMongoManager().getDocumentsWithFilter(MongoManager.Database.GAMES, "maps", new Document("game", "BedWars").append("type", BedWars.getGameConfig().getTYPE()));

        for (Document map : maps) {
            String name = map.getString("name");
            String creator = map.getString("creator");
            List<String> bronzeSpawner = map.getList("bronzeSpawner", String.class);
            List<String> ironSpawner = map.getList("ironSpawner", String.class);
            List<String> goldSpawner = map.getList("goldSpawner", String.class);
            List<String> spawns = map.getList("spawns", String.class);
            List<String> beds = map.getList("beds", String.class);
            List<String> villager = map.getList("villager", String.class);

            for (String string : villager) {
                Bukkit.getLogger().warning(string);
            }

            BedWars.getMapManager().getMapDatas().put(name.toLowerCase(), new MapData(name.toLowerCase(), name, creator, name.toLowerCase(), "GRASS;0", spawns, bronzeSpawner, ironSpawner, goldSpawner, beds, villager));
        }
    }

    public static void selectRandomMap() {
        int random = Math.abs(new Random().nextInt()) % BedWars.getMapManager().getMapDatas().size();
        MapData mapData = new ArrayList<>(BedWars.getMapManager().getMapDatas().values()).get(random);
        BedWars.getGame().setBedWarsMap(mapData.getCreatedMap());
    }
}