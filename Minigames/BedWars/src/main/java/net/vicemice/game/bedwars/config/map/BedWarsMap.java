package net.vicemice.game.bedwars.config.map;

import lombok.Data;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import org.bukkit.Location;
import org.bukkit.World;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/*
 * Class created at 16:19 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class BedWarsMap {

    private World world;
    private String name, displayName, creator;
    private List<Location> bronzeSpawner = new ArrayList<>(), ironSpawner = new ArrayList<>(), goldSpawner = new ArrayList<>(), villager = new ArrayList<>();
    private HashMap<GameTeam, Location> spawns = new HashMap<>();
    private HashMap<GameTeam, List<Location>> beds = new HashMap<>();
    private HashMap<Location, GameTeam> locationBeds = new HashMap<>();

    public BedWarsMap(World world, String name, String displayName, String creator) {
        this.world = world;
        this.name = name;
        this.displayName = displayName;
        this.creator = creator;
    }

    public void saveBeds() {
        for (GameTeam gameTeam : beds.keySet()) {
            for (Location location : beds.get(gameTeam)) {
                locationBeds.put(location, gameTeam);
            }
        }
    }

    public GameTeam getTeamByBed(Location location) {
        for (GameTeam gameTeam : beds.keySet()) {
            AtomicReference<GameTeam> returnAble = new AtomicReference<>(null);
            beds.get(gameTeam).forEach(e -> {
                if (e.distance(location) < 3) {
                    returnAble.set(gameTeam);
                }
            });

            if (returnAble.get() != null) return returnAble.get();
        }

        return null;
    }
}