package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.player.PlayerData;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.shop.types.BlockShopInventory;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;

/**
 * Created by finn on 16.06.17.
 */
public class PlayerInteractAtEntityListener implements Listener {

    @EventHandler
    public void onPlayerInteractAtEntity(PlayerInteractAtEntityEvent event) {
        IUser user = UserManager.getUser(event.getPlayer());

        if (event.getRightClicked().getType() == EntityType.VILLAGER || event.getRightClicked().getType() == EntityType.ARMOR_STAND) {
            event.setCancelled(true);

            PlayerData playerData = PlayerManager.getPlayerDatas().get(user);
            if (playerData == null) return;
            new BlockShopInventory().create(user).open();
        }
    }
}
