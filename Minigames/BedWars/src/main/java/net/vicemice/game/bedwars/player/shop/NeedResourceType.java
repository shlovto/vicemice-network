package net.vicemice.game.bedwars.player.shop;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/*
 * Enum created at 10:15 - 06.04.2020
 * Copyright (C) elrobtossohn
 */
public enum NeedResourceType {
    BRONZE(Material.CLAY_BRICK),
    IRON(Material.IRON_INGOT),
    GOLD(Material.GOLD_INGOT);

    NeedResourceType(Material material) {
        this.material = material;
    }

    private Material material;

    public ItemStack getMaterial() {
        return new ItemStack(this.material);
    }
}