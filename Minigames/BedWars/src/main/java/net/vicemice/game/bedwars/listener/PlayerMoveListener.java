package net.vicemice.game.bedwars.listener;

import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.gameapi.util.Locations;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/*
 * Class created at 12:25 - 06.04.2020
 * Copyright (C) elrobtossohn
 */
public class PlayerMoveListener implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        if (GameManager.getGameState() == GameState.LOBBY) {
            if (event.getTo().getY() < 90)
                event.getPlayer().teleport(Locations.getLobbyLocation());
        }
    }
}