package net.vicemice.game.bedwars.player.stats;

public enum Points {
    KILL(5), BED(10), WIN(15);
    public int points;

    Points(int points) {
        this.points = points;
    }

    public int getPoints() {
        return points;
    }
}
