package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.player.PlayerData;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.spectator.SpectatorManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlaceListener implements Listener {

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlock();
        if (BedWars.getBuildPlayers().contains(player)) {
            BedWars.getGame().getPlacedBlocks().add(event.getBlock().getLocation());
            return;
        }

        if (GameManager.getGameState() != GameState.INGAME) {
            event.setCancelled(true);
            return;
        }
        if (SpectatorManager.getSpectators().contains(player)) {
            event.setCancelled(true);
            return;
        }
        PlayerData playerData = PlayerManager.getPlayerDatas().get(player);
        if (playerData == null) {
            event.setCancelled(true);
            return;
        }

        Location blockLocation = block.getLocation();
        for (Location location : BedWars.getGame().getSpawnLocations()) {
            if (location.getBlockX() == blockLocation.getBlockX() && location.getBlockZ() == blockLocation.getBlockZ()) {
                if (blockLocation.getBlockY() > location.getBlockY()) {
                    int diff = blockLocation.getBlockY() - location.getBlockY();
                    if (diff <= 4) {
                        event.setCancelled(true);
                        player.sendMessage(GoAPI.getUserAPI().translate(player.getUniqueId(), "build-at-spawnpoints"));
                        return;
                    }
                }
            }
        }

        BedWars.getGame().getPlacedBlocks().add(event.getBlock().getLocation());
        if (event.getBlock().getType() == Material.ENDER_CHEST) {
            BedWars.getGame().getTeamChests().put(event.getBlock().getLocation(), playerData.getGameTeam());
        }
    }
}
