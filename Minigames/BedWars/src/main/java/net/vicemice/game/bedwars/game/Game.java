package net.vicemice.game.bedwars.game;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.game.bedwars.config.map.BedWarsMap;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.game.bedwars.shop.GameType;
import org.bukkit.Location;
import org.bukkit.scoreboard.Team;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Class created at 16:15 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class Game {

    @Getter
    @Setter
    private BedWarsMap bedWarsMap;
    @Getter
    @Setter
    private GameType gameType = GameType.CLASSIC;
    @Getter
    private Map<GameTeam, Team> teams = new HashMap<>();
    @Getter
    private List<Location> spawnLocations = new ArrayList<>(), placedBlocks = new ArrayList<>();
    @Getter
    private Map<Location, GameTeam> teamChests = new HashMap<>();
}