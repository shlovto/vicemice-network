package net.vicemice.game.bedwars.player.stats;

import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Getter;

@Getter
public enum Coins {
    KILL(3), BED(10), WIN(15);
    public int coins;

    Coins(int coins) {
        String day = new SimpleDateFormat("dd.MM").format(new Date());
        if (day.equalsIgnoreCase("04.12") || day.equalsIgnoreCase("09.12") || day.equalsIgnoreCase("30.11") || day.equalsIgnoreCase("21.06")) {
            this.coins = coins * 2;
        } else {
            this.coins = coins;
        }
    }
}
