package net.vicemice.game.bedwars.player.spectator;

import com.mojang.authlib.GameProfile;
import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.player.PlayerData;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.util.ItemStackHelper;
import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.internal.SkullChanger;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Team;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SpectatorManager {
    @Getter
    @Setter
    private static Team spectatorTeam;
    @Getter
    private static List<IUser> spectators = new ArrayList<>();
    @Getter
    private static Inventory spectatorInventory;

    public static void updateSpectatorInventory() {
        int size = 0;
        if (PlayerManager.getPlayerDatas().size() <= 9) {
            size = 9;
        } else if (PlayerManager.getPlayerDatas().size() <= 18) {
            size = 18;
        } else if (PlayerManager.getPlayerDatas().size() <= 27) {
            size = 27;
        } else if (PlayerManager.getPlayerDatas().size() <= 36) {
            size = 36;
        } else {
            size = 54;
        }
        if (spectatorInventory == null || size != spectatorInventory.getSize()) {
            Inventory newInventory = Bukkit.getServer().createInventory(null, size, "§6Teleporter");
            spectatorInventory = newInventory;
        }
        int currentSlot = 0;
        HashMap<Integer, ItemStack> addedItems = new HashMap<>();
        for (IUser user : PlayerManager.getPlayerDatas().keySet()) {
            if (currentSlot > 53) {
                break;
            }
            PlayerData playerData = PlayerManager.getPlayerDatas().get(user);
            CraftPlayer entityPlayer = (CraftPlayer) user;
            GameProfile gameProfile = entityPlayer.getProfile();
            ItemStack skull = null;
            if (gameProfile.getProperties().containsKey("textures")) {
                skull = SkullChanger.getSkull(user.getBukkitPlayer());
            } else {
                skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
            }
            ItemMeta skullMeta = skull.getItemMeta();
            skullMeta.setDisplayName(playerData.getGameTeam().getColor() + user.getName());
            skull.setItemMeta(skullMeta);
            spectatorInventory.setItem(currentSlot, skull);
            addedItems.put(currentSlot, skull);
            currentSlot++;
        }
        int currentCheckSlot = 0;
        for (ItemStack content : spectatorInventory.getContents()) {
            if (addedItems.containsKey(currentCheckSlot)) {
                if (content == null || addedItems.get(currentCheckSlot).equals(content)) {
                    spectatorInventory.setItem(currentCheckSlot, addedItems.get(currentCheckSlot));
                }
            } else {
                spectatorInventory.setItem(currentCheckSlot, null);
            }
            currentCheckSlot++;
        }
    }

    public static void spectatorPlayer(IUser user) {
        PlayerManager.resetPlayer(user);
        PlayerManager.getPlayerDatas().remove(user);
        GameManager.getSpectatorPlayers().add(user.getUniqueId());
        user.getBukkitPlayer().setAllowFlight(true);
        user.getBukkitPlayer().setFlying(true);
        user.getBukkitPlayer().spigot().setCollidesWithEntities(false);
        user.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));

        user.teleport(BedWars.getGame().getBedWarsMap().getWorld().getSpawnLocation());

        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(BedWars.getInstance(), () -> {
            if (!user.isOnline()) return;
            user.getBukkitPlayer().getInventory().setItem(4, SpectateItems.COMPASS.get());
        }, 10L);

        if (!spectatorTeam.hasEntry(user.getName())) {
            spectatorTeam.addEntry(user.getName());
        }
        if (user.isNicked()) {
            if (!spectatorTeam.hasEntry(user.getNickName())) {
                spectatorTeam.addEntry(user.getNickName());
            }
        }
        for (IUser onlinePlayer : UserManager.getConnectedUsers().values()) {
            if (onlinePlayer != user) {
                if (!spectators.contains(onlinePlayer)) {
                    onlinePlayer.getBukkitPlayer().hidePlayer(user.getBukkitPlayer());
                }
            }
        }
        for (IUser spectatorPlayer : spectators) {
            user.getBukkitPlayer().showPlayer(spectatorPlayer.getBukkitPlayer());
        }
        if (!user.getBukkitPlayer().getWorld().getName().equalsIgnoreCase(BedWars.getGame().getBedWarsMap().getWorld().getName())) {
            user.teleport(BedWars.getGame().getBedWarsMap().getWorld().getSpawnLocation());
        }
        user.sendMessage("game-spectator", user.translate("bw-prefix"));
        if (!spectators.contains(user)) {
            spectators.add(user);
        }
    }

    public enum SpectateItems {
        COMPASS(ItemStackHelper.createItemStack(Material.COMPASS, 0, "§6Teleporter"));
        ItemStack itemStack;

        SpectateItems(ItemStack itemStack) {
            this.itemStack = itemStack;
        }

        public ItemStack get() {
            return itemStack;
        }
    }
}
