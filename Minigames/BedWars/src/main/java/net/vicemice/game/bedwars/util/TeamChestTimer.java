package net.vicemice.game.bedwars.util;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class TeamChestTimer {
    public static void start() {
        Bukkit.getServer().getScheduler().runTaskTimer(BedWars.getInstance(), () -> {
            for (Location location : BedWars.getGame().getTeamChests().keySet()) {
                GameTeam gameTeam = BedWars.getGame().getTeamChests().get(location);
                location = location.clone().add(0.5, 0.2, 0.5);
                int i = 10;
                while (i != 0) {
                    i--;
                    ParticleEffect.SPELL_MOB.display(gameTeam.getOrdinaryColor(), location, 30);
                }
            }
        }, 10L, 10L);
    }
}
