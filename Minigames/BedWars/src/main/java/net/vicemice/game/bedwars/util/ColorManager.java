package net.vicemice.game.bedwars.util;

import net.vicemice.game.bedwars.player.teams.GameTeam;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;

public class ColorManager {
    public static String getRightName(String color) {
        switch (color.toLowerCase()) {
            case "RED":
                return "red";
            case "BLUE":
                return "blue";
            case "YELLOW":
                return "yellow";
            case "GREEN":
                return "green";
            case "LIGHT_PURPLE":
                return "pink";
            case "AQUA":
                return "cyan";
            case "GOLD":
                return "orange";
            case "DARK_PURPLE":
                return "purple";
        }
        return color;
    }

    public static DyeColor getDyeColor(GameTeam gameTeam) {
        if (gameTeam.getColor() == ChatColor.RED) {
            return DyeColor.RED;
        } else if (gameTeam.getColor() == ChatColor.BLUE) {
            return DyeColor.BLUE;
        } else if (gameTeam.getColor() == ChatColor.YELLOW) {
            return DyeColor.YELLOW;
        } else if (gameTeam.getColor() == ChatColor.GREEN) {
            return DyeColor.GREEN;
        } else if (gameTeam.getColor() == ChatColor.LIGHT_PURPLE) {
            return DyeColor.PINK;
        } else if (gameTeam.getColor() == ChatColor.AQUA) {
            return DyeColor.LIGHT_BLUE;
        } else if (gameTeam.getColor() == ChatColor.GOLD) {
            return DyeColor.ORANGE;
        } else if (gameTeam.getColor() == ChatColor.DARK_PURPLE) {
            return DyeColor.PURPLE;
        }
        return DyeColor.WHITE;
    }
}
