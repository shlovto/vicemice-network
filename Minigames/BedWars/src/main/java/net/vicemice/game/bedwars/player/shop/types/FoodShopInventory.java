package net.vicemice.game.bedwars.player.shop.types;

import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.shop.DefaultShopInventory;
import net.vicemice.game.bedwars.player.shop.NeedResourceType;
import net.vicemice.game.bedwars.player.shop.ShopManager;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/*
 * Class created at 13:38 - 06.04.2020
 * Copyright (C) elrobtossohn
 */
public class FoodShopInventory {

    public GUI create(IUser user) {
        GUI gui = new DefaultShopInventory().create(user, DefaultShopInventory.ShopType.FOOD);

        String bronze = user.translate("bronze");
        String iron = user.translate("iron");
        String gold = user.translate("gold");
        GameTeam gameTeam = PlayerManager.getPlayerDatas().get(user).getGameTeam();

        gui.setItem(28, ItemBuilder.create(Material.APPLE).amount(4).name("&74&8x &e"+user.translate("shop-food-apple")).lore(user.translate("price", "&c2 "+bronze)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 2, ItemBuilder.create(Material.APPLE).amount(4).build());
        });

        gui.setItem(29, ItemBuilder.create(Material.BREAD).amount(4).name("&74&8x &e"+user.translate("shop-food-bread")).lore(user.translate("price", "&c8 "+bronze)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 8, ItemBuilder.create(Material.BREAD).amount(4).build());
        });

        gui.setItem(30, ItemBuilder.create(Material.GRILLED_PORK).amount(4).name("&74&8x &e"+user.translate("shop-food-meat")).lore(user.translate("price", "&c12 "+bronze)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 12, ItemBuilder.create(Material.GRILLED_PORK).amount(4).build());
        });

        gui.setItem(31, ItemBuilder.create(Material.COOKED_MUTTON).amount(4).name("&74&8x &e"+user.translate("shop-food-spike")).lore(user.translate("price", "&c12 "+bronze)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 12, ItemBuilder.create(Material.COOKED_MUTTON).amount(4).build());
        });

        gui.setItem(32, ItemBuilder.create(Material.CAKE).amount(1).name("&71&8x &e"+user.translate("shop-food-cake")).lore(user.translate("price", "&71 "+iron)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.IRON, 1, ItemBuilder.create(Material.CAKE).amount(1).build());
        });

        gui.setItem(33, ItemBuilder.create(Material.RABBIT_STEW).amount(1).name("&71&8x &e"+user.translate("shop-food-kebab")).lore(user.translate("price", "&71 "+iron)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.IRON, 1, ItemBuilder.create(Material.RABBIT_STEW).amount(1).build());
        });

        return gui;
    }
}
