package net.vicemice.game.bedwars.game.tasks;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.gameapi.events.GameEndEvent;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/*
 * Class created at 13:14 - 03.04.2020
 * Copyright (C) elrobtossohn
 */
public class GameEndListener implements Listener {

    @EventHandler
    public void onEnd(GameEndEvent event) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.setLevel(GameManager.getRestartCount());
            player.setExp(GameManager.getRestartCount() * 0.0666666666666667F);
        }
        if (GameManager.getRestartCount() == 0) {
            Bukkit.getOnlinePlayers().forEach(players -> players.kickPlayer("lobby"));
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Bukkit.shutdown();
        } else {
            if (GameManager.getRestartCount() == 15 || GameManager.getRestartCount() == 10 || GameManager.getRestartCount() < 5) {
                if (GameManager.getRestartCount() == 1) {
                    UserManager.getConnectedUsers().values().forEach((o) -> o.sendMessage(o.translate("game-restart-last-msg",o.translate("bw-prefix"))));
                    GoServer.getLocaleManager().getLocales().forEach((k, v) -> GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "game-restart-last-msg", GoServer.getLocaleManager().getPrivateMessage(k, "bw-prefix")), k));
                } else {
                    UserManager.getConnectedUsers().values().forEach((o) -> o.sendMessage(o.translate("game-restart-msg", o.translate("bw-prefix"), GameManager.getRestartCount())));
                    GoServer.getLocaleManager().getLocales().forEach((k, v) -> GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "game-restart-msg", GoServer.getLocaleManager().getPrivateMessage(k, "bw-prefix"), GameManager.getRestartCount()), k));
                }
            }
            GameManager.setRestartCount(GameManager.getRestartCount() - 1);
        }
    }
}