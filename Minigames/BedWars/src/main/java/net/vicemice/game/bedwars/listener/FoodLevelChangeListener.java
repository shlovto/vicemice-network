package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.player.spectator.SpectatorManager;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class FoodLevelChangeListener implements Listener {
    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event) {
        if (event.getEntity() instanceof Player) {
            IUser user = UserManager.getUser((Player) event.getEntity());
            if (GameManager.getGameState() != GameState.INGAME) {
                event.setCancelled(true);
                return;
            }
            if (SpectatorManager.getSpectators().contains(user)) {
                event.setCancelled(true);
            }
        }
    }
}
