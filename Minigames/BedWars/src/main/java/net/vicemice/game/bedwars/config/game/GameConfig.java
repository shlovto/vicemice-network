package net.vicemice.game.bedwars.config.game;

import lombok.Data;

/*
 * Class created at 13:21 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class GameConfig {
    private String TYPE = "12x1";
    private int TEAMS = 12;
    private int PER_TEAM = 1;
    private int TO_START = 4;
    private boolean VOTING = true, RANKED = false;
}