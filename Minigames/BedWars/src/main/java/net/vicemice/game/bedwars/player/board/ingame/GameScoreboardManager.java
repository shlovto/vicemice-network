package net.vicemice.game.bedwars.player.board.ingame;

import lombok.Getter;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class GameScoreboardManager {
    @Getter
    private static HashMap<IUser, GameScoreboard> scoreboards = new HashMap<>();

    public static void createBoard(IUser user) {
        if (scoreboards.containsKey(user)) return;
        scoreboards.put(user, new GameScoreboard(user));
    }

    public static void removeBoard(IUser user) {
        scoreboards.remove(user);
    }

    public static void updateAllTeams() {
        for (IUser user : scoreboards.keySet()) {
            scoreboards.get(user).setTeams();
        }
    }

    public static void removeAllBoards() {
        scoreboards.clear();
    }
}