package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.player.LobbyItems;
import net.vicemice.game.bedwars.player.PlayerData;
import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.hector.gameapi.util.Locations;
import net.vicemice.hector.server.player.UserManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerRespawnListener implements Listener {

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        PlayerData playerData = PlayerManager.getPlayerDatas().get(UserManager.getUser(event.getPlayer()));
        if (playerData == null) {
            event.setRespawnLocation(Locations.getLobbyLocation());
        } else {
            event.setRespawnLocation(BedWars.getGame().getBedWarsMap().getSpawns().get(playerData.getGameTeam()));
            if (BedWars.isEventMode()) {
                event.getPlayer().getInventory().addItem(LobbyItems.TRACKER.get());
            }
        }
    }
}
