package net.vicemice.game.bedwars.listener;

import net.vicemice.game.bedwars.util.DeathManager;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.player.UserManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerDeathListener implements Listener {
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent event) {
		event.setDeathMessage("");
		if (GameManager.getGameState() != GameState.INGAME) return;
		DeathManager.death(UserManager.getUser(event.getEntity()));
	}
}
