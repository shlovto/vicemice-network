package net.vicemice.game.bedwars.player.shop.types;

import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.shop.DefaultShopInventory;
import net.vicemice.game.bedwars.player.shop.NeedResourceType;
import net.vicemice.game.bedwars.player.shop.ShopManager;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/*
 * Class created at 13:58 - 06.04.2020
 * Copyright (C) elrobtossohn
 */
public class SpecialShopInventory {

    public GUI create(IUser user) {
        GUI gui = new DefaultShopInventory().create(user, DefaultShopInventory.ShopType.SPECIAL);

        String bronze = user.translate("bronze");
        String iron = user.translate("iron");
        String gold = user.translate("gold");
        GameTeam gameTeam = PlayerManager.getPlayerDatas().get(user).getGameTeam();

        gui.setItem(28, ItemBuilder.create(Material.WEB).amount(1).name("&71&8x &e"+user.translate("shop-special-cobweb")).lore(user.translate("price", "&c8 "+bronze)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 8, ItemBuilder.create(Material.WEB).amount(1).build());
        });

        gui.setItem(29, ItemBuilder.create(Material.FLINT_AND_STEEL).amount(1).name("&e"+user.translate("shop-special-lighter")).lore(user.translate("price", "&71 "+iron)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.IRON, 1, ItemBuilder.create(Material.FLINT_AND_STEEL).amount(1).build());
        });

        gui.setItem(30, ItemBuilder.create(Material.POTION).durability(16453).amount(1).name("&e"+user.translate("shop-special-heal-potion")).lore(user.translate("price", "&71 "+iron)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.IRON, 1, ItemBuilder.create(Material.POTION).durability(16453).amount(1).build());
        });

        gui.setItem(31, ItemBuilder.create(Material.POTION).durability(16386).amount(1).name("&e"+user.translate("shop-special-speed-potion")).lore(user.translate("price", "&71 "+iron)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.IRON, 1, ItemBuilder.create(Material.POTION).durability(16386).amount(1).build());
        });

        gui.setItem(32, ItemBuilder.create(Material.POTION).durability(16395).amount(1).name("&e"+user.translate("shop-special-jump-potion")).lore(user.translate("price", "&71 "+iron)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.IRON, 1, ItemBuilder.create(Material.POTION).durability(16395).amount(1).build());
        });

        gui.setItem(33, ItemBuilder.create(Material.TNT).amount(1).name("&e"+user.translate("shop-special-tnt")).lore(user.translate("price", "&61 "+gold)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.GOLD, 1, ItemBuilder.create(Material.TNT).amount(1).build());
        });

        gui.setItem(34, ItemBuilder.create(Material.ENDER_PEARL).amount(1).name("&e"+user.translate("shop-special-ender-pearl")).lore(user.translate("price", "&610 "+gold)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.GOLD, 10, ItemBuilder.create(Material.ENDER_PEARL).amount(1).build());
        });

        return gui;
    }
}
