package net.vicemice.game.bedwars.commands;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.hector.utils.Rank;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_Start implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
		Player player = (Player) commandSender;
		PlayerRankData playerRankData = GoAPI.getUserAPI().getRankData(player.getUniqueId());
		if (playerRankData.getAccess_level() < Rank.VIP.getAccessLevel()) {
			player.sendMessage(GoAPI.getUserAPI().translate(player.getUniqueId(), "command-no-permission"));
			return true;
		}

		if (GameManager.getGameState() == GameState.LOBBY) {
			if (Bukkit.getOnlinePlayers().size() >= BedWars.getGameConfig().getTO_START()) {
				if (GameManager.getLobbyCount() > 15) {
					GameManager.setLobbyCount(15);
					player.sendMessage(GoAPI.getUserAPI().translate(player.getUniqueId(), "command-start-success", GoAPI.getUserAPI().translate(player.getUniqueId(), "bw-prefix")));
				} else {
					player.sendMessage(GoAPI.getUserAPI().translate(player.getUniqueId(), "command-start-already-started", GoAPI.getUserAPI().translate(player.getUniqueId(), "bw-prefix")));
				}
			} else {
				player.sendMessage(GoAPI.getUserAPI().translate(player.getUniqueId(), "command-start-at-least", GoAPI.getUserAPI().translate(player.getUniqueId(), "bw-prefix"), BedWars.getGameConfig().getTO_START()));
			}
		} else {
			player.sendMessage(GoAPI.getUserAPI().translate(player.getUniqueId(), "command-start-not-in-lobby-phase", GoAPI.getUserAPI().translate(player.getUniqueId(), "bw-prefix")));
		}

		return true;
	}
}