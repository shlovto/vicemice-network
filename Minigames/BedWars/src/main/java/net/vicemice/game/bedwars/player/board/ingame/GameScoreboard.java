package net.vicemice.game.bedwars.player.board.ingame;

import lombok.Getter;
import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.config.game.GameImporter;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.player.scoreboard.PacketScoreboard;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class GameScoreboard {
    @Getter
    private IUser user;
    @Getter
    private PacketScoreboard packetScoreboard;
    @Getter
    private HashMap<String, Integer> teams;

    public GameScoreboard(IUser user) {
        this.user = user;
        this.packetScoreboard = new PacketScoreboard(user.getBukkitPlayer());
        this.teams = new HashMap<>();
        packetScoreboard.remove();
        packetScoreboard.sendSidebar((BedWars.getGameConfig().isRANKED() ? "§c§lBW §f§lRanked" : "§c§lBW §f§lUnranked"));
        packetScoreboard.setLine(65, "§3");
        for (String color : GameImporter.getTeams().keySet()) {
            GameTeam gameTeam = GameImporter.getTeams().get(color);
            if (this.teams.containsKey(gameTeam.getRightName())) continue;
            this.teams.put(gameTeam.getRightName(), gameTeam.getPlayers().size());
            packetScoreboard.setLine(gameTeam.getPlayers().size(), (gameTeam.isBedAlive() && gameTeam.getPlayers().size() != 0 ? "§a❤ " : "§7❤ ")+gameTeam.getColor()+gameTeam.getRightName(user));
        }
        packetScoreboard.setLine(-1, "§1");
        packetScoreboard.setLine(-2, "§7"+GoAPI.getTimeManager().getTime("dd/MM/yyyy", System.currentTimeMillis()));
    }

    public void setTeams() {
        for (int i = 0; i < 65; i++)
            packetScoreboard.removeLine(i);
        this.teams.clear();
        for (String color : GameImporter.getTeams().keySet()) {
            GameTeam gameTeam = GameImporter.getTeams().get(color);
            if (this.teams.containsKey(gameTeam.getRightName())) continue;
            this.teams.put(gameTeam.getRightName(), gameTeam.getPlayers().size());
            packetScoreboard.setLine(gameTeam.getPlayers().size(), (gameTeam.isBedAlive() && gameTeam.getPlayers().size() != 0 ? "§a❤ " : "§7❤ ")+gameTeam.getColor()+gameTeam.getRightName(user));
        }
    }
}
