package net.vicemice.game.bedwars.player.shop;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/*
 * Class created at 10:23 - 06.04.2020
 * Copyright (C) elrobtossohn
 */
public class ShopManager {

    public String getLanguage(Player p){
        try {
            Object ep = getMethod("getHandle", p.getClass()).invoke(p, (Object[]) null);
            Field f = null;
            f = ep.getClass().getDeclaredField("locale");
            f.setAccessible(true);
            String language = (String) f.get(ep);
            return language;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "en-US";
    }

    private Method getMethod(String name, Class<?> clazz) {
        for (Method m : clazz.getDeclaredMethods()) {
            if (m.getName().equals(name))
                return m;
        }
        return null;
    }

    public static void validBuy(IUser user, InventoryAction inventoryAction, NeedResourceType needResourceType, int amount, ItemStack shopItem) {
        ItemStack needResource = new ItemStack(needResourceType.getMaterial().getType(), amount);
        if (user.getBukkitPlayer().getInventory().containsAtLeast(needResource, needResource.getAmount())) {
            if (inventoryAction == InventoryAction.MOVE_TO_OTHER_INVENTORY) {
                int buyedSize = 0;
                int buyedAmount = 0;
                for (; ; ) {
                    if (buyedAmount >= shopItem.getMaxStackSize()) break;
                    if (user.getBukkitPlayer().getInventory().containsAtLeast(needResource, needResource.getAmount())) {
                        if (user.getBukkitPlayer().getInventory().addItem(shopItem).isEmpty()) {
                            user.getBukkitPlayer().getInventory().removeItem(needResource);
                            if (buyedSize == 0)
                                user.playSound(Sound.CHICKEN_EGG_POP, 1, -10);
                            buyedSize++;
                            buyedAmount += shopItem.getAmount();
                        } else {
                            if (buyedSize == 0) {
                                user.sendMessage("not-enough-space");
                                playFail(user);
                            }
                            break;
                        }
                    } else {
                        break;
                    }
                }
            } else {
                if (user.getBukkitPlayer().getInventory().addItem(shopItem).isEmpty()) {
                    user.getBukkitPlayer().getInventory().removeItem(needResource);
                    user.playSound(Sound.CHICKEN_EGG_POP, 1, -10);
                } else {
                    user.sendMessage("not-enough-space");
                    playFail(user);
                }
            }
        } else {
            user.sendMessage("not-enough-resources");
            playFail(user);
        }
    }

    public static void playFail(IUser user) {
        user.playSound(Sound.NOTE_BASS, 40, 5);
    }

    public static void playClick(IUser user) {
        user.playSound(Sound.CLICK, 40, 5);
    }
}
