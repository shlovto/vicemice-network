package net.vicemice.game.bedwars.game.scoreboard;

import net.vicemice.game.bedwars.config.game.GameImporter;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.game.bedwars.player.spectator.SpectatorManager;
import lombok.Getter;
import net.vicemice.hector.GoAPI;
import org.bukkit.Bukkit;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.HashMap;

public class ScoreboardManager {
    @Getter
    private static Scoreboard scoreboard;
    @Getter
    private static HashMap<GameTeam, Team> scoreboardTeams = new HashMap<>();
    @Getter
    private static Objective sbObjective;

    public static void registerScoreboard() {
        scoreboard = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
        Team spectatorTeam = scoreboard.registerNewTeam("spectator");
        spectatorTeam.setCanSeeFriendlyInvisibles(true);
        spectatorTeam.setPrefix("§7");
        SpectatorManager.setSpectatorTeam(spectatorTeam);
    }

    public static void updateSideboard() {
        if (sbObjective != null) {
            sbObjective.unregister();
        }
        sbObjective = scoreboard.registerNewObjective("test", "dummy");
        sbObjective.setDisplayName("§b§lVICE§f§lMICE §c§lBW");
        sbObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
        sbObjective.getScore("§3").setScore(16);
        int highest = 0;
        for (String color : GameImporter.getTeams().keySet()) {
            GameTeam gameTeam = GameImporter.getTeams().get(color);
            if (gameTeam.isBedAlive() && gameTeam.getPlayers().size() != 0) {
                sbObjective.getScore("§a❤ " + gameTeam.getColor() + gameTeam.getRightName()).setScore(gameTeam.getPlayers().size());
            } else {
                sbObjective.getScore("§7❤ " + gameTeam.getColor() + gameTeam.getRightName()).setScore(gameTeam.getPlayers().size());
            }
            if (gameTeam.getPlayers().size() > highest) {
                highest = gameTeam.getPlayers().size();
            }
        }
        sbObjective.getScore("§3").setScore(-1);
        sbObjective.getScore("§7"+ GoAPI.getTimeManager().getTime("dd/MM/yyyy", System.currentTimeMillis())).setScore(-2);
    }
}
