package net.vicemice.game.bedwars.player.teams.lobby;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.config.game.GameImporter;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.game.bedwars.util.ColorManager;
import lombok.Getter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LobbyTeamManager {
    @Getter
    private static Inventory selectInventory;
    @Getter
    private static HashMap<Integer, GameTeam> teams = new HashMap<>();

    public static void updateSelectInventory() {
        if (selectInventory == null) {
            selectInventory = Bukkit.getServer().createInventory(null, 9, "§6Teams");
        }
        int currentSlot = 0;
        for (String teamColor : GameImporter.getTeams().keySet()) {
            GameTeam gameTeam = GameImporter.getTeams().get(teamColor);
            teams.put(currentSlot, gameTeam);
            ItemStack itemStack = new ItemStack(Material.STAINED_CLAY, gameTeam.getPlayers().size(), ColorManager.getDyeColor(gameTeam).getWoolData());
            ItemMeta itemMeta = itemStack.getItemMeta();
            itemMeta.setDisplayName(gameTeam.getColor() + "Team " + gameTeam.getRightName());
            List<String> lore = new ArrayList<>();
            lore.add(gameTeam.getColor() + "" + gameTeam.getPlayers().size() + "§7/" + gameTeam.getColor() + BedWars.getGameConfig().getPER_TEAM());
            lore.add("§7-------------------");
            int current = 1;
            for (IUser user : gameTeam.getPlayers()) {
                lore.add(gameTeam.getColor() + "#" + current + " " + user.getNickName());
                current++;
            }
            itemMeta.setLore(lore);
            itemStack.setItemMeta(itemMeta);
            selectInventory.setItem(currentSlot, itemStack);
            currentSlot++;
        }
    }
}
