package net.vicemice.game.bedwars.player.board.lobby;

import lombok.Getter;
import net.vicemice.game.bedwars.BedWars;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.util.EloRank;
import net.vicemice.hector.server.player.scoreboard.PacketScoreboard;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Player;

public class LobbyScoreboard {
    @Getter
    private IUser user;
    @Getter
    private PacketScoreboard packetScoreboard;

    public LobbyScoreboard(IUser user) {
        this.user = user;
        this.packetScoreboard = new PacketScoreboard(user.getBukkitPlayer());
        packetScoreboard.remove();
        packetScoreboard.sendSidebar((BedWars.getGameConfig().isRANKED() ? "§c§lBW §f§lRanked" : "§c§lBW §f§lUnranked"));
        if (BedWars.getGameConfig().isRANKED()) {
            long elo = (user.getStatistic("bw_elo", StatisticPeriod.MONTH) >= 1 ? user.getStatistic("bw_elo", StatisticPeriod.MONTH) : 0);
            EloRank rank = EloRank.rankByElo(elo);
            if (rank == null) rank = EloRank.UNRANKED;
            packetScoreboard.setLine(9, "§4");
            packetScoreboard.setLine(8, "§b§l"+user.translate("skill-group"));
            if (rank == EloRank.UNRANKED) {
                packetScoreboard.setLine(7, "§c"+user.translate("none"));
            } else {
                packetScoreboard.setLine(7, "§7"+rank.getColor()+rank.getUniqueName()+" "+rank.getName());
            }
        }
        packetScoreboard.setLine(6, "§3");
        packetScoreboard.setLine(5, "§b§l"+user.translate("map"));
        if (GameManager.getLobbyCount() > 10 && BedWars.getGameConfig().isVOTING()) {
            packetScoreboard.setLine(4, "§7Voting...");
        } else {
            packetScoreboard.setLine(4, "§7"+ BedWars.getGame().getBedWarsMap().getDisplayName());
        }
        packetScoreboard.setLine(3, "§2");
        packetScoreboard.setLine(2, "§b§l"+user.translate("type"));
        packetScoreboard.setLine(1, "§7"+BedWars.getGameConfig().getTYPE());
        packetScoreboard.setLine(0, "§1");
        setMap();
    }

    public void setMap() {
        packetScoreboard.removeLine(4);
        if (GameManager.getLobbyCount() > 10 && BedWars.getGameConfig().isVOTING()) {
            packetScoreboard.setLine(4, "§7Voting...");
        } else {
            packetScoreboard.setLine(4, "§7"+ BedWars.getGame().getBedWarsMap().getDisplayName());
        }
    }
}
