package net.vicemice.game.bedwars.player;

import net.vicemice.game.bedwars.player.stats.Coins;
import net.vicemice.game.bedwars.player.stats.Points;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.game.bedwars.BedWars;
import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Player;

public class PlayerData {
	@Getter
	public IUser user;
	@Getter
	@Setter
	public GameTeam gameTeam;
	@Setter
	private IUser lastHitted;
	private long hittedTime;
	private long coins;
	private int kills, deaths, destroyed_beds, points, wins, played_games;
	private boolean saved = false;
	@Getter
	@Setter
	private int keepLevel;

	public PlayerData(IUser user) {
		this.user = user;
	}

	public void receiveDamageFrom(IUser user) {
		this.lastHitted = user;
		this.hittedTime = System.currentTimeMillis() + 30 * 1000;
	}

	public IUser getLastHitted() {
		if (lastHitted != null) {
			if (hittedTime > System.currentTimeMillis()) { return lastHitted; }
		}
		return null;
	}

	public void addKill() {
		kills++;
		points += Points.KILL.getPoints();
		int coins = Coins.KILL.getCoins();
		if (BedWars.getGameConfig().getTYPE().equalsIgnoreCase("1x1")) {
			coins = coins / 2;
		}
		if (BedWars.isEventMode()) {
			coins = 15;
		}
		this.coins+=coins;
		user.addXP(3);

		if (BedWars.getGameConfig().isRANKED()) {
			user.increaseStatistic("bw_elo", 10);
		}
	}

	public void addPlayedGame() {
		played_games++;
	}

	public void addDeath() {
		deaths++;

		if (BedWars.getGameConfig().isRANKED()) {
			user.decreaseStatistic("bw_elo", 10);
		}
	}
	
	public void addDeaths(int count) {
		deaths += count;
	}

	public void addDestroyedBed() {
		destroyed_beds++;
		points += Points.BED.getPoints();
		int coins = Coins.BED.getCoins();
		if (BedWars.getGameConfig().getTYPE().equalsIgnoreCase("1x1")) {
			coins = coins / 2;
		}
		if (BedWars.isEventMode()) {
			coins = 400;
		}
		this.coins+=coins;
		user.addXP(10);

		if (BedWars.getGameConfig().isRANKED()) {
			user.increaseStatistic("bw_elo", 10);
		}
	}

	public void addWin() {
		wins++;
		points += Points.WIN.getPoints();
		int coins = Coins.WIN.getCoins();
		if (BedWars.getGameConfig().getTYPE().equalsIgnoreCase("1x1")) {
			coins = coins / 2;
		}
		if (BedWars.isEventMode()) {
			coins = 500;
		}
		this.coins+=coins;
		user.addXP(20);

		if (BedWars.getGameConfig().isRANKED()) {
			user.increaseStatistic("bw_elo", 25);
		}
	}

	public void saveStats() {
		if (BedWars.isTestMode()) return;
		if (saved) return;
		saved = true;
		if (BedWars.getGameConfig().getTYPE().equalsIgnoreCase("1x1")) {
			points = points / 2;
		}

		this.user.sendMessage("round-stats",
				GameManager.getTime(GameManager.getStartTime()),
				this.kills,
				this.deaths,
				this.destroyed_beds);

		if (kills != 0) {
			user.increaseStatistic(BedWars.getStatsPrefix()+"kills", kills);
		}
		if (deaths != 0) {
			user.increaseStatistic(BedWars.getStatsPrefix()+"deaths", kills);
		}
		if (destroyed_beds != 0) {
			user.increaseStatistic(BedWars.getStatsPrefix()+"destroyed_beds", destroyed_beds);
		}
		if (points != 0) {
			user.increaseStatistic("points", points);
		}
		if (wins != 0) {
			user.increaseStatistic(BedWars.getStatsPrefix()+"wins", wins);
		}
		if (played_games != 0) {
			user.increaseStatistic(BedWars.getStatsPrefix()+"played_games", played_games);
			user.increaseStatistic("played_games", 1);
		}
		user.increaseCoins(coins);
		user.sendMessage("receive-coins", coins);
	}
}
