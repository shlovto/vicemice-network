package net.vicemice.game.bedwars.game;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.config.game.GameImporter;
import net.vicemice.game.bedwars.game.scoreboard.ScoreboardManager;
import net.vicemice.game.bedwars.player.teams.lobby.LobbyTeamManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.types.ServerState;

public class StartupManager {

    public static void startupBedWars() {
        try {
            // Load config
            GameImporter.importConfiguration();
            // Load maps
            GameImporter.importMaps();
            // Create Voting
            VotingManager.createVoting();
            // Start Timer
            GameManager.startTimer(BedWars.getInstance(), GoServer.getService().getGoAPI());
            // Create Inventories
            VotingManager.createVoting();
            LobbyTeamManager.updateSelectInventory();
            // Select Random Map
            if (!BedWars.getGameConfig().isVOTING()) {
                GameImporter.selectRandomMap();
            }
            // Scoreboard
            ScoreboardManager.registerScoreboard();
            // Open Server for Players
            GameManager.setGameState(GameState.LOBBY);
            if (BedWars.getGameConfig().isVOTING()) {
                GoAPI.getServerAPI().changeServer(ServerState.LOBBY, "&eVoting &8- &e"+ BedWars.getGameConfig().getTYPE());
            } else {
                GoAPI.getServerAPI().changeServer(ServerState.LOBBY, "&e"+BedWars.getGame().getBedWarsMap().getDisplayName()+" &8- &e"+ BedWars.getGameConfig().getTYPE());
            }
            GoAPI.getServerAPI().setKick(true);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }
}