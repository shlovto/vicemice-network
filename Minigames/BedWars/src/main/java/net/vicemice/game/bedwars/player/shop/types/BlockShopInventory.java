package net.vicemice.game.bedwars.player.shop.types;

import net.vicemice.game.bedwars.player.PlayerManager;
import net.vicemice.game.bedwars.player.shop.DefaultShopInventory;
import net.vicemice.game.bedwars.player.shop.NeedResourceType;
import net.vicemice.game.bedwars.player.shop.ShopManager;
import net.vicemice.game.bedwars.player.teams.GameTeam;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/*
 * Class created at 09:57 - 06.04.2020
 * Copyright (C) elrobtossohn
 */
public class BlockShopInventory {

    public GUI create(IUser user) {
        GUI gui = new DefaultShopInventory().create(user, DefaultShopInventory.ShopType.BLOCKS);

        String bronze = user.translate("bronze");
        String iron = user.translate("iron");
        String gold = user.translate("gold");
        GameTeam gameTeam = PlayerManager.getPlayerDatas().get(user).getGameTeam();

        gui.setItem(28, ItemBuilder.create(Material.WOOL).amount(16).durability(gameTeam.getTeamColor()).name("&716&8x &e"+user.translate("shop-blocks-woolblock")).lore(user.translate("price", "&c8 "+bronze)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 8, ItemBuilder.create(Material.WOOL).amount(16).durability(gameTeam.getTeamColor()).build());
        });

        gui.setItem(29, ItemBuilder.create(Material.SANDSTONE).amount(16).name("&716&8x &e"+user.translate("shop-blocks-sandstone")).lore(user.translate("price", "&c16 "+bronze)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 16, ItemBuilder.create(Material.SANDSTONE).amount(16).build());
        });

        gui.setItem(30, ItemBuilder.create(Material.STAINED_GLASS).amount(32).durability(gameTeam.getTeamColor()).name("&732&8x &e"+user.translate("shop-blocks-glass")).lore(user.translate("price", "&c16 "+bronze)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 16, ItemBuilder.create(Material.STAINED_GLASS).amount(32).durability(gameTeam.getTeamColor()).build());
        });

        gui.setItem(31, ItemBuilder.create(Material.WOOD).amount(1).name("&71&8x &e"+user.translate("shop-blocks-woodblock")).lore(user.translate("price", "&c4 "+bronze)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 4, ItemBuilder.create(Material.WOOD).amount(1).build());
        });

        gui.setItem(32, ItemBuilder.create(Material.ENDER_STONE).amount(1).name("&71&8x &e"+user.translate("shop-blocks-endstone")).lore(user.translate("price", "&c7 "+bronze)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.BRONZE, 7, ItemBuilder.create(Material.ENDER_STONE).amount(1).build());
        });

        gui.setItem(33, ItemBuilder.create(Material.IRON_BLOCK).amount(1).name("&71&8x &e"+user.translate("shop-blocks-ironblock")).lore(user.translate("price", "&72 "+iron)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.IRON, 2, ItemBuilder.create(Material.IRON_BLOCK).amount(1).build());
        });

        gui.setItem(34, ItemBuilder.create(Material.OBSIDIAN).amount(1).name("&71&8x &e"+user.translate("shop-blocks-obsidian")).lore(user.translate("price", "&62 "+gold)).build(), e -> {
            ShopManager.validBuy(user, e.getAction(), NeedResourceType.GOLD, 2, ItemBuilder.create(Material.OBSIDIAN).amount(1).build());
        });

        return gui;
    }
}