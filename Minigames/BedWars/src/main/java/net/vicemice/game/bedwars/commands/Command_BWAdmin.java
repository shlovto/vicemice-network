package net.vicemice.game.bedwars.commands;

import net.vicemice.game.bedwars.BedWars;
import net.vicemice.game.bedwars.player.teams.TeamManager;
import net.vicemice.game.bedwars.shop.GameType;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.hector.utils.Rank;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.spigotmc.CustomTimingsHandler;

public class Command_BWAdmin implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        Player player = (Player) sender;
        PlayerRankData playerRankData = GoAPI.getUserAPI().getRankData(player.getUniqueId());
        if (playerRankData.getAccess_level() < Rank.ADMIN.getAccessLevel()) {
            player.sendMessage("§cI'm sorry, but you do not have permission to perform this command. Please contact the server administrators if you believe that this is in error.");
            return true;
        }

        if (strings.length == 0) {
            sender.sendMessage("§cInvalid arg count!");
            return true;
        }
        if (strings[0].equalsIgnoreCase("beds")) {
            BedWars.getGame().getBedWarsMap().getBeds().forEach((k,v) -> {
                player.sendMessage(k.getRightName()+": "+v.toString());
            });
        } else if (strings[0].equalsIgnoreCase("tostart")) {
            BedWars.getGameConfig().setTO_START(Integer.parseInt(strings[1]));
        } else if (strings[0].equalsIgnoreCase("count")) {
            GameManager.setLobbyCount(Integer.parseInt(strings[1]));
        } else if (strings[0].equalsIgnoreCase("tp")) {
            player.sendMessage("Teleport...");
            player.teleport(BedWars.getGame().getBedWarsMap().getWorld().getSpawnLocation());
        } else if (strings[0].equalsIgnoreCase("info")) {
            player.sendMessage("World: " + player.getWorld().getName());
        } else if (strings[0].equalsIgnoreCase("stats")) {
            if (BedWars.isTestMode()) {
                BedWars.setTestMode(false);
                sender.sendMessage("§cTest Mode disabled");
            } else {
                BedWars.setTestMode(true);
                sender.sendMessage("§aTest Mode enabled");
            }
        } else if (strings[0].equalsIgnoreCase("ca")) {
            TeamManager.checkAliveTeams();
        } else if (strings[0].equalsIgnoreCase("sm")) {
            BedWars.getGame().setGameType(GameType.valueOf(strings[1]));
            sender.sendMessage("§aGame Type changed.");
            return true;
        } else if (strings[0].equalsIgnoreCase("eopen")) {
            BedWars.setOpen(true);
        } else if (strings[0].equalsIgnoreCase("eclose")) {
            BedWars.setOpen(false);
        } else if (strings[0].equalsIgnoreCase("timings")) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            PrintStream os = new PrintStream(bos);

            CustomTimingsHandler.printTimings(os);
            os.println("Sample time " + System.nanoTime() + " (" + System.nanoTime() / 1.0E9D + "s)");

            os.println("<spigotConfig>");
            os.println(Bukkit.spigot().getConfig().saveToString());
            os.println("</spigotConfig>");
            System.out.println(new String(bos.toByteArray()));
            sender.sendMessage("§aTimings printed!");
        }
        return true;
    }
}
