package net.vicemice.game.qsg.chest;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class SurvivalChest {

    private static ConcurrentHashMap<Location, Inventory> survivalChests = new ConcurrentHashMap<>();

    public static void openChest(Player player, Location location) {
        if (survivalChests.containsKey(location)) {
            player.openInventory(survivalChests.get(location));
        } else {
            int ranint6;
            Random ran6;
            int ranint5;
            Random ran5;
            int ranint4;
            Random ran4;
            int ranint3;
            Random ran3;
            int ranint2;
            Random ran2;
            player.playSound(player.getLocation(), Sound.CHEST_OPEN, 10.0f, 1.0f);
            Random rnd = new Random();
            int n = 1;
            n = rnd.nextInt(9);
            Inventory inv = Bukkit.createInventory(null, (InventoryType)InventoryType.CHEST);
            ArrayList<ItemStack> items = new ArrayList<ItemStack>();
            items.add(new ItemStack(Material.APPLE));
            items.add(new ItemStack(Material.STONE_AXE));
            items.add(new ItemStack(Material.STONE_SWORD));
            items.add(new ItemStack(Material.IRON_CHESTPLATE));
            items.add(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
            items.add(new ItemStack(Material.CHAINMAIL_LEGGINGS));
            items.add(new ItemStack(Material.DIAMOND_CHESTPLATE));
            items.add(new ItemStack(Material.CHAINMAIL_BOOTS));
            items.add(new ItemStack(Material.BREAD));
            items.add(new ItemStack(Material.ARROW, 3));
            items.add(new ItemStack(Material.BOW));
            items.add(new ItemStack(Material.IRON_SWORD));
            items.add(new ItemStack(Material.IRON_HELMET));
            items.add(new ItemStack(Material.IRON_LEGGINGS));
            items.add(new ItemStack(Material.IRON_BOOTS));
            Random ran = new Random();
            int ranint = ran.nextInt(6);
            if (ranint == 5) {
                ran2 = new Random();
                ranint2 = ran2.nextInt(27);
                ran3 = new Random();
                ranint3 = ran3.nextInt(27);
                ran4 = new Random();
                ranint4 = ran4.nextInt(27);
                ran5 = new Random();
                ranint5 = ran5.nextInt(27);
                ran6 = new Random();
                ranint6 = ran6.nextInt(27);
                setInInventory(inv, new ItemStack(Material.LEATHER_HELMET), ranint2);
                setInInventory(inv, new ItemStack(Material.WOOD_PICKAXE), ranint3);
                setInInventory(inv, new ItemStack(Material.COOKED_BEEF, 2), ranint4);
                setInInventory(inv, new ItemStack(Material.TNT), ranint5);
                setInInventory(inv, new ItemStack(Material.FISHING_ROD), ranint6);
            } else if (ranint == 4) {
                ran2 = new Random();
                ranint2 = ran2.nextInt(27);
                ran3 = new Random();
                ranint3 = ran3.nextInt(27);
                ran4 = new Random();
                ranint4 = ran4.nextInt(27);
                ran5 = new Random();
                ranint5 = ran5.nextInt(27);
                ran6 = new Random();
                ranint6 = ran6.nextInt(27);
                setInInventory(inv, new ItemStack(Material.EXP_BOTTLE, 2), ranint2);
                setInInventory(inv, new ItemStack(Material.COOKED_BEEF), ranint3);
                setInInventory(inv, new ItemStack(Material.FLINT_AND_STEEL), ranint4);
                setInInventory(inv, new ItemStack(Material.STICK), ranint5);
                setInInventory(inv, new ItemStack(Material.LEATHER_CHESTPLATE), ranint6);
            } else if (ranint == 3) {
                ran2 = new Random();
                ranint2 = ran2.nextInt(27);
                ran3 = new Random();
                ranint3 = ran3.nextInt(27);
                ran4 = new Random();
                ranint4 = ran4.nextInt(27);
                ran5 = new Random();
                ranint5 = ran5.nextInt(27);
                ran6 = new Random();
                ranint6 = ran6.nextInt(27);
                setInInventory(inv, new ItemStack(Material.IRON_BOOTS), ranint2);
                setInInventory(inv, new ItemStack(Material.STONE_AXE), ranint3);
                setInInventory(inv, new ItemStack(Material.BREAD, 2), ranint4);
                setInInventory(inv, new ItemStack(Material.TNT), ranint5);
                setInInventory(inv, new ItemStack(Material.ARROW, 3), ranint6);
            } else if (ranint == 2) {
                ran2 = new Random();
                ranint2 = ran2.nextInt(27);
                ran3 = new Random();
                ranint3 = ran3.nextInt(27);
                ran4 = new Random();
                ranint4 = ran4.nextInt(27);
                ran5 = new Random();
                ranint5 = ran5.nextInt(27);
                ran6 = new Random();
                ranint6 = ran6.nextInt(27);
                setInInventory(inv, new ItemStack(Material.BOW), ranint2);
                setInInventory(inv, new ItemStack(Material.IRON_LEGGINGS), ranint3);
                setInInventory(inv, new ItemStack(Material.CHAINMAIL_BOOTS), ranint4);
                setInInventory(inv, new ItemStack(Material.IRON_SWORD), ranint5);
                setInInventory(inv, new ItemStack(Material.CHAINMAIL_HELMET), ranint6);
            } else if (ranint == 1) {
                ran2 = new Random();
                ranint2 = ran2.nextInt(27);
                ran3 = new Random();
                ranint3 = ran3.nextInt(27);
                ran4 = new Random();
                ranint4 = ran4.nextInt(27);
                ran5 = new Random();
                ranint5 = ran5.nextInt(27);
                ran6 = new Random();
                ranint6 = ran6.nextInt(27);
                setInInventory(inv, new ItemStack(Material.DIAMOND_CHESTPLATE), ranint2);
                setInInventory(inv, new ItemStack(Material.BAKED_POTATO), ranint3);
                setInInventory(inv, new ItemStack(Material.CHAINMAIL_LEGGINGS), ranint4);
                setInInventory(inv, new ItemStack(Material.STONE_SWORD), ranint5);
                setInInventory(inv, new ItemStack(Material.BOW), ranint6);
            }
            if (ranint == 0) {
                ran2 = new Random();
                ranint2 = ran2.nextInt(27);
                ran3 = new Random();
                ranint3 = ran3.nextInt(27);
                ran4 = new Random();
                ranint4 = ran4.nextInt(27);
                ran5 = new Random();
                ranint5 = ran5.nextInt(27);
                ran6 = new Random();
                ranint6 = ran6.nextInt(27);
                setInInventory(inv, new ItemStack(Material.IRON_CHESTPLATE), ranint2);
                setInInventory(inv, new ItemStack(Material.APPLE), ranint3);
                setInInventory(inv, new ItemStack(Material.LEATHER_BOOTS), ranint4);
                setInInventory(inv, new ItemStack(Material.BREAD), ranint5);
                setInInventory(inv, new ItemStack(Material.ARROW, 2), ranint6);
            }
            survivalChests.put(location, inv);
            player.openInventory(survivalChests.get(location));
        }
    }

    private static void setInInventory(Inventory inv, ItemStack stack, int position) {
        inv.setItem(position, stack);
    }
}
