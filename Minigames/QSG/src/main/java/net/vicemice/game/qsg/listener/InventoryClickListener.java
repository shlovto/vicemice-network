package net.vicemice.game.qsg.listener;

import net.vicemice.game.qsg.player.spectator.SpectatorManager;
import net.vicemice.game.qsg.util.FlintUtil;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.player.UserManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class InventoryClickListener implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        ItemStack current = event.getCurrentItem();
        if (GameManager.getGameState() == GameState.INGAME) {
            if (SpectatorManager.getSpectatorPlayers().contains(UserManager.getUser(player))) {
                event.setCancelled(true);
                if (event.getClickedInventory() != null && event.getClickedInventory().getTitle() != null && event.getClickedInventory().getTitle().equalsIgnoreCase("§7Teleporter")) {
                    if (current != null) {
                        if (current.getType() == Material.SKULL_ITEM) {
                            String name = ChatColor.stripColor(current.getItemMeta().getDisplayName());
                            Player getPlayer = Bukkit.getPlayer(name);
                            if (getPlayer == null) return;
                            player.teleport(getPlayer);
                            player.closeInventory();
                        }
                    }
                }
            }
        }
        if (GameManager.getGameState() == GameState.INGAME || GameManager.getGameState() == GameState.WARMUP) {
            if (current != null && current.getType() == Material.FLINT_AND_STEEL) {
                short durability = FlintUtil.getFlintAndSteel().getDurability();
                if (current.getDurability() > durability) {
                    current.setDurability(FlintUtil.getFlintAndSteel().getDurability());
                }
            }
        }
    }
}