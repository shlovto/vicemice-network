package net.vicemice.game.qsg.game.tasks;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.game.VotingManager;
import lombok.Getter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.gameapi.events.GameLobbyEvent;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.actionbat.ActionBarAPI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class GameLobbyListener implements Listener {

    @Getter
    private static int playerAnnounce = 8;

    @EventHandler
    public void onLobby(GameLobbyEvent event) {
        if (Bukkit.getServer().getOnlinePlayers().size() >= QSG.getGameConfig().getTO_START()) {
            if (GameManager.getLobbyCount() > 20 && Bukkit.getServer().getOnlinePlayers().size() == GoServer.getService().getGoConfig().getMAX_PLAYERS()) {
                GameManager.setLobbyCount(20);
            }
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.setLevel(GameManager.getLobbyCount());
                player.setExp(GameManager.getLobbyCount() * 0.0166666666666667F);
                if (GameManager.getLobbyCount() <= 3) {
                    player.playSound(player.getLocation(), Sound.NOTE_BASS, 40, 1);
                }
            }
            if (GameManager.getLobbyCount() == 60 || GameManager.getLobbyCount() == 45 || GameManager.getLobbyCount() == 30 || GameManager.getLobbyCount() == 20 || GameManager.getLobbyCount() == 15 || GameManager.getLobbyCount() == 10 || GameManager.getLobbyCount() <= 5) {
                if (GameManager.getLobbyCount() == 1) {
                    GameManager.startGame();
                    UserManager.getConnectedUsers().values().forEach((o) -> o.sendMessage("game-lobby-last-msg", o.translate("qsg-prefix")));
                    GoServer.getLocaleManager().getLocales().forEach((k, v) -> GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "game-lobby-last-msg", GoServer.getLocaleManager().getPrivateMessage(k, "qsg-prefix")), k));
                } else {
                    UserManager.getConnectedUsers().values().forEach((o) -> o.sendMessage("game-lobby-msg", o.translate("qsg-prefix"), GameManager.getLobbyCount()));
                    GoServer.getLocaleManager().getLocales().forEach((k, v) -> GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "game-lobby-msg", GoServer.getLocaleManager().getPrivateMessage(k, "qsg-prefix"), GameManager.getLobbyCount()), k));
                }
                if (GameManager.getLobbyCount() == 10) {
                    VotingManager.endVoting();
                    Bukkit.getOnlinePlayers().forEach(e -> e.getInventory().remove(Material.PAPER));
                    GameManager.sendTitle("§bQuickSurvivalGames", "§e" + QSG.getGame().getQSGMap().getDisplayName());
                }
            }
            GameManager.setLobbyCount(GameManager.getLobbyCount() - 1);
        } else {
            GameManager.setLobbyCount(60);
            for (IUser user : UserManager.getConnectedUsers().values()) {
                user.getBukkitPlayer().setExp(0);
                user.getBukkitPlayer().setLevel(0);

                int need = QSG.getGameConfig().getTO_START() - Bukkit.getOnlinePlayers().size();
                if (need == 1)
                    user.sendActionBarMessage(user.translate("game-start-more-player"));
                else
                    user.sendActionBarMessage(user.translate("game-start-more-players", need));
            }
        }
    }
}
