package net.vicemice.game.qsg.listener;

import net.vicemice.game.qsg.player.spectator.SpectatorManager;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.player.UserManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class ItemListener implements Listener {
    @EventHandler
    public void onItemDrop(PlayerDropItemEvent event) {
        Player player = event.getPlayer();
        if (GameManager.getGameState() != GameState.INGAME) {
            event.setCancelled(true);
            return;
        }
        if (SpectatorManager.getSpectatorPlayers().contains(UserManager.getUser(player))) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onItemPickup(PlayerPickupItemEvent event) {
        Player player = event.getPlayer();
        if (GameManager.getGameState() != GameState.INGAME) {
            event.setCancelled(true);
            return;
        }
        if (SpectatorManager.getSpectatorPlayers().contains(UserManager.getUser(player))) {
            event.setCancelled(true);
        }
    }
}