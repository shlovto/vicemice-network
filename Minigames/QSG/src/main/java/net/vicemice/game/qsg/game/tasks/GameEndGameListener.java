package net.vicemice.game.qsg.game.tasks;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.player.PlayerData;
import net.vicemice.game.qsg.player.PlayerManager;
import net.vicemice.game.qsg.player.spectator.SpectatorManager;
import net.vicemice.game.qsg.player.team.GameTeam;
import net.vicemice.game.qsg.player.team.TeamManager;
import net.vicemice.game.qsg.util.MessageUtils;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.gameapi.events.GameEndGameEvent;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.UUID;

public class GameEndGameListener implements Listener {

    @EventHandler
    public void onEndGame(GameEndGameEvent event) {
        ArrayList<String> uniqueIds = new ArrayList<>();
        for (UUID uniqueId : GameManager.getReplayPlayers()) {
            uniqueIds.add(String.valueOf(uniqueId));
        }

        GameManager.getReplayPlayers().forEach((o) -> GoAPI.getUserAPI().addRecordedGame(o, GoAPI.getGameIDAPI().getGameID(), GoAPI.getGameIDAPI().getGameUID(), GameManager.getReplayStartSeconds(), GameManager.getStartTime(), GameManager.getLength(), uniqueIds, "QSG", QSG.getGameConfig().getTYPE(), QSG.getGame().getQSGMap().getDisplayName(), (GameManager.getSaveReplayPlayers().contains(o))));

        if (TeamManager.getAliveTeams().size() == 1) {
            GameManager.setGameState(GameState.END);
            GoAPI.getServerAPI().setScoreboard(null);
            GameTeam winnerTeam = TeamManager.getAliveTeams().get(0);
            for (IUser user : winnerTeam.getMembers()) {
                UserManager.getConnectedUsers().values().forEach((o) -> o.sendMessage("player-won", o.translate("qsg-prefix"),  (user.isNicked() ? "§9"+user.getNickName() : user.getRank().getRankColor()+user.getName())));
                GoServer.getLocaleManager().getLocales().forEach((k, v) -> GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "player-won", GoServer.getLocaleManager().getPrivateMessage(k, "qsg-prefix"), (user.isNicked() ? "§9"+user.getNickName() : user.getRank().getRankColor()+user.getName())), k));

                PlayerData playerData = PlayerManager.getPlayerData(user);
                playerData.addWin();
                playerData.saveStats();
            }

            for (IUser user : SpectatorManager.getSpectatorPlayers()) {
                user.getBukkitPlayer().removePotionEffect(PotionEffectType.INVISIBILITY);
                for (Player online : Bukkit.getOnlinePlayers()) {
                    online.showPlayer(user.getBukkitPlayer());
                }
            }
        }

        GoAPI.getServerAPI().changeServer(ServerState.NO_STATE, "Restarting Server");
    }
}