package net.vicemice.game.qsg.config.map.vote;

import net.vicemice.game.qsg.config.map.MapData;
import lombok.Getter;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/*
 * Class created at 16:18 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Getter
public class VoteMap {

    private MapData mapData;
    private List<IUser> votes = new ArrayList<>();
    private ItemStack itemStack;

    public VoteMap(MapData mapData, ItemStack itemStack) {
        this.mapData = mapData;
        this.itemStack = itemStack;
    }
}
