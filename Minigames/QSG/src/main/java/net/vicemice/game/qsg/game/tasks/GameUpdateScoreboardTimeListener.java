package net.vicemice.game.qsg.game.tasks;

import net.vicemice.game.qsg.game.ScoreboardManager;
import net.vicemice.game.qsg.player.PlayerData;
import net.vicemice.game.qsg.player.PlayerManager;
import net.vicemice.hector.gameapi.events.GameUpdateScoreboardTimeEvent;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class GameUpdateScoreboardTimeListener implements Listener {

    @EventHandler
    public void onUpdate(GameUpdateScoreboardTimeEvent event) {
        for (IUser user : PlayerManager.getPlayerDatas().keySet()) {
            PlayerData playerData = PlayerManager.getPlayerData(user);
            if (playerData.getPlayerScoreboard() != null) {
                ScoreboardManager.updateTime(playerData);
            }
            double distance = 0;
            Player findPlayer = null;
            for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
                if (onlinePlayer.equals(user.getBukkitPlayer())) continue;
                PlayerData onlineData = PlayerManager.getPlayerData(user);
                if (onlineData == null) continue;
                if (findPlayer == null || onlinePlayer.getLocation().distance(findPlayer.getLocation()) < distance) {
                    if (findPlayer != null) {
                        distance = onlinePlayer.getLocation().distance(findPlayer.getLocation());
                    }
                    findPlayer = onlinePlayer;
                }
            }
            if (findPlayer != null) {
                user.getBukkitPlayer().setCompassTarget(findPlayer.getLocation());
            }
        }
    }
}
