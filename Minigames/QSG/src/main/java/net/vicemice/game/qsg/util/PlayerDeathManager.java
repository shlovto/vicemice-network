package net.vicemice.game.qsg.util;

import net.vicemice.game.qsg.player.team.TeamManager;
import net.vicemice.game.qsg.player.PlayerData;
import net.vicemice.game.qsg.player.PlayerManager;
import net.vicemice.game.qsg.game.ScoreboardManager;
import net.vicemice.game.qsg.player.spectator.SpectatorManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.gameapi.util.Title;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Locale;

public class PlayerDeathManager {

    public static void playerDeath(IUser user, boolean quit) {
        PlayerData playerData = PlayerManager.getPlayerData(user);
        IUser killer = playerData.getLastHitted();
        if (killer != null && PlayerManager.getPlayerDatas().containsKey(killer)) {
            PlayerData killerData = PlayerManager.getPlayerData(killer);
            String colorPlayer, namePlayer, colorKiller, nameKiller;
            if (user.isNicked()) {
                colorPlayer = "§9";
                namePlayer = user.getNickName();
            } else {
                colorPlayer = user.getRank().getRankColor();
                namePlayer = user.getName();
            }
            if (killer.isNicked()) {
                colorKiller = "§9";
                nameKiller = killer.getNickName();
            } else {
                colorKiller = killer.getRank().getRankColor();
                nameKiller = killer.getName();
            }

            UserManager.getConnectedUsers().values().forEach(o -> o.sendMessage("game-player-killed", o.translate("qsg-prefix"), (colorPlayer + namePlayer), (colorKiller + nameKiller)));
            GoServer.getLocaleManager().getLocales().forEach((k, v) -> GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "game-player-killed", GoServer.getLocaleManager().getPrivateMessage(k, "qsg-prefix"), (colorPlayer + namePlayer), (colorKiller + nameKiller)), k));

            killer.playSound(killer.getLocation(), Sound.LEVEL_UP, 40, 1);
            if (playerData.isProjectileHit()) {
                killerData.addProjectile();
            }
            killerData.addKill();
            ScoreboardManager.updateKills(killer);
        } else {
            String color = user.getRank().getRankColor(), name = user.getName();
            if (user.isNicked()) {
                color = "§9";
                name = user.getNickName();
            }

            String finalColor = color;
            String finalName = name;

            for (IUser o : UserManager.getConnectedUsers().values()) {
                o.sendMessage("game-player-died", o.translate("qsg-prefix"), (finalColor + finalName));
            }
            GoServer.getLocaleManager().getLocales().forEach((k, v) -> GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "game-player-died", GoServer.getLocaleManager().getPrivateMessage(k, "qsg-prefix"), (finalColor+finalName)), k));
        }
        playerData.addDeath();
        playerData.saveStats();
        TeamManager.playerDeath(user);
        if (quit) {
            for (ItemStack itemStack : user.getBukkitPlayer().getInventory().getContents()) {
                if (itemStack == null || itemStack.getType() == Material.AIR) continue;
                user.getBukkitPlayer().getWorld().dropItemNaturally(user.getLocation(), itemStack);
            }
            for (ItemStack itemStack : user.getBukkitPlayer().getInventory().getArmorContents()) {
                if (itemStack == null || itemStack.getType() == Material.AIR) continue;
                user.getBukkitPlayer().getWorld().dropItemNaturally(user.getLocation(), itemStack);
            }
            return;
        }
        if (user.getLocation().getBlockY() <= 0) {
            SpectatorManager.spectatorPlayer(user, true);
        } else {
            SpectatorManager.spectatorPlayer(user, false);
        }
        new Title(user.translate("game-you-died"), "", 0, 2, 1).send(user.getBukkitPlayer());
    }
}