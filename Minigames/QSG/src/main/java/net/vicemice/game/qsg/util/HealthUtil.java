package net.vicemice.game.qsg.util;

public class HealthUtil {
    public static double roundToHalf(double x) {
        return (Math.ceil(x * 2) / 2);
    }
}