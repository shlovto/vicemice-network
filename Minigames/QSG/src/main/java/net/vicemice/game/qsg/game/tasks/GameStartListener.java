package net.vicemice.game.qsg.game.tasks;

import net.vicemice.game.qsg.game.ScoreboardManager;
import net.vicemice.game.qsg.player.PlayerManager;
import net.vicemice.game.qsg.player.spectator.SpectatorScoreboard;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.gameapi.events.GameStartEvent;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.gameapi.util.Title;
import net.vicemice.game.qsg.util.MessageUtils;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class GameStartListener implements Listener {

    private boolean setted = false;

    @EventHandler
    public void onStart(GameStartEvent event) {
        if (!setted) {
            GameManager.setStartTime((System.currentTimeMillis()+TimeUnit.SECONDS.toMillis(GameManager.getMapCount())));
            setted = true;
        }

        GameManager.updateScoreboardTime();
        if (GameManager.getMapCount() == 0) {
            GameManager.setGameState(GameState.INGAME);
            for (IUser user : UserManager.getConnectedUsers().values()) {
                user.getBukkitPlayer().setLevel(0);
                user.getBukkitPlayer().setExp(0);
                user.playSound(Sound.NOTE_PLING, 40, 1);
                if (PlayerManager.getPlayerDatas().containsKey(user)) {
                    ScoreboardManager.updateTimeName(PlayerManager.getPlayerData(user));
                }
            }
            SpectatorScoreboard.updateTimeName();

            UserManager.getConnectedUsers().values().forEach((o) -> {
                o.sendMessage("game-round-start", o.translate("qsg-prefix"));
            });
            for (Locale locale : GoServer.getLocaleManager().getLocales().keySet()) {
                GameAPI.sendReplayMessage(MessageFormat.format(GoServer.getLocaleManager().getMessage(locale, "game-round-start"), GoServer.getLocaleManager().getPrivateMessage(locale, "qsg-prefix")), locale);
            }
        } else {
            if (GameManager.getMapCount() == 1) {
                UserManager.getConnectedUsers().values().forEach((o) -> o.sendMessage("game-round-start-last-msg", o.translate("qsg-prefix"), GameManager.getMapCount()));
                for (Locale locale : GoServer.getLocaleManager().getLocales().keySet()) {
                    GameAPI.sendReplayMessage(MessageFormat.format(GoServer.getLocaleManager().getMessage(locale, "game-round-start-last-msg"), GoServer.getLocaleManager().getPrivateMessage(locale, "qsg-prefix"), GameManager.getMapCount()), locale);
                }
            } else {
                UserManager.getConnectedUsers().values().forEach((o) -> o.sendMessage("game-round-start-msg", o.translate("qsg-prefix"), GameManager.getMapCount()));
                for (Locale locale : GoServer.getLocaleManager().getLocales().keySet()) {
                    GameAPI.sendReplayMessage(MessageFormat.format(GoServer.getLocaleManager().getMessage(locale, "game-round-start-msg"), GoServer.getLocaleManager().getPrivateMessage(locale, "qsg-prefix"), GameManager.getMapCount()), locale);
                }
            }
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.setLevel(GameManager.getMapCount());
                player.setExp(GameManager.getMapCount() * 0.3333333333333333F);
                player.playSound(player.getLocation(), Sound.NOTE_BASS, 40, 1);
            }
            ChatColor chatColor = null;
            if (GameManager.getMapCount() == 3) {
                chatColor = ChatColor.RED;
            } else if (GameManager.getMapCount() == 2) {
                chatColor = ChatColor.YELLOW;
            } else if (GameManager.getMapCount() == 1) {
                chatColor = ChatColor.GREEN;
            }
            Title title = new Title(chatColor + "" + GameManager.getMapCount(), "", 0, 1, 1);
            Bukkit.getOnlinePlayers().forEach(title::send);
            GameManager.setMapCount((GameManager.getMapCount()-1));
        }
    }
}