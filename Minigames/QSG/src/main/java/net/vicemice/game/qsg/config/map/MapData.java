package net.vicemice.game.qsg.config.map;

import lombok.Getter;
import net.vicemice.hector.server.utils.LocationSerializer;
import org.bukkit.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Collections;
import java.util.List;

/*
 * Class created at 16:16 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class MapData {

    @Getter
    private String name;
    @Getter
    private String displayName;
    @Getter
    private String creator;
    @Getter
    private String world;
    @Getter
    private String displayItem;

    @Getter
    private List<String> spawns;
    @Getter
    private ItemStack itemStack;


    public MapData(String name, String displayName, String creator, String world, String displayItem, List<String> spawns) {
        this.name = name;
        this.displayName = displayName;
        this.creator = creator;
        this.world = world;
        this.displayItem = displayItem;
        this.spawns = spawns;

        if (this.displayItem == null) {
            this.displayItem = "2;0";
        }

        String[] array = this.displayItem.split(";");
        Material id = Material.valueOf(array[0]);
        int data = Integer.parseInt(array[1]);

        this.itemStack = new ItemStack(id, 1, (short) data);
        ItemMeta meta = this.itemStack.getItemMeta();
        meta.setDisplayName("§6" + this.displayName);
        this.itemStack.setItemMeta(meta);
    }

    /*public MapData(String name, String displayName, String creator, String world, String displayItem, List<String> spawns, List<String> tester) {
        this.name = name;
        this.displayName = displayName;
        this.creator = creator;
        this.world = world;
        this.displayItem = displayItem;
        this.spawns = spawns;
        this.tester = tester;

        if (this.displayItem == null) {
            this.displayItem = "2;0";
        }

        String[] array = this.displayItem.split(";");
        int id = Integer.parseInt(array[0]);
        int data = Integer.parseInt(array[1]);

        this.itemStack = new ItemStack(id, 1, (short) data);
        ItemMeta meta = this.itemStack.getItemMeta();
        meta.setDisplayName("§6" + this.displayName);
        this.itemStack.setItemMeta(meta);
    }*/

    public QSGMap getCreatedMap() {
        try {

            WorldCreator worldCreator = new WorldCreator(this.world);
            Bukkit.getServer().createWorld(worldCreator);

            World world = Bukkit.getWorld(this.world);
            world.setGameRuleValue("mobGriefing", "false");
            world.setGameRuleValue("doDaylightCycle", "false");
            world.setAutoSave(false);
            world.setDifficulty(Difficulty.EASY);
            world.setTime(1000L);
            world.setThundering(false);
            world.setStorm(false);
            world.setPVP(true);

            QSGMap map = new QSGMap(world, this.name, this.displayName, this.creator);

            map.getSpawns().addAll(LocationSerializer.fromList(this.getSpawns()));
            Collections.shuffle(map.getSpawns());

            return map;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}