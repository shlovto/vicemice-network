package net.vicemice.game.qsg.listener;

import net.vicemice.game.qsg.player.spectator.SpectatorManager;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class EntityDamageListener implements Listener {

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        if (GameManager.getGameState() != GameState.INGAME) {
            event.setCancelled(true);
            return;
        }
        if (event.getEntity() instanceof Player) {
            if (GameManager.isPeace()) {
                event.setCancelled(true);
                return;
            }
            IUser user = UserManager.getUser((Player) event.getEntity());
            if (SpectatorManager.getSpectatorPlayers().contains(user)) {
                event.setCancelled(true);
            }
        }
    }
}