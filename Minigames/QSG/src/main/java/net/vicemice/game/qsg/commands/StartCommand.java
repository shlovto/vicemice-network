package net.vicemice.game.qsg.commands;

import net.vicemice.game.qsg.QSG;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.hector.utils.Rank;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StartCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Player player = (Player) commandSender;
        PlayerRankData playerRankData = GoAPI.getUserAPI().getRankData(player.getUniqueId());
        if (playerRankData.getAccess_level() < Rank.VIP.getAccessLevel()) {
            player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-no-permission"));
            return true;
        }

        if (GameManager.getGameState() == GameState.LOBBY) {
            if (Bukkit.getOnlinePlayers().size() >= QSG.getGameConfig().getTO_START()) {
                if (GameManager.getLobbyCount() > 15) {
                    GameManager.setLobbyCount(15);
                    player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-start-success"));
                } else {
                    player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-start-already-started"));
                }
            } else {
                player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-start-at-least", QSG.getGameConfig().getTO_START()));
            }
        } else {
            player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-start-not-in-lobby-phase"));
        }

        return true;
    }
}