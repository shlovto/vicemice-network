package net.vicemice.game.qsg.util;

import lombok.Getter;
import net.vicemice.hector.packets.types.player.stats.ReceivePlayerStatsPacket;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class StatsData {
    @Getter
    private Player player;
    @Getter
    private ReceivePlayerStatsPacket receivePlayerStatsPacket;

    public StatsData(Player player, ReceivePlayerStatsPacket receivePlayerStatsPacket) {
        this.player = player;
        this.receivePlayerStatsPacket = receivePlayerStatsPacket;

        Bukkit.getLogger().info("StatsData opened!");
    }

    public void loadStatsHolo() {
        /*Hologram hologram = new Hologram(new Location(Bukkit.getWorld("lobby"), -124.5, 142.5, 220.5));
        hologram.addItemLine(new ItemStack(Material.GRASS));
        hologram.addTextLine("§eSkyWars §6Statistiken");
        hologram.addTextLine("§7Deine §cAll-Time §6Statistiken");

        String ranked = "Unranked";
        String points = "0";
        String kills = "0";
        String deaths = "0";
        String played = "0";
        String winned = "0";

        try {
            if(!String.valueOf(getGlobalRank("sw")).equalsIgnoreCase("0") || (!String.valueOf(getGlobalRank("sw")).equalsIgnoreCase("-1"))) {
                ranked = String.valueOf(getGlobalRank("sw"));
            }
            points = String.valueOf(getGlobalStatValue(QSG.getStatsPrefix() + "points"));
            kills = String.valueOf(getGlobalStatValue(QSG.getStatsPrefix() + "kills"));
            deaths = String.valueOf(getGlobalStatValue(QSG.getStatsPrefix() + "deaths"));
            played = String.valueOf(getGlobalStatValue(QSG.getStatsPrefix() + "played_games"));
            winned = String.valueOf(getGlobalStatValue(QSG.getStatsPrefix() + "wins"));
        } catch (Exception e) {
            Bukkit.getLogger().info("Statistiken von " + player.getName() + " konnten nicht komplett abgerufen werden");
            Bukkit.getLogger().info(e.getLocalizedMessage());
        }

        hologram.addTextLine("§7Ranked: §e%RANKED%".replace("%RANKED%", ranked));
        hologram.addTextLine("§7Punkte: §e%POINTS%".replace("%POINTS%", points));
        hologram.addTextLine("§7Kills: §e%KILLS%".replace("%KILLS%", kills));
        hologram.addTextLine("§7Deaths: §e%DEATHS%".replace("%DEATHS%", deaths));
        hologram.addTextLine("§7Gespielte Spiele: §e%PLAYED_GAMES%".replace("%PLAYED_GAMES%", played));
        hologram.addTextLine("§7Gewonnene Spiele: §e%WINNED_GAMES%".replace("%WINNED_GAMES%", winned));

        HologramManager.addPlayerToHologram(player, hologram);*/
    }

    public long getDailyRank(String key) {
        if (receivePlayerStatsPacket.getDailyRanking().containsKey(key)) {
            return receivePlayerStatsPacket.getDailyRanking().get(key);
        }
        return 0;
    }

    public long getMonthlyRank(String key) {
        if (receivePlayerStatsPacket.getMonthlyRanking().containsKey(key)) {
            return receivePlayerStatsPacket.getMonthlyRanking().get(key);
        }
        return 0;
    }

    public long getGlobalRank(String key) {
        if (receivePlayerStatsPacket.getGlobalRanking().containsKey(key)) {
            return receivePlayerStatsPacket.getGlobalRanking().get(key);
        }
        return 0;
    }

    public long getDailyStatValue(String key) {
        if (receivePlayerStatsPacket.getDailyStats().containsKey(key)) {
            return receivePlayerStatsPacket.getDailyStats().get(key);
        }
        return 0;
    }

    public long getMonthlyStatsValue(String key) {
        if (receivePlayerStatsPacket.getMonthlyStats().containsKey(key)) {
            return receivePlayerStatsPacket.getMonthlyStats().get(key);
        }
        return 0;
    }

    public long getGlobalStatValue(String key) {
        if (receivePlayerStatsPacket.getGlobalStats().containsKey(key)) {
            return receivePlayerStatsPacket.getGlobalStats().get(key);
        }
        return 0;
    }
}