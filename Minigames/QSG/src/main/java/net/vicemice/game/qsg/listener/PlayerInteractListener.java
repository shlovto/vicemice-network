package net.vicemice.game.qsg.listener;

import net.vicemice.game.qsg.player.spectator.SpectatorManager;
import net.vicemice.game.qsg.util.FlintUtil;
import net.vicemice.game.qsg.util.map.ForceMapInventory;
import net.vicemice.game.qsg.util.map.MapVotingInventory;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerInteractListener implements Listener {

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		IUser user = UserManager.getUser(event.getPlayer());

		if (GameManager.getGameState() == GameState.LOBBY) {
			if (event.getAction().equals(Action.PHYSICAL) && event.getClickedBlock().getType().equals(Material.GOLD_PLATE) && event.getClickedBlock().getRelative(BlockFace.DOWN).getType().equals(Material.QUARTZ_BLOCK)) {
				event.setCancelled(false);
			} else {
				event.setCancelled(true);
			}

			ItemStack hand = user.getBukkitPlayer().getItemInHand();
			if (hand != null) {
				if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
					if (hand.getType() == Material.PAPER) {
						new MapVotingInventory().create(user).open();
					} else if (hand.getType() == Material.CHEST) {
						new ForceMapInventory().create(user, 1).open();
					}
				}
			}
		} else if (GameManager.getGameState() == GameState.INGAME) {
			ItemStack hand = user.getBukkitPlayer().getItemInHand();

			if (SpectatorManager.getSpectatorPlayers().contains(user)) {
				event.setCancelled(true);
				if (hand != null) {
					if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
						if (hand.getType() == Material.COMPASS) {
							user.getBukkitPlayer().openInventory(SpectatorManager.getSpectatorInventory());
						} else if (hand.getType() == Material.WATCH) {
							user.getBukkitPlayer().kickPlayer("lobby");
						}
					}
				}
				return;
			}
			if (hand != null && hand.getType() == Material.FLINT_AND_STEEL) {
				short durability = FlintUtil.getFlintAndSteel().getDurability();
				if (hand.getDurability() > durability) {
					hand.setDurability(FlintUtil.getFlintAndSteel().getDurability());
				}
			}
			/*if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_BLOCK) {
				if (event.getClickedBlock().getType() == Material.CHEST && !ChestManager.getPlacedChests().contains(event.getClickedBlock().getLocation())) {
					Chest chest = (Chest) event.getClickedBlock().getState();
					ChestManager.prepareChest(chest.getBlockInventory(), player);
				}
			}*/
		} else if (GameManager.getGameState() == GameState.END) {
			event.setCancelled(true);
			ItemStack hand = user.getBukkitPlayer().getItemInHand();
			if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
				if (hand.getType() == Material.WATCH) {
					user.getBukkitPlayer().kickPlayer("lobby");
				}
			}
		} else {
			event.setCancelled(true);
		}
	}
}
