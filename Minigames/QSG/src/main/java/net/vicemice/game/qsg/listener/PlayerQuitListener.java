package net.vicemice.game.qsg.listener;

import net.vicemice.game.qsg.game.VotingManager;
import net.vicemice.game.qsg.player.team.TeamManager;
import net.vicemice.game.qsg.util.PlayerDeathManager;
import net.vicemice.game.qsg.player.LobbyScoreboardManager;
import net.vicemice.game.qsg.player.PlayerManager;
import net.vicemice.game.qsg.player.spectator.SpectatorManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Locale;

public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        IUser user = UserManager.getUser(event.getPlayer());
        event.setQuitMessage("");

        String playerName = (user.isNicked() ? "§9"+user.getNickName() : user.getRank().getRankColor() + user.getName());

        if (GameManager.getGameState() == GameState.LOBBY) {
            VotingManager.removePlayerVotes(user);
            UserManager.getConnectedUsers().values().forEach(p -> p.sendMessage("game-quit", playerName));
            GoServer.getLocaleManager().getLocales().forEach((k, v) -> GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "game-quit", playerName), k));
            TeamManager.removePlayerFromTeams(user);
            LobbyScoreboardManager.removeBoard(user);
        } else if (GameManager.getGameState() == GameState.INGAME || GameManager.getGameState() == GameState.WARMUP) {
            if (PlayerManager.getPlayerDatas().containsKey(user)) {
                PlayerDeathManager.playerDeath(user, true);
                UserManager.getConnectedUsers().values().forEach(p -> p.sendMessage("game-quit-ingame-without-teamsize", p.translate("qsg-prefix"), playerName));
                GoServer.getLocaleManager().getLocales().forEach((k, v) -> GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "game-quit-ingame-without-teamsize", GoServer.getLocaleManager().getMessage(k, "qsg-prefix"), playerName), k));
            } else {
                System.out.println(user.getName() + " leaved as spectator!");
                SpectatorManager.getSpectatorPlayers().remove(user);
            }
        }
        SpectatorManager.playerQuit(user);
    }
}