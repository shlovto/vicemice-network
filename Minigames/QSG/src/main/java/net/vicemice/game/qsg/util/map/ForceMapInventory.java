package net.vicemice.game.qsg.util.map;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.config.map.vote.VoteState;
import net.vicemice.game.qsg.game.VotingManager;
import net.vicemice.game.qsg.player.LobbyScoreboardManager;
import net.vicemice.game.qsg.player.PlayerData;
import net.vicemice.game.qsg.player.PlayerManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.players.utils.Pagination;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class ForceMapInventory {
    public GUI create(IUser user, int page) {
        Pagination<String> pagination = new Pagination<>(new ArrayList<>(QSG.getMapManager().getMapDatas().keySet()), 27);
        GUI inventory = user.createGUI("Force Map", (pagination.getPages() > 1 ? 6 : 5));

        inventory.fill(0, 9, user.getUIColor());
        inventory.fill(36, 45, user.getUIColor());

        if (!pagination.getElementsFor((page-1)).isEmpty()) {
            inventory.setItem(48, ItemBuilder.create(GUI.Items.getLeft()).name(user.translate("previous-page")).build(), e -> {
                this.create(user, (page-1)).open();
            });
        }
        if (!pagination.getElementsFor((page+1)).isEmpty()) {
            inventory.setItem(50, ItemBuilder.create(GUI.Items.getRight()).name(user.translate("next-page")).build(), e -> {
                this.create(user, (page+1)).open();
            });
        }

        AtomicInteger i = new AtomicInteger(9);
        pagination.printPage(page, s -> {
            inventory.setItem(i.get(), ItemBuilder.create(QSG.getMapManager().getMapDatas().get(s).getItemStack()).name("§7"+QSG.getMapManager().getMapDatas().get(s).getDisplayName()).lore("§8➥ §7Creator: §e" + QSG.getMapManager().getMapDatas().get(s).getCreator()).build(), e -> {
                user.getBukkitPlayer().closeInventory();
                QSG.getGameConfig().setVOTING(false);
                VotingManager.setVoteState(VoteState.OFF);
                QSG.getGame().setQSGMap(QSG.getMapManager().getMapDatas().get(s).getCreatedMap());
                GoAPI.getServerAPI().changeServer(ServerState.LOBBY, QSG.getGame().getQSGMap().getDisplayName()+" - "+QSG.getGameConfig().getTYPE());
                LobbyScoreboardManager.updateAllMap();
                user.sendMessage("force-map-success", user.translate("qsg-prefix"), QSG.getGame().getQSGMap().getDisplayName());

                Bukkit.getOnlinePlayers().forEach(o -> {
                    o.closeInventory();
                    o.getInventory().remove(Material.PAPER);
                });
            });
            i.incrementAndGet();
        });

        return inventory;
    }
}