package net.vicemice.game.qsg.util;

import net.vicemice.game.qsg.QSG;
import io.netty.channel.Channel;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.stats.ReceivePlayerStatsPacket;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class StatsListener extends PacketGetter {

    @Override
    public void receivePacket(PacketHolder packetHolder) {
        if (packetHolder.getKey().equals(PacketType.RECEIVE_PLAYER_STATS.getKey())) {
            Bukkit.getServer().getScheduler().runTask(QSG.getInstance(), () -> {
                Bukkit.getLogger().info("RECEIVE PLAYER STATS PACKET!");
                ReceivePlayerStatsPacket receivePlayerStatsPacket = (ReceivePlayerStatsPacket) packetHolder.getValue();
                String name = receivePlayerStatsPacket.getForPlayer();
                Player player = Bukkit.getServer().getPlayer(name);
                if (player == null) return;
                if (!QSG.getRequestedStats().contains(player)) {
                    Bukkit.getLogger().info("PLAYER DONT REQUEST!");
                    return;
                }
                QSG.getRequestedStats().remove(player);
                if(!DataManager.getPlayerDatas().containsKey(player)) {
                    DataManager.getPlayerDatas().put(player, new StatsData(player, receivePlayerStatsPacket));
                }
            });
        }
    }

    @Override
    public void channelActive(Channel channel) {

    }
}