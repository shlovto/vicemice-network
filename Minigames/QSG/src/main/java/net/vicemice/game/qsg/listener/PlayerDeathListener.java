package net.vicemice.game.qsg.listener;

import net.vicemice.game.qsg.util.PlayerDeathManager;
import net.vicemice.hector.server.player.UserManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerDeathListener implements Listener {

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        event.setDeathMessage("");
        if (event.getEntity().getLocation().getBlockY() <= 0) {
            event.getDrops().clear();
            event.setDroppedExp(0);
        }
        PlayerDeathManager.playerDeath(UserManager.getUser(player), false);
    }
}