package net.vicemice.game.qsg.listener;

import net.vicemice.game.qsg.player.spectator.SpectatorManager;
import net.vicemice.hector.server.player.UserManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.vehicle.VehicleDestroyEvent;

public class VehicleDestroyListener implements Listener {

    @EventHandler
    public void onVehicleDestroy(VehicleDestroyEvent event) {
        if (event.getAttacker() instanceof Player) {
            Player player = (Player) event.getAttacker();
            if (SpectatorManager.getSpectatorPlayers().contains(UserManager.getUser(player))) {
                event.setCancelled(true);
            }
        }
    }
}