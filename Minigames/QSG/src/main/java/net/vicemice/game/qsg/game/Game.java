package net.vicemice.game.qsg.game;

import net.vicemice.game.qsg.config.map.QSGMap;
import net.vicemice.game.qsg.player.team.GameTeam;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.scoreboard.Team;
import java.util.HashMap;

/*
 * Class created at 16:15 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class Game {

    @Getter
    @Setter
    private QSGMap QSGMap;
    @Getter
    private boolean op;
    @Getter
    private HashMap<GameTeam, Team> teams = new HashMap<>();

}