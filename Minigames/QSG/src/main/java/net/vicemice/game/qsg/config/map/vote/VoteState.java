package net.vicemice.game.qsg.config.map.vote;

/*
 * Class created at 16:19 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public enum VoteState {
    ON,
    OFF
}