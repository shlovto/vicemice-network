package net.vicemice.game.qsg.game.tasks;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.player.spectator.SpectatorScoreboard;
import net.vicemice.hector.gameapi.events.GameIngameEvent;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.mctv.MCTV;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class GameIngameListener implements Listener {

    @EventHandler
    public void onIngame(GameIngameEvent event) {
        if (Bukkit.getOnlinePlayers().size() == 0) {
            Bukkit.getServer().shutdown();
        }
        if (GameManager.getReplayStartSeconds() == 0) {
            GameManager.setReplayStartSeconds(MCTV.getInstance().getApi().getRecordedSeconds());
        }

        GameManager.setPeace(false);
        GameManager.updateScoreboardTime();
        SpectatorScoreboard.updateTime();
        QSG.getGame().getQSGMap().getWorld().setStorm(false);
        QSG.getGame().getQSGMap().getWorld().setThundering(false);
    }
}