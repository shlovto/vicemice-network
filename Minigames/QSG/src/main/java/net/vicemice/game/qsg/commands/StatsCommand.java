package net.vicemice.game.qsg.commands;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.StatisticPeriod;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.util.concurrent.atomic.AtomicLong;

public class StatsCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player)sender;

        AtomicLong rank = new AtomicLong();
        AtomicLong points = new AtomicLong();
        AtomicLong kills = new AtomicLong();
        AtomicLong deaths = new AtomicLong();
        AtomicLong kd = new AtomicLong();
        AtomicLong games = new AtomicLong();
        AtomicLong wins = new AtomicLong();

        rank.set(GoAPI.getUserAPI().getPlayerRanking(player.getUniqueId(), "qsg", StatisticPeriod.MONTH));
        points.set(GoAPI.getUserAPI().getPlayerStatistic(player.getUniqueId(), "qsg_points", StatisticPeriod.MONTH));
        kills.set(GoAPI.getUserAPI().getPlayerStatistic(player.getUniqueId(), "qsg_kills", StatisticPeriod.MONTH));
        deaths.set(GoAPI.getUserAPI().getPlayerStatistic(player.getUniqueId(), "qsg_deaths", StatisticPeriod.MONTH));
        if (kills.get() != 0 && deaths.get() != 0) {
            kd.set((kills.get()/deaths.get()));
        } else {
            kd.set(0);
        }
        games.set(GoAPI.getUserAPI().getPlayerStatistic(player.getUniqueId(), "qsg_played_games", StatisticPeriod.MONTH));
        wins.set(GoAPI.getUserAPI().getPlayerStatistic(player.getUniqueId(), "qsg_wins", StatisticPeriod.MONTH));

        player.sendMessage(GoAPI.getUserAPI().translate(player.getUniqueId(), "command-stats-success",
                GoAPI.getUserAPI().getRankData(player.getUniqueId()).getRankColor()+player.getName(),
                rank.get(),
                points.get(),
                kills.get(),
                deaths.get(),
                kd.get(),
                games.get(),
                wins.get()));

        return false;
    }
}
