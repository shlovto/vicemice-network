package net.vicemice.game.qsg.commands;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.player.team.TeamManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.hector.utils.Rank;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AdminCommand implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Player player = (Player) sender;
		PlayerRankData playerRankData = GoAPI.getUserAPI().getRankData(player.getUniqueId());

		if (playerRankData.getAccess_level() < Rank.ADMIN.getAccessLevel()) {
			player.sendMessage("§cI'm sorry, but you do not have permission to perform this command. Please contact the server administrators if you believe that this is in error.");
			return false;
		}

		if (args.length == 0) {
			player.sendMessage("§7QSG §6Admin §7Commands");
			player.sendMessage("§7Argumente§8:");
			player.sendMessage("§8- §7beta §8| §aSetzt den Server in BetaMode");
			player.sendMessage("§8- §7tostart §8| §aSetzt die mindestanzahl an Spielern");
			player.sendMessage("§8- §7count §8| §aSetzt die Zeit vom LobbyCountdown");
			player.sendMessage("§8- §7stats §8| §aDeaktiviert/Aktiviert den TestModus");
			player.sendMessage("§8- §7ca §8| §aCheck alle noch lebenden Spieler");
			player.sendMessage("§8- §7op §8| §aGibt ein OP");

			return false;
		}

		if (args[0].equalsIgnoreCase("gameState")) {
			player.sendMessage("GameState -> " + GameManager.getGameState().name());
		} else if (args[0].equalsIgnoreCase("tostart")) {
			QSG.getGameConfig().setTO_START(Integer.valueOf(args[1]));
		} else if (args[0].equalsIgnoreCase("count")) {
			GameManager.setLobbyCount(Integer.valueOf(args[1]));
		} else if (args[0].equalsIgnoreCase("op")) {
			Bukkit.getPlayer(args[1]).setOp(true);
		} else if (args[0].equalsIgnoreCase("beta")) {
			if (QSG.isBetaMode()) {
				QSG.setBetaMode(false);
				sender.sendMessage("§cDu hast den Beta Modus erfolgreich deaktiviert!");
			} else {
				QSG.setBetaMode(true);
				sender.sendMessage("§aDu hast den Beta Modus erfolgreich aktiviert!");
			}
			GameManager.setLobbyCount(Integer.parseInt(args[1]));
		} else if (args[0].equalsIgnoreCase("stats")) {
			if (QSG.isTestMode()) {
				QSG.setTestMode(false);
				sender.sendMessage("§cDu hast den Test Modus erfolgreich deaktiviert! §8(§aStats werden nun gespeichert!§8)");
			} else {
				QSG.setTestMode(true);
				sender.sendMessage("§cDu hast den Test Modus erfolgreich aktiviert! §8(§4Stats werden nicht gespeichert!§8)");
			}
		} else if (args[0].equalsIgnoreCase("ca")) {
			if (GameManager.getGameState() == GameState.PREPARING || net.vicemice.hector.gameapi.manager.GameManager.getGameState() == GameState.LOBBY || GameManager.getGameState() == GameState.RESTART) {
				return true;
			}

			TeamManager.checkAliveTeams();
		}
		return true;
	}
}