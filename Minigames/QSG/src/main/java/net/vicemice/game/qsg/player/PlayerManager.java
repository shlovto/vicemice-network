package net.vicemice.game.qsg.player;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.player.spectator.SpectatorManager;
import net.vicemice.game.qsg.player.team.GameTeam;
import net.vicemice.game.qsg.player.team.TeamManager;
import net.vicemice.game.qsg.game.ScoreboardManager;
import lombok.Getter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import java.util.HashMap;

public class PlayerManager {
	@Getter
	private static HashMap<IUser, PlayerData> playerDatas = new HashMap<>();

	public static PlayerData getPlayerData(IUser user) {
		return playerDatas.get(user);
	}

	public static void startGameFor(IUser user) {
		PlayerManager.resetPlayer(user);
		GameTeam targetTeam = TeamManager.getPlayersTeam(user);
		if (targetTeam == null) {
			for (int slot : TeamManager.getTeams().keySet()) {
				GameTeam forTeam = TeamManager.getTeams().get(slot);
				if (forTeam.getMembers().size() < QSG.getGameConfig().getPER_TEAM()) {
					targetTeam = forTeam;
					break;
				}
			}
			if (targetTeam == null) {
				SpectatorManager.spectatorPlayer(user, true);
				//NO-TEAM-FOUND
				return;
			} else {
				targetTeam.playerJoin(user);
			}
		} else {
			//YOU-ARE-IN-TEAM
		}

		int teleportCount = targetTeam.getNumber();
		if (QSG.getGame().getQSGMap().getSpawns().size() >= targetTeam.getNumber()) {
			PlayerData playerData = new PlayerData(user, targetTeam);
			if (!TeamManager.getAliveTeams().contains(targetTeam)) {
				TeamManager.getAliveTeams().add(targetTeam);
			}
			playerDatas.put(user, playerData);
			user.getBukkitPlayer().spigot().setCollidesWithEntities(true);
			playerData.addPlayedGame();
			targetTeam.setSpawnLocation(QSG.getGame().getQSGMap().getSpawns().get(teleportCount));

			user.teleport(QSG.getGame().getQSGMap().getSpawns().get(teleportCount));
			ScoreboardManager.getTeams().get(targetTeam).addEntry(user.getName());
			if (user.isNicked()) {
				ScoreboardManager.getTeams().get(targetTeam).addEntry(user.getNickName());
			}
			ScoreboardManager.setupScoreboard(playerData);
			if (QSG.getGameConfig().getPER_TEAM() > 1)
				user.getBukkitPlayer().setScoreboard(ScoreboardManager.getScoreboard());
		} else {
			user.sendRawMessage("§cAn internal error occurred");

			SpectatorManager.spectatorPlayer(user, true);
		}
	}

	public static void resetPlayer(IUser user) {
		user.getBukkitPlayer().setGameMode(GameMode.SURVIVAL);
		user.getBukkitPlayer().getInventory().clear();
		user.getBukkitPlayer().getInventory().setArmorContents(null);
		user.getBukkitPlayer().setHealth(20);
		user.getBukkitPlayer().setFoodLevel(20);
		user.getBukkitPlayer().setExp(0);
		user.getBukkitPlayer().setAllowFlight(false);
		for (PotionEffect potionEffect : user.getBukkitPlayer().getActivePotionEffects()) {
			user.getBukkitPlayer().removePotionEffect(potionEffect.getType());
		}
	}
}