package net.vicemice.game.qsg.config.map;

import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.ArrayList;
import java.util.List;

/*
 * Class created at 16:19 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class QSGMap {

    @Getter
    private World world;
    @Getter
    private String name, displayName, creator;
    @Getter
    private List<Location> spawns = new ArrayList<>();

    public QSGMap(World world, String name, String displayName, String creator) {
        this.world = world;
        this.name = name;
        this.displayName = displayName;
        this.creator = creator;
    }
}