package net.vicemice.game.qsg.listener;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.player.spectator.SpectatorManager;
import net.vicemice.game.qsg.player.PlayerManager;
import java.util.Arrays;
import java.util.List;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.gameapi.util.EloRank;
import net.vicemice.hector.server.player.permissions.listener.CloudChatEvent;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class CloudChatListener implements Listener {
	private static final List<String> allPrefix = Arrays.asList("@all ", "@all", "@a ", "@a", "@ ", "@");

	@EventHandler
	public void onAsyncPlayerChat(CloudChatEvent event) {
		IUser user = event.getUser();

		if (GameManager.getGameState() == GameState.LOBBY && QSG.getGameConfig().isRANKED()) {
			long elo = (user.getStatistic("qsg_elo", StatisticPeriod.MONTH) >= 1 ? user.getStatistic("qsg_elo", StatisticPeriod.MONTH) : 0);
			EloRank rank = EloRank.rankByElo(elo);
			if (rank == null) rank = EloRank.UNRANKED;
			event.setMessage("§7(§e§l"+rank.getColor()+rank.getName()+"§7) "+event.getMessage());
		}

		if (GameManager.getGameState() == GameState.INGAME) {
			event.setCancelled(true);
			if (SpectatorManager.getSpectatorPlayers().contains(user)) {
				spectatorChat(user, event.getRawMessage());
				return;
			}

			for (String prefix : allPrefix) {
				if (QSG.getGameConfig().getPER_TEAM() == 1) {
					sendGlobalMessage(user, event.getRawMessage());
					return;
				}

				if (event.getRawMessage().toLowerCase().startsWith(prefix)) {
					sendGlobalMessage(user, event.getRawMessage().substring(prefix.length()));
					return;
				}
			}

			sendTeamMessage(user, event.getRawMessage());
		}
	}

	public void spectatorChat(IUser user, String message) {
		message = "§7[§6§oTeam§7] "+(user.isNicked() ? "§9"+user.getNickName() : user.getRank().getRankColor()+user.getName()+"§8: §f"+message);
		String finalMessage = message;
		SpectatorManager.getSpectatorPlayers().forEach(players -> {
			players.sendMessage(finalMessage);
		});
		GameAPI.sendReplayMessage(message);
	}

	public void sendGlobalMessage(IUser user, String message) {
		message = "§7[§6§oTeam§7] "+(user.isNicked() ? "§9"+user.getNickName() : user.getRank().getRankColor()+user.getName()+"§8: §f"+message);
		String finalMessage = message;
		Bukkit.getOnlinePlayers().forEach(players -> {
			players.sendMessage(finalMessage);
		});
		GameAPI.sendReplayMessage(message);
	}

	public void sendTeamMessage(IUser user, String message) {
		message = "§7[§6§oTeam§7] "+(user.isNicked() ? "§9"+user.getNickName() : user.getRank().getRankColor()+user.getName()+"§8: §f"+message);
		String finalMessage = message;
		PlayerManager.getPlayerData(user).getGameTeam().getMembers().forEach(players -> {
			players.sendMessage(finalMessage);
		});
		GameAPI.sendReplayMessage(message);
	}
}
