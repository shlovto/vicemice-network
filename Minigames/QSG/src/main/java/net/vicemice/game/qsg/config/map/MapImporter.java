package net.vicemice.game.qsg.config.map;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.config.map.vote.VoteMap;
import lombok.Getter;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.MongoManager;
import org.bson.Document;
import java.util.*;

/*
 * Class created at 16:18 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class MapImporter {

    @Getter
    private static Map<Integer, VoteMap> forceMaps = new HashMap<>();

    public static void importMaps() {
        List<Document> maps = GoServer.getService().getMongoManager().getDocumentsWithFilter(MongoManager.Database.GAMES, "maps", new Document("game", "QSG").append("type", QSG.getGameConfig().getTYPE()));

        for (Document map : maps) {
            String name = map.getString("name");
            String creator = map.getString("creator");
            List<String> spawns = map.getList("spawns", String.class);

            QSG.getMapManager().getMapDatas().put(name.toLowerCase(), new MapData(name.toLowerCase(), name, creator, name.toLowerCase(), "GRASS;0", spawns));
        }
    }

    public static void selectRandomMap() {
        int random = Math.abs(new Random().nextInt()) % QSG.getMapManager().getMapDatas().size();
        MapData mapData = new ArrayList<>(QSG.getMapManager().getMapDatas().values()).get(random);
        QSG.getGame().setQSGMap(mapData.getCreatedMap());
    }
}