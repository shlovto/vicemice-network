package net.vicemice.game.qsg.player.spectator;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.player.PlayerManager;
import lombok.Getter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.player.scoreboard.PacketScoreboard;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import java.util.HashMap;

public class SpectatorScoreboard {
    @Getter
    private static HashMap<IUser, PacketScoreboard> scoreboards = new HashMap<>();

    public static void setupSpectatorScoreboard(IUser user) {
        PacketScoreboard packetScoreboard = new PacketScoreboard(user.getBukkitPlayer());
        packetScoreboard.remove();
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(QSG.getInstance(), () -> {
            scoreboards.put(user, packetScoreboard);
            packetScoreboard.sendSidebar((QSG.getGameConfig().isRANKED() ? "§a§lQSG §f§lRanked" : "§a§lQSG §f§lUnranked"));
            packetScoreboard.setLine(12, "§5");
            packetScoreboard.setLine(11, "§b§l" + user.translate("teams"));
            if (QSG.getGameConfig().isTEAMING()) {
                packetScoreboard.setLine(10, "§a" + user.translate("allowed"));
            } else {
                packetScoreboard.setLine(10, "§c" + user.translate("forbidden"));
            }
            packetScoreboard.setLine(9, "§4");
            packetScoreboard.setLine(8, "§b§l" + user.translate("players"));
            packetScoreboard.setLine(7, "§7" + PlayerManager.getPlayerDatas().size());
            packetScoreboard.setLine(6, "§3");
            packetScoreboard.setLine(5, "§b§l" + user.translate("map"));
            packetScoreboard.setLine(4, "§7" + QSG.getGame().getQSGMap().getDisplayName());
            packetScoreboard.setLine(3, "§2");
            packetScoreboard.setLine(2, "§b§l" + (GameManager.getGameState() == GameState.INGAME ? user.translate("round-time") : user.translate("starting-in")));
            packetScoreboard.setLine(1, "§7" + (GameManager.getGameState() == GameState.INGAME ? GameManager.getTime(GameManager.getStartTime()) : "00:0"+GameManager.getMapCount()));
            packetScoreboard.setLine(0, "§1");
        }, 10L);
    }

    public static void updateTime() {
        for (IUser user : scoreboards.keySet()) {
            PacketScoreboard packetScoreboard = scoreboards.get(user);
            packetScoreboard.updateLine(1, "§7" + (GameManager.getGameState() == GameState.INGAME ? GameManager.getTime(GameManager.getStartTime()) : "00:0"+GameManager.getMapCount()));
        }
    }

    public static void updateTimeName() {
        for (IUser user : scoreboards.keySet()) {
            PacketScoreboard packetScoreboard = scoreboards.get(user);
            packetScoreboard.updateLine(2, "§b§l" + user.translate("round-time"));
        }
    }

    public static void updatePlayers() {
        for (IUser user : scoreboards.keySet()) {
            PacketScoreboard packetScoreboard = scoreboards.get(user);
            packetScoreboard.updateLine(7, "§7" + PlayerManager.getPlayerDatas().size());
        }
    }
}