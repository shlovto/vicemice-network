package net.vicemice.game.qsg.listener;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.game.ScoreboardManager;
import net.vicemice.game.qsg.player.PlayerData;
import net.vicemice.game.qsg.player.PlayerManager;
import net.vicemice.game.qsg.player.spectator.SpectatorManager;
import net.vicemice.hector.events.PlayerNickEvent;
import net.vicemice.hector.events.PlayerUnnickEvent;
import net.vicemice.hector.events.PlayerUnnickedEvent;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class NickListener implements Listener {
    @EventHandler
    public void onPlayerNick(PlayerNickEvent event) {
        IUser user = event.getUser();
        Bukkit.getScheduler().runTask(QSG.getInstance(), () -> {
            if (SpectatorManager.getSpectatorPlayers().contains(user)) {
                for (IUser getPlayer : PlayerManager.getPlayerDatas().keySet()) {
                    getPlayer.getBukkitPlayer().hidePlayer(user.getBukkitPlayer());
                }
            } else if (PlayerManager.getPlayerDatas().containsKey(user)) {
                PlayerData playerData = PlayerManager.getPlayerData(user);
                ScoreboardManager.getTeams().get(playerData.getGameTeam()).addEntry(event.getNickName());
            }
        });
    }

    @EventHandler
    public void onPlayerUnnick(PlayerUnnickEvent event) {
        IUser user = event.getUser();
        if (GameManager.getGameState() == GameState.LOBBY || GameManager.getGameState() == GameState.END) return;
        Bukkit.getScheduler().runTask(QSG.getInstance(), () -> {
            if (SpectatorManager.getSpectatorPlayers().contains(user)) {
                for (IUser getPlayer : PlayerManager.getPlayerDatas().keySet()) {
                    getPlayer.getBukkitPlayer().hidePlayer(user.getBukkitPlayer());
                }
            } else {
                PlayerData playerData = PlayerManager.getPlayerData(user);
                ScoreboardManager.getTeams().get(playerData.getGameTeam()).removeEntry(event.getNickName());
            }
        });
    }

    @EventHandler
    public void onPlayerUnniked(PlayerUnnickedEvent event) {
        IUser user = event.getUser();
        if (GameManager.getGameState() == GameState.LOBBY || GameManager.getGameState() == GameState.END) return;
        Bukkit.getScheduler().runTask(QSG.getInstance(), () -> {
            if (SpectatorManager.getSpectatorPlayers().contains(user)) {
                for (IUser getPlayer : PlayerManager.getPlayerDatas().keySet()) {
                    getPlayer.getBukkitPlayer().hidePlayer(user.getBukkitPlayer());
                }
            }
        });
    }
}