package net.vicemice.game.qsg.game.tasks;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.game.ScoreboardManager;
import net.vicemice.game.qsg.player.LobbyScoreboardManager;
import net.vicemice.game.qsg.player.PlayerManager;
import net.vicemice.game.qsg.player.spectator.SpectatorManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.events.GameStartPrepareEvent;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.replay.ReplayDataPacket;
import net.vicemice.hector.packets.types.player.replay.ReplayServerPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.types.ServerState;
import net.vicemice.game.qsg.util.MessageUtils;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class GameStartPrepareListener implements Listener {

    @EventHandler
    public void onPrepare(GameStartPrepareEvent event) {
        LobbyScoreboardManager.removeAllBoards();
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.playSound(player.getLocation(), Sound.NOTE_PLING, 40, 1);
            GameManager.getReplayPlayers().add(player.getUniqueId());
            GameManager.getIngamePlayers().add(player.getUniqueId());
        }
        GameManager.setStartTime(System.currentTimeMillis());
        GameManager.setGameState(GameState.WARMUP);
        // Load Map
        for (Player player : Bukkit.getOnlinePlayers()) {
            for (Player player2 : Bukkit.getOnlinePlayers()) {
                player.hidePlayer(player2);
                player.showPlayer(player2);
            }
        }
        GoAPI.getServerAPI().changeServer(ServerState.INGAME, QSG.getGame().getQSGMap().getDisplayName() + " " + QSG.getGameConfig().getTYPE());

        UserManager.getConnectedUsers().values().forEach(PlayerManager::startGameFor);
        if (QSG.getGameConfig().getPER_TEAM() > 1)
            GoAPI.getServerAPI().setScoreboard(ScoreboardManager.getScoreboard());

        SpectatorManager.updateSpectatorInventory();
        GoAPI.getServerAPI().setKick(false);

        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.REPLAY_SERVER_PACKET, new ReplayServerPacket(new ReplayDataPacket(GoAPI.getGameIDAPI().getGameUID(), GoAPI.getGameIDAPI().getGameID(), "QSG", QSG.getGame().getQSGMap().getDisplayName(), true), true)), API.PacketReceiver.SLAVE);
    }
}