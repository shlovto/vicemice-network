package net.vicemice.game.qsg.game;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.player.PlayerData;
import net.vicemice.game.qsg.player.PlayerManager;
import net.vicemice.game.qsg.player.team.GameTeam;
import lombok.Getter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.player.scoreboard.PlayerScoreboard;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import java.util.HashMap;

public class ScoreboardManager {
    @Getter
    private static Scoreboard scoreboard;
    @Getter
    private static Team spectatorTeam;
    @Getter
    private static HashMap<GameTeam, Team> teams = new HashMap<>();

    public static void initScoreboards() {
        scoreboard = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
        spectatorTeam = getScoreboard().registerNewTeam("spectator");
        spectatorTeam.setPrefix("§7");
        spectatorTeam.setCanSeeFriendlyInvisibles(true);
    }

    public static void setupScoreboard(PlayerData playerData) {
        PlayerScoreboard playerScoreboard = new PlayerScoreboard(playerData.getUser().getBukkitPlayer());
        playerData.setPlayerScoreboard(playerScoreboard);
        playerScoreboard.setup((QSG.getGameConfig().isRANKED() ? "§a§lQSG §f§lRanked" : "§a§lQSG §f§lUnranked"));

        playerScoreboard.setLine(15, "§5");
        playerScoreboard.setLine(14, "§b§l" + playerData.getUser().translate("teams"));
        if (QSG.getGameConfig().isTEAMING()) {
            playerScoreboard.setLine(13, "§a" + playerData.getUser().translate("allowed"));
        } else {
            playerScoreboard.setLine(13, "§c" + playerData.getUser().translate("forbidden"));
        }
        playerScoreboard.setLine(12, "§4");
        playerScoreboard.setLine(11, "§b§l" + playerData.getUser().translate("map"));
        playerScoreboard.setLine(10, "§7" + QSG.getGame().getQSGMap().getDisplayName());
        playerScoreboard.setLine(9, "§3");
        playerScoreboard.setLine(8, "§b§l" + playerData.getUser().translate("kills"));
        playerScoreboard.setLine(7, "§7" + playerData.getKills());
        playerScoreboard.setLine(6, "§2");
        playerScoreboard.setLine(5, "§b§l" + playerData.getUser().translate("players"));
        playerScoreboard.setLine(4, "§7" + PlayerManager.getPlayerDatas().size());
        playerScoreboard.setLine(3, "§1");
        playerScoreboard.setLine(2, "§b§l" + (GameManager.getGameState() == GameState.INGAME ? playerData.getUser().translate("round-time") : playerData.getUser().translate("starting-in")));
        playerScoreboard.setLine(1, "§7" + (GameManager.getGameState() == GameState.INGAME ? GameManager.getTime(GameManager.getStartTime()) : "00:0"+GameManager.getMapCount()));
        playerScoreboard.setLine(0, "§0");
    }

    public static void updateKills(IUser user) {
        PlayerData playerData = PlayerManager.getPlayerData(user);
        PlayerScoreboard playerScoreboard = playerData.getPlayerScoreboard();
        playerScoreboard.setLine(7, "§7" + playerData.getKills());
    }

    public static void updatePlayers(IUser user) {
        PlayerScoreboard playerScoreboard = PlayerManager.getPlayerData(user).getPlayerScoreboard();
        playerScoreboard.setLine(4, "§7" + PlayerManager.getPlayerDatas().size());
    }

    public static void updateTimeName(PlayerData playerData) {
        PlayerScoreboard playerScoreboard = playerData.getPlayerScoreboard();
        playerScoreboard.setLine(2, "§b§l" +(GameManager.getGameState() == GameState.INGAME ? playerData.getUser().translate("round-time") : playerData.getUser().translate("starting-in")));
    }

    public static void updateTime(PlayerData playerData) {
        PlayerScoreboard playerScoreboard = playerData.getPlayerScoreboard();
        playerScoreboard.setLine(1, "§7" + (GameManager.getGameState() == GameState.INGAME ? GameManager.getTime(GameManager.getStartTime()) : "00:0"+GameManager.getMapCount()));
    }
}