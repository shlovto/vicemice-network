package net.vicemice.game.qsg.game;

import net.vicemice.game.qsg.config.map.MapData;
import net.vicemice.game.qsg.config.map.QSGMap;
import lombok.Getter;
import org.bukkit.Location;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Class created at 15:56 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class MapManager {

    @Getter
    private Map<String, MapData> mapDatas;
    @Getter
    private Map<String, QSGMap> maps;
    @Getter
    private List<Location> spawnPoints;

    public MapManager() {
        this.mapDatas = new HashMap<>();
        this.maps = new HashMap<>();
        this.spawnPoints = new ArrayList<>();
    }
}
