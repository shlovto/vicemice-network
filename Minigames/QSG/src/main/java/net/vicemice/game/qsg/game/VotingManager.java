package net.vicemice.game.qsg.game;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.game.qsg.player.LobbyScoreboardManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.utils.ItemCreator;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.config.map.MapData;
import net.vicemice.game.qsg.config.map.vote.VoteMap;
import net.vicemice.game.qsg.config.map.vote.VoteState;
import net.vicemice.game.qsg.util.MessageUtils;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

/*
 * Class created at 16:14 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class VotingManager {

    @Getter
    private static List<VoteMap> votemaps = new ArrayList<>();
    @Getter
    private static List<MapData> maps = new ArrayList<>();
    @Getter
    private static HashMap<VoteMap, PlayerRankData> forceRank = new HashMap<>();
    @Getter
    @Setter
    private static VoteState voteState = VoteState.ON;
    @Getter
    @Setter
    private static VoteMap forceMap;

    public static void createVoting() {
        for (String name : QSG.getMapManager().getMapDatas().keySet()) {
            MapData mapData = QSG.getMapManager().getMapDatas().get(name);
            maps.add(mapData);
        }
        while (maps.size() != 0 && votemaps.size() < 3) {
            int random = Math.abs(new Random().nextInt()) % maps.size();
            MapData mapData = maps.get(random);
            ItemStack voteItem = mapData.getItemStack();
            votemaps.add(new VoteMap(mapData, voteItem));
            maps.remove(random);
        }
    }

    public static void removePlayerVotes(IUser user) {
        for (VoteMap voteMap : votemaps) {
            if (voteMap.getVotes().contains(user)) {
                voteMap.getVotes().remove(user);
            }
        }
    }

    public static void addPlayerVote(IUser user, int slot) {
        VoteMap voteMap = votemaps.get(slot);
        if (voteMap == null)
            return;
        if (!voteMap.getVotes().contains(user)) {
            removePlayerVotes(user);
            voteMap.getVotes().add(user);
            user.sendMessage("vote-success", user.translate("qsg-prefix"), voteMap.getMapData().getDisplayName());
        } else {
            user.sendMessage("already-voted", user.translate("qsg-prefix"));
        }
    }

    public static void endVoting() {
        if (!QSG.getGameConfig().isVOTING()) return;
        voteState = VoteState.OFF;
        try {
            VoteMap bestMap = null;
            for (VoteMap voteMap : votemaps) {
                if (bestMap == null || voteMap.getVotes().size() > bestMap.getVotes().size()) {
                    bestMap = voteMap;
                }
            }
            if (getForceMap() != null) {
                bestMap = getForceMap();
            }

            for (IUser user : UserManager.getConnectedUsers().values()) {
                user.playSound(Sound.ANVIL_BREAK, 10L, 10L);
            }

            Bukkit.broadcastMessage(" ");
            VoteMap finalBestMap = bestMap;
            UserManager.getConnectedUsers().values().forEach(o -> o.sendMessage("vote-ends", o.translate("qsg-prefix"), finalBestMap.getMapData().getDisplayName()));
            GoServer.getLocaleManager().getLocales().forEach((k, v) -> GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "vote-ends", GoServer.getLocaleManager().getMessage(k, "qsg-prefix"), finalBestMap.getMapData().getDisplayName()), k));
            Bukkit.broadcastMessage(" ");

            QSG.getGame().setQSGMap(bestMap.getMapData().getCreatedMap());
            GoAPI.getServerAPI().changeServer(ServerState.LOBBY, bestMap.getMapData().getDisplayName() + " - " + QSG.getGameConfig().getTYPE());
            LobbyScoreboardManager.updateAllMap();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
