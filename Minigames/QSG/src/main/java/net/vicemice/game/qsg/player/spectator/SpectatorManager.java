package net.vicemice.game.qsg.player.spectator;

import com.mojang.authlib.GameProfile;
import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.util.ItemStackHelper;
import net.vicemice.game.qsg.game.ScoreboardManager;
import net.vicemice.game.qsg.player.PlayerManager;
import lombok.Getter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.internal.SkullChanger;
import net.vicemice.hector.server.utils.ItemCreator;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SpectatorManager {
	@Getter
	private static List<IUser> spectatorPlayers = new ArrayList<>();
	@Getter
	private static Inventory spectatorInventory;

	public static void updateSpectatorInventory() {
		int size = 0;
		if (PlayerManager.getPlayerDatas().size() <= 9) {
			size = 9;
		} else if (PlayerManager.getPlayerDatas().size() <= 18) {
			size = 18;
		} else if (PlayerManager.getPlayerDatas().size() <= 27) {
			size = 27;
		} else if (PlayerManager.getPlayerDatas().size() <= 36) {
			size = 36;
		} else {
			size = 54;
		}
		if (spectatorInventory == null || size != spectatorInventory.getSize()) {
			Inventory newInventory = Bukkit.getServer().createInventory(null, size, "§7Teleporter");
			spectatorInventory = newInventory;
		}
		int currentSlot = 0;
		HashMap<Integer, ItemStack> addedItems = new HashMap<>();
		for (IUser user : PlayerManager.getPlayerDatas().keySet()) {
			if (currentSlot > 53) {
				break;
			}
			CraftPlayer entityPlayer = (CraftPlayer) user;
			GameProfile gameProfile = entityPlayer.getProfile();
			ItemStack skull = null;
			if (gameProfile.getProperties().containsKey("textures")) {
				skull = SkullChanger.getSkull(user.getBukkitPlayer());
			} else {
				skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
			}
			ItemMeta skullMeta = skull.getItemMeta();
			skullMeta.setDisplayName(user.getRank().getRankColor() + user.getName());
			skull.setItemMeta(skullMeta);
			spectatorInventory.setItem(currentSlot, skull);
			addedItems.put(currentSlot, skull);
			currentSlot++;
		}
		int currentCheckSlot = 0;
		for (ItemStack content : spectatorInventory.getContents()) {
			if (addedItems.containsKey(currentCheckSlot)) {
				if (content == null || !addedItems.get(currentCheckSlot).equals(content)) {
					spectatorInventory.setItem(currentCheckSlot, addedItems.get(currentCheckSlot));
				}
			} else {
				spectatorInventory.setItem(currentCheckSlot, null);
			}
			currentCheckSlot++;
		}
	}

	public static void spectatorPlayer(IUser user, boolean teleport) {
		if (!spectatorPlayers.contains(user)) {
			spectatorPlayers.add(user);
		}
		GameManager.getSpectatorPlayers().add(user.getUniqueId());

		PlayerManager.resetPlayer(user);
		user.getBukkitPlayer().spigot().setCollidesWithEntities(false);
		ScoreboardManager.getSpectatorTeam().addEntry(user.getName());
		user.getBukkitPlayer().setGameMode(GameMode.ADVENTURE);
		user.getBukkitPlayer().setAllowFlight(true);
		user.getBukkitPlayer().setFlying(true);
		user.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));

		if (teleport) {
			user.teleport(QSG.getGame().getQSGMap().getWorld().getSpawnLocation());
		}

		if (!ScoreboardManager.getSpectatorTeam().hasEntry(user.getName())) {
			ScoreboardManager.getSpectatorTeam().addEntry(user.getName());
		}
		if (user.isNicked()) {
			if (!ScoreboardManager.getSpectatorTeam().hasEntry(user.getNickName())) {
				ScoreboardManager.getSpectatorTeam().addEntry(user.getNickName());
			}
		}
		for (IUser onlinePlayer : UserManager.getConnectedUsers().values()) {
			if (PlayerManager.getPlayerDatas().get(onlinePlayer) == null) continue;
			onlinePlayer.getBukkitPlayer().hidePlayer(onlinePlayer.getBukkitPlayer());
		}
		for (IUser spectatorPlayer : spectatorPlayers) {
			if (spectatorPlayer == user) continue;
			user.getBukkitPlayer().showPlayer(spectatorPlayer.getBukkitPlayer());
		}
		new BukkitRunnable() {
			@Override
			public void run() {
				user.getBukkitPlayer().getInventory().setItem(0, new ItemCreator().material(Material.COMPASS).displayName(user.translate("item-spectator-compass")).build());
				user.getBukkitPlayer().getInventory().setItem(8, new ItemCreator().material(Material.PAPER).displayName(user.translate("item-next-round")).build());
				user.getBukkitPlayer().updateInventory();
			}
		}.runTaskLater(QSG.getInstance(), 10L);
		user.sendMessage("game-spectator", user.translate("qsg-prefix"));
		SpectatorScoreboard.setupSpectatorScoreboard(user);
	}

	public static void playerQuit(IUser user) {
		SpectatorScoreboard.getScoreboards().remove(user);
	}

	public enum SpectateItems {
		COMPASS(ItemStackHelper.createItemStack(Material.COMPASS, 0, "§7Teleporter"));
		ItemStack itemStack;

		SpectateItems(ItemStack itemStack) {
			this.itemStack = itemStack;
		}

		public ItemStack get() {
			return itemStack;
		}
	}
}