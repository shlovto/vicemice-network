package net.vicemice.game.qsg.listener;

import net.vicemice.game.qsg.player.party.PartyManager;
import net.vicemice.game.qsg.player.spectator.SpectatorManager;
import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.player.LobbyScoreboardManager;
import net.vicemice.game.qsg.player.PlayerManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.gameapi.util.EloRank;
import net.vicemice.hector.gameapi.util.Locations;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.party.PartyMemberPacket;
import net.vicemice.hector.packets.types.player.stats.GetPlayerStatsPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.server.utils.ItemCreator;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.game.qsg.config.map.vote.VoteState;
import net.vicemice.game.qsg.game.VotingManager;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        IUser user = UserManager.getUser(event.getPlayer());
        event.setJoinMessage("");
        PlayerManager.resetPlayer(user);
        if (GameManager.getGameState() == GameState.LOBBY) {
            if (QSG.getGameConfig().isRANKED()) {
                long elo = (user.getStatistic("qsg_elo", StatisticPeriod.MONTH) >= 1 ? user.getStatistic("qsg_elo", StatisticPeriod.MONTH) : 0);
                EloRank rank = EloRank.rankByElo(elo);
                if (rank == null) rank = EloRank.UNRANKED;
                user.sendMessage("ranked-server", "QSG", ((rank.getMax()+1)-elo));
            } else {
                user.sendMessage("unranked-server", "QSG");
            }

            user.getBukkitPlayer().setGameMode(GameMode.ADVENTURE);
            String playerName = (user.isNicked() ? "§9"+user.getNickName() : user.getRank().getRankColor() + user.getName());
            UserManager.getConnectedUsers().values().forEach(p -> {
                p.sendMessage("game-joined", playerName);
            });
            GoServer.getLocaleManager().getLocales().forEach((k, v) -> {
                GameAPI.sendReplayMessage(GoServer.getLocaleManager().getMessage(k, "game-joined", playerName), k);
            });

            if (user.getRank().isHigherEqualsLevel(Rank.VIP)) {
                if (VotingManager.getVoteState() == VoteState.ON) {
                    user.getBukkitPlayer().getInventory().setItem(4, new ItemCreator().material(Material.CHEST).displayName(user.translate("item-force-map")).build());
                }
            }

            if (VotingManager.getVoteState() == VoteState.ON) {
                if (QSG.getGameConfig().isVOTING()) {
                    user.getBukkitPlayer().getInventory().setItem(0, new ItemCreator().material(Material.PAPER).displayName(user.translate("item-voting")).build());
                }
            }

            if (PartyManager.getPartyMembers().containsKey(user.getUniqueId())) {
                PartyMemberPacket partyMemberPacket = PartyManager.getPartyMembers().get(user.getUniqueId());
                List<IUser> onlinePartyPlayers = new ArrayList<>();
                for (UUID member : partyMemberPacket.getMembers()) {
                    if (UserManager.isConnected(member)) {
                        IUser getPlayer = UserManager.getUser(member);
                        onlinePartyPlayers.add(getPlayer);
                    }
                }
                if (onlinePartyPlayers.size() == partyMemberPacket.getMembers().size()) {
                    PartyManager.allPlayersOnline(onlinePartyPlayers);
                }
            }
            LobbyScoreboardManager.createBoard(user);
        } else if (GameManager.getGameState() == GameState.INGAME || GameManager.getGameState() == GameState.WARMUP) {
            SpectatorManager.spectatorPlayer(user, true);
            return;
        } else if (GameManager.getGameState() == GameState.END) {
            user.getBukkitPlayer().kickPlayer("lobby");
        }
        user.teleport(Locations.getLobbyLocation());
    }
}