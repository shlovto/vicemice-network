package net.vicemice.game.qsg.game;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.chest.ChestManager;
import net.vicemice.game.qsg.config.map.MapImporter;
import net.vicemice.game.qsg.player.team.TeamManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.types.ServerState;

public class StartupManager {

    public static void startQSG() {
        // Chests
        ChestManager.prepareItems();
        // Scoreboard
        ScoreboardManager.initScoreboards();
        // Load Maps
        MapImporter.importMaps();
        // Create Teams
        TeamManager.createTeams();
        // Voting
        VotingManager.createVoting();
        // Select Random Map
        if (!QSG.getGameConfig().isVOTING()) {
            MapImporter.selectRandomMap();
        }
        // Start Timer
        GameAPI.initGameAPI();
        GameManager.startTimer(QSG.getInstance(), GoServer.getService().getGoAPI());

        if (QSG .getGameConfig().isVOTING()) {
            GoAPI.getServerAPI().changeServer(ServerState.LOBBY, "&eVoting &8- &e"+ QSG.getGameConfig().getTYPE());
        } else {
            GoAPI.getServerAPI().changeServer(ServerState.LOBBY, "&e"+QSG.getGame().getQSGMap().getDisplayName()+" &8- &e"+ QSG.getGameConfig().getTYPE());
        }
        GoAPI.getServerAPI().setKick(true);
    }
}