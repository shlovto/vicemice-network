package net.vicemice.game.qsg.player;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.player.stats.Coins;
import net.vicemice.game.qsg.player.stats.Points;
import net.vicemice.game.qsg.player.team.GameTeam;
import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.server.player.scoreboard.PlayerScoreboard;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Player;

public class PlayerData {
    @Getter
    private IUser user;
    @Getter
    private GameTeam gameTeam;
    private IUser lastHitted;
    private long coins;
    private long hittedTime;
    @Getter
    private int kills, deaths, open_chests, points, wins, played_games, placed_blocks, projectile_kills;
    @Getter
    @Setter
    private PlayerScoreboard playerScoreboard;
    private boolean saved = false;
    @Getter
    private boolean projectileHit = false;

    public PlayerData(IUser user, GameTeam gameTeam) {
        this.user = user;
        this.gameTeam = gameTeam;
    }

    public void receiveDamageFrom(IUser user, boolean projectileHit) {
        this.lastHitted = user;
        this.hittedTime = System.currentTimeMillis() + 30000;
        this.projectileHit = projectileHit;
    }

    public IUser getLastHitted() {
        if (lastHitted != null) {
            if (hittedTime > System.currentTimeMillis()) {
                return lastHitted;
            }
        }
        return null;
    }

    public void addKill() {
        kills++;
        points += Points.KILL.getPoints();
        int coins = Coins.KILL.getCoins();
        this.coins+=coins;
        this.user.addXP(3);

        if (QSG.getGameConfig().isRANKED()) {
            this.user.increaseStatistic("qsg_elo", 25);
        }
    }

    public void addPlayedGame() {
        played_games++;
    }

    public void addOpenChest() {
        open_chests++;
    }

    public void addPlacedBlock() {
        placed_blocks++;
    }

    public void addProjectile() {
        projectile_kills++;
    }

    public void addDeath() {
        deaths++;

        if (QSG.getGameConfig().isRANKED()) {
            this.user.decreaseStatistic("qsg_elo", 10);
        }
    }

    public void addWin() {
        wins++;
        points += Points.WIN.getPoints();
        int coins = Coins.WIN.getCoins();
        this.coins+=coins;
        this.user.addXP(20);

        if (QSG.getGameConfig().isRANKED()) {
            this.user.increaseStatistic("qsg_elo", 25);
        }
    }

    public void saveStats() {

        this.user.sendMessage("round-stats",
                GameManager.getTime(GameManager.getStartTime()),
                this.kills,
                this.projectile_kills,
                this.deaths,
                this.open_chests);

        if (QSG.isTestMode()) return;
        if (saved) return;
        saved = true;
        if (kills != 0) {
            this.user.increaseStatistic(QSG.getStatsPrefix()+"kills", kills);
        }
        if (deaths != 0) {
            this.user.increaseStatistic(QSG.getStatsPrefix()+"deaths", deaths);
        }
        if (points != 0) {
            this.user.increaseStatistic("points", points);
        }
        if (wins != 0) {
            this.user.increaseStatistic(QSG.getStatsPrefix()+"wins", wins);
        }
        if (played_games != 0) {
            this.user.increaseStatistic(QSG.getStatsPrefix()+"played_games", played_games);
            this.user.increaseStatistic("played_games", 1);
        }
        if (open_chests != 0) {
            this.user.increaseStatistic(QSG.getStatsPrefix()+"open_chests", open_chests);
        }
        if (projectile_kills != 0) {
            this.user.increaseStatistic(QSG.getStatsPrefix()+"projectile_kills", projectile_kills);
        }
        user.increaseCoins(coins);
        user.sendMessage("receive-coins", coins);
    }
}