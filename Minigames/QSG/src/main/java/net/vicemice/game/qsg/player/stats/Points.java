package net.vicemice.game.qsg.player.stats;

public enum Points {
    KILL( 5 ), WIN( 15 );
    public int points;
    Points( int points ) {
        this.points = points;
    }
    public int getPoints() {
        return points;
    }
}
