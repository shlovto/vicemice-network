package net.vicemice.game.qsg.player.team;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.player.PlayerData;
import net.vicemice.game.qsg.player.PlayerManager;
import net.vicemice.game.qsg.game.ScoreboardManager;
import net.vicemice.game.qsg.player.spectator.SpectatorManager;
import net.vicemice.game.qsg.player.spectator.SpectatorScoreboard;
import lombok.Getter;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class  TeamManager {
    @Getter
    private static HashMap<Integer, GameTeam> teams = new HashMap<>();
    @Getter
    private static List<GameTeam> aliveTeams = new ArrayList<>();
    @Getter
    private static Inventory selectInventory;
    @Getter
    private static Inventory selectInventoryDE;

    public static GameTeam getPlayersTeam(IUser user) {
        for (int slot : teams.keySet()) {
            GameTeam gameTeam = teams.get(slot);
            for (IUser teamPlayer : gameTeam.getMembers()) {
                if (teamPlayer.equals(user)) {
                    return gameTeam;
                }
            }
        }
        return null;
    }

    public static void createTeams() {
        for (int i = 0; i < QSG.getGameConfig().getTEAMS(); i++) {
            teams.put(i, new GameTeam(i));
        }
    }

    public static void playerDeath(IUser user) {
        ScoreboardManager.getTeams().get(PlayerManager.getPlayerData(user).getGameTeam()).removeEntry(user.getName());
        PlayerData playerData = PlayerManager.getPlayerData(user);
        if (playerData != null) {
            if (playerData.getPlayerScoreboard() != null) {
                playerData.getPlayerScoreboard().remove();
            }
        }
        PlayerManager.getPlayerDatas().remove(user);
        removePlayerFromTeams(user);
        checkAliveTeams();
        SpectatorManager.updateSpectatorInventory();
        SpectatorScoreboard.updatePlayers();
        try {
            PlayerManager.getPlayerDatas().keySet().forEach(ScoreboardManager::updatePlayers);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public static void removePlayerFromTeams(IUser user) {
        for (int id : teams.keySet()) {
            GameTeam gameTeam = teams.get(id);
            if (gameTeam.getMembers().contains(user)) {
                gameTeam.getMembers().remove(user);
            }
        }
        for (GameTeam gameTeam : aliveTeams) {
            if (gameTeam.getMembers().contains(user)) {
                gameTeam.getMembers().remove(user);
            }
        }
    }

    public static void checkAliveTeams() {
        Iterator<GameTeam> iterator = aliveTeams.iterator();
        while (iterator.hasNext()) {
            GameTeam skywarsTeam = iterator.next();
            if (skywarsTeam.getMembers().size() == 0) {
                //MessageUtil.sendMessageToAll( SkyWars.getPrefix() + "§7Das Team §6" + skywarsTeam.getNumber() + " §7ist ausgeschieden!" );
                iterator.remove();
            }
        }
        if (aliveTeams.size() == 1) {
            // End Game
            GameManager.endGame();
        }
    }
}