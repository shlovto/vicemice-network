package net.vicemice.game.qsg;

import net.vicemice.game.qsg.commands.AdminCommand;
import net.vicemice.game.qsg.commands.StartCommand;
import net.vicemice.game.qsg.commands.StatsCommand;
import net.vicemice.game.qsg.game.Game;
import net.vicemice.game.qsg.game.MapManager;
import net.vicemice.game.qsg.player.party.PartyPacketListener;
import net.vicemice.game.qsg.util.StatsListener;
import net.vicemice.game.qsg.game.StartupManager;
import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.gameapi.GameAPI;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.Register;
import net.vicemice.game.qsg.config.game.GameConfig;
import net.vicemice.game.qsg.config.game.GameConfiguration;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QSG extends JavaPlugin {
    @Getter
    private static QSG instance;
    @Getter
    public static Game game;
    @Getter
    public static MapManager mapManager;
    @Getter
    public static GameConfig gameConfig;

    @Getter
    private static List<Player> requestedStats = new ArrayList<>();
    @Getter
    @Setter
    private static boolean testMode = false, betaMode = false;
    @Getter
    public static String statsPrefix;

    @Override
    public void onEnable() {
        instance = this;
        game = new Game();
        mapManager = new MapManager();
        GoServer.getLocaleManager().addPaths(new ArrayList<>(Arrays.asList("cloud/locales/qsg", "cloud/locales/game")));
        Register.registerListener(this, "net.vicemice.game.qsg.listener");
        Register.registerListener(this, "net.vicemice.game.qsg.game.tasks");

        GameConfiguration gameConfiguration = new GameConfiguration();
        if (!gameConfiguration.exists()) {
            GameConfig config = new GameConfig();
            gameConfiguration.writeConfiguration(config);
            System.out.println("Game Config File not found and will be created...");
            System.out.println("Game Config File was created!");
        }
        gameConfiguration.readConfiguration();
        gameConfig = gameConfiguration.getGameConfig();

        // StartUP QSG
        GameAPI.initGameAPI();
        StartupManager.startQSG();

        // Register commands
        getCommand("qsgadmin").setExecutor(new AdminCommand());
        getCommand("start").setExecutor(new StartCommand());
        getCommand("stats").setExecutor(new StatsCommand());

        // Register Party Packet listener
        GoServer.getService().getGoAPI().getNettyClient().addGetter(new PartyPacketListener());
        GoServer.getService().getGoAPI().getNettyClient().addGetter(new StatsListener());

        Bukkit.getWorld("lobby").setSpawnLocation(-30, -8, -4);

        if (gameConfig.isRANKED()) {
            statsPrefix = "qsg_";
        } else {
            statsPrefix = "uqsg_";
        }
    }
}