package net.vicemice.game.qsg.player;

import lombok.Getter;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class LobbyScoreboardManager {
    @Getter
    private static HashMap<IUser, LobbyScoreboard> scoreboards = new HashMap<>();

    public static void createBoard(IUser user) {
        scoreboards.put(user, new LobbyScoreboard(user));
    }

    public static void removeBoard(IUser user) {
        scoreboards.remove(user);
    }

    public static void updateAllMap() {
        for (IUser user : scoreboards.keySet()) {
            scoreboards.get(user).setMap();
        }
    }

    public static void removeAllBoards() {
        scoreboards.clear();
    }
}