package net.vicemice.game.qsg.util.map;

import net.vicemice.game.qsg.game.VotingManager;
import net.vicemice.game.qsg.player.PlayerData;
import net.vicemice.game.qsg.player.PlayerManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/*
 * Class created at 16:17 - 23.04.2020
 * Copyright (C) elrobtossohn
 */
public class MapVotingInventory {

    public GUI create(IUser user) {
        GUI gui = user.createGUI("Map Voting", 5);
        PlayerData playerData = PlayerManager.getPlayerDatas().get(user);

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        if (VotingManager.getVotemaps().size() == 3) {
            gui.setItem(19, ItemBuilder.create(VotingManager.getVotemaps().get(0).getItemStack()).name("§e"+VotingManager.getVotemaps().get(0).getMapData().getDisplayName()).lore("§a", "§3Votes: §e"+VotingManager.getVotemaps().get(0).getVotes().size()).glow(VotingManager.getVotemaps().get(0).getVotes().contains(user)).build(), e -> {
                VotingManager.addPlayerVote(user, 0);
                e.getView().close();
            });

            gui.setItem(22, ItemBuilder.create(VotingManager.getVotemaps().get(1).getItemStack()).name("§e"+VotingManager.getVotemaps().get(1).getMapData().getDisplayName()).lore("§a", "§3Votes: §e"+VotingManager.getVotemaps().get(1).getVotes().size()).glow(VotingManager.getVotemaps().get(1).getVotes().contains(user)).build(), e -> {
                VotingManager.addPlayerVote(user, 1);
                e.getView().close();
            });

            gui.setItem(25, ItemBuilder.create(VotingManager.getVotemaps().get(2).getItemStack()).name("§e"+VotingManager.getVotemaps().get(2).getMapData().getDisplayName()).lore("§a", "§3Votes: §e"+VotingManager.getVotemaps().get(2).getVotes().size()).glow(VotingManager.getVotemaps().get(2).getVotes().contains(user)).build(), e -> {
                VotingManager.addPlayerVote(user, 2);
                e.getView().close();
            });
        } else if (VotingManager.getVotemaps().size() == 2) {
            gui.setItem(20, ItemBuilder.create(VotingManager.getVotemaps().get(0).getItemStack()).name("§e"+VotingManager.getVotemaps().get(0).getMapData().getDisplayName()).lore("§a", "§3Votes: §e"+VotingManager.getVotemaps().get(0).getVotes().size()).glow(VotingManager.getVotemaps().get(0).getVotes().contains(user)).build(), e -> {
                VotingManager.addPlayerVote(user, 0);
                e.getView().close();
            });

            gui.setItem(24, ItemBuilder.create(VotingManager.getVotemaps().get(1).getItemStack()).name("§e"+VotingManager.getVotemaps().get(1).getMapData().getDisplayName()).lore("§a", "§3Votes: §e"+VotingManager.getVotemaps().get(1).getVotes().size()).glow(VotingManager.getVotemaps().get(1).getVotes().contains(user)).build(), e -> {
                VotingManager.addPlayerVote(user, 1);
                e.getView().close();
            });
        } else if (VotingManager.getVotemaps().size() == 1) {
            gui.setItem(22, ItemBuilder.create(VotingManager.getVotemaps().get(0).getItemStack()).name("§e"+VotingManager.getVotemaps().get(0).getMapData().getDisplayName()).lore("§a", "§3Votes: §e"+VotingManager.getVotemaps().get(0).getVotes().size()).glow(VotingManager.getVotemaps().get(0).getVotes().contains(user)).build(), e -> {
                VotingManager.addPlayerVote(user, 0);
                e.getView().close();
            });
        } else {
            gui.setItem(25, ItemBuilder.create(Material.BARRIER).name(user.translate("no-maps")).build());
        }

        return gui;
    }
}