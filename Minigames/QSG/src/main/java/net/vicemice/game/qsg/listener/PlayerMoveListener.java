package net.vicemice.game.qsg.listener;

import net.vicemice.game.qsg.player.PlayerData;
import net.vicemice.game.qsg.player.PlayerManager;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.gameapi.util.Locations;
import net.vicemice.hector.server.player.UserManager;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveListener implements Listener {

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent playerMoveEvent) {
        if (GameManager.getGameState() == GameState.LOBBY) {
            if (playerMoveEvent.getTo().getY() < 90)
                playerMoveEvent.getPlayer().teleport(Locations.getLobbyLocation());
        }
        if (GameManager.getGameState() == GameState.WARMUP) {
            Player player = playerMoveEvent.getPlayer();

            PlayerData playerData = PlayerManager.getPlayerData(UserManager.getUser(player));
            if (playerData.getGameTeam() == null || playerData.getGameTeam().getSpawnLocation() == null) return;
            Location loc = playerData.getGameTeam().getSpawnLocation();
            Location to = playerMoveEvent.getTo();
            if (loc.getBlockX() != to.getBlockX() || loc.getBlockZ() != to.getBlockZ()) {
                playerMoveEvent.setCancelled(true);
            }
        }
    }
}