package net.vicemice.game.qsg.chest;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.player.PlayerManager;
import lombok.Getter;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class ChestManager {
    private static RandomCollection randomCollection, randomOPCollection;
    @Getter
    private static List<Inventory> addedInventories = new ArrayList<>();
    @Getter
    private static List<Location> placedChests = new ArrayList<>();
    private static Random random;

    public static void prepareChest(Inventory inventory, IUser user) {
        if (!addedInventories.contains(inventory)) {
            addedInventories.add(inventory);
            List<Integer> addedSlots = new ArrayList<>();
            List<Material> addedMaterials = new ArrayList<>();
            List<String> addedTypes = new ArrayList<>();
            int itemAmount;
            for (; ; ) {
                itemAmount = (int) (Math.random() * 16);
                if (itemAmount >= 8) break;
            }
            while (itemAmount >= 1) {
                itemAmount--;
                for (; ; ) {
                    int randomSlot = (int) (Math.random() * 26);
                    if (!addedSlots.contains(randomSlot)) {
                        addedSlots.add(randomSlot);
                        for (; ; ) {
                            ClickItem chestItem;
                            ClickItem chestItem2;
                            ItemStack randomStack;
                            if (QSG.getGame().isOp()) {
                                chestItem = (ClickItem) randomOPCollection.next();
                                randomStack = chestItem.getItemStack();
                            } else {
                                chestItem = (ClickItem) randomCollection.next();
                                randomStack = chestItem.getItemStack();
                            }
                            if (user.getBukkitPlayer().getInventory().contains(randomStack)) {
                                if (QSG.getGame().isOp()) {
                                    chestItem2 = (ClickItem) randomOPCollection.next();
                                    randomStack = chestItem2.getItemStack();
                                } else {
                                    chestItem2 = (ClickItem) randomCollection.next();
                                    randomStack = chestItem2.getItemStack();
                                }
                            }
                            if (!addedMaterials.contains(randomStack.getType())) {
                                if (randomStack.getType().name().contains("_")) {
                                    String type = randomStack.getType().name().split("_")[1];
                                    if (addedTypes.contains(type)) {
                                        continue;
                                    }
                                    addedTypes.add(type);
                                }
                                addedMaterials.add(randomStack.getType());
                                ItemStack setStack = randomStack.clone();
                                if (chestItem.getAmount() == 0) {
                                    int randomAmount;
                                    if (chestItem.getMinRandom() == 0) {
                                        randomAmount = random.nextInt(chestItem.getRandomAmount());
                                    } else {
                                        randomAmount = random.nextInt(chestItem.getRandomAmount() - chestItem.getMinRandom() + 1) + chestItem.getMinRandom();
                                    }
                                    setStack.setAmount(randomAmount);
                                }
                                inventory.setItem(randomSlot, setStack);
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            PlayerManager.getPlayerData(user).addOpenChest();
        }
    }
    
    public static void prepareItems() {
        random = new Random();
        randomCollection = new RandomCollection<ClickItem>();
        randomOPCollection = new RandomCollection<ClickItem>();

        Potion heal1 = new Potion(PotionType.INSTANT_HEAL, 1);
        heal1.setSplash(true);
        Potion heal2 = new Potion(PotionType.INSTANT_HEAL, 2);
        heal2.setSplash(true);

        // Weight1
        randomCollection.add(1, new ClickItem(ItemBuilder.create(Material.APPLE).build(), 1, 0, null, null, false));
        randomCollection.add(1, new ClickItem(ItemBuilder.create(Material.STONE_AXE).build(), 1, 0, null, null, false));
        randomCollection.add(1, new ClickItem(ItemBuilder.create(Material.STONE_SWORD).build(), 1, 0, null, null, false));
        randomCollection.add(1, new ClickItem(ItemBuilder.create(Material.IRON_CHESTPLATE).build(), 1, 0, null, null, false));
        // Weight2
        randomCollection.add(2, new ClickItem(ItemBuilder.create(Material.CHAINMAIL_CHESTPLATE).build(), 1, 0,null, null, false));
        randomCollection.add(2, new ClickItem(ItemBuilder.create(Material.CHAINMAIL_LEGGINGS).build(), 1, 0, null, null, false));
        randomCollection.add(2, new ClickItem(ItemBuilder.create(Material.DIAMOND_CHESTPLATE).build(), 1, 0, null, null, false));
        randomCollection.add(2, new ClickItem(ItemBuilder.create(Material.CHAINMAIL_BOOTS).build(), 1, 0, null, null, false));
        randomCollection.add(2, new ClickItem(heal1.toItemStack(1), 0, 4, 1, null, null, false));
        randomCollection.add(2, new ClickItem(heal2.toItemStack(1), 0, 4, 1, null, null, false));

        //Weight3
        randomCollection.add(3, new ClickItem(ItemBuilder.create(Material.BREAD).build(), 1, 0, null, null, false));
        randomCollection.add(3, new ClickItem(ItemBuilder.create(Material.ARROW).build(), 3, 0, null, null, false));
        randomCollection.add(3, new ClickItem(ItemBuilder.create(Material.BOW).build(), 1, 0, null, null, false));
        randomCollection.add(3, new ClickItem(ItemBuilder.create(Material.IRON_SWORD).build(), 1, 0, null, null, false));
        randomCollection.add(3, new ClickItem(ItemBuilder.create(Material.IRON_HELMET).build(), 0, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.IRON_LEGGINGS).build(), 1, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.IRON_BOOTS).build(), 1, 0, null, null, false));
        //Weight6
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.LEATHER_HELMET).build(), 1, 0, null, null, false));

        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.WOOD_PICKAXE).build(), 1, 0, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.COOKED_BEEF).build(), 0, 10, 1, null, null, false));

        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.TNT).build(), 0, 5, 1, null, null, false));

        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.FISHING_ROD).build(), 1, 0, 2, null, null, false));

        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.EXP_BOTTLE).build(), 0, 4, 2, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.PUMPKIN_PIE).build(), 0, 4, 2, null, null, false));

        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.FLINT_AND_STEEL).build(), 1, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.STICK).build(), 1, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.LEATHER_CHESTPLATE).build(), 1, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.IRON_BOOTS).build(), 1, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.STONE_AXE).build(), 1, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.BREAD).build(), 2, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.IRON_LEGGINGS).build(), 1, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.IRON_SWORD).build(), 1, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.CHAINMAIL_HELMET).build(), 1, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.DIAMOND_CHESTPLATE).build(), 1, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.BAKED_POTATO).build(), 4, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.CHAINMAIL_LEGGINGS).build(), 1, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.STONE_SWORD).build(), 1, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.IRON_CHESTPLATE).build(), 1, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.APPLE).build(), 3, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.LEATHER_BOOTS).build(), 1, 0, null, null, false));
        randomCollection.add(6, new ClickItem(ItemBuilder.create(Material.BREAD).build(), 6, 0, null, null, false));

        //Weight1
        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.COMPASS).build(), 1, 0, null, null, false));

        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.COOKED_BEEF).build(), 0, 10, 5, null, null, false));
        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.COOKED_BEEF).build(), 0, 10, 5, null, null, false));

        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.FLINT).build(), 0, 5, 1, null, null, false));

        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.WEB).build(), 0, 5, 2, null, null, false));

        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.PUMPKIN_PIE).build(), 0, 4, 2, null, null, false));
        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.PUMPKIN_PIE).build(), 0, 4, 2, null, null, false));

        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.CAKE).build(), 1, 0, null, null, false));
        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.RAW_BEEF).build(), 0, 10, 5, null, null, false));
        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.STICK).build(), 0, 5, 1, null, null, false));
        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.WOOD_SWORD).build(), 1, 0, Arrays.asList(Enchantment.DAMAGE_ALL), Arrays.asList(2), false));
        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.GOLD_SWORD).build(), 1, 0, Arrays.asList(Enchantment.DAMAGE_ALL), Arrays.asList(2), false));
        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.STONE_SWORD).build(), 1, 0, Arrays.asList(Enchantment.DAMAGE_ALL), Arrays.asList(2), false));
        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.LEATHER_HELMET).build(), 1, 0, null, null, false));
        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.LEATHER_CHESTPLATE).build(), 1, 5, null, null, false));
        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.LEATHER_LEGGINGS).build(), 1, 5, null, null, false));
        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.LEATHER_BOOTS).build(), 1, 5, null, null, false));
        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.CHAINMAIL_HELMET).build(), 1, 5, null, null, false));
        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.CHAINMAIL_CHESTPLATE).build(), 1, 5, null, null, false));
        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.CHAINMAIL_LEGGINGS).build(), 1, 5, null, null, false));
        randomOPCollection.add(1, new ClickItem(ItemBuilder.create(Material.CHAINMAIL_BOOTS).build(), 1, 5, null, null, false));

        //Weight2
        randomOPCollection.add(2, new ClickItem(ItemBuilder.create(Material.IRON_INGOT).build(), 0, 10, 1, null, null, false));
        randomOPCollection.add(2, new ClickItem(ItemBuilder.create(Material.TNT).build(), 0, 10, 2, null, null, false));
        randomOPCollection.add(2, new ClickItem(ItemBuilder.create(Material.WATER_BUCKET).build(), 1, 0, null, null, false));
        randomOPCollection.add(2, new ClickItem(ItemBuilder.create(Material.LAVA_BUCKET).build(), 1, 0, null, null, false));
        randomOPCollection.add(2, new ClickItem(ItemBuilder.create(Material.EXP_BOTTLE).build(), 0, 15, 3, null, null, false));
        randomOPCollection.add(2.5, new ClickItem(ItemBuilder.create(Material.GOLD_HELMET).build(), 1, 0, Arrays.asList(Enchantment.PROTECTION_ENVIRONMENTAL), Arrays.asList(1), false));
        randomOPCollection.add(2.5, new ClickItem(ItemBuilder.create(Material.GOLD_CHESTPLATE).build(), 1, 0, Arrays.asList(Enchantment.PROTECTION_ENVIRONMENTAL), Arrays.asList(1), false));
        randomOPCollection.add(2.5, new ClickItem(ItemBuilder.create(Material.GOLD_LEGGINGS).build(), 1, 0, Arrays.asList(Enchantment.PROTECTION_ENVIRONMENTAL), Arrays.asList(2), false));
        randomOPCollection.add(2.5, new ClickItem(ItemBuilder.create(Material.GOLD_BOOTS).build(), 1, 0, Arrays.asList(Enchantment.PROTECTION_ENVIRONMENTAL), Arrays.asList(2), false));
        randomOPCollection.add(2, new ClickItem(ItemBuilder.create(Material.GOLDEN_APPLE).build(), 1, 0, null, null, false));

        //Weight4
        randomOPCollection.add(3, new ClickItem(ItemBuilder.create(Material.DIAMOND).build(), 0, 4, 1, null, null, false));
        randomOPCollection.add(3, new ClickItem(ItemBuilder.create(Material.IRON_HELMET).build(), 1, 0, Arrays.asList(Enchantment.PROTECTION_ENVIRONMENTAL), Arrays.asList(1), false));
        randomOPCollection.add(3, new ClickItem(ItemBuilder.create(Material.IRON_CHESTPLATE).build(), 1, 0, Arrays.asList(Enchantment.PROTECTION_ENVIRONMENTAL), Arrays.asList(1), false));
        randomOPCollection.add(3, new ClickItem(ItemBuilder.create(Material.IRON_LEGGINGS).build(), 1, 0, Arrays.asList(Enchantment.PROTECTION_ENVIRONMENTAL), Arrays.asList(2), false));
        randomOPCollection.add(3, new ClickItem(ItemBuilder.create(Material.IRON_BOOTS).build(), 1, 0, Arrays.asList(Enchantment.PROTECTION_ENVIRONMENTAL), Arrays.asList(2), false));
        randomOPCollection.add(3, new ClickItem(ItemBuilder.create(Material.GOLD_AXE).build(), 1, 0, Arrays.asList(Enchantment.DIG_SPEED), Arrays.asList(2), false));
        randomOPCollection.add(3, new ClickItem(ItemBuilder.create(Material.GOLD_PICKAXE).build(), 1, 0, Arrays.asList(Enchantment.DIG_SPEED), Arrays.asList(2), false));
        randomOPCollection.add(3, new ClickItem(ItemBuilder.create(Material.GOLD_PICKAXE).build(), 1, 0, Arrays.asList(Enchantment.DIG_SPEED), Arrays.asList(2), false));
        randomOPCollection.add(3, new ClickItem(heal1.toItemStack(1), 0, 4, 1, null, null, false));
        randomOPCollection.add(3, new ClickItem(heal2.toItemStack(1), 0, 4, 1, null, null, false));

        //Weight5
        randomOPCollection.add(4.5, new ClickItem(ItemBuilder.create(Material.DIAMOND_HELMET).build(), 1, 0, Arrays.asList(Enchantment.PROTECTION_ENVIRONMENTAL), Arrays.asList(1), false));
        randomOPCollection.add(4.5, new ClickItem(ItemBuilder.create(Material.DIAMOND_CHESTPLATE).build(), 1, 0, Arrays.asList(Enchantment.PROTECTION_ENVIRONMENTAL), Arrays.asList(1), false));
        randomOPCollection.add(4.5, new ClickItem(ItemBuilder.create(Material.DIAMOND_LEGGINGS).build(), 1, 0, Arrays.asList(Enchantment.PROTECTION_ENVIRONMENTAL), Arrays.asList(2), false));
        randomOPCollection.add(4.5, new ClickItem(ItemBuilder.create(Material.DIAMOND_BOOTS).build(), 1, 0, Arrays.asList(Enchantment.PROTECTION_ENVIRONMENTAL), Arrays.asList(2), false));
        randomOPCollection.add(3.5, new ClickItem(ItemBuilder.create(Material.ENDER_PEARL).build(), 1, 0, null, null, false));
        randomOPCollection.add(4.5, new ClickItem(ItemBuilder.create(Material.DIAMOND_AXE).build(), 1, 0, Arrays.asList(Enchantment.DIG_SPEED), Arrays.asList(1), false));
        randomOPCollection.add(4.5, new ClickItem(ItemBuilder.create(Material.DIAMOND_PICKAXE).build(), 1, 0, Arrays.asList(Enchantment.DIG_SPEED), Arrays.asList(1), false));
    }
}