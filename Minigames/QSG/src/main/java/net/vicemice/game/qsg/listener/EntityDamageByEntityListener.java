package net.vicemice.game.qsg.listener;

import net.vicemice.game.qsg.player.PlayerData;
import net.vicemice.game.qsg.player.PlayerManager;
import net.vicemice.game.qsg.player.spectator.SpectatorManager;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class EntityDamageByEntityListener implements Listener {
    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        if (GameManager.getGameState() != GameState.INGAME) {
            event.setCancelled(true);
            return;
        }
        if (event.getDamager() instanceof Player) {
            IUser user = UserManager.getUser((Player) event.getDamager());
            if (SpectatorManager.getSpectatorPlayers().contains(user)) {
                event.setCancelled(true);
                return;
            }
        }
        if (event.getEntity() instanceof Player) {
            if (GameManager.isPeace()) {
                event.setCancelled(true);
                return;
            }
            IUser user = UserManager.getUser((Player) event.getEntity());
            PlayerData playerData = PlayerManager.getPlayerData(user);
            if (playerData == null) {
                event.setCancelled(true);
                return;
            }
            if (event.getDamager() instanceof Player) {
                IUser damager = UserManager.getUser((Player) event.getDamager());
                PlayerData damagerData = PlayerManager.getPlayerData(damager);
                if (damagerData == null) {
                    event.setCancelled(true);
                    return;
                }
                if (damagerData.getGameTeam() != null && playerData.getGameTeam() != null && damagerData.getGameTeam().equals(playerData.getGameTeam())) {
                    event.setCancelled(true);
                    return;
                }
                playerData.receiveDamageFrom(damager, false);
            } else if (event.getDamager() instanceof Projectile) {
                Projectile projectile = (Projectile) event.getDamager();
                if (projectile.getShooter() instanceof Player) {
                    IUser damager = UserManager.getUser((Player) projectile.getShooter());
                    PlayerData damagerData = PlayerManager.getPlayerData(damager);
                    if (damagerData == null) {
                        event.setCancelled(true);
                        return;
                    }
                    if (SpectatorManager.getSpectatorPlayers().contains(damager)) {
                        event.setCancelled(true);
                        return;
                    }
                    if (damagerData.getGameTeam() != null && playerData.getGameTeam() != null && damagerData.getGameTeam().equals(playerData.getGameTeam())) {
                        event.setCancelled(true);
                        return;
                    }
                    playerData.receiveDamageFrom(damager, true);
                }
            }
        }
    }
}