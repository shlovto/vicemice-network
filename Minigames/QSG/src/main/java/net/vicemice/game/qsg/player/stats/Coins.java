package net.vicemice.game.qsg.player.stats;

import java.text.SimpleDateFormat;
import java.util.Date;

public enum Coins {
    KILL( 10 ), WIN( 25 );
    public int coins;
    Coins( int coins ) {
        this.coins = coins;
    }
    public int getCoins() {
        String day = new SimpleDateFormat("dd.MM").format(new Date());
        if(day.equalsIgnoreCase("04.12") || day.equalsIgnoreCase("08.12") || day.equalsIgnoreCase("30.11")) {
            return coins*2;
        } else {
            return coins;
        }
    }
}
