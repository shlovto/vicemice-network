package net.vicemice.game.qsg.listener;

import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/*
 * Class created at 16:21 - 13.04.2020
 * Copyright (C) elrobtossohn
 */
public class ChestListener implements Listener {
    private HashMap<Location, Inventory> chests = new HashMap<>();
    private ArrayList<ItemStack> loot = new ArrayList<>();

    @EventHandler
    public void onChestOpen(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK && e.getClickedBlock().getType() == Material.CHEST && (GameManager.getIngamePlayers().contains(player.getUniqueId()) && GameManager.getGameState() == GameState.INGAME)) {
            e.setCancelled(true);
            if (!this.chests.containsKey(e.getClickedBlock().getLocation())) {
                this.registerLoot();
                Inventory inv = Bukkit.createInventory(null, 27, "Chest");
                for (int i = 0; i < rndInt(4, 9); ++i) {
                    inv.setItem(rndInt(0, inv.getSize() - 1), this.loot.get(rndInt(0, this.loot.size() - 1)));
                }
                this.chests.put(e.getClickedBlock().getLocation(), inv);
            }
            e.getPlayer().openInventory(this.chests.get(e.getClickedBlock().getLocation()));
        }
    }

    public void registerLoot() {
        this.loot.clear();
        this.loot.add(create(Material.WOOD_SWORD, 1));
        this.loot.add(create(Material.CHAINMAIL_HELMET, 1));
        this.loot.add(create(Material.APPLE, rndInt(1, 4)));
        this.loot.add(create(Material.IRON_HELMET, 1));
        this.loot.add(create(Material.GOLD_CHESTPLATE, 1));
        this.loot.add(create(Material.IRON_CHESTPLATE, 1));
        this.loot.add(create(Material.BAKED_POTATO, rndInt(1, 5)));
        this.loot.add(create(Material.GOLDEN_APPLE, 1));
        this.loot.add(create(Material.CHAINMAIL_BOOTS, 1));
        this.loot.add(create(Material.BOW, 1));
        this.loot.add(create(Material.STONE_SWORD, 1));
        this.loot.add(create(Material.FISHING_ROD, 1));
        this.loot.add(create(Material.WEB, rndInt(1, 4)));
        this.loot.add(create(Material.ARROW, rndInt(1, 4)));
        this.loot.add(create(Material.DIAMOND_CHESTPLATE, 1));
        this.loot.add(create(Material.BAKED_POTATO, rndInt(1, 5)));
        this.loot.add(create(Material.CHAINMAIL_LEGGINGS, 1));
        this.loot.add(create(Material.MELON, rndInt(1, 5)));
        this.loot.add(create(Material.IRON_LEGGINGS, 1));
        this.loot.add(create(Material.APPLE, rndInt(1, 4)));
        this.loot.add(create(Material.IRON_BOOTS, 1));
        this.loot.add(create(Material.COOKED_CHICKEN, rndInt(1, 5)));
        this.loot.add(create(Material.CHAINMAIL_LEGGINGS, 1));
        this.loot.add(create(Material.COOKED_BEEF, rndInt(1, 5)));
        this.loot.add(create(Material.GOLD_BOOTS, 1));
        this.loot.add(create(Material.WOOD_SWORD, 1));
        this.loot.add(create(Material.BOW, 1));
        this.loot.add(create(Material.CHAINMAIL_HELMET, 1));
        this.loot.add(create(Material.STONE_SWORD, 1));
        this.loot.add(create(Material.APPLE, rndInt(1, 4)));
        this.loot.add(create(Material.IRON_HELMET, 1));
        this.loot.add(create(Material.GOLD_CHESTPLATE, 1));
        this.loot.add(create(Material.IRON_CHESTPLATE, 1));
        this.loot.add(create(Material.BAKED_POTATO, rndInt(1, 5)));
        this.loot.add(create(Material.STONE_SWORD, 1));
        this.loot.add(create(Material.CHAINMAIL_BOOTS, 1));
        this.loot.add(create(Material.BAKED_POTATO, rndInt(1, 5)));
        this.loot.add(create(Material.CHAINMAIL_LEGGINGS, 1));
        this.loot.add(create(Material.ARROW, rndInt(1, 4)));
        this.loot.add(create(Material.MELON, rndInt(1, 5)));
        this.loot.add(create(Material.IRON_LEGGINGS, 1));
        this.loot.add(create(Material.APPLE, rndInt(1, 4)));
        this.loot.add(create(Material.IRON_BOOTS, 1));
        this.loot.add(create(Material.STONE_SWORD, 1));
        this.loot.add(create(Material.COOKED_CHICKEN, rndInt(1, 5)));
        this.loot.add(create(Material.CHAINMAIL_LEGGINGS, 1));
        this.loot.add(create(Material.COOKED_BEEF, rndInt(1, 5)));
        this.loot.add(create(Material.GOLD_BOOTS, 1));
        this.loot.add(create(Material.WOOD_SWORD, 1));
        this.loot.add(create(Material.FISHING_ROD, 1));
        this.loot.add(create(Material.CHAINMAIL_HELMET, 1));
        this.loot.add(create(Material.WEB, rndInt(1, 4)));
        this.loot.add(create(Material.BOW, 1));
        this.loot.add(create(Material.APPLE, rndInt(1, 4)));
        this.loot.add(create(Material.IRON_HELMET, 1));
        this.loot.add(create(Material.GOLD_CHESTPLATE, 1));
        this.loot.add(create(Material.STONE_AXE, 1));
        this.loot.add(create(Material.IRON_CHESTPLATE, 1));
        this.loot.add(create(Material.BAKED_POTATO, rndInt(1, 5)));
        this.loot.add(create(Material.IRON_SWORD, 1));
        this.loot.add(create(Material.CHAINMAIL_BOOTS, 1));
        this.loot.add(create(Material.ARROW, rndInt(1, 4)));
        this.loot.add(create(Material.BAKED_POTATO, rndInt(1, 5)));
        this.loot.add(create(Material.CHAINMAIL_LEGGINGS, 1));
        this.loot.add(create(Material.STONE_SWORD, 1));
        this.loot.add(create(Material.MELON, rndInt(1, 5)));
        this.loot.add(create(Material.IRON_LEGGINGS, 1));
        this.loot.add(create(Material.APPLE, rndInt(1, 4)));
        this.loot.add(create(Material.IRON_BOOTS, 1));
        this.loot.add(create(Material.DIAMOND_CHESTPLATE, 1));
        this.loot.add(create(Material.COOKED_CHICKEN, rndInt(1, 5)));
        this.loot.add(create(Material.CHAINMAIL_LEGGINGS, 1));
        this.loot.add(create(Material.COOKED_BEEF, rndInt(1, 5)));
        this.loot.add(create(Material.GOLD_BOOTS, 1));
        this.loot.add(create(Material.WOOD_SWORD, 1));
        this.loot.add(create(Material.BOW, 1));
        this.loot.add(create(Material.CHAINMAIL_HELMET, 1));
        this.loot.add(create(Material.APPLE, rndInt(1, 4)));
        this.loot.add(create(Material.IRON_HELMET, 1));
        this.loot.add(create(Material.GOLD_CHESTPLATE, 1));
        this.loot.add(create(Material.IRON_CHESTPLATE, 1));
        this.loot.add(create(Material.BAKED_POTATO, rndInt(1, 5)));
        this.loot.add(create(Material.GOLDEN_APPLE, 1));
        this.loot.add(create(Material.CHAINMAIL_BOOTS, 1));
        this.loot.add(create(Material.STONE_SWORD, 1));
        this.loot.add(create(Material.WEB, rndInt(1, 4)));
        this.loot.add(create(Material.ARROW, rndInt(1, 4)));
        this.loot.add(create(Material.BAKED_POTATO, rndInt(1, 5)));
        this.loot.add(create(Material.CHAINMAIL_LEGGINGS, 1));
        this.loot.add(create(Material.MELON, rndInt(1, 5)));
        this.loot.add(create(Material.IRON_LEGGINGS, 1));
        this.loot.add(create(Material.APPLE, rndInt(1, 4)));
        this.loot.add(create(Material.IRON_BOOTS, 1));
        this.loot.add(create(Material.COOKED_CHICKEN, rndInt(1, 5)));
        this.loot.add(create(Material.CHAINMAIL_LEGGINGS, 1));
        this.loot.add(create(Material.COOKED_BEEF, rndInt(1, 5)));
        this.loot.add(create(Material.GOLD_BOOTS, 1));
        this.loot.add(create(Material.BOW, 1));
        this.loot.add(create(Material.CHAINMAIL_HELMET, 1));
        this.loot.add(create(Material.APPLE, rndInt(1, 4)));
        this.loot.add(create(Material.IRON_HELMET, 1));
        this.loot.add(create(Material.GOLD_CHESTPLATE, 1));
        this.loot.add(create(Material.STONE_SWORD, 1));
        this.loot.add(create(Material.STONE_AXE, 1));
        this.loot.add(create(Material.FISHING_ROD, 1));
        this.loot.add(create(Material.IRON_CHESTPLATE, 1));
        this.loot.add(create(Material.BAKED_POTATO, rndInt(1, 5)));
        this.loot.add(create(Material.GOLDEN_APPLE, 1));
        this.loot.add(create(Material.CHAINMAIL_BOOTS, 1));
        this.loot.add(create(Material.STICK, rndInt(1, 3)));
        this.loot.add(create(Material.BAKED_POTATO, rndInt(1, 5)));
        this.loot.add(create(Material.CHAINMAIL_LEGGINGS, 1));
        this.loot.add(create(Material.MELON, rndInt(1, 5)));
        this.loot.add(create(Material.IRON_LEGGINGS, 1));
        this.loot.add(create(Material.APPLE, rndInt(1, 4)));
        this.loot.add(create(Material.IRON_BOOTS, 1));
        this.loot.add(create(Material.COOKED_CHICKEN, rndInt(1, 5)));
        this.loot.add(create(Material.CHAINMAIL_LEGGINGS, 1));
        this.loot.add(create(Material.COOKED_BEEF, rndInt(1, 5)));
        this.loot.add(create(Material.GOLD_BOOTS, 1));
        this.loot.add(create(Material.BOW, 1));
        this.loot.add(create(Material.WEB, rndInt(1, 4)));
        this.loot.add(create(Material.WOOD_SWORD, 1));
        this.loot.add(create(Material.STONE_SWORD, 1));
        this.loot.add(create(Material.DIAMOND_CHESTPLATE, 1));
        this.loot.add(create(Material.CHAINMAIL_HELMET, 1));
        this.loot.add(create(Material.APPLE, rndInt(1, 4)));
        this.loot.add(create(Material.IRON_HELMET, 1));
        this.loot.add(create(Material.GOLD_CHESTPLATE, 1));
        this.loot.add(create(Material.IRON_CHESTPLATE, 1));
        this.loot.add(create(Material.BAKED_POTATO, rndInt(1, 5)));
        this.loot.add(create(Material.IRON_SWORD, 1));
        this.loot.add(create(Material.CHAINMAIL_BOOTS, 1));
        this.loot.add(create(Material.ARROW, rndInt(1, 4)));
        this.loot.add(create(Material.BAKED_POTATO, rndInt(1, 5)));
        this.loot.add(create(Material.CHAINMAIL_LEGGINGS, 1));
        this.loot.add(create(Material.MELON, rndInt(1, 5)));
        this.loot.add(create(Material.STONE_SWORD, 1));
        this.loot.add(create(Material.IRON_LEGGINGS, 1));
        this.loot.add(create(Material.APPLE, rndInt(1, 4)));
        this.loot.add(create(Material.IRON_BOOTS, 1));
        this.loot.add(create(Material.FISHING_ROD, 1));
        this.loot.add(create(Material.COOKED_CHICKEN, rndInt(1, 5)));
        this.loot.add(create(Material.CHAINMAIL_LEGGINGS, 1));
        this.loot.add(create(Material.BOW, 1));
        this.loot.add(create(Material.COOKED_BEEF, rndInt(1, 5)));
        this.loot.add(create(Material.GOLD_BOOTS, 1));
        this.loot.add(create(Material.CHAINMAIL_HELMET, 1));
        this.loot.add(create(Material.APPLE, rndInt(1, 4)));
        this.loot.add(create(Material.IRON_HELMET, 1));
        this.loot.add(create(Material.GOLD_CHESTPLATE, 1));
        this.loot.add(create(Material.STONE_SWORD, 1));
        this.loot.add(create(Material.IRON_CHESTPLATE, 1));
        this.loot.add(create(Material.WEB, rndInt(1, 4)));
        this.loot.add(create(Material.ARROW, rndInt(1, 4)));
        this.loot.add(create(Material.BAKED_POTATO, rndInt(1, 5)));
        this.loot.add(create(Material.STONE_SWORD, 1));
        this.loot.add(create(Material.CHAINMAIL_BOOTS, 1));
        this.loot.add(create(Material.BAKED_POTATO, rndInt(1, 5)));
        this.loot.add(create(Material.CHAINMAIL_LEGGINGS, 1));
        this.loot.add(create(Material.MELON, rndInt(1, 5)));
        this.loot.add(create(Material.IRON_LEGGINGS, 1));
        this.loot.add(create(Material.BOW, 1));
        this.loot.add(create(Material.APPLE, rndInt(1, 4)));
        this.loot.add(create(Material.IRON_BOOTS, 1));
        this.loot.add(create(Material.COOKED_CHICKEN, rndInt(1, 5)));
        this.loot.add(create(Material.CHAINMAIL_LEGGINGS, 1));
        this.loot.add(create(Material.COOKED_BEEF, rndInt(1, 5)));
        this.loot.add(create(Material.GOLD_BOOTS, 1));
        this.loot.add(create(Material.WOOD_SWORD, 1));
        this.loot.add(create(Material.DIAMOND_CHESTPLATE, 1));
        this.loot.add(create(Material.CHAINMAIL_HELMET, 1));
        this.loot.add(create(Material.ARROW, rndInt(1, 4)));
        this.loot.add(create(Material.FISHING_ROD, 1));
        this.loot.add(create(Material.APPLE, rndInt(1, 4)));
        this.loot.add(create(Material.IRON_HELMET, 1));
        this.loot.add(create(Material.STONE_SWORD, 1));
        this.loot.add(create(Material.GOLD_CHESTPLATE, 1));
        this.loot.add(create(Material.IRON_CHESTPLATE, 1));
        this.loot.add(create(Material.BAKED_POTATO, rndInt(1, 5)));
        this.loot.add(create(Material.GOLDEN_APPLE, 1));
        this.loot.add(create(Material.WEB, rndInt(1, 4)));
        this.loot.add(create(Material.CHAINMAIL_BOOTS, 1));
        this.loot.add(create(Material.BOW, 1));
        this.loot.add(create(Material.BAKED_POTATO, rndInt(1, 5)));
        this.loot.add(create(Material.CHAINMAIL_LEGGINGS, 1));
        this.loot.add(create(Material.MELON, rndInt(1, 5)));
        this.loot.add(create(Material.STONE_SWORD, 1));
        this.loot.add(create(Material.IRON_LEGGINGS, 1));
        this.loot.add(create(Material.APPLE, rndInt(1, 4)));
        this.loot.add(create(Material.IRON_BOOTS, 1));
        this.loot.add(create(Material.COOKED_CHICKEN, rndInt(1, 5)));
        this.loot.add(create(Material.CHAINMAIL_LEGGINGS, 1));
        this.loot.add(create(Material.COOKED_BEEF, rndInt(1, 5)));
        this.loot.add(create(Material.GOLD_BOOTS, 1));
    }
    
    public ItemStack create(Material mat, int amount) {
        return new ItemStack(mat, amount);
    }

    public ItemStack createCustom(Material mat, int amount, short id, String display) {
        ItemStack it = new ItemStack(mat, amount, id);
        ItemMeta meta = it.getItemMeta();
        meta.setDisplayName(display);
        it.setItemMeta(meta);
        return it;
    }

    public int rndInt(int min, int max) {
        Random r = new Random();
        int i = r.nextInt(max - min + 1) + min;
        return i;
    }
}
