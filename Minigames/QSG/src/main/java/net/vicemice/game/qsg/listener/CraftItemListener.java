package net.vicemice.game.qsg.listener;

import net.vicemice.game.qsg.util.FlintUtil;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.CraftItemEvent;

public class CraftItemListener implements Listener {
    @EventHandler
    public void onCraftItem(CraftItemEvent craftItemEvent) {
        if (craftItemEvent.getCurrentItem().getType() == Material.FLINT_AND_STEEL) {
            craftItemEvent.setCurrentItem(FlintUtil.getFlintAndSteel());
        }
    }
}