package net.vicemice.game.qsg.player.party;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.player.team.GameTeam;
import net.vicemice.game.qsg.player.team.TeamManager;
import lombok.Getter;
import net.vicemice.hector.packets.types.player.party.PartyMemberPacket;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class PartyManager {
    @Getter
    private static HashMap<UUID, PartyMemberPacket> partyMembers = new HashMap<>();

    public static void receivePartyPacket(PartyMemberPacket partyMemberPacket) {
        List<IUser> onlinePlayers = new ArrayList<>();
        for (UUID member : partyMemberPacket.getMembers()) {
            if (UserManager.isConnected(member)) {
                IUser user = UserManager.getUser(member);
                onlinePlayers.add(user);
            }
            partyMembers.put(member, partyMemberPacket);
        }
        if (onlinePlayers.size() == partyMemberPacket.getMembers().size()) {
            allPlayersOnline(onlinePlayers);
        }
    }

    public static void allPlayersOnline(List<IUser> onlinePartyPlayers) {
        GameTeam gameTeam = null;
        for (int id : TeamManager.getTeams().keySet()) {
            GameTeam getTeam = TeamManager.getTeams().get(id);
            int space = QSG.getGameConfig().getPER_TEAM() - getTeam.getMembers().size();
            if (space >= onlinePartyPlayers.size()) {
                gameTeam = getTeam;
                break;
            }
        }
        for (IUser user : onlinePartyPlayers) {
            partyMembers.remove(user.getUniqueId());
            if (gameTeam != null) {
                gameTeam.playerJoin(user);
                //player.sendMessage(QSG.getLanguageManager().getLanguages().get(Skynet.getLanguages().get(player.getUniqueId())).getPARTY_TEAM_FOUND().replace("&", "§").replace("%PREFIX% ", QSG.getPrefix()));
            } else {
                //player.sendMessage(QSG.getLanguageManager().getLanguages().get(Skynet.getLanguages().get(player.getUniqueId())).getPARTY_TEAM_NOT_FOUND().replace("&", "§").replace("%PREFIX% ", QSG.getPrefix()));
            }
        }
    }
}