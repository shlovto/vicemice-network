package net.vicemice.game.qsg.listener;

import net.vicemice.game.qsg.player.spectator.SpectatorManager;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.gameapi.manager.GameState;
import net.vicemice.hector.server.player.UserManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class FoodLevelChangeListener implements Listener {
    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            if (GameManager.getGameState() != GameState.INGAME) {
                event.setCancelled(true);
            }
            if (SpectatorManager.getSpectatorPlayers().contains(UserManager.getUser(player))) {
                event.setCancelled(true);
            }
            if (GameManager.isPeace()) {
                event.setCancelled(true);
            }
        }
    }
}