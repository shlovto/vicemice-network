package net.vicemice.game.qsg.config.game;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.AccessLevel;
import lombok.Getter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/*
 * Class created at 13:25 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GameConfiguration {
    private File configFile;
    @Getter(AccessLevel.PUBLIC)
    private GameConfig gameConfig;
    private Gson gson;

    public GameConfiguration() {
        this.configFile = new File("cloud/qsg/config.json");
        this.gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
    }

    public boolean exists() {
        return configFile.exists();
    }

    public void writeConfiguration() {
        writeConfiguration(gameConfig);
    }

    public void writeConfiguration(GameConfig gameConfig) {
        try (FileWriter fileWriter = new FileWriter(configFile)) {
            gson.toJson(gameConfig, fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readConfiguration() {
        try (FileReader fileReader = new FileReader(configFile)) {
            gameConfig = gson.fromJson(fileReader, GameConfig.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
