package net.vicemice.game.qsg.player.team;

import net.vicemice.game.qsg.QSG;
import net.vicemice.game.qsg.game.ScoreboardManager;
import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;
import java.util.ArrayList;
import java.util.List;

public class GameTeam {
    @Getter
    private int number;
    @Getter
    private List<IUser> members = new ArrayList<>();
    @Getter
    @Setter
    private Location spawnLocation;

    public GameTeam(int number) {
        this.number = number;
        int viewNumber = number;
        viewNumber++;
        Team team = ScoreboardManager.getScoreboard().registerNewTeam("team_" + number);
        team.setAllowFriendlyFire(false);
        team.setPrefix("T#" + viewNumber + " | ");
        ScoreboardManager.getTeams().put(this, team);
    }

    public void playerJoin(IUser user) {
        if (members.contains(user)) {
            //ALREADY-IN-THIS-TEAM
            return;
        }
        if (members.size() >= QSG.getGameConfig().getPER_TEAM()) {
            //TEAM-IS-FULL
            return;
        }
        TeamManager.removePlayerFromTeams(user);
        members.add(user);
        int viewNumber = number;
        viewNumber++;
        //YOU-IN-TEAM
    }
}