package net.vicemice.fallback.listener;

import net.vicemice.hector.server.player.permissions.listener.CloudChatEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class Event_CloudChatEvent implements Listener {

    @EventHandler
    public void onCloudChatEvent(CloudChatEvent event) {
        event.setCancelled(true);
    }
}