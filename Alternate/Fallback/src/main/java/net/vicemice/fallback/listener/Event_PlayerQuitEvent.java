package net.vicemice.fallback.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class Event_PlayerQuitEvent implements Listener {

    @EventHandler
    public void on(PlayerQuitEvent e) {
        e.setQuitMessage(null);
    }
}

