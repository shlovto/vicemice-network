package net.vicemice.fallback.listener;

import net.vicemice.fallback.Fallback;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.plugin.Plugin;

public class Event_WeatherChangeEvent
implements Listener {
    public Event_WeatherChangeEvent() {
        Fallback.getInstance().getServer().getPluginManager().registerEvents((Listener)this, (Plugin)Fallback.getInstance());
    }

    @EventHandler
    public void on(WeatherChangeEvent e) {
        e.setCancelled(true);
    }
}

