package net.vicemice.fallback.listener;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.lobby.LobbyJoinPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.utils.PlayerRankData;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Event_PlayerJoinEvent implements Listener {

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent e) {
        Player player = e.getPlayer();

        e.setJoinMessage(null);

        PlayerRankData rankData = GoAPI.getUserAPI().getRankData(player.getUniqueId());

        for (Player all : Bukkit.getOnlinePlayers()) {
            all.hidePlayer(player);
            player.hidePlayer(all);
        }

        player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 99999999, 999));
        player.teleport(new Location(Bukkit.getWorld("world"), -3.5, 14.0, 48.5, -64.5f, 0.0f));

        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.LOBBY_JOIN_PACKET, new LobbyJoinPacket(player.getName())), API.PacketReceiver.SLAVE);
        //GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.SERVER_CONNECT, new ConnectPacket(player.getName(), "BuildEvent")), API.PacketReceiver.SLAVE);

        //all.sendMessage(Clyde.getPrefix() + "§cDie Verbindung zu einem SERVER_NAME Server dauert länger als gewöhnlich...");
        //GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.SERVER_CONNECT, new ConnectPacket(player.getName(), "SERVER_NAME")), API.PacketReceiver.SLAVE);

        player.setGameMode(GameMode.ADVENTURE);
    }
}