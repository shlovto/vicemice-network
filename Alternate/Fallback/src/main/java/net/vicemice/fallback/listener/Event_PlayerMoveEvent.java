package net.vicemice.fallback.listener;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class Event_PlayerMoveEvent implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        Location loc = new Location(Bukkit.getWorld("world"), -3.5, 14.0, 48.5, -64.5f, 0.0f);
        Location to = e.getTo();
        if (loc.getBlockX() != to.getBlockX() || loc.getBlockZ() != to.getBlockZ()) {
            e.setCancelled(true);
        }
    }
}