package net.vicemice.fallback.tasks;

import net.vicemice.fallback.Fallback;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.lobby.LobbyJoinPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.API;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class AutoConnector {

    public static void startAutoConnector() {
        new BukkitRunnable() {

            public void run() {
                if (Bukkit.getOnlinePlayers().size() > 0) {
                    for (Player all : Bukkit.getOnlinePlayers()) {
                        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.LOBBY_JOIN_PACKET, new LobbyJoinPacket(all.getName())), API.PacketReceiver.SLAVE);
                        //GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.SERVER_CONNECT, new ConnectPacket(all.getName(), "BuildEvent")), API.PacketReceiver.SLAVE);

                        //all.sendMessage(Clyde.getPrefix() + "§cDie Verbindung zu einem SERVER_NAME Server dauert länger als gewöhnlich...");
                        //GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.SERVER_CONNECT, new ConnectPacket(all.getName(), "SERVER_NAME")), API.PacketReceiver.SLAVE);
                    }
                }
            }
        }.runTaskTimer(Fallback.getInstance(), 600, 600);
    }
}