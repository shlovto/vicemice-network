package net.vicemice.fallback;

import net.vicemice.fallback.tasks.AutoConnector;
import net.vicemice.hector.utils.players.api.actionbat.ActionBarAPI;
import net.vicemice.hector.utils.Register;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;

public class Fallback extends JavaPlugin {

    private static Fallback instance;
    public void onLoad() {
        instance = this;
    }

    public void onEnable() {
        for (World world : Bukkit.getWorlds()) {
            world.setTime(14000);
            world.setThundering(false);
            world.setStorm(false);
            world.setSpawnLocation(-4, 14, 48);
        }
        AutoConnector.startAutoConnector();

        Register.registerListener(this, "net.vicemice.fallback.listener");
        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> ActionBarAPI.sendActionBar("§cWarte auf Lobby Server..."), 0L, 20L);
    }

    public static Fallback getInstance() {
        return instance;
    }
}