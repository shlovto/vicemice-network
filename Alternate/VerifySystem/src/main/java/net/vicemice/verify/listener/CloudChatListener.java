package net.vicemice.verify.listener;

import net.vicemice.hector.server.player.permissions.listener.CloudChatEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class CloudChatListener implements Listener {

    @EventHandler
    public void onChat(CloudChatEvent event) {
        if(!event.getMessage().startsWith("/forum") && !event.getMessage().startsWith("/verify") && !event.getMessage().startsWith("/unverify")) {
            event.setCancelled(true);
        }
    }
}