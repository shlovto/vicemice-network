package net.vicemice.verify.listener;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveListener implements Listener {

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent playerMoveEvent) {
        Player player = playerMoveEvent.getPlayer();

        if(PlayerJoinListener.verifyHandlerHashMap.containsKey(player.getUniqueId())) {
            Location loc = new Location(Bukkit.getWorld("world"), -3.5, 14.0, 48.5, -64.5f, 0.0f);
            Location to = playerMoveEvent.getTo();
            if (loc.getBlockX() != to.getBlockX() || loc.getBlockZ() != to.getBlockZ()) {
                playerMoveEvent.setCancelled(true);
            }
        }
    }
}