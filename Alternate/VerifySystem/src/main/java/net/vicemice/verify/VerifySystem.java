package net.vicemice.verify;

import net.vicemice.hector.utils.Register;
import net.vicemice.verify.listener.PlayerJoinListener;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.java.JavaPlugin;
import java.util.HashMap;
import java.util.UUID;

public class VerifySystem extends JavaPlugin {

    @Getter
    public static VerifySystem instance;
    @Getter
    public static HashMap<UUID, Long> banned;
    @Getter
    public static Location spawn = new Location(Bukkit.getWorld("world"), -3.5, 14.0, 48.5, -64.5f, 0.0f);

    @Override
    public void onEnable() {
        for (World world : Bukkit.getWorlds()) {
            world.setTime(14000);
            world.setThundering(false);
            world.setStorm(false);
            world.setSpawnLocation(-4, 14, 48);
        }

        banned = new HashMap<>();
        instance = this;

        Register.registerListener(this, "net.vicemice.verify.listener");
        PlayerJoinListener.verifyHandlerHashMap = new HashMap<>();
        Bukkit.getWorld("world").setSpawnLocation(-4, 14, 48);
    }
}