package net.vicemice.verify.handlers;

import net.vicemice.verify.VerifySystem;
import net.vicemice.verify.listener.PlayerJoinListener;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class VerifyHandler {

    public VerifyHandler(Player player) {
        this.player = player;

        startTimer();
    }

    @Getter
    private Player player;
    @Getter
    @Setter
    private int verifyCount = 600;

    public void startTimer() {
        Bukkit.getServer().getScheduler().runTaskTimer(VerifySystem.getInstance(), () -> {
            //getPlayer().setLevel(verifyCount);

            /*if(verifyCount != 1) {
                ActionBarAPI.sendActionBar(player, "§cDu hast noch §e" + verifyCount + " Sekunden§c.");
            }*/

            if(verifyCount == 0) {
                PlayerJoinListener.verifyHandlerHashMap.remove(player.getUniqueId());

                getPlayer().kickPlayer("§cDu wurdest nach §e10 Minuten §cvom Server geworfen. \n §cFalls du dich wieder verifizieren möchtest, join einfach erneut.");
            }/* else if(verifyCount == 45 || verifyCount == 30 || verifyCount == 15 || verifyCount == 10 || verifyCount == 5 || verifyCount == 4 || verifyCount == 3 || verifyCount == 2) {
                player.sendMessage("§cEs verbleiben §e" + verifyCount + " Sekunden §cbevor du gekickt wirst.");
            } else if(verifyCount == 1) {
                getPlayer().sendMessage("§cEs verbleibt §e" + verifyCount + "§c Sekunde bevor du gekickt wirst.");
                player.sendMessage("§cEs verbleibt §e" + verifyCount + " Sekunde §cbevor du gekickt wirst.");
            }*/

            verifyCount--;
        }, 20L, 20L);
    }
}