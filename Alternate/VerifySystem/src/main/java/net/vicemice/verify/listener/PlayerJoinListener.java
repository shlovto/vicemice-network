package net.vicemice.verify.listener;

import net.vicemice.verify.VerifySystem;
import net.vicemice.verify.handlers.VerifyHandler;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffectType;
import java.util.HashMap;
import java.util.UUID;

public class PlayerJoinListener implements Listener {

    public static HashMap<UUID, VerifyHandler> verifyHandlerHashMap;

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        if(VerifySystem.getBanned().containsKey(player.getUniqueId())) {
             VerifySystem.getBanned().remove(player.getUniqueId());
        }

        event.setJoinMessage(null);

        player.addPotionEffect(PotionEffectType.BLINDNESS.createEffect(1000000000, 1));

        verifyHandlerHashMap.put(player.getUniqueId(), new VerifyHandler(player));

        for(Player players : Bukkit.getOnlinePlayers()) {
            player.hidePlayer(players);
            players.hidePlayer(player);
        }

        player.teleport(VerifySystem.getSpawn());
    }
}