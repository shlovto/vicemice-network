/*
 * Decompiled with CFR 0.149.
 */
package net.vicemice.mctv.replay;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.zip.GZIPOutputStream;
import javax.annotation.Nullable;
import net.vicemice.mctv.MCTV;
import org.kamranzafar.jtar.TarEntry;
import org.kamranzafar.jtar.TarOutputStream;

public class MapManager {
    private Map<String, String> mapHashes = new HashMap<String, String>();
    private Set<String> disableAutoAdd = new HashSet<String>();

    public void disableAutoAdd(String world) {
        this.disableAutoAdd.add(world);
    }

    private String hashWorld(String folder) {
        File worldFolder = new File(folder);
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            final ArrayList<String> fileNames = new ArrayList<>();
            Files.walkFileTree(worldFolder.toPath(), (FileVisitor<? super Path>)new FileVisitor<Path>(){

                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Path parent;
                    if (file.endsWith("level.dat")) {
                        fileNames.add(file.toFile().getAbsolutePath());
                    }
                    if ((parent = file.getParent()).endsWith("region") || parent.endsWith("data")) {
                        fileNames.add(file.toFile().getAbsolutePath());
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    return FileVisitResult.CONTINUE;
                }
            });
            Collections.sort(fileNames);
            for (String fileName : fileNames) {
                int nread;
                FileInputStream fis = new FileInputStream(new File(fileName));
                byte[] dataBytes = new byte[1024];
                while ((nread = fis.read(dataBytes)) != -1) {
                    md.update(dataBytes, 0, nread);
                }
            }
            byte[] mdbytes = md.digest();
            StringBuilder sb = new StringBuilder("");
            for (byte mdbyte : mdbytes) {
                sb.append(Integer.toString((mdbyte & 0xFF) + 256, 16).substring(1));
            }
            System.out.println("World " + folder + " hashed to " + sb.toString());
            return sb.toString();
        }
        catch (IOException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void add(String name, final String folder) {
        final String hash = this.hashWorld(folder);
        this.mapHashes.put(name, hash);
        ListenableFuture<Boolean> isWorldStoredFuture = MCTV.getInstance().getStorage().isWorldStored(hash);
        Futures.addCallback(isWorldStoredFuture, new FutureCallback<Boolean>(){

            @Override
            public void onSuccess(@Nullable Boolean aBoolean) {
                if (aBoolean == null || !aBoolean.booleanValue()) {
                    File worldSave = new File(MCTV.getInstance() != null ? MCTV.getInstance().getDataFolder() : new File("."), hash + ".tar.gz");
                    try (TarOutputStream tarOutputStream = new TarOutputStream(new GZIPOutputStream(new FileOutputStream(worldSave)));){
                        MapManager.this.tarFolder(null, folder, tarOutputStream);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
            }
        });
    }

    public void storeWorlds() {
        for (String hash : this.mapHashes.values()) {
            int tries;
            File worldSave = new File(MCTV.getInstance() != null ? MCTV.getInstance().getDataFolder() : new File("."), hash + ".tar.gz");
            if (!worldSave.exists()) continue;
            for (tries = 0; tries < 5; ++tries) {
                ListenableFuture<Void> worldStoreFuture = MCTV.getInstance().getStorage().storeWorld(worldSave, hash);
                try {
                    worldStoreFuture.get();
                    break;
                }
                catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                    continue;
                }
            }
            if (tries >= 5) {
                // empty if block
            }
            if (worldSave.delete()) continue;
            worldSave.deleteOnExit();
        }
    }

    public void tarFolder(String parent, String path, TarOutputStream out) throws IOException {
        BufferedInputStream origin = null;
        File f = new File(path);
        String[] files = f.list();
        if (files == null) {
            files = new String[]{f.getName()};
        }
        parent = parent == null ? "" : parent + f.getName() + "/";
        for (String file : files) {
            int count;
            TarEntry entry;
            System.out.println("Adding: " + file);
            File fe = f;
            byte[] data = new byte[1024];
            if (f.isDirectory()) {
                fe = new File(f, file);
            }
            if (fe.isDirectory()) {
                String[] fl = fe.list();
                if (fl != null && fl.length != 0) {
                    this.tarFolder(parent, fe.getPath(), out);
                    continue;
                }
                entry = new TarEntry(fe, parent + file + "/");
                out.putNextEntry(entry);
                continue;
            }
            FileInputStream fi = new FileInputStream(fe);
            origin = new BufferedInputStream(fi);
            entry = new TarEntry(fe, parent + file);
            out.putNextEntry(entry);
            while ((count = origin.read(data)) != -1) {
                out.write(data, 0, count);
            }
            out.flush();
            origin.close();
        }
    }

    public int size() {
        return this.mapHashes.size();
    }

    public Set<Map.Entry<String, String>> entrySet() {
        return this.mapHashes.entrySet();
    }

    public Set<String> getDisableAutoAdd() {
        return this.disableAutoAdd;
    }
}

