package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerWorldBorder;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import net.vicemice.hector.api.protocol.wrappers.EnumWrappers;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.WorldBorder;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="WorldBorderMeta", opcode=33, adapter= WorldBorderMeta.WorldBorderMetaAdapter.class)
public class WorldBorderMeta implements Meta {
    private String world;
    private byte action;
    private int portalTeleportBoundary;
    private double centerX;
    private double centerZ;
    private double oldCenterX;
    private double oldCenterZ;
    private double oldSize;
    private double size;
    private long speed;
    private int warningTime;
    private int warningDistance;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        out.writeUTF(this.world);
        out.writeByte(this.action);
        DataStreamer.writeVarInt(this.portalTeleportBoundary, out);
        out.writeDouble(this.centerX);
        out.writeDouble(this.centerZ);
        out.writeDouble(this.oldSize);
        out.writeDouble(this.size);
        DataStreamer.writeVarLong(this.speed, out);
        DataStreamer.writeVarInt(this.warningTime, out);
        DataStreamer.writeVarInt(this.warningDistance, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.world = in.readUTF();
        this.action = in.readByte();
        this.portalTeleportBoundary = DataStreamer.readVarInt(in);
        this.centerX = in.readDouble();
        this.centerZ = in.readDouble();
        this.oldSize = in.readDouble();
        this.size = in.readDouble();
        this.speed = DataStreamer.readVarLong(in);
        this.warningTime = DataStreamer.readVarInt(in);
        this.warningDistance = DataStreamer.readVarInt(in);
    }

    @Override
    public void execute(Context context) {
        WorldBorder border = Bukkit.getWorld((String)this.world).getWorldBorder();
        EnumWrappers.WorldBorderAction action = EnumWrappers.WorldBorderAction.values()[this.action];
        if (action == EnumWrappers.WorldBorderAction.SET_SIZE) {
            this.oldSize = border.getSize();
            border.setSize(this.size);
        }
        if (action == EnumWrappers.WorldBorderAction.LERP_SIZE) {
            this.oldSize = border.getSize();
            border.setSize(this.size, this.speed);
        }
        if (action == EnumWrappers.WorldBorderAction.SET_CENTER) {
            this.oldCenterX = border.getCenter().getBlockX();
            this.oldCenterZ = border.getCenter().getBlockZ();
            border.setCenter(this.centerX, this.centerZ);
        }
    }

    @Override
    public void rollback(Context context) {
        WorldBorder border = Bukkit.getWorld((String)this.world).getWorldBorder();
        EnumWrappers.WorldBorderAction action = EnumWrappers.WorldBorderAction.values()[this.action];
        if (action == EnumWrappers.WorldBorderAction.SET_SIZE) {
            border.setSize(this.oldSize);
        }
        if (action == EnumWrappers.WorldBorderAction.LERP_SIZE) {
            border.setSize(this.oldSize, this.speed);
        }
        if (action == EnumWrappers.WorldBorderAction.SET_CENTER) {
            border.setCenter(this.centerX, this.centerZ);
        }
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    @ConstructorProperties(value={"world", "action", "portalTeleportBoundary", "centerX", "centerZ", "oldCenterX", "oldCenterZ", "oldSize", "size", "speed", "warningTime", "warningDistance"})
    public WorldBorderMeta(String world, byte action, int portalTeleportBoundary, double centerX, double centerZ, double oldCenterX, double oldCenterZ, double oldSize, double size, long speed, int warningTime, int warningDistance) {
        this.world = world;
        this.action = action;
        this.portalTeleportBoundary = portalTeleportBoundary;
        this.centerX = centerX;
        this.centerZ = centerZ;
        this.oldCenterX = oldCenterX;
        this.oldCenterZ = oldCenterZ;
        this.oldSize = oldSize;
        this.size = size;
        this.speed = speed;
        this.warningTime = warningTime;
        this.warningDistance = warningDistance;
    }

    public WorldBorderMeta() {
    }

    public String getWorld() {
        return this.world;
    }

    public byte getAction() {
        return this.action;
    }

    public int getPortalTeleportBoundary() {
        return this.portalTeleportBoundary;
    }

    public double getCenterX() {
        return this.centerX;
    }

    public double getCenterZ() {
        return this.centerZ;
    }

    public double getOldCenterX() {
        return this.oldCenterX;
    }

    public double getOldCenterZ() {
        return this.oldCenterZ;
    }

    public double getOldSize() {
        return this.oldSize;
    }

    public double getSize() {
        return this.size;
    }

    public long getSpeed() {
        return this.speed;
    }

    public int getWarningTime() {
        return this.warningTime;
    }

    public int getWarningDistance() {
        return this.warningDistance;
    }

    public void setWorld(String world) {
        this.world = world;
    }

    public void setAction(byte action) {
        this.action = action;
    }

    public void setPortalTeleportBoundary(int portalTeleportBoundary) {
        this.portalTeleportBoundary = portalTeleportBoundary;
    }

    public void setCenterX(double centerX) {
        this.centerX = centerX;
    }

    public void setCenterZ(double centerZ) {
        this.centerZ = centerZ;
    }

    public void setOldCenterX(double oldCenterX) {
        this.oldCenterX = oldCenterX;
    }

    public void setOldCenterZ(double oldCenterZ) {
        this.oldCenterZ = oldCenterZ;
    }

    public void setOldSize(double oldSize) {
        this.oldSize = oldSize;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public void setSpeed(long speed) {
        this.speed = speed;
    }

    public void setWarningTime(int warningTime) {
        this.warningTime = warningTime;
    }

    public void setWarningDistance(int warningDistance) {
        this.warningDistance = warningDistance;
    }

    public static final class WorldBorderMetaAdapter
    implements MetaAdapter {
        private final ReplayFileWriter replayFileWriter;
        private PacketAdapter packetAdapter;

        public WorldBorderMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            this.packetAdapter = new PacketAdapter((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.WORLD_BORDER}){

                public void onPacketSending(PacketEvent event) {
                    WrapperPlayServerWorldBorder worldBorder = new WrapperPlayServerWorldBorder(event.getPacket());
                    WorldBorderMeta m = new WorldBorderMeta();
                    m.setAction((byte)worldBorder.getAction().ordinal());
                    m.setCenterZ(worldBorder.getCenterZ());
                    m.setCenterX(worldBorder.getCenterX());
                    m.setWarningDistance(worldBorder.getWarningDistance());
                    m.setWarningTime(worldBorder.getWarningTime());
                    m.setSpeed(worldBorder.getSpeed());
                    m.setOldSize(worldBorder.getOldRadius());
                    m.setSize(worldBorder.getRadius());
                    m.setPortalTeleportBoundary(worldBorder.getPortalTeleportBoundary());
                    m.setWorld(event.getPlayer().getWorld().getName());
                    replayFileWriter.addMeta(m);
                }
            };
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this.packetAdapter);
        }

        @Override
        public synchronized void stop() {
            if (this.packetAdapter != null) {
                ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this.packetAdapter);
                this.packetAdapter = null;
            }
        }
    }
}

