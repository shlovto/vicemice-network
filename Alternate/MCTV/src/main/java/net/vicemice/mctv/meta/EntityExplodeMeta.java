package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

@MetaInfo(name="EntityExplodeMeta", opcode=27, adapter= EntityExplodeMeta.EntityExplodeMetaAdapter.class)
public class EntityExplodeMeta
implements Meta {
    private String world;
    private Location location;
    private float yield;
    private List<Vector> brokenBlocks;
    private Map<Vector, MaterialData> oldBlocks;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        out.writeDouble(this.location.getX());
        out.writeDouble(this.location.getY());
        out.writeDouble(this.location.getZ());
        out.writeFloat(this.yield);
        DataStreamer.writeString(this.location.getWorld().getName(), out);
        DataStreamer.writeVarInt(this.brokenBlocks.size(), out);
        for (Vector brokenBlock : this.brokenBlocks) {
            DataStreamer.writeVarInt(brokenBlock.getBlockX(), out);
            DataStreamer.writeVarInt(brokenBlock.getBlockY(), out);
            DataStreamer.writeVarInt(brokenBlock.getBlockZ(), out);
        }
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.location = new Location(null, in.readDouble(), in.readDouble(), in.readDouble());
        this.yield = in.readFloat();
        if (version >= 5) {
            this.world = DataStreamer.readString(in);
            this.brokenBlocks = new ArrayList<Vector>();
            int blockDestroySize = DataStreamer.readVarInt(in);
            for (int i = 0; i < blockDestroySize; ++i) {
                int x = DataStreamer.readVarInt(in);
                int y = DataStreamer.readVarInt(in);
                int z = DataStreamer.readVarInt(in);
                this.brokenBlocks.add(new Vector(x, y, z));
            }
        }
    }

    @Override
    public void execute(Context context) {
        World world = context.getMasterPlayer().getWorld();
        if (this.world != null) {
            world = Bukkit.getWorld((String)this.world);
        }
        this.location.setWorld(world);
        this.location.getWorld().createExplosion(this.location.getX(), this.location.getY(), this.location.getZ(), this.yield, false, false);
        this.oldBlocks = new HashMap<Vector, MaterialData>();
        for (Vector brokenBlock : this.brokenBlocks) {
            Block currentBlock = brokenBlock.toLocation(world).getBlock();
            this.oldBlocks.put(brokenBlock, new MaterialData(currentBlock.getType(), currentBlock.getData()));
            currentBlock.setType(Material.AIR);
        }
    }

    @Override
    public void rollback(Context context) {
        if (context.getVersion() >= 5) {
            World world = context.getMasterPlayer().getWorld();
            if (this.world != null) {
                world = Bukkit.getWorld((String)this.world);
            }
            for (Map.Entry<Vector, MaterialData> entry : this.oldBlocks.entrySet()) {
                Block currentBlock = entry.getKey().toLocation(world).getBlock();
                currentBlock.setType(entry.getValue().getItemType());
                currentBlock.setData(entry.getValue().getData());
            }
            this.oldBlocks = null;
        }
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public EntityExplodeMeta() {
    }

    @ConstructorProperties(value={"world", "location", "yield", "brokenBlocks", "oldBlocks"})
    public EntityExplodeMeta(String world, Location location, float yield, List<Vector> brokenBlocks, Map<Vector, MaterialData> oldBlocks) {
        this.world = world;
        this.location = location;
        this.yield = yield;
        this.brokenBlocks = brokenBlocks;
        this.oldBlocks = oldBlocks;
    }

    public static class EntityExplodeMetaAdapter
    implements MetaAdapter,
    Listener {
        private ReplayFileWriter replayFileWriter;

        public EntityExplodeMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            Bukkit.getPluginManager().registerEvents((Listener)this, (Plugin)MCTV.getInstance());
        }

        @Override
        public void stop() {
        }

        @EventHandler(ignoreCancelled=true, priority=EventPriority.MONITOR)
        public void onEntityExplode(EntityExplodeEvent e) {
            ArrayList<Vector> brokenBlocks = new ArrayList<Vector>();
            for (Block block : e.blockList()) {
                brokenBlocks.add(block.getLocation().toVector());
            }
            EntityExplodeMeta meta = new EntityExplodeMeta(null, e.getLocation(), e.getYield(), brokenBlocks, null);
            this.replayFileWriter.addMeta(meta);
        }
    }
}

