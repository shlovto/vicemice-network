package net.vicemice.mctv.inventory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import net.vicemice.hector.api.async.Callback;
import net.vicemice.hector.api.inventory.api.InventoryMenu;
import net.vicemice.hector.api.inventory.api.InventoryOption;
import net.vicemice.hector.api.inventory.api.option.ActionOption;
import net.vicemice.hector.api.util.JavaUtil;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.replay.ScoreboardManager;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Team;

public class Teleporter {
    public static void open(Player player) {
        int position = 0;
        ArrayList<EntityPlayer> list = new ArrayList<EntityPlayer>(MCTV.getInstance().getReplaySession().getContext().getPlayerManager().getPlayers());
        if (list.isEmpty()) {
            MCTV.getInstance().sendMessage(player, "no-players-to-teleport", new Object[0]);
            return;
        }
        list.sort(new Comparator<EntityPlayer>(){

            @Override
            public int compare(EntityPlayer o1, EntityPlayer o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        ArrayList<InventoryOption> options = new ArrayList<InventoryOption>();
        for (EntityPlayer entityPlayer : list) {
            if (!Teleporter.addEntityPlayer(options, entityPlayer, position)) continue;
            ++position;
        }
        InventoryMenu inventoryMenu = new InventoryMenu("Spieler", JavaUtil.addTo(9, options.size()));
        for (InventoryOption inventoryOption : options) {
            inventoryMenu.addOption(inventoryOption);
        }
        inventoryMenu.open(player);
    }

    private static boolean addEntityPlayer(List<InventoryOption> list, final EntityPlayer entityPlayer, int position) {
        Context context = MCTV.getInstance().getReplaySession().getContext();
        ScoreboardManager scoreboard = MCTV.getInstance().getReplaySession().getContext().getVersion() >= 7 ? context.getScoreboard(context.getTeleportToAfterSkipTime()) : context.getScoreboard();
        Team team = null;
        if (scoreboard.getScoreboard() != null) {
            for (Team t : scoreboard.getScoreboard().getTeams()) {
                if (!t.hasEntry(entityPlayer.getName())) continue;
                team = t;
                break;
            }
        }
        if (team != null && team.getPrefix() != null && team.getPrefix().startsWith("\u00a77") && !team.getPrefix().contains("Replay")) {
            return false;
        }
        list.add(new ActionOption(new Callback<Player>(){

            @Override
            public void done(Player player) {
                player.teleport(new Location((World)entityPlayer.getWorld().getWorld(), entityPlayer.locX, entityPlayer.locY, entityPlayer.locZ));
                if (MCTV.getInstance().getApi().getContext().getVersion() >= 7) {
                    MCTV.getInstance().getApi().getContext().getScoreboard(entityPlayer.getUniqueID()).addPlayer(player);
                    if (player.hasPotionEffect(PotionEffectType.BLINDNESS)) {
                        player.removePotionEffect(PotionEffectType.BLINDNESS);
                    }
                }
            }
        }).setItemStack(Teleporter.createSkull(entityPlayer)).setPosition(position).setName(team == null ? entityPlayer.getName() : team.getPrefix() + entityPlayer.getName() + team.getSuffix()));
        return true;
    }

    private static ItemStack createSkull(EntityPlayer entityPlayer) {
        ItemStack itemStack = new ItemStack(Material.SKULL_ITEM, 1);
        SkullMeta skullMeta = (SkullMeta)itemStack.getItemMeta();
        skullMeta.setOwner(entityPlayer.getName());
        itemStack.setItemMeta((ItemMeta)skullMeta);
        return itemStack;
    }
}

