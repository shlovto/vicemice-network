package net.vicemice.mctv.config.replay;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.AccessLevel;
import lombok.Getter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/*
 * Class created at 23:42 - 18.04.2020
 * Copyright (C) elrobtossohn
 */
public class ReplayConfiguration {
    private File configFile;
    @Getter(AccessLevel.PUBLIC)
    private ReplayConfig replayConfig;
    private Gson gson;

    public ReplayConfiguration() {
        this.configFile = new File("cloud/replay/config.json");
        this.gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
    }

    public boolean exists() {
        return configFile.exists();
    }

    public void writeConfiguration() {
        writeConfiguration(replayConfig);
    }

    public void writeConfiguration(ReplayConfig replayConfig) {
        try (FileWriter fileWriter = new FileWriter(configFile)) {
            gson.toJson(replayConfig, fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readConfiguration() {
        try (FileReader fileReader = new FileReader(configFile)) {
            replayConfig = gson.fromJson(fileReader, ReplayConfig.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
