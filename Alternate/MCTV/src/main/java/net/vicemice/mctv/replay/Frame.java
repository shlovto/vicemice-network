package net.vicemice.mctv.replay;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.meta.api.MetaRegistry;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.meta.api.MetaRegistry;

public class Frame {
    private final List<Meta> metas = new ArrayList<Meta>();
    private Context context;
    private long number;
    private String lastMeta;

    public Frame(Context context, long number) {
        this.context = context;
        this.number = number;
    }

    public Frame() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void save(DataOutputStream out) {
        List<Meta> list = this.metas;
        synchronized (list) {
            DataStreamer.writeVarInt(this.metas.size(), out);
            for (Meta meta : this.metas) {
                int opcode = meta.getClass().getAnnotation(MetaInfo.class).opcode();
                try {
                    DataStreamer.writeVarInt(opcode, out);
                    meta.writeMeta(out);
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void load(DataInputStream in) throws IOException {
        try {
            int size;
            int n = size = this.context.getVersion() < 2 ? in.readInt() : DataStreamer.readVarInt(in);
            if (MCTV.getInstance() == null) {
                System.out.println("Frame #" + this.number);
            }
            for (int i = size; i > 0; --i) {
                int opcode = this.context.getVersion() < 2 ? in.readInt() : DataStreamer.readVarInt(in);
                Class<? extends Meta> clazz = MetaRegistry.lookup(opcode);
                if (clazz == null) {
                    System.out.println("Corrupted replay");
                    if (this.lastMeta == null) continue;
                    System.out.println("Last meta: " + this.lastMeta);
                    for (Meta meta : this.metas) {
                        System.out.println(meta);
                    }
                    continue;
                }
                if (MCTV.getInstance() == null) {
                    System.out.println("Meta class: " + clazz.getName());
                }
                Meta meta = clazz.newInstance();
                meta.readMeta(in, this.context.getVersion());
                this.lastMeta = meta.toString();
                List<Meta> list = this.metas;
                synchronized (list) {
                    this.metas.add(meta);
                    continue;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        if (MCTV.getInstance() == null) {
            System.out.println(" ");
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addMeta(Meta meta) {
        List<Meta> list = this.metas;
        synchronized (list) {
            this.metas.add(meta);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void execute(boolean skipping) {
        List<Meta> list = this.metas;
        synchronized (list) {
            for (Meta meta : this.metas) {
                try {
                    this.context.setLoggerPrefix("[Replay] [Meta " + meta.getClass().getSimpleName() + "] ");
                    if (skipping && meta.canBeSkipped()) continue;
                    meta.execute(this.context);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void rollback(boolean skipping) {
        List<Meta> list = this.metas;
        synchronized (list) {
            for (int i = this.metas.size(); i > 0; --i) {
                try {
                    Meta meta = this.metas.get(i - 1);
                    if (skipping && meta.canBeSkipped()) continue;
                    meta.rollback(this.context);
                    continue;
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public int getMetaAmount() {
        return this.metas.size();
    }

    public long getNumber() {
        return this.number;
    }
}

