/*
 * Decompiled with CFR 0.149.
 */
package net.vicemice.mctv.meta.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value={ElementType.TYPE})
@Retention(value=RetentionPolicy.RUNTIME)
public @interface MetaInfo {
    public String name() default "unnamed";

    public int opcode();

    public Class<?> adapter();
}

