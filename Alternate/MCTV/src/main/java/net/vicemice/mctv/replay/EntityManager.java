package net.vicemice.mctv.replay;

import java.util.HashMap;
import java.util.Map;
import net.vicemice.mctv.replay.entity.FakeFishRod;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class EntityManager {
    private Map<Integer, Entity> mappedEntities = new HashMap<Integer, Entity>();
    private Map<Integer, TNTPrimed> tntPassengers = new HashMap<Integer, TNTPrimed>();
    private Map<Integer, FakeFishRod> fishingRods = new HashMap<Integer, FakeFishRod>();
    private boolean currentlyDropping;
    private Map<Integer, Map<Integer, Object>> metadata = new HashMap<Integer, Map<Integer, Object>>();

    public void despawn(int entityId) {
        TNTPrimed tntPassenger;
        Entity entity = this.mappedEntities.get(entityId);
        if (entity != null) {
            entity.remove();
            this.mappedEntities.remove(entityId);
        }
        if ((tntPassenger = this.tntPassengers.get(entityId)) != null) {
            tntPassenger.remove();
            this.tntPassengers.remove(entityId);
        }
    }

    public Location getLocation(int entityId) {
        Entity entity = this.mappedEntities.get(entityId);
        if (entity == null) {
            return null;
        }
        return entity.getLocation();
    }

    public void teleport(int entityId, Location location) {
        Entity entity = this.mappedEntities.get(entityId);
        if (entity == null) {
            return;
        }
        entity.teleport(location);
    }

    public void spawnItem(int entityId, World world, ItemStack item, Vector vector, Vector velocity) {
        if (vector == null || item == null) {
            return;
        }
        this.currentlyDropping = true;
        Item item1 = world.dropItem(new Location(world, vector.getX(), vector.getY(), vector.getZ()), item);
        this.mappedEntities.put(entityId, (Entity)item1);
        this.currentlyDropping = false;
        if (velocity != null) {
            item1.setVelocity(velocity);
        }
    }

    public Entity spawnLivingEntity(int entityId, EntityType entityType, Location location, int color, boolean tnt) {
        Entity spawnedEntity = location.getWorld().spawnEntity(location, entityType);
        if (entityType == EntityType.SHEEP) {
            ((Sheep)spawnedEntity).setColor(DyeColor.values()[color]);
            if (tnt) {
                TNTPrimed tntPrimed = (TNTPrimed)location.getWorld().spawnEntity(location.clone().add(0.0, 0.8, 0.0), EntityType.PRIMED_TNT);
                tntPrimed.setFuseTicks(Integer.MAX_VALUE);
                spawnedEntity.setPassenger((Entity)tntPrimed);
                this.tntPassengers.put(entityId, tntPrimed);
            }
        }
        this.setNoAI(spawnedEntity);
        this.mappedEntities.put(entityId, spawnedEntity);
        return spawnedEntity;
    }

    public Entity getSpawnedLivingEntity(int entityId, EntityType entityType, Location location) {
        Entity spawnedEntity = location.getWorld().spawnEntity(location, entityType);
        this.setNoAI(spawnedEntity);
        this.mappedEntities.put(entityId, spawnedEntity);
        return spawnedEntity;
    }

    private void setNoAI(Entity bukkitEntity) {
        net.minecraft.server.v1_8_R3.Entity nmsEntity = ((CraftEntity)bukkitEntity).getHandle();
        NBTTagCompound tag = nmsEntity.getNBTTag();
        if (tag == null) {
            tag = new NBTTagCompound();
        }
        nmsEntity.c(tag);
        tag.setInt("NoAI", 1);
        nmsEntity.f(tag);
    }

    public void removeFishingHook(int id) {
        this.fishingRods.remove(id);
    }

    public void addFishingHook(int id, FakeFishRod rod) {
        FakeFishRod oldRod = this.fishingRods.remove(id);
        if (oldRod != null) {
            oldRod.die();
        }
        this.fishingRods.put(id, rod);
    }

    public boolean containsItem(int entityId) {
        if (this.currentlyDropping) {
            return true;
        }
        Entity entity = this.mappedEntities.get(entityId);
        return entity != null && entity.getType() == EntityType.DROPPED_ITEM;
    }

    public Entity getEntityById(int id) {
        return this.mappedEntities.get(id);
    }

    public void setMetadata(int entityId, Integer key, Object value) {
        Map<Integer, Object> metadata = this.metadata.get(entityId);
        if (metadata != null) {
            metadata.put(key, value);
        }
    }

    public Object getMetadata(int entityId, Integer key) {
        Map<Integer, Object> metadata = this.metadata.get(entityId);
        if (metadata != null) {
            return metadata.get(key);
        }
        return null;
    }
}

