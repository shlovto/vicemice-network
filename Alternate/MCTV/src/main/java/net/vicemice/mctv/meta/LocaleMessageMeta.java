package net.vicemice.mctv.meta;

import com.google.common.base.Charsets;
import net.vicemice.hector.server.GoServer;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.meta.api.VoidMetaAdapter;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Locale;

/*
 * Class created at 22:17 - 19.04.2020
 * Copyright (C) elrobtossohn
 */
@MetaInfo(name="LocaleMessageMeta", opcode=41, adapter= VoidMetaAdapter.class)
public class LocaleMessageMeta implements Meta {
    private String message;
    private String localeTag;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeString(this.message, Charsets.UTF_8, out);
        DataStreamer.writeString(this.localeTag, Charsets.UTF_8, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.message = DataStreamer.readString(Charsets.UTF_8, in);
        this.localeTag = DataStreamer.readString(Charsets.UTF_8, in);
    }

    @Override
    public void execute(final Context context) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            Locale playerLocale = GoServer.getService().getPlayerManager().getPlayer(player.getUniqueId()).getLocale();
            String localeTagPlayer = playerLocale.toLanguageTag();

            System.out.println("message -> "+this.message);
            System.out.println("localeTag -> "+this.localeTag);
            System.out.println("localeTagPlayer -> "+localeTagPlayer);

            if (localeTagPlayer.equalsIgnoreCase(localeTag)) {
                player.sendMessage(this.message);
            }
        }
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    @ConstructorProperties(value={"message", "localeTag"})
    public LocaleMessageMeta(String message, String localeTag) {
        this.message = message;
        this.localeTag = localeTag;
    }

    public LocaleMessageMeta() {
    }

    public String getMessage() {
        return this.message;
    }

    public String getLocaleTag() {
        return localeTag;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setLocaleTag(String localeTag) {
        this.localeTag = localeTag;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof LocaleMessageMeta)) {
            return false;
        }
        LocaleMessageMeta other = (LocaleMessageMeta)o;
        if (!other.canEqual(this)) {
            return false;
        }
        String this$message = this.getMessage();
        String other$message = other.getMessage();
        if (this$message == null ? other$message != null : !this$message.equals(other$message)) {
            return false;
        }
        String this$localeTag = this.getLocaleTag();
        String other$localeTag = other.getLocaleTag();
        return !(this$localeTag == null ? other$localeTag != null : !((Object)this$localeTag).equals(other$localeTag));
    }

    protected boolean canEqual(Object other) {
        return other instanceof MessageMeta;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        String $message = this.getMessage();
        result = result * 59 + ($message == null ? 43 : $message.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "LocaleMessageMeta{" +
                "message='" + message + '\'' +
                ", localeTag='" + localeTag + '\'' +
                '}';
    }
}