package net.vicemice.mctv.listener;

import net.vicemice.mctv.MCTV;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;

public class ChatTabCompleteListener
implements Listener {
    @EventHandler
    public void onChatTabComplete(PlayerChatTabCompleteEvent event) {
        event.getTabCompletions().clear();
        if (event.getLastToken() != null) {
            String lowerCaseToken = event.getLastToken().toLowerCase();
            for (EntityPlayer entityPlayer : MCTV.getInstance().getReplaySession().getContext().getPlayerManager().getPlayers()) {
                if (!entityPlayer.getName().toLowerCase().startsWith(lowerCaseToken) || event.getTabCompletions().contains(entityPlayer.getName())) continue;
                event.getTabCompletions().add(entityPlayer.getName());
            }
        }
    }
}

