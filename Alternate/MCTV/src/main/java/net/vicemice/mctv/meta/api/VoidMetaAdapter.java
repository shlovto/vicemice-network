package net.vicemice.mctv.meta.api;

import java.beans.ConstructorProperties;
import net.vicemice.mctv.io.writer.ReplayFileWriter;

public class VoidMetaAdapter
implements MetaAdapter {
    private final ReplayFileWriter writer;

    @Override
    public void start() {
    }

    @Override
    public void stop() {
    }

    @ConstructorProperties(value={"writer"})
    public VoidMetaAdapter(ReplayFileWriter writer) {
        this.writer = writer;
    }
}

