package net.vicemice.mctv.io.writer;

import com.google.common.util.concurrent.ListenableFuture;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import net.vicemice.hector.server.GoServer;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.storage.Storage;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.meta.api.MetaRegistry;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.replay.Frame;
import org.apache.commons.io.IOUtils;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

public class ReplayFileWriter {
    private final Plugin plugin;
    private final Context context;
    private AtomicBoolean shouldSave = new AtomicBoolean(false);
    private DataOutputStream dataOutputStream = null;
    private List<MetaAdapter> adapters = new ArrayList<MetaAdapter>();
    private Frame currentFrame;
    private BukkitTask task;
    private final BlockingQueue<Frame> writeQueue = new LinkedBlockingQueue<Frame>();
    private AtomicBoolean writing = new AtomicBoolean(true);
    private int recordedFrames = 0;
    private Thread saveThread;

    public ReplayFileWriter(Plugin plugin, Context context) {
        this.plugin = plugin;
        this.context = context;
    }

    public boolean isRecording() {
        return this.writing.get();
    }

    public void startRecording() {
        Object privateServerModule;
        try {
            this.dataOutputStream = new DataOutputStream(new GZIPOutputStream(new FileOutputStream(new File(this.plugin.getDataFolder(), MCTV.getInstance().getName() + ".dat.gz"))));
        }
        catch (IOException e) {
            e.printStackTrace();
            return;
        }
        /*if (GoFrame.getInstance().getModule("private-server") != null && (privateServerModule = (PrivateServerModule)GoFrame.getInstance().getModule("private-server")).isPrivateServer()) {
            MCTV.getInstance().getReplaySession().getContext().getMetadata().put("private-server", String.valueOf(true));
            MCTV.getInstance().getReplaySession().getContext().getMetadata().put("server-owner", privateServerModule.getOwner().toString());
        }*/
        this.saveThread = new Thread(new Runnable(){

            @Override
            public void run() {
                while (ReplayFileWriter.this.writing.get()) {
                    try {
                        Frame frame = (Frame)ReplayFileWriter.this.writeQueue.poll(50L, TimeUnit.MILLISECONDS);
                        if (frame == null) continue;
                        this.saveFrame(frame);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                for (Frame frame : ReplayFileWriter.this.writeQueue) {
                    this.saveFrame(frame);
                }
            }

            private void saveFrame(Frame frame) {
                try {
                    ReplayFileWriter.this.dataOutputStream.write(1);
                    frame.save(ReplayFileWriter.this.dataOutputStream);
                    ReplayFileWriter.this.dataOutputStream.flush();
                    ReplayFileWriter.this.recordedFrames++;
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        this.saveThread.start();
        this.nextFrame();
        for (Class f : MetaRegistry.getAll()) {
            MetaInfo column = (MetaInfo) f.getAnnotation(MetaInfo.class);
            try {
                this.adapters.add((MetaAdapter)column.adapter().getConstructor(ReplayFileWriter.class).newInstance(this));
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        for (MetaAdapter metaAdapter : this.adapters) {
            metaAdapter.start();
        }
        Runnable runnable = new Runnable(){

            @Override
            public void run() {
                ReplayFileWriter.this.nextFrame();
            }
        };
        this.task = Bukkit.getScheduler().runTaskTimer(this.plugin, runnable, 0L, 1L);
    }

    public void stopRecording() {
        this.task.cancel();
        for (MetaAdapter adapter : this.adapters) {
            adapter.stop();
        }
        this.writeQueue.add(this.currentFrame);
    }

    public Frame nextFrame() {
        if (this.currentFrame != null) {
            this.writeQueue.add(this.currentFrame);
        }
        this.currentFrame = new Frame();
        return this.currentFrame;
    }

    private String emptyIfNull(String s) {
        return s == null ? "" : s;
    }

    public void save() {
        if (this.dataOutputStream != null && this.shouldSave.get()) {
            this.writing.set(false);
            try {
                this.saveThread.join();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                this.dataOutputStream.write(0);
                this.dataOutputStream.flush();
                this.dataOutputStream.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            this.context.getMetadata().put("recorded-frames", String.valueOf(this.recordedFrames));
            this.context.getMetadata().put("record-start", Long.toString(System.currentTimeMillis()));
            if (Bukkit.getPluginManager().isPluginEnabled("Hector")) {
                this.context.getMetadata().put("server-id", this.emptyIfNull(GoServer.getService().getServerName()));
            }
            Bukkit.getLogger().info("Storing worlds");
            this.context.getMapManager().storeWorlds();
            try {
                int tries;
                Bukkit.getLogger().info("Rewriting file");
                File sourceFile = new File(this.plugin.getDataFolder(), MCTV.getInstance().getName() + ".dat.gz");
                File targetFile = new File(this.plugin.getDataFolder(), "upload.dat.gz");
                DataInputStream from = new DataInputStream(new GZIPInputStream(new FileInputStream(sourceFile)));
                DataOutputStream to = new DataOutputStream(new GZIPOutputStream(new FileOutputStream(targetFile)));
                DataStreamer.writeVarInt(15, to);
                Bukkit.getLogger().info("Writing replay version 15");
                DataStreamer.writeVarInt(this.context.getMetadata().size(), to);
                for (Map.Entry<String, String> stringStringEntry : this.context.getMetadata().entrySet()) {
                    DataStreamer.writeString(stringStringEntry.getKey(), to);
                    DataStreamer.writeString(stringStringEntry.getValue(), to);
                }
                Bukkit.getLogger().info("Writing maps15");
                DataStreamer.writeVarInt(this.context.getMapManager().size(), to);
                for (Map.Entry<String, String> stringStringEntry : this.context.getMapManager().entrySet()) {
                    DataStreamer.writeString(stringStringEntry.getKey(), to);
                    DataStreamer.writeString(stringStringEntry.getValue(), to);
                }
                Bukkit.getLogger().info("Copying to temp file 15");
                IOUtils.copy((InputStream)from, (OutputStream)to);
                Bukkit.getLogger().info("Flushing");
                to.flush();
                to.close();
                from.close();
                Bukkit.getLogger().info("Flushed!");
                Storage storage = MCTV.getInstance().getStorage();
                Bukkit.getLogger().info("Saving replay to default storage");
                for (tries = 0; tries < 5; ++tries) {
                    ListenableFuture<Void> storeReplayFuture = storage.storeReplay(targetFile);
                    try {
                        storeReplayFuture.get(storage.getStorageTimeout(), TimeUnit.SECONDS);
                        break;
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                        continue;
                    }
                }
                if (tries >= 5) {
                    // empty if block
                }
                sourceFile.deleteOnExit();
                targetFile.deleteOnExit();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public int getRecordedSeconds() {
        this.shouldSave.set(true);
        return Math.round((float)this.recordedFrames / 20.0f);
    }

    public void addMeta(Meta meta) {
        if (this.currentFrame != null) {
            this.currentFrame.addMeta(meta);
        }
    }

    public Context getContext() {
        return this.context;
    }
}

