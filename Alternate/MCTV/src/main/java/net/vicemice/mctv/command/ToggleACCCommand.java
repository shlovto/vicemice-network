package net.vicemice.mctv.command;

import com.google.common.util.concurrent.Futures;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.Rank;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.data.VerboseDisplayType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.entity.Player;

public class ToggleACCCommand extends Command {

    public ToggleACCCommand(MCTV mctv) {
        super("toggleac");
        this.mctv = mctv;
        ((CraftServer) mctv.getServer()).getCommandMap().register("toggleac", this);
    }

    private MCTV mctv;

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] args) {
        Player player = (Player)commandSender;
        if (GoAPI.getUserAPI().getRankData(player.getUniqueId()).getAccess_level() < Rank.MODERATOR.getAccessLevel()) {
            this.mctv.sendMessage(player, "command-no-permission", new Object[0]);
            return true;
        }
        if (args.length == 0) {
            this.mctv.sendMessage(player, "command-toggleac-usage", new Object[0]);
            return true;
        }
        try {
            VerboseDisplayType verboseDisplayType = VerboseDisplayType.valueOf(args[0].toUpperCase());
            this.mctv.sendMessage(player, "command-toggleac-success", verboseDisplayType.name());
            this.mctv.getReplaySession().getContext().getAccVisibility().put(player.getUniqueId(), Futures.immediateFuture(verboseDisplayType));
        }
        catch (IllegalArgumentException ignored) {
            this.mctv.sendMessage(player, "command-toggleac-usage", new Object[0]);
        }
        return true;
    }
}

