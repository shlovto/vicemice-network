package net.vicemice.mctv.replay.entity;

import api.profiles.HttpProfileRepository;
import api.profiles.Profile;
import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerEntityMetadata;
import net.vicemice.hector.api.protocol.wrappers.WrappedWatchableObject;
import com.mojang.authlib.GameProfile;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import net.vicemice.hector.utils.game.utils.UUIDUtil;
import net.vicemice.mctv.MCTV;
import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityEquipment;
import net.minecraft.server.v1_8_R3.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import net.minecraft.server.v1_8_R3.PlayerInteractManager;
import net.minecraft.server.v1_8_R3.TileEntitySkull;
import net.minecraft.server.v1_8_R3.WorldServer;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

public class FakePlayerManager {
    private final Map<UUID, Integer> entityIdByUUID = new HashMap<UUID, Integer>();
    private final Map<Integer, EntityPlayer> fakePlayers = new HashMap<Integer, EntityPlayer>();
    private final HttpProfileRepository profileRepository = new HttpProfileRepository("minecraft");
    private final Map<UUID, GameProfile> profileCache = new HashMap<UUID, GameProfile>();
    private final Map<Integer, Map<Integer, Object>> metadata = new HashMap<Integer, Map<Integer, Object>>();
    private final Map<Integer, Integer> firstSeen = new HashMap<Integer, Integer>();
    private final Map<UUID, Long> respawnTimers = new HashMap<UUID, Long>();
    private final Map<UUID, net.minecraft.server.v1_8_R3.ItemStack[]> armorContents = new HashMap<UUID, net.minecraft.server.v1_8_R3.ItemStack[]>();

    public int getEntityIdByUUID(UUID uuid, final org.bukkit.World world) {
        return this.entityIdByUUID.computeIfAbsent(uuid, new Function<UUID, Integer>(){

            @Override
            public Integer apply(UUID uuid) {
                return FakePlayerManager.this.spawnFakePlayer(uuid, world);
            }
        });
    }

    public void setItem(UUID uuid, int slot, ItemStack itemStack) {
        if (!this.entityIdByUUID.containsKey(uuid)) {
            return;
        }
        EntityPlayer entityPlayer = this.fakePlayers.get(this.entityIdByUUID.get(uuid));
        if (entityPlayer != null) {
            if (slot == 0) {
                entityPlayer.inventory.setCarried(CraftItemStack.asNMSCopy((ItemStack)itemStack));
            } else {
                entityPlayer.inventory.armor[slot - 1] = CraftItemStack.asNMSCopy((ItemStack)itemStack);
                net.minecraft.server.v1_8_R3.ItemStack[] armorContent = new net.minecraft.server.v1_8_R3.ItemStack[4];
                if (this.armorContents.containsKey(uuid)) {
                    armorContent = this.armorContents.get(uuid);
                    armorContent[slot - 1] = CraftItemStack.asNMSCopy((ItemStack)itemStack);
                    this.armorContents.replace(uuid, armorContent);
                } else {
                    armorContent[slot - 1] = CraftItemStack.asNMSCopy((ItemStack)itemStack);
                    this.armorContents.put(uuid, armorContent);
                }
            }
        }
    }

    public boolean doesPlayerExist(int id) {
        return this.fakePlayers.containsKey(id);
    }

    public ItemStack getItemStack(UUID uuid, int slot) {
        if (!this.entityIdByUUID.containsKey(uuid)) {
            return null;
        }
        EntityPlayer entityPlayer = this.fakePlayers.get(this.entityIdByUUID.get(uuid));
        if (entityPlayer != null) {
            if (slot == 0) {
                return CraftItemStack.asCraftMirror((net.minecraft.server.v1_8_R3.ItemStack)entityPlayer.inventory.getCarried());
            }
            net.minecraft.server.v1_8_R3.ItemStack itemStack = entityPlayer.inventory.armor[slot - 1];
            if (itemStack == null) {
                return null;
            }
            return CraftItemStack.asCraftMirror((net.minecraft.server.v1_8_R3.ItemStack)itemStack);
        }
        return null;
    }

    public void updateLocation(UUID uuid, double x, double y, double z, float yaw, float pitch) {
        if (!this.entityIdByUUID.containsKey(uuid)) {
            return;
        }
        EntityPlayer entityPlayer = this.fakePlayers.get(this.entityIdByUUID.get(uuid));
        if (entityPlayer != null) {
            entityPlayer.setLocation(x, y, z, yaw, pitch);
        }
    }

    public Vector getLocationVector(UUID uuid) {
        if (!this.entityIdByUUID.containsKey(uuid)) {
            return null;
        }
        EntityPlayer entityPlayer = this.fakePlayers.get(this.entityIdByUUID.get(uuid));
        if (entityPlayer != null) {
            return new Vector(entityPlayer.locX, entityPlayer.locY, entityPlayer.locZ);
        }
        return null;
    }

    public String getName(UUID uuid) {
        if (!this.entityIdByUUID.containsKey(uuid)) {
            return null;
        }
        EntityPlayer entityPlayer = this.fakePlayers.get(this.entityIdByUUID.get(uuid));
        if (entityPlayer != null) {
            return entityPlayer.getName();
        }
        return null;
    }

    public void sendPlayerList(Player player, boolean addOrRemove) {
        for (EntityPlayer entityPlayer : this.fakePlayers.values()) {
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket((Packet)new PacketPlayOutPlayerInfo(addOrRemove ? PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER : PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, new EntityPlayer[]{entityPlayer}));
        }
    }

    public void despawnFakePlayer(UUID uuid) {
        if (!this.entityIdByUUID.containsKey(uuid)) {
            return;
        }
        EntityPlayer entityPlayer = this.fakePlayers.remove(this.entityIdByUUID.remove(uuid));
        if (entityPlayer != null) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                ((CraftPlayer)player).getHandle().playerConnection.sendPacket((Packet)new PacketPlayOutEntityDestroy(new int[]{entityPlayer.getId()}));
                ((CraftPlayer)player).getHandle().playerConnection.sendPacket((Packet)new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, new EntityPlayer[]{entityPlayer}));
            }
        }
    }

    private GameProfile getGameProfile(UUID uuid) {
        return this.profileCache.computeIfAbsent(uuid, uuid1 -> {
            try {
                Profile profile = FakePlayerManager.this.profileRepository.findProfileByUUID(uuid1);
                return new GameProfile(UUIDUtil.fromFlatString(profile.getId()), profile.getName());
            }
            catch (Exception e1) {
                e1.printStackTrace();
                return null;
            }
        });
    }

    private int spawnFakePlayer(UUID uuid, org.bukkit.World world) {
        GameProfile gameProfile = this.getGameProfile(uuid);
        return this.spawnFakePlayer(gameProfile, world);
    }

    private int spawnFakePlayer(GameProfile gameProfile, org.bukkit.World world) {
        EntityPlayer fakeEntity = this.createRealFakeEntity(gameProfile, world);
        if (fakeEntity != null) {
            fakeEntity.setPosition(0.0, 0.0, 0.0);
            fakeEntity.yaw = 0.0f;
            fakeEntity.pitch = 0.0f;
            fakeEntity.getDataWatcher().watch(10, (byte)-1);
            HashMap<Integer, Object> metadata = new HashMap<>();
            metadata.put(0, (byte)0);
            metadata.put(1, (short)300);
            metadata.put(3, (byte)0);
            metadata.put(2, "");
            metadata.put(4, (byte)0);
            metadata.put(10, (byte)-1);
            this.metadata.put(fakeEntity.getId(), metadata);
            for (Player player : Bukkit.getOnlinePlayers()) {
                ((CraftPlayer)player).getHandle().playerConnection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, fakeEntity));
            }
            return fakeEntity.getId();
        }
        return -1;
    }

    public void spawnFakePlayer(UUID uuid) {
        if (!this.entityIdByUUID.containsKey(uuid)) {
            return;
        }
        int entityId = this.entityIdByUUID.get(uuid);
        EntityPlayer entityPlayer = this.fakePlayers.get(entityId);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!player.getWorld().equals((Object)entityPlayer.getWorld().getWorld())) continue;
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket((Packet)new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, new EntityPlayer[]{entityPlayer}));
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket((Packet)new PacketPlayOutNamedEntitySpawn((EntityHuman)entityPlayer));
            this.refreshEquipment((CraftPlayer)player, entityId, entityPlayer);
            Map<Integer, Object> metaData = this.metadata.get(entityId);
            if (metaData == null) continue;
            WrapperPlayServerEntityMetadata packet = new WrapperPlayServerEntityMetadata();
            ArrayList<WrappedWatchableObject> wrappedWatchableObjects = new ArrayList<WrappedWatchableObject>();
            for (Map.Entry<Integer, Object> integerObjectEntry : metaData.entrySet()) {
                if (integerObjectEntry.getValue() == null) continue;
                wrappedWatchableObjects.add(new WrappedWatchableObject(integerObjectEntry.getKey().intValue(), integerObjectEntry.getValue()));
            }
            packet.setEntityID(entityId);
            packet.setMetadata(wrappedWatchableObjects);
        }
    }

    private EntityPlayer createRealFakeEntity(GameProfile gameProfile, org.bukkit.World bukkitWorld) {
        WorldServer world = ((CraftWorld)bukkitWorld).getHandle();
        try {
            gameProfile = TileEntitySkull.skinCache.get(gameProfile.getName());
        }
        catch (ExecutionException e) {
            Bukkit.getLogger().severe("Nickname " + gameProfile.getName() + " is corrupted");
            return null;
        }

        GameProfile gameProfile1 = new GameProfile(UUID.randomUUID(), gameProfile.getName());
        gameProfile1.getProperties().putAll(gameProfile.getProperties());

        EntityPlayer fakeEntity = new EntityPlayer(MinecraftServer.getServer(), world, gameProfile, new PlayerInteractManager(world));
        this.fakePlayers.put(fakeEntity.getId(), fakeEntity);
        this.firstSeen.put(fakeEntity.getId(), MCTV.getInstance().getApi().getContext().getPlayedFrames());
        System.out.println("Creating new entity '" + gameProfile.getName() + "' for world " + world.getWorld().getName());
        return fakeEntity;
    }

    public void spawnPlayerFor(UUID uuid, Player player) {
        Integer entityId = this.entityIdByUUID.get(uuid);
        if (entityId == null) {
            return;
        }
        EntityPlayer entityPlayer = this.fakePlayers.get(entityId);
        if (entityPlayer.getWorld().getWorld().equals((Object)player.getWorld())) {
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket((Packet)new PacketPlayOutNamedEntitySpawn((EntityHuman)entityPlayer));
            this.refreshEquipment((CraftPlayer)player, entityId, entityPlayer);
            Map<Integer, Object> metaData = this.metadata.get(entityId);
            if (metaData != null) {
                WrapperPlayServerEntityMetadata packet = new WrapperPlayServerEntityMetadata();
                ArrayList<WrappedWatchableObject> wrappedWatchableObjects = new ArrayList<WrappedWatchableObject>();
                for (Map.Entry<Integer, Object> integerObjectEntry : metaData.entrySet()) {
                    if (integerObjectEntry.getValue() == null) continue;
                    wrappedWatchableObjects.add(new WrappedWatchableObject(integerObjectEntry.getKey().intValue(), integerObjectEntry.getValue()));
                }
                packet.setEntityID(entityId.intValue());
                packet.setMetadata(wrappedWatchableObjects);
                packet.sendPacket(player);
            }
        }
    }

    public void refreshEquipment(CraftPlayer player, int entityId, EntityPlayer entityPlayer) {
        PlayerConnection playerConnection = player.getHandle().playerConnection;
        playerConnection.sendPacket((Packet)new PacketPlayOutEntityEquipment(entityId, 0, entityPlayer.inventory.getCarried()));
        playerConnection.sendPacket((Packet)new PacketPlayOutEntityEquipment(entityId, 1, this.armorContents.containsKey(entityPlayer.getUniqueID()) ? this.armorContents.get(entityPlayer.getUniqueID())[0] : null));
        playerConnection.sendPacket((Packet)new PacketPlayOutEntityEquipment(entityId, 2, this.armorContents.containsKey(entityPlayer.getUniqueID()) ? this.armorContents.get(entityPlayer.getUniqueID())[1] : null));
        playerConnection.sendPacket((Packet)new PacketPlayOutEntityEquipment(entityId, 3, this.armorContents.containsKey(entityPlayer.getUniqueID()) ? this.armorContents.get(entityPlayer.getUniqueID())[2] : null));
        playerConnection.sendPacket((Packet)new PacketPlayOutEntityEquipment(entityId, 4, this.armorContents.containsKey(entityPlayer.getUniqueID()) ? this.armorContents.get(entityPlayer.getUniqueID())[3] : null));
    }

    public void despawnPlayerFor(UUID uuid, Player player) {
        EntityPlayer entityPlayer = this.fakePlayers.get(this.entityIdByUUID.get(uuid));
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket((Packet)new PacketPlayOutEntityDestroy(new int[]{entityPlayer.getId()}));
    }

    public Collection<EntityPlayer> getPlayers() {
        return this.fakePlayers.values();
    }

    public String getWorldName(UUID uuid) {
        if (!this.entityIdByUUID.containsKey(uuid)) {
            return null;
        }
        EntityPlayer entityPlayer = this.fakePlayers.get(this.entityIdByUUID.get(uuid));
        return entityPlayer.getWorld().getWorld().getName();
    }

    public void changeWorld(final UUID uuid, final org.bukkit.World world) {
        if (this.entityIdByUUID.containsKey(uuid)) {
            EntityPlayer entityPlayer = this.fakePlayers.get(this.entityIdByUUID.get(uuid));
            if (!entityPlayer.world.getWorld().equals((Object)world)) {
                System.out.println("Changing world for entity to " + world.getName());
                entityPlayer.world = ((CraftWorld)world).getHandle();
            }
        }
        Bukkit.getScheduler().runTaskLater((Plugin)MCTV.getInstance(), new Runnable(){

            @Override
            public void run() {
                if (FakePlayerManager.this.entityIdByUUID.containsKey(uuid)) {
                    int entityId = (Integer)FakePlayerManager.this.entityIdByUUID.get(uuid);
                    EntityPlayer entityPlayer = (EntityPlayer)FakePlayerManager.this.fakePlayers.get(entityId);
                    if (entityPlayer != null) {
                        for (Player player : Bukkit.getOnlinePlayers()) {
                            if (!player.getWorld().getName().equals(world.getName())) continue;
                            FakePlayerManager.this.refreshEquipment((CraftPlayer)player, (Integer)FakePlayerManager.this.entityIdByUUID.get(uuid), entityPlayer);
                        }
                    }
                }
            }
        }, 5L);
    }

    public void setMetadata(int entityId, Integer key, Object value) {
        Map<Integer, Object> metadata = this.metadata.get(entityId);
        if (metadata != null) {
            metadata.put(key, value);
        }
    }

    public Object getMetadata(int entityId, Integer key) {
        Map<Integer, Object> metadata = this.metadata.get(entityId);
        if (metadata != null) {
            return metadata.get(key);
        }
        return null;
    }

    public int getFirstSeen(int entityId) {
        Integer firstSeen = this.firstSeen.get(entityId);
        return firstSeen == null ? -1 : firstSeen;
    }

    public void sendSlots(int entityId, Player player) {
        EntityPlayer entityPlayer = this.fakePlayers.get(entityId);
        if (entityPlayer.getWorld().getWorld().equals((Object)player.getWorld())) {
            this.refreshEquipment((CraftPlayer)player, entityId, entityPlayer);
        }
    }

    public boolean isRespawnedRecently(UUID uuid) {
        Long lastRespawn = this.respawnTimers.get(uuid);
        return lastRespawn != null && System.currentTimeMillis() - lastRespawn < 100L;
    }

    public void respawn(UUID uuid) {
        this.respawnTimers.put(uuid, System.currentTimeMillis());
    }

    public Map<UUID, Integer> getEntityIdByUUID() {
        return this.entityIdByUUID;
    }

    public Map<Integer, EntityPlayer> getFakePlayers() {
        return this.fakePlayers;
    }

    public HttpProfileRepository getProfileRepository() {
        return this.profileRepository;
    }

    public Map<UUID, GameProfile> getProfileCache() {
        return this.profileCache;
    }

    public Map<Integer, Map<Integer, Object>> getMetadata() {
        return this.metadata;
    }

    public Map<Integer, Integer> getFirstSeen() {
        return this.firstSeen;
    }

    public Map<UUID, Long> getRespawnTimers() {
        return this.respawnTimers;
    }

    public Map<UUID, net.minecraft.server.v1_8_R3.ItemStack[]> getArmorContents() {
        return this.armorContents;
    }
}

