package net.vicemice.mctv.io.reader;

import com.google.common.util.concurrent.ListenableFuture;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.zip.GZIPInputStream;
import net.vicemice.hector.GoAPI;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.buffer.DoubledPlaybackBuffer;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.replay.Frame;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ReplayFileReader {
    private UUID replay;
    private File replayFile;
    private ReadState state;
    private final Context context;
    private DoubledPlaybackBuffer buffer;
    private boolean readVersion = true;
    private long number = 0L;
    private ListenableFuture<InputStream> download;
    private DataInputStream dis;
    private AtomicBoolean read = new AtomicBoolean(true);

    public ReplayFileReader(UUID replay, final Context context) {
        this.replay = replay;
        this.context = context;
        this.state = ReadState.NOT_STARTED;
        try {
            this.download = MCTV.getInstance().getStorage().getReplay(replay, aDouble -> {
                System.out.println("Replay loaded: " + aDouble);
                if (context.getFileDLProgress() != null) {
                    context.getFileDLProgress().accept(aDouble);
                }
            });
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ReplayFileReader(File replay, Context context) {
        this.replayFile = replay;
        this.context = context;
        this.state = ReadState.NOT_STARTED;
    }

    public Map<String, String> readMeta() {
        if (this.state == ReadState.NOT_STARTED) {
            this.buildupDataInput(this.readVersion);
        }
        if (this.state == ReadState.ERROR) {
            return null;
        }
        try {
            HashMap<String, String> meta = new HashMap<String, String>();
            int metaSize = DataStreamer.readVarInt(this.dis);
            for (int i = 0; i < metaSize; ++i) {
                String key = DataStreamer.readString(this.dis);
                String value = DataStreamer.readString(this.dis);
                meta.put(key, value);
            }
            this.state = ReadState.META_READ;
            return meta;
        }
        catch (Exception e) {
            if (!this.readVersion) {
                throw e;
            }
            this.readVersion = false;
            this.state = ReadState.NOT_STARTED;
            return this.readMeta();
        }
    }

    public Map<String, String> readMapMeta() {
        if (this.state != ReadState.META_READ) {
            throw new IllegalStateException("META_READ is not reached. You forget to read general Metadata");
        }
        this.state = ReadState.MAP_META_READ;
        try {
            boolean isPrivateServer;
            HashMap<String, String> maps = new HashMap<String, String>();
            int mapSize = this.context.getVersion() < 2 ? this.dis.readInt() : DataStreamer.readVarInt(this.dis);
            for (int i = 0; i < mapSize; ++i) {
                String key = this.context.getVersion() < 2 ? this.dis.readUTF() : DataStreamer.readString(this.dis);
                String value = this.context.getVersion() < 2 ? this.dis.readUTF() : DataStreamer.readString(this.dis);
                maps.put(key, value);
            }
            if (this.context.getMetadata().containsKey("recorded-frames")) {
                this.context.setFramesRecorded(Integer.parseInt(this.context.getMetadata().get("recorded-frames")));
                int seconds = Math.round((float)this.context.getFramesRecorded() / 20.0f);
                MCTV.getInstance().broadcastLocalized("replay-duration", seconds);
            }
            if (this.context.getMetadata().containsKey("record-start")) {
                final long start = Long.parseLong(this.context.getMetadata().get("record-start"));
                for (final Player player : Bukkit.getOnlinePlayers()) {
                    DateFormat dateFormat = DateFormat.getDateTimeInstance(2, 3, GoAPI.getUserAPI().getLocale(player.getUniqueId()));
                    if (player.isOnline()) {
                        MCTV.getInstance().sendMessage(player, "replay-recorded-on", dateFormat.format(start));
                    }
                }
            }
            boolean bl = isPrivateServer = this.context.getMetadata().containsKey("private-server") && Boolean.parseBoolean(this.context.getMetadata().get("private-server"));
            if (this.context.getMetadata().containsKey("server-id") && this.context.getMetadata().containsKey("cloud-type")) {
                MCTV.getInstance().broadcastLocalized("replay-game-mode", (isPrivateServer ? "P" : "") + this.context.getMetadata().get("cloud-type"), this.context.getMetadata().get("server-id"));
            }
            /*if (isPrivateServer && this.context.getMetadata().containsKey("server-owner")) {
                UUID owner;
                UserCacheModule userCacheModule = (UserCacheModule)GoFrame.getInstance().getModule("usercache");
                String name = userCacheModule.getName(owner = UUID.fromString(this.context.getMetadata().get("server-owner"))) != null ? (String)userCacheModule.getName(owner).get() : owner.toString();
                MCTV.getInstance().broadcastLocalized("private-server-notification", name);
            }*/
            int lengthInSeconds = (int)Math.floor(this.context.getFramesRecorded() / 20);
            this.buffer = new DoubledPlaybackBuffer(lengthInSeconds, TimeUnit.SECONDS, lengthInSeconds, TimeUnit.SECONDS);
            new Thread(new Runnable(){

                @Override
                public void run() {
                    ReplayFileReader.this.checkForNextFrame();
                    ReplayFileReader.this.checkForBufferRead();
                }
            }).start();
            return maps;
        }
        catch (IOException e) {
            this.state = ReadState.ERROR;
            e.printStackTrace();
        }
        return null;
    }

    public Frame getNextFrame() {
        if (!this.buffer.isEmpty()) {
            return this.buffer.popFront();
        }
        return null;
    }

    private void checkForBufferRead() {
        while (this.state == ReadState.FRAME_AVAILABLE && this.read.get()) {
            if (this.buffer.isFull()) continue;
            try {
                Frame frame = new Frame(this.context, this.number++);
                frame.load(this.dis);
                this.buffer.putBack(frame);
                this.checkForNextFrame();
            }
            catch (IOException e) {
                this.state = ReadState.ERROR;
                e.printStackTrace();
            }
        }
    }

    private void checkForNextFrame() {
        try {
            this.state = this.dis.read() == 1 ? ReadState.FRAME_AVAILABLE : ReadState.EOF;
        }
        catch (IOException e) {
            this.state = ReadState.ERROR;
            e.printStackTrace();
        }
    }

    private void buildupDataInput(boolean readVersion) {
        try {
            if (this.dis != null) {
                this.dis.close();
            }
            this.dis = new DataInputStream(new BufferedInputStream(new GZIPInputStream(this.replay != null ? (InputStream)MCTV.getInstance().getStorage().getReplay(this.replay, new Consumer<Double>(){

                @Override
                public void accept(Double aDouble) {
                    if (ReplayFileReader.this.context.getFileDLProgress() != null) {
                        ReplayFileReader.this.context.getFileDLProgress().accept(aDouble);
                    }
                }
            }).get() : new FileInputStream(this.replayFile))));
            if (readVersion) {
                this.context.setVersion(DataStreamer.readVarInt(this.dis));
            } else {
                this.context.setVersion(0);
            }
            MCTV.getInstance().getLogger().info("Replay version: " + this.context.getVersion());
            this.state = ReadState.STARTED;
        }
        catch (Exception e) {
            this.state = ReadState.ERROR;
            e.printStackTrace();
        }
    }

    public int skipBackwards(int skipFrames, int startFrame) {
        List<Frame> skippedFrames = this.buffer.pushBack(skipFrames);
        for (Frame skippedFrame : skippedFrames) {
            skippedFrame.rollback(true);
        }
        return startFrame;
    }

    public int skipForwards(int playedFrames, int toSkip) {
        int skippedFrames = 0;
        while (skippedFrames < toSkip && !this.isEOF()) {
            Frame frame = this.getNextFrame();
            if (frame == null) continue;
            frame.execute(true);
            ++skippedFrames;
        }
        return playedFrames + skippedFrames;
    }

    public boolean isEOF() {
        return this.state == ReadState.EOF && this.buffer.isEmpty();
    }

    public ReadState getState() {
        return this.state;
    }

    public Context getContext() {
        return this.context;
    }

    public ListenableFuture<InputStream> getDownload() {
        return this.download;
    }

    public static enum ReadState {
        ERROR,
        NOT_STARTED,
        STARTED,
        META_READ,
        MAP_META_READ,
        FRAME_AVAILABLE,
        EOF;

    }
}

