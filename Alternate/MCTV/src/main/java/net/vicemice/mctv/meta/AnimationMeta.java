package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerAnimation;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="AnimationMeta", opcode=3, adapter= AnimationMeta.AnimationMetaAdapter.class)
public class AnimationMeta
implements Meta {
    private UUID uuid;
    private int animationId;
    private String world;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeUUID(this.uuid, out);
        DataStreamer.writeVarInt(this.animationId, out);
        DataStreamer.writeString(this.world, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.uuid = DataStreamer.readUUID(in);
        this.animationId = DataStreamer.readVarInt(in);
        if (version >= 4) {
            this.world = DataStreamer.readString(in);
        }
    }

    @Override
    public void execute(Context context) {
        World world = context.getMasterPlayer().getWorld();
        if (this.world != null) {
            world = Bukkit.getWorld((String)this.world);
        }
        if (world == null) {
            context.warn("Replay wanted to get entity in world '" + this.world + "' which is not loaded. Defaulting back to replay master world");
            world = context.getMasterPlayer().getWorld();
        }
        WrapperPlayServerAnimation packet = new WrapperPlayServerAnimation();
        packet.setEntityID(context.getPlayerManager().getEntityIdByUUID(this.uuid, world));
        packet.setAnimation(this.animationId);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!player.getWorld().equals((Object)world)) continue;
            packet.sendPacket(player);
        }
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return true;
    }

    public AnimationMeta() {
    }

    @ConstructorProperties(value={"uuid", "animationId", "world"})
    public AnimationMeta(UUID uuid, int animationId, String world) {
        this.uuid = uuid;
        this.animationId = animationId;
        this.world = world;
    }

    public String toString() {
        return "AnimationMeta(uuid=" + this.uuid + ", animationId=" + this.animationId + ", world=" + this.world + ")";
    }

    public static class AnimationMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;

        public AnimationMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.ANIMATION});
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }

        public void onPacketSending(PacketEvent event) {
            UUID uuid;
            WrapperPlayServerAnimation packet = new WrapperPlayServerAnimation(event.getPacket());
            UUID uUID = uuid = packet.getEntity(event) instanceof Player ? packet.getEntity(event).getUniqueId() : null;
            if (uuid != null) {
                int animationId = packet.getAnimation();
                AnimationMeta meta = new AnimationMeta(uuid, animationId, packet.getEntity(event).getWorld().getName());
                this.replayFileWriter.addMeta(meta);
            }
        }

        public void onPacketReceiving(PacketEvent event) {
        }
    }
}

