package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.plugin.Plugin;

@MetaInfo(opcode=36, name="BucketFillMeta", adapter= BucketFillMeta.BucketFillMetaAdapter.class)
public class BucketFillMeta
implements Meta {
    private BlockFace blockFace;
    private Location location;
    private Material bucket;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeString(this.blockFace.toString(), out);
        out.writeDouble(this.location.getX());
        out.writeDouble(this.location.getY());
        out.writeDouble(this.location.getZ());
        DataStreamer.writeString(this.bucket.toString(), out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.blockFace = BlockFace.valueOf((String)DataStreamer.readString(in));
        this.location = new Location(null, in.readDouble(), in.readDouble(), in.readDouble());
        this.bucket = Material.valueOf((String)DataStreamer.readString(in));
    }

    @Override
    public void execute(Context context) {
        this.location.setWorld(MCTV.getInstance().getApi().getContext().getMasterPlayer().getWorld());
        Block block = this.location.getBlock();
        if (block.getType().equals((Object)Material.LAVA) || block.getType().equals((Object)Material.STATIONARY_LAVA) || block.getType().equals((Object)Material.WATER) || block.getType().equals((Object)Material.STATIONARY_WATER)) {
            block.setType(Material.AIR);
        }
    }

    @Override
    public void rollback(Context context) {
        if (!this.bucket.equals((Object)Material.LAVA_BUCKET) && !this.bucket.equals((Object)Material.WATER_BUCKET)) {
            return;
        }
        this.location.setWorld(MCTV.getInstance().getApi().getContext().getMasterPlayer().getWorld());
        this.location.getBlock().setType(this.bucket.equals((Object)Material.LAVA_BUCKET) ? Material.LAVA : Material.WATER);
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    @ConstructorProperties(value={"blockFace", "location", "bucket"})
    public BucketFillMeta(BlockFace blockFace, Location location, Material bucket) {
        this.blockFace = blockFace;
        this.location = location;
        this.bucket = bucket;
    }

    public BucketFillMeta() {
    }

    public BlockFace getBlockFace() {
        return this.blockFace;
    }

    public Location getLocation() {
        return this.location;
    }

    public Material getBucket() {
        return this.bucket;
    }

    public void setBlockFace(BlockFace blockFace) {
        this.blockFace = blockFace;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setBucket(Material bucket) {
        this.bucket = bucket;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof BucketFillMeta)) {
            return false;
        }
        BucketFillMeta other = (BucketFillMeta)o;
        if (!other.canEqual(this)) {
            return false;
        }
        BlockFace this$blockFace = this.getBlockFace();
        BlockFace other$blockFace = other.getBlockFace();
        if (this$blockFace == null ? other$blockFace != null : !this$blockFace.equals((Object)other$blockFace)) {
            return false;
        }
        Location this$location = this.getLocation();
        Location other$location = other.getLocation();
        if (this$location == null ? other$location != null : !this$location.equals((Object)other$location)) {
            return false;
        }
        Material this$bucket = this.getBucket();
        Material other$bucket = other.getBucket();
        return !(this$bucket == null ? other$bucket != null : !this$bucket.equals((Object)other$bucket));
    }

    protected boolean canEqual(Object other) {
        return other instanceof BucketFillMeta;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        BlockFace $blockFace = this.getBlockFace();
        result = result * 59 + ($blockFace == null ? 43 : $blockFace.hashCode());
        Location $location = this.getLocation();
        result = result * 59 + ($location == null ? 43 : $location.hashCode());
        Material $bucket = this.getBucket();
        result = result * 59 + ($bucket == null ? 43 : $bucket.hashCode());
        return result;
    }

    public static class BucketFillMetaAdapter
    implements MetaAdapter,
    Listener {
        private ReplayFileWriter replayFileWriter;

        public BucketFillMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            Bukkit.getPluginManager().registerEvents((Listener)this, (Plugin)MCTV.getInstance());
        }

        @Override
        public void stop() {
            HandlerList.unregisterAll((Listener)this);
        }

        @EventHandler(ignoreCancelled=true, priority=EventPriority.MONITOR)
        public void onBukkitFill(PlayerBucketFillEvent e) {
            BucketFillMeta meta = new BucketFillMeta(e.getBlockFace(), e.getBlockClicked().getLocation(), e.getBucket());
            this.replayFileWriter.addMeta(meta);
        }
    }
}

