package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

@MetaInfo(name="SpawnItemMeta", opcode=15, adapter= SpawnItemMeta.SpawnItemMetaAdapter.class)
public class SpawnItemMeta implements Meta {
    private int entityId;
    private ItemStack item;
    private Vector vector;
    private Vector velocity;
    private String world;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeVarInt(this.entityId, out);
        DataStreamer.writeItemStack(this.item, out);
        out.writeDouble(this.vector.getX());
        out.writeDouble(this.vector.getY());
        out.writeDouble(this.vector.getZ());
        out.writeDouble(this.velocity.getX());
        out.writeDouble(this.velocity.getY());
        out.writeDouble(this.velocity.getZ());
        DataStreamer.writeString(this.world, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.entityId = DataStreamer.readVarInt(in);
        this.item = DataStreamer.readItemStack(in);
        this.vector = new Vector(in.readDouble(), in.readDouble(), in.readDouble());
        this.velocity = new Vector(in.readDouble(), in.readDouble(), in.readDouble());
        if (version >= 3) {
            this.world = DataStreamer.readString(in);
        }
    }

    @Override
    public void execute(Context context) {
        World world = context.getMasterPlayer().getWorld();
        if (this.world != null) {
            world = Bukkit.getWorld((String)this.world);
        }
        context.getEntityManager().spawnItem(this.entityId, world, this.item, this.vector, this.velocity);
    }

    @Override
    public void rollback(Context context) {
        context.getEntityManager().despawn(this.entityId);
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public SpawnItemMeta() {
    }

    @ConstructorProperties(value={"entityId", "item", "vector", "velocity", "world"})
    public SpawnItemMeta(int entityId, ItemStack item, Vector vector, Vector velocity, String world) {
        this.entityId = entityId;
        this.item = item;
        this.vector = vector;
        this.velocity = velocity;
        this.world = world;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof SpawnItemMeta)) {
            return false;
        }
        SpawnItemMeta other = (SpawnItemMeta)o;
        if (!other.canEqual(this)) {
            return false;
        }
        if (this.entityId != other.entityId) {
            return false;
        }
        ItemStack this$item = this.item;
        ItemStack other$item = other.item;
        if (this$item == null ? other$item != null : !this$item.equals((Object)other$item)) {
            return false;
        }
        Vector this$vector = this.vector;
        Vector other$vector = other.vector;
        if (this$vector == null ? other$vector != null : !this$vector.equals((Object)other$vector)) {
            return false;
        }
        Vector this$velocity = this.velocity;
        Vector other$velocity = other.velocity;
        if (this$velocity == null ? other$velocity != null : !this$velocity.equals((Object)other$velocity)) {
            return false;
        }
        String this$world = this.world;
        String other$world = other.world;
        return !(this$world == null ? other$world != null : !this$world.equals(other$world));
    }

    protected boolean canEqual(Object other) {
        return other instanceof SpawnItemMeta;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        result = result * 59 + this.entityId;
        ItemStack $item = this.item;
        result = result * 59 + ($item == null ? 43 : $item.hashCode());
        Vector $vector = this.vector;
        result = result * 59 + ($vector == null ? 43 : $vector.hashCode());
        Vector $velocity = this.velocity;
        result = result * 59 + ($velocity == null ? 43 : $velocity.hashCode());
        String $world = this.world;
        result = result * 59 + ($world == null ? 43 : $world.hashCode());
        return result;
    }

    public static class SpawnItemMetaAdapter
    implements MetaAdapter,
    Listener {
        private ReplayFileWriter replayFileWriter;

        public SpawnItemMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            Bukkit.getServer().getPluginManager().registerEvents((Listener)this, (Plugin)MCTV.getInstance());
        }

        @Override
        public void stop() {
        }

        @EventHandler(priority=EventPriority.MONITOR, ignoreCancelled=true)
        public void onSpawnItem(ItemSpawnEvent event) {
            SpawnItemMeta spawnItemMeta = new SpawnItemMeta(event.getEntity().getEntityId(), event.getEntity().getItemStack(), event.getLocation().toVector(), event.getEntity().getVelocity(), event.getLocation().getWorld().getName());
            this.replayFileWriter.addMeta(spawnItemMeta);
        }
    }
}

