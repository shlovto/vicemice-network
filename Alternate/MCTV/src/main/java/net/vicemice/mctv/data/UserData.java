package net.vicemice.mctv.data;

import java.beans.ConstructorProperties;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import net.vicemice.mctv.replay.Context;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class UserData {
    private static Map<UUID, UserData> userDatas = new HashMap<UUID, UserData>();
    private final UUID uuid;
    private final Context context;
    private Set<UUID> shownPlayers = new HashSet<UUID>();
    private int lastSlotUpdate = 0;

    public static UserData getUserData(Player player, Context context) {
        return userDatas.computeIfAbsent(player.getUniqueId(), k -> new UserData(player.getUniqueId(), context));
    }

    public static void removeUserData(Player player) {
        userDatas.remove(player.getUniqueId());
    }

    public static void tick(Context context) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            UserData userData = UserData.getUserData(player, context);
            userData.tickPlayers(player.getLocation());
        }
    }

    public void tickPlayers(Location location) {
        for (EntityPlayer entityPlayer : this.context.getPlayerManager().getPlayers()) {
            if (!entityPlayer.getWorld().getWorld().equals((Object)location.getWorld())) continue;
            Location location1 = new Location((World)entityPlayer.getWorld().getWorld(), entityPlayer.locX, entityPlayer.locY, entityPlayer.locZ);
            if (location1.distanceSquared(location) <= (double)(entityPlayer.getWorld().spigotConfig.playerTrackingRange * entityPlayer.getWorld().spigotConfig.playerTrackingRange)) {
                if (!this.shownPlayers.contains(entityPlayer.getUniqueID())) {
                    this.context.getPlayerManager().spawnPlayerFor(entityPlayer.getUniqueID(), Bukkit.getPlayer((UUID)this.uuid));
                    this.shownPlayers.add(entityPlayer.getUniqueID());
                    continue;
                }
                if (this.lastSlotUpdate++ != 15) continue;
                this.context.getPlayerManager().sendSlots(entityPlayer.getId(), Bukkit.getPlayer((UUID)this.uuid));
                this.lastSlotUpdate = 0;
                continue;
            }
            if (!this.shownPlayers.contains(entityPlayer.getUniqueID())) continue;
            this.context.getPlayerManager().despawnPlayerFor(entityPlayer.getUniqueID(), Bukkit.getPlayer((UUID)this.uuid));
            this.shownPlayers.remove(entityPlayer.getUniqueID());
        }
    }

    public void clearPlayers() {
        this.shownPlayers.clear();
    }

    @ConstructorProperties(value={"uuid", "context"})
    public UserData(UUID uuid, Context context) {
        this.uuid = uuid;
        this.context = context;
    }
}

