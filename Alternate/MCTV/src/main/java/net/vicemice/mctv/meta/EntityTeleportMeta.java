package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerEntityTeleport;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Location;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="EntityTeleport", opcode=24, adapter= EntityTeleportMeta.EntityTeleportMetaAdapter.class)
public class EntityTeleportMeta
implements Meta {
    private Location oldLocation;
    private int entityId;
    private int x;
    private int y;
    private int z;
    private float yaw;
    private float pitch;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        out.writeInt(this.entityId);
        out.writeInt(this.x);
        out.writeInt(this.y);
        out.writeInt(this.z);
        out.writeFloat(this.yaw);
        out.writeFloat(this.pitch);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.entityId = in.readInt();
        this.x = in.readInt();
        this.y = in.readInt();
        this.z = in.readInt();
        this.yaw = in.readFloat();
        this.pitch = in.readFloat();
    }

    @Override
    public void execute(Context context) {
        Location entityLocation = context.getEntityManager().getLocation(this.entityId);
        if (entityLocation == null) {
            return;
        }
        this.oldLocation = entityLocation.clone();
        context.getEntityManager().teleport(this.entityId, new Location(this.oldLocation.getWorld(), (double)this.x / 32.0, (double)this.y / 32.0, (double)this.z / 32.0, this.yaw, this.pitch));
    }

    @Override
    public void rollback(Context context) {
        if (this.oldLocation == null) {
            return;
        }
        context.getEntityManager().teleport(this.entityId, this.oldLocation);
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public EntityTeleportMeta() {
    }

    @ConstructorProperties(value={"oldLocation", "entityId", "x", "y", "z", "yaw", "pitch"})
    public EntityTeleportMeta(Location oldLocation, int entityId, int x, int y, int z, float yaw, float pitch) {
        this.oldLocation = oldLocation;
        this.entityId = entityId;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    public static class EntityTeleportMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;

        public EntityTeleportMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.ENTITY_TELEPORT});
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }

        public void onPacketSending(PacketEvent event) {
            WrapperPlayServerEntityTeleport entityTeleport = new WrapperPlayServerEntityTeleport(event.getPacket());
            EntityTeleportMeta entityTeleportMeta = new EntityTeleportMeta(null, entityTeleport.getEntityID(), (int)(entityTeleport.getX() * 32.0), (int)(entityTeleport.getY() * 32.0), (int)(entityTeleport.getZ() * 32.0), entityTeleport.getYaw(), entityTeleport.getPitch());
            this.replayFileWriter.addMeta(entityTeleportMeta);
        }
    }
}

