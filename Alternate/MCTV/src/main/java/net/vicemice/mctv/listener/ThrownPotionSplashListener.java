package net.vicemice.mctv.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PotionSplashEvent;

public class ThrownPotionSplashListener implements Listener {

    @EventHandler(priority=EventPriority.HIGHEST)
    public void onPotionSplash(PotionSplashEvent e) {
        e.setCancelled(true);
    }
}

