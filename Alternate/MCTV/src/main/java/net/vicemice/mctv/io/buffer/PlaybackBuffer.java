package net.vicemice.mctv.io.buffer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import net.vicemice.mctv.replay.Frame;

public class PlaybackBuffer {
    private final Frame[] buffer;
    protected final Object monitor = new Object();
    private int readerIndex;
    private int writerIndex;

    public PlaybackBuffer(int duration, TimeUnit unit) {
        int frames = (int)unit.toSeconds(duration) * 20;
        this.buffer = new Frame[frames];
        this.readerIndex = 0;
        this.writerIndex = 0;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void putBack(Frame frame) {
        Object object = this.monitor;
        synchronized (object) {
            if ((this.writerIndex + 1) % this.buffer.length == this.readerIndex) {
                throw new IllegalStateException("Cannot putBack into full playback buffer!");
            }
            this.buffer[this.writerIndex] = frame;
            this.writerIndex = (this.writerIndex + 1) % this.buffer.length;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isFull() {
        Object object = this.monitor;
        synchronized (object) {
            return (this.writerIndex + 1) % this.buffer.length == this.readerIndex;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Frame popFront() {
        Object object = this.monitor;
        synchronized (object) {
            if (this.readerIndex == this.writerIndex) {
                throw new IllegalStateException("Cannot popFront from empty playback buffer!");
            }
            Frame frame = this.buffer[this.readerIndex];
            this.readerIndex = (this.readerIndex + 1) % this.buffer.length;
            return frame;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Frame popBack() {
        Object object = this.monitor;
        synchronized (object) {
            if (this.readerIndex == this.writerIndex) {
                throw new IllegalStateException("Cannot popBack from empty playback buffer!");
            }
            if ((this.writerIndex + 1) % this.buffer.length == this.readerIndex) {
                throw new IllegalStateException("Cannot popBack from filled playback buffer!");
            }
            this.readerIndex = (this.readerIndex == 0 ? this.buffer.length : this.readerIndex) - 1;
            return this.buffer[this.readerIndex];
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isEmpty() {
        Object object = this.monitor;
        synchronized (object) {
            return this.readerIndex == this.writerIndex;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int size() {
        Object object = this.monitor;
        synchronized (object) {
            if (this.readerIndex == this.writerIndex) {
                return 0;
            }
            if (this.readerIndex < this.writerIndex) {
                return this.writerIndex - this.readerIndex;
            }
            return this.buffer.length - (this.readerIndex - this.writerIndex);
        }
    }

    public int capacity() {
        return this.buffer.length;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public int skip(int frames) {
        int size = this.size();
        Object object = this.monitor;
        synchronized (object) {
            if (frames <= size) {
                this.readerIndex = (this.readerIndex + frames) % this.buffer.length;
                return frames;
            }
            this.readerIndex = 0;
            this.writerIndex = 0;
            return size;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public List<Frame> pushBack(int frames) {
        int available = this.buffer.length - this.size();
        int pushedBack = frames;
        ArrayList<Frame> framesSkipped = new ArrayList<Frame>();
        Object object = this.monitor;
        synchronized (object) {
            if (frames > available) {
                pushedBack = available;
            } else {
                available = frames;
            }
            this.readerIndex = this.readerIndex - available < 0 ? this.buffer.length - (this.readerIndex - available) : (this.readerIndex -= available);
            for (int i = this.readerIndex; i < this.readerIndex + pushedBack; ++i) {
                framesSkipped.add(this.buffer[i]);
            }
            Collections.reverse(framesSkipped);
        }
        return framesSkipped;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void clear() {
        Object object = this.monitor;
        synchronized (object) {
            this.readerIndex = 0;
            this.writerIndex = 0;
        }
    }
}

