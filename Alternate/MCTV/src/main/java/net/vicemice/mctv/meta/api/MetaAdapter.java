package net.vicemice.mctv.meta.api;

public interface MetaAdapter {
    public void start();

    public void stop();
}

