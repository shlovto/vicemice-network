package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketContainer;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import net.vicemice.hector.api.protocol.wrappers.EnumWrappers;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="SoundMeta", opcode=6, adapter= SoundMeta.SoundMetaAdapter.class)
public class SoundMeta implements Meta {
    private static final Set<String> VALID_SOUND_CATEGORY = new HashSet<String>(){
        {
            this.add("music");
            this.add("master");
            this.add("record");
            this.add("weather");
            this.add("block");
            this.add("hostile");
            this.add("neutral");
            this.add("player");
            this.add("ambient");
            this.add("voice");
        }
    };
    private byte version;
    private String soundCategory;
    private String soundName;
    private Location location;
    private float volume;
    private float pitch;
    private String world;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        out.writeByte(this.version);
        if (this.version == 1) {
            DataStreamer.writeString(this.soundCategory, out);
        }
        DataStreamer.writeString(this.soundName, out);
        out.writeDouble(this.location.getX());
        out.writeDouble(this.location.getY());
        out.writeDouble(this.location.getZ());
        out.writeFloat(this.volume);
        out.writeFloat(this.pitch);
        DataStreamer.writeString(this.world, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        if (version >= 12 || version == 11 || version == 8) {
            in.mark(20);
            this.version = in.readByte();
            if (this.version == 1) {
                this.soundCategory = DataStreamer.readString(in);
                if (this.soundCategory == null || !VALID_SOUND_CATEGORY.contains(this.soundCategory)) {
                    this.soundCategory = null;
                    this.version = 0;
                    in.reset();
                }
            } else if (this.version != 0) {
                this.version = 0;
                in.reset();
            }
        }
        this.soundName = DataStreamer.readString(in);
        this.location = new Location(null, in.readDouble(), in.readDouble(), in.readDouble());
        this.volume = in.readFloat();
        this.pitch = in.readFloat();
        if (version >= 4) {
            this.world = DataStreamer.readString(in);
        }
    }

    @Override
    public void execute(Context context) {
        PacketContainer soundPacket = new PacketContainer(PacketType.Play.Server.NAMED_SOUND_EFFECT);
        if (this.soundCategory != null) {
            soundPacket.getSoundCategories().write(0, EnumWrappers.SoundCategory.getByKey((String)this.soundCategory));
            soundPacket.getSoundEffects().write(0, Sound.valueOf((String)this.soundName));
            soundPacket.getFloat().write(1, Float.valueOf(this.pitch));
        } else {
            soundPacket.getStrings().write(0, this.soundName);
            soundPacket.getIntegers().write(3, ((int)this.pitch));
        }
        soundPacket.getFloat().write(0, Float.valueOf(this.volume));
        soundPacket.getIntegers().write(0, ((int)(this.location.getX() * 8.0)));
        soundPacket.getIntegers().write(1, ((int)(this.location.getY() * 8.0)));
        soundPacket.getIntegers().write(2, ((int)(this.location.getZ() * 8.0)));
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (this.world != null && !this.world.equals(player.getWorld().getName())) continue;
            try {
                ProtocolLibrary.getProtocolManager().sendServerPacket(player, soundPacket);
            }
            catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return true;
    }

    public SoundMeta() {
    }

    @ConstructorProperties(value={"version", "soundCategory", "soundName", "location", "volume", "pitch", "world"})
    public SoundMeta(byte version, String soundCategory, String soundName, Location location, float volume, float pitch, String world) {
        this.version = version;
        this.soundCategory = soundCategory;
        this.soundName = soundName;
        this.location = location;
        this.volume = volume;
        this.pitch = pitch;
        this.world = world;
    }

    public static class SoundMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;

        public SoundMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.NAMED_SOUND_EFFECT});
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }

        public void onPacketSending(PacketEvent event) {
            byte version = 0;
            String soundName = (String)event.getPacket().getStrings().readSafely(0);
            String soundCategory = null;
            if (soundName == null) {
                version = 1;
                soundCategory = ((EnumWrappers.SoundCategory)event.getPacket().getSoundCategories().read(0)).getKey();
                soundName = ((Sound)event.getPacket().getSoundEffects().read(0)).name();
            }
            Location location = new Location(event.getPlayer().getWorld(), (double)((Integer)event.getPacket().getIntegers().read(0) / 8), (double)((Integer)event.getPacket().getIntegers().read(1) / 8), (double)((Integer)event.getPacket().getIntegers().read(2) / 8));
            float volume = ((Float)event.getPacket().getFloat().read(0)).floatValue();
            float pitch = event.getPacket().getFloat().readSafely(1) == null ? (float)((Integer)event.getPacket().getIntegers().read(3)).intValue() : ((Float)event.getPacket().getFloat().read(1)).floatValue();
            SoundMeta meta = new SoundMeta(version, soundCategory, soundName, location, volume, pitch, location.getWorld().getName());
            this.replayFileWriter.addMeta(meta);
        }

        public void onPacketReceiving(PacketEvent event) {
        }
    }
}

