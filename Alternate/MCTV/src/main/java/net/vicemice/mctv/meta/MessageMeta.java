package net.vicemice.mctv.meta;

import com.google.common.base.Charsets;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.meta.api.VoidMetaAdapter;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;

/*
 * Class created at 22:17 - 19.04.2020
 * Copyright (C) elrobtossohn
 */
@MetaInfo(name="MessageMeta", opcode=42, adapter= VoidMetaAdapter.class)
public class MessageMeta implements Meta {
    private String message;
    private UUID player;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeString(this.message, Charsets.UTF_8, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.message = DataStreamer.readString(Charsets.UTF_8, in);
    }

    @Override
    public void execute(final Context context) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.sendMessage(this.message);
        }
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    @ConstructorProperties(value={"message"})
    public MessageMeta(String message) {
        this.message = message;
    }

    public MessageMeta() {
    }

    public String getMessage() {
        return this.message;
    }

    public UUID getPlayer() {
        return this.player;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setPlayer(UUID player) {
        this.player = player;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof MessageMeta)) {
            return false;
        }
        MessageMeta other = (MessageMeta)o;
        if (!other.canEqual(this)) {
            return false;
        }
        String this$message = this.getMessage();
        String other$message = other.getMessage();
        if (this$message == null ? other$message != null : !this$message.equals(other$message)) {
            return false;
        }
        return true;
    }

    protected boolean canEqual(Object other) {
        return other instanceof MessageMeta;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        String $message = this.getMessage();
        result = result * 59 + ($message == null ? 43 : $message.hashCode());
        return result;
    }
}