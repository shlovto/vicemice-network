package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.entity.Vehicle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.vehicle.VehicleCreateEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

@MetaInfo(name="CreatureSpawn", opcode=22, adapter= CreatureSpawnMeta.CreatureSpawnMetaAdapter.class)
public class CreatureSpawnMeta
implements Meta {
    private int entityId;
    private EntityType entityType;
    private Location location;
    private String worldName;
    private static final Set<Entity> spawnedEntities = ConcurrentHashMap.newKeySet();
    private int color;
    private boolean tnt;
    private boolean isBaby;
    private int style;
    private int variant;
    private int domestication;
    private int maxDomestication;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeString(this.location.getWorld().getName(), out);
        out.writeInt(this.entityId);
        out.writeDouble(this.location.getX());
        out.writeDouble(this.location.getY());
        out.writeDouble(this.location.getZ());
        out.writeFloat(this.location.getPitch());
        out.writeFloat(this.location.getYaw());
        DataStreamer.writeString(this.entityType.name(), out);
        if (this.entityType == EntityType.SHEEP) {
            out.writeInt(this.color);
            out.writeBoolean(this.tnt);
        }
        out.writeBoolean(this.isBaby);
        if (this.entityType == EntityType.HORSE) {
            out.writeInt(this.style);
            out.writeInt(this.variant);
            out.writeInt(this.domestication);
            out.writeInt(this.maxDomestication);
            out.writeInt(this.color);
        }
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.worldName = DataStreamer.readString(in);
        this.entityId = in.readInt();
        this.location = new Location(null, in.readDouble(), in.readDouble(), in.readDouble(), in.readFloat(), in.readFloat());
        this.entityType = EntityType.valueOf((String)DataStreamer.readString(in));
        if (this.entityType == EntityType.SHEEP) {
            this.color = in.readInt();
            this.tnt = in.readBoolean();
        }
        if (version >= 15) {
            this.isBaby = in.readBoolean();
            if (this.entityType == EntityType.HORSE) {
                this.style = in.readInt();
                this.variant = in.readInt();
                this.domestication = in.readInt();
                this.maxDomestication = in.readInt();
                this.color = in.readInt();
            }
        }
    }

    @Override
    public void execute(Context context) {
        Entity entity;
        if (Bukkit.getServer() != null) {
            this.location.setWorld(Bukkit.getWorld((String)this.worldName));
        }
        if ((entity = context.getEntityManager().spawnLivingEntity(this.entityId, this.entityType, this.location, this.color, this.tnt)) instanceof Horse) {
            Horse horse = (Horse)entity;
            horse.setStyle(Horse.Style.values()[this.style]);
            horse.setVariant(Horse.Variant.values()[this.variant]);
            horse.setMaxDomestication(this.maxDomestication);
            horse.setDomestication(this.domestication);
            horse.setColor(Horse.Color.values()[this.color]);
        }
        if (entity instanceof Ageable && this.isBaby) {
            ((Ageable)entity).setBaby();
        }
    }

    @Override
    public void rollback(Context context) {
        context.getEntityManager().despawn(this.entityId);
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public CreatureSpawnMeta() {
    }

    @ConstructorProperties(value={"entityId", "entityType", "location", "worldName", "color", "tnt", "isBaby", "style", "variant", "domestication", "maxDomestication"})
    public CreatureSpawnMeta(int entityId, EntityType entityType, Location location, String worldName, int color, boolean tnt, boolean isBaby, int style, int variant, int domestication, int maxDomestication) {
        this.entityId = entityId;
        this.entityType = entityType;
        this.location = location;
        this.worldName = worldName;
        this.color = color;
        this.tnt = tnt;
        this.isBaby = isBaby;
        this.style = style;
        this.variant = variant;
        this.domestication = domestication;
        this.maxDomestication = maxDomestication;
    }

    public static Set<Entity> getSpawnedEntities() {
        return spawnedEntities;
    }

    public static class CreatureSpawnMetaAdapter
    implements MetaAdapter,
    Listener {
        private ReplayFileWriter replayFileWriter;

        public CreatureSpawnMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            Bukkit.getPluginManager().registerEvents((Listener)this, (Plugin)MCTV.getInstance());
        }

        @Override
        public void stop() {
            HandlerList.unregisterAll((Listener)this);
        }

        @EventHandler(priority=EventPriority.MONITOR)
        public void onVehicleCreateEvent(VehicleCreateEvent e) {
            Vehicle vehicle = e.getVehicle();
            CreatureSpawnMeta meta = new CreatureSpawnMeta(vehicle.getEntityId(), vehicle.getType(), vehicle.getLocation(), vehicle.getLocation().getWorld().getName(), 0, false, false, 0, 0, 0, 0);
            this.replayFileWriter.addMeta(meta);
        }

        @EventHandler(ignoreCancelled=true, priority=EventPriority.MONITOR)
        public void onCreatureSpawn(final EntitySpawnEvent event) {
            if (!(event.getEntity() instanceof LivingEntity) || event.getEntity() instanceof Player) {
                return;
            }
            final Entity entity = event.getEntity();
            new BukkitRunnable(){

                public void run() {
                    int color = 0;
                    boolean tnt = false;
                    boolean isBaby = false;
                    int variant = 0;
                    int style = 0;
                    int domestication = 0;
                    int maxDomestication = 0;
                    if (entity instanceof Sheep) {
                        Sheep sheep = (Sheep)entity;
                        color = sheep.getColor().ordinal();
                        boolean bl = tnt = entity.getPassenger() != null && entity.getPassenger() instanceof TNTPrimed;
                    }
                    if (entity instanceof Ageable) {
                        boolean bl = isBaby = !((Ageable)entity).isAdult();
                    }
                    if (entity instanceof Horse) {
                        Horse horse = (Horse)entity;
                        color = horse.getColor().ordinal();
                        style = horse.getStyle().ordinal();
                        variant = horse.getVariant().ordinal();
                        domestication = horse.getDomestication();
                        maxDomestication = horse.getMaxDomestication();
                    }
                    spawnedEntities.add(entity);
                    CreatureSpawnMeta creatureSpawnMeta = new CreatureSpawnMeta(entity.getEntityId(), event.getEntityType(), event.getLocation(), event.getLocation().getWorld().getName(), color, tnt, isBaby, style, variant, domestication, maxDomestication);
                    replayFileWriter.addMeta(creatureSpawnMeta);
                }
            }.runTaskLater((Plugin)MCTV.getInstance(), 10L);
        }
    }
}

