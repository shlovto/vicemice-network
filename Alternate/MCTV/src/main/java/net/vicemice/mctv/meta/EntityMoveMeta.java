package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.CreatureSpawnMeta;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

@MetaInfo(name="EntityMoveMeta", opcode=26, adapter= EntityMoveMeta.EntityMoveMetaAdapter.class)
public class EntityMoveMeta
implements Meta {
    private Location oldLocation;
    private int entityId;
    private Location target;
    private String world;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        out.writeInt(this.entityId);
        out.writeDouble(this.target.getX());
        out.writeDouble(this.target.getY());
        out.writeDouble(this.target.getZ());
        out.writeFloat(this.target.getYaw());
        out.writeFloat(this.target.getPitch());
        DataStreamer.writeString(this.target.getWorld().getName(), out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.entityId = in.readInt();
        double x = in.readDouble();
        double y = in.readDouble();
        double z = in.readDouble();
        float yaw = in.readFloat();
        float pitch = in.readFloat();
        this.target = new Location(null, x, y, z, yaw, pitch);
        if (version >= 3) {
            this.world = DataStreamer.readString(in);
        }
    }

    @Override
    public void execute(Context context) {
        World world = context.getMasterPlayer().getWorld();
        if (this.world != null) {
            world = Bukkit.getWorld((String)this.world);
        }
        this.oldLocation = context.getEntityManager().getLocation(this.entityId);
        context.getEntityManager().teleport(this.entityId, new Location(world, this.target.getX(), this.target.getY(), this.target.getZ(), this.target.getYaw(), this.target.getPitch()));
    }

    @Override
    public void rollback(Context context) {
        if (this.oldLocation == null) {
            return;
        }
        context.getEntityManager().teleport(this.entityId, this.oldLocation);
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public EntityMoveMeta() {
    }

    @ConstructorProperties(value={"oldLocation", "entityId", "target", "world"})
    public EntityMoveMeta(Location oldLocation, int entityId, Location target, String world) {
        this.oldLocation = oldLocation;
        this.entityId = entityId;
        this.target = target;
        this.world = world;
    }

    public static class EntityMoveMetaAdapter
    implements MetaAdapter,
    Runnable {
        private ReplayFileWriter replayFileWriter;
        private BukkitTask task;

        public EntityMoveMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            this.task = Bukkit.getScheduler().runTaskTimer((Plugin)MCTV.getInstance(), (Runnable)this, 0L, 1L);
        }

        @Override
        public void stop() {
            this.task.cancel();
        }

        @Override
        public void run() {
            Iterator<Entity> entityIterator = CreatureSpawnMeta.getSpawnedEntities().iterator();
            while (entityIterator.hasNext()) {
                Entity entity = entityIterator.next();
                if (entity.isDead()) {
                    entityIterator.remove();
                    continue;
                }
                EntityMoveMeta meta = new EntityMoveMeta(null, entity.getEntityId(), entity.getLocation(), null);
                this.replayFileWriter.addMeta(meta);
            }
        }
    }
}

