package net.vicemice.mctv.meta.api;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.vicemice.mctv.replay.Context;

public interface Meta {
    public void writeMeta(DataOutputStream var1) throws IOException;

    public void readMeta(DataInputStream var1, int var2) throws IOException;

    public void execute(Context var1);

    public void rollback(Context var1);

    public boolean canBeSkipped();
}

