package net.vicemice.mctv.session;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import java.beans.ConstructorProperties;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.logging.Level;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.replay.RequireReplayGameIDPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.players.api.title.Title;
import net.vicemice.hector.types.ServerState;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.data.UserData;
import net.vicemice.mctv.inventory.Hotbar;
import net.vicemice.mctv.io.reader.ReplayFileReader;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.replay.Frame;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

public class ReplaySession {
    private final Context context;
    private String gameId;
    private BukkitTask playbackTask;
    private ReplayFileReader replay;

    public void addReplayUser(final Player player) {
        player.setGameMode(GameMode.ADVENTURE);
        player.setAllowFlight(true);
        player.setFlying(true);
        player.getInventory().clear();
        if (player.isOnline()) {
            player.getInventory().setItem(0, ItemBuilder.create(Material.COMPASS).name(GoAPI.getUserAPI().translate( player.getUniqueId(), "item-teleporter")).build());
        }

        if (this.context.getMaster() == null) {
            this.context.setMaster(player.getUniqueId());
            GoAPI.getServerAPI().changeServer(ServerState.INGAME, this.context.getMasterPlayer().getName());
            //ServerHelper.updateStatus();
            if (!player.isOnline()) {
                return;
            }
            player.getInventory().setItem(3, ItemBuilder.create(Hotbar.getBackwards()).name(GoAPI.getUserAPI().translate( player.getUniqueId(), "item-rewind")).build());
            player.getInventory().setItem(4, ItemBuilder.create(Hotbar.getPause()).name(GoAPI.getUserAPI().translate( player.getUniqueId(), "item-pause")).build());
            player.getInventory().setItem(5, ItemBuilder.create(Hotbar.getForwards()).name(GoAPI.getUserAPI().translate( player.getUniqueId(), "item-forward")).build());
            player.getInventory().setItem(7, ItemBuilder.create(Hotbar.getSlowmotion()).name(GoAPI.getUserAPI().translate( player.getUniqueId(), "item-slow-motion")).build());
            player.getInventory().setItem(8, ItemBuilder.create(Hotbar.getStop()).name(GoAPI.getUserAPI().translate( player.getUniqueId(), "item-stop")).build());
            player.getInventory().setHeldItemSlot(4);

            GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.REQUIRE_REPLAY_GAMEID, new RequireReplayGameIDPacket(player.getUniqueId())), API.PacketReceiver.SLAVE);
        } else {
            Player masterPlayer = Bukkit.getPlayer(this.context.getMaster());
            if (masterPlayer != null) {
                player.teleport(masterPlayer);
                this.context.getPlayerManager().sendPlayerList(player, true);
            }
            Bukkit.getScheduler().runTask(MCTV.getInstance(), () -> {
                for (Player player1 : Bukkit.getOnlinePlayers()) {
                    player1.hidePlayer(player);
                }
                Bukkit.getScheduler().runTask(MCTV.getInstance(), () -> {
                    for (Player player1 : Bukkit.getOnlinePlayers()) {
                        player1.showPlayer(player);
                    }
                });
            });
        }
        player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 0));
    }

    public void updateInventory(final Player player) {
        player.getInventory().clear();
        player.getInventory().setItem(0, ItemBuilder.create(Material.COMPASS).name(GoAPI.getUserAPI().translate( player.getUniqueId(), "item-teleporter")).build());
        if (this.context.getMasterPlayer().equals(player)) {
            player.getInventory().setItem(3, ItemBuilder.create(Hotbar.getBackwards()).name(GoAPI.getUserAPI().translate( player.getUniqueId(), "item-rewind")).build());
            if (MCTV.getInstance().getReplaySession().isPaused()) {
                player.getInventory().setItem(4, ItemBuilder.create(Hotbar.getPlay()).name(GoAPI.getUserAPI().translate( player.getUniqueId(), "item-pause")).build());
            } else {
                player.getInventory().setItem(4, ItemBuilder.create(Hotbar.getPause()).name(GoAPI.getUserAPI().translate( player.getUniqueId(), "item-pause")).build());
            }
            player.getInventory().setItem(5, ItemBuilder.create(Hotbar.getForwards()).name(GoAPI.getUserAPI().translate( player.getUniqueId(), "item-forward")).build());
            player.getInventory().setItem(7, ItemBuilder.create(Hotbar.getSlowmotion()).name(GoAPI.getUserAPI().translate( player.getUniqueId(), "item-slow-motion")).build());
            player.getInventory().setItem(8, ItemBuilder.create(Hotbar.getStop()).name(GoAPI.getUserAPI().translate( player.getUniqueId(), "item-stop")).build());
            player.getInventory().setHeldItemSlot(4);
        }
    }

    public void loadReplay(UUID replayUUID) {
        this.replay = new ReplayFileReader(replayUUID, this.context);
        Bukkit.getScheduler().runTaskTimer(MCTV.getInstance(), this.context, 30L, 30L);
    }

    public boolean isMaster(Player player) {
        return player.getUniqueId().equals(this.context.getMaster());
    }

    public void pause() {
        this.context.getPaused().set(true);
    }

    public void speed(float speed) {
        this.context.setSpeed(speed);
        this.context.getSkipped().set(0);
    }

    public void skip(int amount) {
        if (this.context.getPaused().get()) {
            this.context.getPaused().set(false);
            if (this.context.getMasterPlayer() != null) {
                this.context.getMasterPlayer().getInventory().setItem(4, new ItemBuilder(Hotbar.getPause()).name("\u00a77Pause").build());
            }
        }
        if (amount < 0) {
            int skipFrames = amount * -1 * 20;
            int startFrame = this.context.getPlayedFrames() - skipFrames;
            if (startFrame < 1) {
                startFrame = 1;
                skipFrames = this.context.getPlayedFrames() - 1;
            }
            this.context.setPlayedFrames(this.replay.skipBackwards(skipFrames, startFrame));
        } else {
            this.context.setPlayedFrames(this.replay.skipForwards(this.context.getPlayedFrames(), amount * 20));
        }
    }

    public void play() {
        if (this.replay != null) {
            if (this.replay.getState() == ReplayFileReader.ReadState.NOT_STARTED) {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    Title title = new Title(GoAPI.getUserAPI().translate( player.getUniqueId(), "loading-title"), GoAPI.getUserAPI().translate( player.getUniqueId(), "loading-subtitle"), 0, 2400, 0);
                    title.send(player);
                    player.addPotionEffect(PotionEffectType.BLINDNESS.createEffect(Integer.MAX_VALUE, 1));
                }
            }
            this.context.setFileDLProgress(new Consumer<Double>(){

                @Override
                public void accept(Double aDouble) {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        Title title = new Title(GoAPI.getUserAPI().translate( player.getUniqueId(), "loading-title"), GoAPI.getUserAPI().translate( player.getUniqueId(), "loading-subtitle", Math.round(aDouble * 100.0)), 0, 2400, 0);
                        title.send(player);
                    }
                }
            });
            Futures.addCallback(this.replay.getDownload(), new FutureCallback<InputStream>(){

                @Override
                public void onSuccess(@Nullable InputStream inputStream) {
                    if (ReplaySession.this.playbackTask == null) {
                        if (ReplaySession.this.replay.getState() == ReplayFileReader.ReadState.ERROR) {
                            for (Player player : Bukkit.getOnlinePlayers()) {
                                ReplaySession.this.sendErrorMessage(player);
                            }
                            Bukkit.getScheduler().runTaskLater(MCTV.getInstance(), () -> {
                                for (Player player : Bukkit.getOnlinePlayers()) {
                                    player.kickPlayer("lobby");
                                }
                            }, 20L);
                            return;
                        }
                        ReplaySession.this.context.setActionBarTask(Bukkit.getScheduler().runTaskTimer(MCTV.getInstance(), ReplaySession.this.context, 0L, 35L));
                        ReplaySession.this.playbackTask = Bukkit.getServer().getScheduler().runTaskTimer(MCTV.getInstance(), () -> {
                            Frame frame;
                            UserData.tick(ReplaySession.this.context);
                            if (ReplaySession.this.context.getPaused().get()) {
                                return;
                            }
                            if (ReplaySession.this.context.getSpeed() < 1.0f) {
                                int needToSkip = Math.round((1.0f - ReplaySession.this.context.getSpeed()) * 10.0f);
                                if (ReplaySession.this.context.getSkipped().incrementAndGet() < needToSkip) {
                                    return;
                                }
                            }
                            if ((frame = ReplaySession.this.replay.getNextFrame()) != null) {
                                frame.execute(false);
                                ReplaySession.this.context.getSkipped().set(0);
                                ReplaySession.this.context.setPlayedFrames(ReplaySession.this.context.getPlayedFrames() + 1);
                            }
                            /*for (Player player : Bukkit.getOnlinePlayers()) {
                                player.setLevel(Math.round((float)ReplaySession.this.context.getPlayedFrames() / 20.0f));
                                player.setExp((float)ReplaySession.this.context.getPlayedFrames() / (float)ReplaySession.this.context.getFramesRecorded());
                            }*/
                        }, 2L, 1L);
                        ReplaySession.this.context.setMetadata(ReplaySession.this.replay.readMeta());
                        if (ReplaySession.this.context.getMetadata() == null) {
                            for (Player player : Bukkit.getOnlinePlayers()) {
                                ReplaySession.this.sendErrorMessage(player);
                            }
                            Bukkit.getScheduler().runTaskLater(MCTV.getInstance(), () -> {
                                for (Player player : Bukkit.getOnlinePlayers()) {
                                    player.kickPlayer("lobby");
                                }
                            }, 20L);
                            return;
                        }
                        for (Map.Entry<String, String> stringStringEntry : ReplaySession.this.replay.readMapMeta().entrySet()) {
                            String mapName = stringStringEntry.getKey();
                            String mapFile = stringStringEntry.getValue();
                            try {
                                if (mapFile.contains("/worlds/")) {
                                    System.out.println("Found old Map: " + mapFile);
                                    mapFile = mapFile.replaceAll("worlds", "worlds_old");
                                }
                                File world = new File(mapFile);
                                File map = new File(mapName);
                                if (map.exists()) {
                                    Bukkit.unloadWorld(mapName, false);
                                    if (!map.delete()) {
                                        MCTV.getInstance().getLogger().info("Could not delete map.");
                                    }
                                }
                                if (!map.exists()) {
                                    map.mkdirs();
                                }
                                if (world.exists()) {
                                    FileUtils.copyDirectory(world, map);
                                } else {
                                    ReplaySession.this.context.setFileDLProgress(aDouble -> {
                                        for (Player player : Bukkit.getOnlinePlayers()) {
                                            Title title = new Title(GoAPI.getUserAPI().translate( player.getUniqueId(), "loading-title"), GoAPI.getUserAPI().translate( player.getUniqueId(), "loading-subtitle", Math.round(aDouble * 100.0)), 0, 20, 0);
                                            title.send(player);
                                        }
                                    });
                                    MCTV.getInstance().getStorage().getWorld(mapFile, map, aDouble -> {
                                        if (ReplaySession.this.context.getFileDLProgress() != null) {
                                            ReplaySession.this.context.getFileDLProgress().accept(aDouble);
                                        }
                                    });
                                }
                                final WorldCreator worldCreator = new WorldCreator(mapName);
                                if (mapName.endsWith("_nether")) {
                                    worldCreator.environment(World.Environment.NETHER);
                                } else if (mapName.endsWith("_the_end")) {
                                    worldCreator.environment(World.Environment.THE_END);
                                }
                                File file = new File(mapName, "environment.txt");
                                if (file.exists()) {
                                    System.out.println("found environment.txt");
                                    worldCreator.environment(World.Environment.valueOf(FileUtils.readFileToString(file)));
                                } else {
                                    System.out.println("no environment.txt was found");
                                }
                                Bukkit.getScheduler().runTask(MCTV.getInstance(), () -> {
                                    final World world1 = Bukkit.createWorld(worldCreator);
                                    world1.setAutoSave(false);
                                    world1.setGameRuleValue("doMobSpawning", "false");
                                    world1.setThundering(false);
                                    world1.setStorm(false);
                                    Bukkit.getScheduler().runTaskTimer(MCTV.getInstance(), () -> world1.setTime(1000L), 5L, 5L);
                                });
                            }
                            catch (IOException e) {
                                Bukkit.getLogger().log(Level.SEVERE, "Error while Maploading", e);
                                Bukkit.shutdown();
                                return;
                            }
                        }
                        System.out.println("skip time: " + ReplaySession.this.context.getSkipTime());
                        if (ReplaySession.this.context.getSkipTime() > 0) {
                            ReplaySession.this.pause();
                            int needToSkip = ReplaySession.this.context.getSkipTime();
                            int tickCounter = 1;
                            final int count = ReplaySession.this.context.getSkipTime() / 100;
                            for (int i = 0; i < ReplaySession.this.context.getSkipTime(); i += 100) {
                                final int skipInTick = Math.min(100, needToSkip);
                                final int k = i / 100;
                                Bukkit.getScheduler().runTaskLater(MCTV.getInstance(), () -> {
                                    if (count > 0) {
                                        ReplaySession.this.broadcastLoadingTitle((float)k / (float)count);
                                    }
                                    ReplaySession.this.skip(skipInTick);
                                    if (skipInTick < 100) {
                                        for (Player player : Bukkit.getOnlinePlayers()) {
                                            ReplaySession.this.resetTitle(player);
                                            if (!player.hasPotionEffect(PotionEffectType.BLINDNESS)) continue;
                                            player.removePotionEffect(PotionEffectType.BLINDNESS);
                                        }
                                        ReplaySession.this.play();
                                        ReplaySession.this.checkForTP();
                                    }
                                }, tickCounter);
                                ++tickCounter;
                                needToSkip -= 100;
                            }
                        } else {
                            ReplaySession.this.checkForTP();
                            for (Player player : Bukkit.getOnlinePlayers()) {
                                ReplaySession.this.resetTitle(player);
                                if (!player.hasPotionEffect(PotionEffectType.BLINDNESS)) continue;
                                player.removePotionEffect(PotionEffectType.BLINDNESS);
                            }
                        }
                    } else {
                        ReplaySession.this.context.getPaused().set(false);
                    }
                }

                @Override
                public void onFailure(@Nonnull Throwable throwable) {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        ReplaySession.this.sendErrorMessage(player);
                    }
                    Bukkit.getScheduler().runTaskLater(MCTV.getInstance(), () -> {
                        for (Player player : Bukkit.getOnlinePlayers()) {
                            player.kickPlayer("lobby");
                        }
                        Bukkit.shutdown();
                    }, 20L);
                }
            });
        }
    }

    private void broadcastLoadingTitle(float percent) {
        String formatted = "\u00a7e" + Math.floor(percent * 1000.0f) / 10.0 + "%";
        for (Player player : Bukkit.getOnlinePlayers()) {
            Title title = new Title(formatted, "");
            title.setFadeInTime(0);
            title.setFadeOutTime(0);
            title.setStayTime(20);
            title.send(player);
        }
    }

    private void sendErrorMessage(Player player) {
        MCTV.getInstance().sendMessage(player, "replay-not-found", gameId);
        this.resetTitle(player);
    }

    private void resetTitle(Player player) {
        Title clear = new Title("", "");
        clear.setFadeInTime(0);
        clear.setFadeOutTime(0);
        clear.setStayTime(1);
        clear.send(player);
    }

    private void checkForTP() {
        block7: {
            block8: {
                if (this.context.getTeleportToAfterSkipTime() == null) break block7;
                Vector tpVector = this.replay.getContext().getPlayerManager().getLocationVector(this.context.getTeleportToAfterSkipTime());
                for (int skippedToFind = 0; tpVector == null && !this.replay.isEOF() && skippedToFind < 100; ++skippedToFind) {
                    this.skip(1);
                    tpVector = this.replay.getContext().getPlayerManager().getLocationVector(this.context.getTeleportToAfterSkipTime());
                }
                if (tpVector == null) break block8;
                for (Player player : Bukkit.getOnlinePlayers()) {
                    player.teleport(new Location(Bukkit.getWorld(this.replay.getContext().getPlayerManager().getWorldName(this.context.getTeleportToAfterSkipTime())), tpVector.getX(), tpVector.getY(), tpVector.getZ()));
                    MCTV.getInstance().sendMessage(player, "command-spec-success", this.replay.getContext().getPlayerManager().getName(this.context.getTeleportToAfterSkipTime()));
                    player.setMetadata("conditionalfilter", new FixedMetadataValue(MCTV.getInstance(), this.replay.getContext().getPlayerManager().getName(this.context.getTeleportToAfterSkipTime())));
                    if (this.context.getVersion() >= 7) {
                        this.context.getScoreboard(this.context.getTeleportToAfterSkipTime()).addPlayer(player);
                    } else {
                        this.context.getScoreboard().addPlayer(player);
                    }
                    if (!player.hasPotionEffect(PotionEffectType.BLINDNESS)) continue;
                    player.removePotionEffect(PotionEffectType.BLINDNESS);
                }
                break block7;
            }
            Iterator<EntityPlayer> it = this.context.getPlayerManager().getPlayers().iterator();
            if (!it.hasNext()) break block7;
            EntityPlayer entityPlayer = it.next();
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.teleport(entityPlayer.getBukkitEntity().getLocation());
                MCTV.getInstance().sendMessage(player, "command-spec-success", entityPlayer.getName());
                player.setMetadata("conditionalfilter", new FixedMetadataValue(MCTV.getInstance(), entityPlayer.getName()));
                if (this.context.getVersion() >= 7) {
                    this.context.getScoreboard(this.context.getTeleportToAfterSkipTime()).addPlayer(player);
                } else {
                    this.context.getScoreboard().addPlayer(player);
                }
                if (!player.hasPotionEffect(PotionEffectType.BLINDNESS)) continue;
                player.removePotionEffect(PotionEffectType.BLINDNESS);
            }
        }
    }

    public void toggleSlowmode() {
        if (this.context.getSpeed() < 1.0f) {
            this.speed(1.0f);
        } else {
            this.speed(0.7f);
        }
    }

    public boolean isPaused() {
        return this.context.getPaused().get();
    }

    @ConstructorProperties(value={"context"})
    public ReplaySession(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return this.context;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}