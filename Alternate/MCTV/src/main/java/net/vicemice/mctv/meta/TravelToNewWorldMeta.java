package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="TravelToNewWorldMeta", opcode=21, adapter= TravelToNewWorldMeta.TravelToNewWorldMetaAdapter.class)
public class TravelToNewWorldMeta implements Meta {
    private UUID uuid;
    private String toWorld;
    private String fromWorld;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeUUID(this.uuid, out);
        DataStreamer.writeString(this.toWorld, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.uuid = DataStreamer.readUUID(in);
        this.toWorld = DataStreamer.readString(in);
    }

    @Override
    public void execute(Context context) {
        this.fromWorld = context.getPlayerManager().getWorldName(this.uuid);
        World world = Bukkit.getWorld((String)this.toWorld);
        if (world == null) {
            System.out.println("Did not find world: " + this.toWorld);
            return;
        }
        context.getPlayerManager().changeWorld(this.uuid, world);
        this.teleport(context, this.toWorld);
    }

    @Override
    public void rollback(Context context) {
        context.getPlayerManager().changeWorld(this.uuid, Bukkit.getWorld((String)this.fromWorld));
        this.teleport(context, this.fromWorld);
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    private void teleport(final Context context, String world) {
        if (!context.getPlayerManager().isRespawnedRecently(this.uuid) && context.getTeleportToAfterSkipTime() != null && this.uuid.equals(context.getTeleportToAfterSkipTime())) {
            final World bukkitWorld = Bukkit.getWorld((String)world);
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.teleport(bukkitWorld.getSpawnLocation());
            }
            Bukkit.getScheduler().runTaskLater((Plugin)MCTV.getInstance(), new Runnable(){

                @Override
                public void run() {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        player.teleport(context.getPlayerManager().getLocationVector(TravelToNewWorldMeta.this.uuid).toLocation(bukkitWorld));
                    }
                }
            }, 2L);
        }
    }

    public TravelToNewWorldMeta() {
    }

    @ConstructorProperties(value={"uuid", "toWorld", "fromWorld"})
    public TravelToNewWorldMeta(UUID uuid, String toWorld, String fromWorld) {
        this.uuid = uuid;
        this.toWorld = toWorld;
        this.fromWorld = fromWorld;
    }

    public String toString() {
        return "TravelToNewWorldMeta(uuid=" + this.uuid + ", toWorld=" + this.toWorld + ", fromWorld=" + this.fromWorld + ")";
    }

    public static class TravelToNewWorldMetaAdapter
    implements MetaAdapter,
    Listener {
        private ReplayFileWriter replayFileWriter;

        public TravelToNewWorldMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            Bukkit.getPluginManager().registerEvents((Listener)this, (Plugin)MCTV.getInstance());
        }

        @Override
        public void stop() {
        }

        @EventHandler
        public void onTeleport(PlayerTeleportEvent event) {
            if (!event.getFrom().getWorld().equals((Object)event.getTo().getWorld())) {
                this.replayFileWriter.addMeta(new TravelToNewWorldMeta(event.getPlayer().getUniqueId(), event.getTo().getWorld().getName(), null));
            }
        }
    }
}

