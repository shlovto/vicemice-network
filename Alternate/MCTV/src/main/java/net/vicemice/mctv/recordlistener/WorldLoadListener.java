package net.vicemice.mctv.recordlistener;

import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.replay.MapManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldLoadEvent;

public class WorldLoadListener
implements Listener {
    @EventHandler
    public void onWorldLoad(WorldLoadEvent event) {
        MapManager mapManager = MCTV.getInstance().getApi().getContext().getMapManager();
        if (!mapManager.getDisableAutoAdd().contains(event.getWorld().getName())) {
            mapManager.add(event.getWorld().getName(), event.getWorld().getWorldFolder().getAbsolutePath());
        }
    }
}

