package net.vicemice.mctv.listener;

import net.vicemice.hector.GoAPI;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.data.UserData;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerDisconnectListener implements Listener {
    @EventHandler
    public void onPlayerDisconnect(PlayerQuitEvent event) {
        event.setQuitMessage(null);
        MCTV.getInstance().getApi().getContext().getScoreboard().removePlayer(event.getPlayer());
        UserData.removeUserData(event.getPlayer());
        if (MCTV.getInstance().getReplaySession().isMaster(event.getPlayer())) {
            for (Player all : Bukkit.getOnlinePlayers()) {
                if (all.equals(event.getPlayer())) continue;
                all.sendMessage(GoAPI.getUserAPI().translate(all.getUniqueId(), "master-left"));
                all.kickPlayer("lobby");
            }
            new BukkitRunnable(){
                public void run() {
                    Bukkit.shutdown();
                }
            }.runTaskLater(MCTV.getInstance(), 100L);
        }
        MCTV.getInstance().getReplaySession().getContext().getAccVisibility().remove(event.getPlayer().getUniqueId());
    }
}

