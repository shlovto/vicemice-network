package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

@MetaInfo(name="PingMeta", opcode=30, adapter= PingMeta.PingMetaAdapter.class)
public class PingMeta
implements Meta {
    private UUID uuid;
    private int ping;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeUUID(this.uuid, out);
        DataStreamer.writeVarInt(this.ping, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.uuid = DataStreamer.readUUID(in);
        this.ping = DataStreamer.readVarInt(in);
    }

    @Override
    public void execute(Context context) {
        EntityPlayer entityPlayer = context.getPlayerManager().getFakePlayers().get(context.getPlayerManager().getEntityIdByUUID().get(this.uuid));
        entityPlayer.ping = this.ping;
        PacketPlayOutPlayerInfo playerInfo = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.UPDATE_LATENCY, new EntityPlayer[]{entityPlayer});
        for (Player player : Bukkit.getOnlinePlayers()) {
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket((Packet)playerInfo);
        }
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return true;
    }

    public PingMeta() {
    }

    @ConstructorProperties(value={"uuid", "ping"})
    public PingMeta(UUID uuid, int ping) {
        this.uuid = uuid;
        this.ping = ping;
    }

    public static class PingMetaAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;
        private BukkitTask task;

        public PingMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            this.task = Bukkit.getScheduler().runTaskTimer((Plugin)MCTV.getInstance(), new Runnable(){

                @Override
                public void run() {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        PingMeta meta = new PingMeta(player.getUniqueId(), ((CraftPlayer)player).getHandle().ping);
                        replayFileWriter.addMeta(meta);
                    }
                }
            }, 1L, 1L);
        }

        @Override
        public void stop() {
            this.task.cancel();
        }
    }
}

