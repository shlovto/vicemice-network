package net.vicemice.mctv.replay;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerChat;
import com.google.common.util.concurrent.ListenableFuture;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import net.vicemice.hector.server.GoServer;
import net.vicemice.mctv.data.VerboseDisplayType;
import net.vicemice.mctv.replay.entity.FakePlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

public class Context
implements Runnable {
    private MapManager mapManager = new MapManager();
    private int mapDestruction = 0;
    private int teams = 2;
    private Map<String, String> metadata = new HashMap<String, String>();
    private String loggerPrefix;
    private int version;
    private final EntityManager entityManager = new EntityManager();
    private final FakePlayerManager playerManager = new FakePlayerManager();
    private int playedFrames = 0;
    private AtomicBoolean paused = new AtomicBoolean(true);
    private UUID master;
    private int skipTime;
    private UUID teleportToAfterSkipTime;
    private float speed = 1.0f;
    private AtomicInteger skipped = new AtomicInteger(0);
    private int framesRecorded;
    private ScoreboardManager scoreboard = new ScoreboardManager();
    private Map<UUID, ScoreboardManager> scoreboards = new HashMap<UUID, ScoreboardManager>();
    private BukkitTask actionBarTask;
    private WrapperPlayServerChat currentActionBarMessage = null;
    private Consumer<Double> fileDLProgress = null;
    private final Map<UUID, ListenableFuture<VerboseDisplayType>> accVisibility = new ConcurrentHashMap<UUID, ListenableFuture<VerboseDisplayType>>();

    public Player getMasterPlayer() {
        return Bukkit.getPlayer(this.master);
    }

    public void warn(String message) {
        System.out.println("[WARN] " + this.loggerPrefix + message);
    }

    public void error(String message) {
        System.out.println("[ERROR] " + this.loggerPrefix + message);
    }

    public void info(String message) {
        System.out.println("[INFO] " + this.loggerPrefix + message);
    }

    public ScoreboardManager getScoreboard(UUID uuid) {
        return this.scoreboards.computeIfAbsent(uuid, uuid1 -> new ScoreboardManager());
    }

    public VerboseDisplayType getVerboseDisplayType(final UUID player) {
        return GoServer.getService().getPlayerManager().getPlayer(player).getSettingsPacket().getSetting("replay-acc-verbose", VerboseDisplayType.class);
    }

    @Override
    public void run() {
        if (this.currentActionBarMessage != null) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                this.currentActionBarMessage.sendPacket(player);
            }
        }
    }

    public MapManager getMapManager() {
        return this.mapManager;
    }

    public int getMapDestruction() {
        return this.mapDestruction;
    }

    public int getTeams() {
        return this.teams;
    }

    public Map<String, String> getMetadata() {
        return this.metadata;
    }

    public String getLoggerPrefix() {
        return this.loggerPrefix;
    }

    public int getVersion() {
        return this.version;
    }

    public EntityManager getEntityManager() {
        return this.entityManager;
    }

    public FakePlayerManager getPlayerManager() {
        return this.playerManager;
    }

    public int getPlayedFrames() {
        return this.playedFrames;
    }

    public AtomicBoolean getPaused() {
        return this.paused;
    }

    public UUID getMaster() {
        return this.master;
    }

    public int getSkipTime() {
        return this.skipTime;
    }

    public UUID getTeleportToAfterSkipTime() {
        return this.teleportToAfterSkipTime;
    }

    public float getSpeed() {
        return this.speed;
    }

    public AtomicInteger getSkipped() {
        return this.skipped;
    }

    public int getFramesRecorded() {
        return this.framesRecorded;
    }

    public ScoreboardManager getScoreboard() {
        return this.scoreboard;
    }

    public Map<UUID, ScoreboardManager> getScoreboards() {
        return this.scoreboards;
    }

    public BukkitTask getActionBarTask() {
        return this.actionBarTask;
    }

    public WrapperPlayServerChat getCurrentActionBarMessage() {
        return this.currentActionBarMessage;
    }

    public Consumer<Double> getFileDLProgress() {
        return this.fileDLProgress;
    }

    public Map<UUID, ListenableFuture<VerboseDisplayType>> getAccVisibility() {
        return this.accVisibility;
    }

    public void setMapManager(MapManager mapManager) {
        this.mapManager = mapManager;
    }

    public void setMapDestruction(int mapDestruction) {
        this.mapDestruction = mapDestruction;
    }

    public void setTeams(int teams) {
        this.teams = teams;
    }

    public void setMetadata(Map<String, String> metadata) {
        this.metadata = metadata;
    }

    public void setLoggerPrefix(String loggerPrefix) {
        this.loggerPrefix = loggerPrefix;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void setPlayedFrames(int playedFrames) {
        this.playedFrames = playedFrames;
    }

    public void setPaused(AtomicBoolean paused) {
        this.paused = paused;
    }

    public void setMaster(UUID master) {
        this.master = master;
    }

    public void setSkipTime(int skipTime) {
        this.skipTime = skipTime;
    }

    public void setTeleportToAfterSkipTime(UUID teleportToAfterSkipTime) {
        this.teleportToAfterSkipTime = teleportToAfterSkipTime;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void setSkipped(AtomicInteger skipped) {
        this.skipped = skipped;
    }

    public void setFramesRecorded(int framesRecorded) {
        this.framesRecorded = framesRecorded;
    }

    public void setScoreboard(ScoreboardManager scoreboard) {
        this.scoreboard = scoreboard;
    }

    public void setScoreboards(Map<UUID, ScoreboardManager> scoreboards) {
        this.scoreboards = scoreboards;
    }

    public void setActionBarTask(BukkitTask actionBarTask) {
        this.actionBarTask = actionBarTask;
    }

    public void setCurrentActionBarMessage(WrapperPlayServerChat currentActionBarMessage) {
        this.currentActionBarMessage = currentActionBarMessage;
    }

    public void setFileDLProgress(Consumer<Double> fileDLProgress) {
        this.fileDLProgress = fileDLProgress;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Context)) {
            return false;
        }
        Context other = (Context)o;
        if (!other.canEqual(this)) {
            return false;
        }
        MapManager this$mapManager = this.getMapManager();
        MapManager other$mapManager = other.getMapManager();
        if (this$mapManager == null ? other$mapManager != null : !this$mapManager.equals(other$mapManager)) {
            return false;
        }
        if (this.getMapDestruction() != other.getMapDestruction()) {
            return false;
        }
        if (this.getTeams() != other.getTeams()) {
            return false;
        }
        Map<String, String> this$metadata = this.getMetadata();
        Map<String, String> other$metadata = other.getMetadata();
        if (this$metadata == null ? other$metadata != null : !((Object)this$metadata).equals(other$metadata)) {
            return false;
        }
        String this$loggerPrefix = this.getLoggerPrefix();
        String other$loggerPrefix = other.getLoggerPrefix();
        if (this$loggerPrefix == null ? other$loggerPrefix != null : !this$loggerPrefix.equals(other$loggerPrefix)) {
            return false;
        }
        if (this.getVersion() != other.getVersion()) {
            return false;
        }
        EntityManager this$entityManager = this.getEntityManager();
        EntityManager other$entityManager = other.getEntityManager();
        if (this$entityManager == null ? other$entityManager != null : !this$entityManager.equals(other$entityManager)) {
            return false;
        }
        FakePlayerManager this$playerManager = this.getPlayerManager();
        FakePlayerManager other$playerManager = other.getPlayerManager();
        if (this$playerManager == null ? other$playerManager != null : !this$playerManager.equals(other$playerManager)) {
            return false;
        }
        if (this.getPlayedFrames() != other.getPlayedFrames()) {
            return false;
        }
        AtomicBoolean this$paused = this.getPaused();
        AtomicBoolean other$paused = other.getPaused();
        if (this$paused == null ? other$paused != null : !this$paused.equals(other$paused)) {
            return false;
        }
        UUID this$master = this.getMaster();
        UUID other$master = other.getMaster();
        if (this$master == null ? other$master != null : !((Object)this$master).equals(other$master)) {
            return false;
        }
        if (this.getSkipTime() != other.getSkipTime()) {
            return false;
        }
        UUID this$teleportToAfterSkipTime = this.getTeleportToAfterSkipTime();
        UUID other$teleportToAfterSkipTime = other.getTeleportToAfterSkipTime();
        if (this$teleportToAfterSkipTime == null ? other$teleportToAfterSkipTime != null : !((Object)this$teleportToAfterSkipTime).equals(other$teleportToAfterSkipTime)) {
            return false;
        }
        if (Float.compare(this.getSpeed(), other.getSpeed()) != 0) {
            return false;
        }
        AtomicInteger this$skipped = this.getSkipped();
        AtomicInteger other$skipped = other.getSkipped();
        if (this$skipped == null ? other$skipped != null : !this$skipped.equals(other$skipped)) {
            return false;
        }
        if (this.getFramesRecorded() != other.getFramesRecorded()) {
            return false;
        }
        ScoreboardManager this$scoreboard = this.getScoreboard();
        ScoreboardManager other$scoreboard = other.getScoreboard();
        if (this$scoreboard == null ? other$scoreboard != null : !((Object)this$scoreboard).equals(other$scoreboard)) {
            return false;
        }
        Map<UUID, ScoreboardManager> this$scoreboards = this.getScoreboards();
        Map<UUID, ScoreboardManager> other$scoreboards = other.getScoreboards();
        if (this$scoreboards == null ? other$scoreboards != null : !((Object)this$scoreboards).equals(other$scoreboards)) {
            return false;
        }
        BukkitTask this$actionBarTask = this.getActionBarTask();
        BukkitTask other$actionBarTask = other.getActionBarTask();
        if (this$actionBarTask == null ? other$actionBarTask != null : !this$actionBarTask.equals((Object)other$actionBarTask)) {
            return false;
        }
        WrapperPlayServerChat this$currentActionBarMessage = this.getCurrentActionBarMessage();
        WrapperPlayServerChat other$currentActionBarMessage = other.getCurrentActionBarMessage();
        if (this$currentActionBarMessage == null ? other$currentActionBarMessage != null : !this$currentActionBarMessage.equals((Object)other$currentActionBarMessage)) {
            return false;
        }
        Consumer<Double> this$fileDLProgress = this.getFileDLProgress();
        Consumer<Double> other$fileDLProgress = other.getFileDLProgress();
        if (this$fileDLProgress == null ? other$fileDLProgress != null : !this$fileDLProgress.equals(other$fileDLProgress)) {
            return false;
        }
        Map<UUID, ListenableFuture<VerboseDisplayType>> this$accVisibility = this.getAccVisibility();
        Map<UUID, ListenableFuture<VerboseDisplayType>> other$accVisibility = other.getAccVisibility();
        return !(this$accVisibility == null ? other$accVisibility != null : !((Object)this$accVisibility).equals(other$accVisibility));
    }

    protected boolean canEqual(Object other) {
        return other instanceof Context;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        MapManager $mapManager = this.getMapManager();
        result = result * 59 + ($mapManager == null ? 43 : $mapManager.hashCode());
        result = result * 59 + this.getMapDestruction();
        result = result * 59 + this.getTeams();
        Map<String, String> $metadata = this.getMetadata();
        result = result * 59 + ($metadata == null ? 43 : ((Object)$metadata).hashCode());
        String $loggerPrefix = this.getLoggerPrefix();
        result = result * 59 + ($loggerPrefix == null ? 43 : $loggerPrefix.hashCode());
        result = result * 59 + this.getVersion();
        EntityManager $entityManager = this.getEntityManager();
        result = result * 59 + ($entityManager == null ? 43 : $entityManager.hashCode());
        FakePlayerManager $playerManager = this.getPlayerManager();
        result = result * 59 + ($playerManager == null ? 43 : $playerManager.hashCode());
        result = result * 59 + this.getPlayedFrames();
        AtomicBoolean $paused = this.getPaused();
        result = result * 59 + ($paused == null ? 43 : $paused.hashCode());
        UUID $master = this.getMaster();
        result = result * 59 + ($master == null ? 43 : ((Object)$master).hashCode());
        result = result * 59 + this.getSkipTime();
        UUID $teleportToAfterSkipTime = this.getTeleportToAfterSkipTime();
        result = result * 59 + ($teleportToAfterSkipTime == null ? 43 : ((Object)$teleportToAfterSkipTime).hashCode());
        result = result * 59 + Float.floatToIntBits(this.getSpeed());
        AtomicInteger $skipped = this.getSkipped();
        result = result * 59 + ($skipped == null ? 43 : $skipped.hashCode());
        result = result * 59 + this.getFramesRecorded();
        ScoreboardManager $scoreboard = this.getScoreboard();
        result = result * 59 + ($scoreboard == null ? 43 : ((Object)$scoreboard).hashCode());
        Map<UUID, ScoreboardManager> $scoreboards = this.getScoreboards();
        result = result * 59 + ($scoreboards == null ? 43 : ((Object)$scoreboards).hashCode());
        BukkitTask $actionBarTask = this.getActionBarTask();
        result = result * 59 + ($actionBarTask == null ? 43 : $actionBarTask.hashCode());
        WrapperPlayServerChat $currentActionBarMessage = this.getCurrentActionBarMessage();
        result = result * 59 + ($currentActionBarMessage == null ? 43 : $currentActionBarMessage.hashCode());
        Consumer<Double> $fileDLProgress = this.getFileDLProgress();
        result = result * 59 + ($fileDLProgress == null ? 43 : $fileDLProgress.hashCode());
        Map<UUID, ListenableFuture<VerboseDisplayType>> $accVisibility = this.getAccVisibility();
        result = result * 59 + ($accVisibility == null ? 43 : ((Object)$accVisibility).hashCode());
        return result;
    }

    public String toString() {
        return "Context(mapManager=" + this.getMapManager() + ", mapDestruction=" + this.getMapDestruction() + ", teams=" + this.getTeams() + ", metadata=" + this.getMetadata() + ", loggerPrefix=" + this.getLoggerPrefix() + ", version=" + this.getVersion() + ", entityManager=" + this.getEntityManager() + ", playerManager=" + this.getPlayerManager() + ", playedFrames=" + this.getPlayedFrames() + ", paused=" + this.getPaused() + ", master=" + this.getMaster() + ", skipTime=" + this.getSkipTime() + ", teleportToAfterSkipTime=" + this.getTeleportToAfterSkipTime() + ", speed=" + this.getSpeed() + ", skipped=" + this.getSkipped() + ", framesRecorded=" + this.getFramesRecorded() + ", scoreboard=" + this.getScoreboard() + ", scoreboards=" + this.getScoreboards() + ", actionBarTask=" + (Object)this.getActionBarTask() + ", currentActionBarMessage=" + (Object)this.getCurrentActionBarMessage() + ", fileDLProgress=" + this.getFileDLProgress() + ", accVisibility=" + this.getAccVisibility() + ")";
    }
}

