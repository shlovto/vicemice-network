package net.vicemice.mctv.replay.entity;

import net.vicemice.mctv.MCTV;
import net.minecraft.server.v1_8_R3.AxisAlignedBB;
import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.EntityFishingHook;
import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.Item;
import net.minecraft.server.v1_8_R3.MinecraftKey;
import net.minecraft.server.v1_8_R3.Vec3D;

public class FakeFishRod
extends EntityFishingHook {
    public FakeFishRod(EntityHuman owner) {
        super(owner.getWorld(), owner);
    }

    public void t_() {
        if (this.owner.dead || !this.owner.isAlive()) {
            this.die();
            return;
        }
        if (this.owner.inventory.getCarried() == null || !this.owner.inventory.getCarried().getItem().equals(Item.REGISTRY.get(new MinecraftKey("minecraft:fishing_rod")))) {
            this.die();
            return;
        }
        if (this.h((Entity)this.owner) > 100.0) {
            this.die();
            return;
        }
        for (EntityPlayer entityPlayer : MCTV.getInstance().getApi().getContext().getPlayerManager().getPlayers()) {
            AxisAlignedBB bounding;
            if (!entityPlayer.getWorld().equals((Object)this.owner.getWorld()) || !(bounding = this.getBoundingBox().grow(1.0, 1.0, 1.0)).a(new Vec3D(entityPlayer.locX, entityPlayer.locY, entityPlayer.locZ))) continue;
            this.die();
            return;
        }
    }

    public void die() {
        super.die();
        MCTV.getInstance().getApi().getContext().getEntityManager().removeFishingHook(this.owner.getId());
    }
}

