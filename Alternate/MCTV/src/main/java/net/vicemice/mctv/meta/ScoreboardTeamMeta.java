package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerEntityEffect;
import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerScoreboardTeam;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.replay.ScoreboardManager;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Team;

@MetaInfo(name="ScoreboardTeamMeta", opcode=18, adapter= ScoreboardTeamMeta.ScoreboardTeamMetaAdapter.class)
public class ScoreboardTeamMeta implements Meta {
    private String name;
    private String displayname;
    private String nametagVisibility;
    private String prefix;
    private String suffix;
    private int color;
    private int mode;
    private int packOptionData;
    private int amount;
    private List<String> players;
    private String oldPrefix;
    private String oldSuffix;
    private String oldDisplayName;
    private Set<String> oldPlayers;
    private UUID uuid;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeString(this.name, out);
        DataStreamer.writeString(this.displayname, out);
        DataStreamer.writeString(this.nametagVisibility, out);
        DataStreamer.writeString(this.prefix, out);
        DataStreamer.writeString(this.suffix, out);
        DataStreamer.writeVarInt(this.color, out);
        DataStreamer.writeVarInt(this.mode, out);
        DataStreamer.writeVarInt(this.packOptionData, out);
        DataStreamer.writeVarInt(this.amount, out);
        for (String s : this.players) {
            DataStreamer.writeString(s, out);
        }
        DataStreamer.writeUUID(this.uuid, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.name = DataStreamer.readString(in);
        this.displayname = DataStreamer.readString(in);
        this.nametagVisibility = DataStreamer.readString(in);
        this.prefix = DataStreamer.readString(in);
        this.suffix = DataStreamer.readString(in);
        this.color = DataStreamer.readVarInt(in);
        this.mode = DataStreamer.readVarInt(in);
        this.packOptionData = DataStreamer.readVarInt(in);
        this.amount = DataStreamer.readVarInt(in);
        if (this.players == null) {
            this.players = new ArrayList<String>();
        }
        for (int i = 0; i < this.amount; ++i) {
            this.players.add(DataStreamer.readString(in));
        }
        if (version >= 7) {
            this.uuid = DataStreamer.readUUID(in);
        }
    }

    @Override
    public void rollback(Context context) {
        block11: {
            ScoreboardManager scoreboard;
            block14: {
                block13: {
                    block12: {
                        block10: {
                            scoreboard = this.uuid == null ? context.getScoreboard() : context.getScoreboard(this.uuid);
                            if (this.mode != 0) break block10;
                            scoreboard.removeTeam(this.name);
                            break block11;
                        }
                        if (this.mode != 1) break block12;
                        Team team = scoreboard.getTeam(this.name);
                        team.setDisplayName(this.oldDisplayName != null ? this.oldDisplayName : this.name);
                        team.setPrefix(this.oldPrefix);
                        team.setSuffix(this.oldSuffix);
                        boolean isSpecTeam = this.name.startsWith("spec");
                        if (isSpecTeam) {
                            team.setNameTagVisibility(NameTagVisibility.NEVER);
                        }
                        for (String player : this.oldPlayers) {
                            if (isSpecTeam) {
                                for (EntityPlayer entityPlayer : context.getPlayerManager().getPlayers()) {
                                    if (!entityPlayer.getName().equals(player)) continue;
                                    WrapperPlayServerEntityEffect effect = new WrapperPlayServerEntityEffect();
                                    effect.setEffectID((byte)14);
                                    effect.setHideParticles(true);
                                    effect.setDuration(Integer.MAX_VALUE);
                                    effect.setAmplifier((byte)0);
                                    effect.setEntityID(entityPlayer.getId());
                                    for (Player player1 : Bukkit.getOnlinePlayers()) {
                                        effect.sendPacket(player1);
                                    }
                                }
                            }
                            team.addEntry(player);
                        }
                        break block11;
                    }
                    if (this.mode != 2) break block13;
                    Team team = scoreboard.getTeam(this.name);
                    team.setDisplayName(this.oldDisplayName != null ? this.oldDisplayName : this.name);
                    team.setPrefix(this.oldPrefix);
                    team.setSuffix(this.oldSuffix);
                    break block11;
                }
                if (this.mode != 3) break block14;
                Team team = scoreboard.getTeam(this.name);
                for (String player : this.oldPlayers) {
                    team.removeEntry(player);
                }
                break block11;
            }
            if (this.mode != 4) break block11;
            Team team = scoreboard.getTeam(this.name);
            boolean isSpecTeam = this.name.startsWith("spec");
            for (String player : this.oldPlayers) {
                if (isSpecTeam) {
                    for (EntityPlayer entityPlayer : context.getPlayerManager().getPlayers()) {
                        if (!entityPlayer.getName().equals(player)) continue;
                        WrapperPlayServerEntityEffect effect = new WrapperPlayServerEntityEffect();
                        effect.setEffectID((byte)14);
                        effect.setHideParticles(true);
                        effect.setDuration(Integer.MAX_VALUE);
                        effect.setAmplifier((byte)0);
                        effect.setEntityID(entityPlayer.getId());
                        for (Player player1 : Bukkit.getOnlinePlayers()) {
                            effect.sendPacket(player1);
                        }
                    }
                }
                team.addEntry(player);
            }
        }
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    @Override
    public void execute(Context context) {
        block12: {
            ScoreboardManager scoreboard;
            block15: {
                block14: {
                    block13: {
                        block11: {
                            if (context.getVersion() == 0) {
                                return;
                            }
                            scoreboard = this.uuid == null ? context.getScoreboard() : context.getScoreboard(this.uuid);
                            if (this.mode != 0) break block11;
                            Team team = scoreboard.getTeam(this.name);
                            team.setDisplayName(this.displayname != null ? this.displayname : this.name);
                            team.setPrefix(this.prefix != null ? this.prefix : "");
                            team.setSuffix(this.suffix != null ? this.suffix : "");
                            boolean isSpecTeam = this.name.startsWith("spec");
                            if (isSpecTeam) {
                                team.setNameTagVisibility(NameTagVisibility.NEVER);
                            }
                            for (String player : this.players) {
                                if (isSpecTeam) {
                                    for (EntityPlayer entityPlayer : context.getPlayerManager().getPlayers()) {
                                        if (!entityPlayer.getName().equals(player)) continue;
                                        WrapperPlayServerEntityEffect effect = new WrapperPlayServerEntityEffect();
                                        effect.setEffectID((byte)14);
                                        effect.setHideParticles(true);
                                        effect.setDuration(Integer.MAX_VALUE);
                                        effect.setAmplifier((byte)0);
                                        effect.setEntityID(entityPlayer.getId());
                                        for (Player player1 : Bukkit.getOnlinePlayers()) {
                                            effect.sendPacket(player1);
                                        }
                                    }
                                }
                                team.addEntry(player);
                            }
                            break block12;
                        }
                        if (this.mode != 1) break block13;
                        Team team = scoreboard.getTeam(this.name);
                        this.oldPrefix = team.getPrefix();
                        this.oldSuffix = team.getSuffix();
                        this.oldDisplayName = team.getDisplayName();
                        this.oldPlayers = team.getEntries();
                        context.getScoreboard().removeTeam(this.name);
                        break block12;
                    }
                    if (this.mode != 2) break block14;
                    Team team = scoreboard.getTeam(this.name);
                    this.oldPrefix = team.getPrefix();
                    this.oldSuffix = team.getSuffix();
                    this.oldDisplayName = team.getDisplayName();
                    team.setDisplayName(this.displayname != null ? this.displayname : this.name);
                    team.setPrefix(this.prefix != null ? this.prefix : "");
                    team.setSuffix(this.suffix != null ? this.suffix : "");
                    break block12;
                }
                if (this.mode != 3) break block15;
                this.oldPlayers = new HashSet<String>(this.players);
                Team team = scoreboard.getTeam(this.name);
                boolean isSpecTeam = this.name.startsWith("spec");
                for (String player : this.players) {
                    if (isSpecTeam) {
                        for (EntityPlayer entityPlayer : context.getPlayerManager().getPlayers()) {
                            if (!entityPlayer.getName().equals(player)) continue;
                            WrapperPlayServerEntityEffect effect = new WrapperPlayServerEntityEffect();
                            effect.setEffectID((byte)14);
                            effect.setHideParticles(true);
                            effect.setDuration(Integer.MAX_VALUE);
                            effect.setAmplifier((byte)0);
                            effect.setEntityID(entityPlayer.getId());
                            for (Player player1 : Bukkit.getOnlinePlayers()) {
                                effect.sendPacket(player1);
                            }
                        }
                    }
                    team.addEntry(player);
                }
                break block12;
            }
            if (this.mode != 4) break block12;
            this.oldPlayers = new HashSet<String>(this.players);
            Team team = scoreboard.getTeam(this.name);
            for (String player : this.players) {
                team.removeEntry(player);
            }
        }
    }

    public ScoreboardTeamMeta() {
    }

    @ConstructorProperties(value={"name", "displayname", "nametagVisibility", "prefix", "suffix", "color", "mode", "packOptionData", "amount", "players", "oldPrefix", "oldSuffix", "oldDisplayName", "oldPlayers", "uuid"})
    public ScoreboardTeamMeta(String name, String displayname, String nametagVisibility, String prefix, String suffix, int color, int mode, int packOptionData, int amount, List<String> players, String oldPrefix, String oldSuffix, String oldDisplayName, Set<String> oldPlayers, UUID uuid) {
        this.name = name;
        this.displayname = displayname;
        this.nametagVisibility = nametagVisibility;
        this.prefix = prefix;
        this.suffix = suffix;
        this.color = color;
        this.mode = mode;
        this.packOptionData = packOptionData;
        this.amount = amount;
        this.players = players;
        this.oldPrefix = oldPrefix;
        this.oldSuffix = oldSuffix;
        this.oldDisplayName = oldDisplayName;
        this.oldPlayers = oldPlayers;
        this.uuid = uuid;
    }

    public static class ScoreboardTeamMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;

        public ScoreboardTeamMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.SCOREBOARD_TEAM});
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }

        public void onPacketSending(PacketEvent e) {
            WrapperPlayServerScoreboardTeam packet = new WrapperPlayServerScoreboardTeam(e.getPacket());
            String name = packet.getName();
            String displayname = packet.getDisplayName();
            String nametagVisibility = packet.getNameTagVisibility();
            String prefix = packet.getPrefix();
            String suffix = packet.getSuffix();
            int color = packet.getColor();
            int mode = packet.getMode();
            int packOptionData = packet.getPackOptionData();
            List players = packet.getPlayers();
            int amount = players.size();
            ScoreboardTeamMeta meta = new ScoreboardTeamMeta(name, displayname, nametagVisibility, prefix, suffix, color, mode, packOptionData, amount, players, null, null, null, null, e.getPlayer().getUniqueId());
            this.replayFileWriter.addMeta(meta);
        }

        public void onPacketReceiving(PacketEvent e) {
        }
    }
}

