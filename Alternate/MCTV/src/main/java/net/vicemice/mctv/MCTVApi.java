package net.vicemice.mctv;

import java.beans.ConstructorProperties;
import java.util.UUID;
import lombok.NonNull;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.*;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class MCTVApi {
    private final MCTV mctv;
    private Context context = new Context();
    private ReplayFileWriter writer;

    @Deprecated
    public void addMap(String name, String folder) {
    }

    @Deprecated
    public void startRecording() {
    }

    @Deprecated
    public void stopRecording() {
    }

    @Deprecated
    public void save() {
    }

    @Deprecated
    public void ignorePlayer(Player player) {
    }

    @Deprecated
    public void uningnorePlayer(Player player) {
    }

    public int getRecordedSeconds() {
        return this.writer == null ? -1 : this.writer.getRecordedSeconds();
    }

    public void setBlock(Location location, Material material, byte data) {
        if (this.writer != null) {
            BlockPlaceMeta blockPlaceMeta = new BlockPlaceMeta(location.getWorld().getName(), location, material, data, null, (byte)0, null, (byte)0);
            this.writer.addMeta(blockPlaceMeta);
        }
    }

    @Deprecated
    public void switchWorld(String toWorld) {
    }

    @Deprecated
    public void switchBoardTarget(Player p) {
    }

    public void changeTeamStatus(int teams) {
        this.mctv.getApi().getContext().setTeams(teams);
        this.mctv.log("TeamsStatus changed to: " + teams);
    }

    public void changeMapDestruction(int i) {
        this.mctv.getApi().getContext().setMapDestruction(i);
        this.mctv.log("MapDestruction changed to: " + i);
    }

    public void spawnTNTPrimed(Location location, int ticksDiff) {
        if (this.writer != null) {
            TNTPrimeMeta tntPrimeMeta = new TNTPrimeMeta(location, 0, null, null);
            this.writer.addMeta(tntPrimeMeta);
        }
    }

    public void spawnTNTPrimed(Location location) {
        this.spawnTNTPrimed(location, 0);
    }

    public void writeMessage(@NonNull String message) {
        if (message == null) {
            throw new NullPointerException("message");
        }
        MessageMeta messageMeta = new MessageMeta(message);
        this.writer.addMeta(messageMeta);
    }

    public void writeMessage(@NonNull String message, @NonNull String locale) {
        if (message == null) {
            throw new NullPointerException("message");
        }
        if (locale == null) {
            throw new NullPointerException("locale");
        }
        LocaleMessageMeta localeMessageMeta = new LocaleMessageMeta(message, locale);
        this.writer.addMeta(localeMessageMeta);
    }

    public void writeConditionalMessage(@NonNull String message, @NonNull String condition) {
        if (message == null) {
            throw new NullPointerException("message");
        }
        if (condition == null) {
            throw new NullPointerException("condition");
        }
        this.writeConditionalMessage(message, condition, null);
    }

    public void writeConditionalMessage(@NonNull String message, @NonNull String condition, UUID player) {
        if (message == null) {
            throw new NullPointerException("message");
        }
        if (condition == null) {
            throw new NullPointerException("condition");
        }
        this.writer.addMeta(new ConditionalMessageMeta(message, condition, player));
    }

    public Context getContext() {
        return this.context;
    }

    @ConstructorProperties(value={"mctv"})
    public MCTVApi(MCTV mctv) {
        this.mctv = mctv;
    }

    public ReplayFileWriter getWriter() {
        return this.writer;
    }

    public void setWriter(ReplayFileWriter writer) {
        this.writer = writer;
    }
}

