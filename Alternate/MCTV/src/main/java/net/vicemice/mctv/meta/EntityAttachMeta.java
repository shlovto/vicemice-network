package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerAttachEntity;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="VehicleEnterMeta", adapter= EntityAttachMeta.EntityAttachMetaAdapter.class, opcode=40)
public class EntityAttachMeta
implements Meta {
    private UUID uuid;
    private int vehicleId;
    private boolean leash;
    private boolean isMob = false;
    private int entityId;

    public EntityAttachMeta(UUID uuid, int vehicleId, boolean leash) {
        this.uuid = uuid;
        this.vehicleId = vehicleId;
        this.leash = leash;
    }

    public EntityAttachMeta(int entityId, int vehicleId, boolean leash) {
        this.entityId = entityId;
        this.vehicleId = vehicleId;
        this.leash = leash;
        this.isMob = true;
    }

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        out.writeBoolean(this.isMob);
        if (this.isMob) {
            out.writeInt(this.entityId);
        } else {
            DataStreamer.writeUUID(this.uuid, out);
        }
        out.writeInt(this.vehicleId);
        out.writeBoolean(this.leash);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.isMob = in.readBoolean();
        if (this.isMob) {
            this.entityId = in.readInt();
        } else {
            this.uuid = DataStreamer.readUUID(in);
        }
        this.vehicleId = in.readInt();
        this.leash = in.readBoolean();
    }

    @Override
    public void execute(Context context) {
        int id = context.getPlayerManager().getEntityIdByUUID(this.uuid, context.getMasterPlayer().getWorld());
        System.out.println("logging entity mount id " + id + ", vehicle " + this.vehicleId);
        WrapperPlayServerAttachEntity packet = new WrapperPlayServerAttachEntity();
        packet.getHandle().getIntegers().write(1, id);
        packet.setVehicleId(this.vehicleId);
        packet.setLeash(this.leash);
        for (Player player : Bukkit.getOnlinePlayers()) {
            packet.sendPacket(player);
        }
    }

    @Override
    public void rollback(Context context) {
        int playerId = context.getPlayerManager().getEntityIdByUUID(this.uuid, context.getMasterPlayer().getWorld());
        WrapperPlayServerAttachEntity packet = new WrapperPlayServerAttachEntity();
        packet.setEntityID(playerId);
        packet.setLeash(false);
        for (Player player : Bukkit.getOnlinePlayers()) {
            packet.sendPacket(player);
        }
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public EntityAttachMeta() {
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof EntityAttachMeta)) {
            return false;
        }
        EntityAttachMeta other = (EntityAttachMeta)o;
        if (!other.canEqual(this)) {
            return false;
        }
        UUID this$uuid = this.uuid;
        UUID other$uuid = other.uuid;
        if (this$uuid == null ? other$uuid != null : !((Object)this$uuid).equals(other$uuid)) {
            return false;
        }
        if (this.vehicleId != other.vehicleId) {
            return false;
        }
        if (this.leash != other.leash) {
            return false;
        }
        if (this.isMob != other.isMob) {
            return false;
        }
        return this.entityId == other.entityId;
    }

    protected boolean canEqual(Object other) {
        return other instanceof EntityAttachMeta;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        UUID $uuid = this.uuid;
        result = result * 59 + ($uuid == null ? 43 : ((Object)$uuid).hashCode());
        result = result * 59 + this.vehicleId;
        result = result * 59 + (this.leash ? 79 : 97);
        result = result * 59 + (this.isMob ? 79 : 97);
        result = result * 59 + this.entityId;
        return result;
    }

    public static class EntityAttachMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;
        private EntityAttachMeta lastEntityAttachMeta;

        public EntityAttachMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.ATTACH_ENTITY});
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }

        public void onPacketSending(PacketEvent event) {
            WrapperPlayServerAttachEntity packet = new WrapperPlayServerAttachEntity(event.getPacket());
            Entity entity = null;
            for (Entity e : event.getPlayer().getWorld().getEntities()) {
                if (e.getEntityId() != ((Integer)packet.getHandle().getIntegers().read(1)).intValue()) continue;
                entity = e;
            }
            if (entity == null) {
                return;
            }
            EntityAttachMeta meta = entity instanceof Player ? new EntityAttachMeta(entity.getUniqueId(), packet.getVehicleId(), packet.getLeash()) : new EntityAttachMeta(entity.getEntityId(), packet.getVehicleId(), packet.getLeash());
            if (this.lastEntityAttachMeta != null && this.lastEntityAttachMeta.equals(meta)) {
                return;
            }
            this.lastEntityAttachMeta = meta;
            this.replayFileWriter.addMeta(meta);
        }

        public void onPacketReceiving(PacketEvent event) {
        }
    }
}

