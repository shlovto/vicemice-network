package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="PlayerDeathMeta", opcode=38, adapter= PlayerDeathMeta.PlayerDeathMetaAdapter.class)
public class PlayerDeathMeta
implements Meta {
    private UUID uuid;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeUUID(this.uuid, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.uuid = DataStreamer.readUUID(in);
    }

    @Override
    public void execute(Context context) {
    }

    @Override
    public void rollback(Context context) {
        context.getPlayerManager().spawnFakePlayer(this.uuid);
        int eId = context.getPlayerManager().getEntityIdByUUID().get(this.uuid);
        for (Player player : Bukkit.getOnlinePlayers()) {
            context.getPlayerManager().refreshEquipment((CraftPlayer)player, eId, context.getPlayerManager().getFakePlayers().get(eId));
        }
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    @ConstructorProperties(value={"uuid"})
    public PlayerDeathMeta(UUID uuid) {
        this.uuid = uuid;
    }

    public PlayerDeathMeta() {
    }

    public static class PlayerDeathMetaAdapter
    implements MetaAdapter,
    Listener {
        private ReplayFileWriter replayFileWriter;

        public PlayerDeathMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @EventHandler
        public void onPlayerDie(PlayerDeathEvent event) {
            PlayerDeathMeta meta = new PlayerDeathMeta(event.getEntity().getUniqueId());
            this.replayFileWriter.addMeta(meta);
        }

        @Override
        public void start() {
            Bukkit.getPluginManager().registerEvents((Listener)this, (Plugin)MCTV.getInstance());
        }

        @Override
        public void stop() {
            HandlerList.unregisterAll((Listener)this);
        }
    }
}

