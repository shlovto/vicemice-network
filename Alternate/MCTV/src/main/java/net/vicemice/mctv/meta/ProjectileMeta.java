package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.replay.entity.FakeFishRod;
import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityVillager;
import net.minecraft.server.v1_8_R3.World;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.Vector;

@MetaInfo(name="ProjectileMeta", opcode=9, adapter= ProjectileMeta.ProjectileMetaAdapter.class)
public class ProjectileMeta
implements Meta {
    private EntityType entityType;
    private Location location;
    private Vector velocity;
    private String worldName;
    private int entityID;
    private String uuid;
    private Potion item;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeString(this.location.getWorld().getName(), out);
        out.writeDouble(this.location.getX());
        out.writeDouble(this.location.getY());
        out.writeDouble(this.location.getZ());
        out.writeFloat(this.location.getPitch());
        out.writeFloat(this.location.getYaw());
        DataStreamer.writeString(this.entityType.name(), out);
        out.writeDouble(this.velocity.getX());
        out.writeDouble(this.velocity.getY());
        out.writeDouble(this.velocity.getZ());
        DataStreamer.writeVarInt(this.entityID, out);
        DataStreamer.writeString(this.uuid, out);
        if (this.item != null) {
            DataStreamer.writeItemStack(this.item.toItemStack(1), out);
            out.writeInt(this.item.getType().ordinal());
        }
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        block9: {
            this.worldName = DataStreamer.readString(in);
            this.location = new Location(null, in.readDouble(), in.readDouble(), in.readDouble(), in.readFloat(), in.readFloat());
            this.entityType = EntityType.valueOf((String)DataStreamer.readString(in));
            this.velocity = new Vector(in.readDouble(), in.readDouble(), in.readDouble());
            if (version == 4) {
                in.mark(102);
                this.entityID = DataStreamer.readVarInt(in);
                this.uuid = DataStreamer.readString(in, 36);
                try {
                    if (this.uuid == null || this.uuid.length() != 36) {
                        in.reset();
                        break block9;
                    }
                    UUID.fromString(this.uuid);
                }
                catch (IllegalArgumentException e) {
                    in.reset();
                }
            } else if (version >= 5) {
                this.entityID = DataStreamer.readVarInt(in);
                this.uuid = DataStreamer.readString(in, 36);
            }
        }
        if (version >= 15 && this.entityType.equals((Object)EntityType.SPLASH_POTION)) {
            ItemStack item = DataStreamer.readItemStack(in);
            if (item != null) {
                this.item = Potion.fromItemStack((ItemStack)item);
                this.item.setType(PotionType.values()[in.readInt()]);
            } else {
                in.readInt();
                throw new NullPointerException("Could not read itemstack: 'potion' is null");
            }
        }
    }

    @Override
    public void execute(Context context) {
        this.location.setWorld(Bukkit.getWorld((String)this.worldName));
        if (this.entityType.equals((Object)EntityType.ENDER_PEARL) || this.entityType.equals((Object)EntityType.FISHING_HOOK)) {
            if (this.entityID == 0) {
                return;
            }
            if (this.entityType.getEntityClass().equals(EnderPearl.class)) {
                EntityVillager entityVillager = new EntityVillager((World)((CraftWorld)this.location.getWorld()).getHandle());
                entityVillager.setInvisible(true);
                entityVillager.setLocation(this.location.getX(), this.location.getY(), this.location.getZ(), 0.0f, 0.0f);
                ((CraftWorld)this.location.getWorld()).getHandle().addEntity((Entity)entityVillager);
                CraftEntity entity = entityVillager.getBukkitEntity();
                EnderPearl pearl = (EnderPearl)((ProjectileSource)entity).launchProjectile(EnderPearl.class);
                pearl.setVelocity(this.velocity);
                ((CraftWorld)this.location.getWorld()).getHandle().removeEntity((Entity)entityVillager);
            } else {
                int ownerID = context.getPlayerManager().getEntityIdByUUID(UUID.fromString(this.uuid), context.getMasterPlayer().getWorld());
                EntityHuman entityHuman = (EntityHuman)context.getPlayerManager().getFakePlayers().get(ownerID);
                FakeFishRod entityFishingHook = new FakeFishRod(entityHuman);
                ((CraftWorld)this.location.getWorld()).getHandle().addEntity((Entity)entityFishingHook);
                context.getEntityManager().addFishingHook(ownerID, entityFishingHook);
            }
            return;
        }
        if (!(this.entityType.equals((Object)EntityType.ARROW) || this.entityType.equals((Object)EntityType.SNOWBALL) || this.entityType.equals((Object)EntityType.EGG) || this.entityType.equals((Object)EntityType.THROWN_EXP_BOTTLE) || this.entityType.equals((Object)EntityType.SPLASH_POTION))) {
            return;
        }
        Projectile projectile = (Projectile)this.location.getWorld().spawn(this.location, this.entityType.getEntityClass());
        if (this.entityType.equals((Object)EntityType.SPLASH_POTION)) {
            ThrownPotion thrownPotion = (ThrownPotion)projectile;
            this.item.setSplash(true);
            thrownPotion.setItem(this.item.toItemStack(1));
        }
        projectile.setVelocity(this.velocity);
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public ProjectileMeta() {
    }

    @ConstructorProperties(value={"entityType", "location", "velocity", "worldName", "entityID", "uuid", "item"})
    public ProjectileMeta(EntityType entityType, Location location, Vector velocity, String worldName, int entityID, String uuid, Potion item) {
        this.entityType = entityType;
        this.location = location;
        this.velocity = velocity;
        this.worldName = worldName;
        this.entityID = entityID;
        this.uuid = uuid;
        this.item = item;
    }

    public static class ProjectileMetaAdapter
    implements MetaAdapter,
    Listener {
        private ReplayFileWriter replayFileWriter;

        public ProjectileMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            Bukkit.getPluginManager().registerEvents((Listener)this, (Plugin)MCTV.getInstance());
        }

        @Override
        public void stop() {
        }

        @EventHandler(ignoreCancelled=true, priority=EventPriority.MONITOR)
        public void onProjectileLaunch(ProjectileLaunchEvent event) {
            if (event.getEntity().getType().equals((Object)EntityType.ENDER_PEARL) || event.getEntity().getType().equals((Object)EntityType.FISHING_HOOK)) {
                if (!(event.getEntity().getShooter() instanceof Player)) {
                    return;
                }
                ProjectileMeta projectileMeta = new ProjectileMeta(event.getEntity().getType(), event.getEntity().getLocation(), event.getEntity().getVelocity(), null, event.getEntity().getEntityId(), ((Player)event.getEntity().getShooter()).getUniqueId().toString(), null);
                this.replayFileWriter.addMeta(projectileMeta);
                return;
            }
            if (event.getEntityType().equals((Object)EntityType.SPLASH_POTION)) {
                ThrownPotion potion = (ThrownPotion)event.getEntity();
                ProjectileMeta projectileMeta = new ProjectileMeta(event.getEntity().getType(), event.getEntity().getLocation(), event.getEntity().getVelocity(), null, 0, null, Potion.fromItemStack((ItemStack)potion.getItem()));
                this.replayFileWriter.addMeta(projectileMeta);
                return;
            }
            ProjectileMeta projectileMeta = new ProjectileMeta(event.getEntity().getType(), event.getEntity().getLocation(), event.getEntity().getVelocity(), null, 0, null, null);
            this.replayFileWriter.addMeta(projectileMeta);
        }
    }
}

