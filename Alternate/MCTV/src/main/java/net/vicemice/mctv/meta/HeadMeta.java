package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerEntityHeadRotation;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

@MetaInfo(name="HeadMeta", opcode=1, adapter= HeadMeta.HeadMetaAdapter.class)
public class HeadMeta
implements Meta {
    private UUID uuid;
    private float rotation;
    private String world;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeUUID(this.uuid, out);
        out.writeFloat(this.rotation);
        DataStreamer.writeString(this.world, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.uuid = DataStreamer.readUUID(in);
        this.rotation = in.readFloat();
        if (version >= 3) {
            this.world = DataStreamer.readString(in);
        }
    }

    @Override
    public void execute(Context context) {
        if (context.getMasterPlayer() == null) {
            return;
        }
        World world = context.getMasterPlayer().getWorld();
        if (this.world != null) {
            world = Bukkit.getWorld((String)this.world);
        }
        if (world == null) {
            context.warn("Replay wanted to get entity in world '" + this.world + "' which is not loaded. Defaulting back to replay master world");
            world = context.getMasterPlayer().getWorld();
        }
        WrapperPlayServerEntityHeadRotation packet = new WrapperPlayServerEntityHeadRotation();
        packet.setEntityID(context.getPlayerManager().getEntityIdByUUID(this.uuid, world));
        packet.setHeadYaw((byte)(this.rotation * 256.0f / 360.0f));
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!player.getWorld().equals((Object)world)) continue;
            packet.sendPacket(player);
        }
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return true;
    }

    public HeadMeta() {
    }

    @ConstructorProperties(value={"uuid", "rotation", "world"})
    public HeadMeta(UUID uuid, float rotation, String world) {
        this.uuid = uuid;
        this.rotation = rotation;
        this.world = world;
    }

    public String toString() {
        return "HeadMeta(uuid=" + this.getUuid() + ", rotation=" + this.rotation + ", world=" + this.world + ")";
    }

    public UUID getUuid() {
        return this.uuid;
    }

    public static class HeadMetaAdapter
    implements MetaAdapter,
    Runnable {
        private ReplayFileWriter replayFileWriter;
        private BukkitTask task;

        public HeadMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            this.task = Bukkit.getScheduler().runTaskTimer((Plugin)MCTV.getInstance(), (Runnable)this, 0L, 1L);
        }

        @Override
        public void stop() {
            this.task.cancel();
        }

        @Override
        public void run() {
            for (Player player : Bukkit.getOnlinePlayers()) {
                HeadMeta meta = new HeadMeta(player.getUniqueId(), player.getEyeLocation().getYaw(), player.getWorld().getName());
                this.replayFileWriter.addMeta(meta);
            }
        }
    }
}

