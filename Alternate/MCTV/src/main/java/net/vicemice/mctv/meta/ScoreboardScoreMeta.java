package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerScoreboardScore;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import net.vicemice.hector.api.protocol.wrappers.EnumWrappers;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.replay.ScoreboardManager;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="ScoreboardScoreMeta", opcode=17, adapter= ScoreboardScoreMeta.ScoreboardScoreMetaAdapter.class)
public class ScoreboardScoreMeta implements Meta {
    private String scorename;
    private String objectivename;
    private int score;
    private int scoreboardAction;
    private UUID uuid;
    private int oldScore;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeString(this.scorename, out);
        DataStreamer.writeString(this.objectivename, out);
        DataStreamer.writeVarInt(this.score, out);
        DataStreamer.writeVarInt(this.scoreboardAction, out);
        DataStreamer.writeUUID(this.uuid, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.scorename = DataStreamer.readString(in);
        this.objectivename = DataStreamer.readString(in);
        this.score = DataStreamer.readVarInt(in);
        this.scoreboardAction = DataStreamer.readVarInt(in);
        if (version >= 7) {
            this.uuid = DataStreamer.readUUID(in);
        }
    }

    @Override
    public void rollback(Context context) {
        ScoreboardManager scoreboard = this.uuid == null ? context.getScoreboard() : context.getScoreboard(this.uuid);
        if (this.scoreboardAction == 0) {
            scoreboard.removeScore(this.scorename, this.objectivename);
        } else if (this.scorename != null && this.objectivename != null) {
            scoreboard.setScore(this.scorename, this.objectivename, this.oldScore);
        }
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    @Override
    public void execute(Context context) {
        ScoreboardManager scoreboard = this.uuid == null ? context.getScoreboard() : context.getScoreboard(this.uuid);
        int n = this.oldScore = this.objectivename != null ? scoreboard.getScore(this.scorename, this.objectivename) : scoreboard.getScore(this.scorename);
        if (this.scoreboardAction == 0) {
            scoreboard.setScore(this.scorename, this.objectivename, this.score);
        } else {
            scoreboard.removeScore(this.scorename, this.objectivename);
        }
    }

    public ScoreboardScoreMeta() {
    }

    @ConstructorProperties(value={"scorename", "objectivename", "score", "scoreboardAction", "uuid", "oldScore"})
    public ScoreboardScoreMeta(String scorename, String objectivename, int score, int scoreboardAction, UUID uuid, int oldScore) {
        this.scorename = scorename;
        this.objectivename = objectivename;
        this.score = score;
        this.scoreboardAction = scoreboardAction;
        this.uuid = uuid;
        this.oldScore = oldScore;
    }

    public String toString() {
        return "ScoreboardScoreMeta(scorename=" + this.scorename + ", objectivename=" + this.objectivename + ", score=" + this.score + ", scoreboardAction=" + this.scoreboardAction + ", uuid=" + this.uuid + ", oldScore=" + this.oldScore + ")";
    }

    public static class ScoreboardScoreMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;

        public ScoreboardScoreMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.SCOREBOARD_SCORE});
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }

        public void onPacketSending(PacketEvent e) {
            WrapperPlayServerScoreboardScore packet = new WrapperPlayServerScoreboardScore(e.getPacket());
            String scorename = packet.getScoreName();
            String objectivename = packet.getObjectiveName();
            int score = packet.getValue();
            int scoreboardAction = packet.getAction().equals((Object)EnumWrappers.ScoreboardAction.CHANGE) ? 0 : 1;
            ScoreboardScoreMeta meta = new ScoreboardScoreMeta(scorename, objectivename, score, scoreboardAction, e.getPlayer().getUniqueId(), 0);
            this.replayFileWriter.addMeta(meta);
        }

        public void onPacketReceiving(PacketEvent e) {
        }
    }
}

