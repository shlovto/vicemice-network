package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocol.wrappers.EnumWrappers;
import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerEntityEquipment;
import com.google.common.base.Charsets;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;

import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.data.VerboseDisplayType;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.meta.api.VoidMetaAdapter;
import net.vicemice.mctv.replay.Context;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

@MetaInfo(opcode=32, name="ConditionMessageMeta", adapter= VoidMetaAdapter.class)
public class ConditionalMessageMeta
implements Meta {
    private String message;
    private String condition;
    private UUID player;

    public ConditionalMessageMeta(String message, String condition) {
        this(message, condition, null);
    }

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeString(this.message, Charsets.UTF_8, out);
        DataStreamer.writeString(this.condition, Charsets.UTF_8, out);
        DataStreamer.writeUUID(this.player, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.message = DataStreamer.readString(Charsets.UTF_8, in);
        this.condition = DataStreamer.readString(Charsets.UTF_8, in);
        if (version >= 10) {
            this.player = DataStreamer.readUUID(in);
        }
    }

    @Override
    public void execute(final Context context) {
        if (this.message.contains("AUTO_ARMOR")) {
            String[] split = this.message.split(" ");
            String player = split[1].replace(":", "");
            String armorPiece = split[9];
            final EnumWrappers.ItemSlot slot = armorPiece.endsWith("LEGGINGS") ? EnumWrappers.ItemSlot.LEGS : (armorPiece.endsWith("BOOTS") ? EnumWrappers.ItemSlot.FEET : (armorPiece.endsWith("CHESTPLATE") ? EnumWrappers.ItemSlot.CHEST : EnumWrappers.ItemSlot.HEAD));
            for (final EntityPlayer entityPlayer : context.getPlayerManager().getPlayers()) {
                if (!player.equals(entityPlayer.getName())) continue;
                ItemStack current = context.getPlayerManager().getItemStack(entityPlayer.getUniqueID(), armorPiece.endsWith("LEGGINGS") ? 3 : (armorPiece.endsWith("BOOTS") ? 4 : (armorPiece.endsWith("CHESTPLATE") ? 2 : 1)));
                final ItemStack oldItemStack = current == null ? new ItemStack(Material.AIR) : current.clone();
                WrapperPlayServerEntityEquipment packet = new WrapperPlayServerEntityEquipment();
                packet.setEntityID(context.getPlayerManager().getEntityIdByUUID(entityPlayer.getUniqueID(), (World)entityPlayer.getWorld().getWorld()));
                packet.setItem(new ItemStack(Material.valueOf((String)armorPiece)));
                packet.setSlot(slot);
                for (Player packetPlayer : Bukkit.getOnlinePlayers()) {
                    if (!packetPlayer.getWorld().equals((Object)entityPlayer.getWorld().getWorld())) continue;
                    packet.sendPacket(packetPlayer);
                }
                Bukkit.getScheduler().runTaskLater((Plugin)MCTV.getInstance(), new Runnable(){

                    @Override
                    public void run() {
                        WrapperPlayServerEntityEquipment packet = new WrapperPlayServerEntityEquipment();
                        packet.setEntityID(context.getPlayerManager().getEntityIdByUUID(entityPlayer.getUniqueID(), (World)entityPlayer.getWorld().getWorld()));
                        packet.setItem(oldItemStack);
                        packet.setSlot(slot);
                        for (Player packetPlayer : Bukkit.getOnlinePlayers()) {
                            if (!packetPlayer.getWorld().equals((Object)entityPlayer.getWorld().getWorld())) continue;
                            packet.sendPacket(packetPlayer);
                        }
                    }
                }, 20L);
                break;
            }
        }
        String permission = this.condition.isEmpty() ? null : "mctv.condition." + this.condition;
        for (Player player : Bukkit.getOnlinePlayers()) {
            VerboseDisplayType future = context.getVerboseDisplayType(player.getUniqueId());
            if (permission != null && !player.hasPermission(permission)) continue;
            if (!future.getShow().test(player, this)) continue;
            player.sendMessage(this.message);
        }
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return true;
    }

    @ConstructorProperties(value={"message", "condition", "player"})
    public ConditionalMessageMeta(String message, String condition, UUID player) {
        this.message = message;
        this.condition = condition;
        this.player = player;
    }

    public ConditionalMessageMeta() {
    }

    public String getMessage() {
        return this.message;
    }

    public String getCondition() {
        return this.condition;
    }

    public UUID getPlayer() {
        return this.player;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public void setPlayer(UUID player) {
        this.player = player;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ConditionalMessageMeta)) {
            return false;
        }
        ConditionalMessageMeta other = (ConditionalMessageMeta)o;
        if (!other.canEqual(this)) {
            return false;
        }
        String this$message = this.getMessage();
        String other$message = other.getMessage();
        if (this$message == null ? other$message != null : !this$message.equals(other$message)) {
            return false;
        }
        String this$condition = this.getCondition();
        String other$condition = other.getCondition();
        if (this$condition == null ? other$condition != null : !this$condition.equals(other$condition)) {
            return false;
        }
        UUID this$player = this.getPlayer();
        UUID other$player = other.getPlayer();
        return !(this$player == null ? other$player != null : !((Object)this$player).equals(other$player));
    }

    protected boolean canEqual(Object other) {
        return other instanceof ConditionalMessageMeta;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        String $message = this.getMessage();
        result = result * 59 + ($message == null ? 43 : $message.hashCode());
        String $condition = this.getCondition();
        result = result * 59 + ($condition == null ? 43 : $condition.hashCode());
        UUID $player = this.getPlayer();
        result = result * 59 + ($player == null ? 43 : ((Object)$player).hashCode());
        return result;
    }
}

