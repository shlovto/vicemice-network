package net.vicemice.mctv.listener;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayClientEntityAction;
import net.vicemice.hector.api.protocolwrapper.WrapperPlayClientUseEntity;
import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerCamera;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.wrappers.EnumWrappers;
import net.vicemice.mctv.MCTV;

public class SpectateListener {
    private PacketAdapter packetAdapter = new SpectatePacketListener();

    public void start() {
        ProtocolLibrary.getProtocolManager().addPacketListener(this.packetAdapter);
    }

    public void stop() {
        ProtocolLibrary.getProtocolManager().removePacketListener(this.packetAdapter);
    }

    private static class SpectatePacketListener
    extends PacketAdapter {
        public SpectatePacketListener() {
            super(MCTV.getInstance(), PacketType.Play.Client.USE_ENTITY, PacketType.Play.Client.ENTITY_ACTION);
        }

        public void onPacketReceiving(PacketEvent event) {
            if (event.getPacketType() == PacketType.Play.Client.USE_ENTITY) {
                WrapperPlayClientUseEntity packetUseEntity = new WrapperPlayClientUseEntity(event.getPacket());
                if (packetUseEntity.getType() == EnumWrappers.EntityUseAction.ATTACK && MCTV.getInstance().getReplaySession().getContext().getPlayerManager().doesPlayerExist(packetUseEntity.getTarget())) {
                    WrapperPlayServerCamera packetCamera = new WrapperPlayServerCamera();
                    packetCamera.setCameraId(packetUseEntity.getTarget());
                    packetCamera.sendPacket(event.getPlayer());
                }
            } else {
                WrapperPlayClientEntityAction packetEntityAction = new WrapperPlayClientEntityAction(event.getPacket());
                if (packetEntityAction.getAction() == EnumWrappers.PlayerAction.START_SNEAKING) {
                    WrapperPlayServerCamera packetCamera = new WrapperPlayServerCamera();
                    packetCamera.setCameraId(event.getPlayer().getEntityId());
                    packetCamera.sendPacket(event.getPlayer());
                }
            }
        }
    }
}

