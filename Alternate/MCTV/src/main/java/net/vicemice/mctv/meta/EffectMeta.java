package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerWorldParticles;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import net.vicemice.hector.api.protocol.wrappers.EnumWrappers;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

@MetaInfo(name="EffectMeta", opcode=7, adapter= EffectMeta.EffectMetaAdapter.class)
public class EffectMeta
implements Meta {
    private String particleName;
    private Location location;
    private Vector offset;
    private int amount;
    private float speed;
    private int[] data = new int[0];
    private String world;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeString(this.particleName, out);
        out.writeDouble(this.location.getX());
        out.writeDouble(this.location.getY());
        out.writeDouble(this.location.getZ());
        out.writeDouble(this.offset.getX());
        out.writeDouble(this.offset.getY());
        out.writeDouble(this.offset.getZ());
        DataStreamer.writeVarInt(this.amount, out);
        out.writeFloat(this.speed);
        if (this.data != null) {
            out.writeByte(this.data.length);
            for (int i : this.data) {
                out.writeInt(i);
            }
        } else {
            out.writeByte(0);
        }
        DataStreamer.writeString(this.location.getWorld().getName(), out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        byte size;
        this.particleName = DataStreamer.readString(in);
        this.location = new Location(null, in.readDouble(), in.readDouble(), in.readDouble());
        this.offset = new Vector(in.readDouble(), in.readDouble(), in.readDouble());
        this.amount = DataStreamer.readVarInt(in);
        this.speed = in.readFloat();
        if (version > 0 && (size = in.readByte()) > 0) {
            this.data = new int[size];
            for (int i = 0; i < this.data.length; ++i) {
                this.data[i] = in.readInt();
            }
        }
        if (version >= 4) {
            this.world = DataStreamer.readString(in);
        }
    }

    @Override
    public void execute(Context context) {
        if (context.getVersion() == 0) {
            return;
        }
        WrapperPlayServerWorldParticles packet = new WrapperPlayServerWorldParticles();
        try {
            packet.setParticleType(EnumWrappers.Particle.getByName((String)this.particleName));
        }
        catch (Exception e) {
            packet.setParticleType(EnumWrappers.Particle.getByName((String)this.particleName.substring(0, this.particleName.length() - 1)));
        }
        packet.setX((float)this.location.getX());
        packet.setY((float)this.location.getY());
        packet.setZ((float)this.location.getZ());
        packet.setOffsetX((float)this.offset.getX());
        packet.setOffsetY((float)this.offset.getY());
        packet.setOffsetZ((float)this.offset.getZ());
        packet.setNumberOfParticles(this.amount);
        packet.setParticleData(this.speed);
        packet.setData(this.data);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (this.world != null && !this.world.equals(player.getWorld().getName())) continue;
            packet.sendPacket(player);
        }
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return true;
    }

    public EffectMeta() {
    }

    @ConstructorProperties(value={"particleName", "location", "offset", "amount", "speed", "data", "world"})
    public EffectMeta(String particleName, Location location, Vector offset, int amount, float speed, int[] data, String world) {
        this.particleName = particleName;
        this.location = location;
        this.offset = offset;
        this.amount = amount;
        this.speed = speed;
        this.data = data;
        this.world = world;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof EffectMeta)) {
            return false;
        }
        EffectMeta other = (EffectMeta)o;
        if (!other.canEqual(this)) {
            return false;
        }
        String this$particleName = this.particleName;
        String other$particleName = other.particleName;
        if (this$particleName == null ? other$particleName != null : !this$particleName.equals(other$particleName)) {
            return false;
        }
        Location this$location = this.location;
        Location other$location = other.location;
        if (this$location == null ? other$location != null : !this$location.equals((Object)other$location)) {
            return false;
        }
        Vector this$offset = this.offset;
        Vector other$offset = other.offset;
        if (this$offset == null ? other$offset != null : !this$offset.equals((Object)other$offset)) {
            return false;
        }
        if (this.amount != other.amount) {
            return false;
        }
        if (Float.compare(this.speed, other.speed) != 0) {
            return false;
        }
        if (!Arrays.equals(this.data, other.data)) {
            return false;
        }
        String this$world = this.world;
        String other$world = other.world;
        return !(this$world == null ? other$world != null : !this$world.equals(other$world));
    }

    protected boolean canEqual(Object other) {
        return other instanceof EffectMeta;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        String $particleName = this.particleName;
        result = result * 59 + ($particleName == null ? 43 : $particleName.hashCode());
        Location $location = this.location;
        result = result * 59 + ($location == null ? 43 : $location.hashCode());
        Vector $offset = this.offset;
        result = result * 59 + ($offset == null ? 43 : $offset.hashCode());
        result = result * 59 + this.amount;
        result = result * 59 + Float.floatToIntBits(this.speed);
        result = result * 59 + Arrays.hashCode(this.data);
        String $world = this.world;
        result = result * 59 + ($world == null ? 43 : $world.hashCode());
        return result;
    }

    public static class EffectMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;
        private EffectMeta lastParticle;
        private long lastEffectSent;

        public EffectMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.WORLD_PARTICLES});
            this.replayFileWriter = replayFileWriter;
            this.lastEffectSent = System.currentTimeMillis();
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }

        public void onPacketSending(PacketEvent event) {
            WrapperPlayServerWorldParticles packet = new WrapperPlayServerWorldParticles(event.getPacket());
            String particleName = packet.getParticleType().getName();
            Location location = new Location(event.getPlayer().getWorld(), (double)packet.getX(), (double)packet.getY(), (double)packet.getZ());
            Vector offset = new Vector(packet.getOffsetX(), packet.getOffsetY(), packet.getOffsetZ());
            int amount = packet.getNumberOfParticles();
            float speed = packet.getParticleData();
            int[] data = packet.getData();
            EffectMeta meta = new EffectMeta(particleName, location, offset, amount, speed, data, null);
            if (this.lastParticle != null && this.lastParticle.equals(meta)) {
                if (System.currentTimeMillis() - this.lastEffectSent > 10L) {
                    this.replayFileWriter.addMeta(meta);
                    this.lastParticle = meta;
                    this.lastEffectSent = System.currentTimeMillis();
                }
            } else {
                this.replayFileWriter.addMeta(meta);
                this.lastParticle = meta;
                this.lastEffectSent = System.currentTimeMillis();
            }
        }

        public void onPacketReceiving(PacketEvent event) {
        }
    }
}

