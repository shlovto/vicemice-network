package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.vicemice.hector.utils.players.api.actionbat.ActionBarAPI;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="StatusMetaAdapter", opcode=23, adapter= StatusMeta.StatusMetaAdapter.class)
public class StatusMeta
implements Meta {
    private int teamsAllowed;
    private int mapDestruction;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeVarInt(this.teamsAllowed, out);
        DataStreamer.writeVarInt(this.mapDestruction, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.teamsAllowed = DataStreamer.readVarInt(in);
        this.mapDestruction = DataStreamer.readVarInt(in);
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    @Override
    public void execute(Context context) {
        if (this.teamsAllowed != 2) {
            this.sendActionbarForAll(this.teamsAllowed == 0 ? "\u00a7cTeams verboten" : "\u00a7aTeams erlaubt");
        }
        context.setMapDestruction(this.mapDestruction);
        if (context.getActionBarTask() != null && Bukkit.getScheduler().isCurrentlyRunning(context.getActionBarTask().getTaskId())) {
            context.getActionBarTask().cancel();
        }
    }

    private void sendActionbarForAll(final String msg) {
        Bukkit.getScheduler().runTaskTimer((Plugin)MCTV.getInstance(), new Runnable(){
            @Override
            public void run() {
                ActionBarAPI.sendActionBar(msg);
            }
        }, 0L, 35L);
    }

    public StatusMeta() {
    }

    @ConstructorProperties(value={"teamsAllowed", "mapDestruction"})
    public StatusMeta(int teamsAllowed, int mapDestruction) {
        this.teamsAllowed = teamsAllowed;
        this.mapDestruction = mapDestruction;
    }

    public static class StatusMetaAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;

        public StatusMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            StatusMeta meta = new StatusMeta(MCTV.getInstance().getApi().getContext().getTeams(), MCTV.getInstance().getApi().getContext().getMapDestruction());
            this.replayFileWriter.addMeta(meta);
        }

        @Override
        public void stop() {
        }
    }
}

