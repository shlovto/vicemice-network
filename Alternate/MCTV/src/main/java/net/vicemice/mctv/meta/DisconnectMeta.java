package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="DisconnectMeta", opcode=10, adapter= DisconnectMeta.DisconnectMetaAdapter.class)
public class DisconnectMeta
implements Meta {
    private UUID uuid;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeUUID(this.uuid, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.uuid = DataStreamer.readUUID(in);
    }

    @Override
    public void execute(Context context) {
        context.getPlayerManager().despawnFakePlayer(this.uuid);
    }

    @Override
    public void rollback(Context context) {
        context.getPlayerManager().spawnFakePlayer(this.uuid);
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public DisconnectMeta() {
    }

    @ConstructorProperties(value={"uuid"})
    public DisconnectMeta(UUID uuid) {
        this.uuid = uuid;
    }

    public static class DisconnectMetaAdapter
    implements MetaAdapter,
    Listener {
        private ReplayFileWriter replayFileWriter;

        public DisconnectMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            Bukkit.getPluginManager().registerEvents((Listener)this, (Plugin)MCTV.getInstance());
        }

        @Override
        public void stop() {
        }

        @EventHandler
        public void onQuit(PlayerQuitEvent event) {
            DisconnectMeta disconnectMeta = new DisconnectMeta(event.getPlayer().getUniqueId());
            this.replayFileWriter.addMeta(disconnectMeta);
        }
    }
}

