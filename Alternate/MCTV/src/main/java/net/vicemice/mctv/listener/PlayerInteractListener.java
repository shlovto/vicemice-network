package net.vicemice.mctv.listener;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.players.api.title.Title;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.inventory.Hotbar;
import net.vicemice.mctv.inventory.Teleporter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteractListener
implements Listener {
    private long lastMasterInteraction = System.currentTimeMillis();
    private long lastShutdownInteraction = 0L;
    private boolean reverse = false;

    public PlayerInteractListener() {
        Bukkit.getScheduler().runTaskTimer( MCTV.getInstance(), () -> PlayerInteractListener.this.reverse = !PlayerInteractListener.this.reverse, 5L, 5L);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        event.setCancelled(true);
        if (event.getAction() == Action.PHYSICAL) {
            return;
        }
        if (event.hasItem() && event.getItem().getType() == Material.COMPASS) {
            Teleporter.open(event.getPlayer());
            return;
        }
        Player player = event.getPlayer();
        if (MCTV.getInstance().getReplaySession().isMaster(event.getPlayer()) && event.hasItem()) {
            if (System.currentTimeMillis() - this.lastMasterInteraction < 50L) {
                return;
            }
            this.lastMasterInteraction = System.currentTimeMillis();
            if (event.getItem().getType() == Material.SKULL_ITEM) {
                switch (event.getPlayer().getInventory().getHeldItemSlot()) {
                    case 3: {
                        MCTV.getInstance().getReplaySession().skip(-2);
                        Title title = new Title("", this.reverse ? "\u00a7c\u00a7l<\u00a7f\u00a7l<\u00a7c\u00a7l<\u00a7f\u00a7l<\u00a7c\u00a7l<\u00a7f\u00a7l<" : "\u00a7f\u00a7l<\u00a7c\u00a7l<\u00a7f\u00a7l<\u00a7c\u00a7l<\u00a7f\u00a7l<\u00a7c\u00a7l<");
                        title.setFadeInTime(0);
                        title.setFadeOutTime(0);
                        title.setStayTime(1);
                        title.send(player);
                        break;
                    }
                    case 4: {
                        if (MCTV.getInstance().getReplaySession().isPaused()) {
                            MCTV.getInstance().getReplaySession().play();
                            MCTV.getInstance().updateActionBar();
                            event.getPlayer().getInventory().setItem(4, new ItemBuilder(Hotbar.getPause()).name(GoAPI.getUserAPI().translate(event.getPlayer().getUniqueId(), "item-pause", new Object[0])).build());
                            break;
                        }
                        MCTV.getInstance().getReplaySession().pause();
                        MCTV.getInstance().updateActionBar();
                        event.getPlayer().getInventory().setItem(4, new ItemBuilder(Hotbar.getPlay()).name(GoAPI.getUserAPI().translate(event.getPlayer().getUniqueId(), "item-resume", new Object[0])).build());
                        break;
                    }
                    case 5: {
                        MCTV.getInstance().getReplaySession().skip(2);
                        MCTV.getInstance().updateActionBar();
                        Title title = new Title("", this.reverse ? "\u00a7a\u00a7l>\u00a7f\u00a7l>\u00a7a\u00a7l>\u00a7f\u00a7l>\u00a7a\u00a7l>\u00a7f\u00a7l>" : "\u00a7f\u00a7l>\u00a7a\u00a7l>\u00a7f\u00a7l>\u00a7a\u00a7l>\u00a7f\u00a7l>\u00a7a\u00a7l>");
                        title.setFadeInTime(0);
                        title.setFadeOutTime(0);
                        title.setStayTime(1);
                        title.send(player);
                        break;
                    }
                    case 7: {
                        MCTV.getInstance().getReplaySession().toggleSlowmode();
                        MCTV.getInstance().updateActionBar();
                        break;
                    }
                    case 8: {
                        long delay = System.currentTimeMillis() - this.lastShutdownInteraction;
                        if (this.lastShutdownInteraction > 0L && delay < 10000L) {
                            for (Player target : Bukkit.getOnlinePlayers()) {
                                target.kickPlayer("lobby");
                            }
                            Bukkit.getScheduler().runTaskLater(MCTV.getInstance(), Bukkit::shutdown, 100L);
                            break;
                        }
                        this.lastShutdownInteraction = System.currentTimeMillis();
                        MCTV.getInstance().sendMessage(event.getPlayer(), "stop-confirm", new Object[0]);
                    }
                }
            }
        }
    }
}