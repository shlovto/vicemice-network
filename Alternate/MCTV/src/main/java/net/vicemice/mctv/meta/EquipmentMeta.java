package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocol.wrappers.EnumWrappers;
import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerEntityEquipment;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.*;

import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="EquipmentMeta", opcode=2, adapter= EquipmentMeta.EquipmentMetaAdapter.class)
public class EquipmentMeta implements Meta {
    private UUID uuid;
    private ItemStack item;
    private int slot;
    private String world;
    private ItemStack oldItemStack;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeUUID(this.uuid, out);
        DataStreamer.writeItemStack(this.item, out);
        out.writeShort(this.slot);
        DataStreamer.writeString(this.world, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.uuid = DataStreamer.readUUID(in);
        this.item = DataStreamer.readItemStack(in);
        this.slot = in.readShort();
        if (version >= 3) {
            this.world = DataStreamer.readString(in);
        }
    }

    @Override
    public void execute(Context context) {
        ItemStack current = context.getPlayerManager().getItemStack(this.uuid, this.slot);
        this.oldItemStack = current == null ? new ItemStack(Material.AIR) : current.clone();
        context.getPlayerManager().setItem(this.uuid, this.slot, this.item);
        World world = context.getMasterPlayer().getWorld();
        if (this.world != null) {
            world = Bukkit.getWorld((String)this.world);
        }
        WrapperPlayServerEntityEquipment packet = new WrapperPlayServerEntityEquipment();
        packet.setEntityID(context.getPlayerManager().getEntityIdByUUID(this.uuid, world));
        packet.setItem(this.item.clone());
        packet.setSlot(this.slot);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!player.getWorld().equals((Object)world)) continue;
            packet.sendPacket(player);
        }
    }

    @Override
    public void rollback(Context context) {
        context.getPlayerManager().setItem(this.uuid, this.slot, this.oldItemStack);
        World world = context.getMasterPlayer().getWorld();
        if (this.world != null) {
            world = Bukkit.getWorld((String)this.world);
        }
        if (world == null) {
            System.out.println("Replay wanted to get entity in world '" + this.world + "' which is not loaded. Defaulting back to replay master world");
            world = context.getMasterPlayer().getWorld();
        }
        WrapperPlayServerEntityEquipment packet = new WrapperPlayServerEntityEquipment();
        packet.setEntityID(context.getPlayerManager().getEntityIdByUUID(this.uuid, world));
        packet.setItem(this.oldItemStack.clone());
        packet.setSlot(this.slot);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!player.getWorld().equals((Object)world)) continue;
            packet.sendPacket(player);
        }
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public EquipmentMeta() {
    }

    @ConstructorProperties(value={"uuid", "item", "slot", "world", "oldItemStack"})
    public EquipmentMeta(UUID uuid, ItemStack item, int slot, String world, ItemStack oldItemStack) {
        this.uuid = uuid;
        this.item = item;
        this.slot = slot;
        this.world = world;
        this.oldItemStack = oldItemStack;
    }

    public String toString() {
        return "EquipmentMeta(uuid=" + this.uuid + ", slot=" + this.slot + ", world=" + this.world + ", oldItemStack=" + (Object)this.oldItemStack + ")";
    }

    /*public static class EquipmentMetaAdapter extends PacketAdapter implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;
        private Map<UUID, Map<Integer, ItemStack>> lastItems = new HashMap<UUID, Map<Integer, ItemStack>>();

        public EquipmentMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), PacketType.Play.Server.ENTITY_EQUIPMENT);
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener(this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener(this);
        }

        public void onPacketSending(PacketEvent event) {
            ItemStack lastItem;
            WrapperPlayServerEntityEquipment packet = new WrapperPlayServerEntityEquipment(event.getPacket());
            if (!(packet.getEntity(event) instanceof Player)) {
                return;
            }
            UUID uuid = packet.getEntity(event).getUniqueId();
            ItemStack item = packet.getItem() == null ? new ItemStack(Material.AIR) : packet.getItem();
            int slot = packet.getSlotAsInt();
            Map<Integer, ItemStack> items = this.lastItems.get(uuid);
            if (items == null) {
                items = new HashMap<>();
                this.lastItems.put(uuid, items);
            }
            if ((lastItem = items.get(slot)) == null || !lastItem.equals((Object)item)) {
                EquipmentMeta meta = new EquipmentMeta(uuid, item, slot, Bukkit.getPlayer((UUID)uuid).getWorld().getName(), null);
                this.replayFileWriter.addMeta(meta);
                items.put(slot, item);
            }
        }
    }*/

    public static class EquipmentMetaAdapter implements Listener, MetaAdapter {
        private ReplayFileWriter replayFileWriter;
        private Map<UUID, Map<Integer, ItemStack>> lastItems = new HashMap<UUID, Map<Integer, ItemStack>>();
        private final static List<Material> ARMOR = Arrays.asList(new Material[] {
                Material.DIAMOND_HELMET, Material.DIAMOND_CHESTPLATE, Material.DIAMOND_LEGGINGS, Material.DIAMOND_BOOTS,
                Material.IRON_HELMET, Material.IRON_CHESTPLATE, Material.IRON_LEGGINGS, Material.IRON_BOOTS,
                Material.CHAINMAIL_HELMET, Material.CHAINMAIL_CHESTPLATE, Material.CHAINMAIL_LEGGINGS, Material.CHAINMAIL_BOOTS,
                Material.GOLD_HELMET, Material.GOLD_CHESTPLATE, Material.GOLD_LEGGINGS, Material.GOLD_BOOTS,
                Material.LEATHER_HELMET, Material.LEATHER_CHESTPLATE, Material.LEATHER_LEGGINGS, Material.LEATHER_BOOTS
        });

        public EquipmentMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            Bukkit.getPluginManager().registerEvents(this, MCTV.getInstance());
        }

        @Override
        public void stop() {}

        @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
        public void onHeld(PlayerItemHeldEvent event) {
            Player player = event.getPlayer();
            setItem(player, 0, player.getItemInHand());
        }

        @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
        public void onClick(InventoryClickEvent event) {
            if (event.getWhoClicked() instanceof Player) {
                Player player = (Player) event.getWhoClicked();
                setItem(player, 0, player.getInventory().getItem(player.getInventory().getHeldItemSlot()));
                setItem(player, 4, player.getInventory().getItem(36));
                setItem(player, 3, player.getInventory().getItem(37));
                setItem(player, 2, player.getInventory().getItem(38));
                setItem(player, 1, player.getInventory().getItem(39));
            }
        }

        @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
        public void onInteract(PlayerInteractEvent event) {
            Player player = event.getPlayer();
            if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                if (isArmor(event.getItem()) && !wearsArmor(player, getArmorType(event.getItem()))) {
                    String armorType = getArmorType(event.getItem());
                    if (armorType != null) {
                        if (armorType.equals("head")) setItem(player, 1, event.getItem());
                        if (armorType.equals("chest")) setItem(player, 2, event.getItem());
                        if (armorType.equals("leg")) setItem(player, 3, event.getItem());
                        if (armorType.equals("boots")) setItem(player, 4, event.getItem());
                    }
                    setItem(player, 0, null);
                }
            }
        }

        public static boolean isArmor(ItemStack stack) {
            if (stack == null) return false;
            return ARMOR.contains(stack.getType());
        }

        public static String getArmorType(ItemStack stack) {
            if (stack == null) return null;

            if (stack.getType().toString().contains("HELMET")) return "head";
            if (stack.getType().toString().contains("CHESTPLATE")) return "chest";
            if (stack.getType().toString().contains("LEGGINGS")) return "leg";
            if (stack.getType().toString().contains("BOOTS")) return "boots";

            return null;

        }

        public static boolean wearsArmor(Player p, String type) {
            PlayerInventory inv = p.getInventory();
            if (type == null) return false;

            if (type.equals("head") && isArmor(inv.getHelmet())) return true;
            if (type.equals("chest") && isArmor(inv.getChestplate())) return true;
            if (type.equals("leg") && isArmor(inv.getLeggings())) return true;
            if (type.equals("boots") && isArmor(inv.getBoots())) return true;

            return false;
        }

        public void setItem(Player player, int slot, ItemStack itemStack) {
            ItemStack lastItem;
            ItemStack item = itemStack == null ? new ItemStack(Material.AIR) : itemStack;
            Map<Integer, ItemStack> items = this.lastItems.get(player.getUniqueId());
            if (items == null) {
                items = new HashMap<>();
                this.lastItems.put(player.getUniqueId(), items);
            }
            if ((lastItem = items.get(slot)) == null || !lastItem.equals(item)) {
                EquipmentMeta meta = new EquipmentMeta(player.getUniqueId(), item, slot, Bukkit.getPlayer(player.getUniqueId()).getWorld().getName(), null);
                this.replayFileWriter.addMeta(meta);
                items.put(slot, item);
            }
        }
    }
}

