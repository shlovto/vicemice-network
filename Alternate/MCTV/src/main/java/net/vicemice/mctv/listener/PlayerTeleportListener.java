package net.vicemice.mctv.listener;

import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.data.UserData;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;

public class PlayerTeleportListener
implements Listener {
    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        UserData userData = UserData.getUserData(event.getPlayer(), MCTV.getInstance().getApi().getContext());
        if (userData != null) {
            userData.clearPlayers();
        }
    }
}

