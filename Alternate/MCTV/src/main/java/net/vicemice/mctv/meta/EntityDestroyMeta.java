package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerEntityDestroy;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="EntityDestroyMeta", opcode=14, adapter= EntityDestroyMeta.EntityDestroyMetaAdapter.class)
public class EntityDestroyMeta implements Meta {
    private int[] entityIDs;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeVarInt(this.entityIDs.length, out);
        for (int entityID : this.entityIDs) {
            DataStreamer.writeVarInt(entityID, out);
        }
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        int size = DataStreamer.readVarInt(in);
        this.entityIDs = new int[size];
        for (int i = 0; i < this.entityIDs.length; ++i) {
            this.entityIDs[i] = DataStreamer.readVarInt(in);
        }
    }

    @Override
    public void execute(Context context) {
        for (int entityID : this.entityIDs) {
            context.getEntityManager().despawn(entityID);
        }
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public EntityDestroyMeta() {
    }

    @ConstructorProperties(value={"entityIDs"})
    public EntityDestroyMeta(int[] entityIDs) {
        this.entityIDs = entityIDs;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof EntityDestroyMeta)) {
            return false;
        }
        EntityDestroyMeta other = (EntityDestroyMeta)o;
        if (!other.canEqual(this)) {
            return false;
        }
        return Arrays.equals(this.entityIDs, other.entityIDs);
    }

    protected boolean canEqual(Object other) {
        return other instanceof EntityDestroyMeta;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        result = result * 59 + Arrays.hashCode(this.entityIDs);
        return result;
    }

    public static class EntityDestroyMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;
        private EntityDestroyMeta lastEntityDestroy;

        public EntityDestroyMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.ENTITY_DESTROY});
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }

        public void onPacketSending(PacketEvent event) {
            WrapperPlayServerEntityDestroy packet = new WrapperPlayServerEntityDestroy(event.getPacket());
            ArrayList<Integer> eIDs = new ArrayList<Integer>();
            for (int entityID : packet.getEntityIDs()) {
                boolean found = false;
                for (EntityPlayer player : MinecraftServer.getServer().getPlayerList().players) {
                    if (entityID != player.getId()) continue;
                    found = true;
                    break;
                }
                if (found) continue;
                eIDs.add(entityID);
            }
            if (eIDs.size() == 0) {
                return;
            }
            int[] finalEIDArray = new int[eIDs.size()];
            int index = 0;
            for (Integer eID : eIDs) {
                finalEIDArray[index] = eID;
                ++index;
            }
            EntityDestroyMeta entityDestroyMeta = new EntityDestroyMeta(finalEIDArray);
            if (this.lastEntityDestroy != null && entityDestroyMeta.equals(this.lastEntityDestroy)) {
                return;
            }
            this.lastEntityDestroy = entityDestroyMeta;
            this.replayFileWriter.addMeta(this.lastEntityDestroy);
        }

        public void onPacketReceiving(PacketEvent event) {
        }
    }
}

