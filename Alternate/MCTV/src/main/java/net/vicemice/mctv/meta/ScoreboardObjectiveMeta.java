package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerScoreboardObjective;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.replay.ScoreboardManager;
import org.bukkit.plugin.Plugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;

@MetaInfo(name="ScoreboardObjectiveMeta", opcode=19, adapter= ScoreboardObjectiveMeta.ScoreboardObjectiveMetaAdapter.class)
public class ScoreboardObjectiveMeta implements Meta {
    private String name;
    private String displayname;
    private String healthDisplay;
    private int mode;
    private UUID uuid;
    private DisplaySlot oldDisplaySlot;
    private String oldDisplayname;
    private List<Score> oldScoreList;
    private String oldName;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeString(this.name, out);
        DataStreamer.writeString(this.displayname, out);
        DataStreamer.writeString(this.healthDisplay, out);
        DataStreamer.writeVarInt(this.mode, out);
        DataStreamer.writeUUID(this.uuid, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.name = DataStreamer.readString(in);
        this.displayname = DataStreamer.readString(in);
        this.healthDisplay = DataStreamer.readString(in);
        this.mode = DataStreamer.readVarInt(in);
        if (version >= 7) {
            this.uuid = DataStreamer.readUUID(in);
        }
    }

    @Override
    public void execute(Context context) {
        ScoreboardManager scoreboard = this.uuid == null ? context.getScoreboard() : context.getScoreboard(this.uuid);
        if (this.mode == 0 || this.mode == 2) {
            scoreboard.setObjective(this.name, this.displayname, this.healthDisplay);
        } else {
            Objective objective = scoreboard.getObjective(this.name);
            if (objective != null) {
                this.oldDisplaySlot = objective.getDisplaySlot();
                this.oldDisplayname = objective.getDisplayName();
                this.oldScoreList = scoreboard.getScores(this.name);
                this.oldName = objective.getName();
            }
            scoreboard.removeObjective(this.name);
        }
    }

    @Override
    public void rollback(Context context) {
        ScoreboardManager scoreboard = this.uuid == null ? context.getScoreboard() : context.getScoreboard(this.uuid);
        if (this.mode == 0 || this.mode == 2) {
            scoreboard.removeObjective(this.name);
        } else {
            scoreboard.setObjective(this.oldName, this.oldDisplayname, "integer");
            Objective objective = scoreboard.getObjective(this.oldName);
            objective.setDisplaySlot(this.oldDisplaySlot);
            for (Score score : this.oldScoreList) {
                scoreboard.setScore(score.getEntry(), this.oldName, score.getScore());
            }
        }
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public ScoreboardObjectiveMeta() {
    }

    @ConstructorProperties(value={"name", "displayname", "healthDisplay", "mode", "uuid", "oldDisplaySlot", "oldDisplayname", "oldScoreList", "oldName"})
    public ScoreboardObjectiveMeta(String name, String displayname, String healthDisplay, int mode, UUID uuid, DisplaySlot oldDisplaySlot, String oldDisplayname, List<Score> oldScoreList, String oldName) {
        this.name = name;
        this.displayname = displayname;
        this.healthDisplay = healthDisplay;
        this.mode = mode;
        this.uuid = uuid;
        this.oldDisplaySlot = oldDisplaySlot;
        this.oldDisplayname = oldDisplayname;
        this.oldScoreList = oldScoreList;
        this.oldName = oldName;
    }

    public String toString() {
        return "ScoreboardObjectiveMeta(name=" + this.name + ", displayname=" + this.displayname + ", healthDisplay=" + this.healthDisplay + ", mode=" + this.mode + ", uuid=" + this.uuid + ", oldDisplaySlot=" + (Object)this.oldDisplaySlot + ", oldDisplayname=" + this.oldDisplayname + ", oldScoreList=" + this.oldScoreList + ", oldName=" + this.oldName + ")";
    }

    public static class ScoreboardObjectiveMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;

        public ScoreboardObjectiveMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.SCOREBOARD_OBJECTIVE});
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }

        public void onPacketSending(PacketEvent e) {
            WrapperPlayServerScoreboardObjective packet = new WrapperPlayServerScoreboardObjective(e.getPacket());
            String name = packet.getName();
            String displayName = packet.getDisplayName();
            String healthDisplay = "integer";

            ScoreboardObjectiveMeta meta = new ScoreboardObjectiveMeta(name, displayName, healthDisplay, packet.getMode(), e.getPlayer().getUniqueId(), null, null, null, null);
            this.replayFileWriter.addMeta(meta);
        }

        public void onPacketReceiving(PacketEvent e) {
        }
    }
}

