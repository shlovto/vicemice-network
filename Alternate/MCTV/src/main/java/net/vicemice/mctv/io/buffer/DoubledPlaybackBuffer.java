package net.vicemice.mctv.io.buffer;

import java.util.concurrent.TimeUnit;
import net.vicemice.mctv.replay.Frame;

public class DoubledPlaybackBuffer
extends PlaybackBuffer {
    private final int forwardFrames;

    public DoubledPlaybackBuffer(int backwardsDuration, TimeUnit backwardsUnit, int forwardDuration, TimeUnit forwardUnit) {
        super((int)(backwardsUnit.toSeconds(backwardsDuration) + forwardUnit.toSeconds(forwardDuration)), TimeUnit.SECONDS);
        this.forwardFrames = (int)forwardUnit.toSeconds(forwardDuration) * 20;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void putBack(Frame frame) {
        Object object = this.monitor;
        synchronized (object) {
            if (this.forwardFrames < super.size()) {
                throw new IllegalStateException("Cannot putBack into full playback buffer!");
            }
            super.putBack(frame);
        }
    }

    @Override
    public boolean isFull() {
        return this.forwardFrames < super.size();
    }
}

