package net.vicemice.mctv.command;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import net.vicemice.mctv.MCTV;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.entity.Player;

public class SpecCommand extends Command {

    public SpecCommand(MCTV mctv) {
        super("spec");
        this.mctv = mctv;
        ((CraftServer) mctv.getServer()).getCommandMap().register("spec", this);
    }

    private MCTV mctv;

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            return true;
        }
        Player player = (Player)commandSender;
        if (!player.hasPermission("mctv.command.spec")) {
            this.mctv.sendMessage(player, "command-no-permission", new Object[0]);
            return true;
        }
        if (args.length < 1) {
            this.mctv.sendMessage(player, "command-spec-no-args", new Object[0]);
            return true;
        }
        for (EntityPlayer entityPlayer : MCTV.getInstance().getApi().getContext().getPlayerManager().getPlayers()) {
            if (!args[0].equalsIgnoreCase(entityPlayer.getName()) && !args[0].equalsIgnoreCase(entityPlayer.getUniqueID().toString()) && !args[0].equalsIgnoreCase(entityPlayer.getUniqueID().toString().replace("-", ""))) continue;
            player.teleport(entityPlayer.getBukkitEntity().getLocation());
            this.mctv.sendMessage(player, "command-spec-success", entityPlayer.getName());
            return true;
        }
        this.mctv.sendMessage(player, "command-spec-wrong-args", new Object[0]);
        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] strings) throws IllegalArgumentException {
        if (strings.length == 0) {
            return MCTV.getInstance().getReplaySession().getContext().getPlayerManager().getPlayers().stream().map(new Function<EntityPlayer, String>(){

                @Override
                public String apply(EntityPlayer entityPlayer) {
                    return entityPlayer.getName();
                }
            }).collect(Collectors.toList());
        }
        ArrayList<String> results = new ArrayList<String>();
        for (EntityPlayer entityPlayer : MCTV.getInstance().getReplaySession().getContext().getPlayerManager().getPlayers()) {
            if (results.contains(entityPlayer.getName()) || !entityPlayer.getName().toLowerCase().toLowerCase().startsWith(strings[0])) continue;
            results.add(entityPlayer.getName());
        }
        return results;
    }
}

