package net.vicemice.mctv.data;

import java.beans.ConstructorProperties;
import java.util.Objects;
import java.util.function.BiPredicate;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.meta.ConditionalMessageMeta;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;

public enum VerboseDisplayType {
    ALL((a, b) -> true),
    TARGET(new BiPredicate<Player, ConditionalMessageMeta>(){

        @Override
        public boolean test(Player player, ConditionalMessageMeta meta) {
            if (player.hasMetadata("conditionalfilter")) {
                String filter = ((MetadataValue)player.getMetadata("conditionalfilter").get(0)).asString();
                return meta.getMessage().contains(filter) || Objects.equals(meta.getPlayer(), MCTV.getInstance().getApi().getContext().getTeleportToAfterSkipTime());
            }
            return true;
        }
    }),
    NONE((a, b) -> false);

    private final BiPredicate<Player, ConditionalMessageMeta> show;

    @ConstructorProperties(value={"show"})
    private VerboseDisplayType(BiPredicate<Player, ConditionalMessageMeta> show) {
        this.show = show;
    }

    public BiPredicate<Player, ConditionalMessageMeta> getShow() {
        return this.show;
    }
}

