/*
 * Decompiled with CFR 0.149.
 * 
 * Could not load the following classes:
 *  net.minecraft.server.v1_8_R3.Scoreboard
 *  net.minecraft.server.v1_8_R3.ScoreboardObjective
 *  net.minecraft.server.v1_8_R3.ScoreboardScore
 *  org.bukkit.Bukkit
 *  org.bukkit.OfflinePlayer
 *  org.bukkit.craftbukkit.v1_8_R3.scoreboard.CraftScoreboard
 *  org.bukkit.entity.Player
 *  org.bukkit.scoreboard.DisplaySlot
 *  org.bukkit.scoreboard.Objective
 *  org.bukkit.scoreboard.Score
 *  org.bukkit.scoreboard.Scoreboard
 *  org.bukkit.scoreboard.Team
 */
package net.vicemice.mctv.replay;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import net.minecraft.server.v1_8_R3.ScoreboardObjective;
import net.minecraft.server.v1_8_R3.ScoreboardScore;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.v1_8_R3.scoreboard.CraftScoreboard;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class ScoreboardManager {
    private Scoreboard scoreboard = null;
    private UUID targetPlayer;

    public void addPlayer(Player player) {
        if (this.scoreboard == null) {
            this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        }
        player.setScoreboard(this.scoreboard);
        Team specTeam = this.getTeam("replay_specs");
        specTeam.setPrefix("\u00a77Replay | \u00a77");
        specTeam.addEntry(player.getName());
        specTeam.setAllowFriendlyFire(false);
        specTeam.setCanSeeFriendlyInvisibles(true);
    }

    public void removePlayer(Player player) {
        if (this.scoreboard == null) {
            this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        }
        player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
    }

    public Team getTeam(String name) {
        Team t;
        if (this.scoreboard == null) {
            this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        }
        if ((t = this.scoreboard.getTeam(name)) == null) {
            t = this.scoreboard.registerNewTeam(name);
        }
        return t;
    }

    public void removeTeam(String name) {
        if (this.scoreboard == null) {
            this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        }
        this.scoreboard.getTeam(name).unregister();
    }

    public void setObjective(String name, String displayname, String healthDisplay) {
        Objective objective;
        if (this.scoreboard == null) {
            this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        }
        if ((objective = this.scoreboard.getObjective(name)) == null) {
            objective = this.scoreboard.registerNewObjective(name, "dummy");
        }
        objective.setDisplayName(displayname);
    }

    public void removeObjective(String name) {
        Objective objective = this.scoreboard.getObjective(name);
        if (objective != null) {
            objective.unregister();
        }
    }

    public void setScore(String scorename, String objectivename, int score) {
        Objective objective;
        if (this.scoreboard == null) {
            this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        }
        if ((objective = this.scoreboard.getObjective(objectivename)) != null) {
            objective.getScore(scorename).setScore(score);
        }
    }

    public void removeScore(String scorename, String objectivename) {
        this.scoreboard.resetScores(scorename);
    }

    public void setDisplaySlotForObjective(String scorename, int position) {
        Objective objective;
        if (this.scoreboard == null) {
            this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        }
        if ((objective = this.scoreboard.getObjective(scorename)) != null) {
            DisplaySlot displaySlot = position == 0 ? DisplaySlot.PLAYER_LIST : (position == 1 ? DisplaySlot.SIDEBAR : DisplaySlot.BELOW_NAME);
            objective.setDisplaySlot(displaySlot);
        }
    }

    public Team getTeamForEntry(String name) {
        return this.scoreboard.getEntryTeam(name);
    }

    public int getScore(String scorename, String objectivename) {
        Objective objective = this.scoreboard.getObjective(objectivename);
        if (objective != null) {
            return objective.getScore(scorename).getScore();
        }
        return 0;
    }

    public Objective getObjective(String name) {
        return this.scoreboard.getObjective(name);
    }

    public List<Score> getScores(final String name) {
        net.minecraft.server.v1_8_R3.Scoreboard nmsScoreboard = ((CraftScoreboard)this.scoreboard).getHandle();
        ScoreboardObjective objective = nmsScoreboard.getObjective(name);
        if (objective != null) {
            ArrayList<Score> scores = new ArrayList<Score>();
            for (final ScoreboardScore scoreboardScore : nmsScoreboard.getScoresForObjective(objective)) {
                scores.add(new Score(){

                    public OfflinePlayer getPlayer() {
                        return Bukkit.getOfflinePlayer((String)scoreboardScore.getPlayerName());
                    }

                    public String getEntry() {
                        return scoreboardScore.getPlayerName();
                    }

                    public Objective getObjective() {
                        return ScoreboardManager.this.scoreboard.getObjective(name);
                    }

                    public int getScore() throws IllegalStateException {
                        return scoreboardScore.getScore();
                    }

                    public void setScore(int i) throws IllegalStateException {
                    }

                    public boolean isScoreSet() throws IllegalStateException {
                        return true;
                    }

                    public Scoreboard getScoreboard() {
                        return ScoreboardManager.this.scoreboard;
                    }
                });
            }
            return scores;
        }
        return Collections.emptyList();
    }

    public int getScore(String scorename) {
        for (Score score : this.scoreboard.getScores(scorename)) {
            if (!score.isScoreSet()) continue;
            return score.getScore();
        }
        return 0;
    }

    public Scoreboard getScoreboard() {
        return this.scoreboard;
    }

    public UUID getTargetPlayer() {
        return this.targetPlayer;
    }

    public void setScoreboard(Scoreboard scoreboard) {
        this.scoreboard = scoreboard;
    }

    public void setTargetPlayer(UUID targetPlayer) {
        this.targetPlayer = targetPlayer;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ScoreboardManager)) {
            return false;
        }
        ScoreboardManager other = (ScoreboardManager)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Scoreboard this$scoreboard = this.getScoreboard();
        Scoreboard other$scoreboard = other.getScoreboard();
        if (this$scoreboard == null ? other$scoreboard != null : !this$scoreboard.equals((Object)other$scoreboard)) {
            return false;
        }
        UUID this$targetPlayer = this.getTargetPlayer();
        UUID other$targetPlayer = other.getTargetPlayer();
        return !(this$targetPlayer == null ? other$targetPlayer != null : !((Object)this$targetPlayer).equals(other$targetPlayer));
    }

    protected boolean canEqual(Object other) {
        return other instanceof ScoreboardManager;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        Scoreboard $scoreboard = this.getScoreboard();
        result = result * 59 + ($scoreboard == null ? 43 : $scoreboard.hashCode());
        UUID $targetPlayer = this.getTargetPlayer();
        result = result * 59 + ($targetPlayer == null ? 43 : ((Object)$targetPlayer).hashCode());
        return result;
    }

    public String toString() {
        return "ScoreboardManager(scoreboard=" + (Object)this.getScoreboard() + ", targetPlayer=" + this.getTargetPlayer() + ")";
    }
}

