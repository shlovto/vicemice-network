package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerScoreboardDisplayObjective;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.UUID;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.replay.ScoreboardManager;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="ScoreboardDisplayObjectiveMeta", opcode=20, adapter= ScoreboardDisplayObjectiveMeta.ScoreboardDisplayObjectiveMetaAdapter.class)
public class ScoreboardDisplayObjectiveMeta
implements Meta {
    private int position;
    private String scoreName;
    private UUID uuid;

    @Override
    public void writeMeta(DataOutputStream out) {
        DataStreamer.writeVarInt(this.position, out);
        DataStreamer.writeString(this.scoreName, out);
        DataStreamer.writeUUID(this.uuid, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) {
        this.position = DataStreamer.readVarInt(in);
        this.scoreName = DataStreamer.readString(in);
        if (version >= 7) {
            this.uuid = DataStreamer.readUUID(in);
        }
    }

    @Override
    public void rollback(Context context) {
        ScoreboardManager scoreboard = this.uuid == null ? context.getScoreboard() : context.getScoreboard(this.uuid);
        scoreboard.setDisplaySlotForObjective(this.scoreName, this.position);
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    @Override
    public void execute(Context context) {
        ScoreboardManager scoreboard = this.uuid == null ? context.getScoreboard() : context.getScoreboard(this.uuid);
        scoreboard.setDisplaySlotForObjective(this.scoreName, this.position);
    }

    public ScoreboardDisplayObjectiveMeta() {
    }

    @ConstructorProperties(value={"position", "scoreName", "uuid"})
    public ScoreboardDisplayObjectiveMeta(int position, String scoreName, UUID uuid) {
        this.position = position;
        this.scoreName = scoreName;
        this.uuid = uuid;
    }

    public String toString() {
        return "ScoreboardDisplayObjectiveMeta(position=" + this.position + ", scoreName=" + this.scoreName + ", uuid=" + this.uuid + ")";
    }

    public static class ScoreboardDisplayObjectiveMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;

        public ScoreboardDisplayObjectiveMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.SCOREBOARD_DISPLAY_OBJECTIVE});
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }

        public void onPacketSending(PacketEvent e) {
            WrapperPlayServerScoreboardDisplayObjective packet = new WrapperPlayServerScoreboardDisplayObjective(e.getPacket());
            ScoreboardDisplayObjectiveMeta meta = new ScoreboardDisplayObjectiveMeta(packet.getPosition(), packet.getScoreName(), e.getPlayer().getUniqueId());
            this.replayFileWriter.addMeta(meta);
        }

        public void onPacketReceiving(PacketEvent e) {
        }
    }
}

