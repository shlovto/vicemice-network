package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerSpawnEntity;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="SpawnEntityObject", opcode=39, adapter= SpawnObjectMeta.SpawnObjectMetaAdapter.class)
public class SpawnObjectMeta
implements Meta {
    private int id;
    private int type;
    private double x;
    private double y;
    private double z;
    private float pitch;
    private float yaw;
    private int data;
    private double velocityX;
    private double velocityY;
    private double velocityZ;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeVarInt(this.id, out);
        DataStreamer.writeVarInt(this.type, out);
        out.writeDouble(this.x);
        out.writeDouble(this.y);
        out.writeDouble(this.z);
        out.writeFloat(this.pitch);
        out.writeFloat(this.yaw);
        DataStreamer.writeVarInt(this.data, out);
        out.writeDouble(this.velocityX);
        out.writeDouble(this.velocityY);
        out.writeDouble(this.velocityZ);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.id = DataStreamer.readVarInt(in);
        this.type = DataStreamer.readVarInt(in);
        this.x = in.readDouble();
        this.y = in.readDouble();
        this.z = in.readDouble();
        this.pitch = in.readFloat();
        this.yaw = in.readFloat();
        this.data = DataStreamer.readVarInt(in);
        this.velocityX = in.readDouble();
        this.velocityY = in.readDouble();
        this.velocityZ = in.readDouble();
    }

    @Override
    public void execute(Context context) {
        WrapperPlayServerSpawnEntity packet = new WrapperPlayServerSpawnEntity();
        packet.setEntityID(this.id);
        packet.setType(this.type);
        packet.setX(this.x);
        packet.setY(this.y);
        packet.setZ(this.z);
        packet.setPitch(this.pitch);
        packet.setYaw(this.yaw);
        packet.setObjectData(this.data);
        packet.setOptionalSpeedX(this.velocityX);
        packet.setOptionalSpeedY(this.velocityY);
        packet.setOptionalSpeedZ(this.velocityZ);
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public SpawnObjectMeta() {
    }

    @ConstructorProperties(value={"id", "type", "x", "y", "z", "pitch", "yaw", "data", "velocityX", "velocityY", "velocityZ"})
    public SpawnObjectMeta(int id, int type, double x, double y, double z, float pitch, float yaw, int data, double velocityX, double velocityY, double velocityZ) {
        this.id = id;
        this.type = type;
        this.x = x;
        this.y = y;
        this.z = z;
        this.pitch = pitch;
        this.yaw = yaw;
        this.data = data;
        this.velocityX = velocityX;
        this.velocityY = velocityY;
        this.velocityZ = velocityZ;
    }

    public static class SpawnObjectMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;
        private int lastSpawnObject;

        public SpawnObjectMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.SPAWN_ENTITY});
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }

        public void onPacketSending(PacketEvent event) {
            WrapperPlayServerSpawnEntity packet = new WrapperPlayServerSpawnEntity(event.getPacket());
            if (this.lastSpawnObject != packet.getEntityID()) {
                this.lastSpawnObject = packet.getEntityID();
                SpawnObjectMeta meta = new SpawnObjectMeta(packet.getEntityID(), packet.getType(), packet.getX(), packet.getY(), packet.getZ(), packet.getPitch(), packet.getYaw(), packet.getObjectData(), packet.getOptionalSpeedX(), packet.getOptionalSpeedY(), packet.getOptionalSpeedZ());
                this.replayFileWriter.addMeta(meta);
            }
        }

        public void onPacketReceiving(PacketEvent event) {
        }
    }
}

