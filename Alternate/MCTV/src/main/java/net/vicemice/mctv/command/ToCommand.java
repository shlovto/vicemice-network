package net.vicemice.mctv.command;

import java.util.Objects;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.session.ReplaySession;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.entity.Player;

public class ToCommand extends Command {

    public ToCommand(MCTV mctv) {
        super("to");
        this.mctv = mctv;
        ((CraftServer) mctv.getServer()).getCommandMap().register("to", this);
    }

    private MCTV mctv;

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] args) {
        int position;
        if (!(commandSender instanceof Player)) {
            return true;
        }
        Player player = (Player)commandSender;
        ReplaySession session = MCTV.getInstance().getReplaySession();
        if (!Objects.equals(session.getContext().getMaster(), player.getUniqueId())) {
            this.mctv.sendMessage(player, "command-to-not-master", new Object[0]);
            return true;
        }
        if (args.length < 1) {
            this.mctv.sendMessage(player, "command-to-no-args", new Object[0]);
            return true;
        }
        try {
            position = Integer.parseInt(args[0]);
        }
        catch (NumberFormatException ex) {
            this.mctv.sendMessage(player, "command-to-correct-args", args[0]);
            return true;
        }
        int currentPosition = session.getContext().getPlayedFrames() / 20;
        session.skip(position - currentPosition);
        this.mctv.sendMessage(player, "command-to-success", session.getContext().getPlayedFrames() / 20);
        return true;
    }
}

