package net.vicemice.mctv.config.replay;

import lombok.Getter;

/*
 * Class created at 23:42 - 18.04.2020
 * Copyright (C) elrobtossohn
 */
@Getter
public class ReplayConfig {
    private boolean REPLAY_SERVER = false;
}