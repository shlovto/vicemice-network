package net.vicemice.mctv.io.storage;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import io.minio.MinioClient;
import io.minio.ObjectStat;
import io.minio.PutObjectOptions;
import io.minio.Result;
import io.minio.messages.Item;
import net.vicemice.hector.GoAPI;
import net.vicemice.mctv.MCTV;
import org.bukkit.Bukkit;
import org.kamranzafar.jtar.TarEntry;
import org.kamranzafar.jtar.TarInputStream;
import java.io.*;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.zip.GZIPInputStream;

/*
 * Class created at 23:32 - 18.04.2020
 * Copyright (C) elrobtossohn
 */
public class S2Storage implements Storage {
    private static final ListeningExecutorService executor = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());
    private MinioClient client = null;

    public S2Storage() {
        try {
            this.client = new MinioClient("http://91.218.67.206:9000/", "minio", "miniostorage");
            if(!this.client.bucketExists("replay")) {
                this.client.makeBucket("replay");
                System.out.println("Create Bucket 'replay'");
            }
            if(!this.client.bucketExists("worlds")) {
                this.client.makeBucket("worlds");
                System.out.println("Create Bucket 'worlds'");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public MinioClient getClient() {
        return client;
    }

    @Override
    public int getStorageTimeout() {
        return 600;
    }

    @Override
    public ListenableFuture<Void> storeWorld(final File file, final String hash) {
        return executor.submit(new Callable<Void>(){

            @Override
            public Void call() throws Exception {
                ClassLoader cl = Thread.currentThread().getContextClassLoader();
                if (MCTV.getInstance() != null) {
                    Thread.currentThread().setContextClassLoader(((Object)(MCTV.getInstance())).getClass().getClassLoader());
                }
                Bukkit.getLogger().info("Storing to os");
                S2Storage.this.getClient().putObject("worlds", hash+".tar.gz", file.toString(), new PutObjectOptions(file.length(), PutObjectOptions.MAX_PART_SIZE));
                Bukkit.getLogger().info("Done");
                Thread.currentThread().setContextClassLoader(cl);
                return null;
            }
        });
    }

    @Override
    public ListenableFuture<Void> storeReplay(final File file) {
        return executor.submit(new Callable<Void>(){

            @Override
            public Void call() throws Exception {
                ClassLoader cl = Thread.currentThread().getContextClassLoader();
                if (MCTV.getInstance() != null) {
                    Thread.currentThread().setContextClassLoader(((Object)(MCTV.getInstance())).getClass().getClassLoader());
                }
                UUID uuid = UUID.randomUUID();
                S2Storage.this.getClient().putObject("replay", (GoAPI.getGameIDAPI().getGameUID() != null ? GoAPI.getGameIDAPI().getGameUID() : uuid)+".dat.gz", file.toString(), new PutObjectOptions(file.length(), PutObjectOptions.MAX_PART_SIZE));
                Thread.currentThread().setContextClassLoader(cl);
                Bukkit.broadcastMessage("-> "+uuid);
                return null;
            }
        });
    }

    @Override
    public ListenableFuture<InputStream> getReplay(final UUID uuid, final Consumer<Double> progress) throws IOException {
        final File tempFile = new File(MCTV.getInstance().getDataFolder(), "temp.dat.gz");
        if (tempFile.exists()) {
            progress.accept(1.0);
            return Futures.immediateCheckedFuture(new FileInputStream(tempFile));
        }
        return executor.submit(new Callable<InputStream>(){

            @Override
            public InputStream call() throws Exception {
                ClassLoader cl = Thread.currentThread().getContextClassLoader();
                if (MCTV.getInstance() != null) {
                    Thread.currentThread().setContextClassLoader(((Object)(MCTV.getInstance())).getClass().getClassLoader());
                }
                S2Storage.this.getClient().statObject("replay", uuid+".dat.gz");
                ObjectStat objectStat = S2Storage.this.getClient().statObject("replay", uuid+".dat.gz");
                executor.submit(new Runnable(){

                    @Override
                    public void run() {
                        double percentage;
                        do {
                            percentage = tempFile.length() / objectStat.length();
                            progress.accept(percentage);
                            try {
                                Thread.sleep(50L);
                            }
                            catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        } while (percentage < 1.0);
                    }
                });
                S2Storage.this.getClient().getObject("replay", uuid+".dat.gz", tempFile.toString());
                Thread.currentThread().setContextClassLoader(cl);
                progress.accept(1.0);
                return new FileInputStream(tempFile);
            }
        });
    }

    @Override
    public ListenableFuture<Boolean> isWorldStored(final String hash) {
        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        return executor.submit(() -> {
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            if (MCTV.getInstance() != null) {
                Thread.currentThread().setContextClassLoader(((Object)(MCTV.getInstance())).getClass().getClassLoader());
            }
            Iterable<Result<Item>> objects = S2Storage.this.getClient().listObjects("worlds");
            for (Result<Item> item : objects) {
                try {
                    if (!item.get().objectName().equals(hash + ".tar.gz")) continue;
                    return true;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            return false;
        });
    }

    @Override
    public void getWorld(final String hash, final File destFolder, final Consumer<Double> progress) {
        try {
            executor.submit((Callable<Void>) () -> {
                ClassLoader cl = Thread.currentThread().getContextClassLoader();
                if (MCTV.getInstance() != null) {
                    Thread.currentThread().setContextClassLoader(((Object)(MCTV.getInstance())).getClass().getClassLoader());
                }
                final File tempFile = new File(MCTV.getInstance().getDataFolder(), hash + ".tar.gz");
                ObjectStat objectStat = S2Storage.this.getClient().statObject("worlds", hash+".tar.gz");
                executor.submit(() -> {
                    double percentage;
                    do {
                        percentage = tempFile.length() / objectStat.length();
                        progress.accept(percentage);
                        try {
                            Thread.sleep(50L);
                        }
                        catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } while (percentage < 1.0);
                });
                S2Storage.this.getClient().getObject("worlds", hash+".tar.gz", tempFile.toString());
                TarInputStream tis = new TarInputStream(new BufferedInputStream(new GZIPInputStream(new FileInputStream(tempFile))));
                S2Storage.this.untar(tis, destFolder.getAbsolutePath());
                tis.close();
                progress.accept(1.0);
                Thread.currentThread().setContextClassLoader(cl);
                return null;
            }).get();
        }
        catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void untar(TarInputStream tis, String destFolder) throws IOException {
        TarEntry entry;
        BufferedOutputStream dest = null;
        while ((entry = tis.getNextEntry()) != null) {
            int count;
            System.out.println("Extracting: " + entry.getName());
            byte[] data = new byte[1024];
            if (entry.isDirectory()) {
                new File(destFolder + "/" + entry.getName()).mkdirs();
                continue;
            }
            int di = entry.getName().lastIndexOf(47);
            if (di != -1) {
                new File(destFolder + "/" + entry.getName().substring(0, di)).mkdirs();
            }
            FileOutputStream fos = new FileOutputStream(destFolder + "/" + entry.getName());
            dest = new BufferedOutputStream(fos);
            while ((count = tis.read(data)) != -1) {
                dest.write(data, 0, count);
            }
            dest.flush();
            dest.close();
        }
    }
}
