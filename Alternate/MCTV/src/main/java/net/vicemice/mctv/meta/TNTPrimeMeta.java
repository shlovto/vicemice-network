package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="TNTPrime", opcode=28, adapter= TNTPrimeMeta.TNTPrimeMetaMetaAdapter.class)
public class TNTPrimeMeta implements Meta {
    private Location location;
    private int fuseTicksDiff;
    private String world;
    private TNTPrimed primed;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeVarInt(this.location.getBlockX(), out);
        DataStreamer.writeVarInt(this.location.getBlockY(), out);
        DataStreamer.writeVarInt(this.location.getBlockZ(), out);
        DataStreamer.writeString(this.location.getWorld().getName(), out);
        DataStreamer.writeVarInt(this.fuseTicksDiff, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.location = new Location(null, (double)DataStreamer.readVarInt(in), (double)DataStreamer.readVarInt(in), (double)DataStreamer.readVarInt(in));
        this.world = DataStreamer.readString(in);
        if (version >= 9) {
            this.fuseTicksDiff = DataStreamer.readVarInt(in);
        }
    }

    @Override
    public void execute(Context context) {
        World world = Bukkit.getWorld((String)this.world);
        this.location.setWorld(world);
        this.location.getBlock().setType(Material.AIR);
        this.primed = (TNTPrimed)world.spawn(this.location, TNTPrimed.class);
        if (this.fuseTicksDiff != -1) {
            this.primed.setFuseTicks(this.primed.getFuseTicks() - this.fuseTicksDiff);
        }
    }

    @Override
    public void rollback(Context context) {
        if (this.primed != null) {
            this.primed.remove();
            this.primed = null;
        }
        this.location.getBlock().setType(Material.TNT);
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public TNTPrimeMeta() {
    }

    @ConstructorProperties(value={"location", "fuseTicksDiff", "world", "primed"})
    public TNTPrimeMeta(Location location, int fuseTicksDiff, String world, TNTPrimed primed) {
        this.location = location;
        this.fuseTicksDiff = fuseTicksDiff;
        this.world = world;
        this.primed = primed;
    }

    public static class TNTPrimeMetaMetaAdapter
    implements MetaAdapter,
    Listener {
        private ReplayFileWriter replayFileWriter;

        public TNTPrimeMetaMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            Bukkit.getPluginManager().registerEvents((Listener)this, (Plugin)MCTV.getInstance());
        }

        @Override
        public void stop() {
        }

        @EventHandler(ignoreCancelled=true, priority=EventPriority.MONITOR)
        public void onInteract(PlayerInteractEvent event) {
            if (event.hasBlock() && event.hasItem() && event.getClickedBlock().getType() == Material.TNT && (event.getItem().getType() == Material.FLINT_AND_STEEL || event.getItem().getType() == Material.FIREBALL)) {
                this.replayFileWriter.addMeta(new TNTPrimeMeta(event.getClickedBlock().getLocation(), 0, null, null));
            }
        }

        @EventHandler(ignoreCancelled=true, priority=EventPriority.MONITOR)
        public void onEntityChangeBlock(EntityChangeBlockEvent event) {
            if (event.getEntityType() == EntityType.ARROW && event.getBlock().getType() == Material.TNT) {
                this.replayFileWriter.addMeta(new TNTPrimeMeta(event.getBlock().getLocation(), 0, null, null));
            }
        }

        @EventHandler(ignoreCancelled=true, priority=EventPriority.MONITOR)
        public void onEntityChangeBlock(BlockRedstoneEvent event) {
            if (event.getBlock().getType() == Material.TNT) {
                this.replayFileWriter.addMeta(new TNTPrimeMeta(event.getBlock().getLocation(), 0, null, null));
            }
        }
    }
}

