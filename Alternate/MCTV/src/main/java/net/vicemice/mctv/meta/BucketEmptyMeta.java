package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="BucketEmptyMeta", opcode=25, adapter= BucketEmptyMeta.BucketEmptyMetaAdapter.class)
public class BucketEmptyMeta
implements Meta {
    private BlockFace blockFace;
    private Location location;
    private Material bucket;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeString(this.blockFace.toString(), out);
        out.writeDouble(this.location.getX());
        out.writeDouble(this.location.getY());
        out.writeDouble(this.location.getZ());
        DataStreamer.writeString(this.bucket.toString(), out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.blockFace = BlockFace.valueOf((String)DataStreamer.readString(in));
        this.location = new Location(null, in.readDouble(), in.readDouble(), in.readDouble());
        this.bucket = Material.valueOf((String)DataStreamer.readString(in));
    }

    @Override
    public void execute(Context context) {
        Block block;
        if (!this.bucket.equals((Object)Material.LAVA_BUCKET) && !this.bucket.equals((Object)Material.WATER_BUCKET)) {
            return;
        }
        this.location.setWorld(MCTV.getInstance().getApi().getContext().getMasterPlayer().getWorld());
        switch (this.blockFace) {
            case UP: {
                block = this.location.clone().add(0.0, 1.0, 0.0).getBlock();
                break;
            }
            case DOWN: {
                block = this.location.clone().subtract(0.0, 1.0, 0.0).getBlock();
                break;
            }
            case SOUTH: {
                block = this.location.clone().add(0.0, 0.0, 1.0).getBlock();
                break;
            }
            case NORTH: {
                block = this.location.clone().subtract(0.0, 0.0, 1.0).getBlock();
                break;
            }
            case EAST: {
                block = this.location.clone().add(1.0, 0.0, 0.0).getBlock();
                break;
            }
            case WEST: {
                block = this.location.clone().subtract(1.0, 0.0, 0.0).getBlock();
                break;
            }
            default: {
                return;
            }
        }
        block.setType(this.bucket.equals((Object)Material.LAVA_BUCKET) ? Material.LAVA : Material.WATER);
    }

    @Override
    public void rollback(Context context) {
        Block block;
        this.location.setWorld(MCTV.getInstance().getApi().getContext().getMasterPlayer().getWorld());
        switch (this.blockFace) {
            case UP: {
                block = this.location.clone().add(0.0, 1.0, 0.0).getBlock();
                break;
            }
            case DOWN: {
                block = this.location.clone().subtract(0.0, 1.0, 0.0).getBlock();
                break;
            }
            case SOUTH: {
                block = this.location.clone().add(0.0, 0.0, 1.0).getBlock();
                break;
            }
            case NORTH: {
                block = this.location.clone().subtract(0.0, 0.0, 1.0).getBlock();
                break;
            }
            case EAST: {
                block = this.location.clone().add(1.0, 0.0, 0.0).getBlock();
                break;
            }
            case WEST: {
                block = this.location.clone().subtract(1.0, 0.0, 0.0).getBlock();
                break;
            }
            default: {
                return;
            }
        }
        if (block.getType().equals((Object)Material.LAVA) || block.getType().equals((Object)Material.STATIONARY_LAVA) || block.getType().equals((Object)Material.WATER) || block.getType().equals((Object)Material.STATIONARY_WATER)) {
            block.setType(Material.AIR);
        }
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public BucketEmptyMeta() {
    }

    @ConstructorProperties(value={"blockFace", "location", "bucket"})
    public BucketEmptyMeta(BlockFace blockFace, Location location, Material bucket) {
        this.blockFace = blockFace;
        this.location = location;
        this.bucket = bucket;
    }

    public static class BucketEmptyMetaAdapter
    implements MetaAdapter,
    Listener {
        ReplayFileWriter replayFileWriter;

        public BucketEmptyMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            Bukkit.getPluginManager().registerEvents((Listener)this, (Plugin)MCTV.getInstance());
        }

        @Override
        public void stop() {
        }

        @EventHandler(ignoreCancelled=true, priority=EventPriority.MONITOR)
        public void onBukkitEmpty(PlayerBucketEmptyEvent e) {
            BucketEmptyMeta meta = new BucketEmptyMeta(e.getBlockFace(), e.getBlockClicked().getLocation(), e.getBucket());
            this.replayFileWriter.addMeta(meta);
        }
    }
}

