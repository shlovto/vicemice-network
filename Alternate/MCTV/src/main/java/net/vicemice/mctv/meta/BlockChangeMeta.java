package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerBlockChange;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import net.vicemice.hector.api.protocol.wrappers.WrappedBlockData;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="BlockActionMeta", opcode=38, adapter= BlockChangeMeta.BlockChangeMetaAdapter.class)
public class BlockChangeMeta
implements Meta {
    private Location location;
    private WrappedBlockData blockData;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        out.writeUTF(this.blockData.getType().name());
        out.writeInt(this.blockData.getData());
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.blockData = new WrappedBlockData(null) {
            @Override
            public Material getType() {
                return null;
            }

            @Override
            public int getData() {
                return 0;
            }

            @Override
            public void setType(Material material) {

            }

            @Override
            public void setData(int i) {

            }

            @Override
            public void setTypeAndData(Material material, int i) {

            }

            @Override
            public WrappedBlockData deepClone() {
                return null;
            }
        };
        this.blockData.setTypeAndData(Material.valueOf((String)in.readUTF()), in.readInt());
    }

    @Override
    public void execute(Context context) {
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return true;
    }

    @ConstructorProperties(value={"location", "blockData"})
    public BlockChangeMeta(Location location, WrappedBlockData blockData) {
        this.location = location;
        this.blockData = blockData;
    }

    public BlockChangeMeta() {
    }

    public static class BlockChangeMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private final ReplayFileWriter replayFileWriter;

        public BlockChangeMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.BLOCK_CHANGE});
            this.replayFileWriter = replayFileWriter;
        }

        public void onPacketSending(PacketEvent event) {
            WrapperPlayServerBlockChange change = new WrapperPlayServerBlockChange(event.getPacket());
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }
    }
}

