package net.vicemice.mctv.command;

import net.vicemice.hector.GoAPI;
import net.vicemice.mctv.MCTV;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CondMessageCommand extends Command {

    public CondMessageCommand() {
        super("condmessage");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] args) {
        if (GoAPI.getUserAPI().getRankData(((Player) commandSender).getUniqueId()).getAccess_level() >= 100) {
            if (args.length < 2) {
                commandSender.sendMessage("\u00a7cInvalid command usage.");
            } else {
                String condition = args[0];
                CharSequence[] newArray = new String[args.length - 1];
                System.arraycopy(args, 1, newArray, 0, args.length - 1);
                MCTV.getInstance().getApi().writeConditionalMessage(ChatColor.translateAlternateColorCodes('&', String.join((CharSequence)" ", newArray)), condition);
            }
        } else {
            commandSender.sendMessage("\u00a7cYou are not permitted to do this.");
        }
        return true;
    }
}

