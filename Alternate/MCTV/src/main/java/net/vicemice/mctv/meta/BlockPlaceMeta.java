package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Set;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.material.Door;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

@MetaInfo(name="blockPlace", opcode=8, adapter= BlockPlaceMeta.BlockPlaceMetaAdapter.class)
public class BlockPlaceMeta
implements Meta {
    private String worldName;
    private Location location;
    private Material blockType;
    private byte durability;
    private Material oldMaterial;
    private byte oldData;
    private Location otherHalf;
    private byte otherHalfData;

    public BlockPlaceMeta(Location location, byte durability) {
        this.location = location;
        this.durability = durability;
    }

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeString(this.location.getWorld().getName(), out);
        DataStreamer.writeVarInt(this.location.getBlockX(), out);
        DataStreamer.writeVarInt(this.location.getBlockY(), out);
        DataStreamer.writeVarInt(this.location.getBlockZ(), out);
        DataStreamer.writeString(this.location.getBlock().getType().name(), out);
        out.writeByte(this.durability);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.worldName = DataStreamer.readString(in);
        this.location = new Location(null, (double)DataStreamer.readVarInt(in), (double)DataStreamer.readVarInt(in), (double)DataStreamer.readVarInt(in));
        this.blockType = Material.valueOf((String)DataStreamer.readString(in));
        this.durability = in.readByte();
    }

    @Override
    public void execute(Context context) {
        this.location.setWorld(Bukkit.getWorld((String)this.worldName));
        this.oldMaterial = this.location.getBlock().getType();
        this.oldData = this.location.getBlock().getData();
        if (this.oldMaterial == Material.BED_BLOCK) {
            BlockFace[] blockFaces;
            for (BlockFace blockFace : blockFaces = new BlockFace[]{BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST}) {
                if (this.location.getBlock().getRelative(blockFace).getType() != this.oldMaterial) continue;
                this.otherHalf = this.location.getBlock().getRelative(blockFace).getLocation();
                this.otherHalfData = this.location.getBlock().getRelative(blockFace).getData();
                break;
            }
            this.otherHalf.getBlock().setType(this.blockType);
            this.otherHalf.getBlock().setData(this.durability);
            this.location.getBlock().setType(this.blockType);
            this.location.getBlock().setData(this.durability);
        } else {
            this.location.getBlock().setType(this.blockType);
            this.location.getBlock().setData(this.durability);
        }
    }

    @Override
    public void rollback(Context context) {
        World world = Bukkit.getServer() != null ? Bukkit.getWorld((String)this.worldName) : null;
        this.location.setWorld(world);
        if (this.location.getBlock().getType() == Material.FIRE) {
            this.location.getBlock().setType(Material.AIR);
        } else {
            this.location.getBlock().setType(this.oldMaterial);
            this.location.getBlock().setData(this.oldData);
        }
        if (this.otherHalf != null) {
            this.otherHalf.getBlock().setType(this.oldMaterial);
            this.otherHalf.getBlock().setData(this.otherHalfData);
        }
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public BlockPlaceMeta() {
    }

    @ConstructorProperties(value={"worldName", "location", "blockType", "durability", "oldMaterial", "oldData", "otherHalf", "otherHalfData"})
    public BlockPlaceMeta(String worldName, Location location, Material blockType, byte durability, Material oldMaterial, byte oldData, Location otherHalf, byte otherHalfData) {
        this.worldName = worldName;
        this.location = location;
        this.blockType = blockType;
        this.durability = durability;
        this.oldMaterial = oldMaterial;
        this.oldData = oldData;
        this.otherHalf = otherHalf;
        this.otherHalfData = otherHalfData;
    }

    public static class BlockPlaceMetaAdapter
    implements MetaAdapter,
    Listener {
        private ReplayFileWriter replayFileWriter;

        public BlockPlaceMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            Bukkit.getPluginManager().registerEvents((Listener)this, (Plugin)MCTV.getInstance());
        }

        @Override
        public void stop() {
            HandlerList.unregisterAll((Listener)this);
        }

        @EventHandler(priority=EventPriority.MONITOR, ignoreCancelled=true)
        public void onBlockForm(final BlockFromToEvent event) {
            new BukkitRunnable(){

                public void run() {
                    BlockPlaceMeta meta = new BlockPlaceMeta(event.getToBlock().getLocation(), event.getToBlock().getData());
                    replayFileWriter.addMeta(meta);
                }
            }.runTaskLater((Plugin)MCTV.getInstance(), 1L);
        }

        @EventHandler(priority=EventPriority.MONITOR)
        public void onBlockPlace(BlockPlaceEvent event) {
            BlockPlaceMeta blockPlaceMeta = new BlockPlaceMeta(event.getBlock().getLocation(), event.getBlockPlaced().getData());
            this.replayFileWriter.addMeta(blockPlaceMeta);
            if (event.getBlockPlaced().getType().name().contains("DOOR") && event.getBlockPlaced().getType() != Material.TRAP_DOOR) {
                Door door;
                Location half = event.getBlock().getLocation().add(0.0, (door = (Door)event.getBlockPlaced().getState().getData()).isTopHalf() ? -1.0 : 1.0, 0.0);
                BlockPlaceMeta otherMeta = new BlockPlaceMeta(half, half.getBlock().getData());
                this.replayFileWriter.addMeta(otherMeta);
            }
        }

        @EventHandler(priority=EventPriority.MONITOR)
        public void onEntityChangeBlock(final EntityChangeBlockEvent event) {
            Bukkit.getScheduler().runTaskLater((Plugin)MCTV.getInstance(), new Runnable(){

                @Override
                public void run() {
                    BlockPlaceMeta meta = new BlockPlaceMeta(event.getBlock().getLocation(), event.getBlock().getData());
                    replayFileWriter.addMeta(meta);
                }
            }, 1L);
        }

        @EventHandler(ignoreCancelled=true)
        public void onInteract(final PlayerInteractEvent event) {
            Block targetBlock;
            Player player = event.getPlayer();
            final Block clickedBlock = event.getClickedBlock();
            if (event.hasBlock()) {
                final Material oldType = clickedBlock.getType();
                final byte oldData = clickedBlock.getData();
                Bukkit.getScheduler().runTask((Plugin)MCTV.getInstance(), new Runnable(){

                    @Override
                    public void run() {
                        Material newType = clickedBlock.getType();
                        final byte newData = clickedBlock.getData();
                        if (oldType != newType || oldData != newData) {
                            BlockPlaceMeta blockPlaceMeta = new BlockPlaceMeta(clickedBlock.getLocation(), clickedBlock.getData());
                            replayFileWriter.addMeta(blockPlaceMeta);
                            if (oldType == newType && (oldType == Material.WOOD_BUTTON || oldType == Material.STONE_BUTTON || oldType == Material.STONE_PLATE || oldType == Material.WOOD_PLATE || oldType == Material.GOLD_PLATE || oldType == Material.IRON_PLATE)) {
                                Bukkit.getScheduler().runTaskLater((Plugin)MCTV.getInstance(), new Runnable(){

                                    @Override
                                    public void run() {
                                        if (clickedBlock.getType() == oldType && clickedBlock.getData() != newData) {
                                            BlockPlaceMeta blockPlaceMeta = new BlockPlaceMeta(clickedBlock.getLocation(), clickedBlock.getData());
                                            replayFileWriter.addMeta(blockPlaceMeta);
                                        }
                                    }
                                }, (oldType == Material.WOOD_BUTTON ? 30L : 20L) + 1L);
                            }
                        }
                    }
                });
                new BukkitRunnable(){

                    public void run() {
                        if (event.getClickedBlock().getType().name().contains("DOOR") && event.getClickedBlock().getType() != Material.TRAP_DOOR) {
                            Door door;
                            Location half = event.getClickedBlock().getLocation().add(0.0, (door = (Door)event.getClickedBlock().getState().getData()).isTopHalf() ? -1.0 : 0.0, 0.0);
                            BlockPlaceMeta otherMeta = new BlockPlaceMeta(half, half.getBlock().getData());
                            replayFileWriter.addMeta(otherMeta);
                        }
                    }
                }.runTaskLater((Plugin)MCTV.getInstance(), 2L);
            }
            if (event.getAction() == Action.LEFT_CLICK_BLOCK && (targetBlock = player.getTargetBlock((Set)null, 5)).getType() == Material.FIRE) {
                Bukkit.getScheduler().runTaskLater((Plugin)MCTV.getInstance(), new Runnable(){

                    @Override
                    public void run() {
                        if (targetBlock.getType() != Material.FIRE) {
                            BlockPlaceMeta blockPlaceMeta = new BlockPlaceMeta(targetBlock.getLocation(), targetBlock.getData());
                            replayFileWriter.addMeta(blockPlaceMeta);
                        }
                    }
                }, 1L);
            }
        }
    }
}

