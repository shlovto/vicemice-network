package net.vicemice.mctv;

import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.ProtocolManager;

import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import lombok.Getter;
import net.vicemice.hector.utils.players.api.actionbat.ActionBarAPI;
import net.vicemice.hector.utils.scheduler.ScheduledExecution;
import net.vicemice.mctv.command.PromoteCommand;
import net.vicemice.mctv.command.SpecCommand;
import net.vicemice.mctv.command.ToCommand;
import net.vicemice.mctv.command.ToggleACCCommand;
import net.vicemice.mctv.io.storage.S2Storage;
import net.vicemice.mctv.io.storage.Storage;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.listener.BlockFromToListener;
import net.vicemice.mctv.listener.SpectateListener;
import net.vicemice.mctv.session.ReplaySession;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.frame.async.MoreExecutors;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.Register;
import net.vicemice.mctv.config.replay.ReplayConfig;
import net.vicemice.mctv.config.replay.ReplayConfiguration;
import net.vicemice.mctv.packetlistener.PlayReplayPacketListener;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class MCTV extends JavaPlugin implements CommandExecutor {
    public static final int REPLAY_VERSION = 15;
    private static MCTV instance;
    private Storage storage;
    private MCTVApi api = new MCTVApi(this);
    private ReplaySession replaySession = new ReplaySession(this.api.getContext());
    private ProtocolManager protocolManager;
    private SpectateListener spectateListener;
    @Getter
    public static ReplayConfig replayConfig;
    public String replayId;

    public void onLoad() {
    }

    public void onEnable() {
        instance = this;

        ReplayConfiguration replayConfiguration = new ReplayConfiguration();
        if (!replayConfiguration.exists()) {
            ReplayConfig config = new ReplayConfig();
            replayConfiguration.writeConfiguration(config);
            System.out.println("Replay Config File not found and will be created...");
            System.out.println("Replay Config File was created!");
        }
        replayConfiguration.readConfiguration();
        replayConfig = replayConfiguration.getReplayConfig();

        this.protocolManager = ProtocolLibrary.getProtocolManager();
        this.spectateListener = new SpectateListener();
        this.spectateListener.start();
        if (!(this.getDataFolder().exists() || this.getDataFolder().mkdirs())) {
            throw new AssertionError();
        }
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(this.getClassLoader());
        this.storage = new S2Storage();
        Thread.currentThread().setContextClassLoader(cl);
        if (replayConfig.isREPLAY_SERVER()) {
            Register.registerListener(this, "net.vicemice.mctv.listener");
            Bukkit.getPluginManager().registerEvents(new BlockFromToListener(), this);
            new ToCommand(this);
            new SpecCommand(this);
            new ToggleACCCommand(this);
            new PromoteCommand(this);
            GoServer.getService().getGoAPI().getNettyClient().addGetter(new PlayReplayPacketListener());

            new ScheduledExecution(this::updateActionBar, 0, 1, TimeUnit.SECONDS);
        } else {
            Register.registerListener(this, "net.vicemice.mctv.recordlistener");
            //((CraftServer)getServer()).getCommandMap().register("condmessage",new CondMessageCommand());
            for (World world : Bukkit.getWorlds()) {
                this.api.getContext().getMapManager().add(world.getName(), world.getWorldFolder().getAbsolutePath());
            }
            Bukkit.getScheduler().runTask(this, () -> {
                ReplayFileWriter writer = new ReplayFileWriter(MCTV.this, MCTV.this.api.getContext());
                MCTV.this.api.setWriter(writer);
                writer.startRecording();
            });
        }
    }

    public void updateActionBar() {
        if (getReplaySession().getContext() != null && getReplaySession().getContext().getMasterPlayer() != null) {
            Bukkit.getOnlinePlayers().forEach(o -> {
                int playedFrames = (Math.round((float)getReplaySession().getContext().getPlayedFrames() / 20.0f));
                long playedHours = playedFrames / 3600;
                long playedMinutes = (playedFrames % 3600) / 60;
                long playedSeconds = playedFrames % 60;

                int framesRecorded = Math.round((float)getReplaySession().getContext().getFramesRecorded() / 20.0f);
                long framesHours = framesRecorded / 3600;
                long framesMinutes = (framesRecorded % 3600) / 60;
                long framesSeconds = framesRecorded % 60;

                String status = "§2§l"+GoAPI.getUserAPI().translate(o.getUniqueId(), "starting")+"...";
                if (getReplaySession().getContext().getPaused().get()) {
                    status = "§c"+GoAPI.getUserAPI().translate(o.getUniqueId(), "paused");
                }
                if (!getReplaySession().getContext().getPaused().get()) {
                    status = "§a"+GoAPI.getUserAPI().translate(o.getUniqueId(), "playing");
                }
                if (getReplaySession().getContext().getPlayedFrames()==getReplaySession().getContext().getFramesRecorded()) {
                    status = "§6"+GoAPI.getUserAPI().translate(o.getUniqueId(), "end");
                }

                ActionBarAPI.sendActionBar(o, status+"    §e"+(playedHours != 0 ? (playedHours < 10 ? "0"+playedHours : playedHours)+":" : "")+(playedMinutes < 10 ? "0"+playedMinutes : playedMinutes)+":"+(playedSeconds < 10 ? "0"+playedSeconds : playedSeconds)+" / "+(framesHours != 0 ? (framesHours < 10 ? "0"+framesHours : framesHours)+":" : "")+(framesMinutes < 10 ? "0"+framesMinutes : framesMinutes)+":"+(framesSeconds < 10 ? "0"+framesSeconds : framesSeconds)+"    §6"+getReplaySession().getContext().getSpeed()+"x");
            });
        } else {
            Bukkit.getOnlinePlayers().forEach(o -> ActionBarAPI.sendActionBar(o, "§b§l"+GoAPI.getUserAPI().translate(o.getUniqueId(), "loading")));
        }
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return onCommand(sender, command, label, args);
    }

    public void log(String msg) {
        MCTV.getInstance().getLogger().info(msg);
    }

    public void broadcast(final String msg) {
        Bukkit.getOnlinePlayers().forEach((Consumer<Player>) player -> player.sendMessage("\u00a77[\u00a76MCTV\u00a77] \u00a7e" + msg));
    }

    public void broadcastLocalized(final String key, final Object ... args) {
        MoreExecutors.spigotSyncExecutor().execute(() -> {
            for (Player player : Bukkit.getOnlinePlayers()) {
                MCTV.this.sendMessage(player, key, args);
            }
        });
    }

    public void onDisable() {
        if (this.api.getWriter() != null) {
            this.api.getWriter().getRecordedSeconds();
            this.api.getWriter().stopRecording();
            this.api.getWriter().save();
            System.out.println("Stopping recording");
        } else {
            System.out.println("Could not stop recording and save the replay");
        }
        this.spectateListener.stop();
    }

    public void sendMessage(Player player, String key, Object ... args) {
        player.sendMessage(GoAPI.getUserAPI().translate(player.getUniqueId(), key, args));
    }

    public Storage getStorage() {
        return this.storage;
    }

    public MCTVApi getApi() {
        return this.api;
    }

    public ReplaySession getReplaySession() {
        return this.replaySession;
    }

    public ProtocolManager getProtocolManager() {
        return this.protocolManager;
    }

    public SpectateListener getSpectateListener() {
        return this.spectateListener;
    }

    public static MCTV getInstance() {
        return instance;
    }
}