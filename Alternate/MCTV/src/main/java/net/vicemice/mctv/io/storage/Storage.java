package net.vicemice.mctv.io.storage;

import com.google.common.util.concurrent.ListenableFuture;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import java.util.function.Consumer;

public interface Storage {
    public int getStorageTimeout();

    public ListenableFuture<Void> storeWorld(File var1, String var2);

    public ListenableFuture<Void> storeReplay(File var1);

    public ListenableFuture<InputStream> getReplay(UUID var1, Consumer<Double> var2) throws IOException;

    public ListenableFuture<Boolean> isWorldStored(String var1);

    public void getWorld(String var1, File var2, Consumer<Double> var3);
}

