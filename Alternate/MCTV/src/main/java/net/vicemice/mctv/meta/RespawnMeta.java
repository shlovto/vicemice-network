package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerEntityDestroy;
import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerEntityEquipment;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="RespawnMeta", opcode=11, adapter= RespawnMeta.RespawnMetaAdapter.class)
public class RespawnMeta
implements Meta {
    private UUID uuid;
    private String world;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeUUID(this.uuid, out);
        DataStreamer.writeString(this.world, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.uuid = DataStreamer.readUUID(in);
        if (version >= 3) {
            this.world = DataStreamer.readString(in);
        }
    }

    @Override
    public void execute(final Context context) {
        World world = context.getMasterPlayer().getWorld();
        if (this.world != null) {
            world = Bukkit.getWorld((String)this.world);
        }
        if (world == null) {
            System.out.println("Replay wanted to get entity in world '" + this.world + "' which is not loaded. Defaulting back to replay master world");
            world = context.getMasterPlayer().getWorld();
        }
        context.getPlayerManager().respawn(this.uuid);
        if (!context.getPlayerManager().getWorldName(this.uuid).equals((Object)world)) {
            // empty if block
        }
        WrapperPlayServerEntityDestroy entityDestroy = new WrapperPlayServerEntityDestroy();
        int entityId = context.getPlayerManager().getEntityIdByUUID(this.uuid, world);
        entityDestroy.setEntityIds(new int[]{entityId});
        ItemStack airBlock = new ItemStack(Material.AIR);
        final WrapperPlayServerEntityEquipment packet = new WrapperPlayServerEntityEquipment();
        context.getPlayerManager().setItem(this.uuid, 0, airBlock);
        packet.setEntityID(entityId);
        packet.setItem(airBlock.clone());
        packet.setSlot(0);
        final WrapperPlayServerEntityEquipment packet1 = new WrapperPlayServerEntityEquipment();
        context.getPlayerManager().setItem(this.uuid, 1, airBlock);
        packet.setEntityID(entityId);
        packet.setItem(airBlock.clone());
        packet.setSlot(1);
        final WrapperPlayServerEntityEquipment packet2 = new WrapperPlayServerEntityEquipment();
        context.getPlayerManager().setItem(this.uuid, 2, airBlock);
        packet.setEntityID(entityId);
        packet.setItem(airBlock.clone());
        packet.setSlot(2);
        final WrapperPlayServerEntityEquipment packet3 = new WrapperPlayServerEntityEquipment();
        context.getPlayerManager().setItem(this.uuid, 3, airBlock);
        packet.setEntityID(entityId);
        packet.setItem(airBlock.clone());
        packet.setSlot(3);
        final WrapperPlayServerEntityEquipment packet4 = new WrapperPlayServerEntityEquipment();
        context.getPlayerManager().setItem(this.uuid, 4, airBlock);
        packet.setEntityID(entityId);
        packet.setItem(airBlock.clone());
        packet.setSlot(4);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!player.getWorld().equals((Object)world)) continue;
            entityDestroy.sendPacket(player);
        }
        final World finalWorld = world;
        Bukkit.getScheduler().runTaskLater((Plugin)MCTV.getInstance(), new Runnable(){

            @Override
            public void run() {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    if (!player.getWorld().equals((Object)finalWorld) || !context.getPlayerManager().getEntityIdByUUID().containsKey(RespawnMeta.this.uuid)) continue;
                    context.getPlayerManager().spawnPlayerFor(RespawnMeta.this.uuid, player);
                    packet.sendPacket(player);
                    packet1.sendPacket(player);
                    packet2.sendPacket(player);
                    packet3.sendPacket(player);
                    packet4.sendPacket(player);
                }
            }
        }, 2L);
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public RespawnMeta() {
    }

    @ConstructorProperties(value={"uuid", "world"})
    public RespawnMeta(UUID uuid, String world) {
        this.uuid = uuid;
        this.world = world;
    }

    public String toString() {
        return "RespawnMeta(uuid=" + this.uuid + ", world=" + this.world + ")";
    }

    public static class RespawnMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;

        public RespawnMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.RESPAWN});
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }

        public void onPacketSending(PacketEvent event) {
            RespawnMeta respawnMeta = new RespawnMeta(event.getPlayer().getUniqueId(), event.getPlayer().getWorld().getName());
            this.replayFileWriter.addMeta(respawnMeta);
        }

        public void onPacketReceiving(PacketEvent event) {
        }
    }
}

