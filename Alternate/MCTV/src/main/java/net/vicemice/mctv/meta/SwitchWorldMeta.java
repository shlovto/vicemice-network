package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

@MetaInfo(name="SwitchWorldMeta", opcode=16, adapter= SwitchWorldMeta.SwitchWorldMetaAdapter.class)
public class SwitchWorldMeta
implements Meta {
    private String fromWorld;
    private String toWorld;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeString(this.toWorld, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.toWorld = DataStreamer.readString(in);
    }

    @Override
    public void execute(Context context) {
        this.fromWorld = context.getMasterPlayer().getWorld().getName();
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.teleport(Bukkit.getWorld((String)this.toWorld).getSpawnLocation());
        }
    }

    @Override
    public void rollback(Context context) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.teleport(Bukkit.getWorld((String)this.fromWorld).getSpawnLocation());
        }
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public SwitchWorldMeta() {
    }

    @ConstructorProperties(value={"fromWorld", "toWorld"})
    public SwitchWorldMeta(String fromWorld, String toWorld) {
        this.fromWorld = fromWorld;
        this.toWorld = toWorld;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof SwitchWorldMeta)) {
            return false;
        }
        SwitchWorldMeta other = (SwitchWorldMeta)o;
        if (!other.canEqual(this)) {
            return false;
        }
        String this$fromWorld = this.fromWorld;
        String other$fromWorld = other.fromWorld;
        if (this$fromWorld == null ? other$fromWorld != null : !this$fromWorld.equals(other$fromWorld)) {
            return false;
        }
        String this$toWorld = this.toWorld;
        String other$toWorld = other.toWorld;
        return !(this$toWorld == null ? other$toWorld != null : !this$toWorld.equals(other$toWorld));
    }

    protected boolean canEqual(Object other) {
        return other instanceof SwitchWorldMeta;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        String $fromWorld = this.fromWorld;
        result = result * 59 + ($fromWorld == null ? 43 : $fromWorld.hashCode());
        String $toWorld = this.toWorld;
        result = result * 59 + ($toWorld == null ? 43 : $toWorld.hashCode());
        return result;
    }

    public static class SwitchWorldMetaAdapter
    implements MetaAdapter,
    Listener {
        public SwitchWorldMetaAdapter(ReplayFileWriter replayFileWriter) {
        }

        @Override
        public void start() {
        }

        @Override
        public void stop() {
        }
    }
}

