package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerEntityMetadata;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import net.vicemice.hector.api.protocol.wrappers.WrappedChunkCoordinate;
import net.vicemice.hector.api.protocol.wrappers.WrappedWatchableObject;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="EntityMetadataMeta", opcode=13, adapter= EntityMetadataMeta.EntityStatusMetaAdapter.class)
public class EntityMetadataMeta
implements Meta {
    private UUID uuid;
    private Map<Integer, Object> metaData;
    private String world;
    private Map<Integer, Object> oldMetaData;
    private int eId;
    private boolean isMob = false;

    public EntityMetadataMeta(UUID uuid, Map<Integer, Object> metaData, String world) {
        this.uuid = uuid;
        this.metaData = metaData;
        this.world = world;
    }

    public EntityMetadataMeta(int entityId, Map<Integer, Object> metaData, String world) {
        this.isMob = true;
        this.eId = entityId;
        this.metaData = metaData;
        this.world = world;
    }

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        out.writeBoolean(this.isMob);
        if (this.isMob) {
            out.writeInt(this.eId);
        } else {
            DataStreamer.writeUUID(this.uuid, out);
        }
        for (Map.Entry<Integer, Object> integerObjectEntry : this.metaData.entrySet()) {
            out.writeByte(1);
            DataStreamer.writeVarInt(integerObjectEntry.getKey(), out);
            if (integerObjectEntry.getValue() instanceof Byte) {
                out.writeByte(0);
                out.write(((Byte)integerObjectEntry.getValue()).byteValue());
                continue;
            }
            if (integerObjectEntry.getValue() instanceof Short) {
                out.writeByte(1);
                out.writeShort(((Short)integerObjectEntry.getValue()).shortValue());
                continue;
            }
            if (integerObjectEntry.getValue() instanceof Integer) {
                out.writeByte(2);
                DataStreamer.writeVarInt((Integer)integerObjectEntry.getValue(), out);
                continue;
            }
            if (integerObjectEntry.getValue() instanceof Float) {
                out.writeByte(3);
                out.writeFloat(((Float)integerObjectEntry.getValue()).floatValue());
                continue;
            }
            if (integerObjectEntry.getValue() instanceof String) {
                out.writeByte(4);
                DataStreamer.writeString((String)integerObjectEntry.getValue(), out);
                continue;
            }
            if (integerObjectEntry.getValue() instanceof ItemStack) {
                out.writeByte(5);
                DataStreamer.writeItemStack((ItemStack)integerObjectEntry.getValue(), out);
                continue;
            }
            if (integerObjectEntry.getValue() instanceof WrappedChunkCoordinate) {
                out.writeByte(6);
                WrappedChunkCoordinate chunkCoordinate = (WrappedChunkCoordinate)integerObjectEntry.getValue();
                DataStreamer.writeVarInt(chunkCoordinate.getX(), out);
                DataStreamer.writeVarInt(chunkCoordinate.getY(), out);
                DataStreamer.writeVarInt(chunkCoordinate.getZ(), out);
                continue;
            }
            if (!(integerObjectEntry.getValue() instanceof Boolean)) continue;
            out.writeByte(7);
            out.writeBoolean((Boolean)integerObjectEntry.getValue());
        }
        out.writeByte(0);
        DataStreamer.writeString(this.world, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        if (version >= 15) {
            this.isMob = in.readBoolean();
            if (this.isMob) {
                this.eId = in.readInt();
            } else {
                this.uuid = DataStreamer.readUUID(in);
            }
        } else {
            this.uuid = DataStreamer.readUUID(in);
        }
        this.metaData = new HashMap<Integer, Object>();
        block10 : while (in.readByte() == 1) {
            int key = DataStreamer.readVarInt(in);
            in.mark(2);
            byte type = in.readByte();
            if ((key == 3 || key == 4 || key == 5 || key == 9) && type == 1) {
                in.reset();
                continue;
            }
            switch (type) {
                case 0: {
                    this.metaData.put(key, in.readByte());
                    continue block10;
                }
                case 1: {
                    this.metaData.put(key, in.readShort());
                    continue block10;
                }
                case 2: {
                    this.metaData.put(key, DataStreamer.readVarInt(in));
                    continue block10;
                }
                case 3: {
                    this.metaData.put(key, Float.valueOf(in.readFloat()));
                    continue block10;
                }
                case 4: {
                    this.metaData.put(key, DataStreamer.readString(in));
                    continue block10;
                }
                case 5: {
                    this.metaData.put(key, (Object)DataStreamer.readItemStack(in));
                    continue block10;
                }
                case 6: {
                    WrappedChunkCoordinate chunkCoordinate = new WrappedChunkCoordinate();
                    chunkCoordinate.setX(DataStreamer.readVarInt(in));
                    chunkCoordinate.setY(DataStreamer.readVarInt(in));
                    chunkCoordinate.setZ(DataStreamer.readVarInt(in));
                    this.metaData.put(key, (Object)chunkCoordinate);
                    continue block10;
                }
                case 7: {
                    this.metaData.put(key, in.readBoolean());
                }
            }
            in.reset();
        }
        if (version >= 3) {
            this.world = DataStreamer.readString(in);
        }
    }

    @Override
    public void execute(Context context) {
        World world = context.getMasterPlayer().getWorld();
        if (this.world != null) {
            world = Bukkit.getWorld((String)this.world);
        }
        int entityId = this.isMob ? this.eId : context.getPlayerManager().getEntityIdByUUID(this.uuid, world);
        WrapperPlayServerEntityMetadata packet = new WrapperPlayServerEntityMetadata();
        this.oldMetaData = new HashMap<Integer, Object>();
        ArrayList<WrappedWatchableObject> wrappedWatchableObjects = new ArrayList<WrappedWatchableObject>();
        for (Map.Entry<Integer, Object> integerObjectEntry : this.metaData.entrySet()) {
            if (integerObjectEntry.getValue() == null) continue;
            wrappedWatchableObjects.add(new WrappedWatchableObject(integerObjectEntry.getKey().intValue(), integerObjectEntry.getValue()));
            if (this.isMob) {
                this.oldMetaData.put(integerObjectEntry.getKey(), context.getEntityManager().getMetadata(entityId, integerObjectEntry.getKey()));
                context.getEntityManager().setMetadata(entityId, integerObjectEntry.getKey(), integerObjectEntry.getValue());
                continue;
            }
            this.oldMetaData.put(integerObjectEntry.getKey(), context.getPlayerManager().getMetadata(entityId, integerObjectEntry.getKey()));
            context.getPlayerManager().setMetadata(entityId, integerObjectEntry.getKey(), integerObjectEntry.getValue());
        }
        packet.setEntityID(entityId);
        packet.setMetadata(wrappedWatchableObjects);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!player.getWorld().equals((Object)world)) continue;
            packet.sendPacket(player);
        }
    }

    @Override
    public void rollback(Context context) {
        World world = context.getMasterPlayer().getWorld();
        if (this.world != null) {
            world = Bukkit.getWorld((String)this.world);
        }
        int entityId = this.isMob ? this.eId : context.getPlayerManager().getEntityIdByUUID(this.uuid, world);
        WrapperPlayServerEntityMetadata packet = new WrapperPlayServerEntityMetadata();
        ArrayList<WrappedWatchableObject> wrappedWatchableObjects = new ArrayList<WrappedWatchableObject>();
        for (Map.Entry<Integer, Object> integerObjectEntry : this.oldMetaData.entrySet()) {
            if (integerObjectEntry.getValue() == null) continue;
            wrappedWatchableObjects.add(new WrappedWatchableObject(integerObjectEntry.getKey().intValue(), integerObjectEntry.getValue()));
        }
        packet.setEntityID(entityId);
        packet.setMetadata(wrappedWatchableObjects);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!player.getWorld().equals((Object)world)) continue;
            packet.sendPacket(player);
        }
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public EntityMetadataMeta() {
    }

    public String toString() {
        return "EntityMetadataMeta(uuid=" + this.uuid + ", metaData=" + this.metaData + ", world=" + this.world + ", oldMetaData=" + this.oldMetaData + ", eId=" + this.eId + ", isMob=" + this.isMob + ")";
    }

    public static class EntityStatusMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;
        private Map<UUID, Map<Integer, Object>> metaData = new HashMap<UUID, Map<Integer, Object>>();

        public EntityStatusMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.ENTITY_METADATA});
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }

        public void onPacketSending(PacketEvent event) {
            WrapperPlayServerEntityMetadata packet = new WrapperPlayServerEntityMetadata(event.getPacket());
            Entity entity = packet.getEntity(event);
            UUID uuid = entity.getUniqueId();
            if (uuid != null && entity instanceof Player || entity instanceof Horse) {
                HashMap<Integer, Object> current = new HashMap<Integer, Object>();
                for (WrappedWatchableObject watchableObject : packet.getMetadata()) {
                    if (watchableObject.getIndex() != 16 && watchableObject.getIndex() != 22 && entity instanceof Horse) continue;
                    current.put(watchableObject.getIndex(), watchableObject.getValue());
                }
                if (!current.equals(this.metaData.get(uuid))) {
                    this.metaData.put(uuid, current);
                    EntityMetadataMeta entityMetadataMeta = packet.getEntity(event) instanceof Player ? new EntityMetadataMeta(uuid, current, Bukkit.getPlayer((UUID)uuid).getWorld().getName()) : new EntityMetadataMeta(packet.getEntityID(), current, event.getPlayer().getWorld().getName());
                    this.replayFileWriter.addMeta(entityMetadataMeta);
                }
            }
        }

        public void onPacketReceiving(PacketEvent event) {
        }
    }
}

