package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerChat;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import net.vicemice.hector.api.protocol.wrappers.WrappedChatComponent;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="ChatMeta", opcode=4, adapter= ChatMeta.ChatMetaAdapter.class)
public class ChatMeta implements Meta {
    private String message;
    private byte position;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeString(this.message, out);
        out.writeByte(this.position);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.message = DataStreamer.readString(in);
        if (version >= 6) {
            this.position = in.readByte();
        }
    }

    @Override
    public void execute(Context context) {
        WrapperPlayServerChat packet = new WrapperPlayServerChat();
        packet.setPosition(this.position);
        packet.setMessage(WrappedChatComponent.fromJson((String)this.message));
        for (Player player : Bukkit.getOnlinePlayers()) {
            packet.sendPacket(player);
        }
        if (this.position == 2) {
            context.setCurrentActionBarMessage(packet);
        }
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public ChatMeta() {
    }

    @ConstructorProperties(value={"message", "position"})
    public ChatMeta(String message, byte position) {
        this.message = message;
        this.position = position;
    }

    public static class ChatMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;

        public ChatMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.CHAT});
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }

        public void onPacketSending(PacketEvent event) {
            WrapperPlayServerChat packet = new WrapperPlayServerChat(event.getPacket());
            if (packet.getPosition() == 2 && event.getPlayer().getUniqueId().equals(this.replayFileWriter.getContext().getScoreboard().getTargetPlayer())) {
                ChatMeta meta = new ChatMeta(packet.getMessage().getJson(), packet.getPosition());
                this.replayFileWriter.addMeta(meta);
            }
        }

        public void onPacketReceiving(PacketEvent event) {
        }
    }
}

