package net.vicemice.mctv.io;

import com.google.common.base.Charsets;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class DataStreamer {
    public static void writeString(String s, DataOutputStream dos) {
        DataStreamer.writeString(s, Charsets.UTF_8, dos);
    }

    public static void writeString(String s, Charset charset, DataOutputStream dos) {
        try {
            if (s == null) {
                s = "";
            }
            byte[] b = s.getBytes(charset);
            DataStreamer.writeVarInt(b.length, dos);
            dos.write(b);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readString(DataInputStream dis) {
        return DataStreamer.readString(Charsets.UTF_8, dis);
    }

    public static String readString(Charset charset, DataInputStream dis) {
        try {
            int len = DataStreamer.readVarInt(dis);
            byte[] b = new byte[len];
            dis.readFully(b);
            String s = new String(b, charset);
            return s.equals("") ? null : s;
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void writeVarInt(int value, DataOutputStream dos) {
        try {
            while ((long)(value & 0xFFFFFF80) != 0L) {
                dos.writeByte(value & 0x7F | 0x80);
                value >>>= 7;
            }
            dos.writeByte(value & 0x7F);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int readVarInt(DataInputStream dis) {
        try {
            int b;
            int value = 0;
            int i = 0;
            while (((b = dis.read()) & 0x80) != 0) {
                value |= (b & 0x7F) << i;
                if ((i += 7) <= 35) continue;
                throw new IllegalArgumentException("Variable length quantity is too long");
            }
            return value | b << i;
        }
        catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static long readVarLong(DataInputStream dis) {
        try {
            byte b0;
            long i = 0L;
            int j = 0;
            do {
                b0 = dis.readByte();
                i |= (long)(b0 & 0x7F) << j++ * 7;
                if (j <= 10) continue;
                throw new RuntimeException("VarLong too big");
            } while ((b0 & 0x80) == 128);
            return i;
        }
        catch (IOException e) {
            e.printStackTrace();
            return 0L;
        }
    }

    public static void writeVarLong(long i, DataOutputStream dos) {
        try {
            while ((i & 0xFFFFFFFFFFFFFF80L) != 0L) {
                dos.writeByte((int)(i & 0x7FL) | 0x80);
                i >>>= 7;
            }
            dos.writeByte((int)i);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeUUID(UUID uuid, DataOutputStream dos) {
        try {
            if (uuid == null) {
                dos.writeLong(0L);
                return;
            }
            dos.writeLong(uuid.getMostSignificantBits());
            dos.writeLong(uuid.getLeastSignificantBits());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static UUID readUUID(DataInputStream dis) {
        try {
            long most = dis.readLong();
            if (most == 0L) {
                return null;
            }
            return new UUID(most, dis.readLong());
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ItemStack readItemStack(DataInputStream dis) {
        try {
            int type = DataStreamer.readVarInt(dis);
            int amount = DataStreamer.readVarInt(dis);
            int damage = DataStreamer.readVarInt(dis);
            ItemStack itemStack = new ItemStack(Material.getMaterial((int)type), amount, (short)damage);
            int encno = DataStreamer.readVarInt(dis);
            for (int i = 0; i < encno; ++i) {
                int enctype = DataStreamer.readVarInt(dis);
                int enclevel = DataStreamer.readVarInt(dis);
                if (Bukkit.getServer() == null) continue;
                itemStack.addUnsafeEnchantment(Enchantment.getById((int)enctype), enclevel);
            }
            if (Bukkit.getServer() == null) {
                if (itemStack.getType().toString().startsWith("LEATHER_")) {
                    DataStreamer.readVarInt(dis);
                }
            } else {
                ItemMeta meta = itemStack.getItemMeta();
                if (meta instanceof LeatherArmorMeta) {
                    LeatherArmorMeta leatherMeta = (LeatherArmorMeta)meta;
                    int rgb = DataStreamer.readVarInt(dis);
                    leatherMeta.setColor(Color.fromRGB((int)rgb));
                }
                itemStack.setItemMeta(meta);
            }
            return itemStack;
        }
        catch (IllegalArgumentException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void writeItemStack(ItemStack itemStack, DataOutputStream dos) {
        DataStreamer.writeVarInt(itemStack.getType().getId(), dos);
        DataStreamer.writeVarInt(itemStack.getAmount(), dos);
        DataStreamer.writeVarInt(itemStack.getDurability(), dos);
        Map<Enchantment, Integer> enchantments = itemStack.getEnchantments();
        DataStreamer.writeVarInt(enchantments.size(), dos);
        for (Map.Entry enchantment : enchantments.entrySet()) {
            DataStreamer.writeVarInt(((Enchantment)enchantment.getKey()).getId(), dos);
            DataStreamer.writeVarInt((Integer)enchantment.getValue(), dos);
        }
        ItemMeta meta = itemStack.getItemMeta();
        if (meta instanceof LeatherArmorMeta) {
            LeatherArmorMeta leatherMeta = (LeatherArmorMeta)meta;
            DataStreamer.writeVarInt(leatherMeta.getColor().asRGB(), dos);
        }
    }

    public static String readString(DataInputStream dis, int maxLength) {
        try {
            int len = DataStreamer.readVarInt(dis);
            if (len < 0 || len > maxLength) {
                return null;
            }
            byte[] b = new byte[len];
            dis.readFully(b);
            String s = new String(b, Charsets.UTF_8);
            return s.equals("") ? null : s;
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

