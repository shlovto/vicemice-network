package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerEntityEffect;
import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerEntityMetadata;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import net.vicemice.hector.api.protocol.wrappers.WrappedWatchableObject;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import net.minecraft.server.v1_8_R3.MobEffect;
import net.minecraft.server.v1_8_R3.PotionBrewer;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="EntityEffectMeta", opcode=29, adapter= EntityEffectMeta.EntityEffectMetaAdapter.class)
public class EntityEffectMeta
implements Meta {
    private UUID uuid;
    private byte potionEffect;
    private int duration;
    private boolean fixedBug;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeUUID(this.uuid, out);
        out.writeByte(this.potionEffect);
        DataStreamer.writeVarInt(this.duration, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.uuid = DataStreamer.readUUID(in);
        this.potionEffect = in.readByte();
        this.duration = DataStreamer.readVarInt(in);
    }

    @Override
    public void execute(Context context) {
        this.sendWithDuration(context, this.duration);
    }

    private void sendWithDuration(Context context, final int duration) {
        block5: {
            int entityId;
            World world;
            block4: {
                String wrld = context.getPlayerManager().getWorldName(this.uuid);
                if (wrld == null) {
                    return;
                }
                world = Bukkit.getWorld((String)wrld);
                entityId = context.getPlayerManager().getEntityIdByUUID(this.uuid, world);
                WrapperPlayServerEntityEffect wrapperPlayServerEntityEffect = new WrapperPlayServerEntityEffect();
                wrapperPlayServerEntityEffect.setEntityID(entityId);
                wrapperPlayServerEntityEffect.setDuration(duration);
                wrapperPlayServerEntityEffect.setAmplifier((byte)0);
                wrapperPlayServerEntityEffect.setEffectID(this.potionEffect);
                wrapperPlayServerEntityEffect.setHideParticles(false);
                for (Player player : Bukkit.getOnlinePlayers()) {
                    if (!player.getWorld().equals((Object)world)) continue;
                    wrapperPlayServerEntityEffect.sendPacket(player);
                }
                if (!this.fixedBug) break block4;
                context.getPlayerManager().setMetadata(entityId, 7, 0);
                WrapperPlayServerEntityMetadata metadata = new WrapperPlayServerEntityMetadata();
                ArrayList<WrappedWatchableObject> wrappedWatchableObjects = new ArrayList<WrappedWatchableObject>();
                wrappedWatchableObjects.add(new WrappedWatchableObject(7, (Object)0));
                metadata.setMetadata(wrappedWatchableObjects);
                metadata.setEntityID(entityId);
                this.fixedBug = false;
                for (Player player : Bukkit.getOnlinePlayers()) {
                    if (!player.getWorld().equals((Object)world)) continue;
                    metadata.sendPacket(player);
                }
                break block5;
            }
            if (context.getPlayerManager().getMetadata(entityId, 7) != null) break block5;
            int color = PotionBrewer.a((Collection)new ArrayList<MobEffect>(){
                {
                    this.add(new MobEffect((int)EntityEffectMeta.this.potionEffect, duration, 0, false, true));
                }
            });
            context.getPlayerManager().setMetadata(entityId, 7, color);
            WrapperPlayServerEntityMetadata metadata = new WrapperPlayServerEntityMetadata();
            ArrayList<WrappedWatchableObject> wrappedWatchableObjects = new ArrayList<WrappedWatchableObject>();
            wrappedWatchableObjects.add(new WrappedWatchableObject(7, (Object)color));
            metadata.setMetadata(wrappedWatchableObjects);
            metadata.setEntityID(entityId);
            this.fixedBug = true;
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (!player.getWorld().equals((Object)world)) continue;
                metadata.sendPacket(player);
            }
        }
    }

    @Override
    public void rollback(Context context) {
        this.sendWithDuration(context, 0);
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public EntityEffectMeta() {
    }

    @ConstructorProperties(value={"uuid", "potionEffect", "duration", "fixedBug"})
    public EntityEffectMeta(UUID uuid, byte potionEffect, int duration, boolean fixedBug) {
        this.uuid = uuid;
        this.potionEffect = potionEffect;
        this.duration = duration;
        this.fixedBug = fixedBug;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof EntityEffectMeta)) {
            return false;
        }
        EntityEffectMeta other = (EntityEffectMeta)o;
        if (!other.canEqual(this)) {
            return false;
        }
        UUID this$uuid = this.uuid;
        UUID other$uuid = other.uuid;
        if (this$uuid == null ? other$uuid != null : !((Object)this$uuid).equals(other$uuid)) {
            return false;
        }
        if (this.potionEffect != other.potionEffect) {
            return false;
        }
        if (this.duration != other.duration) {
            return false;
        }
        return this.fixedBug == other.fixedBug;
    }

    protected boolean canEqual(Object other) {
        return other instanceof EntityEffectMeta;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        UUID $uuid = this.uuid;
        result = result * 59 + ($uuid == null ? 43 : ((Object)$uuid).hashCode());
        result = result * 59 + this.potionEffect;
        result = result * 59 + this.duration;
        result = result * 59 + (this.fixedBug ? 79 : 97);
        return result;
    }

    public static class EntityEffectMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;

        public EntityEffectMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.ENTITY_EFFECT});
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }

        public void onPacketSending(PacketEvent event) {
            WrapperPlayServerEntityEffect packet = new WrapperPlayServerEntityEffect(event.getPacket());
            if (packet.getEntityID() == event.getPlayer().getEntityId()) {
                this.replayFileWriter.addMeta(new EntityEffectMeta(event.getPlayer().getUniqueId(), packet.getEffectID(), packet.getDuration(), false));
            }
        }

        public void onPacketReceiving(PacketEvent event) {
        }
    }
}

