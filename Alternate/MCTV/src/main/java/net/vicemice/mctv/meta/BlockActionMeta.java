package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import net.vicemice.hector.api.protocol.wrappers.BlockPosition;
import com.google.common.base.Charsets;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerBlockAction;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="BlockActionMeta", opcode=31, adapter= BlockActionMeta.BlockActionMetaAdapter.class)
public class BlockActionMeta
implements Meta {
    private int x;
    private int y;
    private int z;
    private short arg1;
    private short arg2;
    private Material material;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeVarInt(this.x, out);
        DataStreamer.writeVarInt(this.y, out);
        DataStreamer.writeVarInt(this.z, out);
        out.writeShort(this.arg1);
        out.writeShort(this.arg2);
        DataStreamer.writeString(this.material.name(), Charsets.UTF_8, out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.x = DataStreamer.readVarInt(in);
        this.y = DataStreamer.readVarInt(in);
        this.z = DataStreamer.readVarInt(in);
        this.arg1 = in.readShort();
        this.arg2 = in.readShort();
        String materialName = DataStreamer.readString(Charsets.UTF_8, in);
        try {
            this.material = Material.valueOf((String)materialName);
        }
        catch (IllegalArgumentException ignored) {
            this.material = Material.AIR;
            MCTV.getInstance().getLogger().warning("No Material found for " + materialName);
        }
    }

    @Override
    public void execute(Context context) {
        WrapperPlayServerBlockAction blockAction = new WrapperPlayServerBlockAction();
        blockAction.setBlockType(this.material);
        blockAction.setLocation(new BlockPosition(this.x, this.y, this.z));
        blockAction.setByte1((int)this.arg1);
        blockAction.setByte2((int)this.arg2);
        for (Player player : Bukkit.getOnlinePlayers()) {
            blockAction.sendPacket(player);
        }
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return true;
    }

    @ConstructorProperties(value={"x", "y", "z", "arg1", "arg2", "material"})
    public BlockActionMeta(int x, int y, int z, short arg1, short arg2, Material material) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.arg1 = arg1;
        this.arg2 = arg2;
        this.material = material;
    }

    public BlockActionMeta() {
    }

    public String toString() {
        return "BlockActionMeta(x=" + this.x + ", y=" + this.y + ", z=" + this.z + ", arg1=" + this.arg1 + ", arg2=" + this.arg2 + ", material=" + (Object)this.material + ")";
    }

    public static class BlockActionMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private final ReplayFileWriter replayFileWriter;
        private final Set<PacketSentEntry> done = ConcurrentHashMap.newKeySet();

        public BlockActionMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.BLOCK_ACTION});
            this.replayFileWriter = replayFileWriter;
        }

        public void onPacketSending(PacketEvent event) {
            WrapperPlayServerBlockAction packet = new WrapperPlayServerBlockAction(event.getPacket());
            PacketSentEntry packetSentEntry = new PacketSentEntry(packet.getLocation(), packet.getByte1(), packet.getByte2(), packet.getBlockType());
            if (this.done.add(packetSentEntry)) {
                this.replayFileWriter.addMeta(new BlockActionMeta(packet.getLocation().getX(), packet.getLocation().getY(), packet.getLocation().getZ(), (short)packet.getByte1(), (short)packet.getByte2(), packet.getBlockType()));
            }
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
            Bukkit.getScheduler().runTaskTimer((Plugin)MCTV.getInstance(), new Runnable(){

                @Override
                public void run() {
                    done.clear();
                }
            }, 1L, 1L);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }

        private final class PacketSentEntry {
            private BlockPosition blockPosition;
            private int byte1;
            private int byte2;
            private Material material;

            public boolean equals(Object o) {
                if (o == this) {
                    return true;
                }
                if (!(o instanceof PacketSentEntry)) {
                    return false;
                }
                PacketSentEntry other = (PacketSentEntry)o;
                BlockPosition this$blockPosition = this.blockPosition;
                BlockPosition other$blockPosition = other.blockPosition;
                if (this$blockPosition == null ? other$blockPosition != null : !this$blockPosition.equals((Object)other$blockPosition)) {
                    return false;
                }
                if (this.byte1 != other.byte1) {
                    return false;
                }
                if (this.byte2 != other.byte2) {
                    return false;
                }
                Material this$material = this.material;
                Material other$material = other.material;
                return !(this$material == null ? other$material != null : !this$material.equals((Object)other$material));
            }

            public int hashCode() {
                int PRIME = 59;
                int result = 1;
                BlockPosition $blockPosition = this.blockPosition;
                result = result * 59 + ($blockPosition == null ? 43 : $blockPosition.hashCode());
                result = result * 59 + this.byte1;
                result = result * 59 + this.byte2;
                Material $material = this.material;
                result = result * 59 + ($material == null ? 43 : $material.hashCode());
                return result;
            }

            @ConstructorProperties(value={"blockPosition", "byte1", "byte2", "material"})
            public PacketSentEntry(BlockPosition blockPosition, int byte1, int byte2, Material material) {
                this.blockPosition = blockPosition;
                this.byte1 = byte1;
                this.byte2 = byte2;
                this.material = material;
            }
        }
    }
}

