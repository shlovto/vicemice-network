package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="BlockBreakMeta", opcode=12, adapter= BlockBreakMeta.BlockBreakMetaAdapter.class)
public class BlockBreakMeta
implements Meta {
    private Location location;
    private String worldName;
    private Material oldMaterial;
    private byte oldData;
    private Location otherHalf;
    private byte otherHalfData;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeString(this.location.getWorld().getName(), out);
        DataStreamer.writeVarInt(this.location.getBlockX(), out);
        DataStreamer.writeVarInt(this.location.getBlockY(), out);
        DataStreamer.writeVarInt(this.location.getBlockZ(), out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.worldName = DataStreamer.readString(in);
        this.location = new Location(null, (double)DataStreamer.readVarInt(in), (double)DataStreamer.readVarInt(in), (double)DataStreamer.readVarInt(in));
    }

    @Override
    public void execute(Context context) {
        World world = Bukkit.getServer() != null ? Bukkit.getWorld((String)this.worldName) : null;
        this.location.setWorld(world);
        this.oldMaterial = this.location.getBlock().getType();
        this.oldData = this.location.getBlock().getData();
        if (this.oldMaterial == Material.ICE) {
            this.location.getBlock().setType(Material.WATER);
        } else if (this.oldMaterial == Material.BED_BLOCK) {
            BlockFace[] blockFaces;
            for (BlockFace blockFace : blockFaces = new BlockFace[]{BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST}) {
                if (this.location.getBlock().getRelative(blockFace).getType() != this.oldMaterial) continue;
                this.otherHalf = this.location.getBlock().getRelative(blockFace).getLocation();
                this.otherHalfData = this.location.getBlock().getRelative(blockFace).getData();
                break;
            }
            this.otherHalf.getBlock().setType(Material.AIR);
            this.location.getBlock().setType(Material.AIR);
        } else {
            this.location.getBlock().setType(Material.AIR);
        }
    }

    @Override
    public void rollback(Context context) {
        World world = Bukkit.getServer() != null ? Bukkit.getWorld((String)this.worldName) : null;
        this.location.setWorld(world);
        this.location.getBlock().setType(this.oldMaterial);
        this.location.getBlock().setData(this.oldData);
        if (this.otherHalf != null) {
            this.otherHalf.getBlock().setType(this.oldMaterial);
            this.otherHalf.getBlock().setData(this.otherHalfData);
        }
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public BlockBreakMeta() {
    }

    @ConstructorProperties(value={"location", "worldName", "oldMaterial", "oldData", "otherHalf", "otherHalfData"})
    public BlockBreakMeta(Location location, String worldName, Material oldMaterial, byte oldData, Location otherHalf, byte otherHalfData) {
        this.location = location;
        this.worldName = worldName;
        this.oldMaterial = oldMaterial;
        this.oldData = oldData;
        this.otherHalf = otherHalf;
        this.otherHalfData = otherHalfData;
    }

    public static class BlockBreakMetaAdapter
    implements MetaAdapter,
    Listener {
        private ReplayFileWriter replayFileWriter;

        public BlockBreakMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            Bukkit.getPluginManager().registerEvents((Listener)this, (Plugin)MCTV.getInstance());
        }

        @Override
        public void stop() {
            HandlerList.unregisterAll((Listener)this);
        }

        @EventHandler(priority=EventPriority.MONITOR)
        public void onBlockPlace(BlockBreakEvent event) {
            if (event.isCancelled()) {
                if (event.getBlock().getType() == Material.WEB && event.getPlayer().getItemInHand() != null && event.getPlayer().getItemInHand().getType().name().contains("_SWORD")) {
                    BlockBreakMeta blockPlaceMeta = new BlockBreakMeta(event.getBlock().getLocation(), null, null, (byte)0, null, (byte)0);
                    this.replayFileWriter.addMeta(blockPlaceMeta);
                }
                return;
            }
            BlockBreakMeta blockPlaceMeta = new BlockBreakMeta(event.getBlock().getLocation(), null, null, (byte)0, null, (byte)0);
            this.replayFileWriter.addMeta(blockPlaceMeta);
        }
    }
}

