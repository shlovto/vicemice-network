package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerWorldEvent;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import net.vicemice.hector.api.protocol.wrappers.BlockPosition;
import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

@MetaInfo(opcode=37, name="WorldEvent", adapter= WorldEventMeta.WorldEventMetaAdapter.class)
public class WorldEventMeta implements Meta {
    private int id;
    private int data;
    private BlockPosition position;
    private boolean disableRelativeVolume;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeVarInt(this.id, out);
        DataStreamer.writeVarInt(this.data, out);
        DataStreamer.writeVarInt(this.position.getX(), out);
        DataStreamer.writeVarInt(this.position.getY(), out);
        DataStreamer.writeVarInt(this.position.getZ(), out);
        out.writeBoolean(this.disableRelativeVolume);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.id = DataStreamer.readVarInt(in);
        this.data = DataStreamer.readVarInt(in);
        this.position = new BlockPosition(DataStreamer.readVarInt(in), DataStreamer.readVarInt(in), DataStreamer.readVarInt(in));
        this.disableRelativeVolume = in.readBoolean();
    }

    @Override
    public void execute(Context context) {
        WrapperPlayServerWorldEvent packet = new WrapperPlayServerWorldEvent();
        packet.setLocation(this.position);
        packet.setEffectId(this.id);
        packet.setData(this.data);
        packet.setDisableRelativeVolume(this.disableRelativeVolume);
        Player master = Bukkit.getPlayer((UUID)MCTV.getInstance().getApi().getContext().getMaster());
        if (master != null) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (!player.getWorld().equals((Object)master.getWorld())) continue;
                packet.sendPacket(player);
            }
        }
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return true;
    }

    @ConstructorProperties(value={"id", "data", "position", "disableRelativeVolume"})
    public WorldEventMeta(int id, int data, BlockPosition position, boolean disableRelativeVolume) {
        this.id = id;
        this.data = data;
        this.position = position;
        this.disableRelativeVolume = disableRelativeVolume;
    }

    public WorldEventMeta() {
    }

    public int getId() {
        return this.id;
    }

    public int getData() {
        return this.data;
    }

    public BlockPosition getPosition() {
        return this.position;
    }

    public boolean isDisableRelativeVolume() {
        return this.disableRelativeVolume;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setData(int data) {
        this.data = data;
    }

    public void setPosition(BlockPosition position) {
        this.position = position;
    }

    public void setDisableRelativeVolume(boolean disableRelativeVolume) {
        this.disableRelativeVolume = disableRelativeVolume;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof WorldEventMeta)) {
            return false;
        }
        WorldEventMeta other = (WorldEventMeta)o;
        if (!other.canEqual(this)) {
            return false;
        }
        if (this.getId() != other.getId()) {
            return false;
        }
        if (this.getData() != other.getData()) {
            return false;
        }
        BlockPosition this$position = this.getPosition();
        BlockPosition other$position = other.getPosition();
        if (this$position == null ? other$position != null : !this$position.equals((Object)other$position)) {
            return false;
        }
        return this.isDisableRelativeVolume() == other.isDisableRelativeVolume();
    }

    protected boolean canEqual(Object other) {
        return other instanceof WorldEventMeta;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        result = result * 59 + this.getId();
        result = result * 59 + this.getData();
        BlockPosition $position = this.getPosition();
        result = result * 59 + ($position == null ? 43 : $position.hashCode());
        result = result * 59 + (this.isDisableRelativeVolume() ? 79 : 97);
        return result;
    }

    public String toString() {
        return "WorldEventMeta(id=" + this.getId() + ", data=" + this.getData() + ", position=" + (Object)this.getPosition() + ", disableRelativeVolume=" + this.isDisableRelativeVolume() + ")";
    }

    public static class WorldEventMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private final ReplayFileWriter replayFileWriter;
        private final Set<WorldEventMeta> done = ConcurrentHashMap.newKeySet();

        public WorldEventMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.WORLD_EVENT});
            this.replayFileWriter = replayFileWriter;
        }

        public void onPacketSending(PacketEvent event) {
            WrapperPlayServerWorldEvent packet = new WrapperPlayServerWorldEvent(event.getPacket());
            WorldEventMeta worldEventMeta = new WorldEventMeta(packet.getEffectId(), packet.getData(), packet.getLocation(), packet.getDisableRelativeVolume());
            if (this.done.add(worldEventMeta)) {
                this.replayFileWriter.addMeta(worldEventMeta);
            }
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
            Bukkit.getScheduler().runTaskTimer((Plugin)MCTV.getInstance(), new Runnable(){

                @Override
                public void run() {
                    done.clear();
                }
            }, 1L, 1L);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }
    }
}

