package net.vicemice.mctv.listener;

import net.vicemice.mctv.MCTV;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.plugin.Plugin;

public class WeatherChangeListener implements Listener {

    @EventHandler
    public void onWeatherChange(final WeatherChangeEvent event) {
        Bukkit.getScheduler().runTaskLater(MCTV.getInstance(), () -> {
            event.getWorld().setThundering(false);
            event.getWorld().setStorm(false);
        }, 2L);
    }
}

