package net.vicemice.mctv.command;

import java.util.Objects;

import net.vicemice.hector.GoAPI;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.session.ReplaySession;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.entity.Player;

public class PromoteCommand extends Command {

    public PromoteCommand(MCTV mctv) {
        super("promote");
        this.mctv = mctv;
        ((CraftServer) mctv.getServer()).getCommandMap().register("promote", this);
    }

    private MCTV mctv;

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            return true;
        }
        Player player = (Player)commandSender;
        ReplaySession session = MCTV.getInstance().getReplaySession();
        if (!Objects.equals(session.getContext().getMaster(), player.getUniqueId())) {
            this.mctv.sendMessage(player, "command-to-not-master", new Object[0]);
            return true;
        }
        if (args.length < 1) {
            this.mctv.sendMessage(player, "command-promote-no-args", new Object[0]);
            return true;
        }
        Player toPromote = Bukkit.getPlayer((String)args[0]);
        if (toPromote == null) {
            this.mctv.sendMessage(player, "user-not-exist", GoAPI.getUserAPI().translate(player.getUniqueId(), "replay-prefix"));
            return true;
        }
        if (Objects.equals(session.getContext().getMaster(), toPromote.getUniqueId())) {
            this.mctv.sendMessage(player, "command-promote-already-leader", new Object[0]);
            return true;
        }
        this.mctv.getReplaySession().getContext().setMaster(toPromote.getUniqueId());
        this.mctv.getReplaySession().updateInventory(toPromote);
        this.mctv.getReplaySession().updateInventory(player);
        this.mctv.broadcastLocalized("command-promote-success", toPromote.getDisplayName());
        return true;
    }
}

