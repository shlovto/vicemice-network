package net.vicemice.mctv.listener;

import net.vicemice.hector.server.GoServer;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.data.VerboseDisplayType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        event.setJoinMessage(null);
        MCTV.getInstance().getReplaySession().addReplayUser(event.getPlayer());
        GoServer.getService().getPlayerManager().getPlayer(event.getPlayer().getUniqueId()).getSettingsPacket().addNewTempSettings("replay-acc-verbose", VerboseDisplayType.NONE);
        MCTV.getInstance().getReplaySession().getContext().getVerboseDisplayType(event.getPlayer().getUniqueId());
    }
}