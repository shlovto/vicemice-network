package net.vicemice.mctv.meta.api;

import com.google.common.reflect.ClassPath;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MetaRegistry {
    private static Map<Integer, Class<? extends Meta>> opcodes = new HashMap<Integer, Class<? extends Meta>>();

    public static Class<? extends Meta> lookup(int opcode) {
        return opcodes.get(opcode);
    }

    public static List<Class<? extends Meta>> getAll() {
        return Collections.unmodifiableList(new ArrayList<Class<? extends Meta>>(opcodes.values()));
    }

    static {
        try {
            for (ClassPath.ClassInfo classInfo : ClassPath.from(MetaRegistry.class.getClassLoader()).getTopLevelClasses("net.vicemice.mctv.meta")) {
                Class<?> metaClass = classInfo.load();
                int opCode = metaClass.getAnnotation(MetaInfo.class).opcode();
                opcodes.put(opCode, (Class<? extends Meta>) metaClass);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}

