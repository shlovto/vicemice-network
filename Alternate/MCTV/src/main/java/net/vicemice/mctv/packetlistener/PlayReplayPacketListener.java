package net.vicemice.mctv.packetlistener;

import io.netty.channel.Channel;
import net.vicemice.mctv.MCTV;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.replay.ViewReplayPacket;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

public class PlayReplayPacketListener extends PacketGetter {
    private BukkitTask shutdownTask;
    private BukkitTask oneTickTask;

    @Override
    public void receivePacket(PacketHolder packetHolder) {
        if (packetHolder.getType() == PacketType.VIEW_REPLAY) {
            ViewReplayPacket packet = (ViewReplayPacket) packetHolder.getValue();
            Player player = Bukkit.getPlayer(packet.getUniqueId());
            if (packet.getReplayEntry() == null) {
                MCTV.getInstance().sendMessage(player, "wrong-replay-id");
                Bukkit.getScheduler().runTask(MCTV.getInstance(), () -> player.kickPlayer("lobby"));
            }
            System.out.println("Playing replay " + packet.getReplayEntry().getGameId());
            MCTV.getInstance().getReplaySession().setGameId(packet.getReplayEntry().getGameId());
            MCTV.getInstance().getReplaySession().loadReplay(packet.getReplayEntry().getReplayUUID());
            MCTV.getInstance().getReplaySession().getContext().setSkipTime(packet.getReplayEntry().getReplayStartSeconds());
            MCTV.getInstance().getReplaySession().getContext().setTeleportToAfterSkipTime(player.getUniqueId());
            Bukkit.getScheduler().runTask(MCTV.getInstance(), () -> MCTV.getInstance().getReplaySession().play());
            if (this.shutdownTask != null) {
                this.shutdownTask.cancel();
                this.oneTickTask.cancel();
            }
            this.shutdownTask = Bukkit.getScheduler().runTaskLater(MCTV.getInstance(), Bukkit::shutdown, 200L);
            this.oneTickTask = Bukkit.getScheduler().runTaskTimer(MCTV.getInstance(), () -> {
                if (Bukkit.getOnlinePlayers().size() > 0) {
                    PlayReplayPacketListener.this.shutdownTask.cancel();
                    PlayReplayPacketListener.this.oneTickTask.cancel();
                }
            }, 1L, 1L);
        }
    }

    @Override
    public void channelActive(Channel var1) {

    }
}