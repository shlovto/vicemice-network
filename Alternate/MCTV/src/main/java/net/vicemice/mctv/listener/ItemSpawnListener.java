package net.vicemice.mctv.listener;

import net.vicemice.mctv.MCTV;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;

public class ItemSpawnListener
implements Listener {
    @EventHandler
    public void onItemSpawn(ItemSpawnEvent event) {
        if (!MCTV.getInstance().getApi().getContext().getEntityManager().containsItem(event.getEntity().getEntityId())) {
            event.setCancelled(true);
        }
    }
}

