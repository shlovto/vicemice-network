package net.vicemice.mctv.meta;

import java.beans.ConstructorProperties;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerEntityTeleport;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

@MetaInfo(name="MoveMeta", opcode=0, adapter= MoveMeta.MoveMetaAdapter.class)
public class MoveMeta implements Meta {
    private UUID uuid;
    private Location target;
    private String world;

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeUUID(this.uuid, out);
        out.writeDouble(this.target.getX());
        out.writeDouble(this.target.getY());
        out.writeDouble(this.target.getZ());
        out.writeFloat(this.target.getYaw());
        out.writeFloat(this.target.getPitch());
        DataStreamer.writeString(this.target.getWorld().getName(), out);
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.uuid = DataStreamer.readUUID(in);
        double x = in.readDouble();
        double y = in.readDouble();
        double z = in.readDouble();
        float yaw = in.readFloat();
        float pitch = in.readFloat();
        this.target = new Location(null, x, y, z, yaw, pitch);
        if (version >= 3) {
            this.world = DataStreamer.readString(in);
        }
    }

    @Override
    public void execute(Context context) {
        String worldName;
        if (this.uuid == null) {
            return;
        }
        context.getPlayerManager().updateLocation(this.uuid, this.target.getX(), this.target.getY(), this.target.getZ(), this.target.getYaw(), this.target.getPitch());
        World world = context.getMasterPlayer().getWorld();
        if (this.world != null) {
            world = Bukkit.getWorld((String)this.world);
        }
        if (world == null) {
            context.warn("Replay wanted to get entity in world '" + this.world + "' which is not loaded. Defaulting back to replay master world");
            world = context.getMasterPlayer().getWorld();
        }
        if ((worldName = context.getPlayerManager().getWorldName(this.uuid)) != null && !worldName.equals(world.getName())) {
            context.getPlayerManager().changeWorld(this.uuid, world);
        }
        WrapperPlayServerEntityTeleport packet = new WrapperPlayServerEntityTeleport();
        packet.setEntityID(context.getPlayerManager().getEntityIdByUUID(this.uuid, world));
        packet.setX(this.target.getX());
        packet.setY(this.target.getY());
        packet.setZ(this.target.getZ());
        packet.setYaw(this.target.getYaw());
        packet.setPitch(this.target.getPitch());
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!player.getWorld().equals((Object)world)) continue;
            packet.sendPacket(player);
        }
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public MoveMeta() {
    }

    @ConstructorProperties(value={"uuid", "target", "world"})
    public MoveMeta(UUID uuid, Location target, String world) {
        this.uuid = uuid;
        this.target = target;
        this.world = world;
    }

    public String toString() {
        return "MoveMeta(uuid=" + this.uuid + ", target=" + (Object)this.target + ", world=" + this.world + ")";
    }

    public static class MoveMetaAdapter
    implements MetaAdapter,
    Runnable {
        private ReplayFileWriter replayFileWriter;
        private BukkitTask task;

        public MoveMetaAdapter(ReplayFileWriter replayFileWriter) {
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            this.task = Bukkit.getScheduler().runTaskTimer((Plugin)MCTV.getInstance(), (Runnable)this, 0L, 1L);
        }

        @Override
        public void stop() {
            this.task.cancel();
        }

        @Override
        public void run() {
            for (Player player : Bukkit.getOnlinePlayers()) {
                MoveMeta meta = new MoveMeta(player.getUniqueId(), player.getLocation(), null);
                this.replayFileWriter.addMeta(meta);
            }
        }
    }
}

