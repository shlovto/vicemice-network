package net.vicemice.mctv.meta;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerEntityStatus;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import net.vicemice.mctv.MCTV;
import net.vicemice.mctv.io.DataStreamer;
import net.vicemice.mctv.io.writer.ReplayFileWriter;
import net.vicemice.mctv.meta.api.Meta;
import net.vicemice.mctv.meta.api.MetaAdapter;
import net.vicemice.mctv.meta.api.MetaInfo;
import net.vicemice.mctv.replay.Context;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

@MetaInfo(name="EntityStatusMeta", opcode=5, adapter= EntityStatusMeta.EntityStatusMetaAdapter.class)
public class EntityStatusMeta
implements Meta {
    private boolean isMob = false;
    private int entityId = -1;
    private UUID uuid;
    private int statusId;
    private String world;

    public EntityStatusMeta(int entityId, int statusId, String world) {
        this.entityId = entityId;
        this.isMob = true;
        this.statusId = statusId;
        this.world = world;
    }

    public EntityStatusMeta(UUID uuid, int statusId, String world) {
        this.uuid = uuid;
        this.statusId = statusId;
        this.world = world;
    }

    @Override
    public void writeMeta(DataOutputStream out) throws IOException {
        DataStreamer.writeUUID(this.uuid, out);
        DataStreamer.writeVarInt(this.statusId, out);
        DataStreamer.writeString(this.world, out);
        out.writeBoolean(this.isMob);
        if (this.isMob) {
            DataStreamer.writeVarInt(this.entityId, out);
        }
    }

    @Override
    public void readMeta(DataInputStream in, int version) throws IOException {
        this.uuid = DataStreamer.readUUID(in);
        this.statusId = DataStreamer.readVarInt(in);
        if (version >= 3) {
            this.world = DataStreamer.readString(in);
        }
        if (version >= 14) {
            this.isMob = in.readBoolean();
            if (this.isMob) {
                this.entityId = DataStreamer.readVarInt(in);
            }
        }
    }

    @Override
    public void execute(Context context) {
        World world = context.getMasterPlayer().getWorld();
        if (this.world != null) {
            world = Bukkit.getWorld((String)this.world);
        }
        if (world == null) {
            context.warn("Replay wanted to get entity in world '" + this.world + "' which is not loaded. Defaulting back to replay master world");
            world = context.getMasterPlayer().getWorld();
        }
        WrapperPlayServerEntityStatus packet = new WrapperPlayServerEntityStatus();
        if (this.isMob) {
            packet.setEntityID(this.entityId);
        } else {
            packet.setEntityID(context.getPlayerManager().getEntityIdByUUID(this.uuid, world));
            System.out.println(this.uuid.toString() + ", " + this.statusId);
        }
        packet.setEntityStatus((byte)this.statusId);
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!player.getWorld().equals((Object)world)) continue;
            packet.sendPacket(player);
        }
    }

    @Override
    public void rollback(Context context) {
    }

    @Override
    public boolean canBeSkipped() {
        return false;
    }

    public EntityStatusMeta() {
    }

    public static class EntityStatusMetaAdapter
    extends PacketAdapter
    implements MetaAdapter {
        private ReplayFileWriter replayFileWriter;
        private Map<Integer, Integer> status = new ConcurrentHashMap<Integer, Integer>();

        public EntityStatusMetaAdapter(ReplayFileWriter replayFileWriter) {
            super((Plugin)MCTV.getInstance(), new PacketType[]{PacketType.Play.Server.ENTITY_STATUS});
            this.replayFileWriter = replayFileWriter;
        }

        @Override
        public void start() {
            ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)this);
        }

        @Override
        public void stop() {
            ProtocolLibrary.getProtocolManager().removePacketListener((PacketListener)this);
        }

        public void onPacketSending(PacketEvent event) {
            UUID uuid;
            final WrapperPlayServerEntityStatus packet = new WrapperPlayServerEntityStatus(event.getPacket());
            UUID uUID = uuid = packet.getEntity(event) instanceof Player ? packet.getEntity(event).getUniqueId() : null;
            if (uuid != null) {
                byte statusId = packet.getEntityStatus();
                Integer lastStatus = this.status.get(packet.getEntityID());
                if (lastStatus == null || lastStatus != statusId) {
                    EntityStatusMeta meta = new EntityStatusMeta(uuid, (int)statusId, Bukkit.getPlayer((UUID)uuid).getWorld().getName());
                    this.replayFileWriter.addMeta(meta);
                    this.status.put(packet.getEntityID(), Integer.valueOf(statusId));
                    Bukkit.getScheduler().runTaskLater((Plugin)MCTV.getInstance(), new Runnable(){

                        @Override
                        public void run() {
                            status.remove(packet.getEntityID());
                        }
                    }, 0L);
                }
            } else {
                byte statusId = packet.getEntityStatus();
                EntityStatusMeta meta = new EntityStatusMeta(packet.getEntityID(), (int)statusId, event.getPlayer().getWorld().getName());
                this.replayFileWriter.addMeta(meta);
            }
        }

        public void onPacketReceiving(PacketEvent event) {
        }
    }
}

