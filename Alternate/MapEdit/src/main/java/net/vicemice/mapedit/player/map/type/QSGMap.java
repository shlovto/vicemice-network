package net.vicemice.mapedit.player.map.type;

import lombok.Getter;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.LocationSerializer;
import org.bson.Document;
import org.bukkit.Location;

import java.util.List;

@Getter
public class QSGMap {
    private String name;
    private String type;
    private String creator;
    private long created;
    private long lastChanges;
    private List<Location> spawns;
    private boolean exist;

    public QSGMap(String name, String type, String creator, long created, long lastChanges, List<Location> spawns, boolean exist) {
        this.name = name;
        this.type = type;
        this.creator = creator;
        this.created = created;
        this.lastChanges = lastChanges;
        this.spawns = spawns;
        this.exist = exist;
    }

    public void save() {
        if (exist) {
            this.lastChanges = System.currentTimeMillis();
            Document document = new Document("type", this.type);
            document.put("lastChanges", System.currentTimeMillis());
            document.put("spawns", LocationSerializer.toStringList(this.spawns));

            GoServer.getService().getMongoManager().getGameDatabase().getCollection("maps").updateOne(new Document("name", name), new Document("$set", document));
            return;
        }

        Document document = new Document("game", "QSG");
        document.append("name", this.name);
        document.append("type", "12x1");
        document.append("creator", this.creator);
        document.append("created", System.currentTimeMillis());
        document.append("lastChanges", System.currentTimeMillis());
        document.append("spawns", LocationSerializer.toStringList(this.spawns));

        GoServer.getService().getMongoManager().getGameDatabase().getCollection("maps").insertOne(document);
    }
}
