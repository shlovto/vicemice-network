package net.vicemice.mapedit.listener;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.edit.MapEditRequestPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.mapedit.MapEdit;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/*
 * Class created at 17:08 - 09.04.2020
 * Copyright (C) elrobtossohn
 */
public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        player.setGameMode(GameMode.CREATIVE);

        if (Bukkit.getOnlinePlayers().size() == 1) {
            GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.MAPEDIT_REQUEST_DATA, new MapEditRequestPacket(player.getUniqueId())), API.PacketReceiver.SLAVE);
            GoAPI.getServerAPI().changeServer(ServerState.INGAME, player.getName());
        } else {
            player.teleport(MapEdit.getPlayerManager().getHoster());
        }

        event.setJoinMessage(null);
        PlayerRankData playerRank = GoAPI.getUserAPI().getRankData(player.getUniqueId());

        String playerName = (GoAPI.getUserAPI().isNicked(player.getUniqueId()) ? "§9"+GoAPI.getUserAPI().getNickName(player.getUniqueId()) : playerRank.getRankColor() + player.getName());
        Bukkit.getOnlinePlayers().forEach(p -> {
            p.sendMessage(GoAPI.getUserAPI().translate(p.getUniqueId(), "player-joined", playerName));
        });
    }
}