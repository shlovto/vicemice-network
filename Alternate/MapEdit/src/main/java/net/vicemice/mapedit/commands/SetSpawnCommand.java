package net.vicemice.mapedit.commands;

import net.vicemice.hector.GoAPI;
import net.vicemice.mapedit.MapEdit;
import net.vicemice.mapedit.player.creator.Creator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/*
 * Class created at 01:21 - 12.04.2020
 * Copyright (C) elrobtossohn
 */
public class SetSpawnCommand extends Command {

    public SetSpawnCommand() {
        super("setspawn");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        Player player = (Player) commandSender;
        Creator creator = MapEdit.getPlayerManager().getCreators().get(player.getUniqueId());

        if (creator == null) {
            player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-no-map"));
            return true;
        }
        player.getWorld().setSpawnLocation(Integer.parseInt(String.valueOf(player.getLocation().getX())), Integer.parseInt(String.valueOf(player.getLocation().getY())), Integer.parseInt(String.valueOf(player.getLocation().getZ())));
        player.getWorld().save();
        player.sendMessage("§aSpawn set!");

        return false;
    }
}
