package net.vicemice.mapedit;

import lombok.Getter;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.Register;
import net.vicemice.mapedit.commands.MiddleCommand;
import net.vicemice.mapedit.commands.SaveCommand;
import net.vicemice.mapedit.commands.SetSpawnCommand;
import net.vicemice.mapedit.commands.ToggleCommand;
import net.vicemice.mapedit.packet.PacketListener;
import net.vicemice.mapedit.player.PlayerManager;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.plugin.java.JavaPlugin;

public class MapEdit extends JavaPlugin {

    @Getter
    private static MapEdit instance;
    @Getter
    private static PlayerManager playerManager;

    @Override
    public void onEnable() {
        instance = this;

        Register.registerListener(this, "net.vicemice.mapedit.listener");

        GoServer.getLocaleManager().addPath("cloud/locales/mapedit");
        playerManager = new PlayerManager();

        ((CraftServer)getServer()).getCommandMap().register("setspawn", new SetSpawnCommand());
        ((CraftServer)getServer()).getCommandMap().register("toggle", new ToggleCommand());
        ((CraftServer)getServer()).getCommandMap().register("middle", new MiddleCommand());
        ((CraftServer)getServer()).getCommandMap().register("save", new SaveCommand());

        GoServer.getService().getGoAPI().getNettyClient().addGetter(new PacketListener());
    }
}
