package net.vicemice.mapedit.player;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.mapedit.player.creator.Creator;
import org.bukkit.entity.Player;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class PlayerManager {

    @Getter
    private ConcurrentHashMap<UUID, Creator> creators;
    @Getter
    @Setter
    private Player hoster;

    public PlayerManager() {
        this.creators = new ConcurrentHashMap<>();
        this.hoster = null;
    }
}
