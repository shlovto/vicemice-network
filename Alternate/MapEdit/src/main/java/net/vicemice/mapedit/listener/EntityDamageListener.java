package net.vicemice.mapedit.listener;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

/*
 * Class created at 12:23 - 10.04.2020
 * Copyright (C) elrobtossohn
 */
public class EntityDamageListener implements Listener {

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        if (event.getEntityType() == EntityType.PLAYER) event.setCancelled(true);
        if (event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK || event.getEntityType() == EntityType.ARMOR_STAND) event.setCancelled(true);
    }
}