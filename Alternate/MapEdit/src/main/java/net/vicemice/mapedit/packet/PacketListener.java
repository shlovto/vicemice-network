package net.vicemice.mapedit.packet;

import io.netty.channel.Channel;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.server.mapedit.MapEditInfoPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.utils.LocationSerializer;
import net.vicemice.hector.server.utils.MongoManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.mapedit.MapEdit;
import net.vicemice.mapedit.player.creator.Creator;
import net.vicemice.mapedit.player.map.type.BedWarsMap;
import net.vicemice.mapedit.player.map.type.QSGMap;
import org.apache.commons.io.FileUtils;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PacketListener extends PacketGetter {

    @Override
    public void receivePacket(PacketHolder var1) {
        if (var1.getType() == PacketType.MAPEDIT_INFO) {
            MapEditInfoPacket mapEditInfoPacket = (MapEditInfoPacket) var1.getValue();
            IUser user = UserManager.getUser(mapEditInfoPacket.getUniqueId());
            user.sendMessage("map-loading");

            try {
                FileUtils.copyDirectory(new File("/opt/network/maps/"+mapEditInfoPacket.getTemplate().toLowerCase()+"/"+mapEditInfoPacket.getMap().toLowerCase()), new File(mapEditInfoPacket.getMap().toLowerCase()));
            } catch(Exception ex) {
                ex.printStackTrace();
            }

            new BukkitRunnable() {
                @Override
                public void run() {
                    Bukkit.createWorld(new WorldCreator(mapEditInfoPacket.getMap().toLowerCase()));
                    Bukkit.getWorld(mapEditInfoPacket.getMap()).setStorm(false);
                    Bukkit.getWorld(mapEditInfoPacket.getMap()).setThundering(false);
                    Bukkit.getWorld(mapEditInfoPacket.getMap()).setTime(10000);
                    Bukkit.getWorld(mapEditInfoPacket.getMap()).setGameRuleValue("randomTickSpeed", "3");
                    Bukkit.getWorld(mapEditInfoPacket.getMap()).setGameRuleValue("doTileDrops", "true");
                    Bukkit.getWorld(mapEditInfoPacket.getMap()).setGameRuleValue("doDaylightCycle", "false");

                    if(Bukkit.getWorld(mapEditInfoPacket.getMap()) != null) {
                        MapEdit.getPlayerManager().getCreators().put(user.getUniqueId(), new Creator(user, mapEditInfoPacket.getMap(), mapEditInfoPacket.getTemplate()));
                        user.sendMessage("map-loaded", mapEditInfoPacket.getMap());
                        user.teleport(Bukkit.getWorld(mapEditInfoPacket.getMap()).getSpawnLocation());
                        MapEdit.getPlayerManager().setHoster(user.getBukkitPlayer());

                        Document document = GoServer.getService().getMongoManager().getDocument(MongoManager.Database.GAMES, "maps", new Document("name", mapEditInfoPacket.getMap()));

                        if (document != null && document.getString("game").equalsIgnoreCase("QSG")) {
                            List<Location> locations = new ArrayList<>();
                            if (document.getList("spawns", String.class) != null && !document.getList("spawns", String.class).isEmpty())
                                locations = LocationSerializer.fromList(document.getList("spawns", String.class));
                            MapEdit.getPlayerManager().getCreators().get(user.getUniqueId()).setQsgMap(new QSGMap(mapEditInfoPacket.getMap(), document.getString("type"), document.getString("creator"), document.getLong("created"), document.getLong("lastChanges"), locations, true));
                            Bukkit.broadcastMessage("Map exist, loading Map...");
                        } else if (document != null && document.getString("game").equalsIgnoreCase("BedWars")) {
                            Map<String, Location> spawns = new HashMap<>();
                            if (document.getList("spawns", String.class) != null && !document.getList("spawns", String.class).isEmpty()) {
                                for (String string : document.getList("spawns", String.class)) {
                                    String[] location = string.split(";");
                                    spawns.put(location[0], new Location(Bukkit.getWorld(mapEditInfoPacket.getMap()), Double.parseDouble(location[1]), Double.parseDouble(location[2]), Double.parseDouble(location[3]), Float.parseFloat(location[4]), Float.parseFloat(location[5])));
                                }
                            }
                            Map<String, Location> beds = new HashMap<>();
                            if (document.getList("beds", String.class) != null && !document.getList("beds", String.class).isEmpty()) {
                                for (String string : document.getList("beds", String.class)) {
                                    String[] location = string.split(";");
                                    beds.put(location[0], new Location(Bukkit.getWorld(mapEditInfoPacket.getMap()), Double.parseDouble(location[1]), Double.parseDouble(location[2]), Double.parseDouble(location[3])));
                                }
                            }

                            List<Location> bronzeSpawner = new ArrayList<>();
                            if (document.getList("bronzeSpawner", String.class) != null && !document.getList("bronzeSpawner", String.class).isEmpty())
                                bronzeSpawner = LocationSerializer.fromListWithoutYawAndPitch(document.getList("bronzeSpawner", String.class));
                            List<Location> ironSpawner = new ArrayList<>();
                            if (document.getList("ironSpawner", String.class) != null && !document.getList("ironSpawner", String.class).isEmpty())
                                ironSpawner = LocationSerializer.fromListWithoutYawAndPitch(document.getList("ironSpawner", String.class));
                            List<Location> goldSpawner = new ArrayList<>();
                            if (document.getList("goldSpawner", String.class) != null && !document.getList("goldSpawner", String.class).isEmpty())
                                goldSpawner = LocationSerializer.fromListWithoutYawAndPitch(document.getList("goldSpawner", String.class));
                            List<Location> villagers = new ArrayList<>();
                            if (document.getList("villager", String.class) != null && !document.getList("villager", String.class).isEmpty())
                                villagers = LocationSerializer.fromList(document.getList("villager", String.class));

                            MapEdit.getPlayerManager().getCreators().get(user.getUniqueId()).setBedWarsMap(new BedWarsMap(mapEditInfoPacket.getMap(), document.getString("type"), document.getString("creator"), document.getLong("created"), document.getLong("lastChanges"), spawns, beds, bronzeSpawner, ironSpawner, goldSpawner, villagers, true));
                            Bukkit.broadcastMessage("Map exist, loading Map...");
                        }

                        if (mapEditInfoPacket.getTemplate().equalsIgnoreCase("QSG") && MapEdit.getPlayerManager().getCreators().get(user.getUniqueId()).getQsgMap() == null) {
                            MapEdit.getPlayerManager().getCreators().get(user.getUniqueId()).setQsgMap(new QSGMap(mapEditInfoPacket.getMap(), "12x1", null, System.currentTimeMillis(), System.currentTimeMillis(), new ArrayList<>(), false));
                        } else if (mapEditInfoPacket.getTemplate().startsWith("bw") && MapEdit.getPlayerManager().getCreators().get(user.getUniqueId()).getBedWarsMap() == null) {
                            MapEdit.getPlayerManager().getCreators().get(user.getUniqueId()).setBedWarsMap(new BedWarsMap(mapEditInfoPacket.getMap(), mapEditInfoPacket.getTemplate().replace("bw", ""), null, System.currentTimeMillis(), System.currentTimeMillis(), new HashMap<>(), new HashMap<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), false));
                        }
                    } else {
                        user.sendMessage("map-loading-error");
                        user.getBukkitPlayer().kickPlayer("lobby");
                    }
                }
            }.runTask(MapEdit.getInstance());
        }
    }

    @Override
    public void channelActive(Channel var1) {

    }
}