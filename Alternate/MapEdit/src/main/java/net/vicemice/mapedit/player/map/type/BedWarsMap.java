package net.vicemice.mapedit.player.map.type;

import lombok.Getter;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.LocationSerializer;
import org.bson.Document;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/*
 * Class created at 00:01 - 12.04.2020
 * Copyright (C) elrobtossohn
 */
@Getter
public class BedWarsMap {
    private String name, type, creator;
    private long created, lastChanges;
    private List<Location> bronzeSpawner, ironSpawner, goldSpawner, villagers;
    private Map<String, Location> spawns, beds;
    private boolean exist;

    public BedWarsMap(String name, String type, String creator, long created, long lastChanges, Map<String, Location> spawns, Map<String, Location> beds, List<Location> bronzeSpawner, List<Location> ironSpawner, List<Location> goldSpawner, List<Location> villagers, boolean exist) {
        this.name = name;
        this.type = type;
        this.creator = creator;
        this.created = created;
        this.lastChanges = lastChanges;
        this.spawns = spawns;
        this.beds = beds;
        this.bronzeSpawner = bronzeSpawner;
        this.ironSpawner = ironSpawner;
        this.goldSpawner = goldSpawner;
        this.villagers = villagers;

        this.exist = exist;
    }

    public void save() {
        if (exist) {
            this.lastChanges = System.currentTimeMillis();
            Document document = new Document("type", this.type);
            document.put("lastChanges", System.currentTimeMillis());

            List<String> spawns = new ArrayList<>();
            for (String s : this.spawns.keySet()) {
                Location location =  this.spawns.get(s);
                spawns.add(s+";"+location.getX()+";"+location.getY()+";"+location.getZ()+";"+location.getYaw()+";"+location.getPitch());
            }
            document.put("spawns", spawns);
            List<String> beds = new ArrayList<>();
            for (String s : this.beds.keySet()) {
                Location location =  this.beds.get(s);
                beds.add(s+";"+location.getX()+";"+location.getY()+";"+location.getZ());
            }
            document.put("beds", beds);
            document.append("bronzeSpawner", LocationSerializer.toStringListWithoutYawAndPitch(this.bronzeSpawner));
            document.append("ironSpawner", LocationSerializer.toStringListWithoutYawAndPitch(this.ironSpawner));
            document.append("goldSpawner", LocationSerializer.toStringListWithoutYawAndPitch(this.goldSpawner));
            document.append("villager", LocationSerializer.toStringList(this.villagers));

            GoServer.getService().getMongoManager().getGameDatabase().getCollection("maps").updateOne(new Document("name", name), new Document("$set", document));
            return;
        }

        Document document = new Document("game", "BedWars");
        document.append("name", this.name);
        document.append("type", this.type);
        document.append("creator", this.creator);
        document.append("created", System.currentTimeMillis());
        document.append("lastChanges", System.currentTimeMillis());

        List<String> spawns = new ArrayList<>();
        for (String s : this.spawns.keySet()) {
            Location location =  this.spawns.get(s);
            spawns.add(s+";"+location.getX()+";"+location.getY()+";"+location.getZ()+";"+location.getYaw()+";"+location.getPitch());
        }
        document.put("spawns", spawns);
        List<String> beds = new ArrayList<>();
        for (String s : this.beds.keySet()) {
            Location location =  this.beds.get(s);
            beds.add(s+";"+location.getX()+";"+location.getY()+";"+location.getZ());
        }
        document.put("beds", beds);
        document.append("bronzeSpawner", LocationSerializer.toStringListWithoutYawAndPitch(this.bronzeSpawner));
        document.append("ironSpawner", LocationSerializer.toStringListWithoutYawAndPitch(this.ironSpawner));
        document.append("goldSpawner", LocationSerializer.toStringListWithoutYawAndPitch(this.goldSpawner));
        document.append("villager", LocationSerializer.toStringList(this.villagers));

        GoServer.getService().getMongoManager().getGameDatabase().getCollection("maps").insertOne(document);
    }

    public boolean removeSpawn(Location location) {
        for (String key : this.getSpawns().keySet()) {
            if (this.getSpawns().get(key).equals(location)) continue;
            this.getSpawns().remove(key);
            return true;
        }

        return false;
    }
}