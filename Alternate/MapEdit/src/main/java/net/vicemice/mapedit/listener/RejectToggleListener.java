package net.vicemice.mapedit.listener;

import net.vicemice.mapedit.MapEdit;
import net.vicemice.mapedit.player.creator.Creator;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

/*
 * Class created at 19:01 - 16.04.2020
 * Copyright (C) elrobtossohn
 */
public class RejectToggleListener implements Listener {

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        Creator creator = MapEdit.getPlayerManager().getCreators().get(player.getUniqueId());

        if (creator == null)
            return;

        if (creator.isToggled()) {
            event.setCancelled(true);
            event.setBuild(false);
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        Creator creator = MapEdit.getPlayerManager().getCreators().get(player.getUniqueId());

        if (creator == null)
            return;

        if (creator.isToggled()) {
            event.setCancelled(true);
        }
    }
}