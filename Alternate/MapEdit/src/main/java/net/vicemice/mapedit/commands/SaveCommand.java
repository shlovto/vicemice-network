package net.vicemice.mapedit.commands;

import net.vicemice.hector.GoAPI;
import net.vicemice.mapedit.MapEdit;
import net.vicemice.mapedit.player.creator.Creator;
import org.apache.commons.io.FileUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.io.File;

/*
 * Class created at 17:14 - 09.04.2020
 * Copyright (C) elrobtossohn
 */
public class SaveCommand extends Command {

    public SaveCommand() {
        super("save");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        Player player = (Player) commandSender;
        Creator creator = MapEdit.getPlayerManager().getCreators().get(player.getUniqueId());

        if (creator == null) {
            player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-no-map"));
            return true;
        }

        try {
            Process dataFolder = Runtime.getRuntime().exec(new String[]{"bash", "-c", "rm -r "+creator.getMap().toLowerCase()+"/playerdata"});
            Process statsFolder = Runtime.getRuntime().exec(new String[]{"bash", "-c", "rm -r "+creator.getMap().toLowerCase()+"/playerdata"});
            Process removeWorldFolder = Runtime.getRuntime().exec(new String[]{"bash", "-c", "rm -r /opt/network/maps/"+creator.getTemplate()+"/"+creator.getMap().toLowerCase()});
            dataFolder.waitFor();
            statsFolder.waitFor();
            removeWorldFolder.waitFor();

            FileUtils.copyDirectory(new File(creator.getMap().toLowerCase()), new File("/opt/network/maps/"+creator.getTemplate()+"/"+creator.getMap().toLowerCase()));
            player.sendMessage(GoAPI.getUserAPI().translate(player.getUniqueId(), "command-save-success"));
        } catch(Exception ex) {
            player.sendMessage(GoAPI.getUserAPI().translate(player.getUniqueId(), "command-save-error"));
            ex.printStackTrace();
        }

        return false;
    }
}
