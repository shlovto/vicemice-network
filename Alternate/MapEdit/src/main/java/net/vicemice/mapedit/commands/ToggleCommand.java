package net.vicemice.mapedit.commands;

import net.vicemice.hector.GoAPI;
import net.vicemice.mapedit.MapEdit;
import net.vicemice.mapedit.player.creator.Creator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ToggleCommand extends Command {

    public ToggleCommand() {
        super("toggle");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        Player player = (Player) commandSender;
        Creator creator = MapEdit.getPlayerManager().getCreators().get(player.getUniqueId());

        if (creator == null) {
            player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-no-map"));
            return true;
        }

        creator.toggle();

        return false;
    }
}
