package net.vicemice.mapedit.player.creator;

import lombok.Data;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.api.clickableitems.ClickType;
import net.vicemice.hector.api.clickableitems.spigot.ClickableItemManagerSpigot;
import net.vicemice.hector.api.clickableitems.spigot.ClickableItemSpigot;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.mapedit.MapEdit;
import net.vicemice.mapedit.player.map.inventory.SpawnsInventory;
import net.vicemice.mapedit.player.map.type.BedWarsMap;
import net.vicemice.mapedit.player.map.type.QSGMap;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Data
public class Creator {

    private IUser user;
    private String map, template;
    private boolean toggled;
    private ClickableItemManagerSpigot clickableItemManagerSpigot;
    private QSGMap qsgMap;
    private BedWarsMap bedWarsMap;

    public Creator(IUser user, String map, String template) {
        this.user = user;
        this.map = map;
        this.template = template;
        clickableItemManagerSpigot = new ClickableItemManagerSpigot(GoServer.getLocaleManager(), MapEdit.getInstance());
    }

    public void toggle() {
        if (this.isToggled()) {
            user.getBukkitPlayer().getInventory().clear();
            toggled = false;
            user.sendMessage("creator-mode-left");
        } else {
            user.getBukkitPlayer().getInventory().clear();

            if (template.equalsIgnoreCase("QSG")) {
                user.getBukkitPlayer().getInventory().setItem(2, clickableItemManagerSpigot.getItem(new ClickableItemSpigot("qsg-add-spawn", false, ItemBuilder.create(Material.SKULL_ITEM).name(user.translate("clickable-item-qsg-add-spawn-name")).build()) {
                    @Override
                    public void onClick(IUser user, ClickType clickType) {
                        qsgMap.getSpawns().add(user.getLocation());
                        user.sendMessage("clickable-item-save-edit-action");
                    }
                }, user.getLocale()));
                user.getBukkitPlayer().getInventory().setItem(4, clickableItemManagerSpigot.getItem(new ClickableItemSpigot("qsg-list-spawns", false, ItemBuilder.create(Material.BOOK).name(user.translate("clickable-item-qsg-list-spawns-name")).build()) {
                    @Override
                    public void onClick(IUser user, ClickType clickType) {
                        new SpawnsInventory().create(user, qsgMap.getSpawns(), 1).open();
                    }
                }, user.getLocale()));
                user.getBukkitPlayer().getInventory().setItem(6, clickableItemManagerSpigot.getItem(new ClickableItemSpigot("save-edit", false, ItemBuilder.create(Material.EMERALD).name(user.translate("clickable-item-save-edit-name")).build()) {
                    @Override
                    public void onClick(IUser user, ClickType clickType) {
                        qsgMap.save();
                        user.sendMessage("clickable-item-save-edit-action");
                    }
                }, user.getLocale()));
            } else if (template.startsWith("bw")) {
                user.getBukkitPlayer().getInventory().setItem(0, clickableItemManagerSpigot.getItem(new ClickableItemSpigot("bw-add-bronze-spawner", false, ItemBuilder.create(Material.CLAY_BRICK).name(user.translate("clickable-item-bw-add-bronze-spawner-name")).build()) {
                    @Override
                    public void onClick(IUser user, ClickType clickType) {
                        bedWarsMap.getBronzeSpawner().add(user.getLocation());
                        user.sendMessage("clickable-item-bw-add-bronze-spawner-action");
                    }
                }, user.getLocale()));
                user.getBukkitPlayer().getInventory().setItem(1, clickableItemManagerSpigot.getItem(new ClickableItemSpigot("bw-add-iron-spawner", false, ItemBuilder.create(Material.IRON_INGOT).name(user.translate("clickable-item-bw-add-iron-spawner-name")).build()) {
                    @Override
                    public void onClick(IUser user, ClickType clickType) {
                        bedWarsMap.getIronSpawner().add(user.getLocation());
                        user.sendMessage("clickable-item-bw-add-iron-spawner-action");
                    }
                }, user.getLocale()));
                user.getBukkitPlayer().getInventory().setItem(2, clickableItemManagerSpigot.getItem(new ClickableItemSpigot("bw-add-gold-spawner", false, ItemBuilder.create(Material.GOLD_INGOT).name(user.translate("clickable-item-bw-add-gold-spawner-name")).build()) {
                    @Override
                    public void onClick(IUser user, ClickType clickType) {
                        bedWarsMap.getGoldSpawner().add(user.getLocation());
                        user.sendMessage("clickable-item-bw-add-gold-spawner-action");
                    }
                }, user.getLocale()));
                user.getBukkitPlayer().getInventory().setItem(3, clickableItemManagerSpigot.getItem(new ClickableItemSpigot("bw-add-npc-spawn", false, ItemBuilder.create(Material.GOLD_NUGGET).name(user.translate("clickable-item-bw-add-npc-spawn-name")).build()) {
                    @Override
                    public void onClick(IUser user, ClickType clickType) {
                        bedWarsMap.getVillagers().add(user.getLocation());
                        user.sendMessage("clickable-item-bw-add-npc-spawn-action");
                    }
                }, user.getLocale()));
                user.getBukkitPlayer().getInventory().setItem(4, clickableItemManagerSpigot.getItem(new ClickableItemSpigot("bw-add-bed", false, ItemBuilder.create(Material.BED).name(user.translate("clickable-item-bw-add-bed-name")).build()) {
                    @Override
                    public void onClick(IUser user, ClickType clickType) {
                        toggleTeamChoose(TeamAction.ADD_BED);
                    }
                }, user.getLocale()));
                user.getBukkitPlayer().getInventory().setItem(5, clickableItemManagerSpigot.getItem(new ClickableItemSpigot("bw-add-spawn", false, ItemBuilder.create(Material.SKULL_ITEM).name(user.translate("clickable-item-bw-add-spawn-name")).build()) {
                    @Override
                    public void onClick(IUser user, ClickType clickType) {
                        toggleTeamChoose(TeamAction.ADD_SPAWN);
                    }
                }, user.getLocale()));
                user.getBukkitPlayer().getInventory().setItem(6, clickableItemManagerSpigot.getItem(new ClickableItemSpigot("bw-list-spawns", false, ItemBuilder.create(Material.BOOK).name(user.translate("clickable-item-bw-list-spawns-name")).build()) {
                    @Override
                    public void onClick(IUser user, ClickType clickType) {
                        new SpawnsInventory().create(user, new ArrayList<>(bedWarsMap.getSpawns().values()), 1).open();
                    }
                }, user.getLocale()));
                user.getBukkitPlayer().getInventory().setItem(7, clickableItemManagerSpigot.getItem(new ClickableItemSpigot("bw-list-spawner", false, ItemBuilder.create(Material.BOOK).name(user.translate("clickable-item-bw-list-spawner-name")).build()) {
                    @Override
                    public void onClick(IUser user, ClickType clickType) {
                        new SpawnsInventory().create(user, 1).open();
                    }
                }, user.getLocale()));
                user.getBukkitPlayer().getInventory().setItem(8, clickableItemManagerSpigot.getItem(new ClickableItemSpigot("save-edit", false, ItemBuilder.create(Material.EMERALD).name(user.translate("clickable-item-save-edit-name")).build()) {
                    @Override
                    public void onClick(IUser user, ClickType clickType) {
                        bedWarsMap.save();
                        user.sendMessage("clickable-item-save-edit-action");
                    }
                }, user.getLocale()));
            } else {
                user.sendMessage("game-not-supported");
                return;
            }

            toggled = true;
            user.sendMessage("creator-mode-enter");
        }
    }

    public void toggleTeamChoose(TeamAction teamAction) {
        GUI gui = this.getUser().createGUI("Choose a team", 5);

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        String[] teams = new String[] { "red", "blue", "yellow", "green", "pink", "cyan", "orange", "purple" };
        Map<String, ChatColor> teamColors = new HashMap<>();
        teamColors.put("red", ChatColor.RED);
        teamColors.put("blue", ChatColor.BLUE);
        teamColors.put("yellow", ChatColor.YELLOW);
        teamColors.put("green", ChatColor.GREEN);
        teamColors.put("pink", ChatColor.LIGHT_PURPLE);
        teamColors.put("cyan", ChatColor.AQUA);
        teamColors.put("orange", ChatColor.GOLD);
        teamColors.put("purple", ChatColor.DARK_PURPLE);

        AtomicInteger i = new AtomicInteger(9);
        for (String team : teams) {
            gui.setItem(i.get(), ItemBuilder.create(Material.WOOL).durability(14).name("§7Team "+teamColors.get(team)+team).glow((teamAction == TeamAction.ADD_BED ? this.getBedWarsMap().getBeds().containsKey(team) : (teamAction == TeamAction.ADD_SPAWN && this.getBedWarsMap().getSpawns().containsKey("red")))).build(), e -> {
                if (teamAction == TeamAction.ADD_BED) {
                    this.getBedWarsMap().getBeds().remove(team);
                    this.getBedWarsMap().getBeds().put(team, user.getLocation());
                    user.sendMessage("clickable-item-bw-add-bed-action", teamColors.get(team)+team);
                } else if (teamAction == TeamAction.ADD_SPAWN) {
                    this.getBedWarsMap().getSpawns().remove(team);
                    this.getBedWarsMap().getSpawns().put(team, user.getLocation());
                    user.sendMessage("clickable-item-bw-add-spawn-action", teamColors.get(team)+team);
            }
            });

            i.set((i.get()+1));
        }

        gui.open();
    }

    public enum TeamAction {
        ADD_SPAWN,
        ADD_BED;
    }
}
