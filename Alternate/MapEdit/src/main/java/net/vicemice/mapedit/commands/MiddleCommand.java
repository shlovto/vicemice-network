package net.vicemice.mapedit.commands;

import net.vicemice.hector.GoAPI;
import net.vicemice.mapedit.MapEdit;
import net.vicemice.mapedit.player.creator.Creator;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/*
 * Class created at 16:11 - 18.04.2020
 * Copyright (C) elrobtossohn
 */
public class MiddleCommand extends Command {

    public MiddleCommand() {
        super("middle");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        Player player = (Player) commandSender;
        Creator creator = MapEdit.getPlayerManager().getCreators().get(player.getUniqueId());

        if (creator == null) {
            player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-no-map"));
            return true;
        }

        player.teleport(new Location(player.getWorld(), (formatDouble(player.getLocation().getX())+0.5), (formatDouble(player.getLocation().getY())+0.0), (formatDouble(player.getLocation().getZ())+0.5)));

        return false;
    }

    private int formatDouble(double value) {
        return (int) value;
    }
}
