package net.vicemice.mapedit.listener;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.mapedit.MapEdit;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/*
 * Class created at 17:10 - 09.04.2020
 * Copyright (C) elrobtossohn
 */
public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        event.setQuitMessage(null);
        PlayerRankData playerRank = GoAPI.getUserAPI().getRankData(player.getUniqueId());

        String playerName = (GoAPI.getUserAPI().isNicked(player.getUniqueId()) ? "§9"+GoAPI.getUserAPI().getNickName(player.getUniqueId()) : playerRank.getRankColor() + player.getName());
        Bukkit.getOnlinePlayers().forEach(p -> {
            p.sendMessage(GoAPI.getUserAPI().translate(p.getUniqueId(), "player-left", playerName));
        });

        MapEdit.getPlayerManager().getCreators().remove(player.getUniqueId());

        if (MapEdit.getPlayerManager().getHoster() != null && MapEdit.getPlayerManager().getHoster() == player) {
            MapEdit.getPlayerManager().setHoster(null);
            GoAPI.getServerAPI().changeServer(ServerState.LOBBY, "AVAILABLE");
        }
    }
}
