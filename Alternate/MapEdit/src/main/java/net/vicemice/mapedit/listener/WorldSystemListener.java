package net.vicemice.mapedit.listener;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

/*
 * Class created at 12:23 - 10.04.2020
 * Copyright (C) elrobtossohn
 */
public class WorldSystemListener implements Listener {

    @EventHandler
    public void onSpawn(EntitySpawnEvent event) {
        if (event.getEntityType() == EntityType.PLAYER) return;
        if (event.getEntityType() == EntityType.VILLAGER) return;
        if (event.getEntityType() == EntityType.ARMOR_STAND) return;
        event.setCancelled(true);
    }
}