package net.vicemice.mapedit.player.map.inventory;

import lombok.Getter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.players.utils.Pagination;
import net.vicemice.mapedit.MapEdit;
import net.vicemice.mapedit.player.creator.Creator;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.bukkit.Material.*;

/*
 * Class created at 23:47 - 11.04.2020
 * Copyright (C) elrobtossohn
 */
public class SpawnsInventory {

    public GUI create(IUser user, List<Location> locations, int page) {
        Creator creator = MapEdit.getPlayerManager().getCreators().get(user.getUniqueId());
        Pagination<Location> pagination = new Pagination<>(locations, 27);

        GUI gui = user.createGUI("Spawns", (pagination.getPages() > 1 ? 6 : 5));

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        AtomicInteger number = new AtomicInteger((page > 1 ? ((1+9)*page) : 1));
        AtomicInteger i = new AtomicInteger(9);
        pagination.printPage(page, location -> {
            gui.setItem(i.get(), ItemBuilder.create(Material.PAPER).name("§7Spawn §e#"+number.get()).lore().build(), e -> {
                if (e.getAction() == InventoryAction.PICKUP_ALL) {
                    user.teleport(location);
                    e.getView().close();
                } else if (e.getAction() == InventoryAction.PICKUP_HALF) {
                    if (creator.getBedWarsMap() != null) {
                        creator.getBedWarsMap().removeSpawn(location);
                        user.sendRawMessage("§cRemoved!");
                    } else if (creator.getQsgMap() != null) {
                        creator.getQsgMap().getSpawns().remove(location);
                        user.sendRawMessage("§cRemoved!");
                    } else {
                        user.sendRawMessage("§cCould not remove!");
                    }
                    e.getView().close();
                }
            });
            i.set((i.get()+1));
            number.set((number.get()+1));
        });

        if (pagination.getElementsFor(page).isEmpty()) {
            gui.setItem(22, ItemBuilder.create(BARRIER).name("§cNo spawns").build());
        }

        if (!pagination.getElementsFor((page-1)).isEmpty()) {
            gui.setItem(50, ItemBuilder.create(GUI.Items.getLeft()).name(user.translate("previous-page")).build(), e -> {
                this.create(user, locations, (page-1)).open();
            });
        }
        if (!pagination.getElementsFor((page+1)).isEmpty()) {
            gui.setItem(52, ItemBuilder.create(GUI.Items.getRight()).name(user.translate("next-page")).build(), e -> {
                this.create(user, locations, (page+1)).open();
            });
        }

        return gui;
    }

    public GUI create(IUser user, int page) {
        Creator creator = MapEdit.getPlayerManager().getCreators().get(user.getUniqueId());

        List<Spawner> spawners = new ArrayList<>();
        for (Location location : creator.getBedWarsMap().getBronzeSpawner()) {
            spawners.add(new Spawner(SpawnerType.BRONZE, location));
        }
        for (Location location : creator.getBedWarsMap().getIronSpawner()) {
            spawners.add(new Spawner(SpawnerType.IRON, location));
        }
        for (Location location : creator.getBedWarsMap().getGoldSpawner()) {
            spawners.add(new Spawner(SpawnerType.GOLD, location));
        }
        for (Location location : creator.getBedWarsMap().getVillagers()) {
            spawners.add(new Spawner(SpawnerType.VILLAGER, location));
        }

        Pagination<Spawner> pagination = new Pagination<>(spawners, 27);

        GUI gui = user.createGUI("Spawns", (pagination.getPages() > 1 ? 6 : 5));

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        AtomicInteger number = new AtomicInteger((page > 1 ? ((1+9)*page) : 1));
        AtomicInteger i = new AtomicInteger(9);
        pagination.printPage(page, spawner -> {
            gui.setItem(i.get(), ItemBuilder.create(spawner.getSpawnerType().getMaterial()).name(spawner.getSpawnerType().getName()+" §7Spawner §e#"+number.get()).lore().build(), e -> {
                if (e.getAction() == InventoryAction.PICKUP_ALL) {
                    user.teleport(spawner.getLocation());
                    e.getView().close();
                } else if (e.getAction() == InventoryAction.PICKUP_HALF) {
                    if (spawner.getSpawnerType() == SpawnerType.BRONZE) {
                        creator.getBedWarsMap().getBronzeSpawner().remove(spawner.getLocation());
                        user.sendRawMessage("§cRemoved!");
                    } else if (spawner.getSpawnerType() == SpawnerType.IRON) {
                        creator.getBedWarsMap().getIronSpawner().remove(spawner.getLocation());
                        user.sendRawMessage("§cRemoved!");
                    } else if (spawner.getSpawnerType() == SpawnerType.GOLD) {
                        creator.getBedWarsMap().getGoldSpawner().remove(spawner.getLocation());
                        user.sendRawMessage("§cRemoved!");
                    } else if (spawner.getSpawnerType() == SpawnerType.VILLAGER) {
                        creator.getBedWarsMap().getVillagers().remove(spawner.getLocation());
                        user.sendRawMessage("§cRemoved!");
                    } else {
                        user.sendRawMessage("§cCould not remove!");
                    }
                    e.getView().close();
                }
            });
            i.set((i.get()+1));
            number.set((number.get()+1));
        });

        if (pagination.getElementsFor(page).isEmpty()) {
            gui.setItem(22, ItemBuilder.create(BARRIER).name("§cNo spawner").build());
        }

        if (!pagination.getElementsFor((page-1)).isEmpty()) {
            gui.setItem(50, ItemBuilder.create(GUI.Items.getLeft()).name(user.translate("previous-page")).build(), e -> {
                this.create(user, (page-1)).open();
            });
        }
        if (!pagination.getElementsFor((page+1)).isEmpty()) {
            gui.setItem(52, ItemBuilder.create(GUI.Items.getRight()).name(user.translate("next-page")).build(), e -> {
                this.create(user, (page+1)).open();
            });
        }

        return gui;
    }

    private class Spawner {
        private SpawnerType spawnerType;
        private Location location;

        public Spawner(SpawnerType spawnerType, Location location) {
            this.spawnerType = spawnerType;
            this.location = location;
        }

        public SpawnerType getSpawnerType() {
            return spawnerType;
        }

        public Location getLocation() {
            return location;
        }
    }

    private enum SpawnerType {
        BRONZE("§cBronze", CLAY_BRICK),
        IRON("§7Iron", IRON_INGOT),
        GOLD("§6Gold", GOLD_INGOT),
        VILLAGER("§2Villager", MONSTER_EGG);

        private SpawnerType(String name, Material material) {
            this.name = name;
            this.material = material;
        }

        @Getter
        private String name;
        @Getter
        private Material material;
    }
}
