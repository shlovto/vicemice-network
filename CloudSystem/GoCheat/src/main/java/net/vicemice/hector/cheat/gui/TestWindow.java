package net.vicemice.hector.cheat.gui;

import net.vicemice.hector.cheat.GoCheat;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class TestWindow extends Window {

    public TestWindow(GoCheat goCheat, Player p) {
        super(goCheat, p, 1, "Test");
        elements[4] = new Element(Material.STONE, "Click on me to update inventory") {
            @Override
            public void doAction(Player p, GoCheat goCheat) {
                Element element = elements[4];
                if(element.getItemStack().getType() == Material.STONE) {
                    element.getItemStack().setType(Material.WOOD);
                }
                else {
                    element.getItemStack().setType(Material.STONE);
                }
                updateWindow();
            }
        };

        prepareInventory();
    }


}
