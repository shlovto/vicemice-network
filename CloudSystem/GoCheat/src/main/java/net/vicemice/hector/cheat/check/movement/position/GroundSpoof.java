package net.vicemice.hector.cheat.check.movement.position;

import net.vicemice.hector.cheat.GoCheat;
import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.MovementCheck;
import net.vicemice.hector.cheat.event.MoveEvent;
import net.vicemice.hector.cheat.util.AdjacentBlocks;
import net.vicemice.hector.cheat.wrap.packet.WrappedPacket;
import org.bukkit.Location;

public class GroundSpoof extends MovementCheck {

    //PASSED (9/13/18)
    private final boolean PREVENT_NOFALL;

    public GroundSpoof() {
        super("groundspoof", true, 0, 3, 0.995, 5000, "%player% failed ground spoof. VL: %vl%", null);
        PREVENT_NOFALL = (boolean) customSetting("preventNoFall", "", true);
    }

    @Override
    protected void check(MoveEvent event) {
        CheatPlayer pp = event.getCheatPlayer();

        boolean onGroundReally;
        if(event.isUpdatePos()) {
            onGroundReally = event.isOnGroundReally();
        } else {
            onGroundReally = AdjacentBlocks.onGroundReally(pp.getPositionPredicted().toLocation(pp.getWorld()), pp.getVelocityPredicted().getY(), true, 0.001, pp);
        }

        if (!event.isStep() && !onGroundReally) {
            if (event.isOnGround()) {

                //Must also check position before, because in the client, Y is clipped first.
                //If Y is clipped, then onGround is set to true.
                Location checkLoc = event.getFrom().clone();
                checkLoc.setY(event.getTo().getY());
                if (AdjacentBlocks.onGroundReally(checkLoc, -1, false, 0.02, pp))
                    return;

                if (event.isOnClientBlock() == null) {
                    punishAndTryRubberband(pp, event, event.getPlayer().getLocation());
                    if (PREVENT_NOFALL)
                        setNotOnGround(event);
                }

            } else {
                reward(pp);
            }
        }
    }

    private void setNotOnGround(MoveEvent e) {
        WrappedPacket packet = e.getWrappedPacket();
        if (GoCheat.getServerVersion() == 7) {
            switch (packet.getType()) {
                case FLYING:
                    packet.setByte(0, 0);
                    return;
                case POSITION:
                    packet.setByte(32, 0);
                    return;
                case LOOK:
                    packet.setByte(8, 0);
                    return;
                case POSITION_LOOK:
                    packet.setByte(40, 0);
            }
        } else {
            switch (packet.getType()) {
                case FLYING:
                    packet.setByte(0, 0);
                    return;
                case POSITION:
                    packet.setByte(24, 0);
                    return;
                case LOOK:
                    packet.setByte(8, 0);
                    return;
                case POSITION_LOOK:
                    packet.setByte(32, 0);
            }
        }
    }
}

