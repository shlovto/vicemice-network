package net.vicemice.hector.cheat.check.movement.position;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.MovementCheck;
import net.vicemice.hector.cheat.event.MoveEvent;
import net.vicemice.hector.cheat.util.AdjacentBlocks;
import org.bukkit.Location;
import org.bukkit.Material;

/**
 * This check is used to flag clients whose jumps are too
 * small. Although insignificant at first glance, small jumps
 * can be exploited to bypass other checks such as speed and
 * criticals.
 */
public class SmallHop extends MovementCheck {

    //TODO: False flag in cobwebs

    public SmallHop() {
        super("smallhop", true, 0, 5, 0.99, 5000, "%player% failed small-hop, VL: %vl%", null);
    }

    @Override
    protected void check(MoveEvent e) {
        CheatPlayer pp = e.getCheatPlayer();
        double deltaY = e.hasTeleported() ? 0D : e.getTo().getY() - e.getFrom().getY();
        double prevDeltaY = pp.getVelocity().getY();
        boolean wasOnGround = pp.isOnGround();
        Location checkPos = e.getFrom().clone().add(0, 1, 0);

        if(!e.getPlayer().isFlying() && !e.hasAcceptedKnockback() && !e.isSlimeBlockBounce() && wasOnGround && deltaY > 0 && deltaY < 0.4 && prevDeltaY <= 0 &&
                !AdjacentBlocks.blockAdjacentIsSolid(checkPos) && !AdjacentBlocks.blockAdjacentIsSolid(checkPos.add(0, 1, 0)) && !AdjacentBlocks.blockAdjacentIsLiquid(checkPos.add(0, -1, 0)) &&
                !AdjacentBlocks.blockAdjacentIsLiquid(checkPos.add(0, -1, 0)) && !AdjacentBlocks.matIsAdjacent(e.getTo(), Material.LADDER, Material.VINE) &&
                !AdjacentBlocks.onGroundReally(e.getTo(), -1, false, 0.001, pp)) {
            punishAndTryRubberband(pp, e, e.getPlayer().getLocation());
        }
        else {
            reward(pp);
        }
    }
}
