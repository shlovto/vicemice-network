package net.vicemice.hector.cheat.check.movement.position;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.MovementCheck;
import net.vicemice.hector.cheat.event.MoveEvent;
import net.vicemice.hector.cheat.util.*;
import org.bukkit.Material;
import org.bukkit.util.Vector;
import java.util.*;

public class SwimVertical extends MovementCheck {

    //TODO: false flag when exiting liquid while on slab while against wall

    private Set<UUID> wasReadyToExit;

    public SwimVertical() {
        super("swimvertical", "%player% failed swim vertical, VL: %vl%");
        wasReadyToExit = new HashSet<>();
    }

    @Override
    protected void check(MoveEvent e) {
        CheatPlayer pp = e.getCheatPlayer();
        double currentDeltaY = MathPlus.round(e.getTo().getY() - e.getFrom().getY(), 6);
        //TODO: optimize
        Set<Direction> boxSidesTouchingBlocks = AdjacentBlocks.checkTouchingBlock(new AABB(e.getFrom().toVector().add(new Vector(-0.299, 0.001, -0.299)), e.getFrom().toVector().add(new Vector(0.299, 1.799, 0.299))), e.getFrom().getWorld(), 0.1, pp.getClientVersion());

        boolean readyToExit = boxSidesTouchingBlocks.size() > 0 && !e.isInLiquid() && pp.isInLiquid();
        boolean exiting = readyToExit && currentDeltaY == 0.34;
        if(exiting || (wasReadyToExit.contains(pp.getUuid()) && currentDeltaY == 0.3)) {
            reward(pp);
        }

        else if(pp.isSwimming() && !e.hasTeleported() && !e.isOnGroundReally() && !pp.isFlying() && !e.hasAcceptedKnockback()) {
            //TODO: when you're getting pushed down by water, your terminal velocity is < 0.1 when going up, thus bypass when swimming up
            if(Math.abs(currentDeltaY) >= 0.1) {
                //i check when it is >= 0.1 because this game is broken
                //and i don't want work around each individual axis that does this
                //stupid compression-like behavior
                float flowForce = (float)pp.getWaterFlowForce().getY();
                double prevDeltaY = pp.getVelocity().getY();
                Set<Material> liquidTypes = e.getLiquidTypes();
                float kineticPreservation = (liquidTypes.contains(Material.LAVA) || liquidTypes.contains(Material.STATIONARY_LAVA) ? Physics.KINETIC_PRESERVATION_LAVA : Physics.KINETIC_PRESERVATION_WATER);
                if(currentDeltaY < kineticPreservation * prevDeltaY + (-(Physics.MOVE_LIQUID_FORCE + 0.000001) + flowForce) ||
                        currentDeltaY > kineticPreservation * prevDeltaY + (Physics.MOVE_LIQUID_FORCE + 0.000001 + flowForce)) {
                    punishAndTryRubberband(pp, e, pp.getPlayer().getLocation());
                }
                else
                    reward(pp);
            }
        }

        if(readyToExit)
            wasReadyToExit.add(pp.getUuid());
        else
            wasReadyToExit.remove(pp.getUuid());
    }
}
