package net.vicemice.hector.cheat.check.movement.position;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.MovementCheck;
import net.vicemice.hector.cheat.event.MoveEvent;
import net.vicemice.hector.cheat.util.Direction;
import net.vicemice.hector.cheat.util.MathPlus;
import net.vicemice.hector.cheat.wrap.entity.WrappedEntity;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.*;

public class SprintDirection extends MovementCheck {

    private Map<UUID, Long> lastSprintTickMap;
    private Set<UUID> collisionHorizontalSet;

    public SprintDirection() {
        super("sprintdirection", true, 5, 5, 0.999, 5000, "%player% failed sprint direction, VL %vl%", null);
        lastSprintTickMap = new HashMap<>();
        collisionHorizontalSet = new HashSet<>();
    }

    @Override
    protected void check(MoveEvent e) {
        CheatPlayer pp = e.getCheatPlayer();

        boolean collisionHorizontal = collidingHorizontally(e);
        Vector moveHoriz = e.getTo().toVector().subtract(e.getFrom().toVector()).setY(0);

        if(!pp.isSprinting())
            lastSprintTickMap.put(pp.getUuid(), pp.getCurrentTick());

        Set<Material> collidedMats = WrappedEntity.getWrappedEntity(e.getPlayer()).getCollisionBox(e.getFrom().toVector()).getMaterials(pp.getWorld());
        if(pp.isSwimming() || e.hasTeleported() || e.hasAcceptedKnockback() ||
                (collisionHorizontal && !collisionHorizontalSet.contains(pp.getUuid())) ||
                pp.getCurrentTick() - lastSprintTickMap.getOrDefault(pp.getUuid(), pp.getCurrentTick()) < 2 ||
                moveHoriz.lengthSquared() < 0.04 || collidedMats.contains(Material.LADDER) ||
                collidedMats.contains(Material.VINE)) {
            return;
        }

        float yaw = e.getTo().getYaw();
        Vector prevVelocity = pp.getVelocity().clone();
        if(e.hasHitSlowdown()) {
            prevVelocity.multiply(0.6);
        }
        double dX = e.getTo().getX() - e.getFrom().getX();
        double dZ = e.getTo().getZ() - e.getFrom().getZ();
        float friction = e.getFriction();
        dX /= friction;
        dZ /= friction;
        if(e.isJump()) {
            float yawRadians = yaw * 0.017453292F;
            dX += (MathPlus.sin(yawRadians) * 0.2F);
            dZ -= (MathPlus.cos(yawRadians) * 0.2F);
        }

        //Div by 1.7948708571637845???? What the hell are these numbers?

        dX -= prevVelocity.getX();
        dZ -= prevVelocity.getZ();

        Vector moveForce = new Vector(dX, 0, dZ);
        Vector yawVec = MathPlus.getDirection(yaw, 0);

        if(MathPlus.angle(yawVec, moveForce) > Math.PI / 4 + 0.3) { //0.3 is arbitrary. Prevents falses due to silly stuff in game
            punishAndTryRubberband(pp, e, e.getPlayer().getLocation());
        }
        else {
            reward(pp);
        }

        if(collisionHorizontal)
            collisionHorizontalSet.add(pp.getUuid());
        else
            collisionHorizontalSet.remove(pp.getUuid());
    }

    private boolean collidingHorizontally(MoveEvent e) {
        for(Direction dir : e.getBoxSidesTouchingBlocks()) {
            if(dir == Direction.EAST || dir == Direction.NORTH || dir == Direction.SOUTH || dir == Direction.WEST)
                return true;
        }
        return false;
    }

    @Override
    public void removeData(Player p) {
        lastSprintTickMap.remove(p.getUniqueId());
        collisionHorizontalSet.remove(p.getUniqueId());
    }
}
