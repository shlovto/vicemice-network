package net.vicemice.hector.cheat.event;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.wrap.packet.WrappedPacket;
import org.bukkit.entity.Player;

public abstract class Event {

    protected boolean cancelled;
    protected final Player p;
    protected final CheatPlayer pp;
    protected final WrappedPacket wPacket;

    public Event(Player p, CheatPlayer pp, WrappedPacket wPacket) {
        this.p = p;
        this.pp = pp;
        this.wPacket = wPacket;
    }

    //to be implemented by other Events
    public boolean preProcess() {
        return true;
    }

    //to be implemented by other Events
    public void postProcess() {
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    //Forces a reconciliation. Syncs the client to the server's game state for this particular action.
    public void resync() {
    }

    public Player getPlayer() {
        return p;
    }

    public CheatPlayer getCheatPlayer() {
        return pp;
    }

    public WrappedPacket getWrappedPacket() {
        return wPacket;
    }
}