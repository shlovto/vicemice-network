package net.vicemice.hector.cheat.util;

import org.bukkit.Material;

/**
 * ClientBlocks allow Hawk to track the placement of blocks
 * before they get processed by the server. This is very
 * useful for mitigating client-server synchronization delay
 * caused by the server taking nearly up to 100ms to process
 * a block. This should especially be used in movement
 * checks to significantly reduce annoying false-positives,
 * but caution is advised since ClientBlocks may be spoofed.
 */
public class ClientBlock {

    public static final long CLIENTTICKS_UNTIL_EXPIRE = 5;
    public static final int MAX_PER_PLAYER = 16;
    private final Material material;
    private final long initTick;

    public ClientBlock(long clientTick, Material material) {
        this.material = material;
        initTick = clientTick;
    }

    public Material getMaterial() {
        return material;
    }

    public long getInitTick() {
        return initTick;
    }
}
