package net.vicemice.hector.cheat.event.bukkit;

import net.vicemice.hector.cheat.wrap.WrappedWatchableObject;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.List;

public class HawkAsyncPlayerMetadataEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private List<WrappedWatchableObject> metaData;
    private boolean cancelled;

    public HawkAsyncPlayerMetadataEvent(Player player, List<WrappedWatchableObject> metaData) {
        super(true);
        this.player = player;
        this.metaData = metaData;
        this.cancelled = false;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public Player getPlayer() {
        return player;
    }

    public List<WrappedWatchableObject> getMetaData() {
        return metaData;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean b) {
        this.cancelled = b;
    }
}
