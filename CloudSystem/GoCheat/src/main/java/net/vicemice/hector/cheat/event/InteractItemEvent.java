package net.vicemice.hector.cheat.event;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.wrap.packet.WrappedPacket;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;

public class InteractItemEvent extends Event {

    private final Action action;
    private final ItemStack itemStack; //cannot be spoofed; determined server-side based on the slot the client says they're on

    public InteractItemEvent(Player p, CheatPlayer pp, ItemStack itemStack, Action action, WrappedPacket wPacket) {
        super(p, pp, wPacket);
        this.action = action;
        this.itemStack = itemStack;
    }

    @Override
    public void postProcess() {
        Material mat = getItemStack().getType();
        boolean gapple = mat == Material.GOLDEN_APPLE && getItemStack().getDurability() == 1;
        if(action == Action.START_USE_ITEM) {
            if((mat.isEdible() && (p.getFoodLevel() < 20 || gapple) && p.getGameMode() != GameMode.CREATIVE) ||
                    (mat == Material.POTION && getItemStack().getDurability() == 0) || //water bottles
                    (mat == Material.POTION && !Potion.fromItemStack(getItemStack()).isSplash())) {
                pp.setConsumingItem(true);
            }
            if(EnchantmentTarget.WEAPON.includes(mat)) {
                pp.setBlocking(true);
            }
            if(mat == Material.BOW && (p.getInventory().contains(Material.ARROW) || p.getGameMode() == GameMode.CREATIVE)) {
                pp.setPullingBow(true);
            }
        }
        else if(action == Action.RELEASE_USE_ITEM || action == Action.DROP_HELD_ITEM || action == Action.DROP_HELD_ITEM_STACK) {
            pp.setConsumingItem(false);
            pp.setBlocking(false);
            pp.setPullingBow(false);
        }
    }

    public Action getAction() {
        return action;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public enum Action {
        START_USE_ITEM,
        RELEASE_USE_ITEM,
        DROP_HELD_ITEM_STACK,
        DROP_HELD_ITEM
    }
}
