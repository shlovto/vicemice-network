package net.vicemice.hector.cheat.check.interaction.terrain;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.BlockInteractionCheck;
import net.vicemice.hector.cheat.event.InteractWorldEvent;
import net.vicemice.hector.cheat.util.ServerUtils;
import org.bukkit.Material;
import org.bukkit.block.Block;

public class InvalidPlacement extends BlockInteractionCheck {

    public InvalidPlacement() {
        super("invalidplacement", true, 0, 0, 0.999, 5000, "%player% failed invalid placement, VL: %vl%", null);
    }

    @Override
    protected void check(InteractWorldEvent e) {
        CheatPlayer pp = e.getCheatPlayer();
        Block targetedBlock = ServerUtils.getBlockAsync(e.getTargetedBlockLocation());
        if(targetedBlock == null)
            return;
        Material mat = targetedBlock.getType();
        if(targetedBlock.isLiquid() || mat == Material.AIR) {
            punishAndTryCancelAndBlockRespawn(pp, 1, e);
        }
    }
}
