package net.vicemice.hector.cheat.check.movement;

import net.vicemice.hector.cheat.check.CustomCheck;
import net.vicemice.hector.cheat.event.Event;

public class HungerSprint extends CustomCheck {

    public HungerSprint() {
        super("hungersprint", "%player% failed hunger-sprint, VL: %vl%");
    }

    @Override
    protected void check(Event e) {

    }
}
