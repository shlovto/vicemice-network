package net.vicemice.hector.cheat.gui;

import net.vicemice.hector.cheat.GoCheat;
import net.vicemice.hector.cheat.check.Check;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class ToggleChecksWindow extends Window {

    //TODO: Paginate this menu. Max checks per page should be 45 so that we can leave one row for page actions.

    public ToggleChecksWindow(GoCheat goCheat, Player p) {
        super(goCheat, p, 6, ChatColor.GOLD + "Toggle checks");
        List<Check> list = goCheat.getCheckManager().getChecks();

        for (int i = 0; i < list.size(); i++) {
            ItemStack status;
            String display = list.get(i).getName();
            status = new ItemStack(Material.INK_SACK);
            if (list.get(i).isEnabled()) {
                status.setDurability((short) 10);
                display += ": ENABLED";
            } else {
                status.setDurability((short) 8);
                display += ": DISABLED";
            }
            ItemMeta buttonName = status.getItemMeta();
            buttonName.setDisplayName(display);
            status.setItemMeta(buttonName);
            final int location = i;
            elements[i] = new Element(status) {
                @Override
                public void doAction(Player p, GoCheat goCheat) {
                    Check check = goCheat.getCheckManager().getChecks().get(location);
                    check.setEnabled(!check.isEnabled());
                    Window window = new ToggleChecksWindow(goCheat, p);
                    goCheat.getGuiManager().sendWindow(p, window);
                }
            };
        }

        elements[53] = new Element(Material.WOOD_DOOR, ChatColor.RED + "Return to Main Menu") {
            @Override
            public void doAction(Player p, GoCheat goCheat) {
                goCheat.getGuiManager().sendWindow(p, new MainMenuWindow(goCheat, p));
            }
        };

        prepareInventory();
    }
}
