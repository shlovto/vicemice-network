package net.vicemice.hector.cheat.module;

import net.vicemice.hector.cheat.GoCheat;
import net.vicemice.hector.cheat.gui.Element;
import net.vicemice.hector.cheat.gui.MainMenuWindow;
import net.vicemice.hector.cheat.gui.Window;
import net.vicemice.hector.cheat.util.ConfigHelper;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class GUIManager implements Listener {

    private final Map<UUID, Window> activeWindows;
    private final GoCheat goCheat;
    private boolean enabled;

    public GUIManager(GoCheat goCheat) {
        this.goCheat = goCheat;
        activeWindows = new HashMap<>();
        enabled = ConfigHelper.getOrSetDefault(false, goCheat.getConfig(), "gui");
        if (enabled)
            Bukkit.getPluginManager().registerEvents(this, goCheat);
    }

    public void sendWindow(Player p, Window window) {
        if (!enabled) return;
        activeWindows.put(p.getUniqueId(), null);
        activeWindows.put(p.getUniqueId(), window);
        p.openInventory(window.getInventory());
    }

    public void sendMainMenuWindow(Player p) {
        if (!enabled) return;
        Window window = new MainMenuWindow(goCheat, p);
        sendWindow(p, window);
    }

    @EventHandler
    public void clickEvent(InventoryClickEvent e) {
        if (!(e.getWhoClicked() instanceof Player))
            return;
        Player p = (Player) e.getWhoClicked();
        if (!activeWindows.containsKey(p.getUniqueId()) || activeWindows.get(p.getUniqueId()) == null)
            return;
        if (!activeWindows.get(p.getUniqueId()).getInventory().equals(e.getClickedInventory()))
            return;
        e.setCancelled(true);
        String perm = GoCheat.BASE_PERMISSION + ".gui";
        if (!p.hasPermission(perm)) {
            p.sendMessage(String.format(GoCheat.NO_PERMISSION, perm));
            p.closeInventory();
            return;
        }
        Window window = activeWindows.get(p.getUniqueId());
        int clickedLoc = e.getRawSlot();
        for (int i = 0; i < window.getElements().length; i++) {
            if (i == clickedLoc) {
                Element element = window.getElements()[i];
                if(element == null)
                    break;
                element.doAction(p, goCheat);
                break;
            }
        }
    }

    public void stop() {
        for(UUID uuid : activeWindows.keySet()) {
            Player p = Bukkit.getPlayer(uuid);
            if(p == null)
                return;
            p.closeInventory();
        }
        enabled = false;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
