package net.vicemice.hector.cheat.check.interaction.terrain;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.BlockInteractionCheck;
import net.vicemice.hector.cheat.event.InteractWorldEvent;
import net.vicemice.hector.cheat.util.AABB;
import net.vicemice.hector.cheat.util.MathPlus;
import net.vicemice.hector.cheat.util.Placeholder;
import net.vicemice.hector.cheat.util.ServerUtils;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.util.Vector;
import org.bukkit.entity.Player;

public class BlockInteractReach extends BlockInteractionCheck {

    private final float MAX_REACH;
    private final float MAX_REACH_CREATIVE;

    public BlockInteractReach() {
        super("blockbreakreach", "%player% failed block interact reach, Reach: %distance%m, VL: %vl%");
        MAX_REACH = (float) customSetting("maxReach", "", 3.1);
        MAX_REACH_CREATIVE = (float) customSetting("maxReachCreative", "", 5.0);
    }

    @Override
    protected void check(InteractWorldEvent e) {
        Player p = e.getPlayer();
        CheatPlayer pp = e.getCheatPlayer();

        Location bLoc = e.getTargetedBlockLocation();
        Vector min = bLoc.toVector();
        Vector max = bLoc.toVector().add(new Vector(1, 1, 1));
        AABB targetAABB = new AABB(min, max);

        Vector ppPos;
        if(pp.isInVehicle()) {
            ppPos = goCheat.getLagCompensator().getHistoryLocation(ServerUtils.getPing(p), p).toVector();
            ppPos.setY(ppPos.getY() + p.getEyeHeight());
        }
        else {
            ppPos = pp.getHeadPosition();
        }

        double maxReach = pp.getPlayer().getGameMode() == GameMode.CREATIVE ? MAX_REACH_CREATIVE : MAX_REACH;
        double dist = targetAABB.distanceToPosition(ppPos);

        if (dist > maxReach) {
            punish(pp, 1, true, e, new Placeholder("distance", MathPlus.round(dist, 2)));
        } else {
            reward(pp);
        }
    }
}
