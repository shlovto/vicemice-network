package net.vicemice.hector.cheat.util;

public interface MathFunction {

    double func(double x);
}
