package net.vicemice.hector.cheat.util;

import net.vicemice.hector.cheat.GoCheat;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class TextFileReader {

    private final File file;
    private BufferedReader buffer;

    public TextFileReader(GoCheat goCheat, String filename) {
        this.file = new File(goCheat.getDataFolder().getAbsolutePath() + File.separator + filename);
    }

    public void load() throws IOException {
        if (!file.exists()) {
            //noinspection ResultOfMethodCallIgnored
            file.createNewFile();
        }
        buffer = new BufferedReader(new FileReader(file));
    }

    public String readLine() throws IOException {
        return buffer.readLine();
    }

    public List<String> read() throws IOException {
        List<String> result = new ArrayList<>();
        String line = readLine();
        while(line != null) {
            result.add(line);
            line = readLine();
        }
        return result;
    }

    public void reset() {
        try {
            buffer.reset();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            buffer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void overwrite(List<String> data) throws IOException {
        FileWriter fw = new FileWriter(file, false);
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter out = new PrintWriter(bw);
        for (String line : data) {
            out.println(line);
        }
        out.close();
    }
}
