package net.vicemice.hector.cheat.check.interaction.terrain;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.BlockInteractionCheck;
import net.vicemice.hector.cheat.event.InteractWorldEvent;
import org.bukkit.entity.Player;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BlockInteractSpeed extends BlockInteractionCheck {

    private final Map<UUID, Long> lastPlaceTick;

    public BlockInteractSpeed() {
        super("blockplacespeed", "%player% failed block place speed. VL: %vl%");
        lastPlaceTick = new HashMap<>();
    }

    @Override
    protected void check(InteractWorldEvent e) {
        Player p = e.getPlayer();
        CheatPlayer pp = e.getCheatPlayer();
        if (pp.getCurrentTick() == lastPlaceTick.getOrDefault(p.getUniqueId(), 0L))
            punishAndTryCancelAndBlockRespawn(pp, 1, e);
        else
            reward(pp);

        lastPlaceTick.put(p.getUniqueId(), pp.getCurrentTick());
    }

    @Override
    public void removeData(Player p) {
        lastPlaceTick.remove(p.getUniqueId());
    }
}
