package net.vicemice.hector.cheat.check.interaction.terrain;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.BlockDigCheck;
import net.vicemice.hector.cheat.event.BlockDigEvent;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class WrongBlock extends BlockDigCheck {

    //PASSED (9/11/18)

    private final Map<UUID, Block> blockinteracted;

    public WrongBlock() {
        super("wrongblock", "%player% failed wrong block. VL: %vl%");
        blockinteracted = new HashMap<>();
    }

    public void check(BlockDigEvent e) {
        Player p = e.getPlayer();
        CheatPlayer pp = e.getCheatPlayer();
        Block b = e.getBlock();
        if (e.getDigAction() == BlockDigEvent.DigAction.START) {
            blockinteracted.put(p.getUniqueId(), e.getBlock());
        } else if (e.getDigAction() == BlockDigEvent.DigAction.COMPLETE) {
            if ((!blockinteracted.containsKey(p.getUniqueId()) || !b.equals(blockinteracted.get(p.getUniqueId())))) {
                punishAndTryCancelAndBlockRespawn(pp, e);
            } else
                reward(pp);
        }

    }

    public void removeData(Player p) {
        blockinteracted.remove(p.getUniqueId());
    }
}
