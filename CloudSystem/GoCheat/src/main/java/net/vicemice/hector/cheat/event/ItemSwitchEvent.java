package net.vicemice.hector.cheat.event;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.wrap.packet.WrappedPacket;
import org.bukkit.entity.Player;

public class ItemSwitchEvent extends Event {

    private int slotIndex;

    public ItemSwitchEvent(Player p, CheatPlayer pp, int slotIndex, WrappedPacket wPacket) {
        super(p, pp, wPacket);
        this.slotIndex = slotIndex;
    }

    @Override
    public void postProcess() {
        if (!isCancelled()) {
            pp.setHeldItemSlot(getSlotIndex());
            pp.setConsumingItem(false);
            pp.setBlocking(false);
            pp.setPullingBow(false);
        }
    }

    public int getSlotIndex() {
        return slotIndex;
    }
}
