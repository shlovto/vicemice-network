package net.vicemice.hector.cheat.check.movement.look;

import net.vicemice.hector.cheat.check.MovementCheck;
import net.vicemice.hector.cheat.event.MoveEvent;

//Not really an important check. This just stops skids from thinking they're so cool.
public class InvalidPitch extends MovementCheck {

    //PASSED (9/11/18)

    public InvalidPitch() {
        super("invalidpitch", "%player% failed invalid pitch. VL: %vl%");
    }

    @Override
    protected void check(MoveEvent event) {
        if (!event.hasDeltaRot())
            return;
        if (Math.abs(event.getTo().getPitch()) > 90)
            punishAndTryRubberband(event.getCheatPlayer(), event, event.getPlayer().getLocation());
        else
            reward(event.getCheatPlayer());
    }
}
