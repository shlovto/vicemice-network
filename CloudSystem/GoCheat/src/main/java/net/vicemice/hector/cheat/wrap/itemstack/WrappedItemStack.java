package net.vicemice.hector.cheat.wrap.itemstack;

import net.vicemice.hector.cheat.GoCheat;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

public abstract class WrappedItemStack {

    public static WrappedItemStack getWrappedItemStack(ItemStack obiItemStack) {
        if (GoCheat.getServerVersion() == 8)
            return new WrappedItemStack8(obiItemStack);

        return null;
    }

    public abstract float getDestroySpeed(Block obbBlock);

    public abstract boolean canDestroySpecialBlock(Block obbBlock);
}
