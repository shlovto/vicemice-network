package net.vicemice.hector.cheat.check.interaction.entity;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.EntityInteractionCheck;
import net.vicemice.hector.cheat.event.InteractEntityEvent;
import net.vicemice.hector.cheat.wrap.entity.WrappedEntity;
import net.vicemice.hector.cheat.util.AABB;
import net.vicemice.hector.cheat.util.MathPlus;
import net.vicemice.hector.cheat.util.ServerUtils;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class EntityInteractDirection extends EntityInteractionCheck {

    private final int PING_LIMIT;
    private final boolean LAG_COMPENSATION;
    private final boolean CHECK_OTHER_ENTITIES;
    private final double BOX_EXPAND;

    public EntityInteractDirection() {
        super("entityinteractdirection", true, 0, 10, 0.9, 5000, "%player% failed entity interact direction, VL: %vl%", null);
        PING_LIMIT = (int) customSetting("pingLimit", "", -1);
        LAG_COMPENSATION = (boolean) customSetting("lagCompensation", "", true);
        CHECK_OTHER_ENTITIES = (boolean) customSetting("checkOtherEntities", "", true);
        BOX_EXPAND = (double) customSetting("boxExpand", "", 0.2);
    }

    @Override
    protected void check(InteractEntityEvent e) {
        Player p = e.getPlayer();
        CheatPlayer pp = e.getCheatPlayer();
        Entity victimEntity = e.getEntity();
        if (!(victimEntity instanceof Player) && !CHECK_OTHER_ENTITIES)
            return;
        int ping = ServerUtils.getPing(e.getPlayer());
        if (PING_LIMIT > -1 && ping > PING_LIMIT)
            return;
        Vector pos;
        if(pp.isInVehicle()) {
            pos = goCheat.getLagCompensator().getHistoryLocation(ping, p).toVector();
            pos.setY(pos.getY() + p.getEyeHeight());
        }
        else {
            pos = pp.getHeadPosition();
        }
        //As I always say in math class, converting polar coordinates to rectangular coordinates is a pain in
        //the ass. If you want to use the previous direction vector, you cannot make an accurate cut through the AABB
        //using only the previous and extrapolated direction vectors. You'd think you could since the extrapolation
        //on yaw and pitch is linear, but the current direction vector won't lie between the other two. Remember Non-
        //Euclidean geometry!
        Vector dir = MathPlus.getDirection(pp.getYaw(), pp.getPitch());
        //Note: in MC 1.8, the cursor yaw is not updated per frame, but rather per tick.
        //For ray-hitbox checks, this means that we do not need to extrapolate the yaw,
        //but for this check it does not matter whether we do it or not.
        Vector extraDir = MathPlus.getDirection(pp.getYaw() + pp.getDeltaYaw(), pp.getPitch() + pp.getDeltaPitch());

        Location victimLoc;
        if(LAG_COMPENSATION)
            victimLoc = goCheat.getLagCompensator().getHistoryLocation(ping, victimEntity);
        else
            victimLoc = e.getEntity().getLocation();

        AABB victimAABB = WrappedEntity.getWrappedEntity(e.getEntity()).getHitbox(victimLoc.toVector());
        victimAABB.expand(BOX_EXPAND, BOX_EXPAND, BOX_EXPAND);

        //this is a crappy workaround to one of the issues with AABB#betweenRays. I'll keep this here until it is rewritten.
        Vector toVictim = victimLoc.toVector().setY(0).subtract(pos.clone().setY(0));
        boolean behind = toVictim.clone().normalize().dot(dir.clone().setY(0).normalize()) < 0 && toVictim.lengthSquared() > victimAABB.getMax().clone().setY(0).subtract(victimAABB.getMin().clone().setY(0)).lengthSquared();

        if(victimAABB.betweenRays(pos, dir, extraDir) && !behind) {
            reward(pp);
        }
        else {
            punish(pp, true, e);
        }
    }
}
