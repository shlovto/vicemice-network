package net.vicemice.hector.cheat.event.bukkit;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.util.Vector;

public class HawkAsyncPlayerVelocityChangeEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private Vector velocity;
    private Player player;
    private boolean additive;
    private boolean cancelled;

    public HawkAsyncPlayerVelocityChangeEvent(Vector velocity, Player player, boolean additive) {
        super(true);
        this.velocity = velocity;
        this.player = player;
        this.additive = additive;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public Vector getVelocity() {
        return velocity;
    }

    public Player getPlayer() {
        return player;
    }

    public boolean isAdditive() {
        return additive;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean b) {
        this.cancelled = b;
    }
}
