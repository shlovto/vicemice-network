package net.vicemice.hector.cheat.wrap.entity.human;

import org.bukkit.block.Block;

public interface WrappedEntityHuman {

    boolean canHarvestBlock(Block block);

    /**
     * Returns how strong the player is against the specified block at this moment
     */
    float getCurrentPlayerStrVsBlock(Block block, boolean flag);

    void releaseItem();
}
