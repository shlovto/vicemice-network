package net.vicemice.hector.cheat.event;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.wrap.packet.WrappedPacket;
import org.bukkit.entity.Player;

public class PlayerActionEvent extends Event {

    private PlayerAction action;

    public PlayerActionEvent(Player p, CheatPlayer pp, WrappedPacket wPacket, PlayerAction action) {
        super(p, pp, wPacket);
        this.action = action;
    }

    @Override
    public void postProcess() {
        if (!isCancelled()) {
            switch (action) {
                case SNEAK_START:
                    pp.setSneaking(true);
                    break;
                case SNEAK_STOP:
                    pp.setSneaking(false);
                    break;
                case SPRINT_START:
                    pp.setSprinting(true);
                    break;
                case SPRINT_STOP:
                    pp.setSprinting(false);
                    break;
            }
        }
    }

    public PlayerAction getAction() {
        return action;
    }

    public enum PlayerAction {
        SNEAK_START, //1
        SNEAK_STOP, //2
        BED_LEAVE, //3
        SPRINT_START, //4
        SPRINT_STOP, //5
        HORSE_JUMP, //6
        INVENTORY_OPEN,
        UNKNOWN
    }
}
