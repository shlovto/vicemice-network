package net.vicemice.hector.cheat.module;

import net.vicemice.hector.cheat.GoCheat;
import net.vicemice.hector.cheat.check.combat.*;
import net.vicemice.hector.cheat.check.interaction.ClickDuration;
import net.vicemice.hector.cheat.check.interaction.MultiAction;
import net.vicemice.hector.cheat.check.interaction.entity.EntityInteractDirection;
import net.vicemice.hector.cheat.check.interaction.entity.EntityInteractReach;
import net.vicemice.hector.cheat.check.interaction.inventory.InventoryActions;
import net.vicemice.hector.cheat.check.interaction.item.ItemSwitchSpeed;
import net.vicemice.hector.cheat.check.interaction.item.ItemUseSpeed;
import net.vicemice.hector.cheat.check.interaction.terrain.*;
import net.vicemice.hector.cheat.check.movement.ActionToggleSpeed;
import net.vicemice.hector.cheat.check.movement.FabricatedMove;
import net.vicemice.hector.cheat.check.movement.look.AimbotConvergence;
import net.vicemice.hector.cheat.check.movement.look.AimbotHeuristic;
import net.vicemice.hector.cheat.check.movement.look.AimbotPrecision;
import net.vicemice.hector.cheat.check.movement.look.InvalidPitch;
import net.vicemice.hector.cheat.check.movement.position.*;
import net.vicemice.hector.cheat.check.tick.TickRate;
import net.vicemice.hector.cheat.check.*;
import net.vicemice.hector.cheat.event.*;
import org.bukkit.entity.Player;
import java.util.*;

public class CheckManager {

    private final Set<UUID> exemptedPlayers;
    private final Set<UUID> forcedPlayers;

    //make these HashSets?
    private final List<Check> checks;
    private final List<BlockDigCheck> blockDigChecks;
    private final List<BlockInteractionCheck> blockInteractionChecks;
    private final List<CustomCheck> customChecks;
    private final List<EntityInteractionCheck> entityInteractionChecks;
    private final List<MovementCheck> movementChecks;

    public CheckManager(GoCheat goCheat) {
        Check.setHawkReference(goCheat);
        exemptedPlayers = new HashSet<>();
        forcedPlayers = new HashSet<>();
        checks = new ArrayList<>();
        blockDigChecks = new ArrayList<>();
        blockInteractionChecks = new ArrayList<>();
        customChecks = new ArrayList<>();
        entityInteractionChecks = new ArrayList<>();
        movementChecks = new ArrayList<>();
    }

    //initialize checks
    public void loadChecks() {
        new TickRate();
        new FightHitbox();
        new Phase();
        //new Fly();
        new Gravity();
        new Step();
        new BlockBreakSpeedSurvival();
        new Inertia();
        new BlockBreakDirection();
        new WrongBlock();
        new GroundSpoof();
        new FightSpeed();
        new FightAccuracy();
        new AimbotHeuristic();
        new FightNoSwing();
        new AntiVelocityBasic();
        new AntiVelocityJump();
        new InvalidPitch();
        new EntityInteractReach();
        new EntityInteractDirection();
        new BlockInteractDirection();
        new BlockInteractSpeed();
        new WrongBlockFace();
        new InvalidPlacement();
        new ItemSwitchSpeed();
        new ActionToggleSpeed();
        new Speed();
        //new SmallHop();
        //new FastFall();
        new MultiAction();
        new SprintDirection();
        new SwimVertical();
        new ClickDuration();
        new FightSpeedConsistency();
        new AimbotPrecision();
        new ItemUseSpeed();
        new FightSynchronized();
        new FightMulti();
        new AimbotConvergence();
        new Strafe();
        new FabricatedMove();
        new FabricatedBlockInteract();
        //new InventoryMove();
        new InventoryActions();
        //new AimbotExperimental();
    }

    public void unloadChecks() {
        checks.clear();
        blockDigChecks.clear();
        blockInteractionChecks.clear();
        customChecks.clear();
        entityInteractionChecks.clear();
        movementChecks.clear();
    }

    //iterate through appropriate checks
    void dispatchEvent(Event e) {
        for (CustomCheck check : customChecks) {
            check.checkEvent(e);
        }
        if (e instanceof MoveEvent) {
            for (MovementCheck check : movementChecks)
                check.checkEvent((MoveEvent) e);
        } else if (e instanceof InteractEntityEvent) {
            for (EntityInteractionCheck check : entityInteractionChecks)
                check.checkEvent((InteractEntityEvent) e);
        } else if (e instanceof BlockDigEvent) {
            for (BlockDigCheck check : blockDigChecks)
                check.checkEvent((BlockDigEvent) e);
        } else if (e instanceof InteractWorldEvent) {
            for (BlockInteractionCheck check : blockInteractionChecks)
                check.checkEvent((InteractWorldEvent) e);
        }
    }

    public void removeData(Player p) {
        for (Check check : checks)
            check.removeData(p);
    }

    public List<Check> getChecks() {
        return checks;
    }

    public List<BlockDigCheck> getBlockDigChecks() {
        return blockDigChecks;
    }

    public List<BlockInteractionCheck> getBlockInteractionChecks() {
        return blockInteractionChecks;
    }

    public List<CustomCheck> getCustomChecks() {
        return customChecks;
    }

    public List<EntityInteractionCheck> getEntityInteractionChecks() {
        return entityInteractionChecks;
    }

    public List<MovementCheck> getMovementChecks() {
        return movementChecks;
    }

    public Set<UUID> getExemptedPlayers() {
        return exemptedPlayers;
    }

    public Set<UUID> getForcedPlayers() {
        return forcedPlayers;
    }

    public void addExemption(UUID uuid) {
        forcedPlayers.remove(uuid);
        exemptedPlayers.add(uuid);
    }

    public void addForced(UUID uuid) {
        exemptedPlayers.remove(uuid);
        forcedPlayers.add(uuid);
    }

    public void removeExemption(UUID uuid) {
        exemptedPlayers.remove(uuid);
    }

    public void removeForced(UUID uuid) {
        forcedPlayers.remove(uuid);
    }
}
