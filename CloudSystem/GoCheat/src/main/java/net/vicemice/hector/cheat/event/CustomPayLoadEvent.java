package net.vicemice.hector.cheat.event;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.wrap.packet.WrappedPacket;
import org.bukkit.entity.Player;

public class CustomPayLoadEvent extends Event {

    private final String tag;
    private final int length;
    private final byte[] data;

    public CustomPayLoadEvent(String tag, int length, byte[] data, Player p, CheatPlayer pp, WrappedPacket packet) {
        super(p, pp, packet);
        this.tag = tag;
        this.length = length;
        this.data = data;
    }

    public String getTag() {
        return tag;
    }

    public int getLength() {
        return length;
    }

    public byte[] getData() {
        return data;
    }
}
