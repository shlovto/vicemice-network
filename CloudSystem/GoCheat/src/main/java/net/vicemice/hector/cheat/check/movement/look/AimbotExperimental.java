package net.vicemice.hector.cheat.check.movement.look;

import net.vicemice.hector.cheat.check.MovementCheck;
import net.vicemice.hector.cheat.event.MoveEvent;
import net.vicemice.hector.cheat.util.ServerUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class AimbotExperimental extends MovementCheck {

    public AimbotExperimental() {
        super("aimbotD", "%player% failed aimbotD, VL: %vl%");
    }

    @Override
    protected void check(MoveEvent e) {
        Player victim = Bukkit.getPlayer("Islandscout");
        if(victim == null) {
            return;
        }

        //TODO make sure to account for when the attacker is moving
        Vector pos = e.getFrom().toVector();
        float deltaYaw = e.getTo().getYaw() - e.getFrom().getYaw();
        float deltaPitch = e.getTo().getPitch() - e.getFrom().getPitch();

        int ping = ServerUtils.getPing(e.getPlayer()) + 50;
        Vector relEntityPosB = goCheat.getLagCompensator().getHistoryLocationNoLerp(ping, victim).toVector();
        Vector relEntityVelocity = goCheat.getLagCompensator().getHistoryVelocity(ping, victim);
        Vector relEntityPosA = relEntityPosB.clone().add(relEntityVelocity);
        Vector toRelEntityPosA = relEntityPosA.clone().subtract(pos);
        Vector toRelEntityPosB = relEntityPosB.clone().subtract(pos);
        int dir = (int)Math.signum(toRelEntityPosA.clone().crossProduct(toRelEntityPosB.clone()).getY());
        double distBSquared = pos.distanceSquared(relEntityPosB);
        double distASquared = pos.distanceSquared(relEntityPosA);
        //law of cosines
        double bestDeltaYaw = dir * Math.toDegrees(Math.acos((distASquared + distBSquared - relEntityVelocity.lengthSquared()) / (2 * Math.sqrt(distASquared * distBSquared))));

        //TODO integrate these delta yaws and then compare them

        //.broadcastMessage( dir + " " + Math.abs(bestDeltaYaw - deltaYaw));
    }
}
