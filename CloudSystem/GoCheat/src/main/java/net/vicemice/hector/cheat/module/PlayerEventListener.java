package net.vicemice.hector.cheat.module;

import net.vicemice.hector.cheat.GoCheat;
import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.event.bukkit.HawkAsyncPlayerAbilitiesEvent;
import net.vicemice.hector.cheat.event.bukkit.HawkAsyncPlayerMetadataEvent;
import net.vicemice.hector.cheat.event.bukkit.HawkAsyncPlayerVelocityChangeEvent;
import net.vicemice.hector.cheat.util.Pair;
import net.vicemice.hector.cheat.util.ServerUtils;
import net.vicemice.hector.cheat.wrap.WrappedWatchableObject;
import net.vicemice.hector.cheat.wrap.entity.MetaData;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.util.Vector;

import java.util.List;

public class PlayerEventListener implements Listener {

    private final GoCheat goCheat;

    public PlayerEventListener(GoCheat goCheat) {
        this.goCheat = goCheat;

        goCheat.getHawkSyncTaskScheduler().addRepeatingTask(new Runnable() {
            @Override
            public void run() {
                for(CheatPlayer pp : goCheat.getCheatPlayers()) {
                    Player p = pp.getPlayer();
                    pp.setPing(ServerUtils.getPing(p));
                }
            }
        }, 40);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onLogin(PlayerLoginEvent e) {
        Player p = e.getPlayer();
        goCheat.addProfile(p); //This line is necessary since it must get called BEFORE hawk listens to the player's packets
        goCheat.getCheatPlayer(p).setOnline(true);
    }

    //Set protocol version
    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        goCheat.getCheatPlayer(e.getPlayer()).setClientVersion(ServerUtils.getProtocolVersion(e.getPlayer()));
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onQuit(PlayerQuitEvent e) {
        goCheat.removeProfile(e.getPlayer().getUniqueId());
        goCheat.getCheckManager().removeData(e.getPlayer());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onTeleport(PlayerTeleportEvent e) {
        if (!e.getTo().getWorld().equals(e.getFrom().getWorld())) {
            return;
        }
        CheatPlayer pp = goCheat.getCheatPlayer(e.getPlayer());
        pp.setTeleporting(true);
        pp.setWorld(e.getTo().getWorld());
        pp.setTeleportLoc(e.getTo());
        pp.setLastTeleportSendTick(pp.getCurrentTick());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void worldChangeEvent(PlayerChangedWorldEvent e) {
        CheatPlayer pp = goCheat.getCheatPlayer(e.getPlayer());
        pp.setTeleporting(true);
        pp.setWorld(e.getPlayer().getLocation().getWorld());
        pp.setTeleportLoc(e.getPlayer().getLocation());
        pp.setLastTeleportSendTick(pp.getCurrentTick());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onRespawn(PlayerRespawnEvent e) {
        CheatPlayer pp = goCheat.getCheatPlayer(e.getPlayer());
        pp.setTeleporting(true);
        pp.setWorld(e.getRespawnLocation().getWorld());
        pp.setTeleportLoc(e.getRespawnLocation());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onVelocity(HawkAsyncPlayerVelocityChangeEvent e) {
        if(e.isAdditive())
            return;
        CheatPlayer pp = goCheat.getCheatPlayer(e.getPlayer());
        Vector vector = e.getVelocity();

        List<Pair<Vector, Long>> pendingVelocities = pp.getPendingVelocities();
        pendingVelocities.add(new Pair<>(vector, System.currentTimeMillis()));
        if(pendingVelocities.size() > 20)
            pendingVelocities.remove(0);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onInventoryOpenServerSide(InventoryOpenEvent e) {
        HumanEntity hE = e.getPlayer();
        if(!(hE instanceof Player))
            return;

        //Fixes issues regarding the client not releasing item usage when a server inventory is opened.
        //Consumables may not have this issue.
        CheatPlayer pp = goCheat.getCheatPlayer((Player) hE);
        pp.sendSimulatedAction(new Runnable() {
            @Override
            public void run() {
                pp.setBlocking(false);
                pp.setPullingBow(false);
                pp.setInventoryOpen((byte)2);
            }
        });
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onInventoryCloseServerSide(InventoryCloseEvent e) {
        HumanEntity hE = e.getPlayer();
        if(!(hE instanceof Player))
            return;

        CheatPlayer pp = goCheat.getCheatPlayer((Player) hE);
        pp.sendSimulatedAction(new Runnable() {
            @Override
            public void run() {
                pp.setInventoryOpen((byte)0);
            }
        });
    }

    @EventHandler
    public void sendMetadataEvent(HawkAsyncPlayerMetadataEvent e) {
        List<WrappedWatchableObject> objects = e.getMetaData();
        for(WrappedWatchableObject object : objects) {
            if(object.getIndex() == 0) {
                Player p = e.getPlayer();
                CheatPlayer pp = goCheat.getCheatPlayer(p);
                byte status = (byte)object.getObject();

                //bitmask
                if((status & 16) == 16) {
                    pp.addMetaDataUpdate(new MetaData(MetaData.Type.USE_ITEM, true));
                } else {
                    pp.addMetaDataUpdate(new MetaData(MetaData.Type.USE_ITEM, false));
                }
                if((status & 8) == 8) {
                    pp.addMetaDataUpdate(new MetaData(MetaData.Type.SPRINT, true));
                } else {
                    pp.addMetaDataUpdate(new MetaData(MetaData.Type.SPRINT, false));
                }
                /*if((status & 2) == 2) {
                    pp.addMetaDataUpdate(new MetaData(MetaData.Type.SNEAK, true));
                } else {
                    pp.addMetaDataUpdate(new MetaData(MetaData.Type.SNEAK, false));
                }*/

                break;
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void vehicleEnter(VehicleEnterEvent e) {
        if(e.getEntered() instanceof Player) {
            CheatPlayer pp = goCheat.getCheatPlayer((Player)e.getEntered());
            pp.sendSimulatedAction(new Runnable() {
                @Override
                public void run() {
                    pp.setInVehicle(true);
                }
            });
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void vehicleExit(VehicleExitEvent e) {
        if(e.getExited() instanceof Player) {
            CheatPlayer pp = goCheat.getCheatPlayer((Player)e.getExited());
            pp.sendSimulatedAction(new Runnable() {
                @Override
                public void run() {
                    pp.setInVehicle(false);
                }
            });
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void abilitiesServerSide(HawkAsyncPlayerAbilitiesEvent e) {
        CheatPlayer pp = goCheat.getCheatPlayer(e.getPlayer());
        pp.sendSimulatedAction(new Runnable() {
            @Override
            public void run() {
                pp.setAllowedToFly(e.isAllowedToFly());
                pp.setFlying(e.isFlying());
                pp.setInCreative(e.isCreativeMode());
            }
        });
    }
}
