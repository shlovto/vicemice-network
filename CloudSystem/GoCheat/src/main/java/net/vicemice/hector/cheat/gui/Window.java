package net.vicemice.hector.cheat.gui;

import net.vicemice.hector.cheat.GoCheat;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public abstract class Window {

    protected final Inventory inventory;
    protected final GoCheat goCheat;
    protected final Element[] elements;
    protected final Player p;

    public Window(GoCheat goCheat, Player p, int rows, String title) {
        inventory = Bukkit.createInventory(null, rows * 9, title);
        this.p = p;
        this.goCheat = goCheat;
        this.elements = new Element[rows * 9];
    }

    public Inventory getInventory() {
        return inventory;
    }

    public Element[] getElements() {
        return elements;
    }

    protected void prepareInventory() {
        for (int i = 0; i < elements.length; i++) {
            if(elements[i] == null)
                continue;
            inventory.setItem(i, elements[i].getItemStack());
        }
    }

    protected void updateWindow() {
        prepareInventory();
        p.updateInventory();
    }
}
