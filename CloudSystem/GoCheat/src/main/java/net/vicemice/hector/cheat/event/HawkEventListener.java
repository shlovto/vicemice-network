package net.vicemice.hector.cheat.event;

public interface HawkEventListener {

    void onEvent(Event e);
}
