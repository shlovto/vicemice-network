package net.vicemice.hector.cheat.check.combat;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.EntityInteractionCheck;
import net.vicemice.hector.cheat.event.InteractEntityEvent;

public class FightMulti extends EntityInteractionCheck {

    public FightMulti() {
        super("fightmulti", true, 0, 5, 0.999, 5000, "%player% failed fight multi, VL %vl%", null);
    }

    @Override
    protected void check(InteractEntityEvent e) {
        CheatPlayer pp = e.getCheatPlayer();
        if(pp.getEntitiesInteractedInThisTick().size() > 0 && !pp.getEntitiesInteractedInThisTick().contains(e.getEntity())) {
            punish(pp, true, e);
        }
        else {
            reward(pp);
        }
    }
}
