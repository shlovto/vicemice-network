package net.vicemice.hector.cheat.event;

import net.vicemice.hector.cheat.GoCheat;
import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.util.ClientBlock;
import net.vicemice.hector.cheat.wrap.block.WrappedBlock;
import net.vicemice.hector.cheat.wrap.block.WrappedBlock8;
import net.vicemice.hector.cheat.wrap.packet.WrappedPacket;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class BlockDigEvent extends Event {

    private final DigAction digAction;
    private final Block block;

    public BlockDigEvent(Player p, CheatPlayer pp, DigAction action, Block block, WrappedPacket packet) {
        super(p, pp, packet);
        digAction = action;
        this.block = block;
    }

    @Override
    public boolean preProcess() {
        if(pp.isTeleporting()) {
            resync();
            return false;
        }
        return true;
    }

    @Override
    public void postProcess() {
        pp.setDigging(digAction == BlockDigEvent.DigAction.START &&
                !pp.isInCreative() && WrappedBlock.getWrappedBlock(block, pp.getClientVersion()).getStrength() != 0);
        if (!isCancelled() && getDigAction() == BlockDigEvent.DigAction.COMPLETE) {
            ClientBlock clientBlock = new ClientBlock(pp.getCurrentTick(), Material.AIR);
            pp.addClientBlock(getBlock().getLocation(), clientBlock);
        }
    }

    @Override
    public void resync() {
        if (GoCheat.getServerVersion() == 8) {
            WrappedBlock8.getWrappedBlock(getBlock(), pp.getClientVersion()).sendPacketToPlayer(pp.getPlayer());
        }
    }

    public DigAction getDigAction() {
        return digAction;
    }

    public Block getBlock() {
        return block;
    }

    public enum DigAction {
        START,
        CANCEL,
        COMPLETE
    }
}
