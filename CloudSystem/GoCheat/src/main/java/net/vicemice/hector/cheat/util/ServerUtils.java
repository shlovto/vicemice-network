package net.vicemice.hector.cheat.util;

import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.cheat.GoCheat;
import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.wrap.block.WrappedBlock;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public final class ServerUtils {

    private ServerUtils() {
    }

    public static int getPing(Player p) {
        if (GoCheat.getServerVersion() == 8)
            return ((org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer) p).getHandle().ping;
        return -1;
    }

    public static int getProtocolVersion(Player p) {
        if(GoCheat.USING_PLIB)
            return ProtocolLibrary.getProtocolManager().getProtocolVersion(p);
        return 47;
    }

    public static double getTps() {
        if (GoCheat.getServerVersion() == 8)
            return net.minecraft.server.v1_8_R3.MinecraftServer.getServer().recentTps[0];
        return -1;
    }

    public static int getCurrentTick() {
        if (GoCheat.getServerVersion() == 8)
            return net.minecraft.server.v1_8_R3.MinecraftServer.currentTick;
        return -1;
    }

    public static double getStress() {
        if (GoCheat.getServerVersion() == 8)
            return net.minecraft.server.v1_8_R3.MathHelper.a(net.minecraft.server.v1_8_R3.MinecraftServer.getServer().h) * 2.0E-8D;
        return -1;
    }

    //Will return null if chunk in location is not in memory. Do not modify blocks asynchronously!
    public static Block getBlockAsync(Location loc) {
        if (loc.getWorld().isChunkLoaded(loc.getBlockX() >> 4, loc.getBlockZ() >> 4))
            return loc.getBlock();
        return null;
    }

    //Will return null if chunk in location is not in memory. Do not modify chunks asynchronously!
    public static Chunk getChunkAsync(Location loc) {
        if (loc.getWorld().isChunkLoaded(loc.getBlockX() >> 4, loc.getBlockZ() >> 4))
            return loc.getChunk();
        return null;
    }

    public static AABB[] getBlockCollisionBoxesAsyncClientSide(Location loc, CheatPlayer pp) {
        if (loc.getWorld().isChunkLoaded(loc.getBlockX() >> 4, loc.getBlockZ() >> 4)) {
            ClientBlock cb = pp.getClientBlocks().get(loc);
            if (cb == null) {
                return WrappedBlock.getWrappedBlock(loc.getBlock(), pp.getClientVersion()).getCollisionBoxes();
            }

            AABB[] result = {null};
            if (cb.getMaterial().isSolid()) {
                //It would be nice if I can manage to get the correct bounding boxes for a hypothetical block.
                //Bounding boxes depend on the data of the block. Unfortunately, data isn't stored in NMS
                //Block; it's stored in NMS World. So, I'd need to "set" the block on the main thread, but
                //that literally defeats the purpose of this method. My checks NEED this utility on the
                //network thread WHILE the player is placing blocks.
                result[0] = new AABB(loc.toVector(), loc.toVector().clone().add(new Vector(1, 1, 1)));
                return result;
            }
            result[0] = new AABB(new Vector(0, 0, 0), new Vector(0, 0, 0));
            return result;
        }
        return null;
    }
}
