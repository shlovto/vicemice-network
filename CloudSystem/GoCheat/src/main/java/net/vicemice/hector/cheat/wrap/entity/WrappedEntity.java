package net.vicemice.hector.cheat.wrap.entity;

import net.vicemice.hector.cheat.GoCheat;
import net.vicemice.hector.cheat.util.AABB;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public abstract class WrappedEntity {

    AABB collisionBox;
    float collisionBorderSize;
    Location location;

    WrappedEntity() {
    }

    public static WrappedEntity getWrappedEntity(Entity entity) {
        if (GoCheat.getServerVersion() == 8)
            return WrappedEntity8.getWrappedEntity(entity);

        return null;
    }

    public AABB getCollisionBox(Vector entityPos) {
        Vector move = entityPos.clone().subtract(location.toVector());
        AABB box = getCollisionBox().clone();
        box.translate(move);
        return box;
    }

    public AABB getHitbox(Vector entityPos) {
        AABB box = getCollisionBox(entityPos);
        box.expand(collisionBorderSize, collisionBorderSize, collisionBorderSize);
        return box;
    }

    public AABB getCollisionBox() {
        return collisionBox;
    }

    public AABB getHitbox() {
        AABB hitbox = collisionBox.clone();
        hitbox.expand(collisionBorderSize, collisionBorderSize, collisionBorderSize);
        return hitbox;
    }
}
