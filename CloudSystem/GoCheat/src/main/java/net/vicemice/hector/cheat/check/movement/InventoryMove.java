package net.vicemice.hector.cheat.check.movement;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.MovementCheck;
import net.vicemice.hector.cheat.event.MoveEvent;

/*
 * InventoryActions check originally written by Havesta; modified, split apart, and implemented into Hawk by Islandscout
 *
 * InventoryMove checks if a player is
 * - rotating, sprinting, or sneaking while inventory is opened
 */

public class InventoryMove extends MovementCheck {

    public InventoryMove() {
        super("inventorymove", true, 3, 5, 0.999, 5000, "%player% failed inventory-move, VL: %vl%", null);
    }

    @Override
    protected void check(MoveEvent e) {
        CheatPlayer pp = e.getCheatPlayer();

        //TODO: false flag: rotation is still possible at least 1 tick after opening inventory
        //TODO: false flag: you gotta do that TP grace period thing for this too
        if((e.isUpdateRot() || pp.isSprinting() || pp.isSneaking()) && pp.hasInventoryOpen() != 0 && !e.hasTeleported()) {
            punishAndTryRubberband(pp, e, e.getPlayer().getLocation());
        }
        else {
            reward(pp);
        }
    }
}