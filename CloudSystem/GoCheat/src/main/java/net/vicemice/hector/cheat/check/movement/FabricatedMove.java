package net.vicemice.hector.cheat.check.movement;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.MovementCheck;
import net.vicemice.hector.cheat.event.MoveEvent;
import net.vicemice.hector.cheat.util.ServerUtils;
import net.vicemice.hector.cheat.wrap.packet.WrappedPacket;
import org.bukkit.entity.Player;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/*
 * Check written by Havesta
 *
 * FabricatedMove checks if a (C05 packet) or a (C06 packet with updated POSITION) is sent by the client
 * (C06 = position and rotation changed, C04 = position changed, C05 = rotation changed)
 *  with the same rotation as he had before... that means that the player
 *  A: got teleported
 *  or B: is cheating and just cancelling packets and sending new ones
 *  (often used in killaura, scaffold, tower and autopotion)
 *  inspired by HeroCode: https://www.youtube.com/watch?v=3MN9EkPjOZ0
 */
public class FabricatedMove extends MovementCheck {

    private final Map<UUID, Integer> flyingTicksMap;

    public FabricatedMove() {
        super("fabricatedmove", true, 0, 2, 0.999, 5000, "%player% failed fabricated move, VL: %vl%", null);
        flyingTicksMap = new HashMap<>();
    }

    @Override
    protected void check(MoveEvent e) {
        CheatPlayer pp = e.getCheatPlayer();

        if(pp.getPlayer().isInsideVehicle() || e.hasTeleported()) {
            flyingTicksMap.put(pp.getUuid(), 0);
            return;
        }

        //Also ignore some moves after the tp to fix false positives.
        //Use ping because if you send multiple TPs with the same location,
        //and if the player stands still, hawk will register the first one
        //accepted in the batch and ignore the succeeding ones. Add 10 ticks
        //just to be safe. - Islandscout
        if(pp.getCurrentTick() - pp.getLastTeleportAcceptTick() > ServerUtils.getPing(e.getPlayer()) / 50 + 10 &&
                pp.getCurrentTick() > 100) {

            WrappedPacket packet = e.getWrappedPacket();
            switch(packet.getType()) {
                case POSITION:
                    //We can extend the check to position packets by checking
                    //the velocity since the last flying packet. - Islandscout
                    if(!e.hasDeltaPos() && pp.getVelocity().lengthSquared() > 0) {
                        punishAndTryRubberband(e.getCheatPlayer(), e, e.getPlayer().getLocation());
                    } else {
                        reward(e.getCheatPlayer());
                    }
                    break;
                case LOOK:
                    if(!e.hasDeltaRot()) {
                        punishAndTryRubberband(e.getCheatPlayer(), e, e.getPlayer().getLocation());
                    } else {
                        reward(e.getCheatPlayer());
                    }
                    break;
                case POSITION_LOOK:
                    if(!e.hasDeltaRot() || (!e.hasDeltaPos() && pp.getVelocity().lengthSquared() > 0)) {
                        punishAndTryRubberband(e.getCheatPlayer(), e, e.getPlayer().getLocation());
                    } else {
                        reward(e.getCheatPlayer());
                    }
                    break;
            }

            UUID uuid = pp.getUuid();
            int flying = flyingTicksMap.getOrDefault(uuid, 0);

            //moved less than 0.03
            if(e.isUpdatePos() && flying == 0 && e.getTo().distanceSquared(e.getFrom()) < 0.00089) {
                punishAndTryRubberband(e.getCheatPlayer(), e, e.getPlayer().getLocation());
            }

            if(!e.isUpdatePos()) {
                flying++;
            }
            else {
                flying = 0;
            }
            flyingTicksMap.put(uuid, flying);

            //sent more than 20 flying packets in succession
            if(flying > 20) {  // inspired by ToonBasic
                punishAndTryRubberband(pp, flying - 20, e, e.getPlayer().getLocation());
            }
        }
    }

    @Override
    public void removeData(Player p) {
        flyingTicksMap.remove(p.getUniqueId());
    }
}
