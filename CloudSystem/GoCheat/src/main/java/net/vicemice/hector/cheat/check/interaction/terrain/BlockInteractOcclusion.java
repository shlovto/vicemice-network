package net.vicemice.hector.cheat.check.interaction.terrain;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.BlockInteractionCheck;
import net.vicemice.hector.cheat.event.InteractWorldEvent;
import net.vicemice.hector.cheat.util.MathPlus;
import net.vicemice.hector.cheat.util.Placeholder;
import net.vicemice.hector.cheat.util.Ray;
import net.vicemice.hector.cheat.wrap.block.WrappedBlock;
import net.vicemice.hector.cheat.util.AABB;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

public class BlockInteractOcclusion extends BlockInteractionCheck {

    public BlockInteractOcclusion() {
        super("blockinteractocclusion", "%player% failed block interact occlusion, Type: %type%, VL: %vl%");
    }

    @Override
    protected void check(InteractWorldEvent e) {
        CheatPlayer pp = e.getCheatPlayer();
        Vector eyePos = pp.getPosition().clone().add(new Vector(0, pp.isSneaking() ? 1.54 : 1.62, 0));
        Vector direction = MathPlus.getDirection(pp.getYaw(), pp.getPitch());

        Location bLoc = e.getTargetedBlockLocation();
        Block b = bLoc.getBlock();
        WrappedBlock bNMS = WrappedBlock.getWrappedBlock(b, pp.getClientVersion());
        AABB targetAABB = new AABB(bNMS.getHitBox().getMin(), bNMS.getHitBox().getMax());

        double distance = targetAABB.distanceToPosition(eyePos);
        BlockIterator iter = new BlockIterator(pp.getWorld(), eyePos, direction, 0, (int) distance + 2);
        while (iter.hasNext()) {
            Block bukkitBlock = iter.next();

            if (bukkitBlock.getType() == Material.AIR || bukkitBlock.isLiquid())
                continue;
            if (bukkitBlock.getLocation().equals(bLoc))
                break;

            WrappedBlock iterBNMS = WrappedBlock.getWrappedBlock(bukkitBlock, pp.getClientVersion());
            AABB checkIntersection = new AABB(iterBNMS.getHitBox().getMin(), iterBNMS.getHitBox().getMax());
            Vector occludeIntersection = checkIntersection.intersectsRay(new Ray(eyePos, direction), 0, Float.MAX_VALUE);
            if (occludeIntersection != null) {
                if (occludeIntersection.distance(eyePos) < distance) {
                    Placeholder ph = new Placeholder("type", iterBNMS.getBukkitBlock().getType());
                    punishAndTryCancelAndBlockRespawn(pp, 1, e, ph);
                    return;
                }
            }
        }
    }
}
