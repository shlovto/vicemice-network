package net.vicemice.hector.cheat.check.interaction.terrain;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.BlockInteractionCheck;
import net.vicemice.hector.cheat.event.InteractWorldEvent;
import net.vicemice.hector.cheat.util.AABB;
import net.vicemice.hector.cheat.util.MathPlus;
import net.vicemice.hector.cheat.util.Ray;
import net.vicemice.hector.cheat.util.ServerUtils;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class BlockInteractDirection extends BlockInteractionCheck {

    private final double BOX_EXPAND;
    private final boolean DEBUG_HITBOX;
    private final boolean DEBUG_RAY;

    public BlockInteractDirection() {
        super("blockinteractdirection", true, 10, 10, 0.9, 5000, "%player% failed block interact direction, VL: %vl%", null);
        BOX_EXPAND = (double) customSetting("boxExpand", "", 0.2);
        DEBUG_HITBOX = (boolean) customSetting("hitbox", "debug", false);
        DEBUG_RAY = (boolean) customSetting("ray", "debug", false);
    }

    @Override
    protected void check(InteractWorldEvent e) {
        Player p = e.getPlayer();
        CheatPlayer pp = e.getCheatPlayer();
        Location bLoc = e.getTargetedBlockLocation();
        Vector pos;
        if(pp.isInVehicle()) {
            pos = goCheat.getLagCompensator().getHistoryLocation(ServerUtils.getPing(p), p).toVector();
            pos.setY(pos.getY() + p.getEyeHeight());
        }
        else {
            pos = pp.getHeadPosition();
        }
        Vector dir = MathPlus.getDirection(pp.getYaw(), pp.getPitch());
        Vector extraDir = MathPlus.getDirection(pp.getYaw() + pp.getDeltaYaw(), pp.getPitch() + pp.getDeltaPitch());

        Vector min = bLoc.toVector();
        Vector max = bLoc.toVector().add(new Vector(1, 1, 1));
        AABB targetAABB = new AABB(min, max);
        targetAABB.expand(BOX_EXPAND, BOX_EXPAND, BOX_EXPAND);

        if (DEBUG_HITBOX)
            targetAABB.highlight(goCheat, p.getWorld(), 0.25);
        if (DEBUG_RAY)
            new Ray(pos, extraDir).highlight(goCheat, p.getWorld(), 6F, 0.3);

        if(targetAABB.betweenRays(pos, dir, extraDir)) {
            reward(pp);
        }
        else {
            punish(pp, true, e);
        }
    }
}
