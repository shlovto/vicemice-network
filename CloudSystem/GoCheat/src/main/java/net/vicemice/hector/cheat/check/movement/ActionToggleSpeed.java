package net.vicemice.hector.cheat.check.movement;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.CustomCheck;
import net.vicemice.hector.cheat.event.Event;
import net.vicemice.hector.cheat.event.PlayerActionEvent;
import org.bukkit.entity.Player;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * This check limits clients' sprint and sneak toggle rates to
 * prevent exploitation of the speed check and combat mechanics.
 */
public class ActionToggleSpeed extends CustomCheck {

    private Map<UUID, Long> lastSneakToggle;
    private Map<UUID, Long> lastSprintToggle;

    public ActionToggleSpeed() {
        super("actiontogglespeed", "%player% failed action toggle speed, VL: %vl%");
        lastSneakToggle = new HashMap<>();
        lastSprintToggle = new HashMap<>();
    }

    @Override
    protected void check(Event e) {
        if(!(e instanceof PlayerActionEvent))
            return;
        PlayerActionEvent aE = (PlayerActionEvent)e;
        PlayerActionEvent.PlayerAction action = aE.getAction();
        CheatPlayer pp = e.getCheatPlayer();
        UUID uuid = e.getPlayer().getUniqueId();
        if(action == PlayerActionEvent.PlayerAction.SNEAK_START || action == PlayerActionEvent.PlayerAction.SNEAK_STOP) {

            if(pp.getCurrentTick() - lastSneakToggle.getOrDefault(uuid, 0L) < 1) {
                punish(pp, canCancel(), e);
            } else {
                reward(pp);
            }

            lastSneakToggle.put(uuid, pp.getCurrentTick());
        } else if(action == PlayerActionEvent.PlayerAction.SPRINT_START || action == PlayerActionEvent.PlayerAction.SPRINT_STOP) {

            if(pp.getCurrentTick() - lastSprintToggle.getOrDefault(uuid, 0L) < 1) {
                punish(pp, canCancel(), e);
            } else {
                reward(pp);
            }

            lastSprintToggle.put(uuid, pp.getCurrentTick());
        }
    }

    public void removeData(Player p) {
        UUID uuid = p.getUniqueId();
        lastSneakToggle.remove(uuid);
        lastSprintToggle.remove(uuid);
    }
}
