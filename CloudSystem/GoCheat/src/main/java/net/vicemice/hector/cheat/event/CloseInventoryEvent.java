package net.vicemice.hector.cheat.event;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.wrap.packet.WrappedPacket;
import org.bukkit.entity.Player;

public class CloseInventoryEvent extends Event {

    public CloseInventoryEvent(Player p, CheatPlayer pp, WrappedPacket wPacket) {
        super(p, pp, wPacket);
    }

    @Override
    public void postProcess() {
        pp.setInventoryOpen((byte)0);
    }
}
