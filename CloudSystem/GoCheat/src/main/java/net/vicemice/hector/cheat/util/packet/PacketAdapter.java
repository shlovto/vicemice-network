package net.vicemice.hector.cheat.util.packet;

import org.bukkit.entity.Player;

public interface PacketAdapter {

    void run(Object packet, Player player);
}
