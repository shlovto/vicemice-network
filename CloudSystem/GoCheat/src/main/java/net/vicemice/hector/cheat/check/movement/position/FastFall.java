package net.vicemice.hector.cheat.check.movement.position;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.MovementCheck;
import net.vicemice.hector.cheat.event.MoveEvent;
import net.vicemice.hector.cheat.util.AdjacentBlocks;
import org.bukkit.Location;
import org.bukkit.Material;

/**
 * Significantly limits y-port speed bypasses
 */
public class FastFall extends MovementCheck {

    //TODO: You need to support "insignificant" moves

    public FastFall() {
        super("fastfall", "%player% failed fast-fall, VL: %vl%");
    }

    @Override
    protected void check(MoveEvent e) {
        CheatPlayer pp = e.getCheatPlayer();
        if(AdjacentBlocks.onGroundReally(new Location(pp.getWorld(), pp.getPosition().getX(), pp.getPosition().getY(), pp.getPosition().getZ()), -1, false, 0.001, pp) ||
                e.hasTeleported() || e.getPlayer().isFlying() || e.hasAcceptedKnockback() || pp.getPlayer().isSleeping() || pp.isSwimming())
            return;
        double deltaY = e.getTo().getY() - e.getFrom().getY();
        double expected = (pp.getVelocity().getY() - 0.08F) * 0.98F;
        Location chkPos = e.getFrom().clone().add(0, 2.5, 0);

        //I HATE this game's movement.
        if(!AdjacentBlocks.blockAdjacentIsSolid(chkPos) &&
                !AdjacentBlocks.blockNearbyIsSolid(chkPos, false) &&
                !AdjacentBlocks.matIsAdjacent(e.getTo(), Material.LADDER, Material.VINE)) {
            if(deltaY + 0.02 < expected) {
                punishAndTryRubberband(pp, e, e.getPlayer().getLocation());
            }
            else {
                reward(pp);
            }
        }
    }
}
