package net.vicemice.hector.cheat.check.interaction.item;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.CustomCheck;
import net.vicemice.hector.cheat.event.Event;
import net.vicemice.hector.cheat.event.InteractItemEvent;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ItemUseSpeed extends CustomCheck {

    private Map<UUID, Long> lastEventTick;

    public ItemUseSpeed() {
        super("itemusespeed", true, 5, 5, 0.99, 5000, "%player% failed item-use speed, VL: %vl%", null);
        lastEventTick = new HashMap<>();
    }

    @Override
    protected void check(Event e) {
        if(!(e instanceof InteractItemEvent)) {
            return;
        }
        InteractItemEvent.Action action = ((InteractItemEvent) e).getAction();
        if(action == InteractItemEvent.Action.DROP_HELD_ITEM || action == InteractItemEvent.Action.DROP_HELD_ITEM_STACK) {
            return;
        }
        CheatPlayer pp = e.getCheatPlayer();
        UUID uuid = pp.getUuid();
        long currTick = pp.getCurrentTick();
        if(currTick == lastEventTick.getOrDefault(uuid, -1L)) {
            punish(pp, true, e);
        }
        else {
            reward(pp);
        }
        lastEventTick.put(uuid, pp.getCurrentTick());
    }

    @Override
    public void removeData(Player p) {
        lastEventTick.remove(p.getUniqueId());
    }
}
