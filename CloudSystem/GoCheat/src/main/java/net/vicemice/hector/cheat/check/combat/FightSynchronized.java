package net.vicemice.hector.cheat.check.combat;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.CustomCheck;
import net.vicemice.hector.cheat.check.Cancelless;
import net.vicemice.hector.cheat.event.Event;
import net.vicemice.hector.cheat.event.InteractAction;
import net.vicemice.hector.cheat.event.InteractEntityEvent;
import net.vicemice.hector.cheat.event.MoveEvent;
import net.vicemice.hector.cheat.util.Pair;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * FightSynchronized exploits a flaw in aim-bot/aim-assist cheats
 * by comparing the player's last attack time to their last move
 * time. Although easily bypassed, it catches a significant
 * number of cheaters.
 */
public class FightSynchronized extends CustomCheck implements Cancelless {

    private final Map<UUID, Long> attackTime;
    private final Map<UUID, Pair<Integer, Integer>> sample;
    private final int SAMPLE_SIZE;
    private static final int THRESHOLD = 6;

    public FightSynchronized() {
        super("fightsync", true, -1, 2, 0.95, 5000, "%player% may be using killaura (sync). VL %vl%", null);
        attackTime = new HashMap<>();
        sample = new HashMap<>();
        SAMPLE_SIZE = (int)customSetting("sampleSize", "", 10);
    }

    @Override
    public void check(Event event) {
        if(event instanceof MoveEvent) {
            processMove((MoveEvent)event);
        }
        else if(event instanceof InteractEntityEvent) {
            processHit((InteractEntityEvent)event);
        }
    }

    private void processMove(MoveEvent e) {
        Player p = e.getPlayer();
        CheatPlayer pp = e.getCheatPlayer();
        long lastAttackTime = attackTime.getOrDefault(p.getUniqueId(), -50L);
        long mToA = lastAttackTime - pp.getLastMoveTime(); //difference between previous move time to last attack time
        long aToM = System.currentTimeMillis() - lastAttackTime; //difference between last attack time to now

        if(mToA < 0) {
            return; //last attack was more than 50ms ago
        }

        if(mToA + aToM < 40) {
            return; //connection catching up from lag spike, so ignore
        }

        Pair<Integer, Integer> ratio = sample.getOrDefault(p.getUniqueId(), new Pair<>(0, 0));
        ratio.setValue(ratio.getValue() + 1);

        if(mToA < THRESHOLD) {
            ratio.setKey(ratio.getKey() + 1);
        }

        if(ratio.getValue() >= SAMPLE_SIZE) {
            if(ratio.getKey() / (double)ratio.getValue() > 0.9) {
                punish(pp, false, e);
            }
            else {
                reward(pp);
            }
            ratio.setKey(0);
            ratio.setValue(0);
        }

        sample.put(p.getUniqueId(), ratio);
    }

    private void processHit(InteractEntityEvent e) {
        if(e.getInteractAction() == InteractAction.ATTACK)
            attackTime.put(e.getPlayer().getUniqueId(), System.currentTimeMillis());
    }

    public void removeData(Player p) {
        attackTime.remove(p.getUniqueId());
        sample.remove(p.getUniqueId());
    }
}
