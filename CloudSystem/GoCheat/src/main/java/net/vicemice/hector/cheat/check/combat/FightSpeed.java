package net.vicemice.hector.cheat.check.combat;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.EntityInteractionCheck;
import net.vicemice.hector.cheat.event.InteractAction;
import net.vicemice.hector.cheat.event.InteractEntityEvent;
import net.vicemice.hector.cheat.util.MathPlus;
import net.vicemice.hector.cheat.util.Placeholder;
import org.bukkit.entity.Player;

import java.util.*;

public class FightSpeed extends EntityInteractionCheck {

    //This check is very similar to the FightSpeedConsistency check, however I will
    //not be merging these, because their SAMPLES variable may vary
    //based on configuration. For instance, FightSpeedConsistency relies on samples
    //of 5, yet this check relies on samples of 10.

    private final Map<UUID, Long> lastClickTime; //in client ticks
    private final Map<UUID, List<Long>> deltaTimes;
    private static final double RECORD_SENSITIVITY = 4; //don't log click if it took longer than these ticks
    private static int SAMPLES;
    private final double MAX_CPS;

    public FightSpeed() {
        super("fightspeed", "%player% failed attack speed. CPS: %cps%, VL: %vl%");
        lastClickTime = new HashMap<>();
        deltaTimes = new HashMap<>();
        SAMPLES = (int)customSetting("sampleSize", "", 10);
        MAX_CPS = (double)customSetting("maxCps", "", 16D);
    }

    @Override
    protected void check(InteractEntityEvent e) {
        if (e.getInteractAction() == InteractAction.INTERACT)
            return;
        UUID uuid = e.getPlayer().getUniqueId();
        CheatPlayer pp = e.getCheatPlayer();
        if (lastClickTime.containsKey(uuid)) {
            List<Long> deltaTs = deltaTimes.getOrDefault(uuid, new ArrayList<>());
            long deltaT = (pp.getCurrentTick() - lastClickTime.get(uuid));
            if (deltaT <= RECORD_SENSITIVITY) {
                deltaTs.add(deltaT);
                if (deltaTs.size() >= SAMPLES) {
                    double avgCps = 0;
                    for (double entry : deltaTs) {
                        avgCps += entry;
                    }
                    double divisor = (avgCps / SAMPLES / 20);
                    avgCps = 1 / divisor;
                    //if someone manages to get an Infinity, they're dumb af
                    if (avgCps > MAX_CPS) {
                        punish(pp, 1, true, e, new Placeholder("cps", (Double.isInfinite(avgCps) ? avgCps : MathPlus.round(avgCps, 2) + "")));
                    } else {
                        reward(pp);
                    }
                    deltaTs.remove(0);
                }
                deltaTimes.put(uuid, deltaTs);
            }
        }
        lastClickTime.put(uuid, pp.getCurrentTick());
    }

    @Override
    public void removeData(Player p) {
        UUID uuid = p.getUniqueId();
        lastClickTime.remove(uuid);
        deltaTimes.remove(uuid);
    }
}
