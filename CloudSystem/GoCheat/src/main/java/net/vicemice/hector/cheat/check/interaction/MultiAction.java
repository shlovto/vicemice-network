package net.vicemice.hector.cheat.check.interaction;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.CustomCheck;
import net.vicemice.hector.cheat.event.*;

public class MultiAction extends CustomCheck {

    public MultiAction() {
        super("multiaction", false, 0, 10, 0.95, 5000, "%player% failed multi-action, VL: %vl%", null);
    }

    @Override
    protected void check(Event event) {
        if(!(event instanceof InteractEntityEvent ||
                event instanceof InteractWorldEvent ||
                event instanceof BlockDigEvent ||
                event instanceof InteractItemEvent))
            return;

        CheatPlayer pp = event.getCheatPlayer();

        //interacting while using item
        if(!(event instanceof InteractItemEvent) && pp.getClientVersion() == 8 &&
                (pp.isBlocking() || pp.isConsumingOrPullingBowMetadataIncluded())) {
            punish(pp, 1, true, event);
            event.resync();
        }
        //interacting while digging
        else if(pp.getClientVersion() == 8 && pp.isDigging() &&
                !(event instanceof BlockDigEvent && ((BlockDigEvent) event).getDigAction() != BlockDigEvent.DigAction.START)) {
            punish(pp, 1, true, event);
            event.resync();
        }
        else {
            reward(pp);
        }
    }
}
