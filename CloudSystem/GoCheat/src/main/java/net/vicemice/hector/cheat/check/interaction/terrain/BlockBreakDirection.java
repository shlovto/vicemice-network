package net.vicemice.hector.cheat.check.interaction.terrain;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.BlockDigCheck;
import net.vicemice.hector.cheat.event.BlockDigEvent;
import net.vicemice.hector.cheat.util.AABB;
import net.vicemice.hector.cheat.util.MathPlus;
import net.vicemice.hector.cheat.util.Ray;
import net.vicemice.hector.cheat.util.ServerUtils;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class BlockBreakDirection extends BlockDigCheck {

    private final boolean DEBUG_HITBOX;
    private final boolean DEBUG_RAY;
    private final boolean CHECK_DIG_START;
    private final boolean CHECK_DIG_CANCEL;
    private final boolean CHECK_DIG_COMPLETE;

    public BlockBreakDirection() {
        super("blockbreakdirection", true, 5, 10, 0.9, 5000, "%player% failed block break direction, VL: %vl%", null);
        DEBUG_HITBOX = (boolean) customSetting("hitbox", "debug", false);
        DEBUG_RAY = (boolean) customSetting("ray", "debug", false);
        CHECK_DIG_START = (boolean) customSetting("checkDigStart", "", false);
        CHECK_DIG_CANCEL = (boolean) customSetting("checkDigCancel", "", false);
        CHECK_DIG_COMPLETE = (boolean) customSetting("checkDigComplete", "", true);
    }

    @Override
    protected void check(BlockDigEvent e) {
        Player p = e.getPlayer();
        CheatPlayer pp = e.getCheatPlayer();
        Location bLoc = e.getBlock().getLocation();
        Vector pos;
        if(pp.isInVehicle()) {
            pos = goCheat.getLagCompensator().getHistoryLocation(ServerUtils.getPing(p), p).toVector();
            pos.setY(pos.getY() + p.getEyeHeight());
        }
        else {
            pos = pp.getHeadPosition();
        }
        Vector dir = MathPlus.getDirection(pp.getYaw(), pp.getPitch());
        Vector extraDir = MathPlus.getDirection(pp.getYaw() + pp.getDeltaYaw(), pp.getPitch() + pp.getDeltaPitch());

        switch (e.getDigAction()) {
            case START:
                if(CHECK_DIG_START || (p.getGameMode() == GameMode.CREATIVE && CHECK_DIG_COMPLETE))
                    break;
                return;
            case CANCEL:
                if(CHECK_DIG_CANCEL)
                    break;
                return;
            case COMPLETE:
                if(CHECK_DIG_COMPLETE)
                    break;
                return;
        }

        Vector min = bLoc.toVector();
        Vector max = bLoc.toVector().add(new Vector(1, 1, 1));
        AABB targetAABB = new AABB(min, max);

        if (DEBUG_HITBOX)
            targetAABB.highlight(goCheat, p.getWorld(), 0.25);
        if (DEBUG_RAY)
            new Ray(pos, extraDir).highlight(goCheat, p.getWorld(), 6F, 0.3);

        if(targetAABB.betweenRays(pos, dir, extraDir)) {
            reward(pp);
        }
        else {
            punish(pp, true, e);
        }
    }
}
