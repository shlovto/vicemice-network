package net.vicemice.hector.cheat.check.movement.position;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.MovementCheck;
import net.vicemice.hector.cheat.check.Cancelless;
import net.vicemice.hector.cheat.event.MoveEvent;

/**
 * The AntiVelocityBasic check verifies that players accept knockback.
 * It relies on MoveEvent's knockback acceptance detection.
 * The AntiVelocityBasic check has been tested to detect as low as a
 * 10% difference in horizontal and/or vertical knockback.
 */
public class AntiVelocityBasic extends MovementCheck implements Cancelless {

    public AntiVelocityBasic() {
        super("antivelocitybasic", true, -1, 5, 0.999, 5000, "%player% may be using anti-velocity (basic), VL: %vl%", null);
    }

    @Override
    protected void check(MoveEvent event) {
        CheatPlayer pp = event.getCheatPlayer();
        if(event.hasFailedKnockback())
            punish(pp, false, event);
        else
            reward(pp);
    }
}
