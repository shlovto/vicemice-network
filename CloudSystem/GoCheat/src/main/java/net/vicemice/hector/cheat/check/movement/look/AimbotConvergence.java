package net.vicemice.hector.cheat.check.movement.look;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.CustomCheck;
import net.vicemice.hector.cheat.event.Event;
import net.vicemice.hector.cheat.event.MoveEvent;
import net.vicemice.hector.cheat.util.Pair;
import net.vicemice.hector.cheat.util.Ray;
import net.vicemice.hector.cheat.util.ServerUtils;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import java.util.*;

public class AimbotConvergence extends CustomCheck {

    //You better turn off your aimbot when your opponent is standing still!
    //Flags basic aimbots that update rotation before motion updates

    private Map<UUID, Vector> lastConvergencePointMap;

    public AimbotConvergence() {
        super("aimbotconvergence", true, -1, 5, 0.999, 5000, "%player% failed aimbot (convergence), VL: %vl%", null);
        this.lastConvergencePointMap = new HashMap<>();
    }

    @Override
    protected void check(Event event) {
        if(event instanceof MoveEvent) {
            MoveEvent e = (MoveEvent) event;
            CheatPlayer pp = e.getCheatPlayer();
            if(!e.hasDeltaPos())
                return;
            UUID uuid = pp.getUuid();
            Vector prePos = e.getFrom().toVector().clone().subtract(pp.getVelocity()).add(new Vector(0, 1.62, 0));
            Vector postPos = e.getFrom().toVector().add(new Vector(0, 1.62, 0));
            Vector preDir = e.getFrom().getDirection();
            Vector postDir = e.getTo().getDirection();
            Pair<Vector, Vector> points = new Ray(prePos, preDir).closestPointsBetweenLines(new Ray(postPos, postDir));

            Vector lastConvergence = lastConvergencePointMap.get(uuid);
            Vector convergence = points.getKey().add(points.getValue()).multiply(0.5);

            if(lastConvergence != null &&
                    //make sure TPs don't false this check
                    pp.getCurrentTick() - pp.getLastTeleportAcceptTick() > ServerUtils.getPing(e.getPlayer()) / 50 + 10 &&
                    pp.getCurrentTick() > 100) {

                double distance = lastConvergence.distanceSquared(convergence);
                if(!Double.isNaN(distance)) {

                    if(distance < 0.00000001)
                        punish(pp, false, e);
                    else
                        reward(pp);

                }
            }

            lastConvergencePointMap.put(uuid, convergence);
        }
    }

    @Override
    public void removeData(Player p) {
        lastConvergencePointMap.remove(p.getUniqueId());
    }
}