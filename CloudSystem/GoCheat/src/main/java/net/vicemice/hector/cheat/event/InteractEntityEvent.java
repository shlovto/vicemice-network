package net.vicemice.hector.cheat.event;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.wrap.packet.WrappedPacket;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class InteractEntityEvent extends Event {

    private final InteractAction interactAction;
    private final Entity entity;
    private final Vector intersectVector;

    public InteractEntityEvent(Player p, CheatPlayer pp, InteractAction action, Entity entity, Vector intersectVector, WrappedPacket packet) {
        super(p, pp, packet);
        interactAction = action;
        this.entity = entity;
        this.intersectVector = intersectVector;
    }

    @Override
    public boolean preProcess() {
        return !pp.isTeleporting();
    }

    @Override
    public void postProcess() {
        //We won't ignore if it's cancelled because otherwise that would set off
        //movement false flags regarding the hit slowdown mechanic. (Look at the
        //MoveEvent class for more information.)
        pp.updateLastEntityInteractTick();
        pp.addEntityToEntitiesInteractedInThisTick(entity);
        if(/*!isCancelled() && */getInteractAction() == InteractAction.ATTACK) {
            pp.updateItemUsedForAttack();
            if(getEntity() instanceof Player) {
                pp.updateLastAttackedPlayerTick();
                ItemStack heldItem = pp.getItemUsedForAttack();
                if(pp.isSprinting() || (heldItem != null && heldItem.getEnchantmentLevel(Enchantment.KNOCKBACK) > 0)) {
                    pp.updateHitSlowdownTick();
                }
            }
        }
    }

    public InteractAction getInteractAction() {
        return interactAction;
    }

    public Entity getEntity() {
        return entity;
    }

    public Vector getIntersectVector() {
        return intersectVector;
    }
}
