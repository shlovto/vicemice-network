package net.vicemice.hector.cheat.util;

public enum Direction {

    TOP, BOTTOM, NORTH, SOUTH, EAST, WEST, NONE

}
