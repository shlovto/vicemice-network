package net.vicemice.hector.cheat.gui;

import net.vicemice.hector.cheat.GoCheat;
import net.vicemice.hector.cheat.CheatPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class MainMenuWindow extends Window {

    public MainMenuWindow(GoCheat goCheat, Player player) {
        super(goCheat, player, 1, ChatColor.GOLD + "GoCheat");
        CheatPlayer pp = goCheat.getCheatPlayer(player);

        /*elements[0] = new Element(Material.SAND, "dummy") {
            @Override
            public void doAction(Player p, Hawk hawk) {
                Window testWindow = new TestWindow(hawk, p);
                hawk.getGuiManager().sendWindow(p, testWindow);
            }
        };*/

        elements[4] = new Element(Material.WORKBENCH, "Toggle Checks") {
            @Override
            public void doAction(Player p, GoCheat goCheat) {
                Window checks = new ToggleChecksWindow(goCheat, p);
                goCheat.getGuiManager().sendWindow(p, checks);
            }
        };

        elements[5] = new Element(Material.PAPER, "Reload Configuration") {
            @Override
            public void doAction(Player p, GoCheat goCheat) {
                Bukkit.dispatchCommand(p, "hawk reload");
            }
        };

        ItemStack notify = new ItemStack(Material.INK_SACK);
        notify.setDurability((short) (pp.canReceiveAlerts() ? 10 : 8));
        ItemMeta notifyName = notify.getItemMeta();
        notifyName.setDisplayName(pp.canReceiveAlerts() ? "Notifications: ON" : "Notifications: OFF");
        notify.setItemMeta(notifyName);
        elements[3] = new Element(notify) {
            @Override
            public void doAction(Player p, GoCheat goCheat) {
                pp.setReceiveNotifications(!pp.canReceiveAlerts());
                Window mainMenu = new MainMenuWindow(goCheat, p);
                goCheat.getGuiManager().sendWindow(p, mainMenu);
            }
        };

        elements[8] = new Element(Material.WOOD_DOOR, "Exit GUI") {
            @Override
            public void doAction(Player p, GoCheat goCheat) {
                p.closeInventory();
            }
        };

        prepareInventory();
    }
}
