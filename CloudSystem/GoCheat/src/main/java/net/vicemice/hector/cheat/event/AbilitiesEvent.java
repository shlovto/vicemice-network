package net.vicemice.hector.cheat.event;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.wrap.packet.WrappedPacket;
import org.bukkit.entity.Player;

/**
 * The vanilla client sends this packet when the player starts/stops
 * flying with the Flags parameter changed accordingly. All other parameters
 * are ignored by the vanilla server. (Wiki.vg)
 **/
public class AbilitiesEvent extends Event {

    private final boolean flying;

    public AbilitiesEvent(Player p, CheatPlayer pp, boolean flying, WrappedPacket packet) {
        super(p, pp, packet);
        this.flying = flying;
    }

    @Override
    public void postProcess() {
        if (!isCancelled() && isFlying()) {
            pp.setFlyPendingTime(System.currentTimeMillis());
        }
        pp.setFlying(flying);
    }

    public boolean isFlying() {
        return flying;
    }
}
