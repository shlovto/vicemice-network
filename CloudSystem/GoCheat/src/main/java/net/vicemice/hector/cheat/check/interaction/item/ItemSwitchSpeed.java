package net.vicemice.hector.cheat.check.interaction.item;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.Cancelless;
import net.vicemice.hector.cheat.check.CustomCheck;
import net.vicemice.hector.cheat.event.InteractWorldEvent;
import net.vicemice.hector.cheat.event.Event;
import net.vicemice.hector.cheat.event.ItemSwitchEvent;
import org.bukkit.entity.Player;
import java.util.*;

public class ItemSwitchSpeed extends CustomCheck implements Cancelless {

    private Map<UUID, Long> lastSwitchTicks;
    private Set<UUID> usedSomething;
    private int MIN_SWITCH_TICKS;

    //might rename this to "AutoUse"
    public ItemSwitchSpeed() {
        super("itemswitchspeed", true, -1, 5, 0.99, 5000, "%player% failed item switch speed, VL: %vl%", null);
        lastSwitchTicks = new HashMap<>();
        usedSomething = new HashSet<>();
        MIN_SWITCH_TICKS = (int)customSetting("minSwitchTicks", "", 2);
    }

    @Override
    protected void check(Event event) {
        if(event instanceof InteractWorldEvent) {
            usedSomething.add(event.getPlayer().getUniqueId());
            return;
        }
        if(!(event instanceof ItemSwitchEvent)) {
            return;
        }

        CheatPlayer pp = event.getCheatPlayer();
        UUID uuid = pp.getUuid();

        if(usedSomething.contains(uuid)) {
            long lastSwitchTick = lastSwitchTicks.getOrDefault(uuid, 0L);
            if(pp.getCurrentTick() - lastSwitchTick < MIN_SWITCH_TICKS) {
                punish(pp, 1, false, event);
            }
            else {
                reward(pp);
            }

            usedSomething.remove(uuid);
        }

        lastSwitchTicks.put(pp.getUuid(), pp.getCurrentTick());
    }

    @Override
    public void removeData(Player p) {
        UUID uuid = p.getUniqueId();
        lastSwitchTicks.remove(uuid);
        usedSomething.remove(uuid);
    }
}
