package net.vicemice.hector.cheat.check.combat;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.Cancelless;
import net.vicemice.hector.cheat.check.EntityInteractionCheck;
import net.vicemice.hector.cheat.util.MathPlus;
import net.vicemice.hector.cheat.event.InteractAction;
import net.vicemice.hector.cheat.event.InteractEntityEvent;
import org.bukkit.entity.Player;
import java.util.*;

public class FightSpeedConsistency extends EntityInteractionCheck implements Cancelless {

    //This check is very similar to the FightSpeed check, however I will
    //not be merging these, because their SAMPLES variable may vary
    //based on configuration. For instance, FightSpeed relies on samples
    //of 10, yet this check relies on samples of 5.

    private final int SAMPLES;
    private final int SAMPLE_SIZE;
    private final double STDEV_THRESHOLD;
    private final double MIN_CPS;
    private final int RECORD_THRESHOLD; //don't log click if it took longer than these ticks

    private final Map<UUID, List<Double>> pSamples;
    private final Map<UUID, Long> lastClickTick;
    private final Map<UUID, List<Long>> deltaTimes;

    public FightSpeedConsistency() {
        super("fightspeedconsistency", false, -1, 3, 0.99, 5000, "%player% failed click consistency. Autoclicker? VL: %vl%", null);
        MIN_CPS = (double)customSetting("checkAboveCPS", "", 10D);
        SAMPLES = (int)customSetting("samples", "", 5);
        SAMPLE_SIZE = (int)customSetting("sampleSize", "", 20);
        STDEV_THRESHOLD = (double)customSetting("stdevThreshold", "", 0.05D);
        RECORD_THRESHOLD = (int)(20 * (1 / MIN_CPS) + 1);
        pSamples = new HashMap<>();
        lastClickTick = new HashMap<>();
        deltaTimes = new HashMap<>();
    }

    @Override
    protected void check(InteractEntityEvent e) {
        if (e.getInteractAction() == InteractAction.INTERACT)
            return;
        UUID uuid = e.getPlayer().getUniqueId();
        CheatPlayer pp = e.getCheatPlayer();
        if (lastClickTick.containsKey(uuid)) {
            List<Long> deltaTs = deltaTimes.getOrDefault(uuid, new ArrayList<>());
            long deltaT = (pp.getCurrentTick() - lastClickTick.get(uuid));

            //log click if player clicks faster than this
            if (deltaT <= RECORD_THRESHOLD) {
                deltaTs.add(deltaT);

                //begin computing avg cps
                if (deltaTs.size() >= SAMPLE_SIZE) {
                    double avgCps = 0;
                    for (double entry : deltaTs) {
                        avgCps += entry;
                    }
                    double divisor = (avgCps / SAMPLE_SIZE / 20);
                    avgCps = 1 / divisor;

                    List<Double> sampless = pSamples.getOrDefault(uuid, new ArrayList<>());
                    sampless.add(avgCps);

                    //Debug.broadcastMessage(avgCps + "");

                    //Begin computing standard deviation and judge player.
                    //Only if above min cps and enough samples.
                    //Acts like a narrow barrier between the two limits to prevent false flags.
                    //Explanation: When clicks are barely meeting speed requirements, a narrow
                    //range of CPSs will be processed, thus setting off false flags.
                    if(avgCps > MIN_CPS && sampless.size() >= SAMPLES) {
                        //Debug.broadcastMessage(ChatColor.YELLOW + "" + MathPlus.stdev(sampless));
                        if(MathPlus.stdev(sampless) < STDEV_THRESHOLD) {
                            punish(pp, false, e);
                        }
                        else {
                            reward(pp);
                        }
                    }

                    if(sampless.size() >= SAMPLES) {
                        sampless.remove(0);
                    }
                    pSamples.put(uuid, sampless);
                    deltaTs.remove(0);
                }
                deltaTimes.put(uuid, deltaTs);
            }
        }
        lastClickTick.put(uuid, pp.getCurrentTick());
    }

    @Override
    public void removeData(Player p) {
        pSamples.remove(p.getUniqueId());
        lastClickTick.remove(p.getUniqueId());
        deltaTimes.remove(p.getUniqueId());
    }
}
