package net.vicemice.hector.cheat.event.bukkit;

import net.vicemice.hector.cheat.util.Violation;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class HawkFlagEvent extends Event {

    private static final HandlerList handlers = new HandlerList();
    private final Violation violation;

    public HawkFlagEvent(Violation violation) {
        super();
        this.violation = violation;
    }

    public Violation getViolation() {
        return violation;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
