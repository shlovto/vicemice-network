package net.vicemice.hector.cheat.wrap.entity.human;

import net.vicemice.hector.cheat.GoCheat;
import net.vicemice.hector.cheat.wrap.block.WrappedBlock;
import net.vicemice.hector.cheat.wrap.entity.WrappedEntity8;
import net.minecraft.server.v1_8_R3.EntityHuman;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;

public class WrappedEntityHuman8 extends WrappedEntity8 implements WrappedEntityHuman {

    public WrappedEntityHuman8(Entity entity) {
        super(entity);
    }

    @Override
    public boolean canHarvestBlock(Block block) {
        Object obj = WrappedBlock.getWrappedBlock(block, GoCheat.getServerVersion()).getNMS();
        net.minecraft.server.v1_8_R3.Block b = (net.minecraft.server.v1_8_R3.Block) obj;
        return ((EntityHuman) nmsEntity).b(b);
    }

    @Override
    public float getCurrentPlayerStrVsBlock(Block block, boolean flag) {
        Object obj = WrappedBlock.getWrappedBlock(block, GoCheat.getServerVersion()).getNMS();
        net.minecraft.server.v1_8_R3.Block b = (net.minecraft.server.v1_8_R3.Block) obj;
        return ((EntityHuman) nmsEntity).a(b);
    }

    @Override
    public void releaseItem() {
        ((EntityHuman) nmsEntity).bU();
    }
}
