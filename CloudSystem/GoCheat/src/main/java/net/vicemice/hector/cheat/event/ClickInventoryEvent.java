package net.vicemice.hector.cheat.event;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.wrap.packet.WrappedPacket;
import org.bukkit.entity.Player;

public class ClickInventoryEvent extends Event {

    private int slot;
    private int windowID;
    private int mode;
    private int button;

    public ClickInventoryEvent(Player p, CheatPlayer pp, int slot, int windowID, int mode, int button, WrappedPacket wPacket) {
        super(p, pp, wPacket);
        this.slot = slot;
        this.windowID = windowID;
        this.mode = mode;
        this.button = button;
    }

    @Override
    public void resync() {
        p.updateInventory();
    }

    public int getSlot() {
        return slot;
    }

    public int getWindowID() {
        return windowID;
    }

    public int getMode() {
        return mode;
    }

    public int getButton() {
        return button;
    }
}
