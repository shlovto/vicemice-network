package net.vicemice.hector.cheat.wrap;

/**
 * A wrapper for Minecraft's WatchableObject class
 */
public class WrappedWatchableObject {

    private final int objectType;
    private final int dataValueId;
    private Object watchedObject;
    private boolean watched;

    public WrappedWatchableObject(int type, int id, Object object) {
        this.dataValueId = id;
        this.watchedObject = object;
        this.objectType = type;
        this.watched = true;
    }

    public int getIndex() {
        return this.dataValueId;
    }

    public void setObject(Object object) {
        this.watchedObject = object;
    }

    public Object getObject() {
        return this.watchedObject;
    }

    public int getObjectType() {
        return this.objectType;
    }

    public boolean isWatched() {
        return this.watched;
    }

    public void setWatched(boolean status) {
        this.watched = status;
    }
}
