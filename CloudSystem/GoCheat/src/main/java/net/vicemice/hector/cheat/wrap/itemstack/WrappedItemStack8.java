package net.vicemice.hector.cheat.wrap.itemstack;

import net.vicemice.hector.cheat.GoCheat;
import net.vicemice.hector.cheat.wrap.block.WrappedBlock;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

public class WrappedItemStack8 extends WrappedItemStack {

    private net.minecraft.server.v1_8_R3.ItemStack itemStack;

    WrappedItemStack8(ItemStack obiItemStack) {
        itemStack = CraftItemStack.asNMSCopy(obiItemStack);
    }

    @Override
    public float getDestroySpeed(Block obbBlock) {
        net.minecraft.server.v1_8_R3.Block block =
                (net.minecraft.server.v1_8_R3.Block) WrappedBlock.getWrappedBlock(obbBlock, GoCheat.getServerVersion()).getNMS();
        if(itemStack == null)
            return 1F;
        return itemStack.a(block);
    }

    @Override
    public boolean canDestroySpecialBlock(Block obbBlock) {
        net.minecraft.server.v1_8_R3.Block block =
                (net.minecraft.server.v1_8_R3.Block) WrappedBlock.getWrappedBlock(obbBlock, GoCheat.getServerVersion()).getNMS();
        if(itemStack == null)
            return false;
        return itemStack.b(block);
    }
}
