package net.vicemice.hector.cheat.check.interaction;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.BlockDigCheck;
import net.vicemice.hector.cheat.check.Cancelless;
import net.vicemice.hector.cheat.event.BlockDigEvent;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Detects basic autoclickers that click and then release instantaneously.
 * Only works when aiming at solid blocks.
 */
public class ClickDuration extends BlockDigCheck implements Cancelless {

    //Inspired by this resource
    //https://www.spigotmc.org/resources/autokiller-advanced-auto-clicker-detection.40520/

    private Map<UUID, Long> digStartTick;

    public ClickDuration() {
        super("clickduration", false, -1, 0, 0.99, 5000, "%player% failed click duration. Autoclicker? VL: %vl%", null);
        digStartTick = new HashMap<>();
    }

    @Override
    protected void check(BlockDigEvent e) {
        BlockDigEvent.DigAction action = e.getDigAction();
        CheatPlayer pp = e.getCheatPlayer();
        if(action == BlockDigEvent.DigAction.START)
            digStartTick.put(pp.getUuid(), pp.getCurrentTick());
        else if(action == BlockDigEvent.DigAction.CANCEL) {
            if(pp.getCurrentTick() == digStartTick.getOrDefault(pp.getUuid(), -10L)) {
                punish(pp, false, e);
            }
            else {
                reward(pp);
            }
        }
    }

    @Override
    public void removeData(Player p) {
        digStartTick.remove(p.getUniqueId());
    }
}
