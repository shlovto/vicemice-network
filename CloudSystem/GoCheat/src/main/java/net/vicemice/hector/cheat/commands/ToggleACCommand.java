package net.vicemice.hector.cheat.commands;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.GoCheat;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.players.NetworkPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ToggleACCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        Player player = (Player) commandSender;
        NetworkPlayer networkPlayer = GoServer.getService().getPlayerManager().getPlayer(player.getUniqueId());

        if (!networkPlayer.getRank().isModeration()){
            player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-no-permission"));
            return true;
        }

        CheatPlayer pp = GoCheat.getInstance().getCheatPlayer(player);
        pp.setReceiveNotifications(!pp.canReceiveAlerts());

        if (pp.canReceiveAlerts()) {
            commandSender.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-toggle-receive"));
        } else {
            commandSender.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-toggle-not-receive"));
        }

        return false;
    }
}