package net.vicemice.hector.cheat.check.interaction.inventory;

/*
 * InventoryActions check originally written by Havesta; modified, split apart
 *
 * InventoryActions checks if a player is
 * - interacting with entities/blocks while inventory is opened
 */

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.CustomCheck;
import net.vicemice.hector.cheat.event.*;

public class InventoryActions extends CustomCheck {

    public InventoryActions() {
        super("inventoryactions", true, 3, 5, 0.999, 5000, "%player% failed inventory-actions, VL: %vl%", null);
    }

    @Override
    protected void check(Event e) {
        CheatPlayer pp = e.getCheatPlayer();
        if(pp.hasInventoryOpen() != 0 && (e instanceof InteractEntityEvent || e instanceof BlockDigEvent ||
                e instanceof ArmSwingEvent || e instanceof InteractWorldEvent)) {
            punish(pp, true, e);
            e.resync();
            //TODO After failing several times, there's a chance that they could be legit, but the inventory state is glitched. Close the player's inventory.
        }
        else if(pp.hasInventoryOpen() == 0 && e instanceof ClickInventoryEvent) {
            punish(pp, true, e);
            e.resync();
        }
        else {
            reward(pp);
        }
    }
}
