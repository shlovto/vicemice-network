package net.vicemice.hector.cheat.check;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.event.BlockDigEvent;
import net.vicemice.hector.cheat.util.Placeholder;

import java.util.List;

public abstract class BlockDigCheck extends Check<BlockDigEvent> {

    protected BlockDigCheck(String name, boolean enabled, int cancelThreshold, int flagThreshold, double vlPassMultiplier, long flagCooldown, String flag, List<String> punishCommands) {
        super(name, enabled, cancelThreshold, flagThreshold, vlPassMultiplier, flagCooldown, flag, punishCommands);
        goCheat.getCheckManager().getBlockDigChecks().add(this);
    }

    protected BlockDigCheck(String name, String flag) {
        this(name, true, 0, 5, 0.9, 5000, flag, null);
    }

    protected void punishAndTryCancelAndBlockRespawn(CheatPlayer offender, BlockDigEvent event, Placeholder... placeholders) {
        punishAndTryCancelAndBlockRespawn(offender, 1, event, placeholders);
    }

    protected void punishAndTryCancelAndBlockRespawn(CheatPlayer offender, double vlAmnt, BlockDigEvent event, Placeholder... placeholders) {
        punish(offender, vlAmnt, true, event, placeholders);
        if (offender.getVL(this) >= cancelThreshold)
            event.resync();
    }
}
