package net.vicemice.hector.cheat.gui;

import net.vicemice.hector.cheat.GoCheat;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public abstract class Element {

    protected ItemStack itemStack;
    protected String name;

    Element(Material mat, String name) {
        this.name = name;
        this.itemStack = new ItemStack(mat);
        ItemMeta checksName = itemStack.getItemMeta();
        checksName.setDisplayName(name);
        itemStack.setItemMeta(checksName);
    }

    Element(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public void setItemStack(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void doAction(Player p, GoCheat goCheat);
}
