package net.vicemice.hector.cheat.module;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.vicemice.hector.cheat.GoCheat;
import org.bukkit.Bukkit;

public class BungeeBridge {

    private final GoCheat goCheat;
    private final boolean enabled;

    public BungeeBridge(GoCheat goCheat, boolean enabled) {
        this.goCheat = goCheat;
        if(enabled)
            goCheat.getServer().getMessenger().registerOutgoingPluginChannel(goCheat, "BungeeCord");
        this.enabled = enabled;
    }

    public void sendAlertForBroadcast(String msg) {
        if(!enabled)
            return;
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("HawkACAlert");
        out.writeUTF(msg);
        Bukkit.getServer().sendPluginMessage(goCheat, "BungeeCord", out.toByteArray());
    }

}
