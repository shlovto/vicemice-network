package net.vicemice.hector.cheat.module;

import net.vicemice.hector.cheat.GoCheat;

import java.util.ArrayList;
import java.util.List;

public class HawkSyncTaskScheduler implements Runnable {

    private long currentTick;
    private final GoCheat goCheat;
    private List<HawkTask> tasks;
    private static int hawkTaskInstances;

    public HawkSyncTaskScheduler(GoCheat goCheat) {
        this.goCheat = goCheat;
        this.tasks = new ArrayList<>();
    }

    @Override
    public void run() {
        for(HawkTask hawkTask : tasks) {
            if(currentTick % hawkTask.interval == 0) {
                hawkTask.task.run();
            }
        }
        currentTick++;
    }

    //Don't harass me for reinventing the wheel; I'm trying to make my life easier.
    public int addRepeatingTask(Runnable task, int interval) {
        HawkTask hawkTask = new HawkTask(task, interval);
        tasks.add(hawkTask);
        return hawkTask.id;
    }

    public void cancelTask(int id) {
        for(int i = 0; i < tasks.size(); i++) {
            HawkTask task = tasks.get(i);
            if(id == task.id) {
                tasks.remove(i);
                break;
            }
        }
    }

    private class HawkTask {

        private Runnable task;
        private int interval;
        private int id;

        private HawkTask(Runnable task, int interval) {
            this.task = task;
            this.interval = interval;
            this.id = hawkTaskInstances;
            hawkTaskInstances++;
        }
    }
}
