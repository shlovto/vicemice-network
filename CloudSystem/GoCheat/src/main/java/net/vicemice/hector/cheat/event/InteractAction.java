package net.vicemice.hector.cheat.event;

public enum InteractAction {

    ATTACK, INTERACT
}
