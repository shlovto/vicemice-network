package net.vicemice.hector.cheat.wrap.packet;

import io.netty.buffer.Unpooled;
import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PacketDataSerializer;

import java.io.IOException;

public class WrappedPacket8 extends WrappedPacket {

    public WrappedPacket8(Packet obj, PacketType type) {
        super(obj, type);
    }

    public Packet getPacket() {
        return (Packet) packet;
    }

    public void setByte(int index, int value) {
        PacketDataSerializer serializer = new PacketDataSerializer(Unpooled.buffer(256));
        try {
            ((Packet) packet).b(serializer); //"b" method writes to PacketDataSerializer (reads from packet)
            serializer.setByte(index, value);
            ((Packet) packet).a(serializer); //"a" method interprets PacketDataSerializer (writes to packet)
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public byte[] getBytes() {
        PacketDataSerializer serializer = new PacketDataSerializer(Unpooled.buffer(256));
        try {
            ((Packet) packet).b(serializer); //"b" method writes to PacketDataSerializer (reads from packet)
        } catch (IOException e) {
            e.printStackTrace();
        }
        return serializer.array();
    }

    public Object readPacket() {
        PacketDataSerializer serializer = new PacketDataSerializer(Unpooled.buffer(0));
        try {
            ((Packet) packet).b(serializer); //"b" method writes to PacketDataSerializer (reads from packet)
        } catch (IOException e) {
            e.printStackTrace();
        }
        return serializer;
    }

    public void overwritePacket(Object packetDataSerializer) {
        PacketDataSerializer serializer = (PacketDataSerializer)packetDataSerializer;
        try {
            ((Packet) packet).a(serializer); //"a" method interprets PacketDataSerializer (writes to packet)
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
