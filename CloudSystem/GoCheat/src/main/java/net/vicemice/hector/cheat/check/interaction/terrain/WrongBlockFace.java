package net.vicemice.hector.cheat.check.interaction.terrain;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.BlockInteractionCheck;
import net.vicemice.hector.cheat.event.InteractWorldEvent;
import net.vicemice.hector.cheat.util.AABB;
import net.vicemice.hector.cheat.util.MathPlus;
import net.vicemice.hector.cheat.util.ServerUtils;
import net.vicemice.hector.cheat.wrap.block.WrappedBlock;
import org.bukkit.block.Block;
import org.bukkit.util.Vector;
import org.bukkit.entity.Player;

/** This check prevents players from interacting on
 * unavailable locations on blocks. Players must be
 * looking at the face of the block they want to interact
 * with.
 */
public class WrongBlockFace extends BlockInteractionCheck {

    public WrongBlockFace() {
        super("wrongblockface", true, 0, 10, 0.99, 5000, "%player% failed wrongblockface; interacted on invalid block face, VL: %vl%", null);
    }

    @Override
    protected void check(InteractWorldEvent e) {
        CheatPlayer pp = e.getCheatPlayer();

        Block b = ServerUtils.getBlockAsync(e.getTargetedBlockLocation());
        AABB hitbox;
        if(b != null) {
            hitbox = WrappedBlock.getWrappedBlock(b, pp.getClientVersion()).getHitBox();
        }
        else {
            hitbox = new AABB(new Vector(), new Vector());
        }

        Vector headPos;
        if(pp.isInVehicle()) {
            Player p = e.getPlayer();
            headPos = goCheat.getLagCompensator().getHistoryLocation(ServerUtils.getPing(p), p).toVector();
            headPos.setY(headPos.getY() + p.getEyeHeight());
        }
        else {
            headPos = pp.getHeadPosition();
        }

        if(e.getTargetedBlockFaceNormal().dot(MathPlus.getDirection(pp.getYaw(), pp.getPitch())) >= 0 &&
            !hitbox.containsPoint(headPos)) {
            punishAndTryCancelAndBlockRespawn(pp, e);
        }
        else {
            reward(pp);
        }
    }
}
