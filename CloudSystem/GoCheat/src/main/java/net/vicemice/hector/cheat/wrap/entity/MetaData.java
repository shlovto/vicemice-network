package net.vicemice.hector.cheat.wrap.entity;

public class MetaData {

    private Type type;
    private boolean value;

    public MetaData(Type type, boolean value) {
        this.type = type;
        this.value = value;
    }

    public Type getType() {
        return type;
    }

    public boolean getValue() {
        return value;
    }

    public enum Type {
        USE_ITEM,
        SPRINT,
        SNEAK
    }
}
