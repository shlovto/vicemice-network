package net.vicemice.hector.cheat.check.combat;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.CustomCheck;
import net.vicemice.hector.cheat.event.ArmSwingEvent;
import net.vicemice.hector.cheat.event.Event;
import net.vicemice.hector.cheat.event.InteractAction;
import net.vicemice.hector.cheat.event.InteractEntityEvent;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class FightNoSwing extends CustomCheck {

    //PASSED (9/11/18)

    private final Map<UUID, Long> lastClientTickSwung;

    public FightNoSwing() {
        super("fightnoswing", "%player% failed noswing. VL: %vl%");
        lastClientTickSwung = new HashMap<>();
    }

    @Override
    protected void check(Event event) {
        if (event instanceof ArmSwingEvent)
            processSwing((ArmSwingEvent) event);
        else if (event instanceof InteractEntityEvent)
            processHit((InteractEntityEvent) event);

    }

    private void processSwing(ArmSwingEvent e) {
        lastClientTickSwung.put(e.getPlayer().getUniqueId(), e.getCheatPlayer().getCurrentTick());
    }

    private void processHit(InteractEntityEvent e) {
        if (e.getInteractAction() != InteractAction.ATTACK)
            return;
        Player p = e.getPlayer();
        CheatPlayer pp = e.getCheatPlayer();
        if (!lastClientTickSwung.containsKey(p.getUniqueId()) || pp.getCurrentTick() != lastClientTickSwung.get(p.getUniqueId())) {
            punish(pp, 1, true, e);
        } else {
            reward(pp);
        }
    }

    @Override
    public void removeData(Player p) {
        lastClientTickSwung.remove(p.getUniqueId());
    }
}
