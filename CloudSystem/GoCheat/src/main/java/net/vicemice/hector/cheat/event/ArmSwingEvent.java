package net.vicemice.hector.cheat.event;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.wrap.packet.WrappedPacket;
import org.bukkit.entity.Player;

public class ArmSwingEvent extends Event {

    private final int type;

    public ArmSwingEvent(Player p, CheatPlayer pp, int type, WrappedPacket packet) {
        super(p, pp, packet);
        this.type = type;
    }

    public int getType() {
        return type;
    }

    //TODO post process? Check if all swing packets are forwarded to nearby players. If this is true, we need to implement a rate limiter to mitigate network congestion.
}
