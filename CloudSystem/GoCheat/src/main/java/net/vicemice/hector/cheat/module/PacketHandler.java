package net.vicemice.hector.cheat.module;

import net.vicemice.hector.cheat.GoCheat;
import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.module.listener.PacketListener;
import net.vicemice.hector.cheat.module.listener.PacketListener8;
import net.vicemice.hector.cheat.util.ConfigHelper;
import net.vicemice.hector.cheat.util.packet.PacketAdapter;
import net.vicemice.hector.cheat.util.packet.PacketConverter8;
import net.vicemice.hector.cheat.event.Event;
import net.vicemice.hector.cheat.event.HawkEventListener;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * This class is mainly used to process packets that are intercepted from the Netty channels.
 * Remember, caution is advised when accessing the Bukkit API from the Netty thread.
 */
public class PacketHandler implements Listener {

    //Welcome to Minecraft protocol damnation.

    private final int serverVersion;
    private final GoCheat goCheat;
    private PacketListener packetListener;
    private List<HawkEventListener> hawkEventListeners;
    private final boolean async;

    public PacketHandler(GoCheat goCheat) {
        this.serverVersion = GoCheat.getServerVersion();
        this.goCheat = goCheat;
        async = ConfigHelper.getOrSetDefault(false, goCheat.getConfig(), "asyncChecking");
        if(async) {
            goCheat.getLogger().warning("---");
            goCheat.getLogger().warning("It appears that you have enabled ASYNCHRONOUS packet checking.");
            goCheat.getLogger().warning("Although this will significantly improve network performance, it");
            goCheat.getLogger().warning("will not prevent cheating. You will not receive any support for");
            goCheat.getLogger().warning("any bypasses that you encounter. You have been warned.");
            goCheat.getLogger().warning("---");
        }
        hawkEventListeners = new CopyOnWriteArrayList<>();
        Bukkit.getPluginManager().registerEvents(this, goCheat);
    }

    //These packets will be converted into Hawk Events for verification by checks
    public boolean processIn(Object packet, Player p) {
        CheatPlayer pp = goCheat.getCheatPlayer(p);

        //ignore packets while player is no longer registered in Hawk
        if (!pp.isOnline())
            return false;

        Event event = convertPacketInboundToEvent(packet, pp);
        if(event == null)
            return true;

        if(!event.preProcess())
            return false;

        for(HawkEventListener eventListener : hawkEventListeners)
            eventListener.onEvent(event);

        goCheat.getCheckManager().dispatchEvent(event);

        event.postProcess();

        return !event.isCancelled();
    }

    //These packets will be converted into Bukkit Events and will be broadcasted using Bukkit's event system
    public boolean processOut(Object packet, Player p) {
        org.bukkit.event.Event event = convertPacketOutboundToEvent(packet, p);
        if (event == null)
            return true;

        Bukkit.getServer().getPluginManager().callEvent(event);

        if(event instanceof Cancellable) {
            return !((Cancellable) event).isCancelled();
        }

        return true;
    }

    private Event convertPacketInboundToEvent(Object packet, CheatPlayer pp) {
        Event event;
        if (serverVersion == 8)
            event = PacketConverter8.packetInboundToEvent(packet, pp.getPlayer(), pp);
        else
            return null;
        return event;
    }

    private org.bukkit.event.Event convertPacketOutboundToEvent(Object packet, Player p) {
        org.bukkit.event.Event event;
        if (serverVersion == 8)
            event = PacketConverter8.packetOutboundToEvent(packet, p);
        else
            return null;
        return event;
    }

    public PacketListener getPacketListener() {
        return packetListener;
    }

    public void startListener() {
        if (serverVersion == 8) {
            packetListener = new PacketListener8(this, async);
            goCheat.getLogger().info("Using NMS 1.8_R3 NIO for packet interception.");
        } else {
            warnConsole(goCheat);
            return;
        }
        packetListener.enable();
    }

    private void warnConsole(GoCheat goCheat) {
        goCheat.getLogger().severe("!!!!!!!!!!");
        goCheat.getLogger().severe("It appears that you are not running Hawk on a supported server version.");
        goCheat.getLogger().severe("We will NOT work. Please run the GoCheat on a 1.8_R3 server. If you");
        goCheat.getLogger().severe("are confident that you are running the correct version of the server,");
        goCheat.getLogger().severe("please verify that the package \"net.minecraft.server.[VERSION]\" in your");
        goCheat.getLogger().severe("Spigot JAR contains one of the above specified versions.");
        goCheat.getLogger().severe("!!!!!!!!!!");
        goCheat.disable();
    }

    public void stopListener() {
        if(packetListener != null)
            packetListener.disable();
    }

    public void setupListenerForOnlinePlayers() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            goCheat.getCheatPlayer(p).setOnline(true);
            setupListenerForPlayer(p);
        }
    }

    private void setupListenerForPlayer(Player p) {
        if(packetListener != null)
            packetListener.addListener(p);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        setupListenerForPlayer(e.getPlayer());
    }

    public void addPacketAdapterInbound(PacketAdapter adapter) {
        packetListener.addAdapterInbound(adapter);
    }

    public void removePacketAdapterInbound(PacketAdapter adapter) {
        packetListener.removeAdapterInbound(adapter);
    }

    public void addPacketAdapterOutbound(PacketAdapter adapter) {
        packetListener.addAdapterOutbound(adapter);
    }

    public void removePacketAdapterOutbound(PacketAdapter adapter) {
        packetListener.removeAdapterOutbound(adapter);
    }

    public List<HawkEventListener> getHawkEventListeners() {
        return hawkEventListeners;
    }
}
