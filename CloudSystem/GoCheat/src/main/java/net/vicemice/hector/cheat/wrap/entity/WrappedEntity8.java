package net.vicemice.hector.cheat.wrap.entity;

import net.vicemice.hector.cheat.util.AABB;
import net.vicemice.hector.cheat.wrap.entity.human.WrappedEntityHuman8;
import net.minecraft.server.v1_8_R3.AxisAlignedBB;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftHumanEntity;
import org.bukkit.entity.Entity;
import org.bukkit.util.Vector;

public class WrappedEntity8 extends WrappedEntity {

    protected net.minecraft.server.v1_8_R3.Entity nmsEntity;

    public WrappedEntity8(Entity entity) {
        super();
        nmsEntity = ((CraftEntity) entity).getHandle();
        AxisAlignedBB bb = nmsEntity.getBoundingBox();
        collisionBox = new AABB(new Vector(bb.a, bb.b, bb.c), new Vector(bb.d, bb.e, bb.f));
        collisionBorderSize = nmsEntity.ao();
        location = entity.getLocation();
    }

    public static WrappedEntity8 getWrappedEntity(Entity entity) {
        if(entity instanceof CraftHumanEntity)
            return new WrappedEntityHuman8(entity);
        else
            return new WrappedEntity8(entity);

    }
}
