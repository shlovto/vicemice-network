package net.vicemice.hector.cheat.module.listener;

import io.netty.channel.*;
import net.vicemice.hector.cheat.module.PacketHandler;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class PacketListener8 extends PacketListener {

    public PacketListener8(PacketHandler packetHandler, boolean async) {
        super(packetHandler, async);
    }

    public void add(Player p) {
        ChannelDuplexHandler channelDuplexHandler = new ChannelDuplexHandler() {
            @Override
            public void channelRead(ChannelHandlerContext context, Object packet) throws Exception {

                if(!processIn(packet, p))
                    return;

                super.channelRead(context, packet);
            }

            @Override
            public void write(ChannelHandlerContext context, Object packet, ChannelPromise promise) throws Exception {

                if(!processOut(packet, p))
                    return;

                super.write(context, packet, promise);
            }
        };
        Channel channel = ((CraftPlayer) p).getHandle().playerConnection.networkManager.channel;
        ChannelPipeline pipeline = channel.pipeline();
        if (pipeline == null)
            return;
        String handlerName = "hawk_packet_processor";
        channel.eventLoop().submit(() -> {
            if(pipeline.get(handlerName) != null)
                pipeline.remove(handlerName);
            pipeline.addBefore("packet_handler", handlerName, channelDuplexHandler);
            return null;
        });
    }

    public void removeAll() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            Channel channel = ((CraftPlayer) p).getHandle().playerConnection.networkManager.channel;
            ChannelPipeline pipeline = channel.pipeline();
            String handlerName = "hawk_packet_processor";
            channel.eventLoop().submit(() -> {
                if(pipeline.get(handlerName) != null)
                    pipeline.remove(handlerName);
                return null;
            });
        }
    }
}
