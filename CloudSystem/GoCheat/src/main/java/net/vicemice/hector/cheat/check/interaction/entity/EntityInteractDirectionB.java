package net.vicemice.hector.cheat.check.interaction.entity;

import net.vicemice.hector.cheat.check.CustomCheck;
import net.vicemice.hector.cheat.event.Event;
import net.vicemice.hector.cheat.event.InteractAction;
import net.vicemice.hector.cheat.event.InteractEntityEvent;
import net.vicemice.hector.cheat.event.MoveEvent;
import net.vicemice.hector.cheat.util.MathPlus;
import net.vicemice.hector.cheat.util.ServerUtils;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class EntityInteractDirectionB extends CustomCheck {

    //In 1.8, a new Vec3D field was added to the PacketPlayInUseEntity packet.
    //It is always null when attacking an entity. It represents the ray-trace
    //intersection position on the hit-box.

    //Strangely, most of the time, two PacketPlayInUseEntity packets are sent
    //during an entity interaction (right click): one with a useful Vec3D and
    //one with a null Vec3D. This appears to be a bug in the game.

    private Map<UUID, Vector> lastHitVecMap;

    public EntityInteractDirectionB() {
        super("lol", "%player% is hacking");
        lastHitVecMap = new HashMap<>();
    }

    @Override
    protected void check(Event e) {
        if(e instanceof MoveEvent) {
            processMove((MoveEvent) e);
        }
        else if(e instanceof InteractEntityEvent) {
            processHit((InteractEntityEvent) e);
        }
    }

    private void processHit(InteractEntityEvent e) {
        if(e.getInteractAction() == InteractAction.INTERACT) {
            Vector hitVec = e.getIntersectVector();
            if(hitVec != null) {
                hitVec = hitVec.clone();
                hitVec.add(goCheat.getLagCompensator().getHistoryLocation(ServerUtils.getPing(e.getPlayer()), e.getEntity()).toVector());
                lastHitVecMap.put(e.getPlayer().getUniqueId(), hitVec);
            }
        }
    }

    private void processMove(MoveEvent e) {
        Player p = e.getPlayer();
        UUID uuid = p.getUniqueId();

        if(lastHitVecMap.containsKey(uuid)) {
            Vector headPos = e.getCheatPlayer().getHeadPosition();
            Vector dirA = MathPlus.getDirection(e.getFrom().getYaw(), e.getTo().getPitch());
            Vector dirB = lastHitVecMap.get(uuid).subtract(headPos).normalize();
            //dirA.dot(dirB) should be close to 1.0
        }

        lastHitVecMap.remove(uuid);
    }
}
