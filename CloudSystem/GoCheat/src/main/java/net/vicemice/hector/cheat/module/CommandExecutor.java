package net.vicemice.hector.cheat.module;

import net.vicemice.hector.cheat.GoCheat;
import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.Check;
import net.vicemice.hector.cheat.util.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import java.util.*;

public class CommandExecutor {

    private final Set<Pair<UUID, Pair<Check, String>>> commandHistory;

    public CommandExecutor(GoCheat goCheat) {
        this.commandHistory = Collections.synchronizedSet(new HashSet<>());
        int cooldown = ConfigHelper.getOrSetDefault(1, goCheat.getConfig(), "commandExecutor.cooldownTicks");

        goCheat.getHawkSyncTaskScheduler().addRepeatingTask(new Runnable() {
            @Override
            public void run() {
                commandHistory.clear();
            }
        }, cooldown);
    }

    public void runACommand(List<String> command, Check check, double deltaVL, Player p, CheatPlayer pp, GoCheat goCheat, Placeholder... placeholders) {
        if (command.size() == 0 || command.get(0).length() == 0) return;
        for (String aCommand : command) {
            if (aCommand.length() == 0) return;
            String[] parts = aCommand.split(":");
            if (parts.length < 3) {
                goCheat.getLogger().warning("Failed command execution: Invalid command syntax in " + check + " configuration!");
                return;
            }

            //ignore colons in command
            //TODO: um... what if there are multiple cmds in the list that meet the conditions? Either get rid of the > / < or fix this
            for (int i = 3; i < parts.length; i++)
                parts[2] += ":" + parts[i];

            try {
                if (parts[0].charAt(0) == '>' && parts[0].charAt(1) == '=') {
                    if (pp.getVL(check) >= Integer.parseInt(parts[0].substring(2))) {
                        execute(parts, p, goCheat, check, placeholders);
                    }
                    return;
                }
                if (parts[0].charAt(0) == '<' && parts[0].charAt(1) == '=') {
                    if (pp.getVL(check) <= Integer.parseInt(parts[0].substring(2))) {
                        execute(parts, p, goCheat, check, placeholders);
                    }
                    return;
                }
                if (parts[0].charAt(0) == '>') {
                    if (pp.getVL(check) > Integer.parseInt(parts[0].substring(1))) {
                        execute(parts, p, goCheat, check, placeholders);
                    }
                    return;
                }
                if (parts[0].charAt(0) == '<') {
                    if (pp.getVL(check) < Integer.parseInt(parts[0].substring(1))) {
                        execute(parts, p, goCheat, check, placeholders);
                    }
                    return;
                }
                int confVL = Integer.parseInt(parts[0]);
                double currVL = pp.getVL(check);
                if (pp.getVL(check) >= confVL && currVL - deltaVL <= confVL) {
                    execute(parts, p, goCheat, check, placeholders);
                }
            } catch (NumberFormatException e) {
                goCheat.getLogger().warning("Failed command execution: Invalid command syntax in " + check + " configuration!");
            }
        }
    }

    private void execute(String[] parts, Player player, GoCheat goCheat, Check check, Placeholder... placeholders) {
        String preCmd = parts[2]
                .replace("%player%", player.getName())
                .replace("%ping%", ServerUtils.getPing(player) + "")
                .replace("%check%", check + "")
                .replace("%tps%", MathPlus.round(ServerUtils.getTps(), 2) + "");
        for (Placeholder placeholder : placeholders)
            preCmd = preCmd.replace("%" + placeholder.getKey() + "%", placeholder.getValue().toString());
        final String cmd = preCmd;
        Pair<UUID, Pair<Check, String>> cmdInfo = new Pair<>(player.getUniqueId(), new Pair<>(check, parts[2]));
        if(!commandHistory.contains(cmdInfo))
            Bukkit.getScheduler().scheduleSyncDelayedTask((goCheat), () -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd), Long.parseLong(parts[1]) * 20);
        commandHistory.add(cmdInfo);
    }
}
