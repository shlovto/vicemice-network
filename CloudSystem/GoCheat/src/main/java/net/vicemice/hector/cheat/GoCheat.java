package net.vicemice.hector.cheat;

import lombok.Getter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.cheat.commands.ToggleACCommand;
import net.vicemice.hector.cheat.module.*;
import net.vicemice.hector.cheat.util.ConfigHelper;
import net.vicemice.hector.server.GoServer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class GoCheat extends JavaPlugin {

    @Getter
    private static GoCheat instance;

    private CheckManager checkManager;
    private SQLModule sqlModule;
    private PacketHandler packetHandler;
    private ViolationLogger violationLogger;
    private FileConfiguration messages;
    private FileConfiguration checksConfig;
    private LagCompensator lagCompensator;
    private BanManager banManager;
    private MuteManager muteManager;
    private MouseRecorder mouseRecorder;
    private BungeeBridge bungeeBridge;
    private PunishmentScheduler punishmentScheduler;
    private HawkSyncTaskScheduler hawkSyncTaskScheduler;
    private CommandExecutor commandExecutor;
    private Map<UUID, CheatPlayer> profiles;
    private static int SERVER_VERSION;
    public static final String BASE_PERMISSION = "hawk";
    public static String BUILD_NAME;
    public static String FLAG_CLICK_COMMAND;
    public static boolean USING_PLIB;
    public static final String NO_PERMISSION = ChatColor.RED + "You do not have permission \"%s\" to perform this action.";
    private boolean sendJSONMessages;
    private boolean playSoundOnFlag;

    @Override
    public void onEnable() {
        instance = this;
        GoServer.getLocaleManager().addPath("cloud/locales/anticheat");
        BUILD_NAME = getDescription().getVersion();
        setServerVersion();

        if (!GoServer.getService().getServerName().contains("Lobby") && !GoServer.getService().getServerName().contains("MapEdit") && !GoServer.getService().getServerName().contains("Dev") && !GoServer.getService().getServerName().contains("Test")) {
            loadModules();
        } else {
            getLogger().warning("Hector will not start on this server.");
        }
    }

    @Override
    public void onDisable() {
        unloadModules();
        instance = null;
    }

    public void loadModules() {
        getLogger().info("Loading modules...");

        USING_PLIB = getServer().getPluginManager().isPluginEnabled("ProtocolLib");

        new File(getInstance().getDataFolder().getAbsolutePath()).mkdirs();
        messages = YamlConfiguration.loadConfiguration(new File(instance.getDataFolder().getAbsolutePath() + File.separator + "messages.yml"));
        checksConfig = YamlConfiguration.loadConfiguration(new File(instance.getDataFolder().getAbsolutePath() + File.separator + "checks.yml"));
        sendJSONMessages = true;
        playSoundOnFlag = true;
        FLAG_CLICK_COMMAND = "spec %player%";

        hawkSyncTaskScheduler = new HawkSyncTaskScheduler(this);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, hawkSyncTaskScheduler, 0L, 1L);

        profiles = new ConcurrentHashMap<>();
        getServer().getPluginManager().registerEvents(new PlayerEventListener(this), this);
        sqlModule = new SQLModule(this);
        sqlModule.createTableIfNotExists();
        commandExecutor = new CommandExecutor(this);
        punishmentScheduler = new PunishmentScheduler(this);
        punishmentScheduler.load();
        punishmentScheduler.start();
        lagCompensator = new LagCompensator(this);
        banManager = new BanManager(this);
        banManager.loadBannedPlayers();
        muteManager = new MuteManager(this);
        muteManager.loadMutedPlayers();
        startLoggerFile();
        bungeeBridge = new BungeeBridge(this, ConfigHelper.getOrSetDefault(false, getConfig(), "enableBungeeAlerts"));
        checkManager = new CheckManager(getInstance());
        checkManager.loadChecks();
        packetHandler = new PacketHandler(this);
        packetHandler.startListener();
        packetHandler.setupListenerForOnlinePlayers();
        mouseRecorder = new MouseRecorder(this);

        registerCommand();

        saveConfigs();
    }

    public void unloadModules() {
        getLogger().info("Unloading modules...");
        if(packetHandler != null)
            packetHandler.stopListener();
        getCommand("hawk").setExecutor(null);
        HandlerList.unregisterAll(this);
        if(punishmentScheduler != null) {
            punishmentScheduler.setEnabled(false);
            punishmentScheduler.stop();
            punishmentScheduler.saveSynchronously();
            punishmentScheduler = null;
        }
        lagCompensator = null;
        if(checkManager != null)
            checkManager.unloadChecks();
        checkManager = null;
        bungeeBridge = null;
        if(banManager != null)
            banManager.saveBannedPlayers();
        banManager = null;
        if(muteManager != null)
            muteManager.saveMutedPlayers();
        muteManager = null;
        profiles = null;
        if(sqlModule != null)
            sqlModule.closeConnection();
        sqlModule = null;
        commandExecutor = null;
        Bukkit.getScheduler().cancelTasks(this);
        hawkSyncTaskScheduler = null;
        violationLogger = null;
        mouseRecorder = null;
    }

    public void disable() {
        Bukkit.getScheduler().runTask(this, () -> Bukkit.getPluginManager().disablePlugin(getInstance()));
    }

    private void saveConfigs() {
        if(getInstance() == null)
            return;
        saveConfig();
        try {
            messages.save(new File(getInstance().getDataFolder().getAbsolutePath() + File.separator + "messages.yml"));
            checksConfig.save(new File(getInstance().getDataFolder().getAbsolutePath() + File.separator + "checks.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void registerCommand() {
        if(getInstance() == null)
            return;
        getCommand("toggleac").setExecutor(new ToggleACCommand());
    }

    private void startLoggerFile() {
        boolean enabled = ConfigHelper.getOrSetDefault(true, getConfig(), "logToFile");
        String pluginFolder = getInstance().getDataFolder().getAbsolutePath();
        File storageFile = new File(pluginFolder + File.separator + "logs.txt");
        violationLogger = new ViolationLogger(this, enabled);
        violationLogger.prepare(storageFile);
    }

    private void setServerVersion() {
        if(Package.getPackage("net.minecraft.server.v1_8_R3") != null) {
            SERVER_VERSION = 8;
        }
        else if(Package.getPackage("net.minecraft.server.v1_7_R4") != null) {
            SERVER_VERSION = 7;
        }
        else {
            SERVER_VERSION = 0;
        }
    }

    public FileConfiguration getMessages() {
        return messages;
    }

    public FileConfiguration getChecksConfig() {
        return checksConfig;
    }

    public CheckManager getCheckManager() {
        return checkManager;
    }

    public SQLModule getSQLModule() {
        return getInstance().sqlModule;
    }

    public GUIManager getGuiManager() {
        return null;
    }

    public static int getServerVersion() {
        return SERVER_VERSION;
    }

    public CheatPlayer getCheatPlayer(Player p) {
        CheatPlayer result = profiles.get(p.getUniqueId());
        if (result == null) {
            addProfile(p);
            result = profiles.get(p.getUniqueId());
        }
        return result;
    }

    public Collection<CheatPlayer> getCheatPlayers() {
        return profiles.values();
    }

    public void broadcastAlertToAdmins(String msg) {
        for(CheatPlayer pp : getCheatPlayers()) {
            if(pp.canReceiveAlerts())
                pp.getPlayer().sendMessage(GoAPI.getUserAPI().translate( pp.getUuid(), "prefix")+msg);
        }
        Bukkit.getConsoleSender().sendMessage(msg);
    }

    public void broadcastPrefixedAlertToAdmins(String msg) {
        broadcastAlertToAdmins(msg);
    }

    public void addProfile(Player p) {
        profiles.put(p.getUniqueId(), new CheatPlayer(p, this));
    }

    public void removeProfile(UUID uuid) {
        profiles.remove(uuid);
    }

    public ViolationLogger getViolationLogger() {
        return violationLogger;
    }

    public LagCompensator getLagCompensator() {
        return lagCompensator;
    }

    public BanManager getBanManager() {
        return banManager;
    }

    public MuteManager getMuteManager() {
        return muteManager;
    }

    public PacketHandler getPacketHandler() {
        return packetHandler;
    }

    public MouseRecorder getMouseRecorder() {
        return mouseRecorder;
    }

    public BungeeBridge getBungeeBridge() {
        return bungeeBridge;
    }

    public PunishmentScheduler getPunishmentScheduler() {
        return punishmentScheduler;
    }

    public HawkSyncTaskScheduler getHawkSyncTaskScheduler() {
        return hawkSyncTaskScheduler;
    }

    public CommandExecutor getCommandExecutor() {
        return commandExecutor;
    }

    public boolean canSendJSONMessages() {
        return sendJSONMessages;
    }

    public boolean canPlaySoundOnFlag() {
        return playSoundOnFlag;
    }
}
