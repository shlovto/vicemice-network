package net.vicemice.hector.cheat.check.interaction.terrain;

import net.vicemice.hector.cheat.check.BlockInteractionCheck;
import net.vicemice.hector.cheat.event.InteractWorldEvent;
import org.bukkit.util.Vector;

public class FabricatedBlockInteract extends BlockInteractionCheck {

    public FabricatedBlockInteract() {
        super("fabricatedblockinteract", true, 0, 2, 0.999, 5000, "%player% failed fabricated block interact, VL: %vl%", null);
    }

    @Override
    protected void check(InteractWorldEvent e) {

        if(e.getTargetedBlockFace() == InteractWorldEvent.BlockFace.INVALID) {
            fail(e);
            return;
        }

        Vector cursorPos = e.getCursorPositionOnTargetedBlock();
        //Disable these for now; currently there is no efficient way to get block hitboxes.
        //boolean onFace = false;
        for(double value : new double[] {cursorPos.getX(), cursorPos.getY(), cursorPos.getZ()}) {
            //Unnecessary to do "value % 0.0625 != 0" because the position is actually
            //stored as 3 bytes, not floats. Each represents a multiple of 1/16 of an axis.
            if(value > 1 || value < 0) {
                fail(e);
                return;
            }
            /*
            if(value == 0 || value == 1)
                onFace = true;
            */
        }

        /*
        if(!onFace) {
            fail(e);
            return;
        }
        */

        /*
        Vector cursorPosOffset = cursorPos.clone().subtract(new Vector(0.5, 0.5, 0.5)).multiply(2);
        Vector normal = e.getTargetedBlockFaceNormal();
        double[] cursorPosOffsetArray = new double[] {cursorPosOffset.getX(), cursorPosOffset.getY(), cursorPosOffset.getZ()};
        double[] normalArray = new double[] {normal.getX(), normal.getY(), normal.getZ()};
        boolean onCorrectFace = false;
        for(int i = 0; i < 3; i ++) {
            double normalComponent = normalArray[i];
            double cursorPosComponent = cursorPosOffsetArray[i];
            if(normalComponent == cursorPosComponent) {
                onCorrectFace = true;
                break;
            }
        }

        if(!onCorrectFace) {
            fail(e);
            return;
        }
        */

        reward(e.getCheatPlayer());

    }

    private void fail(InteractWorldEvent e) {
        punishAndTryCancelAndBlockRespawn(e.getCheatPlayer(), e);
    }
}
