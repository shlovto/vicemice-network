package net.vicemice.hector.cheat.check.movement.position;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.MovementCheck;
import net.vicemice.hector.cheat.event.MoveEvent;

public class Step extends MovementCheck {

    public Step() {
        super("step", "&7%player% failed step, VL: %vl%");
    }

    @Override
    protected void check(MoveEvent e) {

        if(e.isStep() || !(e.isOnGround() && e.getCheatPlayer().isOnGround()) || e.hasTeleported() || e.hasAcceptedKnockback()) {
            return;
        }

        CheatPlayer pp = e.getCheatPlayer();
        double dY = e.getTo().getY() - e.getFrom().getY();

        if(dY > 0.6 || dY < -0.0784) {
            punishAndTryRubberband(pp, e, e.getPlayer().getLocation());
        }
        else {
            reward(pp);
        }
    }
}
