package net.vicemice.hector.cheat.wrap.block;

import net.vicemice.hector.cheat.GoCheat;
import net.vicemice.hector.cheat.util.AABB;
import org.bukkit.block.Block;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public abstract class WrappedBlock {

    final Block obBlock;
    float strength;
    AABB hitbox;
    AABB[] collisionBoxes;
    boolean solid;
    float slipperiness;
    int clientVersion;

    WrappedBlock(Block obBlock, int clientVersion) {
        this.obBlock = obBlock;
        this.clientVersion = clientVersion;
    }

    public abstract Object getNMS();

    public abstract void sendPacketToPlayer(Player p);

    //Returns the amount of damage to apply to this block at this tick,
    //given that an entity is currently mining it.
    public abstract float getDamage(HumanEntity entity);

    public abstract boolean isMaterialAlwaysDestroyable();

    public float getStrength() {
        return strength;
    }

    public Block getBukkitBlock() {
        return obBlock;
    }

    public AABB getHitBox() {
        return hitbox;
    }

    public AABB[] getCollisionBoxes() {
        return collisionBoxes;
    }

    public boolean isColliding(AABB other) {
        for (AABB cBox : collisionBoxes) {
            if (cBox.isColliding(other))
                return true;
        }
        return false;
    }

    public static WrappedBlock getWrappedBlock(Block b, int clientVersion) {
        if (GoCheat.getServerVersion() == 8)
            return new WrappedBlock8(b, clientVersion);

        return null;
    }

    public float getSlipperiness() {
        return slipperiness;
    }

    //Man, I hate having to do this. I don't know why Bukkit is confused over the definition of SOLID.
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean isSolid() {
        return solid;
    }

    public Vector getFlowDirection() {
        return new Vector();
    }
}
