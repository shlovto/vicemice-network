package net.vicemice.hector.cheat.check.interaction.entity;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.check.EntityInteractionCheck;
import net.vicemice.hector.cheat.event.InteractEntityEvent;
import net.vicemice.hector.cheat.wrap.entity.WrappedEntity;
import net.vicemice.hector.cheat.util.AABB;
import net.vicemice.hector.cheat.util.MathPlus;
import net.vicemice.hector.cheat.util.Placeholder;
import net.vicemice.hector.cheat.util.ServerUtils;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class EntityInteractReach extends EntityInteractionCheck {

    //PASSED (6/24/2019)

    private final double MAX_REACH;
    private final double MAX_REACH_CREATIVE;
    private final int PING_LIMIT;
    private final boolean LAG_COMPENSATION;
    private final boolean CHECK_OTHER_ENTITIES;

    public EntityInteractReach() {
        super("entityinteractreach", "%player% failed entity interact reach. Reach: %distance%m VL: %vl%");
        MAX_REACH = (double) customSetting("maxReach", "", 3.1);
        MAX_REACH_CREATIVE = (double) customSetting("maxReachCreative", "", 5.0);
        PING_LIMIT = (int) customSetting("pingLimit", "", -1);
        LAG_COMPENSATION = (boolean) customSetting("lagCompensation", "", true);
        CHECK_OTHER_ENTITIES = (boolean) customSetting("checkOtherEntities", "", false);
    }

    @Override
    protected void check(InteractEntityEvent e) {
        Entity victimEntity = e.getEntity();
        if (!(victimEntity instanceof Player) && !CHECK_OTHER_ENTITIES)
            return;
        int ping = ServerUtils.getPing(e.getPlayer());
        if (PING_LIMIT > -1 && ping > PING_LIMIT)
            return;
        Player p = e.getPlayer();
        CheatPlayer att = e.getCheatPlayer();

        Location victimLocation;
        if (LAG_COMPENSATION)
            victimLocation = goCheat.getLagCompensator().getHistoryLocation(ping, victimEntity);
        else
            victimLocation = victimEntity.getLocation();

        AABB victimAABB = WrappedEntity.getWrappedEntity(victimEntity).getHitbox(victimLocation.toVector());

        Vector attackerPos;
        if(att.isInVehicle()) {
            attackerPos = goCheat.getLagCompensator().getHistoryLocation(ServerUtils.getPing(p), p).toVector();
            attackerPos.setY(attackerPos.getY() + p.getEyeHeight());
        }
        else {
            attackerPos = att.getHeadPosition();
        }

        double maxReach = att.getPlayer().getGameMode() == GameMode.CREATIVE ? MAX_REACH_CREATIVE : MAX_REACH;
        double dist = victimAABB.distanceToPosition(attackerPos);

        if (dist > maxReach) {
            punish(att, 1, true, e, new Placeholder("distance", MathPlus.round(dist, 2)));
        } else {
            reward(att);
        }
    }
}
