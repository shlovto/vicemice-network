package net.vicemice.hector.cheat.event;

import net.vicemice.hector.cheat.CheatPlayer;
import net.vicemice.hector.cheat.wrap.packet.WrappedPacket;
import org.bukkit.entity.Player;

public class BadEvent extends Event {

    public BadEvent(Player p, CheatPlayer pp, WrappedPacket wPacket) {
        super(p, pp, wPacket);
    }

    @Override
    public boolean preProcess() {
        return false;
    }
}
