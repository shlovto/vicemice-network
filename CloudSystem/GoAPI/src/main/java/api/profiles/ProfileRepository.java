package api.profiles;

import java.util.UUID;

public interface ProfileRepository {
    public Profile[] findProfilesByNames(String ... var1) throws Exception;

    public Profile findProfileByUUID(UUID var1) throws Exception;
}