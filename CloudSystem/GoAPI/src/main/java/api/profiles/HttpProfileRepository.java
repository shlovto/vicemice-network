package api.profiles;

import api.http.BasicHttpClient;
import api.http.HttpBody;
import api.http.HttpClient;
import api.http.HttpHeader;
import com.google.gson.Gson;
import com.google.gson.JsonArray;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class HttpProfileRepository implements ProfileRepository {
    private static final int PROFILES_PER_REQUEST = 100;
    private static Gson gson = new Gson();
    private final String agent;
    private HttpClient client;

    public HttpProfileRepository(String agent) {
        this(agent, BasicHttpClient.getInstance());
    }

    public HttpProfileRepository(String agent, HttpClient client) {
        this.agent = agent;
        this.client = client;
    }

    @Override
    public Profile[] findProfilesByNames(String ... names) throws Exception {
        ArrayList<Profile> profiles = new ArrayList<Profile>();
        ArrayList<HttpHeader> headers = new ArrayList<HttpHeader>();
        headers.add(new HttpHeader("Content-Type", "application/json"));
        int namesCount = names.length;
        int start = 0;
        int i = 0;
        do {
            int end;
            if ((end = 100 * (i + 1)) > namesCount) {
                end = namesCount;
            }
            String[] namesBatch = Arrays.copyOfRange(names, start, end);
            HttpBody body = HttpProfileRepository.getHttpBody(namesBatch);
            Profile[] result = this.post(this.getProfilesUrl(), body, headers);
            profiles.addAll(Arrays.asList(result));
            start = end;
            ++i;
        } while (start < namesCount);
        return profiles.toArray(new Profile[profiles.size()]);
    }

    @Override
    public Profile findProfileByUUID(UUID uuid) throws Exception {
        ArrayList<HttpHeader> headers = new ArrayList<HttpHeader>();
        headers.add(new HttpHeader("Content-Type", "application/json"));
        String response = this.client.get(this.getUUIDToNameUrl(uuid), headers);
        JsonArray jsonArray = (JsonArray)new Gson().fromJson(response, JsonArray.class);
        Profile profile = new Profile();
        profile.setId(uuid.toString().replace("-", ""));
        profile.setName(jsonArray.get(jsonArray.size() - 1).getAsJsonObject().get("name").getAsString());
        return profile;
    }

    private URL getProfilesUrl() throws MalformedURLException {
        return new URL("https://api.mojang.com/profiles/" + this.agent);
    }

    private URL getUUIDToNameUrl(UUID uuid) throws MalformedURLException {
        return new URL("https://api.mojang.com/user/profiles/" + uuid.toString().replaceAll("-", "") + "/names");
    }

    private Profile[] post(URL url, HttpBody body, List<HttpHeader> headers) throws IOException {
        String response = this.client.post(url, body, headers);
        return (Profile[])gson.fromJson(response, Profile[].class);
    }

    private static HttpBody getHttpBody(String ... namesBatch) {
        return new HttpBody(gson.toJson((Object)namesBatch));
    }
}