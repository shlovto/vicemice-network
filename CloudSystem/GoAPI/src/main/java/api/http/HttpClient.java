package api.http;

import java.io.IOException;
import java.net.Proxy;
import java.net.URL;
import java.util.List;

public interface HttpClient {
    public String post(URL var1, HttpBody var2, List<HttpHeader> var3) throws IOException;

    public String post(URL var1, Proxy var2, HttpBody var3, List<HttpHeader> var4) throws IOException;

    public String get(URL var1, List<HttpHeader> var2) throws IOException;

    public String get(URL var1, Proxy var2, List<HttpHeader> var3) throws IOException;
}

