package net.vicemice.hector.service.server;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import net.vicemice.hector.backend.GoNetty;
import net.vicemice.hector.packets.types.PacketHolder;

/*
 * Class created at 12:58 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class NettyHandler extends SimpleChannelInboundHandler<Object> {
    GoNetty client;

    public NettyHandler(GoNetty client) {
        this.client = client;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        System.out.println("Hector connection was initialized");
        this.client.setChannel(ctx.channel());
        this.client.channelActive(ctx.channel());
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object o) throws Exception {
        try {
            this.client.channelRead((PacketHolder) o);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext paramChannelHandlerContext, Throwable paramThrowable) throws Exception {
        paramThrowable.printStackTrace();
        System.err.println("Hector connection were terminated");
        paramChannelHandlerContext.disconnect();
    }
}
