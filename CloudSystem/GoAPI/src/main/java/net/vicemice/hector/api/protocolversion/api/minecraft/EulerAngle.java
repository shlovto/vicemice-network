package net.vicemice.hector.api.protocolversion.api.minecraft;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EulerAngle {
    private float x;
    private float y;
    private float z;
}
