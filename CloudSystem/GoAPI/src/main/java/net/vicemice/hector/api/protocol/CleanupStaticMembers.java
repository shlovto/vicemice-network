/*
 *  ProtocolLib - Bukkit server library that allows access to the Minecraft protocol.
 *  Copyright (C) 2012 Kristian S. Stangeland
 *
 *  This program is free software; you can redistribute it and/or modify it under the terms of the 
 *  GNU General Public License as published by the Free Software Foundation; either version 2 of 
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program; 
 *  if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
 *  02111-1307 USA
 */

package net.vicemice.hector.api.protocol;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import net.vicemice.hector.api.protocol.async.AsyncListenerHandler;
import net.vicemice.hector.api.protocol.error.ErrorReporter;
import net.vicemice.hector.api.protocol.error.Report;
import net.vicemice.hector.api.protocol.error.ReportType;
import net.vicemice.hector.api.protocol.events.ListeningWhitelist;
import net.vicemice.hector.api.protocol.events.PacketContainer;
import net.vicemice.hector.api.protocol.injector.BukkitUnwrapper;
import net.vicemice.hector.api.protocol.injector.server.AbstractInputStreamLookup;
import net.vicemice.hector.api.protocol.injector.server.TemporaryPlayerFactory;
import net.vicemice.hector.api.protocol.injector.spigot.SpigotPacketInjector;
import net.vicemice.hector.api.protocol.reflect.FieldUtils;
import net.vicemice.hector.api.protocol.reflect.FuzzyReflection;
import net.vicemice.hector.api.protocol.reflect.MethodUtils;
import net.vicemice.hector.api.protocol.reflect.ObjectWriter;
import net.vicemice.hector.api.protocol.reflect.compiler.BackgroundCompiler;
import net.vicemice.hector.api.protocol.reflect.compiler.StructureCompiler;
import net.vicemice.hector.api.protocol.reflect.instances.CollectionGenerator;
import net.vicemice.hector.api.protocol.reflect.instances.DefaultInstances;
import net.vicemice.hector.api.protocol.reflect.instances.PrimitiveGenerator;
import net.vicemice.hector.api.protocol.utility.MinecraftReflection;
import net.vicemice.hector.api.protocol.wrappers.ChunkPosition;
import net.vicemice.hector.api.protocol.wrappers.WrappedDataWatcher;
import net.vicemice.hector.api.protocol.wrappers.WrappedWatchableObject;
import net.vicemice.hector.api.protocol.wrappers.nbt.io.NbtBinarySerializer;

/**
 * Used to fix ClassLoader leaks that may lead to filling up the permanent generation.
 * 
 * @author Kristian
 */
class CleanupStaticMembers {
	// Reports
	public final static ReportType REPORT_CANNOT_RESET_FIELD = new ReportType("Unable to reset field %s: %s");
	public final static ReportType REPORT_CANNOT_UNLOAD_CLASS = new ReportType("Unable to unload class %s.");
	
	private ClassLoader loader;
	private ErrorReporter reporter;

	public CleanupStaticMembers(ClassLoader loader, ErrorReporter reporter) {
		this.loader = loader;
		this.reporter = reporter;
	}
	
	/**
	 * Ensure that the previous ClassLoader is not leaking.
	 */
	public void resetAll() {
		// This list must always be updated
		@SuppressWarnings("deprecation")
		Class<?>[] publicClasses = { 
				AsyncListenerHandler.class, ListeningWhitelist.class, PacketContainer.class, 
				BukkitUnwrapper.class, DefaultInstances.class, CollectionGenerator.class,
				PrimitiveGenerator.class, FuzzyReflection.class, MethodUtils.class, 
				BackgroundCompiler.class, StructureCompiler.class,
				ObjectWriter.class, Packets.Server.class, Packets.Client.class, 
				ChunkPosition.class, WrappedDataWatcher.class, WrappedWatchableObject.class,
				AbstractInputStreamLookup.class, TemporaryPlayerFactory.class, SpigotPacketInjector.class,
				MinecraftReflection.class, NbtBinarySerializer.class
		};
							   			
		String[] internalClasses = {
			 "net.vicemice.hector.api.protocol.events.SerializedOfflinePlayer",
			 "net.vicemice.hector.api.protocol.injector.player.InjectedServerConnection",
			 "net.vicemice.hector.api.protocol.injector.player.NetworkFieldInjector",
			 "net.vicemice.hector.api.protocol.injector.player.NetworkObjectInjector",
			 "net.vicemice.hector.api.protocol.injector.player.NetworkServerInjector",
			 "net.vicemice.hector.api.protocol.injector.player.PlayerInjector",
			 "net.vicemice.hector.api.protocol.injector.EntityUtilities",
			 "net.vicemice.hector.api.protocol.injector.packet.PacketRegistry",
			 "net.vicemice.hector.api.protocol.injector.packet.PacketInjector",
			 "net.vicemice.hector.api.protocol.injector.packet.ReadPacketModifier",
			 "net.vicemice.hector.api.protocol.injector.StructureCache",
			 "net.vicemice.hector.api.protocol.reflect.compiler.BoxingHelper",
			 "net.vicemice.hector.api.protocol.reflect.compiler.MethodDescriptor",
			 "net.vicemice.hector.api.protocol.wrappers.nbt.WrappedElement",
		};
		
		resetClasses(publicClasses);
		resetClasses(getClasses(loader, internalClasses));
	}
	
	private void resetClasses(Class<?>[] classes) {
		// Reset each class one by one
		for (Class<?> clazz : classes) {
			resetClass(clazz);
		}
	}
	
	private void resetClass(Class<?> clazz) {
		for (Field field : clazz.getFields()) {
			Class<?> type = field.getType();
			
			// Only check static non-primitive fields. We also skip strings.
			if (Modifier.isStatic(field.getModifiers()) && 
					!type.isPrimitive() && !type.equals(String.class) && 
					!type.equals(ReportType.class)) {
				
				try {
					setFinalStatic(field, null);
				} catch (IllegalAccessException e) {
					// Just inform the player
					reporter.reportWarning(this, 
							Report.newBuilder(REPORT_CANNOT_RESET_FIELD).error(e).messageParam(field.getName(), e.getMessage())
					);
					e.printStackTrace();
				}
			}
		}
	}

	// HACK! HAACK!
	private static void setFinalStatic(Field field, Object newValue) throws IllegalAccessException {
		int modifier = field.getModifiers();
		boolean isFinal = Modifier.isFinal(modifier);
		
		Field modifiersField = isFinal ? FieldUtils.getField(Field.class, "modifiers", true) : null;

		// We have to remove the final field first
		if (isFinal) {
			FieldUtils.writeField(modifiersField, field, modifier & ~Modifier.FINAL, true);
		}
			
		// Now we can safely modify the field
		FieldUtils.writeStaticField(field, newValue, true);
		
		// Revert modifier
		if (isFinal) {
			FieldUtils.writeField(modifiersField, field, modifier, true);
		}
	}
	
	private Class<?>[] getClasses(ClassLoader loader, String[] names) {
		List<Class<?>> output = new ArrayList<Class<?>>();
		
		for (String name : names) {
			try {
				output.add(loader.loadClass(name));
			} catch (ClassNotFoundException e) {
				// Warn the user
				reporter.reportWarning(this, Report.newBuilder(REPORT_CANNOT_UNLOAD_CLASS).error(e).messageParam(name));
			}
		}
		
		return output.toArray(new Class<?>[0]);
	}
}
