package net.vicemice.hector.utils.jfuture;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/*
 * Class created at 01:04 - 04.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public abstract class BasicProgressFuture<V> implements ProgressFuture<V> {
    public static ThreadCreator ASYNC_RUNNER = run -> new Thread(run).start();

    @Override
    public void get(ProgressFuture.AsyncCallback<V> task) {
        this.get((a, b, c) -> task.done(a, b));
    }

    @Override
    public void get(ProgressFuture.TimedAsyncCallback<V> task) {
        long start = System.currentTimeMillis();
        ASYNC_RUNNER.runAsync(() -> {
            Object obj = null;
            Exception ex = null;
            try {
                obj = this.get();
            }
            catch (Exception e) {
                ex = e;
            }
            if (ex == null) {
                ex = this.getException();
            }
            task.done((V) obj, ex, System.currentTimeMillis() - start);
        });
    }

    @Override
    public void get(ProgressFuture.AsyncCallback<V> task, int timeout, TimeUnit unit) {
        this.get((a, b, c) -> task.done(a, b), timeout, unit);
    }

    @Override
    public void get(ProgressFuture.TimedAsyncCallback<V> task, int timeout, TimeUnit unit) {
        long start = System.currentTimeMillis();
        ASYNC_RUNNER.runAsync(() -> {
            Object obj = null;
            Exception ex = null;
            try {
                obj = this.get(timeout, unit);
            }
            catch (Exception e) {
                ex = e;
            }
            if (ex == null) {
                ex = this.getException();
            }
            task.done((V) obj, ex, System.currentTimeMillis() - start);
        });
    }

    @Override
    public V get(int timeout, TimeUnit unit) throws TimeoutException {
        long timeoutTime = System.currentTimeMillis() + unit.toMillis(timeout);
        while (timeoutTime > System.currentTimeMillis()) {
            if (this.isDone()) {
                return this.get();
            }
            try {
                Thread.sleep(5L);
            }
            catch (InterruptedException interruptedException) {
                // empty catch block
            }
        }
        throw new TimeoutException();
    }

    @Override
    public V getOr(V obj) {
        return this.isDone() ? this.get() : obj;
    }

    @Override
    public V getOr(int timeout, TimeUnit unit, V obj) {
        try {
            return this.get(timeout, unit);
        }
        catch (TimeoutException e) {
            return obj;
        }
    }

    public static interface ThreadCreator {
        public void runAsync(Runnable var1);
    }

}