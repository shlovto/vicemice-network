package net.vicemice.hector.packets.types.player.replay;

import lombok.Getter;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

/*
 * Class created at 18:22 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PacketReplayEntries implements Serializable {

    @Getter
    private final UUID uniqueId;
    @Getter
    private final ArrayList<PacketReplayEntry> entries;

    @ConstructorProperties(value = {"uniqueId", "entries"})
    public PacketReplayEntries(UUID uniqueId, ArrayList<PacketReplayEntry> entries) {
        this.uniqueId = uniqueId;
        this.entries = entries;
    }

    public static class PacketReplayEntry implements Serializable {

        @Getter
        private final String gameId, gameType, mapType, map;;
        @Getter
        private final UUID replayUUID;
        @Getter
        private final int replayStartSeconds;
        @Getter
        private final long timestamp, gameLength;
        @Getter
        private final ArrayList<String> players;
        @Getter
        private final boolean favorite;

        @ConstructorProperties(value = {"gameId", "replayUUID", "timestamp", "gameLength", "player", "gameType", "mapType", "favorite"})
        public PacketReplayEntry(String gameId, UUID replayUUID, int replayStartSeconds, long timestamp, long gameLength, ArrayList<String> players, String gameType, String mapType, String map, boolean favorite) {
            this.gameId = gameId;
            this.replayUUID = replayUUID;
            this.replayStartSeconds = replayStartSeconds;
            this.timestamp = timestamp;
            this.gameLength = gameLength;
            this.players = players;
            this.gameType = gameType;
            this.mapType = mapType;
            this.map = map;
            this.favorite = favorite;
        }
    }
}
