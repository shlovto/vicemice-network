package net.vicemice.hector.api.inventory.api;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;

public abstract class InventoryOption {
    private static final String DEFAULT_NAME = "this_is_a_default_value";
    private static final String[] DEFAULT_DESCRIPTION = new String[0];
    protected String name = "this_is_a_default_value";
    protected Object[] nameObjects;
    protected String[] description = DEFAULT_DESCRIPTION;
    protected int position = 0;
    protected ItemStack itemStack = new ItemStack(Material.STONE);
    protected boolean cancelOnClick = true;

    public ItemStack getItemStack() {
        ItemMeta itemMeta = this.itemStack.getItemMeta();
        if (!this.name.equals(DEFAULT_NAME)) {
            itemMeta.setDisplayName(this.name);
        }
        if (this.description.length != DEFAULT_DESCRIPTION.length) {
            itemMeta.setLore(Arrays.asList(this.description));
        }
        this.itemStack.setItemMeta(itemMeta);
        return this.itemStack;
    }

    public InventoryOption setMaterial(Material material) {
        this.itemStack = new ItemStack(material);
        return this;
    }

    public InventoryOption setDescription(List<String> list) {
        this.description = new String[list.size()];
        this.description = list.toArray(this.description);
        return this;
    }

    public InventoryOption setDescription(String[] description) {
        this.description = description;
        return this;
    }

    public InventoryOption setName(String name, Object ... nameObjects) {
        this.name = name;
        this.nameObjects = nameObjects;
        return this;
    }

    public InventoryOption setName(String name) {
        this.name = name;
        this.nameObjects = new Object[0];
        return this;
    }

    public abstract void onClick(Player var1);

    public String getName() {
        return this.name;
    }

    public Object[] getNameObjects() {
        return this.nameObjects;
    }

    public String[] getDescription() {
        return this.description;
    }

    public int getPosition() {
        return this.position;
    }

    public InventoryOption setPosition(int position) {
        this.position = position;
        return this;
    }

    public InventoryOption setItemStack(ItemStack itemStack) {
        this.itemStack = itemStack;
        return this;
    }

    public boolean isCancelOnClick() {
        return this.cancelOnClick;
    }

    public InventoryOption setCancelOnClick(boolean cancelOnClick) {
        this.cancelOnClick = cancelOnClick;
        return this;
    }
}