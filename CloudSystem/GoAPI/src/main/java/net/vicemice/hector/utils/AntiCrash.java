package net.vicemice.hector.utils;

public enum AntiCrash {
    POSITION,
    CUSTOM_PAYLOAD;
}