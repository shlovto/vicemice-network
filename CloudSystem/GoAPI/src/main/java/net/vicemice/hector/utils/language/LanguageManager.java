package net.vicemice.hector.utils.language;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.utils.Properties;
import net.vicemice.hector.utils.language.exception.LocaleNotFoundException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * The localeManager is a static service, which handles messages from different locales, which are registered or loaded there
 */
public final class LanguageManager {
    private LanguageManager() {
        throw new UnsupportedOperationException();
    }

    /**
     * The current locale, which the getMessage() method will the message return
     */
    @Getter
    @Setter
    private static volatile Locale locale;

    private static final Map<Locale, Properties> locale_CACHE = new HashMap<>();

    /**
     * Resolve and returns the following message in the locale which is currently set as member "locale"
     *
     * @param property the following message property, which should sort out
     * @return the message which is defined in locale cache or a fallback message like "MESSAGE OR locale NOT FOUND!"
     */
    public static String getMessage(String property) {
        if (locale == null || !locale_CACHE.containsKey(locale))
            throw new LocaleNotFoundException("MESSAGE OR LOCALE NOT FOUND");
        return locale_CACHE.get(locale).get(property);
    }

    /**
     * Resolve and returns the following message in the locale which is currently set as member "locale"
     *
     * @param property the following message property, which should sort out
     * @return the message which is defined in locale cache or a fallback message like "MESSAGE OR locale NOT FOUND!"
     */
    public static String getMessage(Locale locale, String property) {
        if (locale == null || !locale_CACHE.containsKey(locale))
            throw new LocaleNotFoundException("MESSAGE OR LOCALE NOT FOUND");

        return locale_CACHE.get(locale).get(property);
    }

    /**
     * Add a new locale properties, which can resolve with getMessage() in the configured locale.
     *
     * @param locale   the locale, which should append
     * @param properties the properties which will add in the locale as parameter
     */
    public static void addLanguageFile(Locale locale, Properties properties) {
        if (locale == null || properties == null) return;

        if (locale_CACHE.containsKey(locale))
            locale_CACHE.get(locale).putAll(properties);
        else
            locale_CACHE.put(locale, properties);
    }

    /**
     * Add a new locale properties, which can resolve with getMessage() in the configured locale.
     *
     * @param locale the locale, which should append
     * @param file     the properties which will add in the locale as parameter
     */
    public static void addLanguageFile(Locale locale, File file) {
        addLanguageFile(locale, file.toPath());
    }

    /**
     * Add a new locale properties, which can resolve with getMessage() in the configured locale.
     *
     * @param locale the locale, which should append
     * @param file     the properties which will add in the locale as parameter
     */
    public static void addLanguageFile(Locale locale, Path file) {
        try (InputStream inputStream = new FileInputStream(file.toFile())) {
            addLanguageFile(locale, inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Add a new locale properties, which can resolve with getMessage() in the configured locale.
     *
     * @param locale    the locale, which should append
     * @param inputStream the properties which will add in the locale as parameter
     */
    public static void addLanguageFile(Locale locale, InputStream inputStream) {
        Properties properties = new Properties();

        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        addLanguageFile(locale, properties);
    }
}