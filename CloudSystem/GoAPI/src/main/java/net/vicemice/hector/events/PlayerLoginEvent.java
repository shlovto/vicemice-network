package net.vicemice.hector.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import java.util.UUID;

public class PlayerLoginEvent extends Event {
    private static HandlerList handlers = new HandlerList();
    private UUID uniqueId;

    public PlayerLoginEvent(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }
}
