package net.vicemice.hector.packets.types.player.replay;

import lombok.Getter;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:38 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class WatchReplayPacket implements Serializable {

    @Getter
    private UUID uniqueId;
    @Getter
    private String gameID;

    @ConstructorProperties(value = {"uniqueId", "gameid"})
    public WatchReplayPacket(UUID uniqueId, String gameID) {
        this.uniqueId = uniqueId;
        this.gameID = gameID;
    }
}
