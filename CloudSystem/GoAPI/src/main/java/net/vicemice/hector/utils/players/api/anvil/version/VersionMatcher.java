package net.vicemice.hector.utils.players.api.anvil.version;

import org.bukkit.Bukkit;

import java.util.Arrays;
import java.util.List;

/*
 * Class created at 16:15 - 08.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class VersionMatcher {
    /**
     * The server's version
     */
    private final String serverVersion = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3].substring(1);
    /**
     * All available {@link VersionWrapper}s
     */
    private final List<Class<? extends VersionWrapper>> versions = Arrays.asList(
            Wrapper1_8_R3.class
    );

    /**
     * Matches the server version to it's {@link VersionWrapper}
     *
     * @return The {@link VersionWrapper} for this server
     * @throws RuntimeException If AnvilGUI doesn't support this server version
     */
    public VersionWrapper match() {
        try {
            return versions.stream()
                    .filter(version -> version.getSimpleName().substring(7).equals(serverVersion))
                    .findFirst().orElseThrow(() -> new RuntimeException("Your server version isn't supported in AnvilGUI!"))
                    .newInstance();
        } catch (IllegalAccessException | InstantiationException ex) {
            throw new RuntimeException(ex);
        }
    }
}