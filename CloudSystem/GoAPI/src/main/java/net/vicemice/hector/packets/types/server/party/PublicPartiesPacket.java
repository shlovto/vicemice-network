package net.vicemice.hector.packets.types.server.party;

import lombok.Getter;
import net.vicemice.hector.utils.players.party.Party;
import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.List;

/*
 * Class created at 19:03 - 31.03.2020
 * Copyright (C) elrobtossohn
 */
public class PublicPartiesPacket implements Serializable {

    @Getter
    private String callBackId;
    @Getter
    private List<Party> parties;

    @ConstructorProperties(value = {"callBackId", "parties"})
    public PublicPartiesPacket(String callBackId, List<Party> parties) {
        this.callBackId = callBackId;
        this.parties = parties;
    }
}
