package net.vicemice.hector.service.tryjump.event.skip;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.Set;

/*
 * Class created at 01:49 - 10.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SkipSuccessfulEvent extends Event {

    public int getPlayersNeeded() {
        return playersNeeded;
    }

    public Set<Player> getPlayersSkiped() {
        return playersSkiped;
    }

    protected static HandlerList handlerList = new HandlerList();
    private int playersNeeded;
    private Set<Player> playersSkiped;

    public SkipSuccessfulEvent(int playersNeeded, Set<Player> playersSkiped) {
        this.playersNeeded = playersNeeded;
        this.playersSkiped = playersSkiped;
    }

    /** Gets the list of all handlers. */
    public static HandlerList getHandlerList() {
        return handlerList;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
}
