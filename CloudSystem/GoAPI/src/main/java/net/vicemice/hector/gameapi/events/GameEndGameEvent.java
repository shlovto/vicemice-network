package net.vicemice.hector.gameapi.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameEndGameEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    public GameEndGameEvent() {}

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public HandlerList getHandlers() {
        return handlers;
    }
}
