package net.vicemice.hector.packets.types.player;

import lombok.Getter;

import java.io.Serializable;
import java.util.TreeMap;
import java.util.UUID;

/*
 * Class created at 18:21 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class MessagePacket implements Serializable {

    @Getter
    private final UUID receiverUniqueId;
    @Getter
    private final String[] messages;
    @Getter
    private TreeMap<String, ClickHoverEvent> clickEventTreeMap = new TreeMap<>();
    @Getter
    private TreeMap<String, ClickHoverEvent> hoverEventTreeMap = new TreeMap<>();

    public void addClickEvent(String ident, ClickHoverEvent clickEvent) {
        this.clickEventTreeMap.put(ident, clickEvent);
    }

    public void addHoverEvent(String ident, ClickHoverEvent hoverEvent) {
        this.hoverEventTreeMap.put(ident, hoverEvent);
    }

    public MessagePacket(UUID receiverUniqueId, String[] messages) {
        this.receiverUniqueId = receiverUniqueId;
        this.messages = messages;
    }

    public static class ClickHoverEvent implements Serializable {
        @Getter
        private final String action;
        @Getter
        private final String value;

        public ClickHoverEvent(String action, String value) {
            this.action = action;
            this.value = value;
        }
    }
}
