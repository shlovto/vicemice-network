package net.vicemice.hector.utils.players.api.internal.fakemobs.util;

import net.vicemice.hector.api.protocol.wrappers.WrappedGameProfile;
import net.vicemice.hector.api.protocol.wrappers.WrappedSignedProperty;
import com.google.common.collect.Multimap;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mojang.authlib.properties.PropertyMap;
import net.vicemice.hector.utils.players.api.internal.fakemobs.FakeMobs;
import net.vicemice.hector.utils.players.api.internal.fakemobs.merchant.ReflectionUtils;
import net.vicemice.hector.utils.players.api.npc.skin.Callback;
import net.vicemice.hector.utils.players.api.npc.skin.SkinData;
import net.vicemice.hector.utils.players.api.npc.skin.SkinDataReply;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

public class SkinQueue extends Thread {
	private final Queue<SkinEntry> queue = new LinkedBlockingQueue<SkinEntry>();

	public SkinQueue() {
		super("FakeMobs skin queue");
		this.setDaemon(true);
	}

	@Override
	public void run() {
		while (!this.isInterrupted()) {
			this.blockThread();
			while (!this.queue.isEmpty()) {
				SkinEntry entry = this.queue.poll();
				if (FakeMobs.instance.getMob(entry.getMob().getId()) != entry.getMob()) continue;

				if (entry.getSkinType() == SkinType.NAME) {
					String skinName = (String) entry.getSkin();
					Object nmsProfile = ReflectionUtils.searchUUID(skinName);
					WrappedGameProfile profile = nmsProfile == null ? new WrappedGameProfile((UUID) null, skinName) : WrappedGameProfile.fromHandle(nmsProfile);

					if (profile.getProperties().isEmpty() && profile.isComplete())
						profile = WrappedGameProfile.fromHandle(ReflectionUtils.fillProfileProperties(profile.getHandle()));
					if (profile == null) continue;

					entry.getMob().setPlayerSkin(profile.getProperties());
					entry.getMob().updateCustomName();
				} else if (entry.getSkinType() == SkinType.MINESKIN) {
					int skinId = (Integer) entry.getSkin();
					getSkinFromMineSkinAsync(Bukkit.getPluginManager().getPlugin("Hector"), skinId, (e) -> {
						Multimap test = new PropertyMap();
						test.put("textures", new WrappedSignedProperty("textures", e.getSignature(), e.getValue()));
						entry.getMob().setPlayerSkin(test);
					});
				}
			}
		}
	}

	public synchronized void addToQueue(FakeMob mob, int skinId) {
		this.queue.add(new SkinEntry(mob, SkinType.MINESKIN, skinId));
		this.notify();
	}

	public synchronized void addToQueue(FakeMob mob, String skinName) {
		this.queue.add(new SkinEntry(mob, SkinType.NAME, skinName));
		this.notify();
	}

	private synchronized void blockThread() {
		try {
			while (this.queue.isEmpty()) {
				this.wait();
			}
		} catch (Exception ex) { }
	}
	
	private static class SkinEntry {
		private final FakeMob mob;
		private final SkinType skinType;
		private final Object skin;

		public SkinEntry(FakeMob mob, SkinType skinType, Object skin) {
			this.mob = mob;
			this.skinType = skinType;
			this.skin = skin;
		}

		public FakeMob getMob() {
			return this.mob;
		}

		public SkinType getSkinType() {
			return skinType;
		}

		public Object getSkin() {
			return skin;
		}
	}

	private static enum SkinType {
		NAME,
		MINESKIN;
	}

	public static void getSkinFromMineSkinAsync(final Plugin plugin, final int mineskinid, final SkinDataReply skinreply){
		Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
			JsonObject jsonresponse = getJsonResponse("https://api.mineskin.org/get/id/"+mineskinid);
			if(jsonresponse!=null && !jsonresponse.has("error")){
				JsonObject textureProperty = jsonresponse.getAsJsonObject("data").getAsJsonObject("texture");
				String value = textureProperty.get("value").getAsString();
				String signature = textureProperty.get("signature").getAsString();
				skinreply.done(new SkinData(value, signature));
			}else{
				skinreply.done(null);
			}
		});
	}

	private static JsonObject getJsonResponse(String url){
		URL ipAdress;
		JsonObject rootobj = null;
		url = url.replace("-", "");
		try {
			ipAdress = new URL(url);
			BufferedReader in = new BufferedReader(new InputStreamReader(ipAdress.openStream()));
			String jsonresponse = in.readLine();
			JsonParser jsonParser = new JsonParser();
			JsonElement root = jsonParser.parse(jsonresponse);
			rootobj = root.getAsJsonObject();
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			//e.printStackTrace();
		}
		return rootobj;
	}

	public static void getUUIDFromName(final Plugin plugin, String name, Callback<String> callback){
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new Runnable() {
			public void run() {
				long unixTime = System.currentTimeMillis() / 1000L;
				JsonObject jsonresponse = getJsonResponse("https://api.mojang.com/users/profiles/minecraft/"+name+"?at="+unixTime);
				if(jsonresponse!=null && jsonresponse.get("error")==null){
					callback.call(jsonresponse.get("id").getAsString());
				}else{
					callback.call(null);
				}
			}
		});
	}
}
