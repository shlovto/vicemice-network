package net.vicemice.hector.packets.types.server;

import java.beans.ConstructorProperties;
import java.io.Serializable;

/*
 * Class created at 18:35 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SendPluginMessage implements Serializable {
    private Target target;
    private String player;
    private String channel;
    private byte[] data;

    @ConstructorProperties(value={"target", "player", "channel", "data"})
    public SendPluginMessage(Target target, String player, String channel, byte[] data) {
        this.target = target;
        this.player = player;
        this.channel = channel;
        this.data = data;
    }

    public Target getTarget() {
        return this.target;
    }

    public String getPlayer() {
        return this.player;
    }

    public String getChannel() {
        return this.channel;
    }

    public byte[] getData() {
        return this.data;
    }

    public static enum Target {
        SERVER,
        CLIENT;


        private Target() {
        }
    }
}