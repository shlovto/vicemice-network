package net.vicemice.hector.service.tryjump;

import org.bukkit.Location;

import java.util.Set;

/*
 * Class created at 01:41 - 10.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class DeathmatchArena {
    private String[] creators;
    private String name;
    private Set<Location> spawns;

    public DeathmatchArena(String[] creators, String name, Set<Location> spawns) {
        this.creators = creators;
        this.name = name;
        this.spawns = spawns;
    }

    public Set<Location> getSpawns() {
        return spawns;
    }

    public String getName() {
        return name;
    }

    public String[] getCreators() {
        return creators;
    }
}