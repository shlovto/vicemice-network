package net.vicemice.hector.packets.types.player.punish;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 13:09 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class KickPacket implements Serializable {

    @Getter
    private UUID uniqueId;
    @Getter
    private String reason;

    public KickPacket(UUID uniqueId, String reason) {
        this.uniqueId = uniqueId;
        this.reason = reason;
    }
}
