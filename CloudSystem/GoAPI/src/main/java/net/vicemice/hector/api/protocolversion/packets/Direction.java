package net.vicemice.hector.api.protocolversion.packets;

public enum Direction {
    OUTGOING,
    INCOMING
}
