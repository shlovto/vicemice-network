package net.vicemice.hector.packets.types.player.abuse;

import lombok.Getter;
import net.vicemice.hector.utils.Modifications;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 09:44 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class BlockModificationsPacket implements Serializable {

    @Getter
    private UUID uniqueId;
    @Getter
    private Modifications modifications;

    public BlockModificationsPacket(UUID uniqueId, Modifications modifications) {
        this.uniqueId = uniqueId;
        this.modifications = modifications;
    }
}