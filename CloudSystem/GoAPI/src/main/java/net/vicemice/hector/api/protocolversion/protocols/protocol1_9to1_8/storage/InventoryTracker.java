package net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.storage;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.api.protocolversion.api.data.StoredObject;
import net.vicemice.hector.api.protocolversion.api.data.UserConnection;

@Getter
@Setter
public class InventoryTracker extends StoredObject {
    private String inventory;

    public InventoryTracker(UserConnection user) {
        super(user);
    }
}
