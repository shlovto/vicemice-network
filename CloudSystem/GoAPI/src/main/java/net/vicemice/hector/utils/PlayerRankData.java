package net.vicemice.hector.utils;

/*
 * Class created at 06:16 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerRankData {
    private String rankName;
    private String rankColor;
    private int access_level;

    public PlayerRankData(String rankName, String rankColor, int access_level) {
        this.rankName = rankName;
        this.rankColor = rankColor;
        this.access_level = access_level;
    }

    /**
     * @return the Rank Name
     */
    public String getRankName() {
        return this.rankName;
    }

    /**
     * @return the Rank Color
     */
    public String getRankColor() {
        return this.rankColor;
    }

    /**
     * @return the Access Level
     */
    public int getAccess_level() {
        return this.access_level;
    }
}