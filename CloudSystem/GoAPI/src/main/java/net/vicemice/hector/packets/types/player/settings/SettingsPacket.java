package net.vicemice.hector.packets.types.player.settings;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SettingsPacket implements Serializable {

    private final UUID uniqueId;
    private Map<String, Object> settings;
    private final Map<String, Object> tempSettings;

    public SettingsPacket(UUID uniqueId, Map<String, Object> settings) {
        this.uniqueId = uniqueId;
        if (settings != null) {
            this.settings = settings;
        } else {
            this.create();
        }
        this.tempSettings = new HashMap<>();
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public Map<String, Object> getSettings() {
        return settings;
    }

    public Map<String, Object> getTempSettings() {
        return tempSettings;
    }

    public void setSettings(Map<String, Object> settings) {
        this.settings = settings;
    }

    public <T> T getTempSetting(String key, Class<T> clazz) {
        return clazz.cast(this.getTempSettings().get(key));
    }

    public <T> T getSetting(String key, Class<T> clazz) {
        return clazz.cast(this.getSettings().get(key));
    }

    public void setTempSetting(String key, Object value) {
        this.getTempSettings().remove(key);
        this.getTempSettings().put(key, value);
    }

    public void setSetting(String key, Object value) {
        this.getSettings().remove(key);
        this.getSettings().put(key, value);
    }

    public void addNewTempSettings(String key, Object value) {
        if (this.getSettings().containsKey(key)) return;
        this.getSettings().put(key, value);
    }

    public void addNewSettings(String key, Object value) {
        if (this.getSettings().containsKey(key)) return;
        this.getSettings().put(key, value);
    }

    public Map<String, Object> create() {
        if (this.getSettings() == null) {
            this.settings = new HashMap<>();
        }

        this.addNewSettings("advanced-player-settings", 0);

        this.addNewSettings("friend-sorting", 3);
        this.addNewSettings("friend-add", 1);

        this.addNewSettings("friend-add-members", 1);
        this.addNewSettings("friend-add-premiums", 1);
        this.addNewSettings("friend-add-vips", 1);
        this.addNewSettings("friend-add-staffs", 1);
        this.addNewSettings("friend-add-party", 1);
        this.addNewSettings("friend-add-clan", 1);

        this.addNewSettings("friend-jump", 1);
        this.addNewSettings("friend-messages", 1);
        this.addNewSettings("private-messages", 1);

        this.addNewSettings("clan-invites", 1);
        this.addNewSettings("clan-invites-members", 1);
        this.addNewSettings("clan-invites-premiums", 1);
        this.addNewSettings("clan-invites-vips", 1);
        this.addNewSettings("clan-invites-staffs", 1);
        this.addNewSettings("clan-invites-friends", 1);
        this.addNewSettings("clan-invites-friend-favorites", 1);
        this.addNewSettings("clan-invites-party", 1);

        this.addNewSettings("clan-messages", 1);
        this.addNewSettings("clan-chat-messages", 1);
        this.addNewSettings("clan-jump", 1);

        this.addNewSettings("party-invites", 0);
        this.addNewSettings("party-invites-members", 1);
        this.addNewSettings("party-invites-premiums", 1);
        this.addNewSettings("party-invites-vips", 1);
        this.addNewSettings("party-invites-staffs", 1);
        this.addNewSettings("party-invites-friends", 1);
        this.addNewSettings("party-invites-friend-favorites", 1);
        this.addNewSettings("party-invites-clan", 1);

        this.addNewSettings("player-visibility", 0);
        this.addNewSettings("player-visibility-members", 0);
        this.addNewSettings("player-visibility-premiums", 0);
        this.addNewSettings("player-visibility-vips", 0);
        this.addNewSettings("player-visibility-staffs", 0);
        this.addNewSettings("player-visibility-friends", 0);
        this.addNewSettings("player-visibility-friend-favorites", 0);
        this.addNewSettings("player-visibility-party", 0);
        this.addNewSettings("player-visibility-clan", 0);

        this.addNewSettings("ui-color", 0);
        this.addNewSettings("auto-nick", 0);
        this.addNewSettings("lobby-sounds", 1);
        this.addNewSettings("lobby-gadgets", 1);
        this.addNewSettings("lobby-particle", 1);
        this.addNewSettings("lobby-movement", 0);
        this.addNewSettings("lobby-sort-friends", 0);

        this.addNewSettings("last-active-lobby-head", null);
        this.addNewSettings("last-active-lobby-boots", null);
        this.addNewSettings("last-active-lobby-effect", null);
        this.addNewSettings("last-active-lobby-pet", null);
        this.addNewSettings("last-active-lobby-gadget", null);

        this.addNewSettings("hide-last-offline", 0);
        this.addNewSettings("page-view-online-server", 1);
        this.addNewSettings("page-view-last-offline", 1);
        this.addNewSettings("replay-history-private", 0);

        this.addNewSettings("collect-daily-reward-automatically", 0);

        this.addNewSettings("accept-tos", Boolean.valueOf("false"));

        return this.getSettings();
    }
}