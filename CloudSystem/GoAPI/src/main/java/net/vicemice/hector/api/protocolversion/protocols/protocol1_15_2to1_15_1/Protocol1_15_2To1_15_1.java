package net.vicemice.hector.api.protocolversion.protocols.protocol1_15_2to1_15_1;

import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.api.protocol.Protocol;

public class Protocol1_15_2To1_15_1 extends Protocol {

    @Override
    protected void registerPackets() {
    }

    @Override
    public void init(UserConnection user) {
    }
}
