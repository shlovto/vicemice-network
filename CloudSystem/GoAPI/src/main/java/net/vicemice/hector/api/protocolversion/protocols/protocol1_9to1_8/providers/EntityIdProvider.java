package net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.providers;

import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.api.platform.providers.Provider;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.storage.EntityTracker;

public class EntityIdProvider implements Provider {

    public int getEntityId(UserConnection user) throws Exception {
        return user.get(EntityTracker.class).getEntityID();
    }
}
