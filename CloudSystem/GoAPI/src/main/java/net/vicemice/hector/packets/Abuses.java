package net.vicemice.hector.packets;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 12:55 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class Abuses implements Serializable {

    private Abuse data;

    public Abuses(Abuse data) {
        this.data = data;
    }

    public Abuse getData() {
        return data;
    }

    @Getter
    public static class Abuse implements Serializable {
        private UUID author;
        private String reason;
        private long length;

        public Abuse(UUID author, String reason, long length) {
            this.author = author;
            this.reason = reason;
            this.length = length;
        }
    }
}