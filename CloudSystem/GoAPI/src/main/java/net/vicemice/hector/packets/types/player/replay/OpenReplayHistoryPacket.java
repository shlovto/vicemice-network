package net.vicemice.hector.packets.types.player.replay;

import lombok.Getter;
import net.vicemice.hector.utils.Rank;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/*
 * Class created at 06:52 - 18.04.2020
 * Copyright (C) elrobtossohn
 */
@Getter
public class OpenReplayHistoryPacket implements Serializable {

    private final UUID requester, target;
    private final String targetName;
    private final Rank targetRank;
    private final List<PacketReplayEntries.PacketReplayEntry> recentEntries, savedEntries;

    @ConstructorProperties(value = {"requester", "target", "targetName", "targetRank", "replayEntries"})
    public OpenReplayHistoryPacket(UUID requester, UUID target, String targetName, Rank targetRank, List<PacketReplayEntries.PacketReplayEntry> recentEntries, List<PacketReplayEntries.PacketReplayEntry> savedEntries) {
        this.requester = requester;
        this.target = target;
        this.targetName = targetName;
        this.targetRank = targetRank;
        this.recentEntries = recentEntries;
        this.savedEntries = savedEntries;
    }
}
