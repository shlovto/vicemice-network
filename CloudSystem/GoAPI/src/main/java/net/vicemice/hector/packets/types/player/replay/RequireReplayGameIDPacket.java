package net.vicemice.hector.packets.types.player.replay;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:36 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class RequireReplayGameIDPacket implements Serializable {
    @Getter
    private UUID uuid;

    public RequireReplayGameIDPacket(UUID uuid) {
        this.uuid = uuid;
    }
}
