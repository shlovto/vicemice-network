package net.vicemice.hector.utils.jfuture;

import net.vicemice.hector.packets.types.player.stats.StatisticPlayerInfoPacket;

import java.util.concurrent.Future;

/*
 * Class created at 01:06 - 04.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class InstantProgressFuture<V> extends ObjectProgressFuture<V> {
    public InstantProgressFuture(V obj) {
        this.done(obj);
    }

    @Override
    public void done(StatisticPlayerInfoPacket pkt) {

    }

    @Override
    public Future<V> asJavaFuture() {
        return null;
    }

    @Override
    public void throwErrors() throws Exception {

    }

    @Override
    public void throwErrors(Exception _default) throws Exception {
        _default.printStackTrace();
    }
}
