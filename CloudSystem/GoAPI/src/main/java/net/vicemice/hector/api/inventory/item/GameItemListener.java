package net.vicemice.hector.api.inventory.item;

import net.vicemice.hector.api.NetworkAPI;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class GameItemListener implements Listener {
    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        ItemStack clickedItem;
        Player player = event.getPlayer();
        if (event.getAction() != Action.PHYSICAL && player.getItemInHand() != null && (clickedItem = player.getItemInHand()).hasItemMeta() && clickedItem.getItemMeta().hasDisplayName()) {
            for (GameItem gameItem : NetworkAPI.getInstance().getGameItems()) {
                ItemStack item = gameItem.getItemStack();
                if (!gameItem.isCalled(event.getAction()) || clickedItem.getType() != item.getType() || !clickedItem.getItemMeta().getDisplayName().equals(item.getItemMeta().getDisplayName())) continue;
                gameItem.action(player, event.getAction());
                if (!gameItem.isCancelOnClick()) continue;
                event.setCancelled(true);
                return;
            }
        }
    }
}
