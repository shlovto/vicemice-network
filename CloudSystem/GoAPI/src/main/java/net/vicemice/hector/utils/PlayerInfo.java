package net.vicemice.hector.utils;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 11:30 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerInfo implements Serializable {
    @Getter
    public String name, signature, value;
    @Getter
    public UUID uniqueId;
    @Getter
    public Rank rank;
    @Getter
    public boolean online;

    public PlayerInfo(String name, UUID uniqueId, Rank rank, String signature, String value, boolean online) {
        this.name = name;
        this.rank = rank;
        this.uniqueId = uniqueId;
        this.signature = signature;
        this.value = value;
        this.online = online;
    }
}