package net.vicemice.hector.api.protocolversion.protocols.protocol1_13to1_12_2.providers.blockentities;

import net.vicemice.hector.api.nbt.tag.builtin.CompoundTag;
import net.vicemice.hector.api.nbt.tag.builtin.StringTag;
import net.vicemice.hector.api.nbt.tag.builtin.Tag;
import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_13to1_12_2.ChatRewriter;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_13to1_12_2.providers.BlockEntityProvider;

public class CommandBlockHandler implements BlockEntityProvider.BlockEntityHandler {
    @Override
    public int transform(UserConnection user, CompoundTag tag) {
        Tag name = tag.get("CustomName");
        if (name instanceof StringTag) {
            ((StringTag) name).setValue(ChatRewriter.legacyTextToJson(((StringTag) name).getValue()));
        }
        Tag out = tag.get("LastOutput");
        if (out instanceof StringTag) {
            ((StringTag) out).setValue(ChatRewriter.processTranslate(((StringTag) out).getValue()));
        }
        return -1;
    }
}
