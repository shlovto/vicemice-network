package net.vicemice.hector.utils.players.clan;

import lombok.Getter;
import net.vicemice.hector.utils.PlayerInfo;

import java.io.Serializable;
import java.util.Map;
import java.util.UUID;

public class Clan implements Serializable {
    @Getter
    private final String id;
    @Getter
    private final UUID uuid;
    @Getter
    private final String name;
    @Getter
    private final String tag;
    @Getter
    private final Map<PlayerInfo, ClanRank> members;

    public Clan(String id, UUID uuid, String name, String tag, Map<PlayerInfo, ClanRank> members) {
        this.id = id;
        this.uuid = uuid;
        this.name = name;
        this.tag = tag;
        this.members = members;
    }
}