package net.vicemice.hector.gameapi.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameLobbyEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private int time;

    public GameLobbyEvent(int time) {
        this.time = time;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public int getTime() {
        return time;
    }

    public HandlerList getHandlers() {
        return handlers;
    }
}