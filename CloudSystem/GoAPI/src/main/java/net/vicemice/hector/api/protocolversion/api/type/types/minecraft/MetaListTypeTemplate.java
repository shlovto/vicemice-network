package net.vicemice.hector.api.protocolversion.api.type.types.minecraft;

import net.vicemice.hector.api.protocolversion.api.minecraft.metadata.Metadata;
import net.vicemice.hector.api.protocolversion.api.type.Type;

import java.util.List;

public abstract class MetaListTypeTemplate extends Type<List<Metadata>> {
    public MetaListTypeTemplate() {
        super("MetaData List", List.class);
    }

    @Override
    public Class<? extends Type> getBaseClass() {
        return MetaListTypeTemplate.class;
    }
}
