package net.vicemice.hector.api.protocolversion.protocols.protocol1_13to1_12_2.storage;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.api.protocolversion.api.PacketWrapper;
import net.vicemice.hector.api.protocolversion.api.data.StoredObject;
import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.api.type.Type;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_13to1_12_2.Protocol1_13To1_12_2;

@Getter
@Setter
public class TabCompleteTracker extends StoredObject {
    private int transactionId;
    private String input;
    private String lastTabComplete;
    private long timeToSend;

    public TabCompleteTracker(UserConnection user) {
        super(user);
    }

    public void sendPacketToServer() {
        if (lastTabComplete == null || timeToSend > System.currentTimeMillis()) return;
        PacketWrapper wrapper = new PacketWrapper(0x01, null, getUser());
        wrapper.write(Type.STRING, lastTabComplete);
        wrapper.write(Type.BOOLEAN, false);
        wrapper.write(Type.OPTIONAL_POSITION, null);
        try {
            wrapper.sendToServer(Protocol1_13To1_12_2.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        lastTabComplete = null;
    }
}
