package net.vicemice.hector.events;

import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerUnnickEvent extends Event implements Cancellable  {
    private static HandlerList handlers = new HandlerList();
    private IUser user;
    private String nickName;
    private boolean cancel = false;

    public PlayerUnnickEvent(IUser user, String nickName) {
        this.user = user;
        this.nickName = nickName;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public IUser getUser() {
        return user;
    }

    public String getNickName() {
        return this.nickName;
    }

    @Override
    public boolean isCancelled() {
        return cancel;
    }

    @Override
    public void setCancelled(boolean b) {
        this.cancel = b;
    }
}

