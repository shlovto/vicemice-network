package net.vicemice.hector.utils;

/*
 * Class created at 09:45 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public enum Modifications {
    FORGEWURST,
    WORLDDOWNLOADER,
    FIVEZIG,
    BSM,
    SERVERUI,
    PERMISSIONSREPL,
    DIPERMISSIONS,
    LITELOADER,
    MCP,
    UNKNOWN;
}