package net.vicemice.hector.packets.types.server.clan;

import lombok.Getter;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:44 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ClanRequestPacket implements Serializable {

    @Getter
    private UUID uuid;

    @ConstructorProperties(value = {"uuid"})
    public ClanRequestPacket(UUID uuid) {
        this.uuid = uuid;
    }
}
