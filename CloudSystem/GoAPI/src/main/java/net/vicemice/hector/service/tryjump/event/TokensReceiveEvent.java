package net.vicemice.hector.service.tryjump.event;

import net.vicemice.hector.service.tryjump.TryJumpPlayer;

/*
 * Class created at 01:46 - 10.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 * <p>Event gets fired when a player receives tokens. This happens when the player completes a unit.
 */
public class TokensReceiveEvent extends TryJumpEvent {

    /** Gets the amount of tokens that have been added to a {@link TryJumpPlayer}. */
    public int getAmount() {
        return amount;
    }

    private int amount;

    public TokensReceiveEvent(TryJumpPlayer player, int amount) {
        super(player);
        this.amount = amount;
    }
}
