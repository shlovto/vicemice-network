package net.vicemice.hector.utils.gameserver;

import lombok.Getter;
import net.vicemice.hector.utils.Rank;

import java.io.Serializable;

@Getter
public class ServerData implements Serializable {

    public ServerData(String template, String name, int players, int maxPlayers, String message, int version, Rank rank, Type type, boolean playAble) {
        this.template = template;
        this.name = name;
        this.players = players;
        this.maxPlayers = maxPlayers;
        this.message = message;
        this.version = version;
        this.rank = rank;
        this.type = type;
        this.playAble = playAble;
    }

    private String template, name, message;
    private int players, maxPlayers, version;
    private Rank rank;
    private Type type;
    private boolean playAble;

    public enum Type {
        LOBBY,
        INGAME,
        RESTART
    }
}