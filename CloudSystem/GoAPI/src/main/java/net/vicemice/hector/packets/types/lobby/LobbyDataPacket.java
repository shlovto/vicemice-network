package net.vicemice.hector.packets.types.lobby;

import lombok.Getter;
import net.vicemice.hector.packets.types.lobby.type.FallbackDataInfo;
import net.vicemice.hector.packets.types.lobby.type.LobbyDataInfo;
import net.vicemice.hector.packets.types.lobby.type.VerifyDataInfo;

import java.io.Serializable;
import java.util.List;

/*
 * Class created at 13:19 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class LobbyDataPacket implements Serializable {

    @Getter
    private final List<LobbyDataInfo> lobbyDataInfos;
    @Getter
    private final List<FallbackDataInfo> fallbackDataInfos;
    @Getter
    private final List<VerifyDataInfo> verifyDataInfos;

    public LobbyDataPacket(List<LobbyDataInfo> lobbyDataInfos, List<FallbackDataInfo> fallbackDataInfos, List<VerifyDataInfo> verifyDataInfos) {
        this.lobbyDataInfos = lobbyDataInfos;
        this.fallbackDataInfos = fallbackDataInfos;
        this.verifyDataInfos = verifyDataInfos;
    }
}
