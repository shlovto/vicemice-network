package net.vicemice.hector.packets.types.server;

import lombok.Getter;

import java.io.Serializable;

/*
 * Class created at 18:44 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class CloudServerCallBack implements Serializable {

    @Getter
    private int port;

    public CloudServerCallBack(int port) {
        this.port = port;
    }
}