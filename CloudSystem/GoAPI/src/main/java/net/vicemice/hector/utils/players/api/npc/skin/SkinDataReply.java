package net.vicemice.hector.utils.players.api.npc.skin;

public interface SkinDataReply {
    void done(SkinData skinData);
}