package net.vicemice.hector.utils.players;

import lombok.Data;
import java.util.UUID;

/*
 * Class created at 11:59 - 15.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class FriendData {

    private UUID uniqueId;
    private long date;
    private boolean favorite;

    private FriendData() {
        throw new UnsupportedOperationException("Not supported.");
    }

    public FriendData(UUID uniqueId, long date, boolean favorite) {
        this.uniqueId = uniqueId;
        this.date = date;
        this.favorite = favorite;
    }
}
