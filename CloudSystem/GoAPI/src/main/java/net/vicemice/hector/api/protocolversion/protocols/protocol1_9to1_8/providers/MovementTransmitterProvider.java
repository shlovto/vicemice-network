package net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.providers;

import io.netty.channel.ChannelHandlerContext;
import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.api.platform.providers.Provider;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.storage.MovementTracker;
import net.vicemice.hector.api.protocolversion.util.PipelineUtil;

public abstract class MovementTransmitterProvider implements Provider {
    public abstract Object getFlyingPacket();

    public abstract Object getGroundPacket();

    public void sendPlayer(UserConnection userConnection) {
        // Old method using packets.
        ChannelHandlerContext context = PipelineUtil.getContextBefore("decoder", userConnection.getChannel().pipeline());
        if (context != null) {
            if (userConnection.get(MovementTracker.class).isGround()) {
                context.fireChannelRead(getGroundPacket());
            } else {
                context.fireChannelRead(getFlyingPacket());
            }
            userConnection.get(MovementTracker.class).incrementIdlePacket();
        }
    }
}
