package net.vicemice.hector.packets.types.lobby;

import lombok.Getter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*
 * Class created at 18:18 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class LobbyIngameServerPacket implements Serializable {

    @Getter
    private String templateName;
    @Getter
    private List<String[]> servers = new ArrayList<>();

    public LobbyIngameServerPacket(String templateName, List<String[]> servers) {
        this.templateName = templateName;
        this.servers = servers;
    }
}
