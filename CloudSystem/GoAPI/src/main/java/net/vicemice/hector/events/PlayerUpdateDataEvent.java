package net.vicemice.hector.events;

import net.vicemice.hector.utils.players.utils.DataUpdateType;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.UUID;

public class PlayerUpdateDataEvent extends Event {
    private static HandlerList handlers = new HandlerList();
    private UUID uniqueId;
    private DataUpdateType dataUpdateType;

    public PlayerUpdateDataEvent(UUID uniqueId, DataUpdateType dataUpdateType) {
        this.uniqueId = uniqueId;
        this.dataUpdateType = dataUpdateType;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public DataUpdateType getDataUpdateType() {
        return dataUpdateType;
    }
}
