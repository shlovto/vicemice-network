package net.vicemice.hector.packets.types;

import java.io.Serializable;

/*
 * Class created at 18:36 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ServerCallBackInfos implements Serializable {
    private String name;

    public ServerCallBackInfos(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}