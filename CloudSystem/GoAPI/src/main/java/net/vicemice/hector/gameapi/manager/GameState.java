package net.vicemice.hector.gameapi.manager;

public enum GameState {
    PREPARING, WARMUP, LOBBY, INGAME, END, RESTART
}
