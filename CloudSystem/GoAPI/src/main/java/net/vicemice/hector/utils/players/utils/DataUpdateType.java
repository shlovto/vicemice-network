package net.vicemice.hector.utils.players.utils;

/*
 * Enum created at 22:05 - 01.04.2020
 * Copyright (C) elrobtossohn
 */
public enum DataUpdateType {
    JOIN,
    UPDATE_REPLAYS,
    UPDATE_SETTINGS,
    UPDATE_RANK,
    UPDATE_PLAYTIME,
    UPDATE_FRIENDS,
    UPDATE_LOCALE,
    UPDATE_COINS,
    UPDATE_VOTES,
    UPDATE_NICK,
    UPDATE_LEVEL
}