package net.vicemice.hector.api.protocolversion.protocols.protocol1_13to1_12_2.providers.blockentities;

import net.vicemice.hector.api.nbt.tag.builtin.CompoundTag;
import net.vicemice.hector.api.nbt.tag.builtin.Tag;
import net.vicemice.hector.api.protocolversion.api.Via;
import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.api.minecraft.Position;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_13to1_12_2.providers.BlockEntityProvider;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_13to1_12_2.storage.BlockStorage;

public class BedHandler implements BlockEntityProvider.BlockEntityHandler {

    @Override
    public int transform(UserConnection user, CompoundTag tag) {
        BlockStorage storage = user.get(BlockStorage.class);
        Position position = new Position(getLong(tag.get("x")), getLong(tag.get("y")), getLong(tag.get("z")));

        if (!storage.contains(position)) {
            Via.getPlatform().getLogger().warning("Received an bed color update packet, but there is no bed! O_o " + tag);
            return -1;
        }

        //                                              RED_BED + FIRST_BED
        int blockId = storage.get(position).getOriginal() - 972 + 748;

        Tag color = tag.get("color");
        if (color != null) {
            blockId += (((Number) color.getValue()).intValue() * 16);
        }

        return blockId;
    }

    private long getLong(Tag tag) {
        return ((Integer) tag.getValue()).longValue();
    }
}
