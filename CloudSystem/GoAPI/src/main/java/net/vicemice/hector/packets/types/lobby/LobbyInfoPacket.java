package net.vicemice.hector.packets.types.lobby;

import lombok.Getter;
import net.vicemice.hector.utils.gameserver.ServerData;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/*
 * Class created at 18:18 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class LobbyInfoPacket implements Serializable {

    @Getter
    private ConcurrentHashMap<String, Integer> games;
    @Getter
    private List<ServerData> serverData;

    public LobbyInfoPacket(ConcurrentHashMap<String, Integer> games, List<ServerData> serverData) {
        this.games = games;
        this.serverData = serverData;
    }
}