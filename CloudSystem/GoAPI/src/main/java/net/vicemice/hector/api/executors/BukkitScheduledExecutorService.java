package net.vicemice.hector.api.executors;

import com.google.common.util.concurrent.ListeningScheduledExecutorService;

import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Represents a listening scheduler service that returns {@link ListenableScheduledFuture} instead of {@link ScheduledFuture}.
 * @author Kristian
 */
public interface BukkitScheduledExecutorService extends ListeningScheduledExecutorService {
	@Override
	public com.google.common.util.concurrent.ListenableScheduledFuture<?> schedule(
			Runnable command, long delay, TimeUnit unit);

	@Override
	public <V> com.google.common.util.concurrent.ListenableScheduledFuture<V> schedule(
			Callable<V> callable, long delay, TimeUnit unit);

	@Override
	public com.google.common.util.concurrent.ListenableScheduledFuture<?> scheduleAtFixedRate(
			Runnable command, long initialDelay, long period, TimeUnit unit);

	/**
	 * This is not supported by the underlying Bukkit scheduler.
	 */
	@Override
	@Deprecated
	public com.google.common.util.concurrent.ListenableScheduledFuture<?> scheduleWithFixedDelay(
			Runnable command, long initialDelay, long delay, TimeUnit unit);
}