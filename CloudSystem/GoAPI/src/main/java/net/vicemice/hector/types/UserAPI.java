package net.vicemice.hector.types;

import net.vicemice.hector.packets.types.player.replay.PacketEditReplay;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.clan.Clan;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.hector.utils.players.party.Party;
import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;

/*
 * Interface created at 17:56 - 31.03.2020
 * Copyright (C) elrobtossohn
 */
public interface UserAPI {

    /**
     * Request Party Data from a player
     * @param uniqueId defines the unique id of an player
     */
    @Deprecated
    public void requestParty(UUID uniqueId);

    /**
     * Get Party Data from a player
     * @param uniqueId defines the unique id of an player
     * @return returns the clan data from a player
     */
    @Deprecated
    public Party getParty(UUID uniqueId);

    /**
     * Get Party Data from a player
     * @param callback defines the callback
     */
    @Deprecated
    public void getPublicParties(Callback<ArrayList<Party>> callback);

    /**
     * Request Clan Data from a player
     * @param uniqueId defines the unique id of an player
     */
    @Deprecated
    public void requestClan(UUID uniqueId);

    /**
     * Get Clan Data from a player
     * @param uniqueId defines the unique id of an player
     * @return returns the clan data from a player
     */
    @Deprecated
    public Clan getClan(UUID uniqueId);

    /**
     * Get Rank Data from a player
     * @param uniqueId defines the unique id of an player
     * @return returns the rank data from an player
     */
    @Deprecated
    public PlayerRankData getRankData(UUID uniqueId);

    /**
     * Add xp to a player
     * @param uniqueId defines the unique id of a player
     * @param xp defines the number of xp
     */
    @Deprecated
    public void addXP(UUID uniqueId, long xp);

    /**
     * Remove xp from a player
     * @param uniqueId defines the unique id of a player
     * @param xp defines the number of xp
     */
    @Deprecated
    public void removeXP(UUID uniqueId, long xp);

    /**
     * Add coins to a player
     * @param uniqueId defines the unique id of a player
     * @param coins defines the number of coins
     */
    @Deprecated
    public void addCoins(UUID uniqueId, long coins);

    /**
     * Remove coins from a player
     * @param uniqueId defines the unique id of a player
     * @param coins defines the number of coins
     */
    @Deprecated
    public void removeCoins(UUID uniqueId, long coins);

    /**
     * Get coins to a player
     * @param uniqueId defines the unique id of a player
     * @param callback defines the callback
     */
    @Deprecated
    public void getCoins(UUID uniqueId, Callback<Long> callback);

    /**
     * Send a Packet to the Cloud that a Player want to watch a replay
     * @param uniqueId set the unique id of an player
     * @param gameId set the gameId which should be replayed
     */
    @Deprecated
    public void watchGame(UUID uniqueId, String gameId);

    /**
     * Add a Replay Game to the Player, he can watch this later
     * @param uniqueId set the unique id of the operated player
     * @param gameId set the game id of the game
     * @param replayUUID set the replay unique id
     * @param timestamp set the timestamp which the replay was started or ended.
     * @param gameLength set the length of the game
     * @param players set the players which played the game
     * @param gameType set the game type
     * @param mapType set the map type
     * @param map set the map name
     */
    @Deprecated
    public void addRecordedGame(UUID uniqueId, String gameId, UUID replayUUID, int replayStartSeconds, long timestamp, long gameLength, ArrayList<String> players, String gameType, String mapType, String map, boolean save);

    /**
     * Add a Replay Game to the Player, he can watch this later
     * @param uniqueId set the unique id of the operated player
     * @param gameId set the game id of the game
     * @param editType defines the Edit Type.
     */
    @Deprecated
    public void editReplayFavorites(UUID uniqueId, String gameId, PacketEditReplay.EditType editType);

    /**
     * Get a Locale from a Player
     * @param uniqueId defines the unique id of an player
     * @return returns the Language from an Player
     */
    @Deprecated
    public Locale getLocale(UUID uniqueId);

    /**
     * Get a message from a key which are translated
     * @param uniqueId defines the unique id of an player
     * @param string defines the message key
     * @return returns a null or a message
     */
    @Deprecated
    public String translate(UUID uniqueId, String string);

    /**
     * Get a message from a key which are translated and replace {id} objects
     * @param uniqueId defines the unique id of an player
     * @param string defines the message key
     * @param objects replace all {id} objects in the message
     * @return returns a null or a message
     */
    @Deprecated
    public String translate(UUID uniqueId, String string, Object... objects);

    /**
     * @param prefix set the game mode prefix
     * @param type set the type e.q. DAILY, MONTHLY, GLOBAL
     * @param tops set how many tops
     * @param callback set the callback
     */
    public void getGameTop(String prefix, String type, int tops, Callback callback);

    /**
     * Add Statics to an player
     * @param uniqueId set the player unique id
     * @param key set the value key
     * @param value set the value
     */
    @Deprecated
    public void increaseStatistic(UUID uniqueId, String key, long value);

    /**
     * Remove Statics to an player
     * @param uniqueId set the player unique id
     * @param key set the value key
     * @param value set the value
     */
    @Deprecated
    public void decreaseStatistic(UUID uniqueId, String key, long value);

    /**
     * Set Statics to an player
     * @param uniqueId set the player unique id
     * @param key set the value key
     * @param value set the value
     */
    @Deprecated
    public void setStatistic(UUID uniqueId, String key, long value);

    /**
     * Get Statics from an player
     * @param uniqueId set the player unique id
     * @param key set the value key
     * @param period set the StatisticPeriod e.g. Global, Monthly or Daily Stats
     */
    @Deprecated
    public Long getPlayerStatistic(UUID uniqueId, String key, StatisticPeriod period);

    /**
     * Get Statics from an player
     * @param uniqueId set the player unique id
     * @param key set the value key
     * @param period set the StatisticPeriod e.g. Global, Monthly or Daily Stats
     */
    @Deprecated
    public Long getPlayerRanking(UUID uniqueId, String key, StatisticPeriod period);

    /**
     * Get the Nick UniqueId of an player
     * @param uniqueId defines the unique id of the real player
     * @return returns the nickname or the name of the player
     */
    @Deprecated
    public UUID getUniqueId(UUID uniqueId);

    /**
     * Get the Nickname of an player
     * @param uniqueId defines the unique id of an player
     * @return returns the nickname or the name of the player
     */
    @Deprecated
    public String getNickName(UUID uniqueId);

    /**
     * Check if an Player is nicked
     * @param uniqueId defines the unique id of an player
     * @return returns true or false
     */
    @Deprecated
    public boolean isNicked(UUID uniqueId);
}
