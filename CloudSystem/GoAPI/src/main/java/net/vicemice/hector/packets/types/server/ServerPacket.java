package net.vicemice.hector.packets.types.server;

import net.vicemice.hector.types.ServerState;

import java.beans.ConstructorProperties;
import java.io.Serializable;

/*
 * Class created at 18:36 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ServerPacket implements Serializable {
    private String name;
    private String host;
    private int port;
    private String rank;
    private int version;
    private int maxPlayers;
    private ServerState serverState;
    private boolean manualAdd;
    private String msg;
    private String serverGroup;

    public ServerPacket(String name, String host, int port, String rank, int version, ServerState serverState, boolean manualAdd, String msg, int maxPlayers, String serverGroup) {
        this.name = name;
        this.host = host;
        this.port = port;
        this.rank = rank;
        this.version = version;
        this.serverState = serverState;
        this.manualAdd = manualAdd;
        this.msg = msg;
        this.maxPlayers = maxPlayers;
        this.serverGroup = serverGroup;
    }

    @ConstructorProperties(value={"name", "host", "port", "rank", "version", "maxPlayers", "serverState", "manualAdd", "msg", "serverGroup"})
    public ServerPacket(String name, String host, int port, String rank, int version, int maxPlayers, ServerState serverState, boolean manualAdd, String msg, String serverGroup) {
        this.name = name;
        this.host = host;
        this.port = port;
        this.rank = rank;
        this.version = version;
        this.maxPlayers = maxPlayers;
        this.serverState = serverState;
        this.manualAdd = manualAdd;
        this.msg = msg;
        this.serverGroup = serverGroup;
    }

    public String getName() {
        return this.name;
    }

    public String getHost() {
        return this.host;
    }

    public int getPort() {
        return this.port;
    }

    public String getRank() {
        return rank;
    }

    public int getVersion() {
        return version;
    }

    public int getMaxPlayers() {
        return this.maxPlayers;
    }

    public ServerState getServerState() {
        return this.serverState;
    }

    public boolean isManualAdd() {
        return this.manualAdd;
    }

    public String getMsg() {
        return this.msg;
    }

    public String getServerGroup() {
        return this.serverGroup;
    }
}