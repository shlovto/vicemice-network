package net.vicemice.hector.events;

import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerUnnickedEvent extends Event {
    private static HandlerList handlers = new HandlerList();
    private IUser user;

    public PlayerUnnickedEvent(IUser user) {
        this.user = user;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public IUser getUser() {
        return user;
    }
}

