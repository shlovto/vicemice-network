package net.vicemice.hector.api.inventory.api.option;

import lombok.NonNull;
import net.vicemice.hector.api.async.Callback;
import net.vicemice.hector.api.inventory.api.LocalizedInventoryOption;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class ActionOption extends LocalizedInventoryOption {
    private final Plugin plugin;
    @NonNull
    private Callback<Player> action = new Callback<Player>(){

        @Override
        public void done(Player object) {
        }
    };

    public ActionOption(Callback<Player> action) {
        this(action, Bukkit.getPluginManager().getPlugin("GoCore"));
    }

    public ActionOption(Callback<Player> action, Plugin plugin) {
        this.plugin = plugin;
        if (action != null) {
            this.action = action;
        }
    }

    @Override
    public void onClick(final Player player) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable(){

            @Override
            public void run() {
                if (player.isOnline()) {
                    ActionOption.this.action.done(player);
                }
            }
        });
    }

    @NonNull
    public Callback<Player> getAction() {
        return this.action;
    }

    public ActionOption setAction(@NonNull Callback<Player> action) {
        if (action == null) {
            throw new NullPointerException("action");
        }
        this.action = action;
        return this;
    }
}