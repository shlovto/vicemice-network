package net.vicemice.hector.types;

/*
 * Class created at 05:48 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public interface Callback<Object> {
    public void done(Object var1);
}