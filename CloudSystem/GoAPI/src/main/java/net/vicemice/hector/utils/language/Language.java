package net.vicemice.hector.utils.language;

import lombok.Getter;

import java.io.Serializable;

/*
 * Class created at 06:26 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class Language implements Serializable {

    public static enum LanguageType implements Serializable {
        CLOUD("cloud", false),
        GERMAN("german"),
        ENGLISH("english"),
        SPANISH("spanish", false);

        @Getter
        public String key;
        @Getter
        public boolean available;

        private LanguageType(String key) {
            this.key = key;
            this.available = true;
        }

        private LanguageType(String key, boolean available) {
            this.key = key;
            this.available = available;
        }

        public static LanguageType byKey(String key) {
            for (LanguageType type : values()) {
                if (!type.getKey().equals(key)) continue;
                return type;
            }
            return null;
        }

        public static Integer all() {
            int i = 0;
            for (LanguageType type : values()) {
                if (!type.isAvailable()) continue;
                i++;
            }
            return i;
        }
    }
}