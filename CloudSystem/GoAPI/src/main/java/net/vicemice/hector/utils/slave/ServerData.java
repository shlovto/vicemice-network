package net.vicemice.hector.utils.slave;

import net.vicemice.hector.utils.ServerType;
import java.io.Serializable;

/*
 * Class created at 09:06 - 23.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ServerData implements Serializable {

    private String slave, name;
    private int port;
    private ServerType serverType;

    public ServerData(String slave, String name, int port, ServerType serverType) {
        this.slave = slave;
        this.name = name;
        this.port = port;
        this.serverType = serverType;
    }

    public String getSlave() {
        return slave;
    }

    public String getName() {
        return name;
    }

    public int getPort() {
        return port;
    }

    public ServerType getServerType() {
        return serverType;
    }
}
