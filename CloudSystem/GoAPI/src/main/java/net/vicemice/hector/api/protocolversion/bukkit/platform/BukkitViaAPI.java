package net.vicemice.hector.api.protocolversion.bukkit.platform;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import net.vicemice.hector.api.protocolversion.ViaVersionPlugin;
import net.vicemice.hector.api.protocolversion.api.Via;
import net.vicemice.hector.api.protocolversion.api.ViaAPI;
import net.vicemice.hector.api.protocolversion.api.ViaVersionAPI;
import net.vicemice.hector.api.protocolversion.api.boss.BossBar;
import net.vicemice.hector.api.protocolversion.api.boss.BossColor;
import net.vicemice.hector.api.protocolversion.api.boss.BossStyle;
import net.vicemice.hector.api.protocolversion.api.command.ViaVersionCommand;
import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.api.protocol.ProtocolRegistry;
import net.vicemice.hector.api.protocolversion.boss.ViaBossBar;
import net.vicemice.hector.api.protocolversion.bukkit.util.ProtocolSupportUtil;
import net.vicemice.hector.api.protocolversion.protocols.base.ProtocolInfo;

import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

@AllArgsConstructor
public class BukkitViaAPI implements ViaAPI<Player>, ViaVersionAPI {
    private ViaVersionPlugin plugin;

    @Override
    public int getPlayerVersion(@NonNull Player player) {
        if (!isPorted(player))
            return getExternalVersion(player);
        return getPortedPlayers().get(player.getUniqueId()).get(ProtocolInfo.class).getProtocolVersion();
    }

    @Override
    public int getPlayerVersion(@NonNull UUID uuid) {
        if (!isPorted(uuid))
            return getExternalVersion(Bukkit.getPlayer(uuid));
        return getPortedPlayers().get(uuid).get(ProtocolInfo.class).getProtocolVersion();
    }

    private int getExternalVersion(Player player) {
        if (!isProtocolSupport()) {
            return ProtocolRegistry.SERVER_PROTOCOL;
        } else {
            return ProtocolSupportUtil.getProtocolVersion(player);
        }
    }

    @Override
    public boolean isPorted(Player player) {
        return isPorted(player.getUniqueId());
    }

    @Override
    public boolean isPorted(UUID playerUUID) {
        return getPortedPlayers().containsKey(playerUUID);
    }

    @Override
    public String getVersion() {
        return Bukkit.getPluginManager().getPlugin("Hector").getDescription().getVersion();
    }

    @Override
    public void sendRawPacket(UUID uuid, ByteBuf packet) throws IllegalArgumentException {
        if (!isPorted(uuid)) throw new IllegalArgumentException("This player is not controlled by ViaVersion!");
        UserConnection ci = getPortedPlayers().get(uuid);
        ci.sendRawPacket(packet);
    }

    @Override
    public void sendRawPacket(Player player, ByteBuf packet) throws IllegalArgumentException {
        sendRawPacket(player.getUniqueId(), packet);
    }

    @Override
    public BossBar createBossBar(String title, BossColor color, BossStyle style) {
        return new ViaBossBar(title, 1F, color, style);
    }

    @Override
    public BossBar createBossBar(String title, float health, BossColor color, BossStyle style) {
        return new ViaBossBar(title, health, color, style);
    }

    @Override
    public boolean isDebug() {
        return Via.getManager().isDebug();
    }

    @Override
    public ViaVersionCommand getCommandHandler() {
        return Via.getManager().getCommandHandler();
    }

    @Override
    public SortedSet<Integer> getSupportedVersions() {
        SortedSet<Integer> outputSet = new TreeSet<>(ProtocolRegistry.getSupportedVersions());
        outputSet.removeAll(Via.getPlatform().getConf().getBlockedProtocols());

        return outputSet;
    }

    @Override
    public boolean isCompatSpigotBuild() {
        return plugin.isCompatSpigotBuild();
    }


    @Override
    public boolean isSpigot() {
        return plugin.isSpigot();
    }

    @Override
    public boolean isProtocolSupport() {
        return plugin.isProtocolSupport();
    }

    public Map<UUID, UserConnection> getPortedPlayers() {
        return Via.getManager().getPortedPlayers();
    }
}
