package net.vicemice.hector.packets.types.player;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:17 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class LoginPacket implements Serializable {

    @Getter
    private final UUID uniqueId;
    @Getter
    private final String name;
    @Getter
    private final Integer version;
    @Getter
    private final String server, ip, proxyName, skinValue, skinSignature;

    public LoginPacket(UUID uniqueId, String server, String name, Integer version, String ip, String proxyName, String skinValue, String skinSignature) {
        this.uniqueId = uniqueId;
        this.server = server;
        this.name = name;
        this.version = version;
        this.ip = ip;
        this.proxyName = proxyName;
        this.skinValue = skinValue;
        this.skinSignature = skinSignature;
    }
}
