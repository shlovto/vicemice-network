package net.vicemice.hector.types;

import java.util.UUID;

/*
 * Class created at 05:54 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public interface DatabaseAPI {

    /**
     * Call the Cloud to save the Player Database Value which is internal of the cloud
     * @param uniqueId defines the unique id of a player
     * @param key defines the key of the value
     * @param value defines the value which should be saved
     */
    public void savePlayerDatabaseValue(UUID uniqueId, String key, String value);

    /**
     * Get an Database Value from the Players Database
     * @param uniqueId defines the unique id of a player
     * @param key defines the key to get an value
     * @param callback defines the callback which should be runned when the request is finished and an valid response was sent
     */
    public void getPlayerDatabaseValue(UUID uniqueId, String key, DatabaseCallback callback);

    /**
     * Get an Database Value from an specific Database
     * @param uniqueId defines the unique id of a player
     * @param database defines the database in which the value should be saved
     * @param playerKey defines the player key
     * @param key defines the key to get an value
     * @param callback defines the callback which should be runned when the request is finished and an valid response was sent
     */
    public void getDatabaseValue(UUID uniqueId, String database, String playerKey, String key, DatabaseCallback callback);
}