package net.vicemice.hector.api.inventory.api.option;

import lombok.NonNull;
import net.vicemice.hector.api.async.Callback;
import net.vicemice.hector.api.inventory.api.LocalizedInventoryOption;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.beans.ConstructorProperties;

public class ActionWithRefOption extends LocalizedInventoryOption {
    @NonNull
    private Callback<ActionCallbackObject> action = new Callback<ActionCallbackObject>(){

        @Override
        public void done(ActionCallbackObject object) {
        }
    };

    public ActionWithRefOption(Callback<ActionCallbackObject> action) {
        if (action != null) {
            this.action = action;
        }
    }

    @Override
    public void onClick(final Player player) {
        final ActionWithRefOption current = this;
        Bukkit.getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("GoCore"), new Runnable(){

            @Override
            public void run() {
                if (player.isOnline()) {
                    ActionWithRefOption.this.action.done(new ActionCallbackObject(player, current));
                }
            }
        });
    }

    @NonNull
    public Callback<ActionCallbackObject> getAction() {
        return this.action;
    }

    public ActionWithRefOption setAction(@NonNull Callback<ActionCallbackObject> action) {
        if (action == null) {
            throw new NullPointerException("action");
        }
        this.action = action;
        return this;
    }

    public static class ActionCallbackObject {
        private Player player;
        private ActionWithRefOption action;

        @ConstructorProperties(value={"player", "action"})
        public ActionCallbackObject(Player player, ActionWithRefOption action) {
            this.player = player;
            this.action = action;
        }

        public Player getPlayer() {
            return this.player;
        }

        public ActionWithRefOption getAction() {
            return this.action;
        }
    }
}