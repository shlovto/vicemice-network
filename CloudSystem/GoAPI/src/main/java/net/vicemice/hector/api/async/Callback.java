package net.vicemice.hector.api.async;

public interface Callback<T> {
    public void done(T var1);
}