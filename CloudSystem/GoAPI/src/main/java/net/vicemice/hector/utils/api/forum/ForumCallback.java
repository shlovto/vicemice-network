package net.vicemice.hector.utils.api.forum;

/*
 * Class created at 10:56 - 26.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public interface ForumCallback<Object> {
    public void done(Object var1);
}