package net.vicemice.hector.api.protocol.injector.spigot;

import io.netty.channel.Channel;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;

import org.bukkit.entity.Player;

import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.concurrency.PacketTypeSet;
import net.vicemice.hector.api.protocol.events.NetworkMarker;
import net.vicemice.hector.api.protocol.events.PacketContainer;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.utility.MinecraftProtocolVersion;

/**
 * Dummy player handler that simply delegates to its parent Spigot packet injector.
 * 
 * @author Kristian
 */
class DummyPlayerHandler extends AbstractPlayerHandler {
	private SpigotPacketInjector injector;
	
	@Override
	public int getProtocolVersion(Player player) {
		// Just use the server version
		return MinecraftProtocolVersion.getCurrentVersion();
	}
	
	public DummyPlayerHandler(SpigotPacketInjector injector, PacketTypeSet sendingFilters) {
		super(sendingFilters);
		this.injector = injector;
	}

	@Override
	public boolean uninjectPlayer(InetSocketAddress address) {
		return true;
	}

	@Override
	public boolean uninjectPlayer(Player player) {
		injector.uninjectPlayer(player);
		return true;
	}
	
	@Override
	public void sendServerPacket(Player receiver, PacketContainer packet, NetworkMarker marker, boolean filters) throws InvocationTargetException {
		injector.sendServerPacket(receiver, packet, marker, filters);
	}

	@Override
	public void recieveClientPacket(Player player, Object mcPacket) throws IllegalAccessException, InvocationTargetException {
		injector.processPacket(player, mcPacket);
	}
	
	@Override
	public void injectPlayer(Player player, ConflictStrategy strategy) {
		// We don't care about strategy
		injector.injectPlayer(player);
	}
	
	@Override
	public boolean hasMainThreadListener(PacketType type) {
		return sendingFilters.contains(type);
	}
	
	@Override
	public void handleDisconnect(Player player) {
		// Just ignore
	}
	
	@Override
	public PacketEvent handlePacketRecieved(PacketContainer packet, InputStream input, byte[] buffered) {
		// Associate this buffered data
		if (buffered != null) {
			injector.saveBuffered(packet.getHandle(), buffered);
		}
		return null;
	}
	
	@Override
	public void updatePlayer(Player player) {
		// Do nothing
	}

	@Override
	public Channel getChannel(Player player) {
		throw new UnsupportedOperationException();
	}
}
