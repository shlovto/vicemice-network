package net.vicemice.hector.packets.types.player.stats;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:27 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerAddStatPacket implements Serializable {
    private String key;
    private UUID uniqueId;
    private long value;

    public PlayerAddStatPacket(UUID uniqueId, String key, long value) {
        this.uniqueId = uniqueId;
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return this.key;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public long getValue() {
        return this.value;
    }
}