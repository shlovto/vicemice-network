package net.vicemice.hector.utils.players;

import net.vicemice.hector.packets.types.player.replay.PacketEditReplay;
import net.vicemice.hector.packets.types.player.user.UserUpdatePacket;
import net.vicemice.hector.types.Callback;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.hologram.Hologram;
import net.vicemice.hector.utils.players.clan.Clan;
import net.vicemice.hector.utils.players.party.Party;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/*
 * Interface created at 00:14 - 02.05.2020
 * Copyright (C) elrobtossohn
 */
public interface IUser {

    /**
     * Get the name of the user
     * @return will give the name from the user
     */
    public String getName();

    /**
     * Get the unique id of the user
     * @return will give the unique id from the user
     */
    public UUID getUniqueId();

    /**
     * Get the name of the user
     * @return will give the name from the user
     */
    public Player getBukkitPlayer();

    /**
     * Send a translated message to the user
     * @param key must be an valid key
     */
    public void sendActionBarMessage(String key);

    /**
     * Send a translated message to the user
     * @param key must be an valid key
     * @param objects will replace {0-100} strings
     */
    public void sendActionBarMessage(String key, Object... objects);

    /**
     * Send a translated message to the user
     * @param key must be an valid key
     */
    public void sendMessage(String key);

    /**
     * Send a translated message to the user
     * @param key must be an valid key
     * @param objects will replace {0-100} strings
     */
    public void sendMessage(String key, Object... objects);

    /**
     * Send a raw message (no translation) to the user
     * @param message must be an valid string message
     */
    public void sendRawMessage(String message);

    /**
     * Get a translation of the locale which the user has set
     * @param key must be an valid key
     * @return will return the translated string
     */
    public String translate(String key);

    /**
     * Get a translation of the locale which the user has set
     * @param key must be an valid key
     * @param objects will replace {0-100} strings
     * @return will return the translated string
     */
    public String translate(String key, Object... objects);

    /**
     * Get a Locale from the user
     * @return returns the Language from the user
     */
    public Locale getLocale();

    /**
     * Get coins from the user
     * @return returns the coins
     */
    public long getCoins();

    /**
     * Get coins from the user
     * @param callback defines the callback
     */
    public void getCoins(Callback<Long> callback);

    /**
     * Add coins to the user
     * @param coins defines the number of coins
     */
    public void increaseCoins(long coins);

    /**
     * Remove coins from the user
     * @param coins defines the number of coins
     */
    public void decreaseCoins(long coins);
    
    /**
     * Get the Nickname of an player
     * @return returns the nickname or the name of the player
     */
    public String getNickName();

    /**
     * Check if an Player is nicked
     * @return returns true or false
     */
    public boolean isNicked();
    
    /**
     * Add Statics to an player
     * @param key set the value key
     * @param value set the value
     */
    public void increaseStatistic(String key, long value);

    /**
     * Remove Statics to an player
     * @param key set the value key
     * @param value set the value
     */
    public void decreaseStatistic(String key, long value);

    /**
     * Set Statics to an player
     * @param key set the value key
     * @param value set the value
     */
    public void setStatistic(String key, long value);

    /**
     * Get Statics of a user
     * @param key set the value key
     * @param period set the StatisticPeriod e.g. Global, Monthly or Daily Stats
     */
    public Long getStatistic(String key, StatisticPeriod period);

    /**
     * Get Statics of a user
     * @param key set the value key
     * @param period set the StatisticPeriod e.g. Global, Monthly or Daily Stats
     */
    public Long getRankPosition(String key, StatisticPeriod period);

    /**
     * Connect the user to a different server
     * @param server must be a valid string
     */
    public void connectServer(String server);

    /**
     * Get the Rank from a player
     * @return returns the rank from the user
     */
    public Rank getRank();

    /**
     * Get the XP of a user
     * @return returns the rank from the user
     */
    public long getXP();

    /**
     * Add xp to a player
     * @param xp defines the number of xp
     */
    public void addXP(long xp);

    /**
     * Remove xp from a player
     * @param xp defines the number of xp
     */
    public void removeXP(long xp);

    /**
     * Get Party Data from the user
     * @return returns the clan data from the user
     */
    public Party getParty();

    /**
     * Get Clan Data from a player
     * @return returns the clan data from the user
     */
    public Clan getClan();

    /**
     * Add a Replay Game to the Player, he can watch this later
     * @param gameId set the game id of the game
     * @param editType defines the Edit Type.
     */
    public void editReplayFavorites(String gameId, PacketEditReplay.EditType editType);

    /**
     * Send a Packet to Hector that the user want to watch a replay
     * @param gameId set the gameId which should be replayed
     */
    public void watchGame(String gameId);

    /**
     * Add a Replay Game to the user, he can watch this later
     * @param gameId set the game id of the game
     * @param replayUUID set the replay unique id
     * @param timestamp set the timestamp which the replay was started or ended.
     * @param gameLength set the length of the game
     * @param players set the players which played the game
     * @param gameType set the game type
     * @param mapType set the map type
     * @param map set the map name
     */
    public void addRecordedGame(String gameId, UUID replayUUID, int replayStartSeconds, long timestamp, long gameLength, ArrayList<String> players, String gameType, String mapType, String map, boolean save);

    /**
     * Update the user
     */
    public void update(UserUpdatePacket userUpdatePacket);

    /**
     * Is the user online?
     * @return online state
     */
    public boolean isOnline();

    /**
     * Set the online state of a user
     * @param online online state
     */
    public void setOnline(boolean online);

    /**
     * Get the Network Player from a user
     * @return the Network Player
     */
    public NetworkPlayer getNetworkPlayer();

    /**
     * Send settings to Hector
     */
    public void sendSettings();

    /**
     * Create a Hologram for the user
     * @param location must be an valid Location
     * @param text must be an valid List<String>
     * @return returns an valid Hologram
     */
    public Hologram createHologram(Location location, List<String> text);

    /**
     * Create a GUI for the user
     * @param title must be an valid string
     * @param rows must be an valid int
     * @return returns an valid GUI
     */
    public GUI createGUI(String title, int rows);

    /**
     * Teleport a user to a specific location
     * @param location must be an valid Location
     */
    public void teleport(Location location);

    /**
     * Teleport a user to a specific entity
     * @param entity must be an valid entity
     */
    public void teleport(Entity entity);

    /**
     * Play a sound on a user
     * @param location must be an valid location
     * @param sound must be an valid sound
     * @param volume must be an valid float
     * @param pitch must be an valid float
     */
    public void playSound(Location location, Sound sound, float volume, float pitch);

    /**
     * Play a sound on a user
     * @param location must be an valid location
     * @param sound must be an valid sound
     */
    public void playSound(Location location, Sound sound);

    /**
     * Play a sound on a user at the location from the user
     * @param sound must be an valid sound
     * @param volume must be an valid float
     * @param pitch must be an valid float
     */
    public void playSound(Sound sound, float volume, float pitch);

    /**
     * Play a sound on a user at the location from the user
     * @param sound must be an valid sound
     * @param volume must be an valid float
     */
    public void playSound(Sound sound, float volume);

    /**
     * Play a sound on a user at the location from the user
     * @param sound must be an valid sound
     */
    public void playSound(Sound sound);

    /**
     * Gives the Location from the user
     * @return a valid Location
     */
    public Location getLocation();

    /**
     * Gives the Eye Location from the user
     * @return a valid Location
     */
    public Location getEyeLocation();

    /**
     * Gives the UI Color from the user
     * @return a valid ItemStack (GLASS_PAIN not GLASS)
     */
    public ItemStack getUIColor();

    /**
     * Gives the UI Color from the user
     * @return a valid ItemStack (GLASS not GLASS_PAIN)
     */
    public ItemStack getUIColor2();

    /**
     * Perform a aaction
     * @param action must be an valid Action
     */
    public void performAction(Action action);

    public interface Action {
        /**
         * Will runned when the Action is behing performed
         */
        public void done();
    }
}