package net.vicemice.hector.utils.commands;

import java.util.*;
import java.util.stream.Collectors;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.language.Language;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.util.StringUtil;

/*
 * Class created at 00:58 - 14.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public abstract class AbstractCommand implements CommandExecutor, TabCompleter {

    private String command;
    private Rank permission;
    private Map<Language.LanguageType, String> description;
    private List<SubCommand> subCommands;
    private MessageFormat format;

    public AbstractCommand(String command, String enDescription, String deDescription, Rank permission) {
        this.command = command;
        this.permission = permission;
        this.description = new HashMap<>();
        this.description.put(Language.LanguageType.ENGLISH, enDescription);
        this.description.put(Language.LanguageType.GERMAN, deDescription);
        this.subCommands = new ArrayList<>();

        this.format = setupFormat();
        this.subCommands = Arrays.asList(setupCommands());
    }

    public String getCommand() {
        return command;
    }

    public List<SubCommand> getSubCommands() {
        return subCommands;
    }

    public Rank getPermission() {
        return permission;
    }

    protected abstract MessageFormat setupFormat();

    protected abstract SubCommand[] setupCommands();

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        String arg = args.length >= 1 ? args[0] : "overview";

        if (checkPermission(cs, this.permission, false)) {
            if (args.length == 0) {
                cs.sendMessage(cs instanceof Player ? this.description.get(GoAPI.getLanguageAPI().getLanguage(((Player)cs).getUniqueId())) : this.description.get(Language.LanguageType.ENGLISH));
                for (SubCommand sub : this.subCommands) {
                    if (!checkPermission(cs, sub.getPermission(), sub.isPermsOnly()) || !sub.isEnabled()) continue;

                    cs.sendMessage(this.format.getOverviewMessage(this.command, sub.getArgs(), (cs instanceof Player ? GoAPI.getLanguageAPI().getLanguage(((Player)cs).getUniqueId()) == Language.LanguageType.GERMAN ? sub.getGerDescription() : sub.getDescription() : sub.getDescription())));
                }
            } else {
                for (SubCommand sub : this.subCommands) {
                    if (sub.getLabel().equalsIgnoreCase(arg) || sub.getAliases().contains(arg)) {
                        if (sub.isPlayerOnly() && !(cs instanceof Player)) {
                            cs.sendMessage("§cYou must be a player to execute this command.");
                            return true;
                        }

                        if (!this.checkPermission(cs, sub.getPermission(), sub.isPermsOnly())) {
                            cs.sendMessage("§cI'm sorry, but you do not have permission to perform this sub-command. Please contact the server administrators if you believe that this is in error.");
                            return true;
                        }

                        if (!sub.execute(cs, cmd, label, args)) {
                            cs.sendMessage(this.format.getSyntaxMessage((cs instanceof PlayerQuitEvent ? GoAPI.getLanguageAPI().getLanguage(((Player) cs).getUniqueId()) : Language.LanguageType.ENGLISH), this.command, sub.getArgs()));
                        }

                        return true;
                    }
                }

                if (cs instanceof Player) {
                    cs.sendMessage(GoAPI.getLanguageAPI().getLanguage(((Player)cs).getUniqueId()) == Language.LanguageType.GERMAN ? this.format.getNotGerFoundMessage() : this.format.getNotFoundMessage());
                } else {
                    cs.sendMessage("§cCommand not found.");
                }
            }

        } else {
            cs.sendMessage("§cI'm sorry, but you do not have permission to perform this command. Please contact the server administrators if you believe that this is in error.");
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender cs, Command cmd, String label, String[] args) {
        String perm = args.length >= 1 ? args[0] : "overview";

        if (!checkPermission(cs, this.getPermission(), false)) return new ArrayList<>();


        if (args.length == 1) {
            return this.subCommands.stream()
                    .map(SubCommand::getLabel)
                    .filter(name -> StringUtil.startsWithIgnoreCase(name, args[0]))
                    .collect(Collectors.toList());
        } else {
            SubCommand sub = this.subCommands.stream()
                    .filter(sc -> sc.getLabel().equalsIgnoreCase(args[0]) || sc.getAliases().contains(args[0]))
                    .findAny()
                    .orElse(null);

            return sub != null ? sub.onTab(cs, cmd, label, args) : null;
        }

    }

    private boolean checkPermission(CommandSender cs, Rank rank, boolean only) {
        //DEBUG INFO IF HIGHER: System.out.println("Is '"+GoAPI.getUserAPI().getRankData(((Player)cs).getUniqueId()).getRankName()+"' higher as '"+rank.getRankName()+"'? -> " + Rank.valueOf(GoAPI.getUserAPI().getRankData(((Player)cs).getUniqueId()).getRankName().toUpperCase()).isHigherEqualsLevel(rank));
        if (only) {
            boolean o = false;
            if (Rank.valueOf(GoAPI.getUserAPI().getRankData(((Player)cs).getUniqueId()).getRankName().toUpperCase()).equalsLevel(rank)) {
                o = true;
            }
            if (Rank.valueOf(GoAPI.getUserAPI().getRankData(((Player)cs).getUniqueId()).getRankName().toUpperCase()).isAdmin()) {
                o = true;
            }
            return o;
        } else {
            return Rank.valueOf(GoAPI.getUserAPI().getRankData(((Player)cs).getUniqueId()).getRankName().toUpperCase()).isHigherEqualsLevel(rank);
        }
        //return this.permission == null || cs.hasPermission(this.permission + "." + arg);
    }
}