package net.vicemice.hector.packets.types.player.replay;

import lombok.Getter;
import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 07:05 - 18.04.2020
 * Copyright (C) elrobtossohn
 */
@Getter
public class SaveInReplayHistoryPacket implements Serializable {

    private UUID requester;

    @ConstructorProperties(value = {"uniqueId"})
    public SaveInReplayHistoryPacket(UUID requester) {
        this.requester = requester;
    }
}
