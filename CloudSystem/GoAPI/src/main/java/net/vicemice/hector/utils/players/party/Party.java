package net.vicemice.hector.utils.players.party;

import lombok.Getter;
import net.vicemice.hector.utils.PlayerInfo;

import java.io.Serializable;
import java.util.Map;
import java.util.UUID;

/*
 * Class created at 18:15 - 31.03.2020
 * Copyright (C) elrobtossohn
 */
public class Party implements Serializable {
    @Getter
    private final String id;
    @Getter
    private final Map<PlayerInfo, PartyRank> members;

    public Party(String id, Map<PlayerInfo, PartyRank> members) {
        this.id = id;
        this.members = members;
    }
}
