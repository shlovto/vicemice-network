package net.vicemice.hector.packets.types;

import lombok.Getter;

import java.io.Serializable;

/*
 * Class created at 18:32 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ErrorPacket implements Serializable {

    @Getter
    private String errorMessage;

    public ErrorPacket(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
