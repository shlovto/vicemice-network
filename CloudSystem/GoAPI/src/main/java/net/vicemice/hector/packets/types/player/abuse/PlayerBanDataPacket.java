package net.vicemice.hector.packets.types.player.abuse;

import net.vicemice.hector.packets.Abuses;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 08:59 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerBanDataPacket implements Serializable {
    private UUID user;
    private Abuses abuses;

    public PlayerBanDataPacket(UUID user, Abuses abuses) {
        this.user = user;
        this.abuses = abuses;
    }

    public UUID getUser() {
        return user;
    }

    public Abuses getAbuses() {
        return abuses;
    }
}