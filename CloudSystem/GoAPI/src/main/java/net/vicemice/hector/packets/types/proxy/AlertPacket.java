package net.vicemice.hector.packets.types.proxy;

import lombok.Getter;

import java.io.Serializable;

/*
 * Class created at 18:31 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class AlertPacket implements Serializable {

    @Getter
    private String message;

    public AlertPacket(String message) {
        this.message = message;
    }
}
