package net.vicemice.hector.utils.players.api.gui;

import com.google.common.collect.Maps;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_8_R3.NBTBase;
import net.vicemice.hector.utils.players.api.gui.nbt.BuildNBT_v1_8_R3;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import java.util.*;

public class ItemBuilder {
    private Material material = Material.AIR;
    private int metaId = -1;
    private int amount = -1;
    private String name;
    private ArrayList<String> lore = new ArrayList<>();
    private boolean glow;
    private ItemStack handle;
    private Map<Enchantment, Integer> enchantments = new HashMap<>();
    private Map<String, NBTBase> addnbt = Maps.newHashMap();
    private List<String> removenbt = new ArrayList<>();
    private List<PostItemBuilder> postListener = new ArrayList<>();

    public static ItemBuilder create() {
        return new ItemBuilder();
    }

    public static ItemBuilder create(Material material) {
        return new ItemBuilder(material);
    }

    public static ItemBuilder create(ItemStack handle) {
        return new ItemBuilder(handle);
    }

    public ItemBuilder() {
    }

    public ItemBuilder(Material material) {
        this.material = material;
    }

    public ItemBuilder(ItemStack handle) {
        this.handle = new ItemStack(handle);
    }

    public ItemBuilder amount(int n) {
        if (n <= 0) {
            this.amount = 1;
        } else {
            this.amount = n;
        }
        return this;
    }

    public ItemStack build() {
        if (Package.getPackage("net.minecraft.server.v1_8_R3") != null) {
            return new BuildNBT_v1_8_R3().build(this, handle, material, metaId, amount, name, lore, glow, enchantments, addnbt, removenbt, postListener);
        }

        return null;
    }

    public ItemStack build(Color color) {
        if (Package.getPackage("net.minecraft.server.v1_8_R3") != null) {
            return new BuildNBT_v1_8_R3().build(this, handle, color, material, metaId, amount, name, lore, glow, enchantments, addnbt, removenbt, postListener);
        }

        return null;
    }

    public ItemBuilder clearLore() {
        this.lore.clear();
        return this;
    }

    public ItemBuilder enchant(Enchantment enchantment, int level) {
        this.enchantments.put(enchantment, level);
        return this;
    }

    public ItemBuilder durability(int short_) {
        this.metaId = short_;
        return this;
    }

    public ItemBuilder glow(boolean flag) {
        this.glow = flag;
        return this;
    }

    public ItemBuilder lore(String lore) {
        this.lore.add(lore.replace("&", "§"));
        return this;
    }

    public /* varargs */ ItemBuilder lore(String ... lore) {
        List<String> l = new ArrayList<>();
        for (String s : lore) {
            l.add(s.replace("&", "§"));
        }
        this.lore.addAll(l);
        return this;
    }

    public ItemBuilder lore(Collection<String> lore) {
        List<String> l = new ArrayList<>();
        for (String s : lore) {
            l.add(s.replace("&", "§"));
        }
        this.lore.addAll(l);
        return this;
    }

    public ItemBuilder material(Material material) {
        this.material = material;
        return this;
    }

    public ItemBuilder name(String name) {
        this.name = name.replace("&", "§");
        return this;
    }

    public ItemBuilder addNBTTag(String key, NBTBase nbtBase) {
        this.addnbt.put(key, nbtBase);
        return this;
    }

    public ItemBuilder removeNBTTag(String key) {
        this.removenbt.add(key);
        return this;
    }

    public ItemBuilder noName() {
        this.name = ChatColor.BLACK.toString();
        return this;
    }

    public ItemBuilder postListener(PostItemBuilder listener) {
        this.postListener.add(listener);
        return this;
    }

    public ItemBuilder clone() {
        ItemBuilder _new = new ItemBuilder();
        _new.amount = this.amount;
        _new.glow = this.glow;
        _new.handle = this.handle == null ? null : new ItemStack(this.handle);
        _new.material = this.material;
        _new.lore = new ArrayList<>(this.lore);
        _new.name = this.name;
        _new.postListener = new ArrayList<>(this.postListener);
        _new.metaId = this.metaId;
        _new.enchantments = this.enchantments;
        _new.addnbt = this.addnbt;
        _new.removenbt = this.removenbt;
        return _new;
    }

    public Material getMaterial() {
        return material;
    }

    public int getMetaId() {
        return this.metaId;
    }

    public int getAmount() {
        return this.amount;
    }

    public String getName() {
        return this.name;
    }

    public ArrayList<String> getLore() {
        return this.lore;
    }

    public boolean isGlow() {
        return this.glow;
    }

    public ItemStack getHandle() {
        return this.handle;
    }

    public List<PostItemBuilder> getPostListener() {
        return this.postListener;
    }

    @FunctionalInterface
    public static interface PostItemBuilder {
        public ItemStack apply(ItemBuilder var1, ItemStack var2);
    }
}