package net.vicemice.hector.service.tryjump.event;

import net.vicemice.hector.service.tryjump.TryJumpPlayer;

/*
 * Class created at 01:46 - 10.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 * <p> Event gets fired when a player receives ranking points. This happens when completing a
 * unit, killing a player in deathmatch or winning the game.
 */
public class RankingPointsReceiveEvent extends TryJumpEvent {

    /**
     * Gets the amount ranking points a player will receive.
     */
    public int getAmount() {
        return amount;
    }

    private int amount;

    /**
     * @param player the {@link TryJumpPlayer} who receives the points.
     * @param amount the amount ranking points a player will receive.
     */
    public RankingPointsReceiveEvent(TryJumpPlayer player, int amount) {
        super(player);
        this.amount = amount;
    }
}
