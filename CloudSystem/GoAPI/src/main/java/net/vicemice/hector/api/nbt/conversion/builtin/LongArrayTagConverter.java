package net.vicemice.hector.api.nbt.conversion.builtin;

import net.vicemice.hector.api.nbt.conversion.TagConverter;
import net.vicemice.hector.api.nbt.tag.builtin.LongArrayTag;

/**
 * A converter that converts between LongArrayTag and long[].
 */
public class LongArrayTagConverter implements TagConverter<LongArrayTag, long[]> {
    @Override
    public long[] convert(LongArrayTag tag) {
        return tag.getValue();
    }

    @Override
    public LongArrayTag convert(String name, long[] value) {
        return new LongArrayTag(name, value);
    }
}
