package net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.providers;

import net.vicemice.hector.api.nbt.tag.builtin.CompoundTag;
import com.google.common.base.Optional;
import net.vicemice.hector.api.protocolversion.api.PacketWrapper;
import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.api.minecraft.Position;
import net.vicemice.hector.api.protocolversion.api.platform.providers.Provider;
import net.vicemice.hector.api.protocolversion.api.type.Type;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.Protocol1_9To1_8;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.storage.CommandBlockStorage;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.storage.EntityTracker;

public class CommandBlockProvider implements Provider {

    public void addOrUpdateBlock(UserConnection user, Position position, CompoundTag tag) throws Exception {
        checkPermission(user);
        if (isEnabled())
            getStorage(user).addOrUpdateBlock(position, tag);
    }

    public Optional<CompoundTag> get(UserConnection user, Position position) throws Exception {
        checkPermission(user);
        if (isEnabled())
            return getStorage(user).getCommandBlock(position);
        return Optional.absent();
    }

    public void unloadChunk(UserConnection user, int x, int z) throws Exception {
        checkPermission(user);
        if (isEnabled())
            getStorage(user).unloadChunk(x, z);
    }

    private CommandBlockStorage getStorage(UserConnection connection) {
        return connection.get(CommandBlockStorage.class);
    }

    public void sendPermission(UserConnection user) throws Exception {
        if (!isEnabled())
            return;
        PacketWrapper wrapper = new PacketWrapper(0x1B, null, user); // Entity status

        wrapper.write(Type.INT, user.get(EntityTracker.class).getProvidedEntityId()); // Entity ID
        wrapper.write(Type.BYTE, (byte) 26); // Hardcoded op permission level

        wrapper.send(Protocol1_9To1_8.class);

        user.get(CommandBlockStorage.class).setPermissions(true);
    }

    // Fix for Bungee since the join game is not sent after the first one
    private void checkPermission(UserConnection user) throws Exception {
        if (!isEnabled())
            return;
        CommandBlockStorage storage = getStorage(user);
        if (!storage.isPermissions()) {
            sendPermission(user);
        }
    }

    public boolean isEnabled() {
        return true;
    }

    public void unloadChunks(UserConnection userConnection) {
        if (isEnabled())
            getStorage(userConnection).unloadChunks();
    }
}
