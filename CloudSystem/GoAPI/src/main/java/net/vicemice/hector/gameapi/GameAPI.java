package net.vicemice.hector.gameapi;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.gameapi.util.Locations;
import java.util.Locale;

/*
 * Class created at 00:08 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GameAPI {

    @Getter
    @Setter
    private static String prefix;

    public static void initGameAPI() {
        Locations.initLocations();
    }

    public static void sendReplayMessage(String message) {
        /*
        if (GameManager.getReplay() == null) return;
        if (!GameManager.getReplay().isRecording()) return;
        GameManager.getRecorder().sendMessage(message);
        MCTV.getInstance().getApi().getWriter().addMeta(new ChatMeta());
         */
        //MCTV.getInstance().getApi().writeMessage(message);
    }

    public static void sendReplayMessage(String message, Locale locale) {
        /*if (GameManager.getReplay() == null) return;
        if (!GameManager.getReplay().isRecording()) return;
        GameManager.getRecorder().sendMessage(message, locale);*/
        //MCTV.getInstance().getApi().writeMessage(message, locale.toLanguageTag());
    }
}