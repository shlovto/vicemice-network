package net.vicemice.hector.api.util;

import java.util.List;

import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

/*
 * Class created at 21:05 - 30.04.2020
 * Copyright (C) elrobtossohn
 */
public class MinecraftUtil {
    public static String stringFromLocation(Location l) {
        String world = l.getWorld().getName();
        double x = l.getX();
        double y = l.getY();
        double z = l.getZ();
        float yaw = l.getYaw();
        float pitch = l.getPitch();
        return world + ", " + x + ", " + y + ", " + z + ", " + yaw + ", " + pitch;
    }

    public static Location locationfromString(String s) {
        float pitch;
        float yaw;
        double z;
        double y;
        double x;
        if (s.split(", ").length < 6) {
            return null;
        }
        String[] splitted = s.split(", ");
        World world = Bukkit.getWorld((String)splitted[0]);
        try {
            x = Double.parseDouble(splitted[1]);
            y = Double.parseDouble(splitted[2]);
            z = Double.parseDouble(splitted[3]);
            yaw = Float.parseFloat(splitted[4]);
            pitch = Float.parseFloat(splitted[5]);
        }
        catch (NumberFormatException e) {
            return null;
        }
        return new Location(world, x, y, z, yaw, pitch);
    }

    public static String stringFromItemStack(ItemStack item) {
        if (item == null) {
            return "AIR, 1, 0, 0, null";
        }
        String mat = item.getType().name();
        int amount = item.getAmount();
        short durability = item.getDurability();
        int color = item.getItemMeta() instanceof LeatherArmorMeta ? ((LeatherArmorMeta)item.getItemMeta()).getColor().asRGB() : 0;
        return mat + ", " + amount + ", " + durability + ", " + color;
    }

    public static String stringFromItemStackWithName(ItemStack item) {
        if (item == null || item.getType() == Material.AIR) {
            return "AIR, 1, 0, 0, null";
        }
        String mat = item.getType().name();
        int amount = item.getAmount();
        short durability = item.getDurability();
        int color = item.getItemMeta() instanceof LeatherArmorMeta ? ((LeatherArmorMeta)item.getItemMeta()).getColor().asRGB() : 0;
        String name = item.getItemMeta().getDisplayName();
        return mat + ", " + amount + ", " + durability + ", " + color + ", " + name;
    }

    public static ItemStack itemStackFromString(String s) {
        if (s.split(", ").length < 4) {
            return null;
        }
        String[] splitted = s.split(", ");
        Material mat = Material.valueOf((String)splitted[0]);
        int amount = 0;
        Color color = null;
        short durability = 0;
        try {
            amount = Integer.parseInt(splitted[1]);
            color = Color.fromRGB((int)Integer.parseInt(splitted[2]));
            durability = Short.parseShort(splitted[3]);
        }
        catch (NumberFormatException numberFormatException) {
            // empty catch block
        }
        ItemStack returnStack = new ItemStack(mat, amount, durability);
        if (returnStack.getItemMeta() instanceof LeatherArmorMeta) {
            LeatherArmorMeta meta = (LeatherArmorMeta)returnStack.getItemMeta();
            meta.setColor(color);
            returnStack.setItemMeta((ItemMeta)meta);
        }
        return returnStack;
    }

    public static ItemStack itemStackFromStringWithName(String s) {
        Color color;
        short durability;
        int amount;
        if (s.split(", ").length < 5) {
            return null;
        }
        String[] splitted = s.split(", ");
        Material mat = Material.valueOf((String)splitted[0]);
        String name = splitted[4];
        if (mat == Material.AIR) {
            return new ItemStack(mat);
        }
        try {
            amount = Integer.parseInt(splitted[1]);
            durability = Short.parseShort(splitted[2]);
            color = Color.fromRGB((int)Integer.parseInt(splitted[3]));
        }
        catch (NumberFormatException e) {
            return null;
        }
        ItemStack returnStack = new ItemStack(mat, amount, durability);
        ItemMeta meta = returnStack.getItemMeta();
        meta.setDisplayName(name);
        returnStack.setItemMeta(meta);
        if (returnStack.getItemMeta() instanceof LeatherArmorMeta) {
            LeatherArmorMeta armorMeta = (LeatherArmorMeta)returnStack.getItemMeta();
            armorMeta.setColor(color);
            returnStack.setItemMeta((ItemMeta)armorMeta);
        }
        return returnStack;
    }

    public static String convertColorCodes(String convert) {
        return ChatColor.translateAlternateColorCodes((char)'&', (String)convert);
    }

    public static boolean compareLocationWithoutHead(Location first, Location second) {
        return first.getX() != second.getX() || first.getY() != second.getY() || first.getZ() != second.getZ();
    }

    public static boolean compareItemStack(ItemStack first, ItemStack second) {
        return first != null && second != null && first.hasItemMeta() && second.hasItemMeta() && first.getType() == second.getType() && first.getAmount() == second.getAmount() && first.getItemMeta().getDisplayName().equals(second.getItemMeta().getDisplayName());
    }

    public static ItemStack changeColor(ItemStack itemStack, Color color) {
        try {
            LeatherArmorMeta meta = (LeatherArmorMeta)itemStack.getItemMeta();
            meta.setColor(color);
            itemStack.setItemMeta(meta);
            return itemStack;
        }
        catch (Exception e) {
            return null;
        }
    }

    public static boolean isNotInAir(IUser user) {
        return user.getLocation().clone().add(0.0, -1.0, 0.0).getBlock().getType() != Material.AIR || user.getLocation().clone().add(0.0, -0.5, 0.0).getBlock().getType() != Material.AIR;
    }

    public static ItemStack setItemNameAndLore(ItemStack original, String name, List<String> lore) {
        ItemStack item = original.clone();
        ItemMeta itemData = item.getItemMeta();
        itemData.setDisplayName(name);
        itemData.setLore(lore);
        item.setItemMeta(itemData);
        return item;
    }
}
