package net.vicemice.hector.utils.commands;

import java.util.ArrayList;
import java.util.List;

import net.vicemice.hector.utils.Rank;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/*
 * Class created at 00:57 - 14.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public abstract class SubCommand {

    private AbstractCommand parent;
    private String label, description, gerDescription, args;
    private Rank permission;
    private boolean playerOnly, permsOnly = false, enabled = true;
    private List<String> aliases;

    public SubCommand(AbstractCommand parent, String label, String description, String gerDescription, String args, Rank permission, boolean playerOnly) {
        this.parent = parent;
        this.label = label;
        this.description = description;
        this.gerDescription = gerDescription;
        this.args = args;
        this.permission = permission;
        this.playerOnly = playerOnly;
        this.aliases = new ArrayList<String>();
    }

    public SubCommand(AbstractCommand parent, String label, String description, String gerDescription, String args, Rank permission, boolean permsOnly, boolean playerOnly) {
        this.parent = parent;
        this.label = label;
        this.description = description;
        this.gerDescription = gerDescription;
        this.args = args;
        this.permission = permission;
        this.permsOnly = permsOnly;
        this.playerOnly = playerOnly;
        this.aliases = new ArrayList<String>();
    }

    public abstract boolean execute(CommandSender cs, Command cmd, String label, String[] args);

    public List<String> onTab(CommandSender cs, Command cmd, String label, String[] args) {
        return null;
    }

    public SubCommand addAlias(String alias) {
        if (!this.aliases.contains(alias)) this.aliases.add(alias);

        return this;
    }

    public String getDescription() {
        return description;
    }

    public String getGerDescription() {
        return gerDescription;
    }

    public String getLabel() {
        return label;
    }

    public String getArgs() {
        return args;
    }

    public Rank getPermission() {
        return permission;
    }

    public boolean isPermsOnly() {
        return permsOnly;
    }

    public void setPermsOnly(boolean permsOnly) {
        this.permsOnly = permsOnly;
    }

    public boolean isPlayerOnly() {
        return playerOnly;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<String> getAliases() {
        return aliases;
    }

    public AbstractCommand getParent() {
        return parent;
    }
}