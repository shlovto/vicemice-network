package net.vicemice.hector.utils.players.api.internal.fakemobs.listener;

import net.vicemice.hector.utils.players.api.internal.fakemobs.event.PlayerInteractFakeMobEvent;
import net.vicemice.hector.utils.players.api.internal.fakemobs.event.PlayerInteractFakeMobEvent.Action;
import net.vicemice.hector.utils.players.api.internal.fakemobs.interact.InteractAction;
import net.vicemice.hector.utils.players.api.internal.fakemobs.util.FakeMob;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class InteractListener implements Listener {
	
	@EventHandler
	public void onInteractFakeMobEvent(PlayerInteractFakeMobEvent event) {
		if (event.getAction() != Action.RIGHT_CLICK) return;
		Player player = event.getPlayer();
		FakeMob mob = event.getMob();
		
		for (InteractAction action : mob.getInteractActions())
			action.onInteract(player, mob);
	}
	
}
