package net.vicemice.hector.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ServerUpdateLocaleEvent extends Event {
    private static HandlerList handlers = new HandlerList();

    public ServerUpdateLocaleEvent() {}

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public HandlerList getHandlers() {
        return handlers;
    }
}