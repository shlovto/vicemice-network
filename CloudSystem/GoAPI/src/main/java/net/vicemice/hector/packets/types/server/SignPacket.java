package net.vicemice.hector.packets.types.server;

import java.io.Serializable;

/*
 * Class created at 18:37 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SignPacket implements Serializable {
    private String[] lines;
    private int x;
    private int y;
    private int z;
    private String world;
    private String serverName;
    private boolean add;

    public SignPacket(String[] lines, int x, int y, int z, String world, String serverName, boolean add) {
        this.lines = lines;
        this.x = x;
        this.y = y;
        this.z = z;
        this.world = world;
        this.serverName = serverName;
        this.add = add;
    }

    public String[] getLines() {
        return this.lines;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public int getZ() {
        return this.z;
    }

    public String getWorld() {
        return this.world;
    }

    public String getServerName() {
        return this.serverName;
    }

    public boolean isAdd() {
        return this.add;
    }
}