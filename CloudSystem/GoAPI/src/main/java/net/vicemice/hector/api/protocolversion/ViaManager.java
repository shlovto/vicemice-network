package net.vicemice.hector.api.protocolversion;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.api.platform.ViaInjector;
import net.vicemice.hector.api.protocolversion.api.platform.ViaPlatform;
import net.vicemice.hector.api.protocolversion.api.platform.ViaPlatformLoader;
import net.vicemice.hector.api.protocolversion.api.platform.providers.ViaProviders;
import net.vicemice.hector.api.protocolversion.api.protocol.ProtocolRegistry;
import net.vicemice.hector.api.protocolversion.api.protocol.ProtocolVersion;
import net.vicemice.hector.api.protocolversion.commands.ViaCommandHandler;
import net.vicemice.hector.api.protocolversion.protocols.base.ProtocolInfo;
import net.vicemice.hector.api.protocolversion.update.UpdateUtil;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Getter
public class ViaManager {
    private final Map<UUID, UserConnection> portedPlayers = new ConcurrentHashMap<>();
    private ViaPlatform platform;
    private ViaProviders providers = new ViaProviders();
    @Setter
    private boolean debug = false;
    // Internals
    private ViaInjector injector;
    private ViaCommandHandler commandHandler;
    private ViaPlatformLoader loader;

    @Builder
    public ViaManager(ViaPlatform platform, ViaInjector injector, ViaCommandHandler commandHandler, ViaPlatformLoader loader) {
        this.platform = platform;
        this.injector = injector;
        this.commandHandler = commandHandler;
        this.loader = loader;
    }

    public void init() {
        if (System.getProperty("ViaVersion") != null) {
            // Reload?
            platform.onReload();
        }
        // Check for updates
        if (platform.getConf().isCheckForUpdates())
            UpdateUtil.sendUpdateMessage();
        // Force class load
        ProtocolRegistry.getSupportedVersions();
        // Inject
        try {
            injector.inject();
        } catch (Exception e) {
            getPlatform().getLogger().severe("ViaVersion failed to inject:");
            e.printStackTrace();
            return;
        }
        // Mark as injected
        System.setProperty("ViaVersion", getPlatform().getPluginVersion());
        // If successful
        platform.runSync(new Runnable() {
            @Override
            public void run() {
                onServerLoaded();
            }
        });

    }

    public void onServerLoaded() {
        // Load Server Protocol
        try {
            ProtocolRegistry.SERVER_PROTOCOL = injector.getServerProtocolVersion();
        } catch (Exception e) {
            getPlatform().getLogger().severe("ViaVersion failed to get the server protocol!");
            e.printStackTrace();
        }
        // Check if there are any pipes to this version
        if (ProtocolRegistry.SERVER_PROTOCOL != -1) {
            getPlatform().getLogger().info("ViaVersion detected server version: " + ProtocolVersion.getProtocol(ProtocolRegistry.SERVER_PROTOCOL));
            if (!ProtocolRegistry.isWorkingPipe()) {
                getPlatform().getLogger().warning("ViaVersion does not have any compatible versions for this server version, please read our resource page carefully.");
            }
        }
        // Load Listeners / Tasks
        ProtocolRegistry.onServerLoaded();

        // Load Platform
        loader.load();

        // Refresh Versions
        ProtocolRegistry.refreshVersions();
    }

    public void destroy() {
        // Uninject
        getPlatform().getLogger().info("ViaVersion is disabling, if this is a reload and you experience issues consider rebooting.");
        try {
            injector.uninject();
        } catch (Exception e) {
            getPlatform().getLogger().severe("ViaVersion failed to uninject:");
            e.printStackTrace();
        }

        // Unload
        loader.unload();
    }

    public void addPortedClient(UserConnection info) {
        portedPlayers.put(info.get(ProtocolInfo.class).getUuid(), info);
    }

    public void removePortedClient(UUID clientID) {
        portedPlayers.remove(clientID);
    }

    public UserConnection getConnection(UUID playerUUID) {
        return portedPlayers.get(playerUUID);
    }

}
