package net.vicemice.hector.utils.players;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 20:06 - 16.04.2020
 * Copyright (C) elrobtossohn
 */
@Getter
public class ReportPlayer implements Serializable {

    private UUID uniqueId;
    private String name, server, value, signature;

    public ReportPlayer(UUID uniqueId, String name, String server, String value, String signature) {
        this.uniqueId = uniqueId;
        this.name = name;
        this.server = server;
        this.value = value;
        this.signature = signature;
    }
}
