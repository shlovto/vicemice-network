package net.vicemice.hector.api.util;

public class JavaUtil {
    public static int addTo(int to, int s) {
        int mod = s % to;
        return mod == 0 ? s : s + to - mod;
    }

    public static String cutString(String str, int length) {
        return str.length() > length ? str.substring(0, length) : str;
    }
}