package net.vicemice.hector.packets.types.player.database;

import java.io.Serializable;

/*
 * Class created at 09:05 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerDatabasePacket implements Serializable {
    private String callBackID;
    private String value;

    public PlayerDatabasePacket(String callBackID, String value) {
        this.callBackID = callBackID;
        this.value = value;
    }

    public String getCallBackID() {
        return this.callBackID;
    }

    public String getValue() {
        return this.value;
    }
}
