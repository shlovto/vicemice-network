package net.vicemice.hector.packets.types.player;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/*
 * Class created at 18:45 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PermissionsPacket implements Serializable {
    private HashMap<String, List<String>> permissions = new HashMap<>();

    public PermissionsPacket(HashMap<String, List<String>> permissions) {
        this.permissions = permissions;
    }

    public HashMap<String, List<String>> getPermissions() {
        return this.permissions;
    }
}