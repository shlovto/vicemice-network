package net.vicemice.hector.utils.players.api.particles;

import com.captainbern.minecraft.protocol.PacketType;
import com.captainbern.minecraft.wrapper.WrappedPacket;
import net.vicemice.hector.utils.players.api.particles.reflection.Reflection;
import net.vicemice.hector.utils.players.api.particles.util.ServerUtil;
import net.vicemice.hector.utils.players.api.particles.util.Version;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

/*
 * Class created at 10:31 - 02.04.2020
 * Copyright (C) elrobtossohn
 */
public class EffectBuilder implements Cloneable {
    private Particle type;
    private Location position;
    private Vector offset;
    private float speed;
    private int amount;
    private boolean force;
    private int[] data = new int[0];

    @Deprecated
    public EffectBuilder(String name, float speed, int amount) {
        this(Particle.fromName(name), speed, amount);
    }

    public EffectBuilder(Particle type, float speed, int amount) {
        this.ofType(type).offset(0.0f, 0.0f, 0.0f).atSpeed(speed).ofAmount(amount);
    }

    public static EffectBuilder build(Particle particle) {
        return new EffectBuilder(particle.getName(), particle.getSpeed(), particle.getAmount());
    }

    public void show(Player player) {
        new Thread(() -> {}).start();
        WrappedPacket packet = new WrappedPacket(PacketType.Play.Server.WORLD_PARTICLES);
        packet.getAccessor().write(0, this.getNMSParticleType());
        packet.getFloats().write(0, (float) this.position.getX());
        packet.getFloats().write(1, (float) this.position.getY());
        packet.getFloats().write(2, (float) this.position.getZ());
        packet.getFloats().write(3, (float) this.offset.getX());
        packet.getFloats().write(4, (float) this.offset.getY());
        packet.getFloats().write(5, (float) this.offset.getZ());
        packet.getFloats().write(6, this.speed);
        packet.getIntegers().write(0, this.amount);
        if (new Version().isCompatible("1.8")) {
            packet.getIntegerArrays().write(0, this.data);
            packet.getBooleans().write(0, this.force);
        }
        ServerUtil.sendPacket(packet.getHandle(), player);
    }

    private Object getNMSParticleType() {
        if (true) {
            Class var6 = Reflection.getNMSClass("EnumParticle");
            return var6.getEnumConstants()[this.type.getId()];
        }

        if (new Version().isCompatible("1.8")) {
            Class var6 = Reflection.getNMSClass("EnumParticle");
            return var6.getEnumConstants()[this.type.getId()];
        }
        String name = this.type.getName();
        for (int i : this.data) {
            name = name + "_" + i;
        }
        return name;
    }

    public void show() {
        for (Player player : GeometryUtil.getNearbyPlayers(this.position, 50)) {
            this.show(player);
        }
    }

    public EffectBuilder ofType(Particle type) {
        this.type = type;
        return this;
    }

    public EffectBuilder at(Location position) {
        this.position = position.clone();
        return this;
    }

    public EffectBuilder at(World world, float x, float y, float z) {
        this.position = new Location(world, x, y, z);
        return this;
    }

    public EffectBuilder offset(Vector offset) {
        this.offset = offset.clone();
        return this;
    }

    public EffectBuilder offset(float x, float y, float z) {
        this.offset(new Vector(x, y, z));
        return this;
    }

    public EffectBuilder atSpeed(float speed) {
        this.speed = speed;
        return this;
    }

    public EffectBuilder ofAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public EffectBuilder withData(int... data) {
        this.data = data != null ? data : new int[]{};
        return this;
    }

    public EffectBuilder ofBlockType(Material material) {
        return this.ofBlockType(material, 0);
    }

    public EffectBuilder ofBlockType(Material material, int metadata) {
        this.data = new int[]{material.getId(), metadata};
        return this;
    }

    public Particle getType() {
        return this.type;
    }

    public Location getPosition() {
        return this.position;
    }

    public Vector getOffset() {
        return this.offset;
    }

    public float getSpeed() {
        return this.speed;
    }

    public int getAmount() {
        return this.amount;
    }

    public EffectBuilder clone() throws CloneNotSupportedException {
        return (EffectBuilder) super.clone();
    }
}