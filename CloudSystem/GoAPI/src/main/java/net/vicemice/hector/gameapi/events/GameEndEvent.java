package net.vicemice.hector.gameapi.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameEndEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private long endTime;
    private boolean replayStopped;

    public GameEndEvent(long endTime, boolean replayStopped) {
        this.endTime = endTime;
        this.replayStopped = replayStopped;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public long getEndTime() {
        return endTime;
    }

    public boolean isReplayStopped() {
        return replayStopped;
    }

    public HandlerList getHandlers() {
        return handlers;
    }
}