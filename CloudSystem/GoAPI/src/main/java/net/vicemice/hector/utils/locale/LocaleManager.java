package net.vicemice.hector.utils.locale;

import lombok.Data;
import org.json.JSONObject;
import java.io.File;
import java.text.MessageFormat;
import java.util.*;

@Data
public class LocaleManager {

    private HashMap<Locale, JSONObject> locales;
    private ArrayList<String> paths;

    /**
     * Create a new Instance for the LocaleManager
     * @param path defines the Path where the locales are
     * @return returns the new instance
     */
    public static LocaleManager newManager(String path) {
        LocaleManager localeManager = new LocaleManager();
        localeManager.setLocales(new HashMap<>());
        localeManager.setPaths(new ArrayList<>());
        localeManager.getPaths().add(path);
        localeManager.reloadLocales();
        return localeManager;
    }

    /**
     * Create a new Instance for the LocaleManager
     * @param paths defines the Paths where the locales are
     * @return returns the new instance
     */
    public static LocaleManager newManager(ArrayList<String> paths) {
        LocaleManager localeManager = new LocaleManager();
        localeManager.setLocales(new HashMap<>());
        localeManager.setPaths(paths);
        localeManager.reloadLocales();
        return localeManager;
    }

    /**
     * Add a multi Paths to the locale
     * @param paths defines the Paths where locales are
     */
    public void addPaths(List<String> paths) {
        if (this.paths == null) this.paths = new ArrayList<>();
        this.paths.addAll(paths);
        this.reloadLocales();
    }

    /**
     * Add a Path to the locale
     * @param path defines the Path where locales are
     */
    public void addPath(String path) {
        if (this.paths == null) this.paths = new ArrayList<>();
        this.paths.add(path);
        this.reloadLocales();
    }

    /**
     * Reload all Locales for new Messages etc
     * @throws NullPointerException throws if the path is null
     */
    public void reloadLocales() throws NullPointerException {
        locales.clear();

        for (Locale locale : Locale.getAvailableLocales()) {
            JSONObject jsonObject = new JSONObject();

            for (String path : this.getPaths()) {
                File file = new File(path+"/"+locale.toLanguageTag()+".json");
                if (!file.exists() && !file.canRead()) continue;
                if (file.exists()) {
                    LocaleConfiguration localeConfiguration = new LocaleConfiguration(path+"/"+locale.toLanguageTag()+".json");
                    localeConfiguration.readConfiguration();
                    for (String string : localeConfiguration.getJsonObject().keySet()) {
                        jsonObject.put(string, localeConfiguration.getJsonObject().get(string));
                    }
                }
            }

            if (!jsonObject.keySet().isEmpty()) {
                this.getLocales().put(locale, jsonObject);
            }
        }
    }
    
    public String getPrivateMessage(Locale locale, String string) {
        if (!this.getLocales().containsKey(locale)) return null;
        return this.getLocales().get(locale).getString(string).replace("&", "§");
    }

    /**
     * Get a Message from a Locale
     * @param locale defines the locale from which the message should be get
     * @param string defines the language-key
     * @return returns the message or null
     */
    public String getMessage(Locale locale, String string) {
        if (!this.getLocales().containsKey(locale)) return null;
        return this.getLocales().get(locale).getString(string).replace("&", "§")
                .replace("%prefix%", this.getPrivateMessage(locale, "prefix"))
                .replace("%shop_prefix%", this.getPrivateMessage(locale, "shop-prefix"))
                .replace("%forum_prefix%", this.getPrivateMessage(locale, "forum-prefix"))
                .replace("%cloud_prefix%", this.getPrivateMessage(locale, "cloud-prefix"))
                .replace("%team_prefix%", this.getPrivateMessage(locale, "team-prefix"))
                .replace("%friend_prefix%", this.getPrivateMessage(locale, "friend-prefix"))
                .replace("%party_prefix%", this.getPrivateMessage(locale, "party-prefix"))
                .replace("%clan_prefix%", this.getPrivateMessage(locale, "clan-prefix"))
                .replace("%verify_prefix%", this.getPrivateMessage(locale, "verify-prefix"))
                .replace("%nick_prefix%", this.getPrivateMessage(locale, "nick-prefix"))
                .replace("%announcement_prefix%", this.getPrivateMessage(locale, "announcement-prefix"))
                .replace("%report_prefix%", this.getPrivateMessage(locale, "report-prefix"))
                .replace("%replay_prefix%", this.getPrivateMessage(locale, "replay-prefix"))
                .replace("%qsg_prefix%", this.getPrivateMessage(locale, "qsg-prefix"))
                .replace("%bw_prefix%", this.getPrivateMessage(locale, "bw-prefix"))
                .replace("%elo_prefix%", this.getPrivateMessage(locale, "elo-prefix"));
    }

    /**
     * Get a Message from a Locale
     * @param locale defines the locale from which the message should be get
     * @param string defines the language-key
     * @return returns the message or null
     */
    public String getMessage(Locale locale, String string, Object... objects) {
        if (!this.getLocales().containsKey(locale)) return null;
        return MessageFormat.format(this.getLocales().get(locale).getString(string).replace("&", "§")
                .replace("%prefix%", this.getPrivateMessage(locale, "prefix"))
                .replace("%shop_prefix%", this.getPrivateMessage(locale, "shop-prefix"))
                .replace("%forum_prefix%", this.getPrivateMessage(locale, "forum-prefix"))
                .replace("%cloud_prefix%", this.getPrivateMessage(locale, "cloud-prefix"))
                .replace("%team_prefix%", this.getPrivateMessage(locale, "team-prefix"))
                .replace("%friend_prefix%", this.getPrivateMessage(locale, "friend-prefix"))
                .replace("%party_prefix%", this.getPrivateMessage(locale, "party-prefix"))
                .replace("%clan_prefix%", this.getPrivateMessage(locale, "clan-prefix"))
                .replace("%verify_prefix%", this.getPrivateMessage(locale, "verify-prefix"))
                .replace("%nick_prefix%", this.getPrivateMessage(locale, "nick-prefix"))
                .replace("%announcement_prefix%", this.getPrivateMessage(locale, "announcement-prefix"))
                .replace("%report_prefix%", this.getPrivateMessage(locale, "report-prefix"))
                .replace("%replay_prefix%", this.getPrivateMessage(locale, "replay-prefix"))
                .replace("%qsg_prefix%", this.getPrivateMessage(locale, "qsg-prefix"))
                .replace("%bw_prefix%", this.getPrivateMessage(locale, "bw-prefix"))
                .replace("%elo_prefix%", this.getPrivateMessage(locale, "elo-prefix")), objects);
    }
}