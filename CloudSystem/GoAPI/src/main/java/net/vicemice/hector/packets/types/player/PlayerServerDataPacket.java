package net.vicemice.hector.packets.types.player;

import net.vicemice.hector.utils.players.NetworkPlayer;
import net.vicemice.hector.utils.players.utils.DataUpdateType;

import java.io.Serializable;

/*
 * Class created at 00:37 - 04.02.2020
 * Copyright (C) 2016-2020 Mineconia Solutions
 */
public class PlayerServerDataPacket implements Serializable {
    private NetworkPlayer networkPlayer;
    private DataUpdateType dataUpdateType;
    private long time;

    public PlayerServerDataPacket(NetworkPlayer networkPlayer, DataUpdateType dataUpdateType, long time) {
        this.networkPlayer = networkPlayer;
        this.dataUpdateType = dataUpdateType;
        this.time = time;
    }

    public NetworkPlayer getNetworkPlayer() {
        return networkPlayer;
    }

    public DataUpdateType getDataUpdateType() {
        return dataUpdateType;
    }

    public long getTime() {
        return time;
    }
}
