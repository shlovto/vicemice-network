package net.vicemice.hector.packets.types.player;

import lombok.Getter;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:17 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class LogoutPacket implements Serializable {

    @Getter
    private UUID uniqueId;

    public LogoutPacket(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }
}