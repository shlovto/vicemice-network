package net.vicemice.hector.packets.types.player.user;

import lombok.Getter;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.players.clan.Clan;
import net.vicemice.hector.utils.players.party.Party;

import java.io.Serializable;
import java.util.Locale;
import java.util.UUID;

/*
 * Class created at 21:11 - 02.05.2020
 * Copyright (C) elrobtossohn
 */
@Getter
public class UserUpdatePacket implements Serializable {
    private final String name;
    private final UUID uniqueId;
    private final Rank rank;
    private final Locale locale;
    private final long coins;
    private final long xp;
    private final Party party;
    private final Clan clan;

    public UserUpdatePacket(String name, UUID uniqueId, Rank rank, Locale locale, long coins, long xp, Party party, Clan clan) {
        this.name = name;
        this.uniqueId = uniqueId;
        this.rank = rank;
        this.locale = locale;
        this.coins = coins;
        this.xp = xp;
        this.party = party;
        this.clan = clan;
    }
}