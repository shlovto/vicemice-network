package net.vicemice.hector.types;

/*
 * Class created at 06:00 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public interface DatabaseCallback<String> {
    public void done(String var1);
}