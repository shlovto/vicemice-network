package net.vicemice.hector.packets;

import net.vicemice.hector.packets.types.PacketHolder;

/*
 * Class created at 12:56 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PacketHelper {

    /**
     *
     * @param packetType set the type key for the preset object
     * @param object set the object which should be prepared
     * @return returns the packet holder
     */
    public PacketHolder preparePacket(PacketType packetType, Object object) {
        return new PacketHolder(packetType.getKey(), object);
    }
}