package net.vicemice.hector.api.protocolversion.api.type.types;

import io.netty.buffer.ByteBuf;
import net.vicemice.hector.api.protocolversion.api.type.Type;
import net.vicemice.hector.api.protocolversion.api.type.TypeConverter;

public class VoidType extends Type<Void> implements TypeConverter<Void> {
    public VoidType() {
        super(Void.class);
    }

    @Override
    public Void read(ByteBuf buffer) {
        return null;
    }

    @Override
    public void write(ByteBuf buffer, Void object) {

    }

    @Override
    public Void from(Object o) {
        return null;
    }
}
