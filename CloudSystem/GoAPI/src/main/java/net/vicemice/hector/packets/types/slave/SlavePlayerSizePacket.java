package net.vicemice.hector.packets.types.slave;

import java.io.Serializable;

/*
 * Class created at 18:40 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SlavePlayerSizePacket implements Serializable {
    private String serverName;
    private int onlinePlayers;

    public SlavePlayerSizePacket(String serverName, int onlinePlayers) {
        this.serverName = serverName;
        this.onlinePlayers = onlinePlayers;
    }

    public String getServerName() {
        return this.serverName;
    }

    public int getOnlinePlayers() {
        return this.onlinePlayers;
    }
}
