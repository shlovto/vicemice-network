package net.vicemice.hector.packets.types.player.nick;

import java.io.Serializable;

/*
 * Class created at 18:25 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerUnnickPacket implements Serializable {
    private String username;

    public PlayerUnnickPacket(String username) {
        this.username = username;
    }

    public String getUsername() {
        return this.username;
    }
}
