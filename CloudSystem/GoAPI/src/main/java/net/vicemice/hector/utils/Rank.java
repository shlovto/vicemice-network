package net.vicemice.hector.utils;

import java.io.Serializable;

/*
 * Class created at 18:33 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public enum Rank implements Serializable {
    MEMBER(10, 2, new int[] { 19 }, new String[] { "706513901852033025" }, "Member", "Member", "§9", "k", false, "Member"),
    PREMIUM(30, 11, new int[] { 19,18 }, new String[] { "706513900438552586" }, "Premium", "Premium", "§6", "i", false, "Premium"),
    VIP(40, 10, new int[] { 19,17 }, new String[] { "706513899557748827" }, "VIP", "VIP", "§5", "h", false, "VIP"),
    CONTENT(70, 5, new int[] { 19,32 }, new String[] { "706513897850535947" },"Content", "Content", "§c", "g", true, "Dev"),
    MODERATOR(80, 7, new int[] { 19,13 }, new String[] { "706513898915758111" },"Moderator", "Moderator", "§c", "e", true, "Mod"),
    ADMIN(100, 3, new int[] { 19,10 }, new String[] { "706513895308656680" }, "Admin", "Administrator", "§4", "a", true, "Admin");

    private int accessLevel;
    private final int forumId;
    private final int[] teamSpeakGroups;
    private final String[] discordGroups;
    private final String rankName, fullName, rankColor, scoreboardName, shortName;
    private final boolean showFullName;

    Rank(int accessLevel, int forumId, int[] teamSpeakGroups, String[] discordGroups, String rankName, String fullName, String rankColor, String scoreboardName, boolean showFullName, String shortName) {
        this.accessLevel = accessLevel;
        this.forumId = forumId;
        this.rankName = rankName;
        this.fullName = fullName;
        this.rankColor = rankColor;
        this.teamSpeakGroups = teamSpeakGroups;
        this.discordGroups = discordGroups;
        this.scoreboardName = scoreboardName;
        this.showFullName = showFullName;
        this.shortName = shortName;
    }

    /*
    Rank(int accessLevel, int tsgroupid, String rankName, String rankColor, String scoreboardName, boolean showFullName, String shortName) {
        this.accessLevel = accessLevel;
        this.rankName = rankName;
        this.rankColor = rankColor;
        this.tsgroupid = tsgroupid;
        this.scoreboardName = scoreboardName;
        this.showFullName = showFullName;
        this.shortName = shortName;
    }
     */

    public static Rank getRankByTeamSpeakID(int tsGroupID) {
        for (Rank rank : Rank.values()) {
            for (int group : rank.getTeamSpeakGroups()) {
                if (group != tsGroupID) continue;
                return rank;
            }
        }
        return null;
    }

    public static Rank fromString(String getRank) {
        for (Rank rank : Rank.values()) {
            if (!rank.name().equalsIgnoreCase(getRank)) continue;
            return rank;
        }
        return MEMBER;
    }

    public static String getRanks() {
        StringBuilder stringBuilder = new StringBuilder();
        int current = 1;
        for (Rank rank : Rank.values()) {
            if (current == Rank.values().length) {
                stringBuilder.append(rank.getRankColor() + rank.getRankName());
            } else {
                stringBuilder.append(rank.getRankColor() + rank.getRankName() + ", ");
            }
            ++current;
        }
        return stringBuilder.toString();
    }

    public static Rank fromStringExact(String rank) {
        return Rank.fromString(rank);
    }

    public int getAccessLevel() {
        return this.accessLevel;
    }

    /*
    public int getTSGroupID() {
        return this.tsgroupid;
    }
     */

    public boolean containsTeamSpeakRank(int id) {
        for (int s : this.getTeamSpeakGroups()) {
            if (!(s == id)) continue;
            return true;
        }

        return false;
    }

    public boolean containsDiscordRank(String id) {
        for (String s : this.getDiscordGroups()) {
            if (!s.equalsIgnoreCase(id)) continue;
            return true;
        }

        return false;
    }

    public int getForumId() {
        return forumId;
    }

    public int[] getTeamSpeakGroups() {
        return teamSpeakGroups;
    }

    public String[] getDiscordGroups() {
        return discordGroups;
    }

    public String getRankName() {
        return this.rankName;
    }

    public String getFullName() {
        return this.fullName;
    }

    public String getRankColor() {
        return this.rankColor;
    }

    public String getScoreboardName() {
        return this.scoreboardName;
    }

    public boolean isTeam() {
        return this.getAccessLevel() >= 55;
    }

    public void setAccesLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    public boolean isHigherLevel(Rank r) {
        return this.getAccessLevel() > r.getAccessLevel();
    }

    public boolean isHigherEqualsLevel(Rank r) {
        return this.getAccessLevel() >= r.getAccessLevel();
    }

    public boolean equalsLevel(Rank r) {
        return this.getAccessLevel() == r.getAccessLevel();
    }

    public boolean isLowerEqualsLevel(Rank r) {
        return this.getAccessLevel() <= r.getAccessLevel();
    }

    public boolean isLowerLevel(Rank r) {
        return this.getAccessLevel() < r.getAccessLevel();
    }

    public boolean isRankTabList() {
        return this.showFullName;
    }

    public String getShortName() {
        return this.shortName;
    }

    public boolean isJrModeration() {
        if(getAccessLevel() == 80 || getAccessLevel() >= 100) {
            return true;
        }

        return false;
    }

    public boolean isModeration() {
        if(getAccessLevel() == 80 || getAccessLevel() >= 100) {
            return true;
        }

        return false;
    }

    public static boolean isAdmin(Integer accessLevel) {
        if (accessLevel >= 100) {
            return true;
        }

        return false;
    }

    public boolean isBuilder() {
        if(getAccessLevel() == 60 || getAccessLevel() >= 100) {
            return true;
        }

        return false;
    }

    public boolean isAdmin() {
        if (getAccessLevel() >= 100) {
            return true;
        }

        return false;
    }
}