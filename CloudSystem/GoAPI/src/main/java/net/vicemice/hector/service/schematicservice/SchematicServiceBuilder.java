package net.vicemice.hector.service.schematicservice;

import net.vicemice.hector.service.schematicservice.metadata.MetadataDeserializer;
import net.vicemice.hector.service.schematicservice.metadata.MetadataSerializer;

/*
 * Class created at 01:23 - 10.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 * <p>Builder used for creating the {@link SchematicService}.
 */
public class SchematicServiceBuilder<T> {

    private SchematicService<T> service = new SchematicService<>();

    /**
     * Sets the {@link MetadataSerializer} used for serializing metadata.
     *
     * @param serializer Class implementing {@link MetadataSerializer}.
     * @return another {@code SchematicServiceBuilder}
     */
    public SchematicServiceBuilder setSerializer(MetadataSerializer<T> serializer) {
        service.setSerializer(serializer);
        return this;
    }

    /**
     * Sets the {@link MetadataDeserializer} used for deserializing metadata.
     *
     * @param deserializer Class implementing {@link MetadataDeserializer}.
     * @return another {@code SchematicServiceBuilder}
     */
    public SchematicServiceBuilder setDeserializer(MetadataDeserializer<T> deserializer) {
        service.setDeserializer(deserializer);
        return this;
    }

    /** Creates an instance of {@link SchematicService}. */
    public SchematicService<T> build() {
        return service;
    }
}
