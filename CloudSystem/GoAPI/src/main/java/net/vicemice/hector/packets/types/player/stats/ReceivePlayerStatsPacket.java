package net.vicemice.hector.packets.types.player.stats;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

/*
 * Class created at 18:34 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ReceivePlayerStatsPacket implements Serializable {
    private String forPlayer;
    private String statsPlayer;
    private String statsPlayerRankColor;
    private ConcurrentHashMap<String, Long> globalStats;
    private ConcurrentHashMap<String, Long> monthlyStats;
    private ConcurrentHashMap<String, Long> dailyStats;
    private ConcurrentHashMap<String, Long> globalRanking;
    private ConcurrentHashMap<String, Long> monthlyRanking;
    private ConcurrentHashMap<String, Long> dailyRanking;

    public ReceivePlayerStatsPacket(String forPlayer, String statsPlayer, String statsPlayerRankColor, ConcurrentHashMap<String, Long> globalStats, ConcurrentHashMap<String, Long> monthlyStats, ConcurrentHashMap<String, Long> dailyStats, ConcurrentHashMap<String, Long> globalRanking, ConcurrentHashMap<String, Long> monthlyRanking, ConcurrentHashMap<String, Long> dailyRanking) {
        this.forPlayer = forPlayer;
        this.statsPlayer = statsPlayer;
        this.statsPlayerRankColor = statsPlayerRankColor;
        this.globalStats = globalStats;
        this.monthlyStats = monthlyStats;
        this.dailyStats = dailyStats;
        this.globalRanking = globalRanking;
        this.monthlyRanking = monthlyRanking;
        this.dailyRanking = dailyRanking;
    }

    public String getForPlayer() {
        return this.forPlayer;
    }

    public String getStatsPlayer() {
        return this.statsPlayer;
    }

    public String getStatsPlayerRankColor() {
        return this.statsPlayerRankColor;
    }

    public ConcurrentHashMap<String, Long> getGlobalStats() {
        return this.globalStats;
    }

    public ConcurrentHashMap<String, Long> getMonthlyStats() {
        return this.monthlyStats;
    }

    public ConcurrentHashMap<String, Long> getDailyStats() {
        return this.dailyStats;
    }

    public ConcurrentHashMap<String, Long> getGlobalRanking() {
        return this.globalRanking;
    }

    public ConcurrentHashMap<String, Long> getMonthlyRanking() {
        return this.monthlyRanking;
    }

    public ConcurrentHashMap<String, Long> getDailyRanking() {
        return this.dailyRanking;
    }
}
