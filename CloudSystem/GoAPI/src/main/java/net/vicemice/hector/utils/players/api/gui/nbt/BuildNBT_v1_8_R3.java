package net.vicemice.hector.utils.players.api.gui.nbt;

import net.minecraft.server.v1_8_R3.NBTBase;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BuildNBT_v1_8_R3 {

    public ItemStack build(ItemBuilder builder, ItemStack handle, Material material, int metaId, int amount, String name, ArrayList<String> lore, boolean glow, Map<Enchantment, Integer> enchantments, Map<String, NBTBase> nbtBaseMap, List<String> removenbt, List<ItemBuilder.PostItemBuilder> postListener) {
        ItemStack i;
        if (handle != null) {
            if (material != Material.AIR) {
                handle.setType(material);
            }
            if (metaId != -1) {
                handle.setDurability((short)metaId);
            }
            if (amount != -1) {
                handle.setAmount(amount);
            }
            i = handle;
        } else {
            //Validate.validState((material != Material.AIR), ("Invalid item id (" + material.name() + ")"), new Object[0]);
            i = new ItemStack(material, amount == -1 ? 1 : amount, (short)(metaId == -1 ? 0 : metaId));
        }
        ItemMeta item = i.getItemMeta();
        if (name != null) {
            item.setDisplayName(name);
        }
        if (!lore.isEmpty()) {
            item.setLore(lore);
        }
        i.setItemMeta(item);
        if (glow) {
            net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(i);
            NBTTagCompound tag = null;
            if (!nmsStack.hasTag()) {
                tag = new NBTTagCompound();
                nmsStack.setTag(tag);
            }
            if (tag == null) {
                tag = nmsStack.getTag();
            }
            NBTTagList ench = new NBTTagList();
            tag.set("ench", ench);
            nmsStack.setTag(tag);
            i = CraftItemStack.asCraftMirror(nmsStack);
        }
        if (!nbtBaseMap.isEmpty() || !removenbt.isEmpty()) {
            net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(i);
            NBTTagCompound tag = null;
            if (!nmsStack.hasTag()) {
                tag = new NBTTagCompound();
                nmsStack.setTag(tag);
            }
            if (tag == null) {
                tag = nmsStack.getTag();
            }
            if (!removenbt.isEmpty()) {
                for (String s : removenbt) {
                    tag.remove(s);
                }
            }
            if (!nbtBaseMap.isEmpty()) {
                for (String s : nbtBaseMap.keySet()) {
                    tag.set(s, nbtBaseMap.get(s));
                }
            }
            nmsStack.setTag(tag);
            i = CraftItemStack.asCraftMirror(nmsStack);
        }
        i.addEnchantments(enchantments);
        for (ItemBuilder.PostItemBuilder postBuilder : postListener) {
            postBuilder.apply(builder, i);
        }
        return i;
    }

    public ItemStack build(ItemBuilder builder, ItemStack handle, Color color, Material material, int metaId, int amount, String name, ArrayList<String> lore, boolean glow, Map<Enchantment, Integer> enchantments, Map<String, NBTBase> nbtBaseMap, List<String> removenbt, List<ItemBuilder.PostItemBuilder> postListener) {
        ItemStack i;
        if (handle != null) {
            if (material != Material.AIR) {
                handle.setType(material);
            }
            if (metaId != -1) {
                handle.setDurability((short)metaId);
            }
            if (amount != -1) {
                handle.setAmount(amount);
            }
            i = handle;
        } else {
            //Validate.validState((material != Material.AIR), ("Invalid item id (" + material.name() + ")"), new Object[0]);
            i = new ItemStack(material, amount == -1 ? 1 : amount, (short)(metaId == -1 ? 0 : metaId));
        }
        LeatherArmorMeta item = (LeatherArmorMeta) i.getItemMeta();
        item.setColor(color);
        if (name != null) {
            item.setDisplayName(name);
        }
        if (!lore.isEmpty()) {
            item.setLore(lore);
        }
        i.setItemMeta(item);
        if (glow) {
            net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(i);
            NBTTagCompound tag = null;
            if (!nmsStack.hasTag()) {
                tag = new NBTTagCompound();
                nmsStack.setTag(tag);
            }
            if (tag == null) {
                tag = nmsStack.getTag();
            }
            NBTTagList ench = new NBTTagList();
            tag.set("ench", ench);
            nmsStack.setTag(tag);
            i = CraftItemStack.asCraftMirror(nmsStack);
        }
        if (!nbtBaseMap.isEmpty() || !removenbt.isEmpty()) {
            net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(i);
            NBTTagCompound tag = null;
            if (!nmsStack.hasTag()) {
                tag = new NBTTagCompound();
                nmsStack.setTag(tag);
            }
            if (tag == null) {
                tag = nmsStack.getTag();
            }
            if (!removenbt.isEmpty()) {
                for (String s : removenbt) {
                    tag.remove(s);
                }
            }
            if (!nbtBaseMap.isEmpty()) {
                for (String s : nbtBaseMap.keySet()) {
                    tag.set(s, nbtBaseMap.get(s));
                }
            }
            nmsStack.setTag(tag);
            i = CraftItemStack.asCraftMirror(nmsStack);
        }
        i.addEnchantments(enchantments);
        for (ItemBuilder.PostItemBuilder postBuilder : postListener) {
            postBuilder.apply(builder, i);
        }
        return i;
    }
}
