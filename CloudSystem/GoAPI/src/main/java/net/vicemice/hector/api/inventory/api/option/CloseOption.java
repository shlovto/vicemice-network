package net.vicemice.hector.api.inventory.api.option;

import org.bukkit.entity.Player;

public class CloseOption extends ActionOption {
    public CloseOption() {
        super(null);
    }

    @Override
    public void onClick(Player player) {
        super.onClick(player);
        player.closeInventory();
    }
}