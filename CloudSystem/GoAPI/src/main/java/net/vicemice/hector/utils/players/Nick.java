package net.vicemice.hector.utils.players;

import lombok.Getter;
import java.util.UUID;

public class Nick {

    @Getter
    private String name;
    @Getter
    private UUID uuid;

    public Nick(String name, UUID uuid) {
        this.name = name;
        this.uuid = uuid;
    }
}