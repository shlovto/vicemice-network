package net.vicemice.hector.packets.types.slave;

import net.vicemice.hector.utils.ServerType;

import java.io.Serializable;

/*
 * Class created at 18:41 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SlaveServerStart implements Serializable {
    private String name;
    private String id;
    private String serverTemplate;
    private int port;
    private ServerType serverType;

    public SlaveServerStart(String name, String id, int port, String serverTemplate, ServerType serverType) {
        this.name = name;
        this.port = port;
        this.serverTemplate = serverTemplate;
        this.id = id;
        this.serverType = serverType;
    }

    public String getName() {
        return this.name;
    }

    public String getId() {
        return this.id;
    }

    public String getServerTemplate() {
        return this.serverTemplate;
    }

    public int getPort() {
        return this.port;
    }

    public ServerType getServerType() {
        return serverType;
    }
}
