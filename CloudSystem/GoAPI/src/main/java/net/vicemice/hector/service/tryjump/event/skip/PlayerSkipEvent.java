package net.vicemice.hector.service.tryjump.event.skip;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/*
 * Class created at 01:48 - 10.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerSkipEvent extends Event {
    public Player getPlayer() {
        return player;
    }

    public int getNeeded() {
        return needed;
    }

    protected static HandlerList handlerList = new HandlerList();
    private Player player;
    private int needed;

    public PlayerSkipEvent(Player player, int needed) {
        this.player = player;
        this.needed = needed;
    }

    /** Gets the list of all handlers. */
    public static HandlerList getHandlerList() {
        return handlerList;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
}