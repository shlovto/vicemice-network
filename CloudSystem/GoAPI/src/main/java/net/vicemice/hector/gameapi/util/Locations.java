package net.vicemice.hector.gameapi.util;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class Locations {
    @Getter
    private static Location lobbyLocation;

    public static void initLocations() {
        lobbyLocation = new Location(Bukkit.getServer().getWorld("lobby"), -311.5, 131.0, 965.5, -90.0f, 0.0f);
    }
}