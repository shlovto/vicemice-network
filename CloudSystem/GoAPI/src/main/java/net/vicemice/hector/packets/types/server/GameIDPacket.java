package net.vicemice.hector.packets.types.server;

import lombok.Getter;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:30 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GameIDPacket implements Serializable {

    @Getter
    private UUID gameUID;
    @Getter
    private String gameID;

    public GameIDPacket(UUID gameUID, String gameID) {
        this.gameUID = gameUID;
        this.gameID = gameID;
    }
}
