package net.vicemice.hector.api;

import lombok.Getter;
import net.vicemice.hector.api.inventory.item.GameItem;

import java.util.ArrayList;
import java.util.List;

public class NetworkAPI {

    @Getter
    private static NetworkAPI instance;
    @Getter
    private final List<GameItem> gameItems = new ArrayList<>();

    public static void init() {
        instance = new NetworkAPI();
    }
}