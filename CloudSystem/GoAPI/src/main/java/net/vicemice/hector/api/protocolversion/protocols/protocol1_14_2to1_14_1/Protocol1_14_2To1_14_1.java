package net.vicemice.hector.api.protocolversion.protocols.protocol1_14_2to1_14_1;

import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.api.protocol.Protocol;

public class Protocol1_14_2To1_14_1 extends Protocol {

	@Override
	protected void registerPackets() {

	}

	@Override
	public void init(UserConnection userConnection) {

	}
}
