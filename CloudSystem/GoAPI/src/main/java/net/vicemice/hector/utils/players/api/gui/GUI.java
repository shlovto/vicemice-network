package net.vicemice.hector.utils.players.api.gui;

import lombok.Getter;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.internal.SkullChanger;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import java.beans.ConstructorProperties;
import java.util.ArrayList;

/*
 * Class created at 19:56 - 09.03.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GUI {
    @Getter
    private final IUser user;
    @Getter
    private Inventory handle;
    @Getter
    private final ItemClickListener[] clickListeners;
    @Getter
    private final InventoryClickListener clickListener;
    @Getter
    private final ArrayList<CloseListener> closeListener = new ArrayList<>();
    @Getter
    private boolean open = false;
    @Getter
    private int rows = 0, size = 0;

    public GUI(IUser user, String name, int rows) {
        this(user, name, rows, null);
    }

    public GUI(IUser user, String name, int rows, CloseListener closeListener) {
        if (rows > 6) {
            throw new NumberFormatException("too many rows for a gui");
        }
        this.user = user;
        this.rows = rows;
        this.size = (rows * 9);
        this.handle = Bukkit.createInventory(this.getUser().getBukkitPlayer(), (rows * 9), name.replace("&", "§"));
        this.clickListeners = new ItemClickListener[rows * 9];
        this.clickListener = new InventoryClickListener(this);
        if (closeListener != null)
            this.closeListener.add(closeListener);
    }

    public void addCloseListener(CloseListener closeListener) {
        if (closeListener != null)
            this.closeListener.add(closeListener);
    }

    public GUI rename(String name) {
        this.handle = Bukkit.createInventory(this.getUser().getBukkitPlayer(), (rows * 9), name.replace("&", "§"));
        return this;
    }

    protected void handleClose() {
        if (!this.open) {
            return;
        }
        this.open = false;
        HandlerList.unregisterAll(this.clickListener);
        new ArrayList<>(this.closeListener).forEach(e -> {
            e.onClose(this.getUser().getBukkitPlayer());
        });
    }

    public void open() {
        if (this.open) {
            return;
        }
        this.open = true;
        Bukkit.getPluginManager().registerEvents(this.clickListener, Bukkit.getPluginManager().getPlugin("Hector"));
        this.getUser().getBukkitPlayer().openInventory(this.handle);
    }

    public GUI setItem(int index, ItemStack item) {
        return this.setItem(index, item, null);
    }

    public GUI setItem(int index, ItemStack item, ItemClickListener listener) {
        if (this.size < index) return this;
        this.handle.setItem(index, item);
        this.clickListeners[index] = listener;
        return this;
    }

    public GUI fill(ItemStack is) {
        return this.fill(0, Integer.MAX_VALUE, is);
    }

    public GUI fill(int startIndex, ItemStack is) {
        return this.fill(startIndex, Integer.MAX_VALUE, is);
    }

    public GUI fill(int startIndex, int endIndex, ItemStack is) {
        return this.fill(startIndex, endIndex, is, null);
    }

    public GUI fill(int startIndex, int endIndex, ItemStack is, ItemClickListener listener) {
        return this.fill(startIndex, endIndex, is, listener, false);
    }

    public GUI fill(int startIndex, int endIndex, ItemStack is, ItemClickListener listener, boolean force) {
        for (int i = Math.max((int)startIndex, (int)0); i < Math.min(this.handle.getSize(), endIndex); ++i) {
            if (!force && this.handle.getItem(i) != null && this.handle.getItem(i).getType() != Material.AIR) continue;
            this.setItem(i, is, listener);
        }
        return this;
    }

    public static class InventoryClickListener implements Listener {
        private final GUI handle;

        @EventHandler
        public void a(InventoryCloseEvent event) {
            if (event.getInventory().equals(this.handle.getHandle())) {
                this.handle.handleClose();
            }
        }

        @EventHandler
        public void a(InventoryClickEvent e) {
            if (e.getClickedInventory().equals(this.handle.getHandle())) {
                if (e.getSlot() >= 0 && e.getSlot() < this.handle.clickListeners.length && this.handle.clickListeners[e.getSlot()] != null) {
                    this.handle.clickListeners[e.getSlot()].onClick(e);
                }
                e.setCancelled(true);
            }
        }

        @EventHandler
        public void a(PlayerQuitEvent e) {
            if (e.getPlayer().getOpenInventory() == null) {
                return;
            }
            if (this.handle.getHandle().equals(e.getPlayer().getOpenInventory().getTopInventory())) {
                this.handle.handleClose();
            }
        }

        @ConstructorProperties(value={"handle"})
        public InventoryClickListener(GUI handle) {
            this.handle = handle;
        }
    }

    public static interface CloseListener {
        public void onClose(Player var1);
    }

    public static interface ItemClickListener {
        public void onClick(InventoryClickEvent var1);
    }

    public static class Items {
        @Getter
        public static ItemStack leftDis = SkullChanger.getSkullByTexture("f6ce3a72-316b-40c5-a2ee-42a698c1e4ee", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvY2ExM2VkYmU2ODcwMjFlYjU4ZmU1OWY4NWZkNmMwYTc3OWVlZDdiMTQzODAyOTEyYjQ3ZTEzNGY4OTk4YzU5In19fQ==");
        @Getter
        public static ItemStack left = SkullChanger.getSkullByTexture("a80326ee-b589-49fb-8ae1-3c4da63f76f3", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmM0YWQxYTU2ZTI2ZDg0ODJkYTRjYjBmMGQ4N2Q4M2U1NGFiYTgwYTdmMzk2MDkzZjQxNjJiMzVmODc4N2MxMSJ9fX0=");
        @Getter
        public static ItemStack rightDis = SkullChanger.getSkullByTexture("576430b4-8c4f-4e94-bcfc-1e29673c2e61", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvN2YyMDg2ZWM5OTUyZWY3MDUxYzMwOWY2YzM4YmExODczZmVkZGMzZmYzMTdiMWZkMTNhYTJjYTg1MjU4M2U5NSJ9fX0=");
        @Getter
        public static ItemStack right = SkullChanger.getSkullByTexture("9920c7ac-e3bf-4324-a305-253f9c02ddfa", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzI0Mjg4OGJkN2NmYzJkOTgwMTA4MmM0M2IwN2M5ZGJiZjEwMDIyNzFkYzhjOTI4ZTZkMjhkYzE3YjI3YjIxMCJ9fX0=");
    }
}
