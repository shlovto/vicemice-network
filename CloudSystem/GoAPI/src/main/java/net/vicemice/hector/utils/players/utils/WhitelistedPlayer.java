package net.vicemice.hector.utils.players.utils;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.UUID;

public class WhitelistedPlayer implements Serializable {

    private UUID uniqueId;

    @ConstructorProperties(value={"uniqueId"})
    public WhitelistedPlayer(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }
}
