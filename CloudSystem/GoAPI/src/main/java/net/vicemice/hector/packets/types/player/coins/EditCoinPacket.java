package net.vicemice.hector.packets.types.player.coins;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:30 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class EditCoinPacket implements Serializable {

    @Getter
    private UUID uniqueId;
    @Getter
    private long coins;
    @Getter
    private boolean remove;

    public EditCoinPacket(UUID uniqueId, long coins, boolean remove) {
        this.uniqueId = uniqueId;
        this.coins = coins;
        this.remove = remove;
    }
}
