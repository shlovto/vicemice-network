package net.vicemice.hector.api.protocolversion.api.type.types.minecraft;

import net.vicemice.hector.api.protocolversion.api.minecraft.chunks.Chunk;
import net.vicemice.hector.api.protocolversion.api.type.Type;

public abstract class BaseChunkType extends Type<Chunk> {
    public BaseChunkType() {
        super(Chunk.class);
    }

    public BaseChunkType(String typeName) {
        super(typeName, Chunk.class);
    }

    @Override
    public Class<? extends Type> getBaseClass() {
        return BaseChunkType.class;
    }
}
