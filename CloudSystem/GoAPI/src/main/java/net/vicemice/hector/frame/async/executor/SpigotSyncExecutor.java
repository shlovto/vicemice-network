package net.vicemice.hector.frame.async.executor;

import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.beans.ConstructorProperties;
import java.util.concurrent.Executor;

public class SpigotSyncExecutor implements Executor {
    private final Plugin plugin;

    @Override
    public void execute(@NonNull Runnable command) {
        if (command == null) {
            throw new NullPointerException("command");
        }
        if (Bukkit.isPrimaryThread()) {
            command.run();
        } else {
            Bukkit.getScheduler().runTask(this.plugin, command);
        }
    }

    @ConstructorProperties(value={"plugin"})
    public SpigotSyncExecutor(Plugin plugin) {
        this.plugin = plugin;
    }
}
