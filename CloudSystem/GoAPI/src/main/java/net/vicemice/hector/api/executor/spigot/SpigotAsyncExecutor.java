package net.vicemice.hector.api.executor.spigot;

import java.beans.ConstructorProperties;
import java.util.concurrent.Executor;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

public class SpigotAsyncExecutor implements Executor {
    private final Plugin plugin;

    @Override
    public void execute(@NonNull Runnable runnable) {
        if (runnable == null) {
            throw new NullPointerException("command");
        }
        Bukkit.getScheduler().runTaskAsynchronously(this.plugin, runnable);
    }

    @ConstructorProperties(value={"plugin"})
    public SpigotAsyncExecutor(Plugin plugin) {
        this.plugin = plugin;
    }
}