package net.vicemice.hector.packets.types.player.abuse;

import lombok.Getter;
import net.vicemice.hector.utils.AntiCrash;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:31 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class AntiCrashPacket implements Serializable {

    @Getter
    private UUID uniqueId;
    @Getter
    private AntiCrash antiCrash;

    public AntiCrashPacket(UUID uniqueId, AntiCrash antiCrash) {
        this.uniqueId = uniqueId;
        this.antiCrash = antiCrash;
    }
}
