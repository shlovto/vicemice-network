package net.vicemice.hector.packets.types.player.report;

import lombok.Getter;
import net.vicemice.hector.utils.players.ReportPlayer;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 20:07 - 16.04.2020
 * Copyright (C) elrobtossohn
 */
@Getter
public class ReportInventoryPacket implements Serializable {
    private UUID reporter;
    private ReportPlayer reportPlayer;

    public ReportInventoryPacket(UUID reporter, ReportPlayer reportPlayer) {
        this.reporter = reporter;
        this.reportPlayer = reportPlayer;
    }
}