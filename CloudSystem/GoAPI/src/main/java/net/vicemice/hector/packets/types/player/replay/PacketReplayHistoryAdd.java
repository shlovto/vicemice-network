package net.vicemice.hector.packets.types.player.replay;

import lombok.Getter;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:22 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PacketReplayHistoryAdd implements Serializable {

    @Getter
    private UUID uniqueId;
    @Getter
    private PacketReplayEntries.PacketReplayEntry entry;

    @ConstructorProperties(value = {"uniqueId", "entry"})
    public PacketReplayHistoryAdd(UUID uniqueId, PacketReplayEntries.PacketReplayEntry entry) {
        this.uniqueId = uniqueId;
        this.entry = entry;
    }
}