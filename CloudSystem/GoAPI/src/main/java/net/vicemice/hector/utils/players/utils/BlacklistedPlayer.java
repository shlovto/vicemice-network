package net.vicemice.hector.utils.players.utils;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.UUID;

public class BlacklistedPlayer implements Serializable {

    private UUID uniqueId;

    @ConstructorProperties(value={"uniqueId"})
    public BlacklistedPlayer(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }
}
