package net.vicemice.hector.gameapi.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameStartEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private long startTime;
    private boolean replayStarted;

    public GameStartEvent(long startTime, boolean replayStarted) {
        this.startTime = startTime;
        this.replayStarted = replayStarted;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public long getStartTime() {
        return startTime;
    }

    public boolean isReplayStarted() {
        return replayStarted;
    }

    public HandlerList getHandlers() {
        return handlers;
    }
}
