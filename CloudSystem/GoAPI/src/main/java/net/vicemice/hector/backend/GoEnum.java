package net.vicemice.hector.backend;

public enum GoEnum {
    UNKNOWN,
    CLOUD,
    SLAVE,
    PROXY,
    SERVER;
}
