package net.vicemice.hector.packets.types.player.report;

import java.io.Serializable;

/*
 * Class created at 18:26 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerReportedPacket implements Serializable {
    private String playerName;

    public PlayerReportedPacket(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerName() {
        return this.playerName;
    }
}
