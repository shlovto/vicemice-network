package net.vicemice.hector.packets.types.player.stats;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 00:40 - 04.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class StatisticsPlayerRequestInfoPacket implements Serializable {
    private UUID uniqueId;
    private String[] rankKeys;

    @ConstructorProperties(value={"name", "rankKeys"})
    public StatisticsPlayerRequestInfoPacket(UUID uniqueId, String[] rankKeys) {
        this.uniqueId = uniqueId;
        this.rankKeys = rankKeys;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public String[] getRankKeys() {
        return this.rankKeys;
    }
}
