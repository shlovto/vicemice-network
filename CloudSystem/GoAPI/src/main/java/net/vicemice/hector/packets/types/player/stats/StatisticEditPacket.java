package net.vicemice.hector.packets.types.player.stats;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 00:38 - 04.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class StatisticEditPacket implements Serializable {
    private UUID uniqueId;
    private Action action;
    private String key;
    private long value;

    @ConstructorProperties(value = {"name", "action", "key", "value"})
    public StatisticEditPacket(UUID uniqueId, Action action, String key, long value) {
        this.uniqueId = uniqueId;
        this.action = action;
        this.key = key;
        this.value = value;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public Action getAction() {
        return this.action;
    }

    public String getKey() {
        return this.key;
    }

    public long getValue() {
        return this.value;
    }

    public static enum Action {
        ADD,
        SET,
        REMOVE;
    }
}