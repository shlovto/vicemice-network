package net.vicemice.hector.utils.players.utils;

public enum PlayerState {
    ONLINE,
    OFFLINE,
    AFK,
    INGAME,
    SPECTATOR;
}
