package net.vicemice.hector.utils;

import com.google.common.reflect.ClassPath;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.backend.PacketGetter;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import java.io.IOException;
import java.util.logging.Level;

public class Register {

    /**
     * Register all Listener in a package and the packages in the package
     * @param main defines the Plugin which register Listeners
     * @param searchPackage defines the Package e.q. net.vicemice.gonet.game.listeners
     */
    public static void registerListener(Plugin main, String searchPackage) {
        PluginManager pluginManager = Bukkit.getServer().getPluginManager();
        try {
            int i = 0;
            for (ClassPath.ClassInfo classInfo : ClassPath.from(main.getClass().getClassLoader()).getTopLevelClassesRecursive(searchPackage)) {
                Class clazz = Class.forName(classInfo.getName());

                if (Listener.class.isAssignableFrom(clazz)) {
                    pluginManager.registerEvents((Listener) clazz.newInstance(), main);
                    i++;
                }
            }
            Bukkit.getLogger().log(Level.INFO, "[Hector] Successfully loaded "+i+" Listeners in '"+searchPackage+"' from '"+main.getName()+"'");
        } catch (IOException | ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Register all PacketGetters in a package and the packages in the package
     * @param api defines the API which register PacketGetters
     * @param searchPackage defines the Package e.q. net.vicemice.gonet.game.packetlisteners
     */
    public static void registerPacketGetter(GoAPI api, String searchPackage) {
        try {
            int i = 0;
            for (ClassPath.ClassInfo classInfo : ClassPath.from(api.getClass().getClassLoader()).getTopLevelClassesRecursive(searchPackage)) {
                Class clazz = Class.forName(classInfo.getName());

                if (clazz.isInstance(PacketGetter.class)) {
                    api.getNettyClient().addGetter((PacketGetter) clazz.newInstance());
                    i++;
                }
            }
            Bukkit.getLogger().log(Level.INFO, "[Hector] Successfully loaded "+i+" PacketGetters in '"+searchPackage+"'");
        } catch (IOException | ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
            ex.printStackTrace();
        }
    }
}
