package net.vicemice.hector.api.protocolversion.bukkit.listeners.protocol1_9to1_8;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockPlaceEvent;
import net.vicemice.hector.api.protocolversion.ViaVersionPlugin;
import net.vicemice.hector.api.protocolversion.api.minecraft.Position;
import net.vicemice.hector.api.protocolversion.bukkit.listeners.ViaBukkitListener;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.Protocol1_9To1_8;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.storage.EntityTracker;

public class BlockListener extends ViaBukkitListener {

    public BlockListener(ViaVersionPlugin plugin) {
        super(plugin, Protocol1_9To1_8.class);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void placeBlock(BlockPlaceEvent e) {
        if (isOnPipe(e.getPlayer())) {
            Block b = e.getBlockPlaced();
            getUserConnection(e.getPlayer())
                    .get(EntityTracker.class)
                    .addBlockInteraction(new Position((long) b.getX(), (long) b.getY(), (long) b.getZ()));
        }
    }
}
