package net.vicemice.hector.packets.types.lobby.type;

import lombok.Getter;
import java.io.Serializable;

public class LobbyDataInfo implements Serializable, Comparable<LobbyDataInfo> {

    @Getter
    private final String name;
    @Getter
    private final int id, players;

    public LobbyDataInfo(String name, int players) {
        this.name = name;
        this.id = Integer.parseInt(name.split("-")[1]);
        this.players = players;
    }

    @Override
    public int compareTo(LobbyDataInfo o) {
        return this.id < o.id ? -1 : (this.id > o.id ? 1 : 0);
    }
}
