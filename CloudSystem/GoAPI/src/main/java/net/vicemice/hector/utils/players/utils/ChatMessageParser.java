package net.vicemice.hector.utils.players.utils;

import java.beans.ConstructorProperties;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.vicemice.hector.packets.types.player.MessagePacket;

/*
 * Class created at 17:58 - 29.04.2020
 * Copyright (C) elrobtossohn
 */
public class ChatMessageParser {
    private static final Pattern url = Pattern.compile("^(?:(https?)://)?([-\\w_\\.]{2,}\\.[a-z]{2,4})(/\\S*)?$");
    private final String message;
    private TreeMap<String, ClickEvent> clickEventTreeMap = new TreeMap<>();
    private TreeMap<String, HoverEvent> hoverEventTreeMap = new TreeMap<>();

    public void addClickEvent(String ident, MessagePacket.ClickHoverEvent clickEvent) {
        this.clickEventTreeMap.put(ident, new ClickEvent(ClickEvent.Action.valueOf(clickEvent.getAction()), clickEvent.getValue()));
    }

    public void addHoverEvent(String ident, MessagePacket.ClickHoverEvent hoverEvent) {
        this.hoverEventTreeMap.put(ident, new HoverEvent(HoverEvent.Action.valueOf(hoverEvent.getAction()), TextComponent.fromLegacyText(hoverEvent.getValue())));
    }

    public BaseComponent[] parse() {
        ArrayList<TextComponent> components = new ArrayList<TextComponent>();
        StringBuilder builder = new StringBuilder();
        TextComponent component = new TextComponent();
        Matcher matcher = url.matcher(this.message);
        for (int i = 0; i < this.message.length(); ++i) {
            char c = this.message.charAt(i);
            if (c == '\u00a7') {
                if ((c = this.message.charAt(++i)) >= 'A' && c <= 'Z') {
                    c = (char)(c + 32);
                }
                if (builder.length() > 0) {
                    TextComponent old = component;
                    component = new TextComponent(old);
                    old.setText(builder.toString());
                    builder = new StringBuilder();
                    components.add(old);
                }
                ChatColor format = ChatColor.getByChar(c);
                switch (format) {
                    case BOLD: {
                        component.setBold(true);
                        break;
                    }
                    case ITALIC: {
                        component.setItalic(true);
                        break;
                    }
                    case UNDERLINE: {
                        component.setUnderlined(true);
                        break;
                    }
                    case STRIKETHROUGH: {
                        component.setStrikethrough(true);
                        break;
                    }
                    case MAGIC: {
                        component.setObfuscated(true);
                        break;
                    }
                    case RESET: {
                        format = ChatColor.WHITE;
                    }
                    default: {
                        component = new TextComponent();
                        component.setColor(format);
                        break;
                    }
                }
                continue;
            }
            if (c == '%') {
                String identifier;
                ++i;
                StringBuilder sb = new StringBuilder();
                while (i < this.message.length()) {
                    c = this.message.charAt(i);
                    ++i;
                    if (c == '%') break;
                    sb.append(c);
                }
                --i;
                String sbString = sb.toString();
                ClickEvent clickEvent = null;
                HoverEvent hoverEvent = null;
                if (sbString.contains("clickhover:")) {
                    identifier = sbString.replace("clickhover:", "");
                    hoverEvent = this.hoverEventTreeMap.get(identifier);
                    clickEvent = this.clickEventTreeMap.get(identifier);
                    if (hoverEvent == null || clickEvent == null) {
                        System.out.println("hover or click event is null");
                        continue;
                    }
                } else if (sbString.contains("click:")) {
                    identifier = sbString.replace("click:", "");
                    if (!this.clickEventTreeMap.containsKey(identifier)) continue;
                    clickEvent = this.clickEventTreeMap.get(identifier);
                } else if (sbString.contains("hover:")) {
                    identifier = sbString.replace("hover:", "");
                    if (!this.hoverEventTreeMap.containsKey(identifier)) continue;
                    hoverEvent = this.hoverEventTreeMap.get(identifier);
                } else {
                    builder.append("%");
                    builder.append(sbString);
                    continue;
                }
                if (builder.length() <= 0) continue;
                StringBuilder whitespaces = new StringBuilder();
                StringBuilder strBui = new StringBuilder();
                StringBuilder whitespacee = new StringBuilder();
                boolean end = false;
                for (int ia = 0; ia < builder.length(); ++ia) {
                    if ((byte)builder.charAt(ia) == 32) {
                        if (!end) {
                            whitespaces.append(builder.charAt(ia));
                            continue;
                        }
                        boolean append = true;
                        for (int ia2 = ia; ia2 < builder.length(); ++ia2) {
                            if ((byte)builder.charAt(ia2) == 32) continue;
                            append = false;
                        }
                        if (append) {
                            whitespacee.append(builder.charAt(ia));
                            continue;
                        }
                        strBui.append(builder.charAt(ia));
                        continue;
                    }
                    strBui.append(builder.charAt(ia));
                    end = true;
                }
                if (whitespaces.length() > 0) {
                    TextComponent old = component;
                    component = new TextComponent(old);
                    old.setText(whitespaces.toString());
                    components.add(old);
                }
                TextComponent oldb = component;
                component = new TextComponent(oldb);
                oldb.setText(strBui.toString());
                if (clickEvent != null) {
                    oldb.setClickEvent(clickEvent);
                }
                if (hoverEvent != null) {
                    oldb.setHoverEvent(hoverEvent);
                }
                builder = new StringBuilder();
                components.add(oldb);
                if (whitespacee.length() <= 0) continue;
                TextComponent olde = component;
                component = new TextComponent(olde);
                olde.setText(whitespacee.toString());
                components.add(component);
                continue;
            }
            int pos = this.message.indexOf(32, i);
            if (pos == -1) {
                pos = this.message.length();
            }
            if (matcher.region(i, pos).find()) {
                TextComponent old;
                if (builder.length() > 0) {
                    old = component;
                    component = new TextComponent(old);
                    old.setText(builder.toString());
                    builder = new StringBuilder();
                    components.add(old);
                }
                old = component;
                component = new TextComponent(old);
                String urlString = this.message.substring(i, pos);
                component.setText(urlString);
                component.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, urlString.startsWith("http") ? urlString : "http://" + urlString));
                components.add(component);
                i += pos - i - 1;
                component = old;
                continue;
            }
            builder.append(c);
        }
        if (builder.length() > 0) {
            component.setText(builder.toString());
            components.add(component);
        }
        if (components.size() == 0) {
            components.add(new TextComponent(""));
        }
        return components.toArray(new BaseComponent[components.size()]);
    }

    @ConstructorProperties(value={"message"})
    public ChatMessageParser(String message) {
        this.message = message;
    }
}