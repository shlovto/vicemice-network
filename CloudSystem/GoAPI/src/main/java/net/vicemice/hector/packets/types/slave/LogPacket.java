package net.vicemice.hector.packets.types.slave;

import lombok.Getter;
import java.io.Serializable;

/*
 * Class created at 18:20 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class LogPacket implements Serializable {

    @Getter
    private String slaveId, serverId;
    @Getter
    private String host;

    public LogPacket(String slaveId, String serverId, String host) {
        this.slaveId = slaveId;
        this.serverId = serverId;
        this.host = host;
    }
}
