package net.vicemice.hector.packets.types.player;

import lombok.Getter;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 08:52 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ChatPacket implements Serializable {

    @Getter
    private UUID uniqueId;
    @Getter
    private String message;
    @Getter
    private Long time;

    @ConstructorProperties(value = {"uniqueId", "message", "time"})
    public ChatPacket(UUID uniqueId, String message, Long time) {
        this.uniqueId = uniqueId;
        this.message = message;
        this.time = time;
    }
}
