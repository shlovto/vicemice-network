package net.vicemice.hector.packets.types;

import lombok.Getter;
import lombok.NonNull;
import net.vicemice.hector.packets.PacketType;

import java.io.Serializable;

/*
 * Class created at 12:56 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PacketHolder implements Serializable {

    @Getter
    private String key;
    @Getter
    private Object value;

    public PacketHolder(PacketType type, Object value) {
        this.key = type.getKey();
        this.value = value;
    }

    public PacketHolder(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public PacketType getType() {
        return PacketType.valueOf(this.key);
    }

    public <T> T getPacket(@NonNull Class<T> cls) {
        if (cls == null) {
            throw new NullPointerException("cls");
        }
        if (this.value == null) {
            return null;
        }
        if (!this.value.getClass().isAssignableFrom(cls)) {
            throw new RuntimeException("Invalid packet value! (" + cls.getName() + " -> " + this.value.getClass().getName() + ")");
        }
        return (T)this.value;
    }
}
