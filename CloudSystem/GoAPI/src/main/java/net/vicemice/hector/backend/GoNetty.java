package net.vicemice.hector.backend;

import io.netty.channel.Channel;
import net.vicemice.hector.packets.types.PacketHolder;

import java.util.List;

/*
 * Class created at 01:08 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public interface GoNetty {

    public void startClient();

    public void addGetter(PacketGetter packetGetter);

    public Channel getChannel();

    public String getHost();

    public int getPort();

    public List<PacketGetter> getPacketGetters();

    public List<PacketHolder> getBeforeActive();

    public abstract void channelActive(Channel var1);

    public abstract void channelRead(PacketHolder var1);

    public abstract void setChannel(Channel var1);
}