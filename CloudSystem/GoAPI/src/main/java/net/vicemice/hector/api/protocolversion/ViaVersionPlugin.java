package net.vicemice.hector.api.protocolversion;

import com.google.gson.JsonObject;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import net.vicemice.hector.api.protocolversion.api.Via;
import net.vicemice.hector.api.protocolversion.api.ViaAPI;
import net.vicemice.hector.api.protocolversion.api.ViaVersion;
import net.vicemice.hector.api.protocolversion.api.command.ViaCommandSender;
import net.vicemice.hector.api.protocolversion.api.configuration.ConfigurationProvider;
import net.vicemice.hector.api.protocolversion.api.platform.TaskId;
import net.vicemice.hector.api.protocolversion.api.platform.ViaPlatform;
import net.vicemice.hector.api.protocolversion.bukkit.classgenerator.ClassGenerator;
import net.vicemice.hector.api.protocolversion.bukkit.commands.BukkitCommandHandler;
import net.vicemice.hector.api.protocolversion.bukkit.commands.BukkitCommandSender;
import net.vicemice.hector.api.protocolversion.bukkit.platform.*;
import net.vicemice.hector.api.protocolversion.bukkit.util.NMSUtil;
import net.vicemice.hector.api.protocolversion.dump.PluginInfo;
import net.vicemice.hector.api.protocolversion.util.GsonUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

public class ViaVersionPlugin implements ViaPlatform {

    private BukkitCommandHandler commandHandler;
    private boolean compatSpigotBuild = false;
    private boolean spigot = true;
    private boolean lateBind = false;
    private boolean protocolSupport = false;
    @Getter
    private BukkitViaConfig conf;
    @Getter
    private ViaAPI<Player> api = new BukkitViaAPI(this);
    private List<Runnable> queuedTasks = new ArrayList<>();
    private List<Runnable> asyncQueuedTasks = new ArrayList<>();

    public ViaVersionPlugin() {
        // Command handler
        commandHandler = new BukkitCommandHandler();
        // Init platform
        Via.init(ViaManager.builder()
                .platform(this)
                .commandHandler(commandHandler)
                .injector(new BukkitViaInjector())
                .loader(new BukkitViaLoader(this))
                .build());
        // Config magic
        conf = new BukkitViaConfig();
        // For compatibility
        ViaVersion.setInstance(this);

        // Check if we're using protocol support too
        protocolSupport = Bukkit.getPluginManager().getPlugin("ProtocolSupport") != null;

        if (protocolSupport) {
            getLogger().info("Hooking into ProtocolSupport, to prevent issues!");
            try {
                BukkitViaInjector.patchLists();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void load() {
        // Spigot detector
        try {
            Class.forName("org.spigotmc.SpigotConfig");
        } catch (ClassNotFoundException e) {
            spigot = false;
        }

        // Check if it's a spigot build with a protocol mod
        try {
            compatSpigotBuild = NMSUtil.nms("PacketEncoder").getDeclaredField("version") != null;
        } catch (Exception e) {
            compatSpigotBuild = false;
        }

        // Generate classes needed (only works if it's compat or ps)
        ClassGenerator.generate();
        lateBind = !BukkitViaInjector.isBinded();

        getLogger().info("Protocol Version " + Bukkit.getPluginManager().getPlugin("Hector").getDescription().getVersion() + (compatSpigotBuild ? "compat" : "") + " is now loaded" + (lateBind ? ", waiting for boot. (late-bind)" : ", injecting!"));
        if (!lateBind) {
            Via.getManager().init();
        }
    }

    public void enable() {
        if (lateBind) {
            Via.getManager().init();
        }

        //getCommand("viaversion").setExecutor(commandHandler);
        //getCommand("viaversion").setTabCompleter(commandHandler);

        // Warn them if they have anti-xray on and they aren't using spigot
        if (conf.isAntiXRay() && !spigot) {
            getLogger().info("You have anti-xray on in your config, since you're not using spigot it won't fix xray!");
        }

        // Run queued tasks
        for (Runnable r : queuedTasks) {
            Bukkit.getScheduler().runTask(Bukkit.getPluginManager().getPlugin("Hector"), r);
        }
        queuedTasks.clear();

        // Run async queued tasks
        for (Runnable r : asyncQueuedTasks) {
            Bukkit.getScheduler().runTaskAsynchronously(Bukkit.getPluginManager().getPlugin("Hector"), r);
        }
        asyncQueuedTasks.clear();
    }

    public void disable() {
        Via.getManager().destroy();
    }

    public boolean isCompatSpigotBuild() {
        return compatSpigotBuild;
    }


    public boolean isSpigot() {
        return this.spigot;
    }

    public boolean isProtocolSupport() {
        return protocolSupport;
    }

    @Override
    public Logger getLogger() {
        return Bukkit.getPluginManager().getPlugin("Hector").getLogger();
    }

    @Override
    public String getPlatformName() {
        return Bukkit.getServer().getName();
    }

    @Override
    public String getPlatformVersion() {
        return Bukkit.getServer().getVersion();
    }

    @Override
    public String getPluginVersion() {
        return Bukkit.getPluginManager().getPlugin("Hector").getDescription().getVersion();
    }

    @Override
    public TaskId runAsync(Runnable runnable) {
        if (isPluginEnabled()) {
            return new BukkitTaskId(Bukkit.getPluginManager().getPlugin("Hector").getServer().getScheduler().runTaskAsynchronously(Bukkit.getPluginManager().getPlugin("Hector"), runnable).getTaskId());
        } else {
            asyncQueuedTasks.add(runnable);
            return new BukkitTaskId(null);
        }
    }

    @Override
    public TaskId runSync(Runnable runnable) {
        if (isPluginEnabled()) {
            return new BukkitTaskId(Bukkit.getPluginManager().getPlugin("Hector").getServer().getScheduler().runTask(Bukkit.getPluginManager().getPlugin("Hector"), runnable).getTaskId());
        } else {
            queuedTasks.add(runnable);
            return new BukkitTaskId(null);
        }
    }

    @Override
    public TaskId runSync(Runnable runnable, Long ticks) {
        return new BukkitTaskId(Bukkit.getPluginManager().getPlugin("Hector").getServer().getScheduler().runTaskLater(Bukkit.getPluginManager().getPlugin("Hector"), runnable, ticks).getTaskId());
    }

    @Override
    public TaskId runRepeatingSync(Runnable runnable, Long ticks) {
        return new BukkitTaskId(Bukkit.getPluginManager().getPlugin("Hector").getServer().getScheduler().runTaskTimer(Bukkit.getPluginManager().getPlugin("Hector"), runnable, 0, ticks).getTaskId());
    }

    @Override
    public void cancelTask(TaskId taskId) {
        if (taskId == null) return;
        if (taskId.getObject() == null) return;
        if (taskId instanceof BukkitTaskId) {
            Bukkit.getPluginManager().getPlugin("Hector").getServer().getScheduler().cancelTask((Integer) taskId.getObject());
        }
    }

    @Override
    public ViaCommandSender[] getOnlinePlayers() {
        ViaCommandSender[] array = new ViaCommandSender[Bukkit.getOnlinePlayers().size()];
        int i = 0;
        for (Player player : Bukkit.getOnlinePlayers()) {
            array[i++] = new BukkitCommandSender(player);
        }
        return array;
    }

    @Override
    public void sendMessage(UUID uuid, String message) {
        Player player = Bukkit.getPlayer(uuid);
        if (player != null) {
            player.sendMessage(message);
        }
    }

    @Override
    public boolean kickPlayer(UUID uuid, String message) {
        Player player = Bukkit.getPlayer(uuid);
        if (player != null) {
            player.kickPlayer(message);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean isPluginEnabled() {
        return Bukkit.getPluginManager().getPlugin("Hector").isEnabled();
    }

    @Override
    public ConfigurationProvider getConfigurationProvider() {
        return conf;
    }

    @Override
    public void onReload() {
        if (Bukkit.getPluginManager().getPlugin("ProtocolLib") != null) {
            getLogger().severe("ViaVersion is already loaded, we're going to kick all the players... because otherwise we'll crash because of ProtocolLib.");
            for (Player player : Bukkit.getOnlinePlayers()) {
                player.kickPlayer(ChatColor.translateAlternateColorCodes('&', getConf().getReloadDisconnectMsg()));
            }

        } else {
            getLogger().severe("ViaVersion is already loaded, this should work fine. If you get any console errors, try rebooting.");
        }
    }

    @Override
    public JsonObject getDump() {
        JsonObject platformSpecific = new JsonObject();

        List<PluginInfo> plugins = new ArrayList<>();
        for (Plugin p : Bukkit.getPluginManager().getPlugins())
            plugins.add(new PluginInfo(p.isEnabled(), p.getDescription().getName(), p.getDescription().getVersion(), p.getDescription().getMain(), p.getDescription().getAuthors()));

        platformSpecific.add("plugins", GsonUtil.getGson().toJsonTree(plugins));
        // TODO more? ProtocolLib things etc?

        return platformSpecific;
    }

    @Override
    public boolean isOldClientsAllowed() {
        return !protocolSupport; // Use protocolsupport for older clients
    }
}
