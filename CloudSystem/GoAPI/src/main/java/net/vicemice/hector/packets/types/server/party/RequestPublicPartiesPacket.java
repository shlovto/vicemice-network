package net.vicemice.hector.packets.types.server.party;

import lombok.Getter;

import java.beans.ConstructorProperties;
import java.io.Serializable;

/*
 * Class created at 19:08 - 31.03.2020
 * Copyright (C) elrobtossohn
 */
public class RequestPublicPartiesPacket implements Serializable {

    @Getter
    private String callBackId;

    @ConstructorProperties(value = {"uniqueId"})
    public RequestPublicPartiesPacket(String callBackId) {
        this.callBackId = callBackId;
    }
}
