package net.vicemice.hector.api.protocolversion.protocols.protocol1_14_1to1_14;

import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.api.protocol.Protocol;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_14_1to1_14.packets.EntityPackets;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_14_1to1_14.storage.EntityTracker;

public class Protocol1_14_1To1_14 extends Protocol {

    @Override
    protected void registerPackets() {
        EntityPackets.register(this);
    }

    @Override
    public void init(UserConnection userConnection) {
        userConnection.put(new EntityTracker(userConnection));
    }
}
