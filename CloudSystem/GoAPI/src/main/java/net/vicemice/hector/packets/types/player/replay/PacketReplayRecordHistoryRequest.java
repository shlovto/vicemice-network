package net.vicemice.hector.packets.types.player.replay;

import lombok.Getter;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:22 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PacketReplayRecordHistoryRequest implements Serializable {

    @Getter
    private UUID requester, uniqueId;

    @ConstructorProperties(value = {"requester", "uniqueId"})
    public PacketReplayRecordHistoryRequest(UUID requester, UUID uniqueId) {
        this.requester = requester;
        this.uniqueId = uniqueId;
    }
}
