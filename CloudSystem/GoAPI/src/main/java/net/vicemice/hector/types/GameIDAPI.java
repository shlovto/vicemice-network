package net.vicemice.hector.types;

import java.util.UUID;

/*
 * Class created at 06:02 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public interface GameIDAPI {

    /**
     * @return returns the game unique id
     */
    public UUID getGameUID();

    /**
     * @return returns the game id
     */
    public String getGameID();

    /**
     * @param uniqueId set the game unique id
     */
    public void setGameUID(UUID uniqueId);

    /**
     * @param gameID set the game id
     */
    public void setGameID(String gameID);

    /**
     * @param callback defines the callback which will be runned when we get an response
     */
    public void getGameID(Callback callback);
}