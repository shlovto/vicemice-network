package net.vicemice.hector.packets.types.player.stats;

import java.io.Serializable;
import java.util.List;

/*
 * Class created at 09:51 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class TopStatsPacket implements Serializable {
    private String callBackID;
    private List<String[]> statsData;

    public TopStatsPacket(String callBackID, List<String[]> statsData) {
        this.callBackID = callBackID;
        this.statsData = statsData;
    }

    public String getCallBackID() {
        return this.callBackID;
    }

    public List<String[]> getStatsData() {
        return this.statsData;
    }
}
