package net.vicemice.hector.utils.players;

import net.vicemice.hector.packets.types.player.replay.PacketReplayEntries;
import net.vicemice.hector.packets.types.player.settings.SettingsPacket;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.lobby.LobbyFriendData;
import net.vicemice.hector.utils.players.lobby.LobbyRequestData;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class NetworkPlayer implements Serializable {

    private final UUID uniqueId;
    private final long coins, playTime;
    private final Rank rank;
    private final Locale locale;

    private final String statusMessage;
    private long statusSet, statusBan;

    private final List<LobbyFriendData> friends;
    private final List<LobbyRequestData> requests;

    private final SettingsPacket settingsPacket;

    private final ConcurrentHashMap<StatisticPeriod, ConcurrentHashMap<String, Long>> statistics, ranking;

    private final List<PacketReplayEntries.PacketReplayEntry> savedReplays, recentReplays;

    public NetworkPlayer(UUID uniqueId, long coins, long playTime, Rank rank, Locale locale, String statusMessage, long statusSet, long statusBan, List<LobbyFriendData> friends, List<LobbyRequestData> requests, SettingsPacket settingsPacket, ConcurrentHashMap<StatisticPeriod, ConcurrentHashMap<String, Long>> statistics, ConcurrentHashMap<StatisticPeriod, ConcurrentHashMap<String, Long>> ranking, List<PacketReplayEntries.PacketReplayEntry> savedReplays, List<PacketReplayEntries.PacketReplayEntry> recentReplays) {
        this.uniqueId = uniqueId;
        this.coins = coins;
        this.playTime = playTime;
        this.rank = rank;
        this.locale = locale;

        this.statusMessage = statusMessage;
        this.statusSet = statusSet;
        this.statusBan = statusBan;

        this.friends = friends;
        this.requests = requests;
        this.settingsPacket = settingsPacket;
        this.statistics = statistics;
        this.ranking = ranking;
        this.savedReplays = savedReplays;
        this.recentReplays = recentReplays;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public long getCoins() {
        return coins;
    }

    public long getPlayTime() {
        return playTime;
    }

    @Deprecated
    public long getOnlineTime() {
        return playTime;
    }

    public Rank getRank() {
        return rank;
    }

    public Locale getLocale() {
        return locale;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public long getStatusSet() {
        return statusSet;
    }

    public long getStatusBan() {
        return statusBan;
    }

    public List<LobbyFriendData> getFriends() {
        return friends;
    }

    public List<LobbyRequestData> getRequests() {
        return requests;
    }

    public SettingsPacket getSettingsPacket() {
        return settingsPacket;
    }

    public ConcurrentHashMap<StatisticPeriod, ConcurrentHashMap<String, Long>> getStatistics() {
        return statistics;
    }

    public ConcurrentHashMap<StatisticPeriod, ConcurrentHashMap<String, Long>> getRanking() {
        return ranking;
    }

    public List<PacketReplayEntries.PacketReplayEntry> getSavedReplays() {
        return savedReplays;
    }

    public List<PacketReplayEntries.PacketReplayEntry> getRecentReplays() {
        return recentReplays;
    }

    public PacketReplayEntries.PacketReplayEntry getReplayEntryByGameID(String gameId) {
        for (PacketReplayEntries.PacketReplayEntry entry : this.getRecentReplays()) {
            if (!entry.getGameId().equals(gameId)) continue;
            return entry;
        }

        return null;
    }

    public PacketReplayEntries.PacketReplayEntry getSavedReplayEntryByGameID(String gameId) {
        for (PacketReplayEntries.PacketReplayEntry entry : this.getSavedReplays()) {
            if (!entry.getGameId().equals(gameId)) continue;
            return entry;
        }

        return null;
    }
}