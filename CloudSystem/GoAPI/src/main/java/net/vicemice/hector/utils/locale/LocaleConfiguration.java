package net.vicemice.hector.utils.locale;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import lombok.AccessLevel;
import lombok.Getter;
import org.json.JSONObject;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class LocaleConfiguration {
    private File configFile;
    @Getter(AccessLevel.PUBLIC)
    private JSONObject jsonObject;
    private Gson gson;

    /**
     * Create a new Locale Configuration Instance
     * @param path defines the path and the configuration file
     */
    public LocaleConfiguration(String path) {
        this.configFile = new File(path);
        this.gson = new GsonBuilder()
                .disableHtmlEscaping()
                .setPrettyPrinting()
                .create();
    }

    /**
     * Check if the Config File exist
     * @return returns true if the file exist and false if not
     */
    public boolean exists() {
        return configFile.exists();
    }

    /**
     * Read the Config File and parse it to a Json Object
     */
    public void readConfiguration() {
        try (FileReader fileReader = new FileReader(configFile)) {
            JsonObject json = gson.fromJson(fileReader, JsonObject.class);
            jsonObject = new JSONObject(json.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}