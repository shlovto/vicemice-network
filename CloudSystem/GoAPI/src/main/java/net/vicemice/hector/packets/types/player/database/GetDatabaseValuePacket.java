package net.vicemice.hector.packets.types.player.database;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 13:07 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GetDatabaseValuePacket implements Serializable {
    @Getter
    private UUID uuid;
    @Getter
    private String database;
    @Getter
    private String playerKey;
    @Getter
    private String key;
    @Getter
    private String callbackID;

    public GetDatabaseValuePacket(UUID uuid, String database, String playerKey, String key, String callbackID) {
        this.uuid = uuid;
        this.database = database;
        this.playerKey = playerKey;
        this.key = key;
        this.callbackID = callbackID;
    }
}