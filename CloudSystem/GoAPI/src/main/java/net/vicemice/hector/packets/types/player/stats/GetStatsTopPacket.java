package net.vicemice.hector.packets.types.player.stats;

import lombok.Getter;
import java.io.Serializable;

/*
 * Class created at 13:08 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GetStatsTopPacket implements Serializable {

    @Getter
    private String prefix;
    @Getter
    private String callBackID;
    @Getter
    private StatsType statsType;
    @Getter
    private int size;

    public GetStatsTopPacket(String prefix, StatsType statsType, String callBackID, int size) {
        this.prefix = prefix;
        this.statsType = statsType;
        this.callBackID = callBackID;
        this.size = size;
    }

    public static enum StatsType {
        DAILY,
        MONTHLY,
        GLOBAL
    }
}