package net.vicemice.hector.packets.types.player.rank;

import lombok.Getter;
import net.vicemice.hector.utils.Rank;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 09:45 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerShopRankPacket implements Serializable {

    @Getter
    private UUID uniqueId;
    @Getter
    private String timeUnit;
    @Getter
    private Rank rank;
    @Getter
    private long end;

    public PlayerShopRankPacket(UUID uniqueId, String timeUnit, Rank rank, long end) {
        this.uniqueId = uniqueId;
        this.timeUnit = timeUnit;
        this.rank = rank;
        this.end = end;
    }
}
