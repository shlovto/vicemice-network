package net.vicemice.hector.utils.players.api.particles.exception;

/*
 * Class created at 10:47 - 02.04.2020
 * Copyright (C) elrobtossohn
 */
public class RemapperUnavailableException extends RuntimeException {

    public RemapperUnavailableException(Reason reason) {
        super(reason.getMessage());
    }

    public RemapperUnavailableException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public enum Reason {
        CAULDRON_NOT_PRESET("Cauldron/MCPC+ is not present!"),
        REMAPPER_DISABLED("Cauldron/MCPC+ detected, but the remapper is disabled!");

        private String message;

        Reason(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }
}