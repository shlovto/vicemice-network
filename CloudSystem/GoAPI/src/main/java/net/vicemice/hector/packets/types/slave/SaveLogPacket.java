package net.vicemice.hector.packets.types.slave;

import java.io.Serializable;

/*
 * Class created at 18:36 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SaveLogPacket implements Serializable {
    private String serverName;

    public SaveLogPacket(String serverName) {
        this.serverName = serverName;
    }

    public String getServerName() {
        return this.serverName;
    }
}
