package net.vicemice.hector.packets.types.proxy;

import lombok.Getter;

import java.io.Serializable;

/*
 * Class created at 18:22 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class OnlineCountPacket implements Serializable {

    @Getter
    private int online;

    public OnlineCountPacket(int online) {
        this.online = online;
    }
}