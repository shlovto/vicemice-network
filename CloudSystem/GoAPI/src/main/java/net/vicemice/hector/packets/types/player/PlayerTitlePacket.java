package net.vicemice.hector.packets.types.player;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 10:08 - 22.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerTitlePacket implements Serializable {
    private UUID uniqueId;
    private String title;
    private String subtitle;
    private int fadeInTime = -1;
    private int stayTime = -1;
    private int fadeOutTime = -1;

    public PlayerTitlePacket(UUID uniqueId, String title, String subtitle) {
        this.uniqueId = uniqueId;
        this.title = title;
        this.subtitle = subtitle;
        this.fadeInTime = 0;
        this.stayTime = 20;
        this.fadeOutTime = 0;
    }

    public PlayerTitlePacket(UUID uniqueId, String title, String subtitle, int fadeInTime, int stayTime, int fadeOutTime) {
        this.uniqueId = uniqueId;
        this.title = title;
        this.subtitle = subtitle;
        this.fadeInTime = fadeInTime;
        this.stayTime = stayTime;
        this.fadeOutTime = fadeOutTime;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public String getTitle() {
        return this.title;
    }

    public String getSubtitle() {
        return this.subtitle;
    }

    public int getFadeInTime() {
        return fadeInTime;
    }

    public int getStayTime() {
        return stayTime;
    }

    public int getFadeOutTime() {
        return fadeOutTime;
    }
}
