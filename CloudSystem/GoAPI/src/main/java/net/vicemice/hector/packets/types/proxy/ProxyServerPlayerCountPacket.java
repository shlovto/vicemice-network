package net.vicemice.hector.packets.types.proxy;

import lombok.Getter;

import java.io.Serializable;

/*
 * Class created at 13:05 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ProxyServerPlayerCountPacket implements Serializable {

    @Getter
    private String serverName;
    @Getter
    private String bungeeName;
    @Getter
    private int size;

    public ProxyServerPlayerCountPacket(String serverName, String bungeeName, int size) {
        this.serverName = serverName;
        this.bungeeName = bungeeName;
        this.size = size;
    }
}