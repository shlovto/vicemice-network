package net.vicemice.hector.api.util;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.WebServiceClient;
import com.maxmind.geoip2.model.AsnResponse;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Country;

import java.io.File;
import java.net.InetAddress;

public class GeoIP2API {

    public static WebServiceClient getWebClient() {
        try (WebServiceClient client = new WebServiceClient.Builder(257593, "jJJYocaCZsgqG4r0").build()) {
            return client;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static Country getCountry(String ip) {
        try {
            File database = new File("/usr/share/GeoIP/GeoLite2-City.mmdb");

            // This creates the DatabaseReader object. To improve performance, reuse
            // the object across lookups. The object is thread-safe.
            DatabaseReader reader = new DatabaseReader.Builder(database).build();

            InetAddress ipAddress = InetAddress.getByName(ip);

            // Replace "city" with the appropriate method for your database, e.g.,
            // "country".
            CityResponse response = reader.city(ipAddress);

            return response.getCountry();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static AsnResponse getASN(String ip) {
        // A File object pointing to your GeoLite2 ASN database
        File database = new File("/usr/share/GeoIP/GeoLite2-ASN.mmdb");

        // This creates the DatabaseReader object. To improve performance, reuse
        // the object across lookups. The object is thread-safe.
        try (DatabaseReader reader = new DatabaseReader.Builder(database).build()) {

            InetAddress ipAddress = InetAddress.getByName(ip);

            return reader.asn(ipAddress);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static CityResponse getCity(String ip) {
        // A File object pointing to your GeoLite2 ASN database
        File database = new File("/usr/share/GeoIP/GeoLite2-City.mmdb");

        // This creates the DatabaseReader object. To improve performance, reuse
        // the object across lookups. The object is thread-safe.
        try (DatabaseReader reader = new DatabaseReader.Builder(database).build()) {

            InetAddress ipAddress = InetAddress.getByName(ip);

            return reader.city(ipAddress);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
