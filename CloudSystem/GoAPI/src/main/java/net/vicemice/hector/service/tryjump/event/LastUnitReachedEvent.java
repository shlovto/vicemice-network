package net.vicemice.hector.service.tryjump.event;

import net.vicemice.hector.service.tryjump.TryJumpPlayer;

/*
 * Class created at 01:43 - 10.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 * <p>Event gets fired when a player reaches the last checkpoint of the jump phase.
 */
public class LastUnitReachedEvent extends TryJumpEvent {

    /**
     * Token boost the player receives when completing the jump phase with 0 fails. Zero tokens when
     * finished with >= 1 fails.
     */
    public int getTokenBoost() {
        return this.tokenBoost;
    }

    private int tokenBoost;

    /**
     * @param player the {@link TryJumpPlayer} who finished the jump phase.
     * @param tokenBoost token boost the player receives when completing the jump phase with 0 fails.
     *     Zero tokens when finished with >= 1 fails.
     */
    public LastUnitReachedEvent(TryJumpPlayer player, int tokenBoost) {
        super(player);
        this.tokenBoost = tokenBoost;
    }
}
