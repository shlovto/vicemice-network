package net.vicemice.hector.utils.players.punish;

import lombok.Getter;
import java.io.Serializable;
import java.util.List;

@Getter
public class Punish implements Serializable {

    public Punish(String key, String type, int points, boolean viewAble, boolean useAble, boolean replayRequired, boolean chatLogRequired, int accessLevel, List<String> keyWords, List<String> waves) {
        this.key = key;
        this.type = type;
        this.points = points;
        this.viewAble = viewAble;
        this.useAble = useAble;
        this.replayRequired = replayRequired;
        this.chatLogRequired = chatLogRequired;
        this.accessLevel = accessLevel;
        this.keyWords = keyWords;
        this.waves = waves;
    }

    private String key, type;
    private int points, accessLevel;
    private boolean viewAble, useAble, replayRequired, chatLogRequired;
    private List<String> keyWords;
    private List<String> waves;
}
