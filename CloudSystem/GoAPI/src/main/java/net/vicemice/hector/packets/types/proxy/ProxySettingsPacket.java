package net.vicemice.hector.packets.types.proxy;

import lombok.Getter;

import java.io.Serializable;

/*
 * Class created at 13:05 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ProxySettingsPacket implements Serializable {

    @Getter
    private String motd;
    @Getter
    private String maintenanceMOTD;
    @Getter
    private String blackListed;
    @Getter
    private int maxPlayers;

    public ProxySettingsPacket(String motd, int maxPlayers, String maintenanceMOTD, String blackListed) {
        this.motd = motd;
        this.maxPlayers = maxPlayers;
        this.maintenanceMOTD = maintenanceMOTD;
        this.blackListed = blackListed;
    }
}