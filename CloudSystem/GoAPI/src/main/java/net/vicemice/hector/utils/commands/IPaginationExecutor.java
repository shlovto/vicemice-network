package net.vicemice.hector.utils.commands;

/*
 * Class created at 01:02 - 14.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public interface IPaginationExecutor<T> {

    void print(T element);
}