package net.vicemice.hector.utils.scheduler;

import java.util.concurrent.*;

public class ScheduledExecution {
    private ScheduledExecutorService scheduledExecutorService;
    private ScheduledFuture scheduledFuture;

    public ScheduledExecution(Runnable runnable, Integer delay, Integer period, TimeUnit timeUnit) {
        this.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        this.scheduledFuture = this.scheduledExecutorService.scheduleAtFixedRate(runnable, delay, period, timeUnit);
    }

    public void shutdown() {
        if (this.scheduledFuture != null) {
            this.scheduledFuture.cancel(true);
        }
        if (this.scheduledExecutorService != null) {
            this.scheduledExecutorService.shutdown();
        }
    }
}