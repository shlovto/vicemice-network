package net.vicemice.hector.packets.types.proxy;

import lombok.Getter;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 13:02 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ProxyChatPacket implements Serializable {

    @Getter
    private UUID uniqueId;
    @Getter
    private String server;
    @Getter
    private String message;

    public ProxyChatPacket(UUID uniqueId, String server, String message) {
        this.uniqueId = uniqueId;
        this.server = server;
        this.message = message;
    }
}
