package net.vicemice.hector.packets.types.slave;

import java.io.Serializable;

/*
 * Class created at 18:39 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SlaveAlivePacket implements Serializable {
    private String name;
    private double cpuUsage;
    private double cpuAverage;
    private int usedMemory;
    private int freeMemory;
    private int totalMemory;

    public SlaveAlivePacket(String name, double cpuUsage, int usedMemory, int freeMemory, int totalMemory, double cpuAverage) {
        this.name = name;
        this.cpuUsage = cpuUsage;
        this.usedMemory = usedMemory;
        this.freeMemory = freeMemory;
        this.totalMemory = totalMemory;
        this.cpuAverage = cpuAverage;
    }

    public String getName() {
        return this.name;
    }

    public double getCpuUsage() {
        return this.cpuUsage;
    }

    public double getCpuAverage() {
        return this.cpuAverage;
    }

    public int getUsedMemory() {
        return this.usedMemory;
    }

    public int getFreeMemory() {
        return this.freeMemory;
    }

    public int getTotalMemory() {
        return this.totalMemory;
    }
}
