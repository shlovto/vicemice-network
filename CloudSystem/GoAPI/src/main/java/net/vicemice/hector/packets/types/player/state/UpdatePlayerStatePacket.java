package net.vicemice.hector.packets.types.player.state;

import lombok.Getter;
import net.vicemice.hector.utils.players.utils.PlayerState;

import java.io.Serializable;
import java.util.UUID;

public class UpdatePlayerStatePacket implements Serializable {
    @Getter
    private UUID uniqueId;
    @Getter
    private PlayerState playerState;

    public UpdatePlayerStatePacket(UUID uniqueId, PlayerState playerState) {
        this.uniqueId = uniqueId;
        this.playerState = playerState;
    }
}