package net.vicemice.hector.utils;

/*
 * Class created at 18:34 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public enum PunishType {
    BAN,
    UNBAN,
    MUTE,
    UNMUTE,
    KICK;
}