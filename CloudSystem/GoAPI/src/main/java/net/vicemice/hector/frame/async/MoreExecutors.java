package net.vicemice.hector.frame.async;

import com.google.common.util.concurrent.ListeningExecutorService;
import net.vicemice.hector.frame.async.executor.SpigotSyncExecutor;
import org.bukkit.Bukkit;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MoreExecutors {
    private static final Executor directExecutor = new Executor(){

        @Override
        public void execute(Runnable command) {
            command.run();
        }
    };
    private static final Executor spigotSyncExecutor = new SpigotSyncExecutor(Bukkit.getPluginManager().getPlugin("Hector"));
    private static final ListeningExecutorService asyncExecutor = com.google.common.util.concurrent.MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());

    public static Executor directExecutor() {
        return directExecutor;
    }

    public static Executor spigotSyncExecutor() {
        return spigotSyncExecutor;
    }

    public static ListeningExecutorService asyncExecutor() {
        return asyncExecutor;
    }

    private MoreExecutors() {
    }
}
