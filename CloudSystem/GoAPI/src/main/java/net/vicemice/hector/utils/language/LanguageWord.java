package net.vicemice.hector.utils.language;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/*
 * Class created at 12:02 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class LanguageWord {
    private String key;
    private Map<Locale, String> versions = new HashMap<>();

    public LanguageWord(String key) {
        this.key = key;
    }

    public void addWord(String word, Locale locale) {
        this.versions.put(locale, word);
    }

    public String getWord(String key, Locale locale) {
        if (this.versions.containsKey(locale)) {
            return this.versions.get(locale);
        }
        return key;
    }

    public String getKey() {
        return this.key;
    }
}