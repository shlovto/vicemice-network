package net.vicemice.hector.api.protocolversion.protocols.protocol1_14_3to1_14_2;

import net.vicemice.hector.api.protocolversion.api.PacketWrapper;
import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.api.protocol.Protocol;
import net.vicemice.hector.api.protocolversion.api.remapper.PacketHandler;
import net.vicemice.hector.api.protocolversion.api.remapper.PacketRemapper;
import net.vicemice.hector.api.protocolversion.api.type.Type;
import net.vicemice.hector.api.protocolversion.packets.State;

public class Protocol1_14_3To1_14_2 extends Protocol {

    @Override
    protected void registerPackets() {
        // Trade list
        registerOutgoing(State.PLAY, 0x27, 0x27, new PacketRemapper() {
            @Override
            public void registerMap() {
                handler(new PacketHandler() {
                    @Override
                    public void handle(PacketWrapper wrapper) throws Exception {
                        wrapper.passthrough(Type.VAR_INT);
                        int size = wrapper.passthrough(Type.UNSIGNED_BYTE);
                        for (int i = 0; i < size; i++) {
                            wrapper.passthrough(Type.FLAT_VAR_INT_ITEM);
                            wrapper.passthrough(Type.FLAT_VAR_INT_ITEM);
                            if (wrapper.passthrough(Type.BOOLEAN)) {
                                wrapper.passthrough(Type.FLAT_VAR_INT_ITEM);
                            }
                            wrapper.passthrough(Type.BOOLEAN);
                            wrapper.passthrough(Type.INT);
                            wrapper.passthrough(Type.INT);
                            wrapper.passthrough(Type.INT);
                            wrapper.passthrough(Type.INT);
                            wrapper.passthrough(Type.FLOAT);
                        }
                        wrapper.passthrough(Type.VAR_INT);
                        wrapper.passthrough(Type.VAR_INT);
                        boolean regularVillager = wrapper.passthrough(Type.BOOLEAN);
                        wrapper.write(Type.BOOLEAN, regularVillager); // new boolean added in pre-1
                    }
                });
            }
        });
    }

    @Override
    public void init(UserConnection userConnection) {
    }
}
