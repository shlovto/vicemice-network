package net.vicemice.hector.packets.types.server;

import java.io.Serializable;
import java.util.List;

/*
 * Class created at 18:38 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class StatSignPacket implements Serializable {
    private List<String[]> signDatas;

    public StatSignPacket(List<String[]> signDatas) {
        this.signDatas = signDatas;
    }

    public List<String[]> getSignDatas() {
        return this.signDatas;
    }
}
