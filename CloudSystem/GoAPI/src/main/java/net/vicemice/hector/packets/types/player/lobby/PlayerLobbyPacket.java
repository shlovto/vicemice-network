package net.vicemice.hector.packets.types.player.lobby;

import java.io.Serializable;

/*
 * Class created at 18:27 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerLobbyPacket implements Serializable {
    private String playerName;

    public PlayerLobbyPacket(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerName() {
        return this.playerName;
    }
}
