package net.vicemice.hector.utils.players.api.internal.fakemobs.util;

import java.util.List;
import net.vicemice.hector.utils.players.api.internal.fakemobs.FakeMobs;
import org.bukkit.entity.Player;

public class LookUpdate implements Runnable {
	
	@Override
	public void run() {
		try {
			for (FakeMob mob : FakeMobs.instance.getMobs()) {
				if (!mob.isPlayerLook()) continue;
				List<Player> players = mob.getNearbyPlayers(5D);
				for (Player p : players)
					mob.sendLookPacket(p, p.getLocation());
			}
		} catch (Exception e) {
			//Do Nothing
		}
	}
}