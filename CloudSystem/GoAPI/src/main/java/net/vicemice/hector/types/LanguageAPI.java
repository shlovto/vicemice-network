package net.vicemice.hector.types;

import net.vicemice.hector.utils.language.Language;

import java.util.UUID;

/*
 * Class created at 06:04 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public interface LanguageAPI {

    /**
     * @param uniqueId defines the unique id of an player
     * @return returns the Language from an Player
     */
    public Language.LanguageType getLanguage(UUID uniqueId);
}