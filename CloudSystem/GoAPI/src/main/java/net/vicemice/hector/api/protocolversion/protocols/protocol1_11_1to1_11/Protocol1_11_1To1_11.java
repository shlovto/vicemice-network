package net.vicemice.hector.api.protocolversion.protocols.protocol1_11_1to1_11;

import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.api.protocol.Protocol;

public class Protocol1_11_1To1_11 extends Protocol {
    @Override
    protected void registerPackets() {
        // Only had metadata changes, see wiki.vg for full info.
    }

    @Override
    public void init(UserConnection userConnection) {

    }
}
