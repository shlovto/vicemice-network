package net.vicemice.hector.packets.types.player.user;

import lombok.Getter;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 23:45 - 02.05.2020
 * Copyright (C) elrobtossohn
 */
@Getter
public class RequestUserDataPacket implements Serializable {
    private final UUID uniqueId;
    private final String serverId;

    public RequestUserDataPacket(UUID uniqueId, String serverId) {
        this.uniqueId = uniqueId;
        this.serverId = serverId;
    }
}