package net.vicemice.hector.utils.game.utils;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.UUID;

public final class UUIDUtil {

    private static final String PROFILE_URL = "https://api.mojang.com/users/profiles/minecraft/";

    private UUIDUtil() {
    }

    public static UUID fromFlatString(String str) {
        if (str.length() == 36) {
            return UUID.fromString(str);
        }
        return UUID.fromString(str.substring(0, 8) + "-" + str.substring(8, 12) + "-" + str.substring(12, 16) + "-" + str.substring(16, 20) + "-" + str.substring(20, 32));
    }

    public static String toFlatString(UUID uuid) {
        return UUIDUtil.toFlatString(uuid.toString());
    }

    public static String toFlatString(String uuid) {
        return uuid.replace("-", "");
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    public static UUID fetchUuid(String username) {
        try {
            URLConnection connection = new URL(PROFILE_URL + username).openConnection();
            try (InputStream is = connection.getInputStream();){
                JSONObject json = (JSONObject)new JSONParser().parse(new InputStreamReader(is));
                UUID uUID = UUIDUtil.fromFlatString((String)json.get("id"));
                return uUID;
            }
        }
        catch (Throwable e) {
            System.out.println("Failed to fetch UUID for " + username);
            e.printStackTrace();
            return null;
        }
    }
}
