package net.vicemice.hector.api.inventory.api.option;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.api.inventory.api.InventoryMenu;
import net.vicemice.hector.api.inventory.api.LocalizedInventoryMenu;
import net.vicemice.hector.api.util.BukkitSchedulerHelper;
import org.bukkit.entity.Player;

public class SubmenuOption extends ActionOption {
    private InventoryMenu submenu;
    private LocalizedInventoryMenu localizedSubmenu;

    public SubmenuOption(InventoryMenu submenu) {
        super(null);
        this.submenu = submenu;
        this.submenu.setRoot(false);
    }

    public SubmenuOption(LocalizedInventoryMenu submenu) {
        super(null);
        this.localizedSubmenu = submenu;
    }

    @Override
    public void onClick(final Player player) {
        super.onClick(player);
        BukkitSchedulerHelper.executeDelayed(1L, new Runnable(){

            @Override
            public void run() {
                if (player.isOnline()) {
                    if (SubmenuOption.this.submenu != null) {
                        SubmenuOption.this.submenu.open(player);
                    } else {
                        SubmenuOption.this.localizedSubmenu.open(player, GoAPI.getUserAPI().getLocale(player.getUniqueId()));
                    }
                }
            }
        });
    }

    public InventoryMenu getSubmenu() {
        return this.submenu;
    }

    public SubmenuOption setSubmenu(InventoryMenu submenu) {
        this.submenu = submenu;
        return this;
    }

    public LocalizedInventoryMenu getLocalizedSubmenu() {
        return this.localizedSubmenu;
    }

    public SubmenuOption setLocalizedSubmenu(LocalizedInventoryMenu localizedSubmenu) {
        this.localizedSubmenu = localizedSubmenu;
        return this;
    }
}