package net.vicemice.hector.api.inventory.api;

import lombok.NonNull;
import net.vicemice.hector.api.async.Callback;
import net.vicemice.hector.api.inventory.api.option.NoActionOption;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.*;

public class InventoryMenu {
    private static InventoryListener listener;
    private static final HashMap<UUID, InventoryMenu> currentInventories;
    private boolean root = true;
    private final boolean saveForever;
    private String name;
    private int size;
    private final ArrayList<InventoryOption> inventoryOptions = new ArrayList<>();
    private Inventory inventory;
    private InventoryMenu source = null;
    private boolean cancelOnNoItem = true;
    private boolean autoRecreate = true;
    private Callback<Void> onCloseCallback;

    public InventoryMenu() {
        this("Inventory");
    }

    public InventoryMenu(int size) {
        this("Inventory", size);
    }

    public InventoryMenu(String name) {
        this(name, 9);
    }

    public InventoryMenu(String name, int size) {
        this(name, size, false);
    }

    public InventoryMenu(String name, int size, boolean saveForever) {
        this(name, size, saveForever, Bukkit.getPluginManager().getPlugin("GoCore"));
    }

    public InventoryMenu(String name, int size, boolean saveForever, Plugin plugin) {
        this.name = name;
        this.size = size;
        this.saveForever = saveForever;
        this.recreateInventory();
        if (listener == null) {
            listener = new InventoryListener();
            Bukkit.getPluginManager().registerEvents(listener, plugin);
        }
    }

    public InventoryMenu addOption(@NonNull InventoryOption option) {
        if (option == null) {
            throw new NullPointerException("option");
        }
        this.inventoryOptions.add(option);
        if (this.autoRecreate) {
            this.recreateInventory();
        }
        return this;
    }

    public InventoryMenu removeOption(@NonNull InventoryOption option) {
        if (option == null) {
            throw new NullPointerException("option");
        }
        this.inventoryOptions.remove(option);
        if (this.autoRecreate) {
            this.recreateInventory();
        }
        return this;
    }

    public InventoryMenu recreateInventory() {
        this.inventory = Bukkit.createInventory(null, this.size, this.name);
        for (InventoryOption itemOption : this.inventoryOptions) {
            if (itemOption.getPosition() >= this.size) {
                System.err.println("Cannot create inventory: size: " + this.size + " position: " + itemOption.getPosition());
                continue;
            }
            this.inventory.setItem(itemOption.getPosition(), itemOption.getItemStack());
        }
        return this;
    }

    public InventoryMenu addItem(List<ItemStack> itemStacks) {
        for (ItemStack next : itemStacks) {
            int freeslot = -1;
            for (int i = 0; i < this.inventory.getSize(); ++i) {
                if (this.inventory.getItem(i) != null && this.inventory.getItem(i).getType() != Material.AIR) continue;
                freeslot = i;
                break;
            }
            this.addOption(new NoActionOption().setItemStack(next).setPosition(freeslot));
        }
        return this;
    }

    public InventoryMenu addItem(ItemStack ... itemStacks) {
        return this.addItem(Arrays.asList(itemStacks));
    }

    public InventoryMenu addItem(InventoryOption ... options) {
        for (InventoryOption next : Arrays.asList(options)) {
            int freeslot = -1;
            for (int i = 0; i < this.inventory.getSize(); ++i) {
                if (this.inventory.getItem(i) != null && this.inventory.getItem(i).getType() != Material.AIR) continue;
                freeslot = i;
                break;
            }
            this.addOption(next.setPosition(freeslot));
        }
        return this;
    }

    public InventoryMenu open(Player player) {
        currentInventories.put(player.getUniqueId(), this);
        player.openInventory(this.inventory);
        return this;
    }

    public InventoryMenu onClose(Callback<Void> callback) {
        this.onCloseCallback = callback;
        return this;
    }

    @EventHandler
    public void onInventoryClose(final InventoryCloseEvent event) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("GoCore"), new Runnable(){

            @Override
            public void run() {
                if (event.getPlayer() instanceof Player) {
                    Player player = (Player)event.getPlayer();
                    InventoryMenu currentRootInventory = currentInventories.get(player.getUniqueId());
                    if (player.getOpenInventory().getType() == InventoryType.CRAFTING && currentRootInventory != null && !currentRootInventory.isSaveForever()) {
                        if (currentRootInventory.getOnCloseCallback() != null) {
                            currentRootInventory.getOnCloseCallback().done(null);
                        }
                        currentInventories.remove(player.getUniqueId());
                    }
                }
            }
        }, 1L);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        currentInventories.remove(event.getPlayer().getUniqueId());
    }

    public static HashMap<UUID, InventoryMenu> getCurrentInventories() {
        return currentInventories;
    }

    public boolean isRoot() {
        return this.root;
    }

    public void setRoot(boolean root) {
        this.root = root;
    }

    public boolean isSaveForever() {
        return this.saveForever;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return this.size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public ArrayList<InventoryOption> getInventoryOptions() {
        return this.inventoryOptions;
    }

    public Inventory getInventory() {
        return this.inventory;
    }

    public InventoryMenu getSource() {
        return this.source;
    }

    public void setSource(InventoryMenu source) {
        this.source = source;
    }

    public boolean isCancelOnNoItem() {
        return this.cancelOnNoItem;
    }

    public void setCancelOnNoItem(boolean cancelOnNoItem) {
        this.cancelOnNoItem = cancelOnNoItem;
    }

    public boolean isAutoRecreate() {
        return this.autoRecreate;
    }

    public void setAutoRecreate(boolean autoRecreate) {
        this.autoRecreate = autoRecreate;
    }

    public Callback<Void> getOnCloseCallback() {
        return this.onCloseCallback;
    }

    static {
        currentInventories = new HashMap();
    }

    private class InventoryListener
            implements Listener {
        private InventoryListener() {
        }

        @EventHandler
        public void onInventoryClick(InventoryClickEvent event) {
            if (!(event.getWhoClicked() instanceof Player)) {
                return;
            }
            Player player = (Player)event.getWhoClicked();
            if (currentInventories.containsKey(player.getUniqueId())) {
                InventoryMenu menu = currentInventories.get(player.getUniqueId());
                if (event.getInventory().equals(menu.getInventory())) {
                    ItemStack clickedItem = event.getCurrentItem();
                    if (clickedItem == null) {
                        return;
                    }
                    boolean optionClicked = false;
                    for (InventoryOption option : new ArrayList<>(menu.getInventoryOptions())) {
                        if (!option.getItemStack().equals(clickedItem) || option.getPosition() != event.getSlot()) continue;
                        optionClicked = true;
                        option.onClick(player);
                        if (!option.isCancelOnClick()) continue;
                        event.setCancelled(true);
                    }
                    if (!optionClicked && menu.isCancelOnNoItem()) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }
}