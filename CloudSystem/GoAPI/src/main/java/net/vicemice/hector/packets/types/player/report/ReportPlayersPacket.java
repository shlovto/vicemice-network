package net.vicemice.hector.packets.types.player.report;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/*
 * Class created at 08:58 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ReportPlayersPacket implements Serializable {
    private static final long serialVersionUID = 1;
    private ArrayList<DataReportEntry> reportData;

    @ConstructorProperties(value={"reportData"})
    public ReportPlayersPacket(ArrayList<DataReportEntry> reportData) {
        this.reportData = reportData;
    }

    public ArrayList<DataReportEntry> getReportData() {
        return this.reportData;
    }

    public static class DataReportEntry implements Serializable {
        private ReportEntry entry;
        private HashMap<String, String> prefixMapping;

        @ConstructorProperties(value={"entry", "prefixMapping"})
        public DataReportEntry(ReportEntry entry, HashMap<String, String> prefixMapping) {
            this.entry = entry;
            this.prefixMapping = prefixMapping;
        }

        public ReportEntry getEntry() {
            return this.entry;
        }

        public HashMap<String, String> getPrefixMapping() {
            return this.prefixMapping;
        }
    }

    public static class ReportEntry implements Serializable {
        private static final long serialVersionUID = 1;
        private String visitor = null;
        private UUID uniqueID;
        private ReportState state = ReportState.NOT_EDITED;
        private String suspect;
        private String suspectsCurrentServer;
        private ArrayList<AccusationsReason> accusations;
        private ArrayList<String> revisors;

        @ConstructorProperties(value={"suspect", "uuid", "suspectsCurrentServer", "accusations", "revisors"})
        public ReportEntry(String suspect, UUID uuid, String suspectsCurrentServer, ArrayList<AccusationsReason> accusations, ArrayList<String> revisors) {
            this.suspect = suspect;
            this.uniqueID = uuid;
            this.suspectsCurrentServer = suspectsCurrentServer;
            this.accusations = accusations;
            this.revisors = revisors;
        }

        public UUID getUniqueID() {
            return uniqueID;
        }

        public String getVisitor() {
            return visitor;
        }

        public void setVisitor(String visitor) {
            this.visitor = visitor;
        }

        public ReportState getState() {
            return state;
        }

        public void setState(ReportState state) {
            this.state = state;
        }

        public String getSuspect() {
            return this.suspect;
        }

        public String getSuspectsCurrentServer() {
            return this.suspectsCurrentServer;
        }

        public ArrayList<AccusationsReason> getAccusations() {
            return this.accusations;
        }

        public ArrayList<String> getRevisors() {
            return this.revisors;
        }

        public void setSuspectsCurrentServer(String suspectsCurrentServer) {
            this.suspectsCurrentServer = suspectsCurrentServer;
        }
    }

    public static class AccusationsReason implements Serializable {
        private static final long serialVersionUID = 5316640100847161643L;
        private String name;
        private String reason;
        private String server;
        private String replayId;
        private String chatLogId;

        @ConstructorProperties(value={"name", "reason", "server", "replayId", "chatLogId"})
        public AccusationsReason(String name, String reason, String server, String replayId, String chatLogId) {
            this.name = name;
            this.reason = reason;
            this.server = server;
            this.replayId = replayId;
            this.chatLogId = chatLogId;
        }

        public String getName() {
            return this.name;
        }

        public String getReason() {
            return this.reason;
        }

        public String getServer() {
            return this.server;
        }

        public String getReplayId() {
            return this.replayId;
        }

        public String getChatLogId() {
            return chatLogId;
        }
    }

    public static enum ReportState {
        NOT_EDITED,
        IN_PROCESS,
        EDITED;
    }
}
