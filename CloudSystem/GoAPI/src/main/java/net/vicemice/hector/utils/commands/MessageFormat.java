package net.vicemice.hector.utils.commands;

import lombok.Getter;
import net.vicemice.hector.utils.language.Language;

/*
 * Class created at 00:59 - 14.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class MessageFormat {

    @Getter
    private String syntaxMessage, syntaxGerMessage, overviewMessage, notFoundMessage, notGerFoundMessage;

    public MessageFormat overview(String overviewMessage) {
        this.overviewMessage = overviewMessage;
        return this;
    }

    public MessageFormat syntax(Language.LanguageType languageType, String syntaxMessage) {
        if (languageType == Language.LanguageType.GERMAN) {
            this.syntaxGerMessage = syntaxMessage;
        } else {
            this.syntaxMessage = syntaxMessage;
        }
        return this;
    }

    public MessageFormat notFound(Language.LanguageType languageType, String notFoundMessage) {
        if (languageType == Language.LanguageType.GERMAN) {
            this.notGerFoundMessage = notFoundMessage;
        } else {
            this.notFoundMessage = notFoundMessage;
        }
        return this;
    }

    public String getSyntaxMessage(Language.LanguageType languageType, String command, String arg) {
        return new MessageBuilder(languageType == Language.LanguageType.GERMAN ? this.syntaxGerMessage : this.syntaxMessage).
                set("command", command)
                .set("args", arg)
                .build();
    }

    public String getOverviewMessage(String command, String arg, String desc) {
        return new MessageBuilder(this.overviewMessage).
                set("command", command)
                .set("args", arg)
                .set("desc", desc)
                .build();
    }
}