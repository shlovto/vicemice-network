package net.vicemice.hector.packets.types.player.stats;

import net.vicemice.hector.utils.StatisticPeriod;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.HashMap;
import java.util.UUID;

/*
 * Class created at 00:37 - 04.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class StatisticPlayerInfoPacket implements Serializable {
    private UUID uniqueId;
    private HashMap<String, HashMap<StatisticPeriod, Long>> info = new HashMap<>();
    private HashMap<String, HashMap<StatisticPeriod, Long>> ranking = new HashMap<>();

    @ConstructorProperties(value={"name", "info", "ranking"})
    public StatisticPlayerInfoPacket(UUID uniqueId, HashMap<String, HashMap<StatisticPeriod, Long>> info, HashMap<String, HashMap<StatisticPeriod, Long>> ranking) {
        this.uniqueId = uniqueId;
        this.info = info;
        this.ranking = ranking;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public HashMap<String, HashMap<StatisticPeriod, Long>> getInfo() {
        return this.info;
    }

    public HashMap<String, HashMap<StatisticPeriod, Long>> getRanking() {
        return this.ranking;
    }

    public String toString() {
        return "StatisticPlayerInfoPacket(uniqueId=" + this.getUniqueId() + ", info=" + this.getInfo() + ", ranking=" + this.getRanking() + ")";
    }
}
