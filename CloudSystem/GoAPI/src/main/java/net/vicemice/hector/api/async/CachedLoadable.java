package net.vicemice.hector.api.async;

public abstract class CachedLoadable<T> implements Loadable<T> {
    private Future<T> future;

    @Override
    public synchronized Future<T> load() {
        if (this.future == null) {
            this.future = this.load0();
        }
        return this.future;
    }

    protected abstract Future<T> load0();
}