package net.vicemice.hector.utils;

import lombok.Getter;
import net.vicemice.hector.utils.locale.LocaleManager;

import java.text.SimpleDateFormat;
import java.util.*;

public class TimeManager {

    public String getTime(String format, long millis) {
        if (millis < 1) {
            return "-1 Error";
        }
        try {
            Date date = new Date(millis);
            return new SimpleDateFormat(format).format(date);
        } catch (Exception ex) {
            System.out.println("Could not create a valid date");
            System.out.println("Milliseconds: " + millis);
            System.out.println(Arrays.deepToString(ex.getStackTrace()));
            return "31.12."+this.getYear()+" - 23:59:59";
        }
    }

    public String getYear() {
        Date date = new Date(System.currentTimeMillis());
        return new SimpleDateFormat("yyyy").format(date);
    }

    public String getHour() {
        Date date = new Date(System.currentTimeMillis());
        return new SimpleDateFormat("HH").format(date);
    }

    public String get(DateTime dateTime) {
        Date date = new Date(System.currentTimeMillis());
        return new SimpleDateFormat(dateTime.getPattern()).format(date);
    }

    public enum DateTime {
        SECOND("ss"),
        MINUTE("mm"),
        HOUR("HH"),
        DAY("dd"),
        WEEK("WW"),
        MONTH("MM"),
        YEAR("yyyy");

        private DateTime(String pattern) {
            this.pattern = pattern;
        }

        @Getter
        protected final String pattern;
    }

    public Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }

    public String getRemainingTimeString(LocaleManager localeManager, Locale locale, long endTime) {
        String[] end = getRemainingTime(endTime);
        StringBuilder stringBuilder = new StringBuilder();
        int days = Integer.parseInt(end[0]);
        int hours = Integer.parseInt(end[1]);
        int minutes = Integer.parseInt(end[2]);
        int seconds = Integer.parseInt(end[3]);
        if (days != 0) {
            stringBuilder.append((days==1?days+" "+localeManager.getMessage(locale, "DAY")+" ":days+" "+localeManager.getMessage(locale, "DAYS")+" "));
        }
        if (hours != 0) {
            stringBuilder.append((hours==1?hours+" "+localeManager.getMessage(locale, "HOUR")+" ":hours+" "+localeManager.getMessage(locale, "HOURS")+" "));
        }
        if (minutes != 0) {
            stringBuilder.append((minutes==1?minutes+" "+localeManager.getMessage(locale, "MINUTE")+" ":minutes+" "+localeManager.getMessage(locale, "MINUTES")+" "));
        }
        if (seconds != 0) {
            stringBuilder.append((seconds==1?seconds+" "+localeManager.getMessage(locale, "SECOND"):seconds+" "+localeManager.getMessage(locale, "SECONDS")));
        }
        return stringBuilder.toString();
    }

    public String getRemainingTimeString1(LocaleManager localeManager, Locale locale, long endTime) {
        String[] end = this.getRemainingTime(endTime);
        int days = Integer.parseInt(end[0]);
        int hours = Integer.parseInt(end[1]);
        int minutes = Integer.parseInt(end[2]);
        int seconds = Integer.parseInt(end[0]);
        if (days != 0) {
            return days==1?days+" "+localeManager.getMessage(locale, "DAY"):days+" "+localeManager.getMessage(locale, "DAYS");
        }

        if (hours != 0) {
            return hours==1?hours+" "+localeManager.getMessage(locale, "HOUR"):hours+" "+localeManager.getMessage(locale, "HOURS");
        }

        if (minutes != 0) {
            return minutes==1?minutes+" "+localeManager.getMessage(locale, "MINUTE"):minutes+" "+localeManager.getMessage(locale, "MINUTES");
        }

        if (seconds != 0) {
            return seconds==1?seconds+" "+localeManager.getMessage(locale, "SECOND"):seconds+localeManager.getMessage(locale, "SECONDS");
        }

        return "0 "+localeManager.getMessage(locale, "SECONDS");
    }

    private String[] getRemainingTime(long milli) {
        long time = System.currentTimeMillis() > milli ? System.currentTimeMillis() - milli : milli - System.currentTimeMillis();
        int days = 0;
        int hours = 0;
        int minutes = 0;
        int seconds = 0;
        if ((time /= 1000) >= 86400) {
            days = (int) time / 86400;
        }
        if ((time -= (days * 86400)) > 3600) {
            hours = (int) time / 3600;
        }
        if ((time -= (hours * 3600)) > 60) {
            minutes = (int) time / 60;
        }
        seconds = (int) (time - (minutes * 60));
        return new String[]{Integer.toString(days), Integer.toString(hours), Integer.toString(minutes), Integer.toString(seconds)};
    }

    public String getTimeFromSeconds(LocaleManager localeManager, Locale locale, long longSeconds, ArrayList<TimeNames> needed) {
        long hours = longSeconds / 3600;
        long minutes = (longSeconds % 3600) / 60;
        long seconds = longSeconds % 60;

        if (hours != 0 && needed.contains(TimeNames.HOURS)) {
            return hours==1?hours+" "+localeManager.getMessage(locale, "HOUR"):hours+" "+localeManager.getMessage(locale, "HOURS");
        }

        if (minutes != 0 && needed.contains(TimeNames.MINUTES)) {
            return minutes==1?minutes+" "+localeManager.getMessage(locale, "MINUTE"):minutes+" "+localeManager.getMessage(locale, "MINUTES");
        }

        if (needed.contains(TimeNames.SECONDS)) {
            return seconds==1?seconds+" "+localeManager.getMessage(locale, "SECOND"):seconds+localeManager.getMessage(locale, "SECONDS");
        }

        return "0 "+localeManager.getMessage(locale, "SECONDS");
    }

    public enum TimeNames {
        SECONDS,
        MINUTES,
        HOURS,
        DAYS,
        WEEKS,
        MONTHS,
        YEARS;
    }
}