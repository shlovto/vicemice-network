package net.vicemice.hector.packets.types.player;

import lombok.Getter;

import java.io.Serializable;
import java.util.List;

/*
 * Class created at 18:32 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ComponentMessagePacket implements Serializable {

    @Getter
    private String name;
    @Getter
    private List<String[]> textComponentMessages;

    public ComponentMessagePacket(String name, List<String[]> textComponentMessages) {
        this.name = name;
        this.textComponentMessages = textComponentMessages;
    }
}
