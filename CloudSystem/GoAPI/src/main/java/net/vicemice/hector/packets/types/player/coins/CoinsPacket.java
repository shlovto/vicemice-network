package net.vicemice.hector.packets.types.player.coins;

import lombok.Getter;

import java.io.Serializable;

/*
 * Class created at 09:51 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class CoinsPacket implements Serializable {

    @Getter
    private String callBackID;
    @Getter
    private long coins;

    public CoinsPacket(String callBackID, long coins) {
        this.callBackID = callBackID;
        this.coins = coins;
    }
}
