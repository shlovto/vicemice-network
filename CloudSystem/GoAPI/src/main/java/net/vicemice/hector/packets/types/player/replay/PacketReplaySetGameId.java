package net.vicemice.hector.packets.types.player.replay;

import lombok.Getter;

import java.beans.ConstructorProperties;
import java.io.Serializable;

/*
 * Class created at 18:22 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PacketReplaySetGameId implements Serializable {

    @Getter
    private String player;
    @Getter
    private String gameID;

    @ConstructorProperties(value = {"player", "gameid"})
    public PacketReplaySetGameId(String player, String gameID) {
        this.player = player;
        this.gameID = gameID;
    }
}