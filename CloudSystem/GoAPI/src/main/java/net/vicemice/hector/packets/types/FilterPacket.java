package net.vicemice.hector.packets.types;

import lombok.Getter;

import java.io.Serializable;
import java.util.List;

/*
 * Class created at 08:51 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class FilterPacket implements Serializable {

    @Getter
    private List<String> entries;

    public FilterPacket(List<String> entries) {
        this.entries = entries;
    }
}
