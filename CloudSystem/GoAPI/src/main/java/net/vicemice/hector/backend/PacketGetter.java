package net.vicemice.hector.backend;

import io.netty.channel.Channel;
import net.vicemice.hector.packets.types.PacketHolder;

public abstract class PacketGetter {
    public abstract void receivePacket(PacketHolder var1);
    public abstract void channelActive(Channel var1);
}