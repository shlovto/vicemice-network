package net.vicemice.hector.api.protocolversion.protocols.protocol1_10to1_9_3.storage;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import net.vicemice.hector.api.protocolversion.api.data.StoredObject;
import net.vicemice.hector.api.protocolversion.api.data.UserConnection;

@Getter
@Setter
@ToString
public class ResourcePackTracker extends StoredObject {
    private String lastHash = "";

    public ResourcePackTracker(UserConnection user) {
        super(user);
    }
}
