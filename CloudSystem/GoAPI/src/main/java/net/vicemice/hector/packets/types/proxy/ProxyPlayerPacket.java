package net.vicemice.hector.packets.types.proxy;

import lombok.Getter;

import java.io.Serializable;

/*
 * Class created at 13:03 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ProxyPlayerPacket implements Serializable {

    @Getter
    private String serverName;
    @Getter
    private String proxyName;
    @Getter
    private int playerSize;

    public ProxyPlayerPacket(String serverName, String proxyName, int playerSize) {
        this.serverName = serverName;
        this.proxyName = proxyName;
        this.playerSize = playerSize;
    }
}