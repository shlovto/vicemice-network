package net.vicemice.hector.packets.types.slave;

import java.io.Serializable;

/*
 * Class created at 18:41 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SlaveServerKill implements Serializable {
    private String name;
    private boolean delete;

    public SlaveServerKill(String name, boolean delete) {
        this.name = name;
        this.delete = delete;
    }

    public String getName() {
        return this.name;
    }

    public boolean isDelete() {
        return delete;
    }
}
