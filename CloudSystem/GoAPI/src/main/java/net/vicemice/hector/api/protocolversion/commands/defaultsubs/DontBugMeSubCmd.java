package net.vicemice.hector.api.protocolversion.commands.defaultsubs;

import net.vicemice.hector.api.protocolversion.api.Via;
import net.vicemice.hector.api.protocolversion.api.command.ViaCommandSender;
import net.vicemice.hector.api.protocolversion.api.command.ViaSubCommand;
import net.vicemice.hector.api.protocolversion.api.configuration.ConfigurationProvider;

public class DontBugMeSubCmd extends ViaSubCommand {
    @Override
    public String name() {
        return "dontbugme";
    }

    @Override
    public String description() {
        return "Toggle checking for updates";
    }

    @Override
    public boolean execute(ViaCommandSender sender, String[] args) {
        ConfigurationProvider provider = Via.getPlatform().getConfigurationProvider();
        boolean newValue = !Via.getConfig().isCheckForUpdates();

        provider.set("checkforupdates", newValue);
        provider.saveConfig();
        sendMessage(sender, "&6We will %snotify you about updates.", (newValue ? "&a" : "&cnot "));

        return true;
    }
}
