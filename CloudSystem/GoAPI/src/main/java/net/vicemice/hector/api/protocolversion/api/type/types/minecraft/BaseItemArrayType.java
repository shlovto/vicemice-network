package net.vicemice.hector.api.protocolversion.api.type.types.minecraft;

import net.vicemice.hector.api.protocolversion.api.minecraft.item.Item;
import net.vicemice.hector.api.protocolversion.api.type.Type;

public abstract class BaseItemArrayType extends Type<Item[]> {
    public BaseItemArrayType() {
        super(Item[].class);
    }

    public BaseItemArrayType(String typeName) {
        super(typeName, Item[].class);
    }

    @Override
    public Class<? extends Type> getBaseClass() {
        return BaseItemArrayType.class;
    }
}
