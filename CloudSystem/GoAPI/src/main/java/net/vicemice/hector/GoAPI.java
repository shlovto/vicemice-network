package net.vicemice.hector;

import com.google.common.annotations.Beta;
import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.backend.GoEnum;
import net.vicemice.hector.packets.PacketHelper;
import net.vicemice.hector.service.server.NettyClient;
import net.vicemice.hector.utils.players.clan.Clan;
import net.vicemice.hector.utils.TimeManager;
import net.vicemice.hector.utils.api.forum.GoForumAPI;
import net.vicemice.hector.types.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/*
 * Class created at 05:46 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GoAPI {

    @Getter
    @Setter
    private static UserAPI userAPI;
    @Getter
    @Setter
    private static ServerAPI serverAPI;
    @Getter
    @Setter
    private static DatabaseAPI databaseAPI;
    @Getter
    @Setter
    private static GameIDAPI gameIDAPI;
    @Getter
    @Setter
    private static LanguageAPI languageAPI;
    @Getter
    @Setter
    private static ForumAPI forumAPI;
    @Getter
    private static GoEnum serverType = GoEnum.UNKNOWN;
    @Getter
    private static PacketHelper packetHelper;
    @Getter
    private static TimeManager timeManager;

    @Getter
    @Setter
    private NettyClient nettyClient;
    @Getter
    private HashMap<UUID, Clan> clan;
    @Getter
    private HashMap<String, List<String>> playerCommands;
    @Getter
    @Setter
    private UUID privateServerOwner;

    static {
        forumAPI = new GoForumAPI();
        packetHelper = new PacketHelper();
        timeManager = new TimeManager();
    }

    public GoAPI() {
        this.clan = new HashMap<>();
        this.playerCommands = new HashMap<>();
    }

    /**
     * Format a Object to a Decimal Format String e.q. 10000 to 10.000
     * @param locale can be null, defines the player locale
     * @param object must be a valid object
     * @return returns the formatted string
     */
    public static String formatDecimal(Locale locale, Object object) {
        if (locale == null) locale = Locale.GERMAN;

        DecimalFormat nf = new DecimalFormat();
        nf.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(locale));
        return nf.format(object);
    }

    /**
     * This Method is not stable for use!
     * Get an Image File in Binary Letters (0 and 1)
     * @param url defines the url where the picture is
     * @return returns a binoary string
     * @throws IOException if the url is invalid
     */
    @Beta
    public static String getImageData(String url) throws IOException {
        // read "any" type of image (in this case a png file)
        BufferedImage image = ImageIO.read(new URL(url));

        // write it to byte array in-memory (jpg format)
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", b);

        // do whatever with the array...
        byte[] jpgByteArray = b.toByteArray();

        // convert it to a String with 0s and 1s
        StringBuilder sb = new StringBuilder();
        for (byte by : jpgByteArray)
            sb.append(Integer.toBinaryString(by & 0xFF));

        return sb.toString();
    }
}