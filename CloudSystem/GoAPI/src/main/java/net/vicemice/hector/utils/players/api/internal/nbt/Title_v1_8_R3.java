package net.vicemice.hector.utils.players.api.internal.nbt;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.TileEntitySkull;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.util.UUID;

/*
 * Class created at 16:59 - 13.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class Title_v1_8_R3 {

    public void setHead(Block skull, String skinUrl) {
        TileEntitySkull tile = (TileEntitySkull) ((CraftWorld) skull.getWorld()).getHandle().getTileEntity(new BlockPosition(skull.getX(), skull.getY(), skull.getZ()));
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        profile.getProperties().put("textures", new Property("textures", Base64Coder.encodeString(("{textures:{SKIN:{url:" + skinUrl + " + skinUrl + " + skinUrl + "}}}"))));
        tile.setGameProfile(profile);
        skull.getWorld().refreshChunk(skull.getChunk().getX(), skull.getChunk().getZ());
    }
}