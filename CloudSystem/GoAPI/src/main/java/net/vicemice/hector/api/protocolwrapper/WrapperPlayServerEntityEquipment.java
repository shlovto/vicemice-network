/**
 * PacketWrapper - ProtocolLib wrappers for Minecraft packets
 * Copyright (C) dmulloy2 <http://dmulloy2.net>
 * Copyright (C) Kristian S. Strangeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.vicemice.hector.api.protocolwrapper;

import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;

import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.events.PacketContainer;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.wrappers.EnumWrappers.ItemSlot;

public class WrapperPlayServerEntityEquipment extends AbstractPacket {
	public static final PacketType TYPE =
			PacketType.Play.Server.ENTITY_EQUIPMENT;

	public WrapperPlayServerEntityEquipment() {
		super(new PacketContainer(TYPE), TYPE);
		handle.getModifier().writeDefaults();
	}

	public WrapperPlayServerEntityEquipment(PacketContainer packet) {
		super(packet, TYPE);
	}

	/**
	 * Retrieve Entity ID.
	 * <p>
	 * Notes: entity's ID
	 * 
	 * @return The current Entity ID
	 */
	public int getEntityID() {
		return handle.getIntegers().read(0);
	}

	/**
	 * Set Entity ID.
	 * 
	 * @param value - new value.
	 */
	public void setEntityID(int value) {
		handle.getIntegers().write(0, value);
	}

	/**
	 * Retrieve the entity of the painting that will be spawned.
	 * 
	 * @param world - the current world of the entity.
	 * @return The spawned entity.
	 */
	public Entity getEntity(World world) {
		return handle.getEntityModifier(world).read(0);
	}

	/**
	 * Retrieve the entity of the painting that will be spawned.
	 * 
	 * @param event - the packet event.
	 * @return The spawned entity.
	 */
	public Entity getEntity(PacketEvent event) {
		return getEntity(event.getPlayer().getWorld());
	}

	public int getSlotAsInt() {
		return handle.getIntegers().read(1);
	}

	public ItemSlot getSlot() {
		ItemSlot slot = null;
		int value = handle.getIntegers().read(1);
		if (value == 1) {
			slot = ItemSlot.HEAD;
		} else if (value == 2) {
			slot = ItemSlot.CHEST;
		} else if (value == 3) {
			slot = ItemSlot.LEGS;
		} else if (value == 4) {
			slot = ItemSlot.FEET;
		} else {
			slot = ItemSlot.MAINHAND;
		}

		return slot;
	}

	public void setSlot(ItemSlot slot) {
		int value = -1;
		if (slot == ItemSlot.HEAD) {
			value = 1;
		} else if (slot == ItemSlot.CHEST) {
			value = 2;
		} else if (slot == ItemSlot.LEGS) {
			value = 3;
		} else if (slot == ItemSlot.FEET) {
			value = 4;
		} else if (slot == ItemSlot.MAINHAND) {
			value = 0;
		}
		handle.getIntegers().write(1, value);
	}

	public void setSlot(int value) {
		handle.getIntegers().write(1, value);
	}

	/**
	 * Retrieve Item.
	 * slot
	 * Notes: item in slot format
	 * 
	 * @return The current Item
	 */
	public ItemStack getItem() {
		return handle.getItemModifier().read(0);
	}

	/**
	 * Set Item.
	 * 
	 * @param value - new value.
	 */
	public void setItem(ItemStack value) {
		handle.getItemModifier().write(0, value);
	}
}
