package net.vicemice.hector.packets.types.player.party;

import lombok.Getter;
import java.io.Serializable;

/*
 * Class created at 18:24 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PartyPacket implements Serializable {
    @Getter
    private String id;
    @Getter
    private Integer members;
    @Getter
    private Integer max;

    public PartyPacket(String id, Integer members, Integer max) {
        this.id = id;
        this.members = members;
        this.max = max;
    }
}