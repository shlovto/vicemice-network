package net.vicemice.hector.utils.language.exception;

public class LocaleNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 5162710183389021489L;

    /**
     * Constructs a {@code LocaleNotFoundException} with no detail message.
     */
    public LocaleNotFoundException() {
        super();
    }

    /**
     * Constructs a {@code LocaleNotFoundException} with the specified
     * detail message.
     *
     * @param s the detail message.
     */
    public LocaleNotFoundException(String s) {
        super(s);
    }
}
