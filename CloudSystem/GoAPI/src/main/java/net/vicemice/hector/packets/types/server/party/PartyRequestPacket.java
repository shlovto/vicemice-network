package net.vicemice.hector.packets.types.server.party;

import lombok.Getter;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:44 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PartyRequestPacket implements Serializable {

    @Getter
    private UUID uniqueId;

    @ConstructorProperties(value = {"uniqueId"})
    public PartyRequestPacket(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }
}
