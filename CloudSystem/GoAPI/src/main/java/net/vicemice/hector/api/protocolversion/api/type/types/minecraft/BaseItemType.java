package net.vicemice.hector.api.protocolversion.api.type.types.minecraft;

import net.vicemice.hector.api.protocolversion.api.minecraft.item.Item;
import net.vicemice.hector.api.protocolversion.api.type.Type;

public abstract class BaseItemType extends Type<Item> {
    public BaseItemType() {
        super(Item.class);
    }

    public BaseItemType(String typeName) {
        super(typeName, Item.class);
    }

    @Override
    public Class<? extends Type> getBaseClass() {
        return BaseItemType.class;
    }
}
