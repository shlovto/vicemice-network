/**
 * PacketWrapper - ProtocolLib wrappers for Minecraft packets
 * Copyright (C) dmulloy2 <http://dmulloy2.net>
 * Copyright (C) Kristian S. Strangeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.vicemice.hector.api.protocolwrapper;

import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.events.PacketContainer;
import net.vicemice.hector.api.protocol.wrappers.EnumWrappers;

public class WrapperPlayServerWorldParticles extends AbstractPacket {
	public static final PacketType TYPE;

	public WrapperPlayServerWorldParticles() {
		super(new PacketContainer(TYPE), TYPE);
		this.handle.getModifier().writeDefaults();
	}

	public WrapperPlayServerWorldParticles(PacketContainer packet) {
		super(packet, TYPE);
	}

	public EnumWrappers.Particle getParticleType() {
		return (EnumWrappers.Particle)this.handle.getParticles().read(0);
	}

	public void setParticleType(EnumWrappers.Particle value) {
		this.handle.getParticles().write(0, value);
	}

	public float getX() {
		return (Float)this.handle.getFloat().read(0);
	}

	public void setX(float value) {
		this.handle.getFloat().write(0, value);
	}

	public float getY() {
		return (Float)this.handle.getFloat().read(1);
	}

	public void setY(float value) {
		this.handle.getFloat().write(1, value);
	}

	public float getZ() {
		return (Float)this.handle.getFloat().read(2);
	}

	public void setZ(float value) {
		this.handle.getFloat().write(2, value);
	}

	public float getOffsetX() {
		return (Float)this.handle.getFloat().read(3);
	}

	public void setOffsetX(float value) {
		this.handle.getFloat().write(3, value);
	}

	public float getOffsetY() {
		return (Float)this.handle.getFloat().read(4);
	}

	public void setOffsetY(float value) {
		this.handle.getFloat().write(4, value);
	}

	public float getOffsetZ() {
		return (Float)this.handle.getFloat().read(5);
	}

	public void setOffsetZ(float value) {
		this.handle.getFloat().write(5, value);
	}

	public float getParticleData() {
		return (Float)this.handle.getFloat().read(6);
	}

	public void setParticleData(float value) {
		this.handle.getFloat().write(6, value);
	}

	public int getNumberOfParticles() {
		return (Integer)this.handle.getIntegers().read(0);
	}

	public void setNumberOfParticles(int value) {
		this.handle.getIntegers().write(0, value);
	}

	public boolean getLongDistance() {
		return (Boolean)this.handle.getBooleans().read(0);
	}

	public void setLongDistance(boolean value) {
		this.handle.getBooleans().write(0, value);
	}

	public int[] getData() {
		return (int[])this.handle.getIntegerArrays().read(0);
	}

	public void setData(int[] value) {
		this.handle.getIntegerArrays().write(0, value);
	}

	static {
		TYPE = PacketType.Play.Server.WORLD_PARTICLES;
	}
}
