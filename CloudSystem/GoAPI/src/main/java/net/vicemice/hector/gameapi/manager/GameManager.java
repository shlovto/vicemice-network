package net.vicemice.hector.gameapi.manager;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.gameapi.events.*;
import net.vicemice.hector.gameapi.util.Locations;
import net.vicemice.hector.gameapi.util.Title;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.replay.ReplayDataPacket;
import net.vicemice.hector.packets.types.player.state.UpdatePlayerStatePacket;
import net.vicemice.hector.utils.players.utils.PlayerState;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import java.util.ArrayList;
import java.util.UUID;

public class GameManager {
    @Getter
    @Setter
    private static GameState gameState = GameState.LOBBY;
    @Getter
    @Setter
    private static int lobbyCount = 60, restartCount = 15, mapCount = 3, toSendMessage = 10,  peaceTime = 60, replayStartSeconds = 0;
    @Getter
    @Setter
    private static long startTime, length;
    @Setter
    @Getter
    private static boolean peace = true;
    @Getter
    @Setter
    private static ReplayDataPacket replayDataPacket;
    protected static boolean sendIngamePlayers;

    @Getter
    private static final ArrayList<UUID> replayPlayers = new ArrayList<>(), saveReplayPlayers = new ArrayList<>(), ingamePlayers = new ArrayList<>(), spectatorPlayers = new ArrayList<>();

    public static void startTimer(Plugin plugin, GoAPI goAPI) {
        Bukkit.getServer().getScheduler().runTaskTimer(plugin, () -> {
            Locations.getLobbyLocation().getWorld().setThundering(false);
            Locations.getLobbyLocation().getWorld().setStorm(false);
            if (Locations.getLobbyLocation().getWorld().getTime() > 8000) {
                Locations.getLobbyLocation().getWorld().setTime(0);
            }

            spectatorPlayers.forEach(e -> {
                goAPI.getNettyClient().getChannel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.UPDATE_PLAYER_STATE, new UpdatePlayerStatePacket(e, PlayerState.SPECTATOR)));
                spectatorPlayers.remove(e);
            });

            if (gameState == GameState.LOBBY) {
                Bukkit.getPluginManager().callEvent(new GameLobbyEvent(lobbyCount));
            } else if (gameState == GameState.WARMUP || gameState == GameState.PREPARING) {
                Bukkit.getPluginManager().callEvent(new GameStartEvent(System.currentTimeMillis(), true));
            } else if (gameState == GameState.INGAME) {
                length++;
                if (!sendIngamePlayers) {
                    ingamePlayers.forEach(e -> {
                        goAPI.getNettyClient().getChannel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.UPDATE_PLAYER_STATE, new UpdatePlayerStatePacket(e, PlayerState.INGAME)));
                    });
                    sendIngamePlayers = true;
                }
                Bukkit.getPluginManager().callEvent(new GameIngameEvent());
            } else if (gameState == GameState.END || gameState == GameState.RESTART) {
                Bukkit.getPluginManager().callEvent(new GameEndEvent(System.currentTimeMillis(), true));
            }
        }, 20L, 20L);
    }

    public static void sendTitle(String game, String map) {
        Title title = new Title(game, "§7" + map, 0, 3, 2);
        Bukkit.getOnlinePlayers().forEach(title::send);
    }

    public static void updateScoreboardTime() {
        Bukkit.getPluginManager().callEvent(new GameUpdateScoreboardTimeEvent());
    }

    public static void endGame() {
        Bukkit.getPluginManager().callEvent(new GameEndGameEvent());
    }

    public static String getTime(long time) {

        long diff = System.currentTimeMillis() - time;
        diff = diff / 1000;

        int minuten = 0;
        int sekunden = 0;

        if (diff >= 60) {
            minuten = (int) diff / 60;
            diff -= minuten * 60;
        }

        sekunden = (int) diff;

        String minRet = String.valueOf(minuten);
        String secRet = String.valueOf(sekunden);

        if (minuten < 10) {
            minRet = "0" + minRet;
        }

        if (sekunden < 10) {
            secRet = "0" + secRet;
        }

        return minRet + ":" + secRet;

    }

    public static void startGame() {
        Bukkit.getPluginManager().callEvent(new GameStartPrepareEvent());
    }
}