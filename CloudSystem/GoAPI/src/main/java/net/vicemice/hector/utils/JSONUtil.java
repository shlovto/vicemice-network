package net.vicemice.hector.utils;

import lombok.Getter;
import lombok.Setter;
import org.json.JSONObject;

public class JSONUtil {

    private final String url;
    @Setter
    private boolean post = true;

    /**
     @param url defined the url where we should get the result
     */
    public JSONUtil(String url) {
        this.url = url;
    }

    /**
     @param api defined the API-URL
     @param value defined the value which comes after the last /
     */
    public JSONUtil(API api, String value) {
        this.url = api.getUrl() + value;
    }

    /**
     Get the URL Result
     @return an Result from the gived URL
     */
    public String getResult() {
        try {
            if (post) {
                return new URLConnection().sendPost(this.url, "");
            } else {
                return new URLConnection().sendGet(this.url);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    /**
     Get the JSON Object from an valid URL Result
     @return an JSON Object
     */
    public JSONObject getObject() {
        return new JSONObject(getResult());
    }

    /**
     Here are all API-URL's we must use more then one times
     */
    public enum API {
        INTERNAL_API("https://vicemice.net/h8RJKtbZv3vRg77Lw22hYmPwCN4aMme5k4xxjLtB7nMPjrFgbmEZTxaZeTZfn8qkn/api_v2.php"),
        FORUM("https://vicemice.net/forum/api/"),
        MCSTATS("https://api.mcstats.net/v2/player/"),
        IPAPI("http://ip-api.com/json/");

        @Getter
        String url;

        private API(String url) {
            this.url = url;
        }
    }
}