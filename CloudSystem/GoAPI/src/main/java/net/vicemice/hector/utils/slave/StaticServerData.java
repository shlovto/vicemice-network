package net.vicemice.hector.utils.slave;

import java.io.Serializable;

/*
 * Class created at 01:02 - 05.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class StaticServerData implements Serializable {

    private String name, jarFile;
    private int port;

    public StaticServerData(String name, String jarFile, int port) {
        this.name = name;
        this.jarFile = jarFile;
        this.port = port;
    }

    public String getName() {
        return name;
    }

    public String getJarFile() {
        return jarFile;
    }

    public int getPort() {
        return port;
    }
}