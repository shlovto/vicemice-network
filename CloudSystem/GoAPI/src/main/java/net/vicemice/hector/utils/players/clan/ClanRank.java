package net.vicemice.hector.utils.players.clan;

import lombok.Getter;

import java.io.Serializable;

/**
 * Created by Paul B. on 15.02.2019.
 */
public enum ClanRank implements Serializable {
    LEADER("leader", 3),
    MODERATOR("moderator", 2),
    MEMBER("member", 1);

    @Getter
    private final String key;
    @Getter
    private final int accessLevel;

    ClanRank(String key, int accessLevel) {
        this.key = key;
        this.accessLevel = accessLevel;
    }

    public boolean isHigherEquals(ClanRank clanRank) {
        if (accessLevel >= clanRank.getAccessLevel()) return true;
        return false;
    }
}