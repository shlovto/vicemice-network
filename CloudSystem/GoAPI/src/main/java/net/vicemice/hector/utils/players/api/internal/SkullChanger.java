package net.vicemice.hector.utils.players.api.internal;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;
import net.minecraft.server.v1_8_R3.NBTTagString;
import net.vicemice.hector.utils.players.api.internal.nbt.Title_v1_8_R3;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.UUID;

public class SkullChanger {
    public static Class<?> skullMetaClass;
    public static Class<?> tileEntityClass;
    public static Class<?> blockPositionClass;

    public static ItemStack getSkull(Player player) {
        return SkullChanger.getSkull(getSignature(player), getValue(player), 1);
    }

    public static ItemStack getSkull(String signature, String value) {
        return SkullChanger.getSkull(signature, value, 1);
    }

    public static ItemStack getSkull(String value) {
        return SkullChanger.getSkull(value, 1);
    }

    public static ItemStack getSkull(String owner, String signature, String value) {
        ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta meta = (SkullMeta) skull.getItemMeta();
        meta.setOwner(owner);
        try {
            Field profileField = skullMetaClass.getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(meta, SkullChanger.getProfile(signature, value));
        } catch (Exception e) {
            e.printStackTrace();
        }
        skull.setItemMeta(meta);
        return skull;
    }

    public static ItemStack getSkull(String signature, String value, int amount) {
        ItemStack skull = new ItemStack(Material.SKULL_ITEM, amount, (short) 3);
        SkullMeta meta = (SkullMeta) skull.getItemMeta();
        try {
            Field profileField = skullMetaClass.getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(meta, SkullChanger.getProfile(signature, value));
        } catch (Exception e) {
            e.printStackTrace();
        }
        skull.setItemMeta(meta);
        return skull;
    }

    public static ItemStack getSkull(String value, int amount) {
        ItemStack skull = new ItemStack(Material.SKULL_ITEM, amount, (short) 3);
        SkullMeta meta = (SkullMeta) skull.getItemMeta();
        try {
            Field profileField = skullMetaClass.getDeclaredField("profile");
            profileField.setAccessible(true);
            profileField.set(meta, SkullChanger.getProfile(value));
        } catch (Exception e) {
            e.printStackTrace();
        }
        skull.setItemMeta(meta);
        return skull;
    }

    public static boolean setBlock(Location loc, String signature, String value) {
        return SkullChanger.setBlock(loc.getBlock(), signature, value);
    }

    public static boolean setBlock(Block block, String signature, String value) {
        try {
            Object nmsWorld = block.getWorld().getClass().getMethod("getHandle", new Class[0]).invoke(block.getWorld(), new Object[0]);
            Object tileEntity = null;
            Method getTileEntity = nmsWorld.getClass().getMethod("getTileEntity", blockPositionClass);
            tileEntity = tileEntityClass.cast(getTileEntity.invoke(nmsWorld, SkullChanger.getBlockPositionFor(block.getX(), block.getY(), block.getZ())));
            tileEntityClass.getMethod("setGameProfile", GameProfile.class).invoke(tileEntity, new Object[]{SkullChanger.getProfile(signature, value)});
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static String getSignature(Player p) {
        try {
            return SkullChanger.getSignature((GameProfile) p.getClass().getMethod("getProfile", new Class[0]).invoke(p, new Object[0]));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getSignature(GameProfile gp) {
        Property textures = gp.getProperties().get("textures").iterator().next();
        return textures.getSignature();
    }

    public static String getValue(Player p) {
        try {
            return SkullChanger.getValue((GameProfile) p.getClass().getMethod("getProfile", new Class[0]).invoke(p, new Object[0]));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getValue(GameProfile gp) {
        Property textures = gp.getProperties().get("textures").iterator().next();
        return textures.getValue();
    }

    private static GameProfile getProfile(String signature, String value) {
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        Property property = new Property("textures", value, signature);
        profile.getProperties().put("textures", property);
        return profile;
    }

    private static GameProfile getProfile(String value) {
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        Property property = new Property("textures", value);
        profile.getProperties().put("textures", property);
        return profile;
    }

    private static Object getBlockPositionFor(int x, int y, int z) {
        Object blockPosition = null;
        try {
            Constructor cons = blockPositionClass.getConstructor(Integer.TYPE, Integer.TYPE, Integer.TYPE);
            blockPosition = cons.newInstance(x, y, z);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return blockPosition;
    }

    public void setHead(Block skull, String skinUrl) {
        if (Package.getPackage("net.minecraft.server.v1_8_R3") != null) {
            new Title_v1_8_R3().setHead(skull, skinUrl);
        }
    }

    public static ItemStack getSkullByTexture(String id, String texture) {
        ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(" ");
        item.setItemMeta(meta);

        net.minecraft.server.v1_8_R3.ItemStack nmsItemStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound compound = nmsItemStack.getTag();

        if(compound == null) {
            compound = new NBTTagCompound();
            nmsItemStack.setTag(compound);
            compound = nmsItemStack.getTag();
        }

        NBTTagCompound skullOwner = new NBTTagCompound();
        skullOwner.set("Id", new NBTTagString(id));
        NBTTagCompound properties = new NBTTagCompound();
        NBTTagList textures = new NBTTagList();
        NBTTagCompound value = new NBTTagCompound();
        value.set("Value", new NBTTagString(texture));
        textures.add(value);
        properties.set("textures", textures);
        skullOwner.set("Properties", properties);

        compound.set("SkullOwner", skullOwner);
        nmsItemStack.setTag(compound);

        return CraftItemStack.asBukkitCopy(nmsItemStack);
    }

    static {
        String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
        try {
            skullMetaClass = Class.forName("org.bukkit.craftbukkit." + version + ".inventory.CraftMetaSkull");
            tileEntityClass = Class.forName("net.minecraft.server." + version + ".TileEntitySkull");
            blockPositionClass = Class.forName("net.minecraft.server." + version + ".BlockPosition");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}