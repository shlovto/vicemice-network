package net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8;

import net.vicemice.hector.api.protocolversion.api.Via;
import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.protocols.base.ProtocolInfo;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.providers.MovementTransmitterProvider;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.storage.MovementTracker;

public class ViaIdleThread implements Runnable {
    @Override
    public void run() {
        for (UserConnection info : Via.getManager().getPortedPlayers().values()) {
            ProtocolInfo protocolInfo = info.get(ProtocolInfo.class);
            if (protocolInfo != null && protocolInfo.getPipeline().contains(Protocol1_9To1_8.class)) {
                long nextIdleUpdate = info.get(MovementTracker.class).getNextIdlePacket();
                if (nextIdleUpdate <= System.currentTimeMillis()) {
                    if (info.getChannel().isOpen()) {
                        Via.getManager().getProviders().get(MovementTransmitterProvider.class).sendPlayer(info);
                    }
                }
            }
        }
    }
}
