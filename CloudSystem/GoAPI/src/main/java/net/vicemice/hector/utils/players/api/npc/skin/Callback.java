package net.vicemice.hector.utils.players.api.npc.skin;

public interface Callback<O> {
    void call(O object);
}