package net.vicemice.hector.packets.types.server;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:25 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PrivateOwnerPacket implements Serializable {

    @Getter
    private UUID uniqueId;

    public PrivateOwnerPacket(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }
}
