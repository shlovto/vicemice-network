package net.vicemice.hector.events;

import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.event.AsyncEvent;

public class ProxyUpdateLocaleEvent extends AsyncEvent<ProxyUpdateLocaleEvent> {

    public ProxyUpdateLocaleEvent(Callback<ProxyUpdateLocaleEvent> done) {
        super(done);
    }
}
