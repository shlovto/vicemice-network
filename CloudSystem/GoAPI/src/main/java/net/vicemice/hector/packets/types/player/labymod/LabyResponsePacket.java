package net.vicemice.hector.packets.types.player.labymod;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:28 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class LabyResponsePacket implements Serializable {
    @Getter
    private UUID uuid;
    @Getter
    private String response, value;

    public LabyResponsePacket(UUID uuid, String response, String value) {
        this.uuid = uuid;

        this.response = response;
        this.value = value;
    }
}
