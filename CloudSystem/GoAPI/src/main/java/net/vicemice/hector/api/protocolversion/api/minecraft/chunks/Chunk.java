package net.vicemice.hector.api.protocolversion.api.minecraft.chunks;

import net.vicemice.hector.api.nbt.tag.builtin.CompoundTag;

import java.util.List;

public interface Chunk {
    int getX();

    int getZ();

    boolean isBiomeData();

    int getBitmask();

    ChunkSection[] getSections();

    int[] getBiomeData();

    void setBiomeData(int[] biomeData);

    CompoundTag getHeightMap();

    void setHeightMap(CompoundTag heightMap);

    List<CompoundTag> getBlockEntities();

    boolean isGroundUp();
}
