package net.vicemice.hector.packets.types.proxy;

import java.io.Serializable;

/*
 * Class created at 18:39 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class MaintenancePacket implements Serializable {
    private boolean enabled;
    private boolean kickAll = false;

    public MaintenancePacket(boolean enabled, boolean kickAll) {
        this.enabled = enabled;
        this.kickAll = kickAll;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public boolean isKickAll() {
        return this.kickAll;
    }
}
