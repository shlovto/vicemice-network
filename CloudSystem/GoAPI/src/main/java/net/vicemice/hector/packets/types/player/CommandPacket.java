package net.vicemice.hector.packets.types.player;

import lombok.Getter;
import net.vicemice.hector.utils.CommandType;
import java.io.Serializable;

/*
 * Class created at 18:33 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class CommandPacket implements Serializable {

    @Getter
    private String name;
    @Getter
    private CommandType commandType;
    @Getter
    private String[] command;

    public CommandPacket(String name, CommandType commandType, String[] command) {
        this.name = name;
        this.commandType = commandType;
        this.command = command;
    }
}
