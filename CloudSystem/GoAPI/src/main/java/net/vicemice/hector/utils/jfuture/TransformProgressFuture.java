package net.vicemice.hector.utils.jfuture;

import lombok.NonNull;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

/*
 * Class created at 01:05 - 04.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class TransformProgressFuture<I, O> implements ProgressFuture<O> {
    @NonNull
    private final ProgressFuture<I> handle;
    private final Function<I, O> transformer;

    public TransformProgressFuture(ProgressFuture<I> handle) {
        this(handle, null);
    }

    public TransformProgressFuture(@NonNull ProgressFuture<I> handle, Function<I, O> transformer) {
        if (handle == null) {
            throw new NullPointerException("handle");
        }
        this.handle = handle;
        this.transformer = transformer;
    }

    protected O transform(I in) {
        return this.transformer.apply(in);
    }

    @Override
    public O get() {
        return this.transform(this.handle.get());
    }

    @Override
    public O getOr(O obj) {
        return this.isDone() ? this.transform(this.handle.get()) : obj;
    }

    @Override
    public void get(ProgressFuture.AsyncCallback<O> task) {
        this.handle.get((obj, ex) -> task.done(this.transform(obj), ex));
    }

    @Override
    public void get(ProgressFuture.TimedAsyncCallback<O> task) {
        this.handle.get((obj, ex, diff) -> task.done(this.transform(obj), ex, diff));
    }

    @Override
    public O get(int timeout, TimeUnit unit) throws TimeoutException {
        return this.transform(this.handle.get(timeout, unit));
    }

    @Override
    public O getOr(int timeout, TimeUnit unit, O obj) {
        try {
            return this.transform(this.handle.get(timeout, unit));
        }
        catch (TimeoutException e) {
            return obj;
        }
    }

    @Override
    public void get(ProgressFuture.AsyncCallback<O> task, int timeout, TimeUnit unit) {
        this.handle.get((obj, ex) -> task.done(this.transform(obj), ex), timeout, unit);
    }

    @Override
    public void get(ProgressFuture.TimedAsyncCallback<O> task, int timeout, TimeUnit unit) {
        this.handle.get((obj, ex, diff) -> task.done(this.transform(obj), ex, diff), timeout, unit);
    }

    @Override
    public boolean isDone() {
        return this.handle.isDone();
    }

    @Override
    public boolean isSuccessful() {
        return this.handle.isSuccessful();
    }

    @Override
    public Exception getException() {
        return this.handle.getException();
    }
}
