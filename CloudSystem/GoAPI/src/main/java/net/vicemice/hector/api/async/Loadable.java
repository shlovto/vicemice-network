package net.vicemice.hector.api.async;

public interface Loadable<T> {
    public Future<T> load();
}