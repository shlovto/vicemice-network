package net.vicemice.hector.api.protocol.wrappers;

public interface ClonableWrapper {
	Object getHandle();
	ClonableWrapper deepClone();

}
