package net.vicemice.hector.service.server;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import net.vicemice.hector.backend.GoNetty;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.packets.types.PacketHolder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/*
 * Class created at 12:53 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class NettyClient implements GoNetty {
    private Channel channel;
    private String host;
    private int port;
    private List<PacketGetter> packetGetters = new CopyOnWriteArrayList<>();
    private List<PacketHolder> beforeActive = new ArrayList<>();
    private NettyClient instance;

    public NettyClient(String host, int port) {
        this.instance = this;
        this.host = host;
        this.port = port;
    }

    @Override
    public void startClient() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                NioEventLoopGroup eventLoopGroup = new NioEventLoopGroup();
                try {
                    Bootstrap bootstrap = new Bootstrap();
                    ((bootstrap.group(eventLoopGroup)).channel(NioSocketChannel.class)).handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            socketChannel.pipeline().addLast(new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(this.getClass().getClassLoader())));
                            socketChannel.pipeline().addLast(new ObjectEncoder());
                            socketChannel.pipeline().addLast(new NettyHandler(NettyClient.this.instance));
                        }
                    });
                    ChannelFuture channelFuture = bootstrap.connect(NettyClient.this.getHost(), NettyClient.this.getPort());
                    Channel channel = channelFuture.channel();
                    ChannelFuture closeFuture = channel.closeFuture();
                    closeFuture.addListener(channelFuture1 -> {
                        System.out.println("A connection to the cloud could not be established! (New connection in 2 seconds)");
                        Thread.sleep(2000);
                        NettyClient.this.startClient();
                        eventLoopGroup.shutdownGracefully();
                    });
                    closeFuture.sync();
                } catch (Exception exc) {
                    exc.printStackTrace();
                } finally {
                    eventLoopGroup.shutdownGracefully();
                }
            }
        }).start();
    }

    @Override
    public void channelActive(Channel channel) {
        for (PacketGetter packetGetter : this.packetGetters) {
            packetGetter.channelActive(channel);
        }
    }

    @Override
    public void channelRead(PacketHolder initPacket) {
        for (PacketGetter packetGetter : this.packetGetters) {
            try {
                long start = System.currentTimeMillis();
                packetGetter.receivePacket(initPacket);
                long end = System.currentTimeMillis();
                if (end - start <= 10) continue;
                System.err.println("Need more than 10ms (" + (end - start) + ") to handle " + initPacket.getKey() + " at " + packetGetter);
            } catch (Exception e) {
                System.err.println("Exception while handling packet " + initPacket.getKey() + " for handler " + packetGetter);
                e.printStackTrace();
            }
        }
    }

    @Override
    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    @Override
    public void addGetter(PacketGetter packetGetter)  {
        this.packetGetters.add(packetGetter);
    }

    @Override
    public Channel getChannel() {
        return this.channel;
    }

    @Override
    public String getHost() {
        return this.host;
    }

    @Override
    public int getPort() {
        return this.port;
    }

    @Override
    public List<PacketGetter> getPacketGetters() {
        return this.packetGetters;
    }

    @Override
    public List<PacketHolder> getBeforeActive() {
        return this.beforeActive;
    }
}
