package net.vicemice.hector.api.protocolversion.protocols.protocol1_12_2to1_12_1;

import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.api.protocol.Protocol;
import net.vicemice.hector.api.protocolversion.api.remapper.PacketRemapper;
import net.vicemice.hector.api.protocolversion.api.type.Type;
import net.vicemice.hector.api.protocolversion.packets.State;

public class Protocol1_12_2To1_12_1 extends Protocol {
    @Override
    protected void registerPackets() {
        // Outgoing
        // 0x1f - Keep alive
        registerOutgoing(State.PLAY, 0x1f, 0x1f, new PacketRemapper() {
            @Override
            public void registerMap() {
                map(Type.VAR_INT, Type.LONG);
            }
        }); // Keep alive
        // Incoming
        // 0xb - Keep alive
        registerIncoming(State.PLAY, 0xb, 0xb, new PacketRemapper() {
            @Override
            public void registerMap() {
                map(Type.LONG, Type.VAR_INT);
            }
        });
    }

    @Override
    public void init(UserConnection userConnection) {

    }
}
