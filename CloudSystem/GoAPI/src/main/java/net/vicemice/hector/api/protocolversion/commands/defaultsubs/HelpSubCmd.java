package net.vicemice.hector.api.protocolversion.commands.defaultsubs;

import net.vicemice.hector.api.protocolversion.api.Via;
import net.vicemice.hector.api.protocolversion.api.command.ViaCommandSender;
import net.vicemice.hector.api.protocolversion.api.command.ViaSubCommand;

public class HelpSubCmd extends ViaSubCommand {
    @Override
    public String name() {
        return "help";
    }

    @Override
    public String description() {
        return "You are looking at it right now!";
    }

    @Override
    public boolean execute(ViaCommandSender sender, String[] args) {
        Via.getManager().getCommandHandler().showHelp(sender);
        return true;
    }
}
