package net.vicemice.hector.packets.types.player.friend;

import lombok.Getter;

import java.io.Serializable;
import java.util.List;

/*
 * Class created at 18:30 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class FriendPacket implements Serializable {

    @Getter
    private List<String[]> friends;
    @Getter
    private List<String[]> requests;
    @Getter
    private String name;

    public FriendPacket(List<String[]> friends, List<String[]> requests, String name) {
        this.friends = friends;
        this.requests = requests;
        this.name = name;
    }
}
