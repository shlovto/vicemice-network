package net.vicemice.hector.packets.types.player.nick;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:26 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerNickPacket implements Serializable {
    private String name;
    private UUID uniqueId;
    private String nickname;
    private String value;
    private String signature;

    public PlayerNickPacket(String name, UUID uniqueId, String nickname, String value, String signature) {
        this.name = name;
        this.uniqueId = uniqueId;
        this.nickname = nickname;
        this.value = value;
        this.signature = signature;
    }

    public String getName() {
        return this.name;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public String getNickname() {
        return this.nickname;
    }

    public String getValue() {
        return this.value;
    }

    public String getSignature() {
        return this.signature;
    }
}
