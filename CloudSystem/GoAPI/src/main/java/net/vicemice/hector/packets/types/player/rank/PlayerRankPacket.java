package net.vicemice.hector.packets.types.player.rank;

import net.vicemice.hector.utils.Rank;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/*
 * Class created at 08:56 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerRankPacket implements Serializable {
    private String name;
    private UUID uniqueId;
    private Rank rank;
    private List<String> addedPermissions;

    public PlayerRankPacket(String name, UUID uniqueId, Rank rank, List<String> addedPermissions) {
        this.name = name;
        this.uniqueId = uniqueId;
        this.rank = rank;
        this.addedPermissions = addedPermissions;
    }

    public String getName() {
        return name;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public Rank getRank() {
        return this.rank;
    }

    public List<String> getAddedPermissions() {
        return this.addedPermissions;
    }
}
