package net.vicemice.hector.utils;

/*
 * Class created at 09:01 - 23.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public enum ServerType {
    CLOUD_SERVER,
    PROXY,
    STATIC;
}