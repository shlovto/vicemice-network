package net.vicemice.hector.packets.types.slave;

import java.io.Serializable;

/*
 * Class created at 18:42 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SlaveStopPacket implements Serializable {
    private String name;

    public SlaveStopPacket(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
