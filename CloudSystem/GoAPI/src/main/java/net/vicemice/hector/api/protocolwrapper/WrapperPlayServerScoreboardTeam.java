/**
 * PacketWrapper - ProtocolLib wrappers for Minecraft packets
 * Copyright (C) dmulloy2 <http://dmulloy2.net>
 * Copyright (C) Kristian S. Strangeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.vicemice.hector.api.protocolwrapper;

import java.util.Collection;
import java.util.List;

import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.events.PacketContainer;

import lombok.Getter;

public class WrapperPlayServerScoreboardTeam extends AbstractPacket {
	public static final PacketType TYPE;

	public WrapperPlayServerScoreboardTeam() {
		super(new PacketContainer(TYPE), TYPE);
		this.handle.getModifier().writeDefaults();
	}


	public WrapperPlayServerScoreboardTeam(PacketContainer packet) {
		super(packet, TYPE);
	}

	public String getName() {
		return (String)this.handle.getStrings().read(0);
	}

	public void setName(String value) {
		this.handle.getStrings().write(0, value);
	}

	public String getDisplayName() {
		return (String)this.handle.getStrings().read(1);
	}

	public void setDisplayName(String value) {
		this.handle.getStrings().write(1, value);
	}

	public String getPrefix() {
		return (String)this.handle.getStrings().read(2);
	}

	public void setPrefix(String value) {
		this.handle.getStrings().write(2, value);
	}

	public String getSuffix() {
		return (String)this.handle.getStrings().read(3);
	}

	public void setSuffix(String value) {
		this.handle.getStrings().write(3, value);
	}

	public String getNameTagVisibility() {
		return (String)this.handle.getStrings().read(4);
	}

	public void setNameTagVisibility(String value) {
		this.handle.getStrings().write(4, value);
	}

	public int getColor() {
		return (Integer)this.handle.getIntegers().read(0);
	}

	public void setColor(int value) {
		this.handle.getIntegers().write(0, value);
	}

	public List<String> getPlayers() {
		return (List)this.handle.getSpecificModifier(Collection.class).read(0);
	}

	public void setPlayers(List<String> value) {
		this.handle.getSpecificModifier(Collection.class).write(0, value);
	}

	public int getMode() {
		return (Integer)this.handle.getIntegers().read(1);
	}

	public void setMode(int value) {
		this.handle.getIntegers().write(1, value);
	}

	public int getPackOptionData() {
		return (Integer)this.handle.getIntegers().read(2);
	}

	public void setPackOptionData(int value) {
		this.handle.getIntegers().write(2, value);
	}

	static {
		TYPE = PacketType.Play.Server.SCOREBOARD_TEAM;
	}

	@Getter
	public class Mode {
		public static final int TEAM_CREATED = 0;
		public static final int TEAM_REMOVED = 1;
		public static final int TEAM_UPDATED = 2;
		public static final int PLAYERS_ADDED = 3;
		public static final int PLAYERS_REMOVED = 4;
	}
}
