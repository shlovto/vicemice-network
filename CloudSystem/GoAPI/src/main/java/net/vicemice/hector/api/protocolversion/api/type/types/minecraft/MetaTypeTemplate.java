package net.vicemice.hector.api.protocolversion.api.type.types.minecraft;

import net.vicemice.hector.api.protocolversion.api.minecraft.metadata.Metadata;
import net.vicemice.hector.api.protocolversion.api.type.Type;

public abstract class MetaTypeTemplate extends Type<Metadata> {
    public MetaTypeTemplate() {
        super("Metadata type", Metadata.class);
    }

    @Override
    public Class<? extends Type> getBaseClass() {
        return MetaTypeTemplate.class;
    }
}
