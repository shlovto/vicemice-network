package net.vicemice.hector.types;

import org.bukkit.scoreboard.Scoreboard;

/*
 * Class created at 06:17 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public interface ServerAPI {

    /**
     *
     * @return returns the maximal players number for the server, which can be joined without premium
     */
    public int getServerMaxPlayers();

    /**
     *
     * @param state defines the server state
     * @param motd defines the server message on e.q. signs
     */
    public void changeServer(ServerState state, String motd);

    /**
     *
     * @return returns the server name
     */
    public String getServerName();

    /**
     *
     * @param scoreboard defines the scoreboard which will be set when an player joined
     */
    public void setScoreboard(Scoreboard scoreboard);

    /**
     *
     * @param var1 defines if premium players can kick non-premium players
     */
    public void setKick(boolean var1);
}