/**
 * PacketWrapper - ProtocolLib wrappers for Minecraft packets
 * Copyright (C) dmulloy2 <http://dmulloy2.net>
 * Copyright (C) Kristian S. Strangeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.vicemice.hector.api.protocolwrapper;

import org.bukkit.World;
import org.bukkit.entity.Entity;

import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.events.PacketContainer;
import net.vicemice.hector.api.protocol.events.PacketEvent;

public class WrapperPlayServerAttachEntity extends AbstractPacket {
	public static final PacketType TYPE;

	public WrapperPlayServerAttachEntity() {
		super(new PacketContainer(TYPE), TYPE);
		this.handle.getModifier().writeDefaults();
	}

	public WrapperPlayServerAttachEntity(PacketContainer packet) {
		super(packet, TYPE);
	}

	public int getEntityID() {
		return (Integer)this.handle.getIntegers().read(0);
	}

	public void setEntityID(int value) {
		this.handle.getIntegers().write(0, value);
	}

	public Entity getEntity(World world) {
		return (Entity)this.handle.getEntityModifier(world).read(0);
	}

	public Entity getEntity(PacketEvent event) {
		return this.getEntity(event.getPlayer().getWorld());
	}

	public int getVehicleId() {
		return (Integer)this.handle.getIntegers().read(2);
	}

	public void setVehicleId(int value) {
		this.handle.getIntegers().write(2, value);
	}

	public boolean getLeash() {
		return (Integer)this.handle.getIntegers().read(0) != 0;
	}

	public void setLeash(boolean value) {
		this.handle.getIntegers().write(0, value ? 1 : 0);
	}

	static {
		TYPE = PacketType.Play.Server.ATTACH_ENTITY;
	}
}
