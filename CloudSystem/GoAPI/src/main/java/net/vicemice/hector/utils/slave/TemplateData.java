package net.vicemice.hector.utils.slave;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;

/*
 * Class created at 01:13 - 05.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class TemplateData implements Serializable {

    private String name, prefix, color, rank, statsPrefix, startCommand, forceSlave;
    private int version, minServer, maxServer, maxRamProServer, minRamProServer, maxPlayers, lobbySigns;
    private boolean active = false, nickAble = false, playAble = false, newDownload = false, privateTemplate = false;
    private HashMap<String, String[]> signDatas = new HashMap<>();

    public TemplateData(String name, String prefix, String color, String rank, int version, int minServer, int maxServer, int maxRamProServer, int minRamProServer, int lobbySigns, int maxPlayers, String statsPrefix, String startCommand, boolean nickAble, boolean playAble, boolean privateTemplate) {
        this.name = name;
        this.prefix = prefix;
        this.color = color;
        this.rank = rank;
        this.version = version;
        this.minServer = minServer;
        this.maxServer = maxServer;
        this.maxRamProServer = maxRamProServer;
        this.minRamProServer = minRamProServer;
        this.lobbySigns = lobbySigns;
        this.maxPlayers = maxPlayers;
        this.statsPrefix = statsPrefix;
        this.startCommand = startCommand;
        this.nickAble = nickAble;
        this.playAble = playAble;
        this.privateTemplate = privateTemplate;
    }

    public String getGame() {
        return (this.color + this.prefix);
    }

    public String[] toStringArray(boolean newload) {
        if (newload) {
            return new String[]{this.name, this.prefix, String.valueOf(this.maxRamProServer), String.valueOf(this.minRamProServer), Boolean.toString(true), this.startCommand};
        }
        return new String[]{this.name, this.prefix, String.valueOf(this.maxRamProServer), String.valueOf(this.minRamProServer), Boolean.toString(false), this.startCommand};
    }
}