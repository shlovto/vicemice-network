package net.vicemice.hector.utils;

import sun.net.www.protocol.https.HttpsURLConnectionImpl;
import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class URLConnection {
    private final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36 OPR/64.0.3417.167";

    /**
     * Send an GET Request to an Website e.q. an API
     * @param url set the url of the page to which we will sent the get request
     * @return returns the response
     * @throws IOException throws the exception when the url is invalid
     */
    public String sendGet(String url) throws IOException {

        URL obj = new URL(url);

        if (obj.openConnection() instanceof HttpsURLConnection) {
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("XF-Api-User", "1");
            con.setRequestProperty("XF-Api-Key", "q-BJoMoK54PrCdrtbn1qrV0LTcyk_s9i");

            int responseCode = con.getResponseCode();
            //System.out.println("\nSending 'GET' request to URL : " + url);
            //System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //return result
            return response.toString();
        } else if (obj.openConnection() instanceof HttpsURLConnectionImpl) {
            HttpsURLConnectionImpl con = (HttpsURLConnectionImpl) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("XF-Api-User", "1");
            con.setRequestProperty("XF-Api-Key", "q-BJoMoK54PrCdrtbn1qrV0LTcyk_s9i");

            int responseCode = con.getResponseCode();
            //System.out.println("\nSending 'GET' request to URL : " + url);
            //System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //return result
            return response.toString();
        }

        return null;
    }

    /**
     * Send an POST Request to an Website e.q. an API
     * @param url set the url of the page to which we will sent the post request
     * @param urlParameters set the url parameters which we need to send
     * @return returns the response
     * @throws IOException throws the exception when the url is invalid
     */
    public String sendPost(String url, String urlParameters) throws IOException {
        URL obj = new URL(url);
        if (obj.openConnection() instanceof HttpsURLConnection) {
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

            //add request header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("XF-Api-User", "1");
            con.setRequestProperty("XF-Api-Key", "q-BJoMoK54PrCdrtbn1qrV0LTcyk_s9i");

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            //System.out.println("\nSending 'POST' request to URL : " + url);
            //System.out.println("Post parameters : " + urlParameters);
            //System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //return result
            return response.toString();
        } else if (obj.openConnection() instanceof HttpsURLConnectionImpl) {
            HttpsURLConnectionImpl con = (HttpsURLConnectionImpl) obj.openConnection();

            //add request header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("XF-Api-User", "1");
            con.setRequestProperty("XF-Api-Key", "q-BJoMoK54PrCdrtbn1qrV0LTcyk_s9i");

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            //System.out.println("\nSending 'POST' request to URL : " + url);
            //System.out.println("Post parameters : " + urlParameters);
            //System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            //return result
            return response.toString();
        }

        return null;
    }
}