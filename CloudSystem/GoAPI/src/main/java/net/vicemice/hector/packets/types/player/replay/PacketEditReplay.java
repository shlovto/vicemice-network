package net.vicemice.hector.packets.types.player.replay;

import lombok.Getter;
import java.io.Serializable;
import java.util.UUID;

/* 
 * Class created at 07:29 - 18.04.2020
 * Copyright (C) elrobtossohn
 */
@Getter
public class PacketEditReplay implements Serializable {
    private final UUID uniqueId;
    private final String gameId;
    private final EditType editType;

    public PacketEditReplay(UUID uniqueId, String gameId, EditType editType) {
        this.uniqueId = uniqueId;
        this.gameId = gameId;
        this.editType = editType;
    }

    public enum EditType implements Serializable {
        ADD_FAVORITE,
        REMOVE_FAVORITE,
        DELETE_REPLAY;
    }
}
