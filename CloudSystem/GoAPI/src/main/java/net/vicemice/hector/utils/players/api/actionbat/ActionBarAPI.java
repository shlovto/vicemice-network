package net.vicemice.hector.utils.players.api.actionbat;

import net.vicemice.hector.utils.players.api.actionbat.version.*;
import org.bukkit.entity.Player;

public class ActionBarAPI {

    public static void sendActionBar(String msg) {
        if (Package.getPackage("net.minecraft.server.v1_8_R3") != null) {
            ActionBarAPI_v1_8_R3.sendActionBar(msg);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }

    public static void sendActionBar(Player p, String msg) {
        if (Package.getPackage("net.minecraft.server.v1_8_R3") != null) {
            ActionBarAPI_v1_8_R3.sendActionBar(p, msg);
        } else {
            throw new UnsupportedOperationException("Not supported yet");
        }
    }
}