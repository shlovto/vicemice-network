package net.vicemice.hector.gameapi.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameStartPrepareEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    public GameStartPrepareEvent() {}

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public HandlerList getHandlers() {
        return handlers;
    }
}
