package net.vicemice.hector.api.protocolversion.protocols.base;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.api.protocolversion.api.data.StoredObject;
import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.api.protocol.ProtocolPipeline;
import net.vicemice.hector.api.protocolversion.packets.State;

import java.util.UUID;

@Getter
@Setter
public class ProtocolInfo extends StoredObject {
    private State state = State.HANDSHAKE;
    private int protocolVersion = -1;
    private int serverProtocolVersion = -1;
    private String username;
    private UUID uuid;
    private ProtocolPipeline pipeline;

    public ProtocolInfo(UserConnection user) {
        super(user);
    }
}
