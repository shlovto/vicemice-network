package net.vicemice.hector.packets.types.player;

import lombok.Getter;
import net.vicemice.hector.utils.language.Language;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 08:40 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class LanguagePacket implements Serializable {

    @Getter
    private UUID uniqueId;
    @Getter
    private Language.LanguageType language;

    public LanguagePacket(UUID uniqueId, Language.LanguageType language) {
        this.uniqueId = uniqueId;
        this.language = language;
    }
}