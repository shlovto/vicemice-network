package net.vicemice.hector.service.tryjump.event;

import net.vicemice.hector.service.tryjump.TryJumpPlayer;

/*
 * Class created at 01:41 - 10.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class CoinsReceiveEvent extends TryJumpEvent {

    /** Gets the amount of coins given to a player. */
    public int getAmount() {
        return amount;
    }

    private int amount;

    public CoinsReceiveEvent(TryJumpPlayer player, int amount) {
        super(player);
        this.amount = amount;
    }
}
