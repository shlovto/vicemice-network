package net.vicemice.hector.api.protocolversion.protocols.base;

import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.api.platform.providers.Provider;
import net.vicemice.hector.api.protocolversion.api.protocol.ProtocolRegistry;

public class VersionProvider implements Provider {

    public int getServerProtocol(UserConnection connection) throws Exception {
        return ProtocolRegistry.SERVER_PROTOCOL;
    }
}
