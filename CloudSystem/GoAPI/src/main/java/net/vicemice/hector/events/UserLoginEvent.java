package net.vicemice.hector.events;

import net.vicemice.hector.utils.players.IUser;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/*
 * Class created at 02:27 - 03.05.2020
 * Copyright (C) elrobtossohn
 */
public class UserLoginEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private final IUser user;

    public UserLoginEvent(IUser user) {
        this.user = user;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public IUser getUser() {
        return user;
    }
}