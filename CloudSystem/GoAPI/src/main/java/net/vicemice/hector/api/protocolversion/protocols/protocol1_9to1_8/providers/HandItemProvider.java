package net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.providers;

import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.api.minecraft.item.Item;
import net.vicemice.hector.api.protocolversion.api.platform.providers.Provider;

public class HandItemProvider implements Provider {
    public Item getHandItem(final UserConnection info) {
        return new Item((short) 0, (byte) 0, (short) 0, null);
    }
}
