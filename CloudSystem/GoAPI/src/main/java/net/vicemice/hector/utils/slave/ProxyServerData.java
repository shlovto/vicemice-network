package net.vicemice.hector.utils.slave;

import java.io.Serializable;

/*
 * Class created at 01:11 - 05.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ProxyServerData implements Serializable {

    private String name, jarFile;

    public ProxyServerData(String name, String jarFile) {
        this.name = name;
        this.jarFile = jarFile;
    }

    public String getName() {
        return name;
    }

    public String getJarFile() {
        return jarFile;
    }
}