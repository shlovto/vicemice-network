package net.vicemice.hector.utils.players.api.npc.skin;

public enum SkinType {
    IDENTIFIER,
    MINESKINID,
    PLAYER;
}