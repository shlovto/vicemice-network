package net.vicemice.hector.utils.players.api.actionbat.version;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

/*
 * Class created at 16:59 - 13.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ActionBarAPI_v1_8_R3 {
    public static void sendActionBar(String msg) {
        IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a( "{\"text\": \"" + msg + "\"}" );
        PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte)2);
        for (Player p : Bukkit.getOnlinePlayers()) {
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(ppoc);
        }
    }

    public static void sendActionBar(Player p, String msg) {
        IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a( "{\"text\": \"" + msg + "\"}" );
        PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte)2);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(ppoc);
    }
}