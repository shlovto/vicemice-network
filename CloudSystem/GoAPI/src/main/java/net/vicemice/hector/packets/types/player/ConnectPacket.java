package net.vicemice.hector.packets.types.player;

import lombok.Getter;
import java.io.Serializable;

/*
 * Class created at 13:06 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ConnectPacket implements Serializable {

    @Getter
    private String name;
    @Getter
    private String server;

    public ConnectPacket(String name, String server) {
        this.name = name;
        this.server = server;
    }
}