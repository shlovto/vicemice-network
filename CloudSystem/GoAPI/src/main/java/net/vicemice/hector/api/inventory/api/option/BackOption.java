package net.vicemice.hector.api.inventory.api.option;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.api.inventory.api.InventoryMenu;
import net.vicemice.hector.api.inventory.api.LocalizedInventoryMenu;
import org.bukkit.entity.Player;

public class BackOption extends ActionOption {
    private InventoryMenu fallbackMenu;
    private LocalizedInventoryMenu localizedFallbackMenu;

    public BackOption() {
        super(null);
    }

    public BackOption(InventoryMenu fallbackMenu) {
        super(null);
        this.fallbackMenu = fallbackMenu;
    }

    public BackOption(LocalizedInventoryMenu fallbackMenu) {
        super(null);
        this.localizedFallbackMenu = fallbackMenu;
    }

    @Override
    public void onClick(Player player) {
        super.onClick(player);
        if (this.fallbackMenu != null) {
            this.fallbackMenu.open(player);
        } else {
            this.localizedFallbackMenu.open(player, GoAPI.getUserAPI().getLocale(player.getUniqueId()));
        }
    }

    public InventoryMenu getFallbackMenu() {
        return this.fallbackMenu;
    }

    public BackOption setFallbackMenu(InventoryMenu fallbackMenu) {
        this.fallbackMenu = fallbackMenu;
        return this;
    }

    public LocalizedInventoryMenu getLocalizedFallbackMenu() {
        return this.localizedFallbackMenu;
    }

    public BackOption setLocalizedFallbackMenu(LocalizedInventoryMenu localizedFallbackMenu) {
        this.localizedFallbackMenu = localizedFallbackMenu;
        return this;
    }
}