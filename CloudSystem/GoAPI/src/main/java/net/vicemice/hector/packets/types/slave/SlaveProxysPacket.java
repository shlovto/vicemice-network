package net.vicemice.hector.packets.types.slave;

import net.vicemice.hector.utils.slave.StaticServerData;
import java.io.Serializable;
import java.util.List;

/*
 * Class created at 18:40 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SlaveProxysPacket implements Serializable {
    private List<StaticServerData> data;

    public SlaveProxysPacket(List<StaticServerData> data) {
        this.data = data;
    }

    public List<StaticServerData> getData() {
        return data;
    }
}
