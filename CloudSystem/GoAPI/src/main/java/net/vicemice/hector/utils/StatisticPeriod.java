package net.vicemice.hector.utils;

import java.io.Serializable;

/*
 * Class created at 00:37 - 04.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public enum StatisticPeriod implements Serializable {
    DAY,
    MONTH,
    GLOBAL;
}