package net.vicemice.hector.packets.types.player.database;

import lombok.Getter;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 13:07 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GetPlayerDatabaseValue implements Serializable {

    @Getter
    private UUID uniqueId;
    @Getter
    private String key;
    @Getter
    private String callbackID;

    public GetPlayerDatabaseValue(UUID uniqueId, String key, String callbackID) {
        this.uniqueId = uniqueId;
        this.key = key;
        this.callbackID = callbackID;
    }
}
