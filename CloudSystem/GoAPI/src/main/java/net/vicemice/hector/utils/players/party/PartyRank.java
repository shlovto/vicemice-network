package net.vicemice.hector.utils.players.party;

import lombok.Getter;
import java.io.Serializable;

/**
 * Created by Paul B. on 15.02.2019.
 */
public enum PartyRank implements Serializable {
    LEADER("leader", 3),
    MODERATOR("moderator", 2),
    MEMBER("member", 1);

    @Getter
    private final String key;
    @Getter
    private final int accessLevel;

    PartyRank(String key, int accessLevel) {
        this.key = key;
        this.accessLevel = accessLevel;
    }

    public boolean isHigherEquals(PartyRank partyRank) {
        if (accessLevel >= partyRank.getAccessLevel()) return true;
        return false;
    }
}