package net.vicemice.hector.packets.types.player;

import java.io.Serializable;

/*
 * Class created at 09:40 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerLoginPacket implements Serializable {
    private String playerName;
    private boolean loggedIn;

    public PlayerLoginPacket(String playerName, boolean loggedIn) {
        this.playerName = playerName;
        this.loggedIn = loggedIn;
    }

    public String getPlayerName() {
        return this.playerName;
    }

    public boolean isLoggedIn() {
        return this.loggedIn;
    }
}
