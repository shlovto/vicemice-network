package net.vicemice.hector.api.protocolversion.dump;

import com.google.gson.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class DumpTemplate {
    private VersionInfo versionInfo;
    private Map<String, Object> configuration;
    private JsonObject platformDump;
    private JsonObject injectionDump;
}
