package net.vicemice.hector.packets.types.player.report;

import java.io.Serializable;

/*
 * Class created at 18:35 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class RemoveReportPacket implements Serializable {
    private String name;

    public RemoveReportPacket(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
