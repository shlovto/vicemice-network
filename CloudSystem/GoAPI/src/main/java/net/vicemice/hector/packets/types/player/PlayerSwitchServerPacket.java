package net.vicemice.hector.packets.types.player;

import java.io.Serializable;

/*
 * Class created at 08:53 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerSwitchServerPacket implements Serializable {
    private String name;
    private String serverName;

    public PlayerSwitchServerPacket(String name, String serverName) {
        this.name = name;
        this.serverName = serverName;
    }

    public String getName() {
        return this.name;
    }

    public String getServerName() {
        return this.serverName;
    }
}