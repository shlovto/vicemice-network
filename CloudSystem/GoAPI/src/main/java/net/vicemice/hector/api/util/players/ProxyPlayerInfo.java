package net.vicemice.hector.api.util.players;

import lombok.Getter;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 01:43 - 03.05.2020
 * Copyright (C) elrobtossohn
 */
@Getter
public class ProxyPlayerInfo implements Serializable {

    private final UUID uniqueId;
    private final String server;

    @ConstructorProperties(value={"uniqueId","server"})
    public ProxyPlayerInfo(UUID uniqueId, String server) {
        this.uniqueId = uniqueId;
        this.server = server;
    }
}
