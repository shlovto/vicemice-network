package net.vicemice.hector.packets.types.proxy;

import lombok.Getter;
import java.io.Serializable;

/*
 * Class created at 13:02 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ProxyAddServerPacket implements Serializable {

    @Getter
    private String name;
    @Getter
    private String host;
    @Getter
    private int port;

    public ProxyAddServerPacket(String name, String host, int port) {
        this.name = name;
        this.host = host;
        this.port = port;
    }
}
