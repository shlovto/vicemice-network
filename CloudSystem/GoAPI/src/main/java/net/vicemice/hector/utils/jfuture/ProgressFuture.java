package net.vicemice.hector.utils.jfuture;

import java.beans.ConstructorProperties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/*
 * Class created at 01:04 - 04.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public interface ProgressFuture<V> {
    public V get();

    public V getOr(V var1);

    public void get(AsyncCallback<V> var1);

    public void get(TimedAsyncCallback<V> var1);

    public V get(int var1, TimeUnit var2) throws TimeoutException;

    public V getOr(int var1, TimeUnit var2, V var3);

    public void get(AsyncCallback<V> var1, int var2, TimeUnit var3);

    public void get(TimedAsyncCallback<V> var1, int var2, TimeUnit var3);

    public boolean isDone();

    public boolean isSuccessful();

    public Exception getException();

    default public Future<V> asJavaFuture() {
        return new ProgressFutureJavaImpl<V>(this);
    }

    default public void throwErrors() throws Exception {
        this.throwErrors(new Exception());
    }

    default public void throwErrors(Exception _default) throws Exception {
        if (!this.isDone()) {
            return;
        }
        if (this.isSuccessful()) {
            return;
        }
        Exception ex = this.getException();
        if (ex == null) {
            throw _default;
        }
        throw ex;
    }

    public static interface AsyncCallback<T> {
        public void done(T var1, Exception var2);
    }

    public static class ProgressFutureJavaImpl<V>
            implements Future<V> {
        protected final ProgressFuture<V> handle;

        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            return false;
        }

        @Override
        public boolean isCancelled() {
            return false;
        }

        @Override
        public boolean isDone() {
            return this.handle.isDone();
        }

        @Override
        public V get() throws InterruptedException, ExecutionException {
            V ret = this.handle.get();
            if (!this.handle.isSuccessful()) {
                throw new ExecutionException(this.handle.getException());
            }
            return ret;
        }

        @Override
        public V get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            V ret = this.handle.get((int)timeout, unit);
            if (!this.handle.isSuccessful()) {
                throw new ExecutionException(this.handle.getException());
            }
            return ret;
        }

        @ConstructorProperties(value={"handle"})
        public ProgressFutureJavaImpl(ProgressFuture<V> handle) {
            this.handle = handle;
        }
    }

    public static interface TimedAsyncCallback<T> {
        public void done(T var1, Exception var2, long var3);
    }
}