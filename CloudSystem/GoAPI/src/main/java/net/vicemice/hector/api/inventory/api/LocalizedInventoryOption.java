package net.vicemice.hector.api.inventory.api;

import net.vicemice.hector.utils.locale.LocaleManager;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public abstract class LocalizedInventoryOption
        extends InventoryOption {
    private static final String DEFAULT_NAME = "this_is_a_default_value";
    private static final String[] DEFAULT_DESCRIPTION = new String[0];
    protected boolean translated = true;
    private Map<Locale, InventoryOption> localizedOptions = new HashMap<Locale, InventoryOption>();

    public InventoryOption createLocalized(LocaleManager localeManager, Locale locale) {
        if (!this.localizedOptions.containsKey(locale)) {
            final LocalizedInventoryOption instance = this;
            InventoryOption option = new InventoryOption(){

                @Override
                public void onClick(Player player) {
                    instance.onClick(player);
                }
            };
            option.setPosition(this.position);
            option.setCancelOnClick(this.cancelOnClick);
            option.setItemStack(new ItemStack(this.itemStack.getType(), this.itemStack.getAmount(), this.itemStack.getDurability()));
            if (this.translated) {
                ArrayList<String> localizedDescription = new ArrayList<String>();
                for (String descriptionLine : this.description) {
                    for (String addString : localeManager.getMessage(locale, descriptionLine, new Object[0]).split("\n")) {
                        localizedDescription.add(addString);
                    }
                }
                option.setDescription(localizedDescription);
                if (!this.name.equals(DEFAULT_NAME)) {
                    option.setName(localeManager.getMessage(locale, this.name, this.nameObjects));
                }
            } else {
                option.setDescription(this.description);
                option.setName(this.name);
            }
            this.localizedOptions.put(locale, option);
            return option;
        }
        return this.localizedOptions.get(locale);
    }

    public boolean isTranslated() {
        return this.translated;
    }

    public LocalizedInventoryOption setTranslated(boolean translated) {
        this.translated = translated;
        return this;
    }
}