package net.vicemice.hector.packets.types.player.abuse;

import lombok.Getter;
import net.vicemice.hector.utils.PunishReason;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 09:52 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PunishPacket implements Serializable {

    @Getter
    private UUID uuid;
    @Getter
    private PunishReason.Points banReason;
    @Getter
    private PunishReason.Points muteReason;
    @Getter
    private boolean autoEye;

    public PunishPacket(UUID uuid, PunishReason.Points banReason, PunishReason.Points muteReason, boolean autoEye) {
        this.uuid = uuid;
        this.banReason = banReason;
        this.muteReason = muteReason;
        this.autoEye = autoEye;
    }
}