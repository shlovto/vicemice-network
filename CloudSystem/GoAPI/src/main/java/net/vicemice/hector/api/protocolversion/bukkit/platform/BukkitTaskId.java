package net.vicemice.hector.api.protocolversion.bukkit.platform;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.vicemice.hector.api.protocolversion.api.platform.TaskId;

@Getter
@AllArgsConstructor
public class BukkitTaskId implements TaskId {
    private Integer object;
}
