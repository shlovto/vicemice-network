package net.vicemice.hector.packets.types.server;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:38 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class StartPrivateServerPacket implements Serializable {

    @Getter
    private UUID uuid;
    @Getter
    private String template;

    public StartPrivateServerPacket(UUID uuid, String template) {
        this.uuid = uuid;
        this.template = template;
    }
}
