package net.vicemice.hector.api.util;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

public class BukkitSchedulerHelper {
    public static BukkitTask runTask(Runnable runnable) {
        return Bukkit.getScheduler().runTask(Bukkit.getPluginManager().getPlugin("GoCore"), runnable);
    }

    public static int executeDelayed(long ticks, Runnable runnable) {
        return Bukkit.getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("GoCore"), runnable, ticks);
    }

    public static int repeat(long ticks, Runnable runnable) {
        return BukkitSchedulerHelper.repeatDelayed(0L, ticks, runnable);
    }

    public static int repeatDelayed(long delay, long ticks, Runnable runnable) {
        return Bukkit.getScheduler().scheduleSyncRepeatingTask(Bukkit.getPluginManager().getPlugin("GoCore"), runnable, delay, ticks);
    }

    public static BukkitTask runTaskAsync(Runnable runnable) {
        return Bukkit.getScheduler().runTaskAsynchronously(Bukkit.getPluginManager().getPlugin("GoCore"), runnable);
    }

    public static BukkitTask executeDelayedAsync(long ticks, Runnable runnable) {
        return Bukkit.getScheduler().runTaskLaterAsynchronously(Bukkit.getPluginManager().getPlugin("GoCore"), runnable, ticks);
    }

    public static BukkitTask repeatAsync(long ticks, Runnable runnable) {
        return BukkitSchedulerHelper.repeatDelayedAsync(0L, ticks, runnable);
    }

    public static BukkitTask repeatDelayedAsync(long delay, long ticks, Runnable runnable) {
        return Bukkit.getScheduler().runTaskTimerAsynchronously(Bukkit.getPluginManager().getPlugin("GoCore"), runnable, delay, ticks);
    }
}