package net.vicemice.hector.packets.types.player.coins;

import lombok.Getter;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 13:07 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GetCoinsPacket implements Serializable {

    @Getter
    private UUID uniqueId;
    @Getter
    private String callbackID;

    public GetCoinsPacket(UUID uniqueId, String callbackID) {
        this.uniqueId = uniqueId;
        this.callbackID = callbackID;
    }
}