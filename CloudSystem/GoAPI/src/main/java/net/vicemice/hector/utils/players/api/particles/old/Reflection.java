package net.vicemice.hector.utils.players.api.particles.old;

/*
 * Class created at 21:14 - 15.04.2020
 * Copyright (C) elrobtossohn
 */
public class Reflection {
    public static Class<?> getClass(PackageType type, String name) {
        Class<?> clazz = null;
        try {
            clazz = Class.forName(type.getPath() + ".v1_8_R3." + name);
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return clazz;
    }

    public static enum PackageType {
        NMS("net.minecraft.server"),
        OBC("org.bukkit.craftbukkit");

        private String path;

        private PackageType(String path) {
            this.path = path;
        }

        public String getPath() {
            return this.path;
        }
    }
}