package net.vicemice.hector.utils.players.api.title;

import io.netty.channel.Channel;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.PlayerTitlePacket;
import org.bukkit.Bukkit;

/*
 * Class created at 10:09 - 22.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class TitleListener extends PacketGetter {
    @Override
    public void receivePacket(PacketHolder initPacket) {
        if (initPacket.getKey().equals(PacketType.PLAYER_TITLE_PACKET.getKey())) {
            PlayerTitlePacket playerTitlePacket = (PlayerTitlePacket)initPacket.getValue();

            Title title = new Title(playerTitlePacket.getTitle(), playerTitlePacket.getSubtitle(), playerTitlePacket.getFadeInTime(), playerTitlePacket.getStayTime(), playerTitlePacket.getFadeOutTime());
            title.send(Bukkit.getPlayer(playerTitlePacket.getUniqueId()));
        }
    }

    @Override
    public void channelActive(Channel channel) {
    }
}
