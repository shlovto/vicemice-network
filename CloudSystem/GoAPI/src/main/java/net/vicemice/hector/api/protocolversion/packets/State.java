package net.vicemice.hector.api.protocolversion.packets;

public enum State {
    HANDSHAKE,
    STATUS,
    LOGIN,
    PLAY
}
