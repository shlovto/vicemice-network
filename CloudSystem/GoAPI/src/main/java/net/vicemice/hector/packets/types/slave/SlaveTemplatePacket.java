package net.vicemice.hector.packets.types.slave;

import net.vicemice.hector.utils.slave.TemplateData;

import java.io.Serializable;
import java.util.List;

/*
 * Class created at 18:42 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SlaveTemplatePacket implements Serializable {
    private List<TemplateData> templates;
    private boolean download;

    public SlaveTemplatePacket(List<TemplateData> templates, boolean download) {
        this.templates = templates;
        this.download = download;
    }

    public List<TemplateData> getTemplates() {
        return this.templates;
    }

    public boolean isDownload() {
        return this.download;
    }
}