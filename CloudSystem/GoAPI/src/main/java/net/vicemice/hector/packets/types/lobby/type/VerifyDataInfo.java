package net.vicemice.hector.packets.types.lobby.type;

import lombok.Getter;

import java.io.Serializable;

/*
 * Class created at 00:18 - 03.05.2020
 * Copyright (C) elrobtossohn
 */
public class VerifyDataInfo implements Serializable, Comparable<VerifyDataInfo> {
    @Getter
    private final String name;
    @Getter
    private final int id, players;

    public VerifyDataInfo(String name, int players) {
        this.name = name;
        this.id = Integer.parseInt(name.split("-")[1]);
        this.players = players;
    }

    @Override
    public int compareTo(VerifyDataInfo o) {
        return this.id < o.id ? -1 : (this.id > o.id ? 1 : 0);
    }
}