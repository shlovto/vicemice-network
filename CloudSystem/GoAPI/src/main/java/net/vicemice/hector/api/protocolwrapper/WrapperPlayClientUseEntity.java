package net.vicemice.hector.api.protocolwrapper;

import org.bukkit.util.Vector;
import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.events.PacketContainer;
import net.vicemice.hector.api.protocol.wrappers.EnumWrappers.EntityUseAction;

public class WrapperPlayClientUseEntity extends AbstractPacket {
	public static final PacketType TYPE;

	public WrapperPlayClientUseEntity() {
		super(new PacketContainer(TYPE), TYPE);
		this.handle.getModifier().writeDefaults();
	}

	public WrapperPlayClientUseEntity(PacketContainer packet) {
		super(packet, TYPE);
	}

	public int getTarget() {
		return (Integer)this.handle.getIntegers().read(0);
	}

	public void setTarget(int value) {
		this.handle.getIntegers().write(0, value);
	}

	public EntityUseAction getType() {
		return (EntityUseAction)this.handle.getEntityUseActions().read(0);
	}

	public void setType(EntityUseAction value) {
		this.handle.getEntityUseActions().write(0, value);
	}

	public Vector getTargetVector() {
		return (Vector)this.handle.getVectors().read(0);
	}

	public void setTargetVector(Vector value) {
		this.handle.getVectors().write(0, value);
	}

	static {
		TYPE = PacketType.Play.Client.USE_ENTITY;
	}
}
