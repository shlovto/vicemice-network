package net.vicemice.hector.api.protocolversion.api.type.types.minecraft;

import io.netty.buffer.ByteBuf;
import net.vicemice.hector.api.protocolversion.api.minecraft.Position;
import net.vicemice.hector.api.protocolversion.api.type.Type;

public class Position1_14Type extends Type<Position> {
    public Position1_14Type() {
        super(Position.class);
    }

    @Override
    public Position read(ByteBuf buffer) {
        long val = buffer.readLong();

        long x = (val >> 38);
        long y = val << 52 >> 52;
        long z = val << 26 >> 38;

        return new Position(x, y, z);
    }

    @Override
    public void write(ByteBuf buffer, Position object) {
        buffer.writeLong(((object.getX() & 0x3ffffff) << 38) | (object.getY() & 0xfff) | ((object.getZ() & 0x3ffffff) << 12));
    }
}
