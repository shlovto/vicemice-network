package net.vicemice.hector.packets.types.player.replay;

import lombok.Getter;

import java.io.Serializable;

/*
 * Class created at 09:46 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ReplayServerPacket implements Serializable {
    @Getter
    private ReplayDataPacket dataPacket;
    @Getter
    private boolean insert;

    public ReplayServerPacket(ReplayDataPacket dataPacket, boolean insert) {
        this.dataPacket = dataPacket;
        this.insert = insert;
    }
}