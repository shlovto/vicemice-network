package net.vicemice.hector.packets.types.player.edit;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 22:45 - 09.04.2020
 * Copyright (C) elrobtossohn
 */
public class MapEditRequestPacket implements Serializable {
    @Getter
    private UUID uuid;

    public MapEditRequestPacket(UUID uuid) {
        this.uuid = uuid;
    }
}