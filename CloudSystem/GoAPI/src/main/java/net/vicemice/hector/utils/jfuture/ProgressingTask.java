package net.vicemice.hector.utils.jfuture;

import java.beans.ConstructorProperties;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;

/*
 * Class created at 01:05 - 04.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public abstract class ProgressingTask<ReturnType> extends ObjectProgressFuture<ReturnType> {
    private boolean running = false;
    private boolean canceled = false;
    private Thread thandle;
    private final Callable<ReturnType> call;

    public final synchronized ProgressingTask<ReturnType> execurteTask() {
        if (this.running || this.isDone()) {
            return this;
        }
        this.running = true;
        this.thandle = new Thread(() -> {
            try {
                try {
                    this.done(this.execute());
                }
                catch (Exception e) {
                    this.error(e);
                    this.running = false;
                }
            }
            finally {
                this.running = false;
            }
        });
        this.thandle.start();
        return this;
    }

    public ProgressingTask() {
        this.call = null;
    }

    protected ReturnType execute() throws Exception {
        if (this.call == null) {
            throw new UnsupportedOperationException();
        }
        return this.call.call();
    }

    public ProgressingTask<ReturnType> waitFor() throws InterruptedException {
        while (this.running) {
            Thread.sleep(10L);
        }
        return this;
    }

    public boolean cancel() {
        if (this.thandle == null) {
            return false;
        }
        this.canceled = true;
        this.thandle.stop();
        this.error(new CancellationException("Execution canceled!"));
        return true;
    }

    @Override
    public Future<ReturnType> asJavaFuture() {
        return new ProgressFuture.ProgressFutureJavaImpl<ReturnType>(this){

            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                return ((ProgressingTask)this.handle).cancel();
            }

            @Override
            public boolean isCancelled() {
                return ((ProgressingTask)this.handle).isCanceled();
            }
        };
    }

    public boolean isRunning() {
        return this.running;
    }

    public boolean isCanceled() {
        return this.canceled;
    }

    @ConstructorProperties(value={"call"})
    public ProgressingTask(Callable<ReturnType> call) {
        this.call = call;
    }
}
