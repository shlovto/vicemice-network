package net.vicemice.hector.packets.types.lobby;

import lombok.Getter;
import java.io.Serializable;
import java.util.HashMap;

/*
 * Class created at 18:20 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class LobbyTopPacket implements Serializable {

    @Getter
    private HashMap<String, String[]> topPlayers;

    public LobbyTopPacket(HashMap<String, String[]> topPlayers) {
        this.topPlayers = topPlayers;
    }
}