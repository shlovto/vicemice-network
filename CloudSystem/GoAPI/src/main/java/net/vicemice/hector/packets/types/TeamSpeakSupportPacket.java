package net.vicemice.hector.packets.types;

import java.io.Serializable;

/*
 * Class created at 06:12 - 22.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class TeamSpeakSupportPacket implements Serializable {
    private int players;

    public TeamSpeakSupportPacket(int players) {
        this.players = players;
    }

    public int getPlayers() {
        return this.players;
    }
}
