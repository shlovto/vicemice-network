package net.vicemice.hector.packets.types.server;

import java.io.Serializable;

/*
 * Class created at 12:39 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class RunCommandPacket implements Serializable {
    private String name;
    private String command;

    public RunCommandPacket(String name, String command) {
        this.name = name;
        this.command = command;
    }

    public String getName() {
        return this.name;
    }

    public String getCommand() {
        return this.command;
    }
}
