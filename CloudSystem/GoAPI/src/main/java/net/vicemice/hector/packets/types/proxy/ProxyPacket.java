package net.vicemice.hector.packets.types.proxy;

import lombok.Getter;
import net.vicemice.hector.api.util.players.ProxyPlayerInfo;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/*
 * Class created at 13:03 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ProxyPacket implements Serializable {

    @Getter
    private final String name;
    @Getter
    private final String ipAddress;
    @Getter
    private final int players;
    @Getter
    private final int port;
    @Getter
    private final List<ProxyPlayerInfo> onlineCloudPlayers;

    public ProxyPacket(String name, int players, List<ProxyPlayerInfo> onlineCloudPlayers, String ipAddress, int port) {
        this.name = name;
        this.players = players;
        this.onlineCloudPlayers = onlineCloudPlayers;
        this.ipAddress = ipAddress;
        this.port = port;
    }
}