package net.vicemice.hector.service.tryjump.event;

import net.vicemice.hector.service.tryjump.TryJumpPlayer;
import net.vicemice.hector.service.tryjump.Unit;

/*
 * Class created at 01:48 - 10.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class UnitToggleLiteEvent extends TryJumpEvent {

    private Unit currentUnit;

    public UnitToggleLiteEvent(TryJumpPlayer player, Unit unit) {
        super(player);
        currentUnit = unit;
    }

    public Unit getCurrentUnit() {
        return currentUnit;
    }
}
