package net.vicemice.hector.api.async;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Future<T> {
    private boolean done = false;
    private boolean success = false;
    private T result = null;
    private Throwable reason = null;
    private boolean cancelled = false;
    private final Object monitor = new Object();

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isDone() {
        Object object = this.monitor;
        synchronized (object) {
            return this.done;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isSuccess() {
        Object object = this.monitor;
        synchronized (object) {
            return this.success;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isError() {
        Object object = this.monitor;
        synchronized (object) {
            return !this.success;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isCancelled() {
        Object object = this.monitor;
        synchronized (object) {
            return this.cancelled;
        }
    }

    public T get() throws InterruptedException, ExecutionException {
        Object object = this.monitor;
        synchronized (object) {
            while (!this.done) {
                this.monitor.wait();
            }
            if (this.cancelled) {
                throw new InterruptedException();
            }
            if (this.success) {
                return this.result;
            }
            if (this.reason != null) {
                throw new ExecutionException(this.reason);
            }
            throw new ExecutionException(new Exception("Failed to resolve future for unknown reason!"));
        }
    }

    public T get(long duration, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        long start = System.nanoTime();
        long end = start + unit.toNanos(duration);
        Object object = this.monitor;
        synchronized (object) {
            while (!this.done && start < end) {
                long timeout = (end - start) / 1000000L;
                int nanos = (int)(end - start - timeout * 1000000L);
                this.monitor.wait(timeout, nanos);
                start = System.nanoTime();
            }
            if (!this.done) {
                throw new TimeoutException();
            }
            if (this.cancelled) {
                throw new InterruptedException();
            }
            if (this.success) {
                return this.result;
            }
            if (this.reason != null) {
                throw new ExecutionException(this.reason);
            }
            throw new ExecutionException(new Exception("Failed to resolve future for unknown reason!"));
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void resolve(T result) {
        Object object = this.monitor;
        synchronized (object) {
            if (this.done) {
                return;
            }
            this.done = true;
            this.success = true;
            this.result = result;
            this.monitor.notifyAll();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void fail(Throwable reason) {
        Object object = this.monitor;
        synchronized (object) {
            if (this.done) {
                return;
            }
            this.done = true;
            this.success = false;
            this.reason = reason;
            this.monitor.notifyAll();
        }
    }

    public void cancel() {
    }
}
