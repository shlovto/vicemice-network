package net.vicemice.hector.utils.players.lobby;

import lombok.Getter;
import net.vicemice.hector.utils.Rank;

import java.io.Serializable;

/*
 * Class created at 17:56 - 11.03.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Getter
public class LobbyRequestData implements Serializable {

    public LobbyRequestData(String name, Rank rank, String value, String signature) {
        this.name = name;
        this.rank = rank;
        this.value = value;
        this.signature = signature;
    }

    private String name;
    private Rank rank;

    private String value;
    private String signature;
}
