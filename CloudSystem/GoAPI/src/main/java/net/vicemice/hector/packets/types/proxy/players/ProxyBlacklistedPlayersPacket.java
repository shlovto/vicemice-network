package net.vicemice.hector.packets.types.proxy.players;

import lombok.Getter;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class ProxyBlacklistedPlayersPacket implements Serializable {

    @Getter
    private List<UUID> players;

    public ProxyBlacklistedPlayersPacket(List<UUID> players) {
        this.players = players;
    }
}