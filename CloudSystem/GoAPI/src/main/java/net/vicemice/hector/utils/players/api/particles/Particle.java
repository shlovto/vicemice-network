package net.vicemice.hector.utils.players.api.particles;

/*
 * Enum created at 10:30 - 02.04.2020
 * Copyright (C) elrobtossohn
 */
public enum Particle {
    DEATH_CLOUD(0, "explode", 1.0E-4f, 10),
    EXPLOSION(1, "largeexplode", 0.1f, 1),
    HUGE_EXPLOSION(2, "hugeexplosion", 0.1f, 1),
    FIREWORKS_SPARK(3, "fireworksSpark", 0.0f, 10),
    BUBBLE(4, "bubble", 0.4f, 50),
    SPLASH(5, "splash", 1.0f, 40),
    WAKE(6, "wake", 0.1f, 50),
    SUSPENDED(7, "suspended", 0.0f, 50),
    DEPTH_SUSPEND(8, "depthsuspend", 0.0f, 100),
    CRITICAL(9, "crit", -1.0E-4f, 10),
    MAGIC_CRITICAL(10, "magicCrit", -1.0E-4f, 100),
    SMALL_SMOKE(11, "smoke", 0.05f, 100),
    SMOKE(12, "largesmoke", 0.0f, 10),
    SPELL(13, "spell", 0.05f, 50),
    INSTANT_SPELL(14, "instantSpell", 0.05f, 50),
    SPELL_MOB(15, "mobSpell", 1.0f, 100),
    SPELL_MOB_AMBIENT(16, "mobSpellAmbient", 1.0f, 100),
    WITCH_MAGIC(17, "witchMagic", 0.0f, 10),
    WATER_DRIP(18, "dripWater", 0.0f, 100),
    LAVA_DRIP(19, "dripLava", 0.0f, 100),
    ANGRY_VILLAGER(20, "angryVillager", 0.0f, 10),
    SPARKLE(21, "happyVillager", 0.0f, 10),
    VOID(22, "townaura", 1.0f, 100),
    NOTE(23, "note", 0.0f, 3),
    PORTAL(24, "portal", 0.0f, 30),
    MAGIC_RUNES(25, "enchantmenttable", 0.0f, 10),
    FIRE(26, "flame", 0.0f, 20),
    LAVA_SPARK(27, "lava", 0.0f, 4),
    FOOTSTEP(28, "footstep", 0.0f, 10),
    CLOUD(29, "cloud", 0.0f, 10),
    RED_SMOKE(30, "reddust", 0.0f, 40),
    RAINBOW_SMOKE(30, "reddust", 1.0f, 100),
    SNOWBALL(32, "snowballpoof", 1.0f, 20),
    SNOW_SHOVEL(32, "snowshovel", 0.02f, 30),
    SLIME_SPLAT(33, "slime", 0.0f, 20),
    HEART(34, "heart", 0.0f, 2),
    BARRIER(35, "barrier", 0.0f, 1),
    WATER_DROPLET(39, "droplet", 0.0f, 10),
    ITEM_TAKE(40, "take", 0.0f, 1),
    GUARDIAN_APPEARANCE(41, "mobappearance", 0.0f, 1),
    ICON_BREAK(36, "iconcrack", 0.1f, 100),
    BLOCK_BREAK(37, "blockcrack", 0.1f, 100),
    BLOCK_DUST(38, "blockdust", 0.1f, 100);

    private int id;
    private String name;
    private float speed;
    private int amount;

    Particle(int id, String name, float speed, int amount) {
        this.id = id;
        this.name = name;
        this.speed = speed;
        this.amount = amount;
    }

    public EffectBuilder builder() {
        return new EffectBuilder(this, this.speed, this.amount);
    }

    public int getId() {
        return this.id;
    }

    @Deprecated
    public String getName() {
        return this.name;
    }

    public float getSpeed() {
        return this.speed;
    }

    public int getAmount() {
        return this.amount;
    }

    public static Particle fromName(String name) {
        for (Particle type : Particle.values()) {
            if (!type.name.equalsIgnoreCase(name)) continue;
            return type;
        }
        return null;
    }
}