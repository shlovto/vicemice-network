package net.vicemice.hector.utils;

import lombok.Getter;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Getter
public class PunishData implements Serializable {

    public PunishData(String reason, long end, UUID punisher, List<String> proofs) {
        this.reason = reason;
        this.end = end;
        this.punisher = punisher;
        this.proofs = proofs;
    }

    private String reason;
    private long end;
    private UUID punisher;
    private List<String> proofs;
}