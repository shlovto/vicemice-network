package net.vicemice.hector.utils.players.api.hologram;

import net.md_5.bungee.api.ChatColor;
import net.vicemice.hector.utils.players.api.hologram.nbt.Hologram_v1_8_R3;
import org.bukkit.Bukkit;

import java.util.logging.Level;

/*
 * Class created at 06:32 - 09.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class HologramAPI {

    private static String version;

    private static void setupVersion() {
        try {
            version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
        } catch (ArrayIndexOutOfBoundsException ex) {
            ex.printStackTrace();
        }
    }

    public static Hologram getNewHologram() {
        if (version == null) {
            setupVersion();
        }
        if (version.equals("v1_8_R3")) {
            return new Hologram_v1_8_R3();
        } else {
            Bukkit.getLogger().log(Level.SEVERE, ChatColor.RED + "Unsupported server version.");
            return null;
        }
    }
}