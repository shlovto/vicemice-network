package net.vicemice.hector.utils;

import lombok.Getter;

/*
 * Class created at 09:02 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PunishReason {

    public enum Points {
        ADVERT("Werbung", "Advert", 39, 0),
        HACKING("Hacking", "Hacking", 54, 0, true),
        MODIFICATIONS("Unerlaubte Clientmodifikation", "Forbidden Clientmodifications", 45, 0, true),
        MODIFICATIONS_PACKET("Unerlaubte Clientmodifikation", "Forbidden Clientmodifications", 1, 0, true, false),
        BUGUSING("Bugusing", "Bugusing", 21, 0, true),
        USERNAME("Username", "Username", 45, 0, true),
        SKIN("Skin", "Skin", 24, 0, true),
        PLAYINGBEHAVIOUR("Spielverhalten", "playing behaviour", 24, 0, true),
        RANDOMKILLING("Random Killing in TTT", "Random Killing in TTT", 12, 0, true),

        BANBYPASS("Bannumgehung", "Ban bypass", 54, 0),

        SHORTENING("Verkürzung", "Shortening", 30, 0, true, false),

        INSULT("Beleidigung", "Insult", 0, 3),
        SPAM("Spam", "Spam", 0, 3),
        PROVOKATION("Provokation", "Provocation", 0, 3),
        THREAT("Drohung", "Threat", 0, 39),
        RACISM("Rassismus", "Racism", 0, 39);

        @Getter
        private String name, name2;
        @Getter
        private int banPoints, mutePoints;
        @Getter
        private boolean moderation, viewAble;

        public boolean isBan() {
            if (banPoints > 0) {
                return true;
            }
            return false;
        }

        public boolean isMute() {
            if (mutePoints > 0) {
                return true;
            }
            return false;
        }

        Points(String name, String name2, int banPoints, int mutePoints) {
            this.name = name;
            this.name2 = name2;
            this.banPoints = banPoints;
            this.mutePoints = mutePoints;
            this.moderation = false;
            this.viewAble = true;
        }

        Points(String name, String name2, int banPoints, int mutePoints, boolean moderation) {
            this.name = name;
            this.name2 = name2;
            this.banPoints = banPoints;
            this.mutePoints = mutePoints;
            this.moderation = moderation;
            this.viewAble = true;
        }

        Points(String name, String name2, int banPoints, int mutePoints, boolean moderation, boolean viewAble) {
            this.name = name;
            this.name2 = name2;
            this.banPoints = banPoints;
            this.mutePoints = mutePoints;
            this.moderation = moderation;
            this.viewAble = viewAble;
        }

        public static Points getPoint(String cause) {
            for (Points points : values()) {
                if (cause.equalsIgnoreCase(points.name())) {
                    return points;
                }
            }
            return null;
        }
    }

    public enum Bans {
        WERBUNG("Werbung", "Advert", 30),
        HACKING("Hacking", "Hacking", 12),
        BUGUSING("Bugusing", "Bugusing", 12),
        USERNAME("Username", "Username", 30),
        SKIN("Skin", "Skin", 30),
        BANNUMGEHUNG("Bannumgehung", "Bann bypass", 30);

        private String name, name2;
        private int points;

        Bans(String name, String name2, int points) {
            this.name = name;
            this.name2 = name2;
            this.points = points;
        }

        public String getName() {
            return name;
        }

        public String getName2() {
            return name2;
        }

        public int getPoints() {
            return points;
        }

        public static Bans getPoints(String cause) {
            for (Bans points : values()) {
                if (cause.equalsIgnoreCase(points.name())) {
                    return points;
                }
            }
            return null;
        }
    }

    public enum Mutes {
        BELEIDIGUNG("Beleidigung", "insult", 2),
        SPAM("Spam", "Spam", 1),
        PROVOKATION("Provokation", "provocation", 2),
        DROHUNG("Drohung", "threat", 6),
        RASSISMUS("Rassismus", "racism", 6),
        WERBUNG("Werbung", "advert", 30);

        private String name, name2;
        private int points;

        Mutes(String name, String name2, int points) {
            this.name = name;
            this.name2 = name2;
            this.points = points;
        }

        public String getName() {
            return name;
        }

        public String getName2() {
            return name2;
        }

        public int getPoints() {
            return points;
        }

        public static Mutes getPoints(String cause) {
            for (Mutes points : values()) {
                if (cause.equalsIgnoreCase(points.name())) {
                    return points;
                }
            }
            return null;
        }
    }
}