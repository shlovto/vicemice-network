package net.vicemice.hector.packets.types.proxy;

import lombok.Getter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*
 * Class created at 13:05 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ProxySkinPacket implements Serializable {

    @Getter
    private List<String[]> skinData = new ArrayList<String[]>();

    public ProxySkinPacket(List<String[]> skinData) {
        this.skinData = skinData;
    }
}