package net.vicemice.hector.packets.types.player.level;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 15:26 - 07.04.2020
 * Copyright (C) elrobtossohn
 */
public class EditXPPacket implements Serializable {

    @Getter
    private UUID uniqueId;
    @Getter
    private long xp;
    @Getter
    private boolean remove;

    public EditXPPacket(UUID uniqueId, long xp, boolean remove) {
        this.uniqueId = uniqueId;
        this.xp = xp;
        this.remove = remove;
    }
}
