package net.vicemice.hector.api.protocolversion.api.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class StoredObject {
    private UserConnection user;
}
