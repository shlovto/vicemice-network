package net.vicemice.hector.packets.types;

import lombok.Getter;
import java.io.Serializable;

public class UpdateLocalePacket implements Serializable {

    @Getter
    private Long millis;

    public UpdateLocalePacket(Long millis) {
        this.millis = millis;
    }
}
