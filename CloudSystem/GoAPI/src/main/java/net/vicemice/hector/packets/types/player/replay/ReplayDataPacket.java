package net.vicemice.hector.packets.types.player.replay;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 09:41 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ReplayDataPacket implements Serializable {
    @Getter
    private UUID uid;
    @Getter
    private String gameID;
    @Getter
    private String game;
    @Getter
    private String map;
    @Getter
    @Setter
    private boolean available;

    public ReplayDataPacket(UUID uid, String gameID, String game, String map, boolean available) {
        this.uid = uid;
        this.gameID = gameID;
        this.game = game;
        this.map = map;
        this.available = available;
    }
}
