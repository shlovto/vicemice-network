package net.vicemice.hector.packets.types.server.mapedit;

import lombok.Getter;
import java.io.Serializable;
import java.util.UUID;

public class MapEditInfoPacket implements Serializable {

    @Getter
    private UUID uniqueId;
    @Getter
    private String template, map;

    public MapEditInfoPacket(UUID uniqueId, String template, String map) {
        this.uniqueId = uniqueId;
        this.template = template;
        this.map = map;
    }
}
