package net.vicemice.hector.packets.types.server.clan;

import lombok.Getter;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:45 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ClanInformationPacket implements Serializable {
    @Getter
    private UUID uniqueId;
    @Getter
    private String id;
    @Getter
    private UUID uuid1;
    @Getter
    private String name;
    @Getter
    private String tag;
    @Getter
    private String members;

    @ConstructorProperties(value = {"uniqueId", "id", "uuid1", "name", "tag", "members"})
    public ClanInformationPacket(UUID uniqueId, String id, UUID uuid1, String name, String tag, String members) {
        this.uniqueId = uniqueId;
        this.id = id;
        this.uuid1 = uuid1;
        this.name = name;
        this.tag = tag;
        this.members = members;
    }

    @ConstructorProperties(value = {"uniqueId"})
    public ClanInformationPacket(UUID uniqueId) {
        this.uniqueId = uniqueId;
        this.id = null;
        this.uuid1 = null;
        this.name = null;
        this.tag = null;
        this.members = null;
    }
}