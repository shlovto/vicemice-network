package net.vicemice.hector.packets.types.proxy;

import lombok.Getter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*
 * Class created at 13:04 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ProxyRankPacket implements Serializable {

    @Getter
    private List<String[]> ranks = new ArrayList<String[]>();

    public ProxyRankPacket(List<String[]> ranks) {
        this.ranks = ranks;
    }
}