package net.vicemice.hector.utils.players.api.sound;

import io.netty.channel.Channel;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.PlayerSoundPacket;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/*
 * Class created at 14:27 - 07.04.2020
 * Copyright (C) elrobtossohn
 */
public class SoundListener extends PacketGetter {

    @Override
    public void receivePacket(PacketHolder initPacket) {
        if (initPacket.getKey().equals(PacketType.PLAYER_SOUND_PACKET.getKey())) {
            PlayerSoundPacket playerSoundPacket = (PlayerSoundPacket)initPacket.getValue();

            Player player = Bukkit.getPlayer(playerSoundPacket.getUniqueId());

            if (player != null) {
                player.playSound(player.getLocation(), Sound.valueOf(playerSoundPacket.getSound().name()), 10L, 10L);
            }
        }
    }

    @Override
    public void channelActive(Channel channel) {
    }
}
