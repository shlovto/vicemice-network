package net.vicemice.hector.api.inventory.api;

import lombok.NonNull;
import net.vicemice.hector.api.async.Callback;
import net.vicemice.hector.utils.locale.LocaleManager;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class LocalizedInventoryMenu {
    private LocaleManager localeManager;
    private String title;
    private int size;
    private final boolean saveForever;
    private final Object[] titleObjects;
    private final Map<Locale, InventoryMenu> inventories = new HashMap<Locale, InventoryMenu>();

    public LocalizedInventoryMenu(LocaleManager localeManager, String title, int size, boolean saveForever, Object ... titleObjects) {
        this.localeManager = localeManager;
        this.title = title;
        this.size = size;
        this.saveForever = saveForever;
        this.titleObjects = titleObjects;
        this.createInventories();
    }

    public LocalizedInventoryMenu(LocaleManager localeManager, String title, int size, boolean saveForever) {
        this.localeManager = localeManager;
        this.title = title;
        this.size = size;
        this.saveForever = saveForever;
        this.titleObjects = new Object[0];
        this.createInventories();
    }

    public LocalizedInventoryMenu addOption(@NonNull LocalizedInventoryOption option) {
        if (option == null) {
            throw new NullPointerException("option");
        }
        for (Map.Entry<Locale, InventoryMenu> entry : this.inventories.entrySet()) {
            entry.getValue().addOption(option.createLocalized(this.localeManager, entry.getKey()));
        }
        return this;
    }

    public LocalizedInventoryMenu removeOption(@NonNull LocalizedInventoryOption option) {
        if (option == null) {
            throw new NullPointerException("option");
        }
        for (Map.Entry<Locale, InventoryMenu> entry : this.inventories.entrySet()) {
            entry.getValue().addOption(option.createLocalized(this.localeManager, entry.getKey()));
        }
        return this;
    }

    public LocalizedInventoryMenu addItem(List<ItemStack> itemStacks) {
        for (Map.Entry<Locale, InventoryMenu> entry : this.inventories.entrySet()) {
            ArrayList<ItemStack> copied = new ArrayList<ItemStack>();
            for (ItemStack itemStack : itemStacks) {
                copied.add(new ItemStack(itemStack.getType(), itemStack.getAmount(), itemStack.getDurability()));
            }
            entry.getValue().addItem(copied);
        }
        return this;
    }

    public LocalizedInventoryMenu addItem(ItemStack ... itemStacks) {
        return this.addItem(Arrays.asList(itemStacks));
    }

    public LocalizedInventoryMenu addItem(LocalizedInventoryOption ... options) {
        for (Map.Entry<Locale, InventoryMenu> entry : this.inventories.entrySet()) {
            InventoryOption[] localizedOptions = new InventoryOption[options.length];
            for (int i = 0; i < options.length; ++i) {
                localizedOptions[i] = options[i].createLocalized(this.localeManager, entry.getKey());
            }
            entry.getValue().addItem(localizedOptions);
        }
        return this;
    }

    public InventoryMenu getInventoryMenu(Locale locale) {
        return this.inventories.get(locale);
    }

    public LocalizedInventoryMenu setSize(int size) {
        this.size = size;
        for (Map.Entry<Locale, InventoryMenu> entry : this.inventories.entrySet()) {
            entry.getValue().setSize(this.size);
        }
        return this;
    }

    public LocalizedInventoryMenu open(Player player, Locale locale) {
        InventoryMenu menu = this.inventories.get(locale);
        menu.open(player);
        return this;
    }

    public LocalizedInventoryMenu onClose(Callback<Void> callback) {
        for (Map.Entry<Locale, InventoryMenu> entry : this.inventories.entrySet()) {
            entry.getValue().onClose(callback);
        }
        return this;
    }

    private void createInventories() {
        for (Locale locale : this.localeManager.getLocales().keySet()) {
            String localizedTitle = this.localeManager.getMessage(locale, this.title, this.titleObjects);
            this.inventories.put(locale, new InventoryMenu(localizedTitle, this.size, this.saveForever));
        }
    }

    public LocaleManager getLocaleManager() {
        return this.localeManager;
    }

    public void setLocaleManager(LocaleManager localeManager) {
        this.localeManager = localeManager;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSize() {
        return this.size;
    }

    public boolean isSaveForever() {
        return this.saveForever;
    }
}