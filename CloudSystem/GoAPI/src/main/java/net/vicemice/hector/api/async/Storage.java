package net.vicemice.hector.api.async;

public class Storage<T> {
    private T object = null;

    public Storage() {
    }

    public Storage(T object) {
        this.object = object;
    }

    public void set(T object) {
        this.object = object;
    }

    public T get() {
        return this.object;
    }
}
