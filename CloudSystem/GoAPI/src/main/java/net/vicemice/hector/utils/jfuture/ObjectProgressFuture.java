package net.vicemice.hector.utils.jfuture;

import net.vicemice.hector.packets.types.player.stats.StatisticPlayerInfoPacket;

/*
 * Class created at 01:05 - 04.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public abstract class ObjectProgressFuture<V> extends BasicProgressFuture<V> {
    private V obj = null;
    private boolean done = false;
    private Exception exception;

    @Override
    public boolean isDone() {
        return this.done;
    }

    @Override
    public boolean isSuccessful() {
        return this.isDone() && this.exception == null;
    }

    @Override
    public Exception getException() {
        return this.exception;
    }

    @Override
    public V get() {
        while (!this.isDone()) {
            try {
                Thread.sleep(5L);
            }
            catch (Exception exception) {
                // empty catch block
            }
        }
        return this.obj;
    }

    protected void done(V obj) {
        this.obj = obj;
        this.done = true;
    }

    public abstract void done(StatisticPlayerInfoPacket pkt);

    protected void error(Exception ex) {
        this.exception = ex;
        this.done = true;
    }
}
