package net.vicemice.hector.packets.types.lobby;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:19 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class LobbyLocPacket implements Serializable {

    @Getter
    private UUID uniqueId;
    @Getter
    private double x;
    @Getter
    private double y;
    @Getter
    private double z;
    @Getter
    private float yaw;
    @Getter
    private float pitch;

    public LobbyLocPacket(UUID uniqueId, double x, double y, double z, float yaw, float pitch) {
        this.uniqueId = uniqueId;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }
}