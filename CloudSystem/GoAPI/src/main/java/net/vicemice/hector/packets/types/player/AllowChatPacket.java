package net.vicemice.hector.packets.types.player;

import lombok.Getter;

import java.io.Serializable;

/*
 * Class created at 08:41 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class AllowChatPacket implements Serializable {

    @Getter
    private final String player;

    public AllowChatPacket(String player) {
        this.player = player;
    }
}