package net.vicemice.hector.packets.types.player.stats;

import lombok.Getter;
import java.io.Serializable;

/*
 * Class created at 13:08 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GetPlayerStatsPacket implements Serializable {

    @Getter
    private String forPlayer;
    @Getter
    private String statsPlayer;

    public GetPlayerStatsPacket(String forPlayer, String statsPlayer) {
        this.forPlayer = forPlayer;
        this.statsPlayer = statsPlayer;
    }
}