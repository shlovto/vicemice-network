package net.vicemice.hector.packets.types.proxy;

import lombok.Getter;

import java.io.Serializable;

/*
 * Class created at 13:06 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ProxyStopPacket implements Serializable {

    @Getter
    private String name;

    public ProxyStopPacket(String name) {
        this.name = name;
    }
}