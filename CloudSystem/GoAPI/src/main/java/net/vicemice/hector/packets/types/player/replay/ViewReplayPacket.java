package net.vicemice.hector.packets.types.player.replay;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:38 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ViewReplayPacket implements Serializable {

    @Getter
    private final UUID uniqueId;
    @Getter
    private final PacketReplayEntries.PacketReplayEntry replayEntry;

    public ViewReplayPacket(UUID uniqueId, PacketReplayEntries.PacketReplayEntry replayEntry) {
        this.uniqueId = uniqueId;
        this.replayEntry = replayEntry;
    }
}


/*
 * Class created at 18:38 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
/*public class ViewReplayPacket implements Serializable {

    @Getter
    private String name;
    @Getter
    private UUID uid;
    @Getter
    private String gameID;
    @Getter
    private String game;
    @Getter
    private String map;
    @Getter
    @Setter
    private boolean available;

    public ViewReplayPacket(String name, UUID uid, String gameID, String game, String map, boolean available) {
        this.name = name;
        this.uid = uid;
        this.gameID = gameID;
        this.game = game;
        this.map = map;
        this.available = available;
    }
}*/
