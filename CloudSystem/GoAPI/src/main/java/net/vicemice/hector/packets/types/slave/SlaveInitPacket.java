package net.vicemice.hector.packets.types.slave;

import java.io.Serializable;

/*
 * Class created at 18:40 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SlaveInitPacket implements Serializable {
    private String host;
    private String name;
    private int maxRam;
    private String forceTemplate;

    public SlaveInitPacket(String host, String name, int maxRam) {
        this.host = host;
        this.name = name;
        this.maxRam = maxRam;
    }

    public String getHost() {
        return this.host;
    }

    public String getName() {
        return this.name;
    }

    public int getMaxRam() {
        return this.maxRam;
    }

    public String getForceTemplate() {
        return this.forceTemplate;
    }

    public void setForceTemplate(String forceTemplate) {
        this.forceTemplate = forceTemplate;
    }
}
