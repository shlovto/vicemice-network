package net.vicemice.hector.packets.types.player.labymod;

import lombok.Getter;
import net.vicemice.hector.packets.types.player.party.PartyPacket;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:28 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class LabyModPacket implements Serializable {
    @Getter
    private UUID uuid;
    @Getter
    private boolean hasParty;
    @Getter
    private PartyPacket partyPacket;
    @Getter
    private UUID joinSecret, matchSecret, specSecret;

    public LabyModPacket(UUID uuid, boolean hasParty, PartyPacket partyPacket, UUID joinSecret, UUID matchSecret, UUID specSecret) {
        this.uuid = uuid;
        this.hasParty = hasParty;
        if (hasParty) {
            this.partyPacket = partyPacket;
        }

        this.joinSecret = joinSecret;
        this.matchSecret = matchSecret;
        this.specSecret = specSecret;
    }
}
