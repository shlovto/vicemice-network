package net.vicemice.hector.utils.players.lobby;

import lombok.Getter;
import net.vicemice.hector.utils.Rank;
import java.io.Serializable;

/*
 * Class created at 17:47 - 11.03.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Getter
public class LobbyFriendData implements Serializable {

    public LobbyFriendData(String name, boolean favorite, Rank rank, String message, String status, Long statusChanged, String value, String signature, boolean online) {
        this.name = name;
        this.favorite = favorite;
        this.rank = rank;
        this.message = message;
        this.status = status;
        this.statusChanged = statusChanged;
        this.value = value;
        this.signature = signature;
        this.online = online;
    }

    private String name;
    private Rank rank;
    private String message;
    private String status;
    private Long statusChanged;

    private boolean favorite, online;

    private String value;
    private String signature;
}