package net.vicemice.hector.packets.types.lobby;

import lombok.Getter;
import net.vicemice.hector.packets.types.lobby.type.LobbyDataInfo;

import java.io.Serializable;
import java.util.List;

/*
 * Class created at 18:19 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class LobbyPacket implements Serializable {

    @Getter
    private List<LobbyDataInfo> lobbies;

    public LobbyPacket(List<LobbyDataInfo> lobbies) {
        this.lobbies = lobbies;
    }
}