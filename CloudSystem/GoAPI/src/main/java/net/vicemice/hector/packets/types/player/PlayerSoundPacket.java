package net.vicemice.hector.packets.types.player;

import net.vicemice.hector.utils.players.utils.Sound;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 14:26 - 07.04.2020
 * Copyright (C) elrobtossohn
 */
public class PlayerSoundPacket implements Serializable {
    private UUID uniqueId;
    private Sound sound;

    public PlayerSoundPacket(UUID uniqueId, Sound sound) {
        this.uniqueId = uniqueId;
        this.sound = sound;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public Sound getSound() {
        return sound;
    }
}
