package net.vicemice.hector.service.tryjump.event;

import net.vicemice.hector.service.tryjump.TryJumpPlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/*
 * Class created at 01:47 - 10.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public abstract class TryJumpEvent extends Event {

    public TryJumpPlayer getPlayer() {
        return this.player;
    }

    protected static HandlerList handlerList = new HandlerList();
    protected TryJumpPlayer player;

    TryJumpEvent(TryJumpPlayer player) {
        this.player = player;
    }

    /** Gets the list of all handlers. */
    public static HandlerList getHandlerList() {
        return handlerList;
    }

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
}