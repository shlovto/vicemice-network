package net.vicemice.hector.utils.players.api.internal.fakemobs.listener;

import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.events.ListenerOptions;
import net.vicemice.hector.api.protocol.events.ListenerPriority;
import net.vicemice.hector.api.protocol.events.ListeningWhitelist;
import net.vicemice.hector.api.protocol.events.PacketContainer;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.events.PacketListener;
import net.vicemice.hector.api.protocol.injector.GamePhase;
import net.vicemice.hector.utils.players.api.internal.fakemobs.FakeMobs;
import net.vicemice.hector.utils.players.api.internal.fakemobs.event.PlayerInteractFakeMobEvent;
import net.vicemice.hector.utils.players.api.internal.fakemobs.event.PlayerInteractFakeMobEvent.Action;
import net.vicemice.hector.utils.players.api.internal.fakemobs.util.FakeMob;
import java.lang.reflect.Field;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class ProtocolListener implements PacketListener {

	@Override
	public void onPacketSending(PacketEvent pe) { }

	@Override
	public void onPacketReceiving(PacketEvent pe) {
		PacketContainer packet = pe.getPacket();
		final Player player = pe.getPlayer();
		
		if (packet.getType() == PacketType.Play.Client.USE_ENTITY) {
			int id = packet.getIntegers().read(0) - 2300;

			if (id < 0) return;
			final FakeMob mob = FakeMobs.instance.getMob(id);
			if (mob == null || player.getWorld() != mob.getWorld()) return;
			
			if (player.isDead()) return;
			if (player.getLocation().distance(mob.getLocation()) > 8) {
				return;
			}

			final Action action;
			try {
				Field field = packet.getEntityUseActions().getField(0);
				field.setAccessible(true);
				Object obj = field.get(packet.getEntityUseActions().getTarget());
				String actionName = (obj == null) ? "" : obj.toString();

				if (actionName.equals("INTERACT")) {
					action = Action.RIGHT_CLICK;
				} else if (actionName.equals("ATTACK")) {
					action = Action.LEFT_CLICK;
				} else {
					return;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}

			pe.setCancelled(true);
			Bukkit.getScheduler().runTask(Bukkit.getPluginManager().getPlugin("Hector"), () -> {
				PlayerInteractFakeMobEvent event = new PlayerInteractFakeMobEvent(player, mob, action);
				Bukkit.getPluginManager().callEvent(event);
			});
		}
	}

	@Override
	public ListeningWhitelist getSendingWhitelist() {
		return ListeningWhitelist.EMPTY_WHITELIST;
	}

	@Override
	public ListeningWhitelist getReceivingWhitelist() {
		return ListeningWhitelist.newBuilder().
				priority(ListenerPriority.NORMAL).
				types(PacketType.Play.Client.USE_ENTITY).
				gamePhase(GamePhase.PLAYING).
				options(new ListenerOptions[0]).
				build();
	}

	@Override
	public Plugin getPlugin() {
		return Bukkit.getPluginManager().getPlugin("Hector");
	}
}
