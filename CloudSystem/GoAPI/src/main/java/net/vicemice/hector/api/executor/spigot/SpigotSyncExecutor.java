package net.vicemice.hector.api.executor.spigot;

import java.beans.ConstructorProperties;
import java.util.concurrent.Executor;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

public class SpigotSyncExecutor implements Executor {
    private final Plugin plugin;

    @Override
    public void execute(@NonNull Runnable runnable) {
        if (runnable == null) {
            throw new NullPointerException("runnable");
        }
        if (!Bukkit.isPrimaryThread()) {
            Bukkit.getScheduler().runTask(this.plugin, runnable);
        } else {
            runnable.run();
        }
    }

    @ConstructorProperties(value={"plugin"})
    public SpigotSyncExecutor(Plugin plugin) {
        this.plugin = plugin;
    }
}
