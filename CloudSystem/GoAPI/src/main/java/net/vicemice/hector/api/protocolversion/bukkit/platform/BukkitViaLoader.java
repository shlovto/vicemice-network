package net.vicemice.hector.api.protocolversion.bukkit.platform;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitTask;
import net.vicemice.hector.api.protocolversion.ViaVersionPlugin;
import net.vicemice.hector.api.protocolversion.api.Via;
import net.vicemice.hector.api.protocolversion.api.data.UserConnection;
import net.vicemice.hector.api.protocolversion.api.minecraft.item.Item;
import net.vicemice.hector.api.protocolversion.api.platform.ViaPlatformLoader;
import net.vicemice.hector.api.protocolversion.api.protocol.ProtocolRegistry;
import net.vicemice.hector.api.protocolversion.api.protocol.ProtocolVersion;
import net.vicemice.hector.api.protocolversion.bukkit.classgenerator.ClassGenerator;
import net.vicemice.hector.api.protocolversion.bukkit.listeners.UpdateListener;
import net.vicemice.hector.api.protocolversion.bukkit.listeners.multiversion.PlayerSneakListener;
import net.vicemice.hector.api.protocolversion.bukkit.listeners.protocol1_9to1_8.*;
import net.vicemice.hector.api.protocolversion.bukkit.providers.BukkitBlockConnectionProvider;
import net.vicemice.hector.api.protocolversion.bukkit.providers.BukkitInventoryQuickMoveProvider;
import net.vicemice.hector.api.protocolversion.bukkit.providers.BukkitViaBulkChunkTranslator;
import net.vicemice.hector.api.protocolversion.bukkit.providers.BukkitViaMovementTransmitter;
import net.vicemice.hector.api.protocolversion.protocols.base.ProtocolInfo;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_12to1_11_1.providers.InventoryQuickMoveProvider;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_13to1_12_2.blockconnections.providers.BlockConnectionProvider;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.providers.BulkChunkTranslatorProvider;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.providers.HandItemProvider;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.providers.MovementTransmitterProvider;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public class BukkitViaLoader implements ViaPlatformLoader {
    private ViaVersionPlugin plugin;

    private Set<Listener> listeners = new HashSet<>();
    private Set<BukkitTask> tasks = new HashSet<>();

    public BukkitViaLoader(ViaVersionPlugin plugin) {
        this.plugin = plugin;
    }

    public void registerListener(Listener listener) {
        Bukkit.getPluginManager().registerEvents(storeListener(listener), Bukkit.getPluginManager().getPlugin("Hector"));
    }

    public <T extends Listener> T storeListener(T listener) {
        listeners.add(listener);
        return listener;
    }

    @Override
    public void load() {
        // Update Listener
        registerListener(new UpdateListener());

        /* Base Protocol */
        final ViaVersionPlugin plugin = (ViaVersionPlugin) Bukkit.getPluginManager().getPlugin("ViaVersion");

        // Add ProtocolSupport ConnectListener if necessary.
        ClassGenerator.registerPSConnectListener(plugin);

        registerListener(new Listener() {
            @EventHandler
            public void onPlayerQuit(PlayerQuitEvent e) {
                Via.getManager().removePortedClient(e.getPlayer().getUniqueId());
            }
        });

        /* 1.9 client to 1.8 server */

        storeListener(new ArmorListener(plugin)).register();
        storeListener(new DeathListener(plugin)).register();
        storeListener(new BlockListener(plugin)).register();

        if (ProtocolRegistry.SERVER_PROTOCOL < ProtocolVersion.v1_14.getId()) {
            boolean use1_9Fix = plugin.getConf().is1_9HitboxFix() && ProtocolRegistry.SERVER_PROTOCOL < ProtocolVersion.v1_9.getId();
            if (use1_9Fix || plugin.getConf().is1_14HitboxFix()) {
                try {
                    storeListener(new PlayerSneakListener(plugin, use1_9Fix, plugin.getConf().is1_14HitboxFix())).register();
                } catch (ReflectiveOperationException e) {
                    Via.getPlatform().getLogger().warning("Could not load hitbox fix - please report this on our GitHub");
                    e.printStackTrace();
                }
            }
        }

        if ((Bukkit.getVersion().toLowerCase(Locale.ROOT).contains("paper")
                || Bukkit.getVersion().toLowerCase(Locale.ROOT).contains("taco")
                || Bukkit.getVersion().toLowerCase(Locale.ROOT).contains("torch"))
                && ProtocolRegistry.SERVER_PROTOCOL < ProtocolVersion.v1_12.getId()) {
            plugin.getLogger().info("Enabling Paper/TacoSpigot/Torch patch: Fixes block placement.");
            storeListener(new PaperPatch(plugin)).register();
        }
        if (plugin.getConf().isItemCache()) {
            tasks.add(new HandItemCache().runTaskTimerAsynchronously(Bukkit.getPluginManager().getPlugin("Hector"), 2L, 2L)); // Updates player's items :)
            HandItemCache.CACHE = true;
        }

        /* Providers */
        Via.getManager().getProviders().use(BulkChunkTranslatorProvider.class, new BukkitViaBulkChunkTranslator());
        Via.getManager().getProviders().use(MovementTransmitterProvider.class, new BukkitViaMovementTransmitter());
        if (plugin.getConf().is1_12QuickMoveActionFix()) {
            Via.getManager().getProviders().use(InventoryQuickMoveProvider.class, new BukkitInventoryQuickMoveProvider());
        }
        if (Via.getConfig().getBlockConnectionMethod().equalsIgnoreCase("world")) {
            Via.getManager().getProviders().use(BlockConnectionProvider.class, new BukkitBlockConnectionProvider());
        }
        Via.getManager().getProviders().use(HandItemProvider.class, new HandItemProvider() {
            @Override
            public Item getHandItem(final UserConnection info) {
                if (HandItemCache.CACHE) {
                    return HandItemCache.getHandItem(info.get(ProtocolInfo.class).getUuid());
                } else {
                    try {
                        return Bukkit.getScheduler().callSyncMethod(Bukkit.getPluginManager().getPlugin("ViaVersion"), new Callable<Item>() {
                            @Override
                            public Item call() throws Exception {
                                UUID playerUUID = info.get(ProtocolInfo.class).getUuid();
                                Player player = Bukkit.getPlayer(playerUUID);
                                if (player != null) {
                                    return HandItemCache.convert(player.getItemInHand());
                                }
                                return null;
                            }
                        }).get(10, TimeUnit.SECONDS);
                    } catch (Exception e) {
                        Via.getPlatform().getLogger().severe("Error fetching hand item: " + e.getClass().getName());
                        if (Via.getManager().isDebug())
                            e.printStackTrace();
                        return null;
                    }
                }
            }
        });

    }

    @Override
    public void unload() {
        for (Listener listener : listeners) {
            HandlerList.unregisterAll(listener);
        }
        listeners.clear();
        for (BukkitTask task : tasks) {
            task.cancel();
        }
        tasks.clear();
    }
}
