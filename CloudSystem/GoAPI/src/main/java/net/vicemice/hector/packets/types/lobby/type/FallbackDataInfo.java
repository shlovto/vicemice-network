package net.vicemice.hector.packets.types.lobby.type;

import lombok.Getter;

import java.io.Serializable;

/*
 * Class created at 23:08 - 02.05.2020
 * Copyright (C) elrobtossohn
 */
public class FallbackDataInfo implements Serializable, Comparable<FallbackDataInfo> {
    @Getter
    private final String name;
    @Getter
    private final int id, players;

    public FallbackDataInfo(String name, int players) {
        this.name = name;
        this.id = Integer.parseInt(name.split("-")[1]);
        this.players = players;
    }

    @Override
    public int compareTo(FallbackDataInfo o) {
        return this.id < o.id ? -1 : (this.id > o.id ? 1 : 0);
    }
}