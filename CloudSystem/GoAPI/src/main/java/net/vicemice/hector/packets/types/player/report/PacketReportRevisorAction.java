package net.vicemice.hector.packets.types.player.report;

import lombok.Getter;

import java.beans.ConstructorProperties;
import java.io.Serializable;

/*
 * Class created at 18:24 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PacketReportRevisorAction implements Serializable {

    @Getter
    private String suspect;
    @Getter
    private String revisor;
    @Getter
    private Action action;

    @ConstructorProperties(value={"suspect", "revisor", "action"})
    public PacketReportRevisorAction(String suspect, String revisor, Action action) {
        this.suspect = suspect;
        this.revisor = revisor;
        this.action = action;
    }

    public static enum Action {
        ADD,
        REMOVE,
        DELETE
    }
}