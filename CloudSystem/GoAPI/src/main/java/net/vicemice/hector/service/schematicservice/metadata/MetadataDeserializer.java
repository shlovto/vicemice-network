package net.vicemice.hector.service.schematicservice.metadata;

import com.flowpowered.nbt.CompoundTag;

/*
 * Class created at 01:24 - 10.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public interface MetadataDeserializer<T> {

    /**
     * @param schematicTag
     * @return
     */
    T deserialize(CompoundTag schematicTag);
}