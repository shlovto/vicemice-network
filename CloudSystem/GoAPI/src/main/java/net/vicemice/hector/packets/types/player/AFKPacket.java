package net.vicemice.hector.packets.types.player;

import lombok.Getter;

import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 18:44 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class AFKPacket implements Serializable {
    @Getter
    private UUID uniqueId;
    @Getter
    private boolean afk;

    public AFKPacket(UUID uniqueId, boolean afk) {
        this.uniqueId = uniqueId;
        this.afk = afk;
    }
}