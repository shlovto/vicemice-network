package net.vicemice.hector.packets.types.server;

import java.io.Serializable;

/*
 * Class created at 18:37 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SpigotServerStop implements Serializable {
    private String name;

    public SpigotServerStop(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
