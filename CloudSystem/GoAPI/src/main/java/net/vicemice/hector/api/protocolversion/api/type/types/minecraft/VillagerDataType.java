package net.vicemice.hector.api.protocolversion.api.type.types.minecraft;

import io.netty.buffer.ByteBuf;
import net.vicemice.hector.api.protocolversion.api.minecraft.VillagerData;
import net.vicemice.hector.api.protocolversion.api.type.Type;

public class VillagerDataType extends Type<VillagerData> {
    public VillagerDataType() {
        super(VillagerData.class);
    }

    @Override
    public VillagerData read(ByteBuf buffer) throws Exception {
        return new VillagerData(Type.VAR_INT.read(buffer), Type.VAR_INT.read(buffer), Type.VAR_INT.read(buffer));
    }

    @Override
    public void write(ByteBuf buffer, VillagerData object) throws Exception {
        Type.VAR_INT.write(buffer, object.getType());
        Type.VAR_INT.write(buffer, object.getProfession());
        Type.VAR_INT.write(buffer, object.getLevel());
    }
}
