package net.vicemice.hector.packets.types.server.software;

import java.io.Serializable;
import java.util.UUID;

public class RunCommandPacket implements Serializable {
    private Type type;
    private UUID uniqueId;
    private String command;

    public RunCommandPacket(Type type, UUID uniqueId, String command) {
        this.type = type;
        this.uniqueId = uniqueId;
        this.command = command;
    }

    public Type getType() {
        return type;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public String getCommand() {
        return command;
    }

    public enum Type {
        PLAYER,
        CONSOLE;
    }
}
