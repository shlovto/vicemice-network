package net.vicemice.hector.utils.players.utils;

public interface IPaginationExecutor<T> {
    void print(T element);
}