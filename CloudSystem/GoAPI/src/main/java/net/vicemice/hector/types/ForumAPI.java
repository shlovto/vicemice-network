package net.vicemice.hector.types;

import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.api.forum.ForumCallback;
import org.json.JSONObject;
import java.util.UUID;

/*
 * Class created at 10:23 - 26.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public interface ForumAPI {
    public void createUser(UUID uniqueId, Rank rank, String username, String email, String password, ForumCallback<JSONObject> forumCallback);

    public void changeName(int userId, String name, ForumCallback<JSONObject> forumCallback);

    public void changePassword(int userId, String password, ForumCallback<JSONObject> forumCallback);

    public void changeRank(int userId, Rank rank, ForumCallback<JSONObject> forumCallback);

    public void changeAvatar(int userId, String file, ForumCallback<JSONObject> forumCallback);
}