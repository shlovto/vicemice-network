package net.vicemice.hector.service.schematicservice.event;

import net.vicemice.hector.service.schematicservice.GoSchematic;
import org.bukkit.Location;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/*
 * Class created at 01:23 - 10.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 * <p>Event that will be called when a Schematic has been pasted.
 */
public class SchematicPlacedEvent extends Event {

    private static HandlerList handlers = new HandlerList();
    private GoSchematic schematic;
    private Location placed;

    /**
     * @param schematic The schematic that has been placed.
     * @param placed The location where the schematic has been placed to.
     */
    public SchematicPlacedEvent(GoSchematic schematic, Location placed) {
        this.schematic = schematic;
        this.placed = placed;
    }

    /** Gets the list of all handlers. */
    public static HandlerList getHandlerList() {
        return handlers;
    }

    /** Gets the schematic that has been placed. */
    public GoSchematic getSchematic() {
        return schematic;
    }

    /** Gets the location where the schematic has been placed to. */
    public Location getPlaced() {
        return placed;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}