package net.vicemice.hector.api.protocolversion.api.remapper;

import net.vicemice.hector.api.protocolversion.api.PacketWrapper;
import net.vicemice.hector.api.protocolversion.api.type.Type;

public class TypeRemapper<T> implements ValueReader<T>, ValueWriter<T> {
    private final Type<T> type;

    public TypeRemapper(Type<T> type) {
        this.type = type;
    }

    @Override
    public T read(PacketWrapper wrapper) throws Exception {
        return wrapper.read(type);
    }

    @Override
    public void write(PacketWrapper output, T inputValue) {
        output.write(type, inputValue);
    }
}
