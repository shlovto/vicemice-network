package net.vicemice.hector.packets.types.player.punish;

import lombok.Getter;
import java.io.Serializable;

/*
 * Class created at 13:01 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class BanIpPacket implements Serializable {

    @Getter
    boolean ban;
    @Getter
    private String ip;

    public BanIpPacket(String ip, boolean ban) {
        this.ip = ip;
        this.ban = ban;
    }
}
