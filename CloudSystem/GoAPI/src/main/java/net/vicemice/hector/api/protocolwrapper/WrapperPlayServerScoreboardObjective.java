/**
 * PacketWrapper - ProtocolLib wrappers for Minecraft packets
 * Copyright (C) dmulloy2 <http://dmulloy2.net>
 * Copyright (C) Kristian S. Strangeland
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.vicemice.hector.api.protocolwrapper;

import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.events.PacketContainer;
import net.vicemice.hector.api.protocol.reflect.StructureModifier;

public class WrapperPlayServerScoreboardObjective extends AbstractPacket {
	public static final PacketType TYPE;

	public WrapperPlayServerScoreboardObjective() {
		super(new PacketContainer(TYPE), TYPE);
		this.handle.getModifier().writeDefaults();
	}

	public WrapperPlayServerScoreboardObjective(PacketContainer packet) {
		super(packet, TYPE);
	}

	public String getName() {
		return (String)this.handle.getStrings().read(0);
	}

	public void setName(String value) {
		this.handle.getStrings().write(0, value);
	}

	public String getDisplayName() {
		return (String)this.handle.getStrings().read(1);
	}

	public void setDisplayName(String value) {
		this.handle.getStrings().write(1, value);
	}

	public String getHealthDisplay() {
		return ((Enum)this.handle.getSpecificModifier(Enum.class).read(0)).name();
	}

	public void setHealthDisplay(String value) {
		StructureModifier<Enum> mod = this.handle.getSpecificModifier(Enum.class);
		Enum constant = Enum.valueOf(((Enum)mod.read(0)).getClass(), value.toUpperCase());
		mod.write(0, constant);
	}

	public int getMode() {
		return (Integer)this.handle.getIntegers().read(0);
	}

	public void setMode(int value) {
		this.handle.getIntegers().write(0, value);
	}

	static {
		TYPE = PacketType.Play.Server.SCOREBOARD_OBJECTIVE;
	}

	public class Mode {
		public static final int ADD_OBJECTIVE = 0;
		public static final int REMOVE_OBJECTIVE = 1;
		public static final int UPDATE_VALUE = 2;
	}
}
