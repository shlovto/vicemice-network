package net.vicemice.hector.utils.api.forum;

import net.vicemice.hector.types.ForumAPI;
import net.vicemice.hector.utils.JSONUtil;
import net.vicemice.hector.utils.Rank;
import org.json.JSONObject;
import java.util.UUID;

/*
 * Class created at 10:36 - 26.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GoForumAPI implements ForumAPI {

    @Override
    public void createUser(UUID uniqueId, Rank rank, String username, String email, String password, ForumCallback<JSONObject> forumCallback) {
        String variables = "?type=createUser&username="+username+"&email="+email+"&password="+password+"&uniqueId="+uniqueId+"&timezone=Europe/Berlin&user_group_id="+rank.getForumId()+"&is_staff="+(rank.isTeam() ? 1 : 0);
        JSONUtil JSONUtil = new JSONUtil(net.vicemice.hector.utils.JSONUtil.API.INTERNAL_API, variables);

        forumCallback.done(JSONUtil.getObject());
    }

    @Override
    public void changeName(int userId, String name, ForumCallback<JSONObject> forumCallback) {
        String variables = "?type=changeName&userId="+userId+"&username="+name;
        JSONUtil JSONUtil = new JSONUtil(net.vicemice.hector.utils.JSONUtil.API.INTERNAL_API, variables);

        forumCallback.done(JSONUtil.getObject());
    }

    @Override
    public void changePassword(int userId, String password, ForumCallback<JSONObject> forumCallback) {
        String variables = "?type=changePassword&userId="+userId+"&password="+password;
        JSONUtil JSONUtil = new JSONUtil(net.vicemice.hector.utils.JSONUtil.API.INTERNAL_API, variables);

        forumCallback.done(JSONUtil.getObject());
    }

    @Override
    public void changeRank(int userId, Rank rank, ForumCallback<JSONObject> forumCallback) {
        String variables = "?type=changeRank&userId="+userId+"&user_group_id="+rank.getForumId()+"&is_staff="+(rank.isTeam() ? 1 : 0);
        JSONUtil JSONUtil = new JSONUtil(net.vicemice.hector.utils.JSONUtil.API.INTERNAL_API, variables);

        forumCallback.done(JSONUtil.getObject());
    }

    @Override
    public void changeAvatar(int userId, String file, ForumCallback<JSONObject> forumCallback) {
        String variables = "?type=changeAvatar&userId="+userId+"&avatar="+file;
        JSONUtil JSONUtil = new JSONUtil(net.vicemice.hector.utils.JSONUtil.API.INTERNAL_API, variables);

        forumCallback.done(JSONUtil.getObject());
    }
}
