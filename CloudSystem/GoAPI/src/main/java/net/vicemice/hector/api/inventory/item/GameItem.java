package net.vicemice.hector.api.inventory.item;

import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.inventory.ItemStack;

public interface GameItem {
    public String getName();

    public String getDisplayName();

    public ItemStack getItemStack();

    public boolean isCancelOnClick();

    public boolean isCalled(Action var1);

    public void action(Player var1, Action var2);
}