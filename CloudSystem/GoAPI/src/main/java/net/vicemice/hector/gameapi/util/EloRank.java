package net.vicemice.hector.gameapi.util;

import lombok.Getter;
import org.bukkit.ChatColor;

/*
 * Enum created at 19:11 - 10.04.2020
 * Copyright (C) elrobtossohn
 */
public enum EloRank {
    UNRANKED(0, 249, "Unranked", "-", ChatColor.DARK_GRAY),
    BRONZE_I(250, 499, "Bronze","I", ChatColor.RED),
    BRONZE_II(500, 749, "Bronze","II", ChatColor.RED),
    BRONZE_III(750, 999, "Bronze","III", ChatColor.RED),
    SILVER_I(1000, 1249, "Silver","I", ChatColor.GRAY),
    SILVER_II(1250, 1499, "Silver","II", ChatColor.GRAY),
    SILVER_III(1500, 1749, "Silver","III", ChatColor.GRAY),
    GOLD_I(1750, 1999, "Gold","I", ChatColor.GOLD),
    GOLD_II(2000, 2249, "Gold","II", ChatColor.GOLD),
    GOLD_III(2250, 2499, "Gold","III", ChatColor.GOLD),
    DIAMOND_I(2500, 2749, "Diamond","I", ChatColor.AQUA),
    DIAMOND_II(2750, 2999, "Diamond","II", ChatColor.AQUA),
    DIAMOND_III(3000, 3999, "Diamond","III", ChatColor.AQUA),
    SUPREME(4000, Long.MAX_VALUE, "Supreme","S", ChatColor.BLUE);

    private EloRank(long elo, long max, String uniqueName, String name, ChatColor color) {
        this.elo = elo;
        this.max = max;
        this.uniqueName = uniqueName;
        this.name = name;
        this.color = color;
    }

    @Getter
    private long elo, max;
    @Getter
    private String uniqueName, name;
    @Getter
    private ChatColor color;

    public static EloRank rankByElo(long elo) {
        for (EloRank eloRank : values()) {
            if (eloRank.getElo() <= elo && eloRank.getMax() >= elo)
                return eloRank;
        }

        return null;
    }
}
