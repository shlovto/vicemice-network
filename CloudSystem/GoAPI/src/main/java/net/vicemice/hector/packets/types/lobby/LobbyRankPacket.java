package net.vicemice.hector.packets.types.lobby;

import lombok.Getter;
import java.io.Serializable;

/*
 * Class created at 18:19 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class LobbyRankPacket implements Serializable {

    @Getter
    private String playerName;

    public LobbyRankPacket(String playerName) { this.playerName = playerName; }
}
