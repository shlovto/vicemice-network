package net.vicemice.hector.utils.language;

import java.util.UUID;

/*
 * Class created at 12:13 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public interface LanguageAction {

    public void done(UUID uuid);
}