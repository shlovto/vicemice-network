package net.vicemice.hector.types;

public enum ServerState {
    STARTUP,
    STARTING,
    LOBBY,
    FALLBACK,
    INGAME,
    REMOVE,
    ALL,
    NO_STATE;
}