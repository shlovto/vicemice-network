package net.vicemice.hector.api.protocolversion.boss;

import lombok.Getter;
import org.bukkit.entity.Player;
import net.vicemice.hector.api.protocolversion.api.boss.BossBar;
import net.vicemice.hector.api.protocolversion.api.boss.BossColor;
import net.vicemice.hector.api.protocolversion.api.boss.BossStyle;

@Getter
public class ViaBossBar extends CommonBoss<Player> {

    public ViaBossBar(String title, float health, BossColor color, BossStyle style) {
        super(title, health, color, style);
    }

    @Override
    public BossBar addPlayer(Player player) {
        addPlayer(player.getUniqueId());
        return this;
    }

    @Override
    public BossBar addPlayers(Player... players) {
        for (Player p : players)
            addPlayer(p);
        return this;
    }

    @Override
    public BossBar removePlayer(Player player) {
        removePlayer(player.getUniqueId());
        return this;
    }
}
