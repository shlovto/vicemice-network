package net.vicemice.hector.utils;

import lombok.NonNull;

public enum CommandType {

    HELP("help", Rank.MEMBER),

    LEVEL("level", Rank.MEMBER),
    XP("xp", Rank.MEMBER),

    AFK("afk", Rank.MEMBER),
    VOTE("vote", Rank.MEMBER),
    RANG("rang", Rank.ADMIN),
    TS("ts", Rank.MEMBER),
    CLOUD("clyde", Rank.ADMIN),
    ONLINE("online", Rank.MEMBER),
    SERVER("server", Rank.ADMIN),
    HUB("hub", Rank.MEMBER),
    ALERT("alert", Rank.ADMIN),
    PLAYTIME("playtime", Rank.MEMBER),
    MSG("msg", Rank.MEMBER),
    R("r", Rank.MEMBER),
    LANGUAGE("language", Rank.MEMBER),
    CHATLOG("language", Rank.MEMBER),
    RUN("runcommand", Rank.ADMIN),
    REPORT("report", Rank.MEMBER),
    BANIP("bi", Rank.ADMIN),
    UNBANIP("ubi", Rank.ADMIN),
    SEND("send", Rank.ADMIN),
    GOTO("goto", Rank.MODERATOR),
    NOTIFY("notify", Rank.MODERATOR),
    IP_COMPARE("ipcmp", Rank.ADMIN),
    BAN("ban", Rank.ADMIN),
    MUTE("mute", Rank.ADMIN),
    REPLAY("replay", Rank.MEMBER),
    MAPEDIT("mapedit", Rank.ADMIN),

    FRIENDS("friends", Rank.MEMBER),
    PARTY("party", Rank.MEMBER),
    PARTYCHAT("partychat", Rank.MEMBER),
    CLAN("clan", Rank.MEMBER),
    CLANCHAT("clanchat", Rank.MEMBER),

    NICK("nick", Rank.VIP),
    LINK("link", Rank.MEMBER),
    UNLINK("unlink", Rank.MEMBER),

    PLAY("play", Rank.MEMBER),
    FORUM("forum", Rank.MEMBER),

    TEAM("team", Rank.MODERATOR),
    LOCALTEAMCHAT("ltc", Rank.MODERATOR),
    TEAMCHAT("tc", Rank.MODERATOR),
    BANLOG("banlog", Rank.MODERATOR),
    BANINFO("baninfo", Rank.MODERATOR),
    KICK("kick", Rank.ADMIN),
    TEMPBAN("tempban", Rank.ADMIN),
    TEMPMUTE("tempmute", Rank.ADMIN),
    PUNISH("punish", Rank.MODERATOR),
    UNBAN("unban", Rank.MODERATOR),
    UNMUTE("unmute", Rank.MODERATOR);

    @NonNull
    private final String key;
    private int accessLevel = Rank.ADMIN.getAccessLevel();

    private CommandType(String key, Rank required) {
        if (required == null) {
            required = Rank.MEMBER;
        }
        this.accessLevel = required.getAccessLevel();
        this.key = key;
    }

    public static CommandType getCommandByName(String name) {
        for (CommandType commandType : CommandType.values()) {
            if (!commandType.getKey().equalsIgnoreCase(name)) continue;
            return commandType;
        }
        return null;
    }

    private CommandType(String key) {
        if (key == null) {
            throw new NullPointerException("key");
        }
        this.key = key;
    }

    private CommandType(String key, int accessLevel) {
        if (key == null) {
            throw new NullPointerException("key");
        }
        this.key = key;
        this.accessLevel = accessLevel;
    }

    @NonNull
    public String getKey() {
        return this.key;
    }

    public int getAccessLevel() {
        return this.accessLevel;
    }
}