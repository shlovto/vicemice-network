package net.vicemice.hector.api.protocolversion.api.data;

public interface ExternalJoinGameListener {
    void onExternalJoinGame(int playerEntityId);
}
