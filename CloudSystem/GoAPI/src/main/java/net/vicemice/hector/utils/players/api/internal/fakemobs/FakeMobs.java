package net.vicemice.hector.utils.players.api.internal.fakemobs;

import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.ProtocolManager;
import net.vicemice.hector.api.protocol.wrappers.WrappedGameProfile;
import net.vicemice.hector.api.protocol.wrappers.WrappedSignedProperty;
import com.google.common.collect.Multimap;
import lombok.Getter;
import net.vicemice.hector.utils.players.api.internal.fakemobs.adjuster.MyWorldAccess;
import net.vicemice.hector.utils.players.api.internal.fakemobs.event.RemoveFakeMobEvent;
import net.vicemice.hector.utils.players.api.internal.fakemobs.event.SpawnFakeMobEvent;
import net.vicemice.hector.utils.players.api.internal.fakemobs.listener.InteractListener;
import net.vicemice.hector.utils.players.api.internal.fakemobs.listener.MobListener;
import net.vicemice.hector.utils.players.api.internal.fakemobs.listener.ProtocolListener;
import net.vicemice.hector.utils.players.api.internal.fakemobs.merchant.ReflectionUtils;
import net.vicemice.hector.utils.players.api.internal.fakemobs.util.Cache;
import net.vicemice.hector.utils.players.api.internal.fakemobs.util.FakeMob;
import net.vicemice.hector.utils.players.api.internal.fakemobs.util.LookUpdate;
import net.vicemice.hector.utils.players.api.internal.fakemobs.util.SkinQueue;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/*
 * Class created at 14:55 - 14.04.2020
 * Copyright (C) elrobtossohn
 */
@Getter
public class FakeMobs {

    public static FakeMobs instance;

    private ProtocolManager pManager;
    private final Map<Integer, FakeMob> mobs = new HashMap<>();
    private ProtocolListener pListener;
    private SkinQueue skinQueue;

    public FakeMobs() {
        instance = this;
        this.pManager = ProtocolLibrary.getProtocolManager();
        this.skinQueue = new SkinQueue();
        this.skinQueue.start();

        Bukkit.getPluginManager().registerEvents(new InteractListener(), Bukkit.getServer().getPluginManager().getPlugin("Hector"));
        Bukkit.getPluginManager().registerEvents(new MobListener(), Bukkit.getServer().getPluginManager().getPlugin("Hector"));

        for (Player player : Bukkit.getOnlinePlayers())
            this.updatePlayerView(player);

        Bukkit.getScheduler().scheduleAsyncRepeatingTask(Bukkit.getServer().getPluginManager().getPlugin("Hector"), new LookUpdate(), 5L, 5L);
        this.pManager.addPacketListener(pListener = new ProtocolListener());

        for (World world : Bukkit.getWorlds()) {
            MyWorldAccess.registerWorldAccess(world);
        }
    }


    public SkinQueue getSkinQueue() {
        return this.skinQueue;
    }

    public boolean existsMob(int id) {
        return this.mobs.containsKey(id);
    }

    public FakeMob getMob(Location loc) {
        for (FakeMob mob : this.getMobs()) {
            if (mob.getLocation().getWorld() == loc.getWorld() &&
                    mob.getLocation().getBlockX() == loc.getBlockX() &&
                    mob.getLocation().getBlockY() == loc.getBlockY() &&
                    mob.getLocation().getBlockZ() == loc.getBlockZ())
                return mob;
        }
        return null;
    }

    public boolean isMobOnLocation(Location loc) {
        return (this.getMob(loc) != null);
    }

    public FakeMob getMob(int id) {
        return this.mobs.get(id);
    }

    public void removeMob(int id) {
        FakeMob mob = this.mobs.get(id);
        if (mob == null) return;

        RemoveFakeMobEvent event = new RemoveFakeMobEvent(mob);
        Bukkit.getPluginManager().callEvent(event);

        for (Player player : mob.getWorld().getPlayers()) {
            mob.unloadPlayer(player);
        }

        Map<Player, FakeMob> selectedMap = new HashMap<>(Cache.selectedMobs);
        for (Map.Entry<Player, FakeMob> e : selectedMap.entrySet()) {
            if (e.getValue() == mob)
                Cache.selectedMobs.remove(e.getKey());
        }

        this.mobs.remove(id);
    }

    public FakeMob spawnMob(Location loc, EntityType type) {
        return this.spawnMob(loc, type, null);
    }

    public FakeMob spawnMob(Location loc, EntityType type, String customName) {
        if (!type.isAlive()) return null;

        int id = this.getNewId();
        FakeMob mob = new FakeMob(id, loc, type);
        mob.setCustomName(customName);

        SpawnFakeMobEvent event = new SpawnFakeMobEvent(loc, type, mob);
        Bukkit.getPluginManager().callEvent(event);
        if (event.isCancelled()) return null;

        for (Player player : loc.getWorld().getPlayers()) {
            if (mob.isInRange(player)) {
                mob.loadPlayer(player);
            }
        }

        this.mobs.put(id, mob);
        return mob;
    }

    public FakeMob spawnPlayer(Location loc, String name) {
        return this.spawnPlayer(loc, name, (Multimap<String, WrappedSignedProperty>) null);
    }

    public FakeMob spawnPlayer(Location loc, String name, Player skin) {
        return this.spawnPlayer(loc, name, WrappedGameProfile.fromPlayer(skin).getProperties());
    }

    public FakeMob spawnPlayer(Location loc, String name, Multimap<String, WrappedSignedProperty> skin) {
        int id = this.getNewId();
        FakeMob mob = new FakeMob(id, loc, EntityType.PLAYER);
        mob.setCustomName(name);
        mob.setPlayerSkin(skin);

        SpawnFakeMobEvent event = new SpawnFakeMobEvent(loc, EntityType.PLAYER, mob);
        Bukkit.getPluginManager().callEvent(event);
        if (event.isCancelled()) return null;

        for (Player player : loc.getWorld().getPlayers()) {
            if (mob.isInRange(player)) {
                mob.loadPlayer(player);
            }
        }

        this.mobs.put(id, mob);
        return mob;
    }

    public int getNewId() {
        int id = -1;
        for (FakeMob mob : this.getMobs())
            if (mob.getId() > id)
                id = mob.getId();
        return id+1;
    }

    public List<FakeMob> getMobs() {
        List<FakeMob> mobList = new ArrayList<>();
        mobList.addAll(this.mobs.values());
        return mobList;
    }

    public Map<Integer, FakeMob> getMobsMap() {
        return this.mobs;
    }

    /** Called every chunk move */
    public void updatePlayerView(Player player) {
        for (FakeMob mob : this.getMobs()) {
            if (mob.isInRange(player)) {
                mob.loadPlayer(player);
            } else {
                mob.unloadPlayer(player);
            }
        }
    }

    public List<FakeMob> getMobsInRadius(Location loc, int radius) {
        List<FakeMob> mobList = new ArrayList<>();
        for (FakeMob mob : this.getMobs()) {
            if (mob.getWorld() == loc.getWorld() && mob.getLocation().distance(loc) <= radius) {
                mobList.add(mob);
            }
        }

        return mobList;
    }

    public List<FakeMob> getMobsInChunk(World world, int chunkX, int chunkZ) {
        List<FakeMob> mobList = new ArrayList<FakeMob>();

        for (FakeMob mob : this.getMobs()) {
            Chunk chunk = mob.getLocation().getChunk();
            if (mob.getWorld() == world && chunk.getX() == chunkX && chunk.getZ() == chunkZ) {
                mobList.add(mob);
            }
        }

        return mobList;
    }

    public void adjustEntityCount() {
        try {
            Class entityClass = Class.forName(ReflectionUtils.getNMSPackageName() + ".Entity");

            Field field = entityClass.getDeclaredField("entityCount");
            field.setAccessible(true);
            int currentCount = field.getInt(null);

            if (currentCount >= 2300) {
                while (this.existsMob(currentCount - 2300)) {
                    currentCount++;
                }

                field.set(null, currentCount);
            }
        } catch (Exception ex) {
            Bukkit.getPluginManager().getPlugin("Hector").getLogger().log(Level.WARNING, "Can't adjust entity count", ex);
        }
    }
}
