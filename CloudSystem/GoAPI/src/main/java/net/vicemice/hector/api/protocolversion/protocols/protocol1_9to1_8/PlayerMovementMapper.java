package net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8;

import net.vicemice.hector.api.protocolversion.api.PacketWrapper;
import net.vicemice.hector.api.protocolversion.api.remapper.PacketHandler;
import net.vicemice.hector.api.protocolversion.api.type.Type;
import net.vicemice.hector.api.protocolversion.protocols.protocol1_9to1_8.storage.MovementTracker;

public class PlayerMovementMapper extends PacketHandler {
    @Override
    public void handle(PacketWrapper wrapper) throws Exception {
        MovementTracker tracker = wrapper.user().get(MovementTracker.class);
        tracker.incrementIdlePacket();
        // If packet has the ground data
        if (wrapper.is(Type.BOOLEAN, 0)) {
            tracker.setGround(wrapper.get(Type.BOOLEAN, 0));
        }
    }
}
