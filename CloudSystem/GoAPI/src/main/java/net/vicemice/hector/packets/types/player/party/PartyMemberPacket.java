package net.vicemice.hector.packets.types.player.party;

import lombok.Getter;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/*
 * Class created at 18:24 - 18.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PartyMemberPacket implements Serializable {

    @Getter
    private List<UUID> members;

    public PartyMemberPacket(List<UUID> members) {
        this.members = members;
    }
}