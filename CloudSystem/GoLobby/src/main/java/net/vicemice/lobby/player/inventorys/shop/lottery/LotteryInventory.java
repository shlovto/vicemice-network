package net.vicemice.lobby.player.inventorys.shop.lottery;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.utils.Pagination;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.bukkit.Material.*;

/*
 * Class created at 21:20 - 01.04.2020
 * Copyright (C) elrobtossohn
 */
public class LotteryInventory {

    public static ArrayList<IUser> reopen = new ArrayList<>();

    public GUI create(IUser user, int page) {
        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(user);
        GUI gui = user.createGUI(ChatColor.stripColor(user.translate("shop-lottery")), 6);

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        Pagination<Long> guiPagination = new Pagination<>(getTickets(user), 27);
        AtomicInteger i = new AtomicInteger(9);

        guiPagination.printPage(page, number -> {
            gui.setItem(i.get(), ItemBuilder.create(PAPER).name(user.translate("lottery-ticket")).lore(user.translate("open-ticket")).build(), e -> {
                user.decreaseStatistic("lobby_lottery_tickets", 1);
                new OpenTicketInventory().create(user).open();
                OpenTicketInventory.resultOpen.remove(user.getUniqueId());
            });

            i.set(i.get() + 1);
        });

        if (guiPagination.getElementsFor(page).isEmpty()) {
            gui.setItem(22, ItemBuilder.create(BARRIER).name(user.translate("lottery-no-tickets")).build());
        }

        if (!guiPagination.getElementsFor((page-1)).isEmpty()) {
            gui.setItem(47, ItemBuilder.create(GUI.Items.getLeft()).name(user.translate("previous-page")).build(), e -> {
                this.create(user, (page-1)).open();
            });
        }
        gui.setItem(49, ItemBuilder.create(DOUBLE_PLANT).name(user.translate("buy-tickets")).lore((playerData.getNetworkPlayer().getCoins() >= 1000 ? user.translate("buy-ticket") : user.translate("lottery-not-enough-coins", (1000-playerData.getNetworkPlayer().getCoins())))).build(), e -> {
            if (playerData.getNetworkPlayer().getCoins() < 1000) return;
            user.decreaseCoins(1000);
            user.increaseStatistic("lobby_lottery_tickets", 1);
            reopen.add(user);
        });
        if (!guiPagination.getElementsFor((page+1)).isEmpty()) {
            gui.setItem(51, ItemBuilder.create(GUI.Items.getRight()).name(user.translate("next-page")).build(), e -> {
                this.create(user, (page+1)).open();
            });
        }

        return gui;
    }

    public List<Long> getTickets(IUser user) {
        List<Long> list = new ArrayList<>();

        for (long i = 0; i < user.getStatistic("lobby_lottery_tickets", StatisticPeriod.GLOBAL); i++) {
            list.add(i);
        }

        return list;
    }
}
