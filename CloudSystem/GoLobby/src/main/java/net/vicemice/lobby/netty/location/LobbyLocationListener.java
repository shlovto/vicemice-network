package net.vicemice.lobby.netty.location;

import io.netty.channel.Channel;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.utils.Locations;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.lobby.LobbyLocPacket;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class LobbyLocationListener extends PacketGetter {

    public void receivePacket(PacketHolder initPacket) {
        if (initPacket.getKey().equals(PacketType.LOBBY_LOC_PACKET.getKey())) {
            LobbyLocPacket lobbyLocPacket = (LobbyLocPacket) initPacket.getValue();
            Location location = new Location(Locations.getSpawnLocation().getWorld(), lobbyLocPacket.getX(), lobbyLocPacket.getY(), lobbyLocPacket.getZ(), lobbyLocPacket.getYaw(), lobbyLocPacket.getPitch());
            Player player = Bukkit.getServer().getPlayer(lobbyLocPacket.getUniqueId());
            if (player != null) {
                GoLobby.getPlayerManager().getPlayerLocations().put(lobbyLocPacket.getUniqueId(), location);
                player.teleport(location);
            } else {
                GoLobby.getPlayerManager().getPlayerLocations().put(lobbyLocPacket.getUniqueId(), location);
            }
        }
    }

    public void channelActive(Channel channel) {
    }
}
