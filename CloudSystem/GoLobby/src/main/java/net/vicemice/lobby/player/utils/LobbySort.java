package net.vicemice.lobby.player.utils;

public class LobbySort implements Comparable<LobbySort> {
    private final int count;
    private final String[] lobbyData;

    public LobbySort(int count, String[] lobbyData) {
        this.count = count;
        this.lobbyData = lobbyData;
    }

    @Override
    public int compareTo(LobbySort o) {
        return Integer.compare(this.count, o.count);
    }

    public int getCount() {
        return this.count;
    }

    public String[] getLobbyData() {
        return this.lobbyData;
    }
}