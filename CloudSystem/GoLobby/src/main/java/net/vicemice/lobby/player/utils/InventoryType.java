package net.vicemice.lobby.player.utils;

public enum InventoryType {
    FRIENDS,
    SETTINGS,
    REPLAYS_SAVED,
    REPLAYS_RECENT,

    LOTTERY,
    DAILY_REWARD,
    CHESTS,

    NONE,
    PETS,
    MOUNTS,
    BOOTS,
    PARTICLE_EFFECTS
}
