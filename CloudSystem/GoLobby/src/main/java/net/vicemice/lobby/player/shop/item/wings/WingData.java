package net.vicemice.lobby.player.shop.item.wings;

import org.bukkit.entity.Player;

/*
 * Class created at 21:09 - 15.04.2020
 * Copyright (C) elrobtossohn
 */
public class WingData {
    public int wingType;
    public float currentSize;
    public boolean equipped;
    public boolean enableGlide;

    public WingData(Player player, int wingType) {
        this.wingType = wingType;
        this.currentSize = 0.0f;
        this.equipped = true;
    }

    public void toggleEquip() {
        this.equipped = !this.equipped;
    }
}