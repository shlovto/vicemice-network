package net.vicemice.lobby.player.shop.utils;

import org.bukkit.entity.Entity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/*
 * Class created at 22:21 - 30.04.2020
 * Copyright (C) elrobtossohn
 */
public class EntityClearupRunnable implements Runnable {
    private Map<Entity, Long> entitiesToCleanup = new HashMap<>();

    @Override
    public void run() {
        long now = System.currentTimeMillis();
        for (Map.Entry<Entity, Long> entityLongEntry : new HashSet<>(this.entitiesToCleanup.entrySet())) {
            if (now - entityLongEntry.getValue() <= TimeUnit.MILLISECONDS.toMillis(500L)) continue;
            this.entitiesToCleanup.remove(entityLongEntry.getKey());
            entityLongEntry.getKey().remove();
        }
    }

    public Map<Entity, Long> getEntitiesToCleanup() {
        return this.entitiesToCleanup;
    }
}