package net.vicemice.lobby.listeners;

import net.vicemice.lobby.GoLobby;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

/*
 * Class created at 12:11 - 03.04.2020
 * Copyright (C) elrobtossohn
 */
public class InventoryListener implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (!GoLobby.getPlayerManager().getBuilders().contains(event.getWhoClicked().getUniqueId())) {
            event.setCancelled(true);
        }
    }
}