package net.vicemice.lobby.netty.switcher;

import io.netty.channel.Channel;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.lobby.GoLobby;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.lobby.LobbyPacket;

public class SwitcherPacketListener extends PacketGetter {

    public void receivePacket(PacketHolder initPacket) {
        if (initPacket.getKey().equals(PacketType.LOBBY_PACKET.getKey())) {
            LobbyPacket lobbyPacket = (LobbyPacket) initPacket.getValue();
            GoLobby.getPlayerManager().receiveSwitcherPacket(lobbyPacket);
        }
    }

    public void channelActive(Channel channel) {
    }
}
