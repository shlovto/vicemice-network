package net.vicemice.lobby.player.inventorys.profile.friends;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.inventorys.profile.DefaultProfile;
import net.vicemice.lobby.player.utils.InventoryType;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.players.api.internal.SkullChanger;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.players.lobby.LobbyFriendData;
import net.vicemice.hector.utils.players.utils.Pagination;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import static org.bukkit.Material.BARRIER;

public class FriendsInventory {

    public GUI create(IUser user, int page) {
        GUI gui = new DefaultProfile().create(user, InventoryType.FRIENDS);

        Pagination<LobbyFriendData> pagination = new Pagination<>(user.getNetworkPlayer().getFriends(), 27);
        AtomicInteger i = new AtomicInteger(9);
        pagination.printPage(page, lobbyFriendData -> {
            ItemBuilder itemBuilder = new ItemBuilder();
            if (lobbyFriendData.isOnline()) {
                itemBuilder = ItemBuilder.create(SkullChanger.getSkull(lobbyFriendData.getSignature(), lobbyFriendData.getValue()));
                itemBuilder = itemBuilder.name((lobbyFriendData.isFavorite() ? "§e✪ " : "") + lobbyFriendData.getRank().getRankColor() + lobbyFriendData.getName()).lore(lobbyFriendData.getMessage());
            } else {
                itemBuilder = itemBuilder.material(Material.SKULL_ITEM).name((lobbyFriendData.isFavorite() ? "§e✪ " : "") + "§7" + lobbyFriendData.getName()).lore(lobbyFriendData.getMessage());
            }

            gui.setItem(i.get(), itemBuilder.build(), e -> {
                new FriendInventory().create(user, lobbyFriendData).open();
            });
            i.set(i.get() + 1);
        });

        if (pagination.getElementsFor(page).isEmpty()) {
            gui.setItem(22, ItemBuilder.create(BARRIER).name(user.translate("friends-no-friends")).build());
        }

        String sort = "friends-sort-az",
                headId = "f3ea52b8-49c8-4fbd-86b9-19f3358d3c42",
                headTexture = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMWE3NGQ5MjEyZTY0OTFiYzM3MGNhZjAyNWZkNDU5ZmU2MzJjYTdjNmJhNGRmN2ViZWZhZmQ0ODlhYjMyZmQifX19";
        if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-sorting", Integer.class) == 0) {
            sort = "friends-sort-az";
            headId = "f3ea52b8-49c8-4fbd-86b9-19f3358d3c42";
            headTexture = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMWE3NGQ5MjEyZTY0OTFiYzM3MGNhZjAyNWZkNDU5ZmU2MzJjYTdjNmJhNGRmN2ViZWZhZmQ0ODlhYjMyZmQifX19";
        } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-sorting", Integer.class) == 1) {
            sort = "friends-sort-za";
            headId = "2ecd2195-a6b2-4c3c-ad63-093bd41c5482";
            headTexture = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvY2ZhNzMwYjVlYzczY2VhZjRhYWQ1MjRlMjA3ZmYzMWRmNzg1ZTdhYjZkYjFhZWIzZjFhYTRkMjQ1ZWQyZSJ9fX0=";
        } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-sorting", Integer.class) == 2) {
            sort = "friends-sort-favorites";
            headId = "bcefcc41-e997-4845-ae08-7b8a1a2d51b6";
            headTexture = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvODY4ZjRjZWY5NDlmMzJlMzNlYzVhZTg0NWY5YzU2OTgzY2JlMTMzNzVhNGRlYzQ2ZTViYmZiN2RjYjYifX19";
        } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-sorting", Integer.class) == 3) {
            sort = "friends-sort-last-seen";
            headId = "42c92521-31d6-4780-a7bd-cecd5e6c044e";
            headTexture = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTBmNGVmN2Q2YzVkOGRhMzYxODI3NTIyM2JkY2ExNDdjOWNmYWFjNzdkMzdlOGFlODFkNzU3YTU5MzY3NmFjMiJ9fX0=";
        } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-sorting", Integer.class) == 4) {
            sort = "friends-sort-offline-time";
            headId = "cea33f6c-b8f1-43f3-a886-cc7c5071377c";
            headTexture = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYjI1NGUyNDI2ZDJhY2M4NGM1YzY1ODg2OWE4NmQ3YTFjYzlhNGY2MDQ4NjZhYWU0NjdiNWI0OTY2NTQwNGY1ZCJ9fX0=";
        }

        gui.setItem(51, ItemBuilder.create(SkullChanger.getSkullByTexture(headId, headTexture)).name(user.translate("friends-sort", user.translate(sort))).lore(new ArrayList<>(Arrays.asList(user.translate("friends-sort-description").split("\n")))).build(), e -> {
            new FriendSortingInventory().create(user).open();
        });
        gui.setItem(52, ItemBuilder.create(GUI.Items.getLeft()).name(user.translate("previous-page")).build(), e -> {
            if (pagination.getElementsFor((page-1)).isEmpty()) return;
            this.create(user, (page-1)).open();
        });
        gui.setItem(53, ItemBuilder.create(GUI.Items.getRight()).name(user.translate("next-page")).build(), e -> {
            if (pagination.getElementsFor((page+1)).isEmpty()) return;
            this.create(user, (page+1)).open();
        });

        List requests = user.getNetworkPlayer().getRequests();
        int size = requests.size();
        if (size > 64) {
            size = 64;
        }

        String requestName = user.translate("friends-no-requests");
        if (requests.size() == 1) {
            requestName = user.translate("friends-one-request");
        } else if (requests.size() > 1) {
            requestName = user.translate("friends-more-requests", requests.size());
        }

        final int requestSize = size;
        gui.setItem(50, new ItemBuilder(Material.BOOK_AND_QUILL).name(requestName).amount((size == 0 ? 1 : size)).build(), e -> {
            if (requestSize > 0) {
                new RequestsInventory().create(user, 1).open();
            }
        });

        return gui;
    }
}