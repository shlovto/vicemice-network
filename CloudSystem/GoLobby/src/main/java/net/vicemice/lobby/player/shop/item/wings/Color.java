package net.vicemice.lobby.player.shop.item.wings;

/*
 * Class created at 21:10 - 15.04.2020
 * Copyright (C) elrobtossohn
 */
public class Color {
    private int red;
    private int green;
    private int blue;

    public Color(int red, int green, int blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public int getBlue() {
        return this.blue;
    }

    public int getGreen() {
        return this.green;
    }

    public int getRed() {
        return this.red;
    }
}
