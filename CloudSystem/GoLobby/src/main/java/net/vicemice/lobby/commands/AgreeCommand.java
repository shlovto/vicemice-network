package net.vicemice.lobby.commands;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/*
 * Class created at 22:29 - 02.05.2020
 * Copyright (C) elrobtossohn
 */
public class AgreeCommand extends Command {

    public AgreeCommand() {
        super("agree");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        IUser user = UserManager.getUser(Bukkit.getPlayer(commandSender.getName()));

        if (user.getNetworkPlayer().getSettingsPacket().getSetting("accept-tos", Boolean.class)) {
            user.sendMessage("user-tos-already-accepted");
            return false;
        }

        user.getNetworkPlayer().getSettingsPacket().setSetting("accept-tos", Boolean.valueOf("true"));
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.UPDATE_PLAYER_SETTINGS, user.getNetworkPlayer().getSettingsPacket()), API.PacketReceiver.SLAVE);
        user.sendMessage("user-tos-accepted");

        return false;
    }
}
