package net.vicemice.lobby.listeners;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.utils.Locations;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.lobby.LobbyLocPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.API;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage(null);
        Player player = event.getPlayer();

        GoLobby.getPlayerManager().getBuilders().remove(event.getPlayer().getUniqueId());
        if (!GoLobby.getPlayerManager().getPlayers().containsKey(player.getUniqueId())) return;

        if (player.getLocation().distance(Locations.getSpawnLocation()) > 0)
            GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.LOBBY_LOC_PACKET, new LobbyLocPacket(player.getUniqueId(),player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ(), player.getLocation().getYaw(), player.getLocation().getPitch())), API.PacketReceiver.SLAVE);

        GoLobby.getPlayerManager().getPlayer(UserManager.getUser(player)).disableWings();
        GoLobby.getPlayerManager().getPlayer(UserManager.getUser(player)).saveLastShopItems();
        GoLobby.getPlayerManager().getPlayers().remove(player.getUniqueId());
    }
}