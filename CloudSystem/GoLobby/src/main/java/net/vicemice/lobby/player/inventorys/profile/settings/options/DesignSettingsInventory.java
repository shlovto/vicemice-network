package net.vicemice.lobby.player.inventorys.profile.settings.options;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.lobby.player.inventorys.profile.settings.SettingsInventory;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;

public class DesignSettingsInventory {

    public GUI create(IUser user) {
        GUI gui = user.createGUI(ChatColor.stripColor(user.translate("settings-change-design")), 6);

        gui.fill(0, 9, user.getUIColor());
        gui.fill(45, 54, user.getUIColor());

        gui.setItem(8, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 14 ? 1 : 14)).name("§c✗").lore(new ArrayList<>(Arrays.asList(user.translate("gui-return").split("\n")))).build(), e -> {
            new SettingsInventory().create(user).open();
        });

        gui.setItem(19, ItemBuilder.create(Material.STAINED_GLASS).durability(0).glow((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 0)).lore((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 0 ? user.translate("already-selected") : user.translate("settings-design-select"))).build(), e -> {
            user.getNetworkPlayer().getSettingsPacket().setSetting("ui-color", 0);
            user.sendSettings();
            new SettingsInventory().create(user).open();
        });
        gui.setItem(20, ItemBuilder.create(Material.STAINED_GLASS).durability(1).glow((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 1)).lore((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 1 ? user.translate("already-selected") : user.translate("settings-design-select"))).build(), e -> {
            user.getNetworkPlayer().getSettingsPacket().setSetting("ui-color", 1);
            user.sendSettings();
            new SettingsInventory().create(user).open();
        });
        gui.setItem(21, ItemBuilder.create(Material.STAINED_GLASS).durability(2).glow((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 2)).lore((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 2 ? user.translate("already-selected") : user.translate("settings-design-select"))).build(), e -> {
            user.getNetworkPlayer().getSettingsPacket().setSetting("ui-color", 2);
            user.sendSettings();
            new SettingsInventory().create(user).open();
        });
        gui.setItem(22, ItemBuilder.create(Material.STAINED_GLASS).durability(3).glow((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 3)).lore((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 3 ? user.translate("already-selected") : user.translate("settings-design-select"))).build(), e -> {
            user.getNetworkPlayer().getSettingsPacket().setSetting("ui-color", 3);
            user.sendSettings();
            new SettingsInventory().create(user).open();
        });
        gui.setItem(23, ItemBuilder.create(Material.STAINED_GLASS).durability(4).glow((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 4)).lore((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 4 ? user.translate("already-selected") : user.translate("settings-design-select"))).build(), e -> {
            user.getNetworkPlayer().getSettingsPacket().setSetting("ui-color", 4);
            user.sendSettings();
            new SettingsInventory().create(user).open();
        });
        gui.setItem(24, ItemBuilder.create(Material.STAINED_GLASS).durability(5).glow((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 5)).lore((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 5 ? user.translate("already-selected") : user.translate("settings-design-select"))).build(), e -> {
            user.getNetworkPlayer().getSettingsPacket().setSetting("ui-color", 5);
            user.sendSettings();
            new SettingsInventory().create(user).open();
        });
        gui.setItem(25, ItemBuilder.create(Material.STAINED_GLASS).durability(6).glow((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 6)).lore((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 6 ? user.translate("already-selected") : user.translate("settings-design-select"))).build(), e -> {
            user.getNetworkPlayer().getSettingsPacket().setSetting("ui-color", 6);
            user.sendSettings();
            new SettingsInventory().create(user).open();
        });

        gui.setItem(28, ItemBuilder.create(Material.STAINED_GLASS).durability(7).glow((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 7)).lore((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 7 ? user.translate("already-selected") : user.translate("settings-design-select"))).build(), e -> {
            user.getNetworkPlayer().getSettingsPacket().setSetting("ui-color", 7);
            user.sendSettings();
            new SettingsInventory().create(user).open();
        });
        gui.setItem(29, ItemBuilder.create(Material.STAINED_GLASS).durability(9).glow((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 9)).lore((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 9 ? user.translate("already-selected") : user.translate("settings-design-select"))).build(), e -> {
            user.getNetworkPlayer().getSettingsPacket().setSetting("ui-color", 9);
            user.sendSettings();
            new SettingsInventory().create(user).open();
        });
        gui.setItem(30, ItemBuilder.create(Material.STAINED_GLASS).durability(10).glow((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 10)).lore((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 10 ? user.translate("already-selected") : user.translate("settings-design-select"))).build(), e -> {
            user.getNetworkPlayer().getSettingsPacket().setSetting("ui-color", 10);
            user.sendSettings();
            new SettingsInventory().create(user).open();
        });
        gui.setItem(31, ItemBuilder.create(Material.STAINED_GLASS).durability(11).glow((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 11)).lore((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 11 ? user.translate("already-selected") : user.translate("settings-design-select"))).build(), e -> {
            user.getNetworkPlayer().getSettingsPacket().setSetting("ui-color", 11);
            user.sendSettings();
            new SettingsInventory().create(user).open();
        });
        gui.setItem(32, ItemBuilder.create(Material.STAINED_GLASS).durability(13).glow((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 13)).lore((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 13 ? user.translate("already-selected") : user.translate("settings-design-select"))).build(), e -> {
            user.getNetworkPlayer().getSettingsPacket().setSetting("ui-color", 13);
            user.sendSettings();
            new SettingsInventory().create(user).open();
        });
        gui.setItem(33, ItemBuilder.create(Material.STAINED_GLASS).durability(14).glow((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 14)).lore((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 14 ? user.translate("already-selected") : user.translate("settings-design-select"))).build(), e -> {
            user.getNetworkPlayer().getSettingsPacket().setSetting("ui-color", 14);
            user.sendSettings();
            new SettingsInventory().create(user).open();
        });
        gui.setItem(34, ItemBuilder.create(Material.STAINED_GLASS).durability(15).glow((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 15)).lore((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 15 ? user.translate("already-selected") : user.translate("settings-design-select"))).build(), e -> {
            user.getNetworkPlayer().getSettingsPacket().setSetting("ui-color", 15);
            user.sendSettings();
            new SettingsInventory().create(user).open();
        });

        return gui;
    }
}