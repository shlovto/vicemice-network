package net.vicemice.lobby;

import lombok.Getter;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.vicemice.lobby.commands.AgreeCommand;
import net.vicemice.lobby.commands.BuildCommand;
import net.vicemice.lobby.commands.DevCommand;
import net.vicemice.lobby.commands.ServersCommand;
import net.vicemice.lobby.netty.info.LobbyInfoListener;
import net.vicemice.lobby.netty.location.LobbyLocationListener;
import net.vicemice.lobby.netty.sign.SignPacketListener;
import net.vicemice.lobby.netty.switcher.SwitcherPacketListener;
import net.vicemice.lobby.player.PlayerManager;
import net.vicemice.lobby.player.shop.item.wings.WingManager;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.players.api.internal.fakemobs.util.FakeMob;
import net.vicemice.hector.utils.Register;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class GoLobby extends JavaPlugin {

    @Getter
    private static GoLobby instance;
    @Getter
    private static PlayerManager playerManager;
    @Getter
    private static FakeMob bwRanked, bwUnranked, qsgRanked, qsgUnranked, heisenberg, privateServer;
    @Getter
    private static Villager villager;

    @Override
    public void onEnable() {
        instance = this;
        playerManager = new PlayerManager();
        ArrayList<String> paths = new ArrayList<>();
        paths.add("cloud/locales/lobby");
        paths.add("cloud/locales/lobby-shop");
        paths.add("cloud/locales/lobby-private-server");
        GoServer.getLocaleManager().addPaths(paths);

        Register.registerListener(this, "net.vicemice.lobby");


        GoServer.getService().getGoAPI().getNettyClient().addGetter(new LobbyInfoListener());
        GoServer.getService().getGoAPI().getNettyClient().addGetter(new SignPacketListener());
        GoServer.getService().getGoAPI().getNettyClient().addGetter(new LobbyLocationListener());
        GoServer.getService().getGoAPI().getNettyClient().addGetter(new SwitcherPacketListener());

        ((CraftServer) getServer()).getCommandMap().register("dev", new DevCommand());
        ((CraftServer) getServer()).getCommandMap().register("agree", new AgreeCommand());
        ((CraftServer) getServer()).getCommandMap().register("servers", new ServersCommand());
        getCommand("build").setExecutor(new BuildCommand());

        //GoServer.getService().getFakeMobs().getSkinQueue().addToQueue(shop = GoServer.getService().getFakeMobs().spawnPlayer(new Location(Bukkit.getWorld("world"), 1.5, 14.0, 51.0, 125.0f, 0.0f), "§eShop"), "PremiumPlus");
        GoServer.getService().getFakeMobs().getSkinQueue().addToQueue(bwRanked = GoServer.getService().getFakeMobs().spawnPlayer(new Location(Bukkit.getWorld("world"), 23.5, 14.5, 73.5, 135.0f, 0.0f), "§cBW §bRanked"), "Wubbzzz");
        GoServer.getService().getFakeMobs().getSkinQueue().addToQueue(bwUnranked = GoServer.getService().getFakeMobs().spawnPlayer(new Location(Bukkit.getWorld("world"), 21.5, 14.5, 74.5, 135.0f, 0.0f), "§cBW §7Unranked"), "Wubbzzz");
        GoServer.getService().getFakeMobs().getSkinQueue().addToQueue(qsgRanked = GoServer.getService().getFakeMobs().spawnPlayer(new Location(Bukkit.getWorld("world"), 41.5, 14.5, -2.5, 45.0f, 0.0f), "§aQSG §bRanked"), "xXxLazzoTS61xXx");
        GoServer.getService().getFakeMobs().getSkinQueue().addToQueue(qsgUnranked = GoServer.getService().getFakeMobs().spawnPlayer(new Location(Bukkit.getWorld("world"), 43.5, 14.5, -0.5, 45.0f, 0.0f), "§aQSG §7Unranked"), "xXxLazzoTS61xXx");
        GoServer.getService().getFakeMobs().getSkinQueue().addToQueue(heisenberg = GoServer.getService().getFakeMobs().spawnPlayer(new Location(Bukkit.getWorld("world"), 2.5, 14.0, 41.5, 0.0f, 0.0f), "§a"), "ByrdDE");
        GoServer.getService().getFakeMobs().getSkinQueue().addToQueue(privateServer = GoServer.getService().getFakeMobs().spawnPlayer(new Location(Bukkit.getWorld("world"), 4.5, 14.0, 50.5, 103.0f, 0.0f), "§a"), "Delayable");
        Bukkit.getWorld("world").setSpawnLocation(-4, 14, 48);

        /* villager = (Villager) Bukkit.getWorld("world").spawnEntity(new Location(Bukkit.getWorld("world"), 2.5, 14.0, 41.5, 0.0f, 0.0f), EntityType.VILLAGER);
        villager.setCanPickupItems(false);
        villager.setRemoveWhenFarAway(false);
        net.minecraft.server.v1_8_R3.Entity nmsEn = ((CraftEntity) villager).getHandle();
        NBTTagCompound compound = new NBTTagCompound();
        nmsEn.c(compound);
        compound.setByte("NoAI", (byte) 1);
        nmsEn.f(compound);*/
    }

    @Override
    public void onDisable() {
        WingManager.stopWingUpdater();
    }
}
