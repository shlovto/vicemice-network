package net.vicemice.lobby.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;

/*
 * Class created at 14:34 - 02.04.2020
 * Copyright (C) elrobtossohn
 */
public class PlayerArmorStandManipulateListener implements Listener {

    @EventHandler
    public void onPlayerArmorStandManipulate(PlayerArmorStandManipulateEvent event) {
        event.setCancelled(true);
    }
}