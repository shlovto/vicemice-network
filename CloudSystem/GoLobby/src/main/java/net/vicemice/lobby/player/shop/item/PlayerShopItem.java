package net.vicemice.lobby.player.shop.item;

import lombok.Getter;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.shop.utils.Found;
import net.vicemice.hector.server.GoServer;
import org.bson.Document;

/*
 * Class created at 10:06 - 02.04.2020
 * Copyright (C) elrobtossohn
 */
public class PlayerShopItem {

    public PlayerShopItem(String itemId, String id, Found found) {
        this.id = itemId;
        this.found = found;
        this.shopItem = GoLobby.getPlayerManager().getShopManager().getItemById(id);
    }

    @Getter
    private String id;
    @Getter
    private Found found;
    @Getter
    private ShopItem shopItem;

    public void delete() {
        GoServer.getService().getMongoManager().getLobbyService().getCollection("lobbyshop_player_items").deleteOne(new Document("itemId", this.getId()));
    }
}
