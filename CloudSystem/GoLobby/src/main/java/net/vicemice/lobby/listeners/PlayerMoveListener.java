package net.vicemice.lobby.listeners;

import net.vicemice.hector.api.util.MinecraftUtil;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

/*
 * Class created at 05:30 - 25.04.2020
 * Copyright (C) elrobtossohn
 */
public class PlayerMoveListener implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(UserManager.getUser(player));

        Vector velocity = player.getVelocity();
        // Check if the player is moving "up"
        if (velocity.getY() > 0 && playerData.getBoots() != null && playerData.getBoots().getActionId() == 95215646) {
            // Default jump velocity
            double jumpVelocity = 0.42F; // Default jump velocity
            // Check if player is not on ladder and if jump velocity calculated is equals to player Y velocity
            if (player.getLocation().getBlock().getType() != Material.LADDER && Double.compare(velocity.getY(), jumpVelocity) == 0)
            {
                Vector dir = player.getEyeLocation().getDirection().normalize();
                dir.multiply(0);
                dir.setX(0);
                dir.setY(2.0);
                dir.setZ(0);
                player.setVelocity(dir);
                player.setAllowFlight(true);
                player.setFlying(true);
            }
        }

        if (MinecraftUtil.compareLocationWithoutHead(event.getFrom(), event.getTo()) && playerData.getBoots() != null && playerData.getBoots().getActionId() != 95215646) {
            final Location loc = event.getPlayer().getLocation().clone().add(0.0, 0.25, 0.0);
            Bukkit.getScheduler().runTaskAsynchronously(GoLobby.getInstance(), () -> playerData.getBoots().getParticle().builder().ofAmount(playerData.getBoots().getCount()).atSpeed(playerData.getBoots().getParticleSpeed()).at(loc).show());
        }

        if (playerData.getBoots() != null && playerData.getBoots().getActionId() == 95215646 && player.isFlying()) {
            final Location loc = event.getPlayer().getLocation().clone().add(0.0, -0.15, 0.0);
            Bukkit.getScheduler().runTaskAsynchronously(GoLobby.getInstance(), () -> playerData.getBoots().getParticle().builder().ofAmount(playerData.getBoots().getCount()).atSpeed(playerData.getBoots().getParticleSpeed()).at(loc).show());
        }

        if (playerData.getLottery().getLocation().distance(player.getLocation()) < 30) {
            playerData.getLottery().display();
        }
        if (playerData.getLottery().getLocation().distance(player.getLocation()) > 30) {
            playerData.getLottery().remove();
        }

        if (playerData.getDailyReward().getLocation().distance(player.getLocation()) < 30) {
            playerData.getDailyReward().display();
        }
        if (playerData.getDailyReward().getLocation().distance(player.getLocation()) > 30) {
            playerData.getDailyReward().remove();
        }

        if (playerData.getItemChest().getLocation().distance(player.getLocation()) < 30) {
            playerData.getItemChest().display();
        }
        if (playerData.getItemChest().getLocation().distance(player.getLocation()) > 30) {
            playerData.getItemChest().remove();
        }

        if (playerData.getPrivateServer().getLocation().distance(player.getLocation()) < 30) {
            playerData.getPrivateServer().display();
        }
        if (playerData.getPrivateServer().getLocation().distance(player.getLocation()) > 30) {
            playerData.getPrivateServer().remove();
        }
    }
}