package net.vicemice.lobby.player.inventorys.server;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.CommandPacket;
import net.vicemice.hector.packets.types.player.ConnectPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.players.api.internal.SkullChanger;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.CommandType;
import net.vicemice.hector.utils.gameserver.ServerData;
import net.vicemice.hector.utils.players.utils.Pagination;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ServerInventory {

    public GUI create(IUser user, int page) {
        Pagination<ServerData> pagination = new Pagination<>(getData(null, null), 27);
        GUI gui = user.createGUI("§3GoServers", (pagination.getPages() > 1 ? 6 : 5));

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        if (pagination.getPages() < page && page > 1)
            this.create(user, 1).open();
        if (pagination.getElementsFor(page).isEmpty())
            gui.setItem(22, ItemBuilder.create(Material.BARRIER).name(user.translate("servernpc-no-servers", "§3GoServer")).build());

        AtomicInteger i = new AtomicInteger(9);
        pagination.printPage(page, server -> {
            String info = user.translate("interaction-connect");
            if (!user.getRank().isHigherEqualsLevel(server.getRank()))
                info = user.translate("interaction-connect-higher-rank");
            List<String> lore = new ArrayList<>(Collections.singletonList(info));
            lore.addAll(Arrays.asList(user.translate("servernpc-information", server.getPlayers(), server.getMaxPlayers(), server.getMessage()).split("\n")));

            gui.setItem(i.get(), new ItemBuilder(Material.PAPER).name("§b"+server.getName()).lore(lore).build(), e -> user.connectServer(server.getName()));

            i.incrementAndGet();
        });

        if (!pagination.getElementsFor((page-1)).isEmpty()) {
            gui.setItem(46, ItemBuilder.create(GUI.Items.getLeft()).name(user.translate("previous-page")).build(), e -> this.create(user, (page-1)).open());
        }
        if (!pagination.getElementsFor((page+1)).isEmpty()) {
            gui.setItem(52, ItemBuilder.create(GUI.Items.getRight()).name(user.translate("next-page")).build(), e -> this.create(user, (page+1)).open());
        }

        return gui;
    }

    public GUI create(IUser user, String title, String template, int page) {
        Pagination<ServerData> pagination = new Pagination<>(getData(template, ServerData.Type.LOBBY), 27);
        GUI gui = user.createGUI(user.translate("navigator-"+title), 6);

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        if (pagination.getPages() < page && page > 1)
            this.create(user, title, template, (page-1)).open();
        if (pagination.getElementsFor(page).isEmpty())
            gui.setItem(22, ItemBuilder.create(Material.BARRIER).name(user.translate("servernpc-no-servers", template)).build());

        AtomicInteger i = new AtomicInteger(9);
        pagination.printPage(page, server -> {
            String info = user.translate("interaction-connect");
            if (!user.getRank().isHigherEqualsLevel(server.getRank()))
                info = user.translate("interaction-connect-higher-rank");
            List<String> lore = new ArrayList<>(Collections.singletonList(info));
            lore.addAll(Arrays.asList(MessageFormat.format(user.translate("servernpc-information"), server.getPlayers(), server.getMaxPlayers(), server.getMessage()).split("\n")));

            gui.setItem(i.get(), new ItemBuilder(Material.PAPER).name("§b"+server.getName()).lore(lore).build(), e -> user.connectServer(server.getName()));

            i.incrementAndGet();
        });

        if (!pagination.getElementsFor((page-1)).isEmpty()) {
            gui.setItem(46, ItemBuilder.create(GUI.Items.getLeft()).name(user.translate("previous-page")).build(), e -> this.create(user, title, template, (page-1)).open());
        }
        gui.setItem(48, ItemBuilder.create(SkullChanger.getSkull("doCeYLhcz36kRwaxxsbXEK39aw1DjQP5+5stu/gcWaZpNWSwJVyDt9DnsDFaDc1bXKNmIWD/0XXfmO/D+qGD5Gzb115aahtlw5DA2x8Ik51u2Ue2oRxE7Q6fN6yuDKQXaUK+nVwY+jOTPycpMWsxkjIY3nPk3mqImEpAbtxAyKMGBNMoxEH1sOqhprGlCy9juTF3tNbCkQ1ctC0iN5sWdGUWENgwqlsSBKQxk6CTAYj899wZ0Deg9mT4HdX4GOj0/VzX2iTZN1tenNN7HFR3hwocJoPgNSUn8hgdrz2BsgfYTO2oO23RsNrGJ5Pay2UPAWLXRzpTGpkXIhMbdpeoHoPKe7mYLuDuWiSDSLtp+NPWvOLqt9pIjyIx/rGWPbcio+0efhxrrONI1TbxnmRf0mWsJDZoOLWN0PeGxBX0fkuifRHQPdFyvLTzyA6oeXbPV4fwsC0R/xlkimqsVT+RKdH0W59Q7i0YF6tGNhxs6CJzlncZX9fj+JczGbXj6uMBRdg6jaQ679VP+LpDC9OHWRudZIUo/docKsvyCYeVx9XGlZJrHJzZVEef+WkUcqyWnPoPy+GP9pBAEwoh6p8ACscPRVh4l+oVWX5gboA9DRQtgrrpv/RUXi9CD6SH/kj6hTYmxBT752KVNjzXLGXAfXZ8U13PwnuJGi7I/XBTaXM=", "eyJ0aW1lc3RhbXAiOjE1ODA3Mjg4NjEwMTMsInByb2ZpbGVJZCI6IjIzZjFhNTlmNDY5YjQzZGRiZGI1MzdiZmVjMTA0NzFmIiwicHJvZmlsZU5hbWUiOiIyODA3Iiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS8xNmJjMDNlY2ZjNGQxZTNiNGIzZTczZTJlNWRiZmRkMjNhZGY4YzFiNzNkMGViYzdhZmMyNWUwZDU0OWFhNDA4In19fQ==")).name(user.translate("quickjoin")).build(), e -> {
            e.getView().close();
            GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.PLAY, new String[]{template})), API.PacketReceiver.SLAVE);
        });
        gui.setItem(50, ItemBuilder.create(SkullChanger.getSkullByTexture("c462a1ab-ae2e-438c-b7cc-cd9d5f3bff04", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzgyMDk1ODQ0OTgyOGZjODMxZDM2YzI2MzcyNDAxNTg4ZGE2MTk2ZjgzMTY0M2MzYTFkYTE3N2QxODZmMzJjIn19fQ==")).name(user.translate("spectate")).build(), e -> {
            this.createSpectate(user, title, template, 1).open();
        });
        if (!pagination.getElementsFor((page+1)).isEmpty()) {
            gui.setItem(52, ItemBuilder.create(GUI.Items.getRight()).name(user.translate("next-page")).build(), e -> this.create(user, title, template, (page+1)).open());
        }

        return gui;
    }

    public GUI createSpectate(IUser user, String title, String template, int page) {
        Pagination<ServerData> pagination = new Pagination<>(getData(template, ServerData.Type.INGAME), 27);
        GUI gui = user.createGUI(user.translate("navigator-"+title), 6);

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        if (pagination.getPages() < page && page > 1)
            this.create(user, title, template, (page-1)).open();
        if (pagination.getElementsFor(page).isEmpty())
            gui.setItem(22, ItemBuilder.create(Material.BARRIER).name(user.translate("servernpc-no-servers", template)).build());

        AtomicInteger i = new AtomicInteger(9);
        pagination.printPage(page, server -> {
            String info = user.translate("interaction-connect");
            if (!user.getRank().isHigherEqualsLevel(server.getRank()))
                info = user.translate("interaction-connect-higher-rank");
            List<String> lore = new ArrayList<>(Collections.singletonList(info));
            lore.addAll(Arrays.asList(user.translate("servernpc-information", server.getPlayers(), server.getMaxPlayers(), server.getMessage()).split("\n")));

            gui.setItem(i.get(), new ItemBuilder(Material.PAPER).name("§b"+server.getName()).lore(lore).build(), e -> user.connectServer(server.getName()));

            i.incrementAndGet();
        });

        if (!pagination.getElementsFor((page-1)).isEmpty()) {
            gui.setItem(46, ItemBuilder.create(GUI.Items.getLeft()).name(user.translate("previous-page")).build(), e -> this.create(user, title, template, (page-1)).open());
        }
        gui.setItem(49, ItemBuilder.create(SkullChanger.getSkull("doCeYLhcz36kRwaxxsbXEK39aw1DjQP5+5stu/gcWaZpNWSwJVyDt9DnsDFaDc1bXKNmIWD/0XXfmO/D+qGD5Gzb115aahtlw5DA2x8Ik51u2Ue2oRxE7Q6fN6yuDKQXaUK+nVwY+jOTPycpMWsxkjIY3nPk3mqImEpAbtxAyKMGBNMoxEH1sOqhprGlCy9juTF3tNbCkQ1ctC0iN5sWdGUWENgwqlsSBKQxk6CTAYj899wZ0Deg9mT4HdX4GOj0/VzX2iTZN1tenNN7HFR3hwocJoPgNSUn8hgdrz2BsgfYTO2oO23RsNrGJ5Pay2UPAWLXRzpTGpkXIhMbdpeoHoPKe7mYLuDuWiSDSLtp+NPWvOLqt9pIjyIx/rGWPbcio+0efhxrrONI1TbxnmRf0mWsJDZoOLWN0PeGxBX0fkuifRHQPdFyvLTzyA6oeXbPV4fwsC0R/xlkimqsVT+RKdH0W59Q7i0YF6tGNhxs6CJzlncZX9fj+JczGbXj6uMBRdg6jaQ679VP+LpDC9OHWRudZIUo/docKsvyCYeVx9XGlZJrHJzZVEef+WkUcqyWnPoPy+GP9pBAEwoh6p8ACscPRVh4l+oVWX5gboA9DRQtgrrpv/RUXi9CD6SH/kj6hTYmxBT752KVNjzXLGXAfXZ8U13PwnuJGi7I/XBTaXM=", "eyJ0aW1lc3RhbXAiOjE1ODA3Mjg4NjEwMTMsInByb2ZpbGVJZCI6IjIzZjFhNTlmNDY5YjQzZGRiZGI1MzdiZmVjMTA0NzFmIiwicHJvZmlsZU5hbWUiOiIyODA3Iiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS8xNmJjMDNlY2ZjNGQxZTNiNGIzZTczZTJlNWRiZmRkMjNhZGY4YzFiNzNkMGViYzdhZmMyNWUwZDU0OWFhNDA4In19fQ==")).name(user.translate("play")).build(), e -> {
            this.create(user, title, template, 1).open();
        });
        if (!pagination.getElementsFor((page+1)).isEmpty()) {
            gui.setItem(52, ItemBuilder.create(GUI.Items.getRight()).name(user.translate("next-page")).build(), e -> this.create(user, title, template, (page+1)).open());
        }

        return gui;
    }

    public List<ServerData> getData(String template, ServerData.Type type) {
        List<ServerData> list = new ArrayList<>();
        for (ServerData data : GoLobby.getPlayerManager().getServerData()) {
            if (template != null && !data.getName().startsWith(template)) continue;
            if (type != null && data.getType() != type) continue;
            list.add(data);
        }
        return list;
    }
}
