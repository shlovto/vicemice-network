package net.vicemice.lobby.listeners;

import net.vicemice.hector.events.UserLoginEvent;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.utils.Locations;
import net.vicemice.lobby.utils.PlayerHotBar;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/*
 * Class created at 02:28 - 03.05.2020
 * Copyright (C) elrobtossohn
 */
public class UserLoginListener implements Listener {

    @EventHandler
    public void onLogin(UserLoginEvent event) {
        IUser user = event.getUser();

        GoLobby.getPlayerManager().getBeforePlayers().remove(user.getUniqueId());
        GoLobby.getPlayerManager().addPlayer(user);

        PlayerHotBar.setItems(user);
        GoLobby.getPlayerManager().getPlayer(user.getUniqueId()).updateBoard();

        GoLobby.getPlayerManager().getPlayer(user.getUniqueId()).updatePlayers();
        GoLobby.getPlayerManager().getPlayers().forEach((k,v) -> v.updatePlayers());

        if (GoLobby.getPlayerManager().getPlayerLocations().containsKey(user.getUniqueId())) {
            Bukkit.getPlayer(user.getUniqueId()).teleport(GoLobby.getPlayerManager().getPlayerLocations().get(user.getUniqueId()));
        } else {
            Bukkit.getPlayer(user.getUniqueId()).teleport(Locations.getSpawnLocation());
        }

        Bukkit.getPlayer(user.getUniqueId()).setGameMode(GameMode.ADVENTURE);

        if (user.getNetworkPlayer().getRank().isHigherEqualsLevel(Rank.VIP) && user.getNetworkPlayer().getSettingsPacket().getSetting("player-visibility", Integer.class) == 2) {
            Bukkit.getPlayer(user.getUniqueId()).setAllowFlight(true);
            Bukkit.getPlayer(user.getUniqueId()).setFlying(true);
        } else if (user.getNetworkPlayer().getRank().isHigherEqualsLevel(Rank.PREMIUM) && user.getNetworkPlayer().getSettingsPacket().getSetting("player-visibility", Integer.class) == 1) {
            Bukkit.getPlayer(user.getUniqueId()).setAllowFlight(true);
        } else {
            Bukkit.getPlayer(user.getUniqueId()).setAllowFlight(false);
            Bukkit.getPlayer(user.getUniqueId()).setFlying(false);
        }

        if (user.getRank().isHigherEqualsLevel(Rank.PREMIUM))
            if (user.getStatistic("user_daily_server_tokens", StatisticPeriod.DAY) == 0) {
                user.increaseStatistic("user_daily_server_tokens", 1);
                user.increaseStatistic("private_server_tokens", 10);
            }
    }
}