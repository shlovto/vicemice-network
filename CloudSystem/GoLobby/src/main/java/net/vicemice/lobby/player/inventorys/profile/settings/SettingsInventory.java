package net.vicemice.lobby.player.inventorys.profile.settings;

import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.lobby.player.inventorys.profile.DefaultProfile;
import net.vicemice.lobby.player.inventorys.profile.settings.options.sub.ClanInvitationsInventory;
import net.vicemice.lobby.player.inventorys.profile.settings.options.sub.FriendRequestInventory;
import net.vicemice.lobby.player.inventorys.profile.settings.options.sub.PartyInvitationsInventory;
import net.vicemice.lobby.player.utils.InventoryType;
import net.vicemice.hector.utils.players.api.internal.SkullChanger;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.lobby.player.inventorys.profile.settings.options.*;
import org.bukkit.Material;
import java.util.ArrayList;
import java.util.Arrays;

public class SettingsInventory {

    public GUI create(IUser user) {
        GUI gui = new DefaultProfile().create(user, InventoryType.SETTINGS);
        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(user);

        boolean extended = (playerData.getNetworkPlayer().getSettingsPacket().getSetting("advanced-player-settings", Integer.class) == 1);

        if (extended) {
            gui.setItem(18, ItemBuilder.create(SkullChanger.getSkull(user.getBukkitPlayer())).name(user.translate("settings-friend-settings")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-friend-settings-description").split("\n")))).build(), e -> {
                new FriendSettingsInventory().create(user).open();
            });
            gui.setItem(19, ItemBuilder.create(Material.MAP).name(user.translate("settings-friend-requests")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-friend-requests-description").split("\n")))).build(), e -> {
                new FriendRequestInventory().create(user).open();
            });
            gui.setItem(20, ItemBuilder.create(Material.IRON_CHESTPLATE).name(user.translate("settings-clan-settings")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-clan-settings-description").split("\n")))).build(), e -> {
                new ClanSettingsInventory().create(user).open();
            });
            gui.setItem(21, ItemBuilder.create(SkullChanger.getSkullByTexture("99e42a6d-44c4-4b62-a2f8-eabe9cdf2933", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZGYyYzlkYzMxNGJlZmM4ZDEyZjJlODc4NmM5NDM1YmZkMjRkOTExNmZjOTRjNWVjNDVmZDgzYmQyZGM4NGE0ZiJ9fX0=")).name(user.translate("settings-clan-invitations")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-clan-invitations-description").split("\n")))).build(), e -> {
                new ClanInvitationsInventory().create(user).open();
            });
            gui.setItem(22, ItemBuilder.create(Material.CAKE).name(user.translate("settings-party-invitations")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-party-invitations-menu-description").split("\n")))).build(), e -> {
                new PartyInvitationsInventory().create(user).open();
            });
            gui.setItem(23, ItemBuilder.create(Material.PAPER).name(user.translate("settings-change-language")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-change-language-description").split("\n")))).build(), e -> {
                new LocaleSettingsInventory().create(user, 1).open();
            });
            gui.setItem(24, ItemBuilder.create(user.getUIColor2()).name(user.translate("settings-change-design")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-change-design-description").split("\n")))).build(), e -> {
                new DesignSettingsInventory().create(user).open();
            });
            gui.setItem(25, ItemBuilder.create((user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 2 ? Material.FEATHER : (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 1 ? Material.SLIME_BALL : Material.LEATHER_BOOTS))).name(user.translate("settings-change-movement")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-change-movement-description").split("\n")))).build(), e -> {
                new MovementSettingsInventory().create(user).open();
            });
            gui.setItem(26, ItemBuilder.create(Material.BOOK_AND_QUILL).name(user.translate("settings-further-settings")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-further-settings-description").split("\n")))).build(), e -> {
                new FurtherSettingsInventory().create(user).open();
            });
        } else {
            gui.setItem(19, ItemBuilder.create(SkullChanger.getSkull(user.getBukkitPlayer())).name(user.translate("settings-friend-settings")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-friend-settings-description").split("\n")))).build(), e -> {
                new FriendSettingsInventory().create(user).open();
            });
            gui.setItem(20, ItemBuilder.create(Material.IRON_CHESTPLATE).name(user.translate("settings-clan-settings")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-clan-settings-description").split("\n")))).build(), e -> {
                new ClanSettingsInventory().create(user).open();
            });

            gui.setItem(21, ItemBuilder.create(Material.CAKE).name(user.translate("settings-party-invitations")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-party-invitations-menu-description").split("\n")))).build(), e -> {
                new PartyInvitationsInventory().create(user).open();
            });
            gui.setItem(22, ItemBuilder.create(Material.PAPER).name(user.translate("settings-change-language")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-change-language-description").split("\n")))).build(), e -> {
                new LocaleSettingsInventory().create(user, 1).open();
            });
            gui.setItem(23, ItemBuilder.create(user.getUIColor2()).name(user.translate("settings-change-design")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-change-design-description").split("\n")))).build(), e -> {
                new DesignSettingsInventory().create(user).open();
            });
            gui.setItem(24, ItemBuilder.create((user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 2 ? Material.FEATHER : (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 1 ? Material.SLIME_BALL : Material.LEATHER_BOOTS))).name(user.translate("settings-change-movement")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-change-movement-description").split("\n")))).build(), e -> {
                new MovementSettingsInventory().create(user).open();
            });
            gui.setItem(25, ItemBuilder.create(Material.BOOK_AND_QUILL).name(user.translate("settings-further-settings")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-further-settings-description").split("\n")))).build(), e -> {
                new FurtherSettingsInventory().create(user).open();
            });
        }

        return gui;
    }
}
