package net.vicemice.lobby.player.shop.item.pets;

import lombok.Getter;
import org.bukkit.entity.EntityType;

import java.util.UUID;

/*
 * Class created at 03:22 - 15.04.2020
 * Copyright (C) elrobtossohn
 */
@Getter
public class Pet {
    private EntityType type;
    private String name;
    private UUID owner;
    private String itemId;

    public Pet(EntityType type, String name, UUID owner, String itemId) {
        this.type = type;
        this.name = name;
        this.owner = owner;
        this.itemId = itemId;
    }
}