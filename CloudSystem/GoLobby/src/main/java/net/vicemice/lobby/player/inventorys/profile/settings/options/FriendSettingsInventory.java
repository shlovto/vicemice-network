package net.vicemice.lobby.player.inventorys.profile.settings.options;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.lobby.player.inventorys.profile.settings.SettingsInventory;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;

public class FriendSettingsInventory {

    public GUI create(IUser user) {
        GUI gui = new GUI(user, ChatColor.stripColor(user.translate("settings-friend-settings")), 4);
        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(user);

        gui.fill(0, 9, user.getUIColor());
        gui.fill(27, 36, user.getUIColor());

        gui.setItem(8, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability((playerData.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 14 ? 1 : 14)).name("§c✗").lore(new ArrayList<>(Arrays.asList(user.translate("gui-return").split("\n")))).build(), e -> {
            new SettingsInventory().create(user).open();
        });

        if (playerData.getNetworkPlayer().getSettingsPacket().getSetting("advanced-player-settings", Integer.class) == 0) {
            gui.setItem(9, ItemBuilder.create(Material.BOOK_AND_QUILL).name(user.translate("settings-friend-requests-item")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-friend-requests-description").split("\n")))).build());
            gui.setItem(11, ItemBuilder.create(Material.PAPER).name(user.translate("settings-friend-private-messages")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-friend-private-messages-description").split("\n")))).build());
            gui.setItem(13, ItemBuilder.create(Material.EYE_OF_ENDER).name(user.translate("settings-friend-replay-list")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-friend-replay-list-description").split("\n")))).build());
            gui.setItem(15, ItemBuilder.create(Material.ENDER_PEARL).name(user.translate("settings-friend-jump")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-friend-jump-description").split("\n")))).build());
            gui.setItem(17, ItemBuilder.create(Material.SIGN).name(user.translate("settings-friend-messages")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-friend-messages-description").split("\n")))).build());

            boolean requests = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add", Integer.class) == 1);
            gui.setItem(18, ItemBuilder.create(Material.INK_SACK).durability((requests ? 10 : 8)).name((requests ? "§a✔": "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
            boolean privateMessage = (user.getNetworkPlayer().getSettingsPacket().getSetting("private-messages", Integer.class) == 1);
            gui.setItem(20, ItemBuilder.create(Material.INK_SACK).durability((privateMessage ? 10 : 8)).name((privateMessage ? "§a✔": "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("private-messages", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("private-messages", 1);
                } else if (playerData.getNetworkPlayer().getSettingsPacket().getSetting("friend-add", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("private-messages", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
            boolean replayList = (user.getNetworkPlayer().getSettingsPacket().getSetting("replay-history-private", Integer.class) == 0);
            gui.setItem(22, ItemBuilder.create(Material.INK_SACK).durability((replayList ? 10 : 8)).name((replayList ? "§a✔": "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("replay-history-private", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("replay-history-private", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("replay-history-private", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("replay-history-private", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
            boolean jump = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-jump", Integer.class) == 1);
            gui.setItem(24, ItemBuilder.create(Material.INK_SACK).durability((jump ? 10 : 8)).name((jump ? "§a✔": "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-jump", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("friend-jump", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-jump", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("friend-jump", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
            boolean onlineOfflineMessage = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-messages", Integer.class) == 1);
            gui.setItem(26, ItemBuilder.create(Material.INK_SACK).durability((onlineOfflineMessage ? 10 : 8)).name((onlineOfflineMessage ? "§a✔": "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-messages", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("friend-messages", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-messages", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("friend-messages", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
        } else if (playerData.getNetworkPlayer().getSettingsPacket().getSetting("advanced-player-settings", Integer.class) == 1) {
            gui.setItem(10, ItemBuilder.create(Material.PAPER).name(user.translate("settings-friend-private-messages")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-friend-private-messages-description").split("\n")))).build());
            gui.setItem(12, ItemBuilder.create(Material.EYE_OF_ENDER).name(user.translate("settings-friend-replay-list")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-friend-replay-list-description").split("\n")))).build());
            gui.setItem(14, ItemBuilder.create(Material.ENDER_PEARL).name(user.translate("settings-friend-jump")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-friend-jump-description").split("\n")))).build());
            gui.setItem(16, ItemBuilder.create(Material.SIGN).name(user.translate("settings-friend-messages")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-friend-messages-description").split("\n")))).build());

            boolean privateMessage = (user.getNetworkPlayer().getSettingsPacket().getSetting("private-messages", Integer.class) == 1);
            gui.setItem(19, ItemBuilder.create(Material.INK_SACK).durability((privateMessage ? 10 : 8)).name((privateMessage ? "§a✔": "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("private-messages", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("private-messages", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("private-messages", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
            boolean replayList = (user.getNetworkPlayer().getSettingsPacket().getSetting("replay-history-private", Integer.class) == 0);
            gui.setItem(21, ItemBuilder.create(Material.INK_SACK).durability((replayList ? 10 : 8)).name((replayList ? "§a✔": "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("replay-history-private", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("replay-history-private", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("replay-history-private", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("replay-history-private", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
            boolean jump = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-jump", Integer.class) == 1);
            gui.setItem(23, ItemBuilder.create(Material.INK_SACK).durability((jump ? 10 : 8)).name((jump ? "§a✔": "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-jump", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("friend-jump", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-jump", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("friend-jump", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
            boolean onlineOfflineMessage = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-messages", Integer.class) == 1);
            gui.setItem(25, ItemBuilder.create(Material.INK_SACK).durability((onlineOfflineMessage ? 10 : 8)).name((onlineOfflineMessage ? "§a✔": "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-messages", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("friend-messages", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-messages", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("friend-messages", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
        }

        return gui;
    }
}
