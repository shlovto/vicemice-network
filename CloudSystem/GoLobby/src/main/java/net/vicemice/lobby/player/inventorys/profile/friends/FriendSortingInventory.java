package net.vicemice.lobby.player.inventorys.profile.friends;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.hector.utils.players.api.internal.SkullChanger;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import java.util.ArrayList;
import java.util.Arrays;

/*
 * Class created at 08:48 - 18.04.2020
 * Copyright (C) elrobtossohn
 */
public class FriendSortingInventory {

    public GUI create(IUser user) {
        GUI gui = user.createGUI(user.translate("friends-sort-title"), 4);
        gui.fill(0, 9, user.getUIColor());
        gui.fill(27, 36, user.getUIColor());

        gui.setItem(8, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 14 ? 1 : 14)).name("§c✗").lore(new ArrayList<>(Arrays.asList(user.translate("gui-return").split("\n")))).build(), e -> {
            new FriendsInventory().create(user, 1).open();
        });

        String azHead = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMWE3NGQ5MjEyZTY0OTFiYzM3MGNhZjAyNWZkNDU5ZmU2MzJjYTdjNmJhNGRmN2ViZWZhZmQ0ODlhYjMyZmQifX19";
        gui.setItem(11, ItemBuilder.create(SkullChanger.getSkullByTexture("f3ea52b8-49c8-4fbd-86b9-19f3358d3c42", azHead)).name(user.translate("friends-sort-az")).lore(new ArrayList<>(Arrays.asList(user.translate("friends-sort-az-description").split("\n")))).build());
        String zaHead = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvY2ZhNzMwYjVlYzczY2VhZjRhYWQ1MjRlMjA3ZmYzMWRmNzg1ZTdhYjZkYjFhZWIzZjFhYTRkMjQ1ZWQyZSJ9fX0=";
        gui.setItem(12, ItemBuilder.create(SkullChanger.getSkullByTexture("2ecd2195-a6b2-4c3c-ad63-093bd41c5482", zaHead)).name(user.translate("friends-sort-za")).lore(new ArrayList<>(Arrays.asList(user.translate("friends-sort-za-description").split("\n")))).build());
        String favoritesHead = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvODY4ZjRjZWY5NDlmMzJlMzNlYzVhZTg0NWY5YzU2OTgzY2JlMTMzNzVhNGRlYzQ2ZTViYmZiN2RjYjYifX19";
        gui.setItem(13, ItemBuilder.create(SkullChanger.getSkullByTexture("bcefcc41-e997-4845-ae08-7b8a1a2d51b6", favoritesHead)).name(user.translate("friends-sort-favorites")).lore(new ArrayList<>(Arrays.asList(user.translate("friends-sort-favorites-description").split("\n")))).build());
        String lastSeenHead = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTBmNGVmN2Q2YzVkOGRhMzYxODI3NTIyM2JkY2ExNDdjOWNmYWFjNzdkMzdlOGFlODFkNzU3YTU5MzY3NmFjMiJ9fX0=";
        gui.setItem(14, ItemBuilder.create(SkullChanger.getSkullByTexture("42c92521-31d6-4780-a7bd-cecd5e6c044e", lastSeenHead)).name(user.translate("friends-sort-last-seen")).lore(new ArrayList<>(Arrays.asList(user.translate("friends-sort-last-seen-description").split("\n")))).build());
        String offlineTimeHead = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYjI1NGUyNDI2ZDJhY2M4NGM1YzY1ODg2OWE4NmQ3YTFjYzlhNGY2MDQ4NjZhYWU0NjdiNWI0OTY2NTQwNGY1ZCJ9fX0=";
        gui.setItem(15, ItemBuilder.create(SkullChanger.getSkullByTexture("cea33f6c-b8f1-43f3-a886-cc7c5071377c", offlineTimeHead)).name(user.translate("friends-sort-offline-time")).lore(new ArrayList<>(Arrays.asList(user.translate("friends-sort-offline-time-description").split("\n")))).build());

        boolean az = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-sorting", Integer.class) == 0);
        gui.setItem(20, ItemBuilder.create(Material.INK_SACK).durability((az ? 10 : 8)).name((az ? "§a✔": "§c✘")).build(), e -> {
            if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-sorting", Integer.class) == 0) return;
            user.getNetworkPlayer().getSettingsPacket().setSetting("friend-sorting", 0);
            user.sendSettings();
            this.create(user).open();
        });

        boolean za = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-sorting", Integer.class) == 1);
        gui.setItem(21, ItemBuilder.create(Material.INK_SACK).durability((za ? 10 : 8)).name((za ? "§a✔": "§c✘")).build(), e -> {
            if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-sorting", Integer.class) == 1) return;
            user.getNetworkPlayer().getSettingsPacket().setSetting("friend-sorting", 1);
            user.sendSettings();
            this.create(user).open();
        });

        boolean favorites = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-sorting", Integer.class) == 2);
        gui.setItem(22, ItemBuilder.create(Material.INK_SACK).durability((favorites ? 10 : 8)).name((favorites ? "§a✔": "§c✘")).build(), e -> {
            if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-sorting", Integer.class) == 2) return;
            user.getNetworkPlayer().getSettingsPacket().setSetting("friend-sorting", 2);
            user.sendSettings();
            this.create(user).open();
        });

        boolean lastSeen = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-sorting", Integer.class) == 3);
        gui.setItem(23, ItemBuilder.create(Material.INK_SACK).durability((lastSeen ? 10 : 8)).name((lastSeen ? "§a✔": "§c✘")).build(), e -> {
            if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-sorting", Integer.class) == 3) return;
            user.getNetworkPlayer().getSettingsPacket().setSetting("friend-sorting", 3);
            user.sendSettings();
            this.create(user).open();
        });

        boolean offlineTime = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-sorting", Integer.class) == 4);
        gui.setItem(24, ItemBuilder.create(Material.INK_SACK).durability((offlineTime ? 10 : 8)).name((offlineTime ? "§a✔": "§c✘")).build(), e -> {
            if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-sorting", Integer.class) == 4) return;
            user.getNetworkPlayer().getSettingsPacket().setSetting("friend-sorting", 4);
            user.sendSettings();
            this.create(user).open();
        });

        return gui;
    }
}