package net.vicemice.lobby.player.inventorys.inventory.type;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.inventorys.inventory.DefaultInventory;
import net.vicemice.lobby.player.shop.item.PlayerShopItem;
import net.vicemice.lobby.player.shop.item.pets.PetUtils;
import net.vicemice.lobby.player.shop.utils.Category;
import net.vicemice.lobby.player.utils.InventoryType;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.players.utils.Pagination;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

/*
 * Class created at 03:08 - 15.04.2020
 * Copyright (C) elrobtossohn
 */
public class PetsInventory {

    public GUI create(IUser user, int page) {
        Pagination<Integer> pagination = new Pagination<>(new ArrayList<>(GoLobby.getPlayerManager().getPlayer(user).getItems().get(Category.PETS).keySet()), 27);
        GUI gui = new DefaultInventory().create(user, InventoryType.PETS);

        AtomicInteger i = new AtomicInteger(9);
        pagination.printPage(page, itemId -> {
            PlayerShopItem item = GoLobby.getPlayerManager().getPlayer(user).getItems().get(Category.PETS).get(itemId);
            if (item == null) return;
            boolean glow = (PetUtils.pets.containsKey(user) && PetUtils.pets.get(user).getItemId().equalsIgnoreCase(item.getId()));
            String customName = user.translate("no-custom-name");

            if (GoLobby.getPlayerManager().getPlayer(user).getPetNames().containsKey(item.getId())) {
                customName = GoLobby.getPlayerManager().getPlayer(user).getPetNames().get(item.getId());
            }

            gui.setItem(i.get(), ItemBuilder.create(item.getShopItem().getDefaultItem(user)).name(user.translate(item.getShopItem().getLanguageKey())).lore(new ArrayList<>(Arrays.asList((user.translate("shop-item-pet-description", user.translate(glow ? "shop-pet-selected" : "shop-pet-selectable"), item.getShopItem().getRarity().getRarityColor()+user.translate(item.getShopItem().getRarity().getKey()), user.translate("damage-new"), user.translate(item.getShopItem().getLanguageKey()+"-description"), customName)).split("\n")))).glow(glow).build(), e -> {
                new DefaultInventory().generateViewPets(user, item.getShopItem().getDefaultItem(user), item.getShopItem().getCategory(), user.translate(item.getShopItem().getLanguageKey()), itemId).open();
            });
            i.set((i.get() + 1));
        });

        if (PetUtils.pets.containsKey(user)) {
            gui.setItem(50, ItemBuilder.create(Material.BARRIER).name(user.translate("shop-item-remove-pet")).build(), e -> {
                PetUtils.deletePet(user);
                this.create(user, page).open();
            });
        }

        if (pagination.getElementsFor(page).isEmpty()) {
            gui.setItem(22, ItemBuilder.create(Material.BARRIER).name(user.translate("shop-nothing-available")).build());
        }

        if (!pagination.getElementsFor((page - 1)).isEmpty()) {
            gui.setItem(50, ItemBuilder.create(GUI.Items.getLeft()).name(user.translate("previous-page")).build(), e -> {
                this.create(user, (page - 1)).open();
            });
        }
        if (!pagination.getElementsFor((page + 1)).isEmpty()) {
            gui.setItem(52, ItemBuilder.create(GUI.Items.getRight()).name(user.translate("next-page")).build(), e -> {
                this.create(user, (page + 1)).open();
            });
        }

        return gui;
    }
}