package net.vicemice.lobby.listeners;

import net.vicemice.lobby.GoLobby;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreakListener implements Listener {

    @EventHandler
    public void onPlace(BlockBreakEvent event) {
        if (!GoLobby.getPlayerManager().getBuilders().contains(event.getPlayer().getUniqueId())) event.setCancelled(true);
    }
}
