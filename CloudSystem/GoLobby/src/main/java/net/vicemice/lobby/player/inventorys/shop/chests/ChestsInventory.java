package net.vicemice.lobby.player.inventorys.shop.chests;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.api.util.RandomStringGenerator;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.lobby.player.inventorys.inventory.DefaultInventory;
import net.vicemice.lobby.player.inventorys.shop.chests.type.Animator;
import net.vicemice.lobby.player.inventorys.shop.chests.type.IAnimator;
import net.vicemice.lobby.player.shop.item.PlayerShopItem;
import net.vicemice.lobby.player.shop.item.ShopItem;
import net.vicemice.lobby.player.shop.utils.Category;
import net.vicemice.lobby.player.shop.utils.Found;
import net.vicemice.lobby.player.shop.utils.Rarity;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.utils.Pagination;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static org.bukkit.Material.*;

/*
 * Class created at 21:25 - 01.04.2020
 * Copyright (C) elrobtossohn
 */
public class ChestsInventory {

    public static ArrayList<IUser> reopen = new ArrayList<>();

    public GUI create(IUser user, int page) {
        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(user);
        GUI gui = new GUI(user, ChatColor.stripColor(user.translate("shop-chests")), 6);

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        Pagination<Long> pagination = new Pagination<>(getChests(user), 27);
        AtomicInteger i = new AtomicInteger(9);

        pagination.printPage(page, number -> {
            gui.setItem(i.get(), ItemBuilder.create(CHEST).name(user.translate("chests-chest")).lore((user.translate("open-chest"))).build(), e -> {
                user.decreaseStatistic("lobby_chests", 1);
                /*playerData.getOpeningManager().start(player, player.getLocation(), Quality.COMMON);
                e.getView().close();*/
                startAnimation(user);
            });

            i.set(i.get() + 1);
        });

        if (pagination.getElementsFor(page).isEmpty()) {
            gui.setItem(22, ItemBuilder.create(BARRIER).name(user.translate("chests-no-chests")).build());
        }

        if (!pagination.getElementsFor((page-1)).isEmpty()) {
            gui.setItem(47, ItemBuilder.create(GUI.Items.getLeft()).name(user.translate("previous-page")).build(), e -> {
                this.create(user, (page-1)).open();
            });
        }
        gui.setItem(49, ItemBuilder.create(DOUBLE_PLANT).name(user.translate("buy-chests")).lore((playerData.getNetworkPlayer().getCoins() >= 10000 ? user.translate("buy-chest") : user.translate("chest-not-enough-coins", (10000-playerData.getNetworkPlayer().getCoins())))).build(), e -> {
            if (playerData.getNetworkPlayer().getCoins() < 10000) return;
            user.decreaseCoins(10000);
            user.increaseStatistic("lobby_chests", 1);
            reopen.add(user);
        });
        if (!pagination.getElementsFor((page+1)).isEmpty()) {
            gui.setItem(51, ItemBuilder.create(GUI.Items.getRight()).name(user.translate("next-page")).build(), e -> {
                this.create(user, (page+1)).open();
            });
        }

        return gui;
    }

    public List<Long> getChests(IUser user) {
        List<Long> list = new ArrayList<>();

        for (long i = 0; i < user.getStatistic("lobby_chests", StatisticPeriod.GLOBAL); i++) {
            list.add(i);
        }

        return list;
    }

    public void startAnimation(IUser user) {
        Animator animator = new Animator(user, new IAnimator() {

            @Override
            public GUI setupInventory(IUser user) {
                GUI inv = user.createGUI(user.translate("shop-your-prize"), 5);
                for (int i = 0; i < inv.getSize(); ++i) {
                }
                return inv;
            }

            @Override
            public void onTick(IUser user, GUI gui, List<ShopItem> items, boolean lastTick) {
                user.playSound(Sound.WOOD_CLICK, 1.0f, 1.0f);
                if (lastTick && user != null) {
                    user.playSound(Sound.LEVEL_UP, 1.0f, 1.0f);
                    ShopItem prize = GoLobby.getPlayerManager().getShopManager().getItemByItemStack(user, gui.getHandle().getItem(22).clone());

                    PlayerData data = GoLobby.getPlayerManager().getPlayer(user);
                    String itemId = RandomStringGenerator.generateRandomString(255, RandomStringGenerator.Mode.ALPHANUMERIC);
                    data.getBuyedData().add(itemId);
                    int id = (data.getItems().size()+1);
                    data.getItems().get(prize.getCategory()).put(id, new PlayerShopItem(itemId, prize.getId(), Found.CHEST_OPENING));
                    data.saveAll();
                    if (prize.getRarity() == Rarity.LEGENDARY) {
                        for (IUser all : UserManager.getConnectedUsers().values()) {
                            all.sendMessage("player-found", (user.getRank().getRankColor()+user.getName()), prize.getRarity().getRarityColor()+all.translate(prize.getLanguageKey()), all.translate("shop-"+prize.getCategory().name().toLowerCase().replace("_", "-")));
                            all.playSound(all.getLocation(), Sound.ENDERDRAGON_DEATH, 1.0F, 1.0F);
                        }
                    }

                    gui.fill(9, 36, ItemBuilder.create(STAINED_GLASS_PANE).durability(prize.getRarity().getData()).name(prize.getRarity().getRarityColor()+user.translate(prize.getRarity().getKey())).build(), e -> {}, true);
                    gui.setItem(22, prize.getDefaultItem(user), e-> {
                        if (prize.getCategory() == Category.PETS) {
                            new DefaultInventory().generateViewPets(user, prize.getDefaultItem(user), prize.getCategory(), user.translate(prize.getLanguageKey()), id).open();
                        } else {
                            new DefaultInventory().generateView(user, prize.getDefaultItem(user), prize.getCategory(), user.translate(prize.getLanguageKey()), id).open();
                        }
                    });
                    gui.setItem(4, new ItemStack(AIR));
                    gui.setItem(40, new ItemStack(AIR));

                    ItemStack chest = new ItemBuilder(Material.CHEST).name(user.translate("shop-play-again")).lore(new ArrayList<>(Arrays.asList(user.translate((user.getStatistic("lobby_chests", StatisticPeriod.GLOBAL) == 1 ? "shop-play-again-one-chest-left" : "shop-play-again-chests-left"), user.getStatistic("lobby_chests", StatisticPeriod.GLOBAL)).split("\n")))).build();
                    gui.setItem(36, chest, e -> {
                        if (user.getStatistic("lobby_chests", StatisticPeriod.GLOBAL) < 1) return;
                        user.decreaseStatistic("lobby_chests", 1);
                        startAnimation(user);
                    });

                    if (prize != null) {
                        user.sendMessage("you-found", prize.getRarity().getRarityColor()+user.translate(prize.getLanguageKey()), user.translate("shop-"+prize.getCategory().name().toLowerCase().replace("_", "-")));
                    } else {
                        user.sendMessage("you-found-nothing");
                    }
                    return;
                }

                gui.setItem(18, items.get(0).getDefaultItem(user));
                gui.setItem(19, items.get(1).getDefaultItem(user));
                gui.setItem(20, items.get(2).getDefaultItem(user));
                gui.setItem(21, items.get(3).getDefaultItem(user));
                gui.setItem(22, items.get(4).getDefaultItem(user));
                gui.setItem(23, items.get(5).getDefaultItem(user));
                gui.setItem(24, items.get(6).getDefaultItem(user));
                gui.setItem(25, items.get(7).getDefaultItem(user));
                gui.setItem(26, items.get(8).getDefaultItem(user));
                gui.setItem(4, new ItemBuilder(Material.NETHER_STAR).name("§5"+user.translate("shop-your-prize")).build());
                gui.setItem(40, new ItemBuilder(Material.NETHER_STAR).name("§5"+user.translate("shop-your-prize")).build());
                items.add(items.get(0));
                items.remove(items.get(0));
            }
        });

            List<ShopItem> items = new ArrayList<>();

            List<ShopItem> common =  GoLobby.getPlayerManager().getShopManager().getCommonItems();
            List<ShopItem> uncommon = GoLobby.getPlayerManager().getShopManager().getUncommonItems();
            List<ShopItem> rare = GoLobby.getPlayerManager().getShopManager().getRareItems();
            List<ShopItem> epic = GoLobby.getPlayerManager().getShopManager().getEpicItems();
            List<ShopItem> legendary = GoLobby.getPlayerManager().getShopManager().getLegendaryItems();

            Random rnd = new Random();

            if(common.size() > 0) {
                for (int i = 0; i < 16; i++) {
                    items.add(common.get(rnd.nextInt(common.size())));
                }
            }
            if(uncommon.size() > 0) {
                for (int i = 0; i < 4; i++) {
                    items.add(uncommon.get(rnd.nextInt(uncommon.size())));
                }
            }
            if(rare.size() > 0) {
                for (int j = 0; j < 2; j++) {
                    items.add(rare.get(rnd.nextInt(rare.size())));
                }
            }
            if(epic.size() > 0) {
                for (int k = 0; k < 1; k++) {
                    items.add(epic.get(rnd.nextInt(epic.size())));
                }
            }
            if(legendary.size() > 0) {
                for (int l = 0; l < 1; l++) {
                    items.add(legendary.get(rnd.nextInt(legendary.size())));
                }
            }

        Collections.shuffle(items);
        animator.startAnimation(70, items, 20L);
    }
}
