package net.vicemice.lobby.player.shop.item;

import lombok.Data;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.player.shop.utils.Category;
import net.vicemice.lobby.player.shop.utils.Rarity;
import net.vicemice.hector.utils.players.api.internal.SkullChanger;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import java.util.HashMap;

/*
 * Class created at 09:51 - 02.04.2020
 * Copyright (C) elrobtossohn
 */
@Data
public class ShopItem {

    public ShopItem() {
        this.values = new HashMap<>();
    }

    public ShopItem(Category category, Rarity rarity, String id, String languageKey, Long price, boolean giveAble, boolean sellAble, boolean findAble, HashMap<String, String> values) {
        this.category = category;
        this.rarity = rarity;
        this.id = id;
        this.languageKey = languageKey;
        this.price = price;
        this.giveAble = giveAble;
        this.sellAble = sellAble;
        this.findAble = findAble;
        this.values = values;
    }

    private Category category;
    private Rarity rarity;
    private String id, languageKey;
    private Long price;
    private boolean giveAble, sellAble, findAble;
    private HashMap<String, String> values;

    public ItemStack getDefaultItem(IUser user) {
        Color color = null;
        ItemBuilder item = ItemBuilder.create(Material.STONE).name("§cInvalid item");
        if (this.getValues().containsKey("id") && this.getValues().containsKey("texture")) {
            item = ItemBuilder.create(SkullChanger.getSkullByTexture(this.getValues().get("id"), this.getValues().get("texture")));
            item = item.name(user.translate(this.getLanguageKey()));
        } else if (this.getValues().containsKey("material")) {
            item = ItemBuilder.create(Material.valueOf(this.getValues().get("material")));
            item = item.name(user.translate(this.getLanguageKey()));

            if (item.getMaterial() == Material.LEATHER_BOOTS) {
                if (this.getValues().containsKey("color")) {
                    String[] colorList = this.getValues().get("color").split(";");
                    color = Color.fromRGB(Integer.parseInt(colorList[0]),Integer.parseInt(colorList[1]),Integer.parseInt(colorList[2]));
                }
            }
        } else {
            if (this.getLanguageKey() != null) item = item.lore("§7Item-Language-Key: §9"+this.getLanguageKey());
            if (user.translate(this.getLanguageKey()) != null) item = item.lore("§7Item-Language-Message: §9"+user.translate(this.getLanguageKey()));
        }

        if (color != null) {
            return item.build(color);
        }
        return item.build();
    }

    public ItemStack getItem(IUser user) {
        ItemBuilder item = ItemBuilder.create(Material.STONE).name("§cInvalid item");
        if (this.getValues().containsKey("id") && this.getValues().containsKey("texture")) {
            item = ItemBuilder.create(SkullChanger.getSkullByTexture(this.getValues().get("id"), this.getValues().get("texture")));
            item = item.name(user.translate(this.getLanguageKey()));
        } else if (this.getValues().containsKey("material")) {
            item = ItemBuilder.create(Material.valueOf(this.getValues().get("material")));
            item = item.name(user.translate(this.getLanguageKey()));
        } else {
            if (this.getLanguageKey() != null) item = item.lore("§7Item-Language-Key: §9"+this.getLanguageKey());
            if (user.translate(this.getLanguageKey()) != null) item = item.lore("§7Item-Language-Message: §9"+user.translate(this.getLanguageKey()));
        }

        item.lore((giveAble ? "§8➥ §aClick to select this Item" : "§8➥ §cThis Item can't be gived or selected."));
        if (this.getCategory() == Category.PETS) {
            item.lore("§7Category: §6Pets");
        } else if (this.getCategory() == Category.MOUNTS) {
            item.lore("§7Category: §eMounts");
        } else if (this.getCategory() == Category.BOOTS) {
            item.lore("§7Category: §5Boots");
        } else if (this.getCategory() == Category.PARTICLE_EFFECTS) {
            item.lore("§7Category: §cParticle Effects");
        }
        item.lore("§7Rarity: "+rarity.getRarityColor()+user.translate(rarity.getKey()));
        item.lore("§7Findable? "+(findAble ? "§aYes" : "§cNo"));
        item.lore("§7Sellable? "+(sellAble ? "§aYes" : "§cNo"));
        item.lore("§0 ");
        item.lore("§6§nItems which are gived by the Team can not be selled!");

        return item.build();
    }
}
