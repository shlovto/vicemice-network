package net.vicemice.lobby.listeners;

import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.player.inventorys.shop.lottery.OpenTicketInventory;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import java.util.Random;

/*
 * Class created at 19:26 - 13.04.2020
 * Copyright (C) elrobtossohn
 */
public class InventoryCloseListener implements Listener {
    
    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        IUser user = UserManager.getUser(event.getPlayer().getUniqueId());
        if (event.getInventory().getTitle().equalsIgnoreCase(user.translate("lottery-ticket-title")) && !OpenTicketInventory.resultOpen.contains(user.getUniqueId())) {
            if (OpenTicketInventory.pickedMap.containsKey(user)) {
                long coins = OpenTicketInventory.pickedMap.get(user).getCoins();
                if (coins >= 100000) {
                    user.playSound(Sound.WITHER_DEATH);
                } else {
                    user.playSound(Sound.LEVEL_UP);
                }

                user.increaseCoins(coins);
                user.sendMessage("receive-coins", coins);
                OpenTicketInventory.canNotContinue.remove(user);
                OpenTicketInventory.pickedMap.remove(user);
                OpenTicketInventory.scheduledExecution.get(user.getUniqueId()).shutdown();
                OpenTicketInventory.scheduledExecution.remove(user.getUniqueId());
            } else if (!OpenTicketInventory.pickedMap.containsKey(user)) {
                user.playSound(Sound.LEVEL_UP);
                long coins = new Random().nextInt(500);
                user.increaseCoins(coins);
                user.sendMessage("receive-coins", coins);
            }
        }
    }
}
