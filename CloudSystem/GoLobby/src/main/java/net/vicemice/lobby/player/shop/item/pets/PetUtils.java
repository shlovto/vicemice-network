package net.vicemice.lobby.player.shop.item.pets;

import gnu.trove.map.TObjectLongMap;
import gnu.trove.map.hash.TObjectLongHashMap;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.minecraft.server.v1_8_R3.*;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.player.shop.item.entity.PathfinderGoalWalkToLoc;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/*
 * Class created at 03:22 - 15.04.2020
 * Copyright (C) elrobtossohn
 */
public class PetUtils implements Listener {
    public static HashMap<IUser, Pet> pets = new HashMap<>();
    public static HashMap<IUser, Entity> entities = new HashMap<>();
    public static HashMap<Integer, IUser> entityIds = new HashMap<>();

    public static boolean spawnPet(IUser user, Pet pet) {
        if (!pets.containsKey(user)) {
            LivingEntity livingEntity = (LivingEntity)user.getLocation().getWorld().spawnEntity(user.getLocation(), pet.getType());
            livingEntity.setCustomName(pet.getName());
            livingEntity.setCustomNameVisible(true);

            EntityInsentient entity = (EntityInsentient) ((CraftEntity) livingEntity).getHandle();

            ((List)getPrivateField("b", PathfinderGoalSelector.class, entity.goalSelector)).clear();
            ((List)getPrivateField("c", PathfinderGoalSelector.class, entity.goalSelector)).clear();
            ((List)getPrivateField("b", PathfinderGoalSelector.class, entity.targetSelector)).clear();
            ((List)getPrivateField("c", PathfinderGoalSelector.class, entity.targetSelector)).clear();
            entity.getAttributeInstance(GenericAttributes.FOLLOW_RANGE).setValue(40.0);

            entity.goalSelector.a(8, new PathfinderGoalWalkToLoc(entity, user.getBukkitPlayer(), 1.0));

            pets.put(user, pet);
            entities.put(user, livingEntity);
            entityIds.put(livingEntity.getEntityId(), user);
            user.getBukkitPlayer().closeInventory();

            return true;
        } else if (pets.containsKey(user)) {
            deletePet(user);
            return spawnPet(user, pet);
        }

        return false;
    }

    public static void deletePet(IUser user) {
        if (pets.containsKey(user)) {
            pets.remove(user);
            entityIds.remove(entities.get(user).getEntityId());
            entities.get(user).remove();
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        if (pets.containsKey(UserManager.getUser(event.getPlayer()))) {
            PetUtils.deletePet(UserManager.getUser(event.getPlayer()));
        }
    }

    @EventHandler
    public void onKick(PlayerKickEvent event) {
        if (pets.containsKey(UserManager.getUser(event.getPlayer()))) {
            PetUtils.deletePet(UserManager.getUser(event.getPlayer()));
        }
    }

    private final TObjectLongMap<UUID> cooldown = new TObjectLongHashMap<UUID>();

    @EventHandler
    public void onPlayerInteractAtEntity(PlayerInteractEntityEvent event) {
        Player player = event.getPlayer();
        Entity entity = event.getRightClicked();
        Pet pet = pets.get(entityIds.get(entity.getEntityId()));
        if (pet != null) {
            if (entity instanceof Villager) {
                event.setCancelled(true);
            }
            if (!this.cooldown.containsKey(player.getUniqueId()) || this.cooldown.get(player.getUniqueId()) + 5000L < System.currentTimeMillis()) {
                entity.getWorld().playEffect(entity instanceof LivingEntity ? ((LivingEntity)entity).getEyeLocation() : entity.getLocation(), Effect.HEART, null);
                player.spigot().sendMessage(new ComponentBuilder("§c❤ §f" + (entity.getCustomName() == null ? entity.getType().toString().toLowerCase() : entity.getCustomName()) + " §c❤").color(ChatColor.RED).event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Owner: " + GoAPI.getUserAPI().getRankData(pet.getOwner()).getRankColor()+Bukkit.getPlayer(pet.getOwner()).getName()).create())).create());
                this.cooldown.put(player.getUniqueId(), System.currentTimeMillis());
            }
        }
    }

    public static Object getPrivateField(String fieldName, Class clazz, Object object) {
        Object o = null;
        try {
            Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
            o = field.get(object);
        }
        catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        return o;
    }
}