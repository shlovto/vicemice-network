package net.vicemice.lobby.listeners;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.lobby.player.inventorys.inventory.DefaultInventory;
import net.vicemice.lobby.player.inventorys.inventory.type.BootsInventory;
import net.vicemice.lobby.player.inventorys.inventory.type.ParticleEffectsInventory;
import net.vicemice.lobby.player.inventorys.inventory.type.PetsInventory;
import net.vicemice.lobby.player.inventorys.navigator.NavigatorInventory;
import net.vicemice.lobby.player.inventorys.profile.friends.FriendsInventory;
import net.vicemice.lobby.player.inventorys.profile.replays.ReplayInventory;
import net.vicemice.lobby.player.inventorys.profile.settings.SettingsInventory;
import net.vicemice.lobby.player.inventorys.shop.chests.ChestsInventory;
import net.vicemice.lobby.player.inventorys.shop.lottery.LotteryInventory;
import net.vicemice.lobby.player.inventorys.switcher.LobbySwitcherInventory;
import net.vicemice.lobby.player.utils.InventoryType;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.ConnectPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.Rank;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteractListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        IUser user = UserManager.getUser(event.getPlayer());
        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(user);

        if (event.getAction() == Action.RIGHT_CLICK_BLOCK && GoLobby.getPlayerManager().getBuilders().contains(user.getUniqueId())) {
            if (user.getRank().isHigherEqualsLevel(Rank.ADMIN) && user.getBukkitPlayer().isSneaking() && event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                Location location = event.getClickedBlock().getLocation();
                user.sendRawMessage("X:" + location.getBlockX() + " Y:" + location.getBlockY() + " Z:" + location.getBlockZ());
                return;
            }
        }

        if ((event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.LEFT_CLICK_BLOCK) && event.getClickedBlock().getType() == Material.WALL_SIGN) {
            Location location = event.getClickedBlock().getLocation();
            if (GoLobby.getPlayerManager().getServerSigns().containsKey(location)) {
                String serverName = GoLobby.getPlayerManager().getServerSigns().get(location);
                user.connectServer(serverName);
                return;
            }
            //SignManager.handleRightClickSign(player, location);
        }

        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (event.getClickedBlock().getType() == Material.CHEST && event.getClickedBlock().getLocation().distance(new Location(Bukkit.getWorld("world"), 4.5, 14.0, 42.5)) <= 1) {
                event.setCancelled(true);
                new ChestsInventory().create(user, 1).open();
                return;
            }
            if (event.getClickedBlock().getType() == Material.ENDER_CHEST && event.getClickedBlock().getLocation().distance(new Location(Bukkit.getWorld("world"), 0.5, 14.0, 42.5)) <= 1) {
                event.setCancelled(true);
                new LotteryInventory().create(user, 1).open();
                return;
            }
        }

        if (event.getAction() == Action.PHYSICAL) event.setCancelled(true);
        if (GoLobby.getPlayerManager().getBuilders().contains(user.getUniqueId())) return;
        if (event.getItem() == null) return;
        if (event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) return;

        if (user.getBukkitPlayer().getInventory().getHeldItemSlot() == 0)
            new NavigatorInventory().create(user).open();
        if (user.getBukkitPlayer().getInventory().getHeldItemSlot() == 1)
            new LobbySwitcherInventory().create(user, 1).open();
        if (user.getBukkitPlayer().getInventory().getHeldItemSlot() == 3) {
            if (playerData.getNetworkPlayer().getSettingsPacket().getSetting("auto-nick", Integer.class) == 1) {
                playerData.getNetworkPlayer().getSettingsPacket().setSetting("auto-nick", 0);
                user.getBukkitPlayer().getInventory().setItem(3, ItemBuilder.create(Material.NAME_TAG).name("§c"+user.translate("items-nick")).build());
            } else {
                playerData.getNetworkPlayer().getSettingsPacket().setSetting("auto-nick", 1);
                user.getBukkitPlayer().getInventory().setItem(3, ItemBuilder.create(Material.NAME_TAG).name("§a"+user.translate("items-nick")).build());
            }
            playerData.setNickChanged(true);
            playerData.sendSettings();
        }
        if (user.getBukkitPlayer().getInventory().getHeldItemSlot() == 4) {
            if (playerData.getLastInventoryType() == InventoryType.PETS) {
                new PetsInventory().create(user, 1).open();
            } else if (playerData.getLastInventoryType() == InventoryType.BOOTS) {
                new BootsInventory().create(user, 1).open();
            } else if (playerData.getLastInventoryType() == InventoryType.PARTICLE_EFFECTS) {
                new ParticleEffectsInventory().create(user, 1).open();
            } else {
                new DefaultInventory().categorys(user).open();
            }
        }
        if (user.getBukkitPlayer().getInventory().getHeldItemSlot() == 7) {
            if (playerData.getLastProfileType() == InventoryType.FRIENDS) {
                new FriendsInventory().create(user, 1).open();
            } else if (playerData.getLastProfileType() == InventoryType.SETTINGS) {
                new SettingsInventory().create(user).open();
            } else if (playerData.getLastProfileType() == InventoryType.REPLAYS_SAVED
                    || playerData.getLastProfileType() == InventoryType.REPLAYS_RECENT) {
                new ReplayInventory().create(user, 1, (playerData.getLastProfileType() == InventoryType.REPLAYS_RECENT)).open();
            }
        }
        if (user.getBukkitPlayer().getInventory().getHeldItemSlot() == 8) {
            if (playerData.getNetworkPlayer().getSettingsPacket().getSetting("player-visibility", Integer.class) == 0) {
                playerData.getNetworkPlayer().getSettingsPacket().setSetting("player-visibility", 1);
            } else if (playerData.getNetworkPlayer().getSettingsPacket().getSetting("player-visibility", Integer.class) == 1) {
                playerData.getNetworkPlayer().getSettingsPacket().setSetting("player-visibility", 2);
            } else if (playerData.getNetworkPlayer().getSettingsPacket().getSetting("player-visibility", Integer.class) == 2) {
                playerData.getNetworkPlayer().getSettingsPacket().setSetting("player-visibility", 0);
            }
            playerData.setSettingsChanged(true);
            playerData.sendSettings();
            playerData.updatePlayers();
        }
    }
}