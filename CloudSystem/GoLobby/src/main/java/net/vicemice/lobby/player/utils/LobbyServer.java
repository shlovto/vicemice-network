package net.vicemice.lobby.player.utils;

import lombok.Data;

@Data
public class LobbyServer {
    public LobbyServer() {}

    private String name, players = "0";
    private boolean own;
    private Type type;

    public enum Type {
        PLAYERS,
        PREMIUM,
        STAFF,
        SILENT;
    }
}