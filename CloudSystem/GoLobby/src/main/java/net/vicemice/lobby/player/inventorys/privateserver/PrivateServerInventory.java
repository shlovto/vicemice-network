package net.vicemice.lobby.player.inventorys.privateserver;

import lombok.Getter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.server.StartPrivateServerPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.players.api.internal.SkullChanger;
import net.vicemice.hector.utils.players.utils.Pagination;
import net.vicemice.lobby.player.inventorys.profile.friends.FriendInventory;
import org.bukkit.Material;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/*
 * Class created at 23:24 - 04.05.2020
 * Copyright (C) elrobtossohn
 */
public class PrivateServerInventory {

    public GUI create(IUser user, int page) {
        GUI gui = user.createGUI(user.translate("private-server"), 6);

        gui.fill(0, 10, user.getUIColor());
        gui.setItem(17, user.getUIColor());
        gui.setItem(18, user.getUIColor());
        gui.setItem(26, user.getUIColor());
        gui.setItem(27, user.getUIColor());
        gui.setItem(35, user.getUIColor());
        gui.setItem(36, user.getUIColor());
        gui.fill(44, 54, user.getUIColor());

        Pagination<PrivateGameInfo> pagination = new Pagination<>(getGames(user), 28) ;

        if (user.getRank().isLowerLevel(Rank.VIP))
            gui.setItem(4, ItemBuilder.create(Material.SIGN).name("§b"+user.translate("private-server-tokens")).lore(new ArrayList<>(Arrays.asList(user.translate((user.getStatistic("private_server_tokens", StatisticPeriod.GLOBAL) == 1 ? "private-server-token-description" : "private-server-tokens-description"), user.getStatistic("private_server_tokens", StatisticPeriod.GLOBAL)).split("\n")))).build());

        if (pagination.getElementsFor(page).isEmpty())
            gui.setItem(22, ItemBuilder.create(Material.BARRIER).name(user.translate("private-server-no-game-available")).lore(new ArrayList<>(Arrays.asList(user.translate("private-server-no-game-available-description").split("\n")))).build());

        AtomicInteger i = new AtomicInteger(10);
        if (!pagination.getElementsFor(page).isEmpty())
            pagination.printPage(page, privateGameInfo -> {
                if (i.get() == 17) i.set(19);
                if (i.get() == 25) i.set(28);
                if (i.get() == 34) i.set(37);

                gui.setItem(i.get(), new ItemBuilder(privateGameInfo.getMaterial()).name(privateGameInfo.getName()).lore(privateGameInfo.getDescription()).build(), e -> GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.START_PRIVATE_SERVER, new StartPrivateServerPacket(user.getUniqueId(), privateGameInfo.getTemplate())), API.PacketReceiver.SLAVE));
                i.set(i.get() + 1);
            });

        if (pagination.getPages() > 1) {
            gui.setItem(48, ItemBuilder.create(GUI.Items.getLeft()).name(user.translate("previous-page")).build(), e -> {
                if (pagination.getElementsFor((page-1)).isEmpty()) return;
                this.create(user, (page-1)).open();
            });
            gui.setItem(50, ItemBuilder.create(GUI.Items.getRight()).name(user.translate("next-page")).build(), e -> {
                if (pagination.getElementsFor((page+1)).isEmpty()) return;
                this.create(user, (page+1)).open();
            });
        }

        return gui;
    }

    private List<PrivateGameInfo> getGames(IUser user) {
        List<PrivateGameInfo> list = new ArrayList<>();

        list.add(new PrivateGameInfo(user, "pbuilding", Material.GRASS, "ps-building", false));

        return list;
    }

    @Getter
    public class PrivateGameInfo {
        private final Material material;
        private final String name, template;
        private final List<String> description;
        private final boolean mapAble;

        public PrivateGameInfo(IUser user, String template, Material material, String name, boolean mapAble) {
            this.template = template;
            this.material = material;
            this.name = user.translate(name);
            this.description = new ArrayList<>(Arrays.asList(user.translate(name+"-description").split("\n")));
            this.mapAble = mapAble;
        }
    }
}