package net.vicemice.lobby.player.inventorys.shop;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.inventorys.shop.chests.ChestsInventory;
import net.vicemice.lobby.player.inventorys.shop.lottery.LotteryInventory;
import net.vicemice.lobby.player.utils.InventoryType;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/*
 * Class created at 21:17 - 01.04.2020
 * Copyright (C) elrobtossohn
 */
public class DefaultShop {

    public GUI create(IUser user, InventoryType inventoryGUI) {
        GUI gui = user.createGUI("§7Loading...", 6);
        if (inventoryGUI == InventoryType.LOTTERY)
            gui = user.createGUI(user.translate("shop-lottery-title"), 6);
        else if (inventoryGUI == InventoryType.DAILY_REWARD)
            gui = user.createGUI(user.translate("shop-daily-reward-title"), 6);
        else if (inventoryGUI == InventoryType.CHESTS)
            gui = user.createGUI(user.translate("shop-chests-title"), 6);

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        if (inventoryGUI == InventoryType.LOTTERY) {
            gui.setItem(36, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability(5).name("§0 ").build());
            GoLobby.getPlayerManager().getPlayer(user).setLastShopType(InventoryType.LOTTERY);
        //} else if (inventoryGUI == InventoryType.DAILY_REWARD) {
        //    gui.setItem(37, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability(5).name("§0 ").build());
        //    GoLobby.getPlayerManager().getPlayer(player).setLastShopType(InventoryType.DAILY_REWARD);
        } else if (inventoryGUI == InventoryType.CHESTS) {
            gui.setItem(37, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability(5).name("§0 ").build());
            GoLobby.getPlayerManager().getPlayer(user).setLastShopType(InventoryType.CHESTS);
        }

        gui.setItem(45, ItemBuilder.create(Material.PAPER).name(user.translate("shop-lottery")).build(), e -> {
            new LotteryInventory().create(user, 1).open();
        });
        //gui.setItem(46, ItemBuilder.create(SkullChanger.getSkull(player)).name(user.translate("shop-daily-reward")).build(), e -> {

        //});
        gui.setItem(46, ItemBuilder.create(Material.CHEST).name(user.translate("shop-chests")).build(), e -> {
            new ChestsInventory().create(user, 1).open();
        });

        return gui;
    }

    public GUI create(IUser user, String name, int rows) {
        GUI gui = user.createGUI(name, rows);

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        return gui;
    }
}
