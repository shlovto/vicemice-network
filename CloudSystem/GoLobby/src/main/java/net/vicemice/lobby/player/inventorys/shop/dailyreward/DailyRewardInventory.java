package net.vicemice.lobby.player.inventorys.shop.dailyreward;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.scheduler.ScheduledExecution;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/*
 * Class created at 00:47 - 17.04.2020
 * Copyright (C) elrobtossohn
 */
public class DailyRewardInventory {

    public GUI create(IUser user) {
        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(user);
        GUI gui = user.createGUI(ChatColor.stripColor(user.translate("shop-daily-rewards")), 5);
        ScheduledExecution scheduledExecution = new ScheduledExecution(() -> {
            if (gui.isOpen()) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = null;

                try {
                    date = sdf.parse((GoAPI.getTimeManager().getTime("yyyy/MM/dd", System.currentTimeMillis()) + " 00:00:00"));
                } catch (Exception ignored) {
                }

                long millis = GoAPI.getTimeManager().addDays(date, 1).getTime();

                if (user.getStatistic("member_coin_reward", StatisticPeriod.DAY) >= 1) {
                    gui.setItem(19, ItemBuilder.create(Material.SULPHUR).name(user.translate("daily-rewards-coin-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("daily-rewards-reward-claimed", GoAPI.getTimeManager().getRemainingTimeString(GoServer.getLocaleManager(), playerData.getLocale(), millis)).split("\n")))).build(), e -> {
                        user.playSound(Sound.ANVIL_BREAK);
                    });
                }
                if (user.getStatistic("member_lottery_reward", StatisticPeriod.DAY) >= 1) {
                    gui.setItem(20, ItemBuilder.create(Material.EMPTY_MAP).name(user.translate("daily-rewards-lottery-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("daily-rewards-reward-claimed", GoAPI.getTimeManager().getRemainingTimeString(GoServer.getLocaleManager(), playerData.getLocale(), millis)).split("\n")))).build(), e -> {
                        user.playSound(Sound.ANVIL_BREAK);
                    });
                }
                if (user.getStatistic("daily_chest", StatisticPeriod.DAY) >= 1) {
                    gui.setItem(22, ItemBuilder.create(Material.MINECART).name(user.translate("daily-rewards-daily-chest-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("daily-rewards-reward-claimed", GoAPI.getTimeManager().getRemainingTimeString(GoServer.getLocaleManager(), playerData.getLocale(), millis)).split("\n")))).build(), e -> {
                        user.playSound(Sound.ANVIL_BREAK);
                    });
                }
                if (playerData.getNetworkPlayer().getRank().isHigherEqualsLevel(Rank.PREMIUM)) {
                    if (user.getStatistic("premium_lottery_reward", StatisticPeriod.DAY) == 1) {
                        gui.setItem(24, ItemBuilder.create(Material.EMPTY_MAP).name(user.translate("daily-rewards-premium-lottery-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("daily-rewards-reward-claimed", GoAPI.getTimeManager().getRemainingTimeString(GoServer.getLocaleManager(), playerData.getLocale(), millis)).split("\n")))).build(), e -> {
                            user.playSound(Sound.ANVIL_BREAK);
                        });
                    }
                    if (user.getStatistic("premium_coin_reward", StatisticPeriod.DAY) == 1) {
                        gui.setItem(25, ItemBuilder.create(Material.SULPHUR).name(user.translate("daily-rewards-premium-coin-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("daily-rewards-reward-claimed", GoAPI.getTimeManager().getRemainingTimeString(GoServer.getLocaleManager(), playerData.getLocale(), millis)).split("\n")))).build(), e -> {
                            user.playSound(Sound.ANVIL_BREAK);
                        });
                    }
                }
            }
        }, 0, 1, TimeUnit.SECONDS);
        gui.addCloseListener(target -> scheduledExecution.shutdown());

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = null;

        try {
            date = sdf.parse((GoAPI.getTimeManager().getTime("yyyy/MM/dd", System.currentTimeMillis()) + " 00:00:00"));
        } catch (Exception ignored) {
        }

        long millis = GoAPI.getTimeManager().addDays(date, 1).getTime();

        if (user.getStatistic("member_coin_reward", StatisticPeriod.DAY) >= 1) {
            gui.setItem(19, ItemBuilder.create(Material.SULPHUR).name(user.translate("daily-rewards-coin-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("daily-rewards-reward-claimed", GoAPI.getTimeManager().getRemainingTimeString(GoServer.getLocaleManager(), playerData.getLocale(), millis)).split("\n")))).build(), e -> {
                user.playSound(Sound.ANVIL_BREAK);
            });
        } else {
            gui.setItem(19, ItemBuilder.create(Material.GLOWSTONE_DUST).name(user.translate("daily-rewards-coin-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("daily-rewards-coin-reward-claimable", GoAPI.formatDecimal(playerData.getLocale(), 1000)).split("\n")))).build(), e -> {
                user.playSound(Sound.LEVEL_UP);
                user.increaseCoins(1000);
                user.sendMessage("receive-coins", 1000);
                user.increaseStatistic("member_coin_reward", 1);
                GoServer.getService().getPlayerManager().getPlayer(user.getUniqueId()).getStatistics().get(StatisticPeriod.DAY).put("member_coin_reward", 1L);
                this.create(user).open();
            });
        }
        if (user.getStatistic("member_lottery_reward", StatisticPeriod.DAY) >= 1) {
            gui.setItem(20, ItemBuilder.create(Material.EMPTY_MAP).name(user.translate("daily-rewards-lottery-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("daily-rewards-reward-claimed", GoAPI.getTimeManager().getRemainingTimeString(GoServer.getLocaleManager(), playerData.getLocale(), millis)).split("\n")))).build(), e -> {
                user.playSound(Sound.ANVIL_BREAK);
            });
        } else {
            gui.setItem(20, ItemBuilder.create(Material.PAPER).name(user.translate("daily-rewards-lottery-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("daily-rewards-premium-lottery-reward-claimable").split("\n")))).build(), e -> {
                user.playSound(Sound.LEVEL_UP);
                user.increaseStatistic("lobby_lottery_tickets", 1);
                user.increaseStatistic("member_lottery_reward", 1);
                GoServer.getService().getPlayerManager().getPlayer(user.getUniqueId()).getStatistics().get(StatisticPeriod.DAY).put("member_lottery_reward", 1L);
                this.create(user).open();
            });
        }

        if (user.getStatistic( "daily_chest", StatisticPeriod.DAY) >= 1) {
            gui.setItem(22, ItemBuilder.create(Material.MINECART).name(user.translate("daily-rewards-daily-chest-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("daily-rewards-reward-claimed", GoAPI.getTimeManager().getRemainingTimeString(GoServer.getLocaleManager(), playerData.getLocale(), millis)).split("\n")))).build(), e -> {
                user.playSound(Sound.ANVIL_BREAK);
            });
        } else {
            gui.setItem(22, ItemBuilder.create(Material.STORAGE_MINECART).name(user.translate("daily-rewards-daily-chest-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("daily-rewards-daily-chest-reward-claimable").split("\n")))).build(), e -> {
                user.playSound(Sound.LEVEL_UP);
                user.increaseStatistic("lobby_chests", 1);
                user.increaseStatistic("daily_chest", 1);
                this.create(user).open();
            });
        }

        if (playerData.getNetworkPlayer().getRank().isHigherEqualsLevel(Rank.PREMIUM)) {
            if (user.getStatistic("premium_lottery_reward", StatisticPeriod.DAY) == 1) {
                gui.setItem(24, ItemBuilder.create(Material.EMPTY_MAP).name(user.translate("daily-rewards-premium-lottery-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("daily-rewards-reward-claimed", GoAPI.getTimeManager().getRemainingTimeString(GoServer.getLocaleManager(), playerData.getLocale(), millis)).split("\n")))).build(), e -> {
                    user.playSound(Sound.ANVIL_BREAK);
                });
            } else {
                gui.setItem(24, ItemBuilder.create(Material.PAPER).name(user.translate("daily-rewards-premium-lottery-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("daily-rewards-premium-lottery-reward-claimable").split("\n")))).build(), e -> {
                    user.playSound(Sound.LEVEL_UP);
                    user.increaseStatistic("lobby_lottery_tickets", 2);
                    user.increaseStatistic("premium_lottery_reward", 1);
                    GoServer.getService().getPlayerManager().getPlayer(user.getUniqueId()).getStatistics().get(StatisticPeriod.DAY).put("premium_lottery_reward", 1L);
                    this.create(user).open();
                });
            }
            if (user.getStatistic("premium_coin_reward", StatisticPeriod.DAY) == 1) {
                gui.setItem(25, ItemBuilder.create(Material.SULPHUR).name(user.translate("daily-rewards-premium-coin-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("daily-rewards-reward-claimed", GoAPI.getTimeManager().getRemainingTimeString(GoServer.getLocaleManager(), playerData.getLocale(), millis)).split("\n")))).build(), e -> {
                    user.playSound(Sound.ANVIL_BREAK);
                });
            } else {
                gui.setItem(25, ItemBuilder.create(Material.GLOWSTONE_DUST).name(user.translate("daily-rewards-premium-coin-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("daily-rewards-coin-reward-claimable", GoAPI.formatDecimal(playerData.getLocale(), 2000)).split("\n")))).build(), e -> {
                    user.playSound(Sound.LEVEL_UP);
                    user.increaseCoins(2000);
                    user.sendMessage("receive-coins", 2000);
                    user.increaseStatistic("premium_coin_reward", 1);
                    GoServer.getService().getPlayerManager().getPlayer(user.getUniqueId()).getStatistics().get(StatisticPeriod.DAY).put("premium_coin_reward", 1L);
                    this.create(user).open();
                });
            }
        } else {
            gui.setItem(24, ItemBuilder.create(Material.PAPER).name(user.translate("daily-rewards-premium-lottery-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("daily-rewards-reward-locked").split("\n")))).build(), e -> {
                user.playSound(Sound.ANVIL_BREAK);
            });
            gui.setItem(25, ItemBuilder.create(Material.GLOWSTONE_DUST).name(user.translate("daily-rewards-premium-coin-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("daily-rewards-reward-locked").split("\n")))).build(), e -> {
                user.playSound(Sound.ANVIL_BREAK);
            });
        }

        return gui;
    }
}