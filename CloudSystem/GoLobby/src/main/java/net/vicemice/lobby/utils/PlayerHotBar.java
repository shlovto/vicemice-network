package net.vicemice.lobby.utils;

import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.hector.utils.players.api.internal.SkullChanger;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.players.NetworkPlayer;
import org.bukkit.Material;

public class PlayerHotBar {

    public static void setItems(IUser user) {
        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(user);
        user.getBukkitPlayer().getInventory().clear();
        user.getBukkitPlayer().getInventory().setArmorContents(null);

        user.getBukkitPlayer().getInventory().setItem(0, ItemBuilder.create(Material.COMPASS).name(user.translate("items-compass")).build());
        user.getBukkitPlayer().getInventory().setItem(1, ItemBuilder.create(Material.WATCH).name(user.translate("items-switcher")).build());

        if (user.getRank().isHigherEqualsLevel(Rank.VIP))
            if (playerData.getNetworkPlayer().getSettingsPacket().getSetting("auto-nick", Integer.class) == 1)
                user.getBukkitPlayer().getInventory().setItem(3, ItemBuilder.create(Material.NAME_TAG).name("§a"+user.translate("items-nick")).build());
            else
                user.getBukkitPlayer().getInventory().setItem(3, ItemBuilder.create(Material.NAME_TAG).name("§c"+user.translate("items-nick")).build());
        user.getBukkitPlayer().getInventory().setItem(4, ItemBuilder.create(Material.CHEST).name("§c"+user.translate("items-your-inventory")).build());
        user.getBukkitPlayer().getInventory().setItem(7, ItemBuilder.create(SkullChanger.getSkull(user.getBukkitPlayer())).name(user.translate("items-profile")).build());

        setVisibilityItem(user);
    }

    public static void setVisibilityItem(IUser user) {
        NetworkPlayer networkPlayer = user.getNetworkPlayer();
        if (networkPlayer.getSettingsPacket().getSetting("player-visibility", Integer.class) == 0) {
            user.getBukkitPlayer().getInventory().setItem(8, ItemBuilder.create(Material.INK_SACK).durability(10).name(user.translate("items-visibility-all")).build());
        } else if (networkPlayer.getSettingsPacket().getSetting("player-visibility", Integer.class) == 1) {
            user.getBukkitPlayer().getInventory().setItem(8, ItemBuilder.create(Material.INK_SACK).durability(5).name(user.translate("items-visibility-vip")).build());
        } else if (networkPlayer.getSettingsPacket().getSetting("player-visibility", Integer.class) == 2) {
            user.getBukkitPlayer().getInventory().setItem(8, ItemBuilder.create(Material.INK_SACK).durability(8).name(user.translate("items-visibility-none")).build());
        }
    }
}