package net.vicemice.lobby.player.inventorys.navigator;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.inventorys.navigator.games.BWInventory;
import net.vicemice.lobby.player.inventorys.navigator.games.QSGInventory;
import net.vicemice.lobby.player.inventorys.profile.DefaultProfile;
import net.vicemice.lobby.utils.Locations;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;

public class NavigatorInventory {

    public GUI create(IUser user) {
        GUI gui = new DefaultProfile().create(user, user.translate("navigator-title"), 5);

        gui.setItem(20, ItemBuilder.create(Material.BED).name(user.translate("navigator-bedwars")).lore(new ArrayList<>(Arrays.asList(user.translate("navigator-bedwars-description", (GoLobby.getPlayerManager().getGamePlayers("bedwars") == 1 ? "1 "+user.translate("player") : GoLobby.getPlayerManager().getGamePlayers("bedwars")+" "+user.translate("players"))).split("\n")))).build(), e -> {
            if (e.getAction() == InventoryAction.PICKUP_ALL) {
                e.getView().close();
                user.teleport(Locations.getBedWarsLocation());
            } else if (e.getAction() == InventoryAction.PICKUP_HALF) {
                new BWInventory().create(user).open();
            }
        });

        gui.setItem(22, ItemBuilder.create(Material.MAGMA_CREAM).name(user.translate("navigator-spawn")).lore(new ArrayList<>(Arrays.asList(user.translate("navigator-spawn-description", "0").split("\n")))).build(), e -> {
            if (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-sounds", Integer.class) == 1)
                user.playSound(Sound.ENDERMAN_TELEPORT);
            user.teleport(Locations.getSpawnLocation());
        });

        gui.setItem(24, ItemBuilder.create(Material.FISHING_ROD).name(user.translate("navigator-qsg")).lore(new ArrayList<>(Arrays.asList(user.translate("navigator-qsg-description", (GoLobby.getPlayerManager().getGamePlayers("qsg") == 1 ? "1 "+user.translate("player") : GoLobby.getPlayerManager().getGamePlayers("qsg")+" "+user.translate("players"))).split("\n")))).build(), e -> {
            if (e.getAction() == InventoryAction.PICKUP_ALL) {
                e.getView().close();
                user.teleport(Locations.getQsgLocation());
            } else if (e.getAction() == InventoryAction.PICKUP_HALF) {
                new QSGInventory().create(user).open();
            }
        });

        return gui;
    }
}