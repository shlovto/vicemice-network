package net.vicemice.lobby.listeners;

import net.vicemice.hector.events.PlayerLoginEvent;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.utils.PlayerHotBar;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PlayerLoginListener implements Listener {

    @EventHandler
    public void onLogin(PlayerLoginEvent event) {
        if (GoLobby.getPlayerManager().getPlayer(event.getUniqueId()) != null) {
            PlayerHotBar.setItems(UserManager.getUser(event.getUniqueId()));
            GoLobby.getPlayerManager().getPlayer(event.getUniqueId()).updateBoard();
        } else {
            GoLobby.getPlayerManager().addBefore(event.getUniqueId());
        }
    }
}