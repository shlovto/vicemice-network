package net.vicemice.lobby.listeners;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.events.PlayerUpdateDataEvent;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.user.RequestUserDataPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.lobby.player.inventorys.shop.chests.ChestsInventory;
import net.vicemice.lobby.player.inventorys.shop.lottery.LotteryInventory;
import net.vicemice.lobby.utils.Locations;
import net.vicemice.lobby.utils.PlayerHotBar;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.players.utils.DataUpdateType;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PlayerUpdateDataListener implements Listener {

    @EventHandler
    public void onUpdate(PlayerUpdateDataEvent event) {
        if (Bukkit.getPlayer(event.getUniqueId()) == null) return;
        IUser user = UserManager.getUser(event.getUniqueId());

        if (!GoLobby.getPlayerManager().getPlayers().containsKey(event.getUniqueId()) && user.isOnline()) {
            PlayerHotBar.setItems(user);
            GoLobby.getPlayerManager().getPlayer(event.getUniqueId()).updateBoard();

            GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.REQUEST_USER_DATA, new RequestUserDataPacket(user.getUniqueId(), GoServer.getService().getServerName())), API.PacketReceiver.SLAVE);

            GoLobby.getPlayerManager().addPlayer(user);
            if (GoLobby.getPlayerManager().getBeforePlayers().contains(user.getUniqueId())) {
                PlayerHotBar.setItems(user);
                GoLobby.getPlayerManager().getPlayer(user.getUniqueId()).updateBoard();
            }
            GoLobby.getPlayerManager().getPlayer(user.getUniqueId()).updatePlayers();
            GoLobby.getPlayerManager().getPlayers().forEach((k,v) -> v.updatePlayers());

            if (GoLobby.getPlayerManager().getPlayerLocations().containsKey(user.getUniqueId())) {
                user.teleport(GoLobby.getPlayerManager().getPlayerLocations().get(user.getUniqueId()));
            } else {
                user.teleport(Locations.getSpawnLocation());
            }

            user.getBukkitPlayer().setGameMode(GameMode.ADVENTURE);

            if (GoLobby.getPlayerManager().getPlayer(user.getUniqueId()).getNetworkPlayer().getRank().isHigherEqualsLevel(Rank.VIP) && GoLobby.getPlayerManager().getPlayer(user.getUniqueId()).getNetworkPlayer().getSettingsPacket().getSetting("player-visibility", Integer.class) == 2) {
                user.getBukkitPlayer().setAllowFlight(true);
                user.getBukkitPlayer().setFlying(true);
            } else if (GoLobby.getPlayerManager().getPlayer(user.getUniqueId()).getNetworkPlayer().getRank().isHigherEqualsLevel(Rank.PREMIUM) && GoLobby.getPlayerManager().getPlayer(user.getUniqueId()).getNetworkPlayer().getSettingsPacket().getSetting("player-visibility", Integer.class) == 1) {
                user.getBukkitPlayer().setAllowFlight(true);
            } else {
                user.getBukkitPlayer().setAllowFlight(false);
                user.getBukkitPlayer().setFlying(false);
            }
        }

        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(event.getUniqueId());
        if (!playerData.getLocale().toLanguageTag().equalsIgnoreCase(playerData.getNetworkPlayer().getLocale().toLanguageTag())) {
            PlayerHotBar.setItems(UserManager.getUser(event.getUniqueId()));
            return;
        }

        if (ChestsInventory.reopen.contains(user)) {
            new ChestsInventory().create(user, 1).open();
            ChestsInventory.reopen.remove(user);
        }
        if (LotteryInventory.reopen.contains(user)) {
            new LotteryInventory().create(user, 1).open();
            LotteryInventory.reopen.remove(user);
        }

        if (event.getDataUpdateType() == DataUpdateType.UPDATE_COINS || event.getDataUpdateType() == DataUpdateType.UPDATE_PLAYTIME || event.getDataUpdateType() == DataUpdateType.UPDATE_FRIENDS || event.getDataUpdateType() == DataUpdateType.UPDATE_LOCALE) {
            playerData.updateBoard();
        }

        if (event.getDataUpdateType() == DataUpdateType.UPDATE_LOCALE) {
            playerData.updateHolos();
            playerData.setScoreboardCreated(false);
            PlayerHotBar.setItems(user);
        }

        if (event.getDataUpdateType() == DataUpdateType.UPDATE_NICK) {
            playerData.setNickChanged(false);
        }

        if (event.getDataUpdateType() == DataUpdateType.UPDATE_SETTINGS) {
            if (playerData.isSettingsChanged())
                PlayerHotBar.setVisibilityItem(user);
            playerData.setSettingsChanged(false);
        }

        if (!user.getRank().isHigherEqualsLevel(Rank.PREMIUM) && user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 1 ||
                !user.getRank().isHigherEqualsLevel(Rank.VIP) && user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 2) {
            if (playerData.getBoots() == null || playerData.getBoots() != null && playerData.getBoots().getActionId() != 95215646) {
                playerData.getUser().getBukkitPlayer().setAllowFlight(false);
                playerData.getUser().getBukkitPlayer().setFlying(false);
            }
            playerData.getNetworkPlayer().getSettingsPacket().setSetting("lobby-movement", 0);
            playerData.sendSettings();
        }

        if (user.getRank().isHigherEqualsLevel(Rank.PREMIUM) && playerData.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 1 ||
                user.getRank().isHigherEqualsLevel(Rank.VIP) && playerData.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 2) {
            playerData.getUser().getBukkitPlayer().setAllowFlight(true);
        }
    }
}
