package net.vicemice.lobby.player;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.inventorys.switcher.LobbySwitcherInventory;
import net.vicemice.lobby.player.shop.ShopManager;
import net.vicemice.lobby.player.utils.LobbyServer;
import net.vicemice.hector.packets.types.lobby.type.LobbyDataInfo;
import net.vicemice.hector.packets.types.lobby.LobbyPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.api.cuboid.Cuboid;
import net.vicemice.hector.utils.gameserver.ServerData;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class PlayerManager {

    @Getter
    private final ShopManager shopManager;
    @Getter
    private final ConcurrentHashMap<UUID, PlayerData> players;
    @Getter
    private final ConcurrentHashMap<Location, String> serverSigns;
    @Getter
    private final ConcurrentHashMap<UUID, Location> playerLocations;
    @Getter
    private final ConcurrentHashMap<String, LobbyServer> lobbyServerList;
    @Getter
    private final ConcurrentHashMap<IUser, Integer> lobbySwitcherOpened;
    @Getter
    @Setter
    private ConcurrentHashMap<String, Integer> games;
    @Getter
    @Setter
    private List<ServerData> serverData;
    @Getter
    private final List<UUID> builders;
    @Getter
    private final List<UUID> beforePlayers;
    @Getter
    private final Cuboid qsgRegion, bwRegion;

    public PlayerManager() {
        this.shopManager = new ShopManager();
        this.shopManager.init();
        this.players = new ConcurrentHashMap<>();
        this.serverSigns = new ConcurrentHashMap<>();
        this.playerLocations = new ConcurrentHashMap<>();
        this.lobbyServerList = new ConcurrentHashMap<>();
        this.lobbySwitcherOpened = new ConcurrentHashMap<>();
        this.games = new ConcurrentHashMap<>();
        this.builders = new ArrayList<>();
        this.beforePlayers = new ArrayList<>();
        this.qsgRegion = new Cuboid(new Location(Bukkit.getWorld("world"), 47.5, 0.0, -6.5), new Location(Bukkit.getWorld("world"), 35.5, 255.0, 7.5));
        this.bwRegion = new Cuboid(new Location(Bukkit.getWorld("world"), 47.5, 0.0, -6.5), new Location(Bukkit.getWorld("world"), 35.5, 255.0, 7.5));
    }

    public PlayerData getPlayer(IUser user) {
        return this.players.get(user.getUniqueId());
    }

    public PlayerData getPlayer(UUID uniqueId) {
        return this.players.get(uniqueId);
    }

    public void addPlayer(IUser player) {
        this.players.put(player.getUniqueId(), new PlayerData(player));
    }

    public void addBefore(UUID uniqueId) {
        this.beforePlayers.add(uniqueId);
    }

    public void addSwitcherPlayer(IUser user, Integer page) {
        this.lobbySwitcherOpened.put(user, page);
    }

    public void removeSwitcherPlayer(IUser user) {
        this.lobbySwitcherOpened.remove(user);
    }

    public void receiveSwitcherPacket(LobbyPacket lobbyPacket) {
        this.getLobbyServerList().clear();

        for (LobbyDataInfo data : lobbyPacket.getLobbies()) {
            LobbyServer lobbyServer = new LobbyServer();
            lobbyServer.setName(data.getName());
            lobbyServer.setOwn(data.getName().equalsIgnoreCase(GoServer.getService().getServerName().split(":")[2]));
            lobbyServer.setType(LobbyServer.Type.PLAYERS);
            lobbyServer.setPlayers(String.valueOf(data.getPlayers()));
            lobbyServerList.remove(data.getName());
            lobbyServerList.put(data.getName(), lobbyServer);

            Bukkit.getScheduler().runTaskAsynchronously(GoLobby.getInstance(), () -> getLobbySwitcherOpened().forEach((k, v) -> {
                new LobbySwitcherInventory().create(k,v).open();
            }));
        }
    }

    public int getGamePlayers(String game) {
        if (!this.getGames().containsKey(game)) return 0;
        return this.getGames().get(game);
    }
}