package net.vicemice.lobby.player.shop.utils;

import org.bukkit.ChatColor;

/*
 * Enum created at 09:54 - 02.04.2020
 * Copyright (C) elrobtossohn
 */
public enum Rarity {
    COMMON("common", 70, ChatColor.WHITE, 0),
    UNCOMMON("uncommon", 70, ChatColor.GREEN, 5),
    RARE("rare", 15, ChatColor.DARK_PURPLE, 10),
    EPIC("epic", 10, ChatColor.RED, 14),
    LEGENDARY("legendary", 5, ChatColor.GOLD, 1),
    EXCLUSIVE("exclusive", -1, ChatColor.YELLOW, 4);

    private final String key;
    private final int chance, data;
    private final ChatColor rarityColor;

    private Rarity(String key, int chance, ChatColor rarityColor, int data) {
        this.key = key;
        this.chance = chance;
        this.rarityColor = rarityColor;
        this.data = data;
    }

    public String getKey() {
        return key;
    }

    public int getChance() {
        return this.chance;
    }

    public ChatColor getRarityColor() {
        return this.rarityColor;
    }

    public int getData() {
        return data;
    }
}
