package net.vicemice.lobby.player.shop.utils;

import lombok.Getter;
import org.bukkit.ChatColor;

/*
 * Enum created at 09:53 - 02.04.2020
 * Copyright (C) elrobtossohn
 */
public enum Category {
    PETS("shop-pets"),
    MOUNTS("shop-mounts"),
    BOOTS("shop-boots"),
    PARTICLE_EFFECTS("shop-particle-effects");

    private Category(String localKey) {
        this.localKey = localKey;
    }

    @Getter
    private final String localKey;
}
