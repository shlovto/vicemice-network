package net.vicemice.lobby.player.inventorys.profile.settings.options.sub;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.lobby.player.inventorys.profile.settings.SettingsInventory;
import net.vicemice.hector.utils.players.api.internal.SkullChanger;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;

/*
 * Class created at 13:39 - 18.04.2020
 * Copyright (C) elrobtossohn
 */
public class DefaultInventory {

    public GUI create(IUser user) {
        GUI gui = user.createGUI(ChatColor.stripColor(user.translate("settings-requests-title")), 6);
        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(user);

        gui.fill(0, 9, user.getUIColor());
        gui.fill(45, 54, user.getUIColor());

        gui.setItem(8, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 14 ? 1 : 14)).name("§c✗").lore(new ArrayList<>(Arrays.asList(user.translate("gui-return").split("\n")))).build(), e -> {
            new SettingsInventory().create(user).open();
        });

        gui.setItem(11, ItemBuilder.create(SkullChanger.getSkullByTexture("f6f0d638-2ccf-4f8a-94e4-06881385614d", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvY2RiMGNlZmEyMTUxZDIxMzk3ZGFkODE0NTU3NjU4NGI0YzVjZmIxYmY2NWQ5NjM5ZDhjZTQxZTA4MmEyOGQ4OCJ9fX0=")).name(user.translate("advanced-player-tool-players")).build());
        gui.setItem(12, ItemBuilder.create(SkullChanger.getSkullByTexture("e19442f7-4300-4004-b3cf-7d5ec3c72d0c", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2JiNjEyZWI0OTVlZGUyYzVjYTUxNzhkMmQxZWNmMWNhNWEyNTVkMjVkZmMzYzI1NGJjNDdmNjg0ODc5MWQ4In19fQ==")).name(user.translate("advanced-player-tool-premium")).build());
        gui.setItem(13, ItemBuilder.create(SkullChanger.getSkullByTexture("c8a11994-e4b0-4179-9387-7964bd0eb733", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYjQzNTNmZDBmODYzMTQzNTM4NzY1ODYwNzViOWJkZjBjNDg0YWFiMDMzMWI4NzJkZjExYmQ1NjRmY2IwMjllZCJ9fX0=")).name(user.translate("advanced-player-tool-vip")).build());
        gui.setItem(14, ItemBuilder.create(SkullChanger.getSkullByTexture("61132547-215a-4690-9eab-5c547ec8db73", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNGRkYzc2YTQ3MWIxM2E4YWRiOTkzYjk5MTgzNTNhN2RjOWY3OTExZDRhNzNlNTA2MzE3MTU3NGJkOTg5YTZjZiJ9fX0=")).name(user.translate("advanced-player-tool-staffs")).build());
        gui.setItem(15, ItemBuilder.create(SkullChanger.getSkullByTexture("77550f9a-f591-414d-874d-979a55cd5c05", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmQ2ZDVmZDUxMmEzNDdjOGM4Y2JhYjRjYzlhZjNlNTMyNDlkOGQyMWQ1N2Y0YWE1ZTIxZGE5MzgxZGFlNmYifX19")).name(user.translate("advanced-player-tool-friends")).build());
        gui.setItem(30, ItemBuilder.create(SkullChanger.getSkullByTexture("bcefcc41-e997-4845-ae08-7b8a1a2d51b6", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvODY4ZjRjZWY5NDlmMzJlMzNlYzVhZTg0NWY5YzU2OTgzY2JlMTMzNzVhNGRlYzQ2ZTViYmZiN2RjYjYifX19")).name(user.translate("advanced-player-tool-friend-favorites")).build());
        gui.setItem(31, ItemBuilder.create(SkullChanger.getSkullByTexture("7cce0961-9c7d-425f-b4b9-9427c1621a33", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTIxZDhkOWFlNTI3OGUyNmJjNDM5OTkyM2QyNWNjYjkxNzNlODM3NDhlOWJhZDZkZjc2MzE0YmE5NDM2OWUifX19")).name(user.translate("advanced-player-tool-party")).build());
        gui.setItem(32, ItemBuilder.create(SkullChanger.getSkullByTexture("99e42a6d-44c4-4b62-a2f8-eabe9cdf2933", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZGYyYzlkYzMxNGJlZmM4ZDEyZjJlODc4NmM5NDM1YmZkMjRkOTExNmZjOTRjNWVjNDVmZDgzYmQyZGM4NGE0ZiJ9fX0=")).name(user.translate("advanced-player-tool-clan")).build());

        boolean members = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-members", Integer.class) == 1);
        gui.setItem(20, ItemBuilder.create(Material.INK_SACK).durability((members ? 10 : 8)).name((members ? "§a✔": "§c✘")).build(), e -> {
            if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-members", Integer.class) == 0) {
                user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add-members", 1);
            } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-members", Integer.class) == 1) {
                user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add-members", 0);
            }
            user.sendSettings();
            this.create(user).open();
        });
        boolean premiums = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-premiums", Integer.class) == 1);
        gui.setItem(21, ItemBuilder.create(Material.INK_SACK).durability((premiums ? 10 : 8)).name((premiums ? "§a✔": "§c✘")).build(), e -> {
            if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-premiums", Integer.class) == 0) {
                user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add-premiums", 1);
            } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-premiums", Integer.class) == 1) {
                user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add-premiums", 0);
            }
            user.sendSettings();
            this.create(user).open();
        });
        boolean vips = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-vips", Integer.class) == 1);
        gui.setItem(22, ItemBuilder.create(Material.INK_SACK).durability((vips ? 10 : 8)).name((vips ? "§a✔": "§c✘")).build(), e -> {
            if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-vips", Integer.class) == 0) {
                user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add-vips", 1);
            } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-vips", Integer.class) == 1) {
                user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add-vips", 0);
            }
            user.sendSettings();
            this.create(user).open();
        });
        boolean staffs = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-staffs", Integer.class) == 1);
        gui.setItem(23, ItemBuilder.create(Material.INK_SACK).durability((staffs ? 10 : 8)).name((staffs ? "§a✔": "§c✘")).build(), e -> {
            if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-staffs", Integer.class) == 0) {
                user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add-staffs", 1);
            } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-staffs", Integer.class) == 1) {
                user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add-staffs", 0);
            }
            user.sendSettings();
            this.create(user).open();
        });
        boolean friends = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-friends", Integer.class) == 1);
        gui.setItem(24, ItemBuilder.create(Material.INK_SACK).durability((friends ? 10 : 8)).name((friends ? "§a✔": "§c✘")).build(), e -> {
            if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-friends", Integer.class) == 0) {
                user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add-friends", 1);
            } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-friends", Integer.class) == 1) {
                user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add-friends", 0);
            }
            user.sendSettings();
            this.create(user).open();
        });

        boolean favorites = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-friend-favorites", Integer.class) == 1);
        gui.setItem(39, ItemBuilder.create(Material.INK_SACK).durability((favorites ? 10 : 8)).name((favorites ? "§a✔": "§c✘")).build(), e -> {
            if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-friend-favorites", Integer.class) == 0) {
                user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add-friend-favorites", 1);
            } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-friend-favorites", Integer.class) == 1) {
                user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add-friend-favorites", 0);
            }
            user.sendSettings();
            this.create(user).open();
        });
        boolean partyMembers = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-party", Integer.class) == 1);
        gui.setItem(40, ItemBuilder.create(Material.INK_SACK).durability((partyMembers ? 10 : 8)).name((partyMembers ? "§a✔": "§c✘")).build(), e -> {
            if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-party", Integer.class) == 0) {
                user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add-party", 1);
            } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-party", Integer.class) == 1) {
                user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add-party", 0);
            }
            user.sendSettings();
            this.create(user).open();
        });
        boolean clanMembers = (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-clan", Integer.class) == 1);
        gui.setItem(41, ItemBuilder.create(Material.INK_SACK).durability((clanMembers ? 10 : 8)).name((clanMembers ? "§a✔": "§c✘")).build(), e -> {
            if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-clan", Integer.class) == 0) {
                user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add-clan", 1);
            } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("friend-add-clan", Integer.class) == 1) {
                user.getNetworkPlayer().getSettingsPacket().setSetting("friend-add-clan", 0);
            }
            user.sendSettings();
            this.create(user).open();
        });

        return gui;
    }
}
