package net.vicemice.lobby.player.inventorys.shop.chests.type;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.shop.item.ShopItem;
import net.vicemice.lobby.player.shop.utils.Rarity;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.scheduler.BukkitTask;
import java.util.List;

/*
 * Class created at 02:05 - 29.04.2020
 * Copyright (C) elrobtossohn
 */
public class Animator implements Listener {
    private final IUser user;
    private final IAnimator ianimator;
    private final Animator animator;
    private GUI gui;
    private boolean running;
    private BukkitTask task;
    private int rolls;
    private int timeout;

    public Animator(IUser user, IAnimator ianimator) {
        this.user = user;
        this.ianimator = ianimator;
        this.animator = this;
    }

    public void startAnimation(int rolls, final List<ShopItem> items, final long waitAtEnd) {
        this.gui = this.ianimator.setupInventory(this.user);
        this.rolls = rolls;
        this.gui.open();
        this.running = true;
        Bukkit.getPluginManager().registerEvents(this.animator, GoServer.getService());
        this.task = Bukkit.getScheduler().runTaskTimer(GoServer.getService(), () -> {
            if (Animator.this.timeout != 0) {
                Animator animator = Animator.this;
                animator.timeout = animator.timeout - 1;
            } else {
                Animator.this.ianimator.onTick(Animator.this.user, Animator.this.gui, items, false);
                Animator animator = Animator.this;
                animator.rolls = animator.rolls - 1;
                if (Animator.this.rolls >= 20) {
                    Animator.this.timeout = 0;
                } else if (Animator.this.rolls >= 10) {
                    Animator.this.timeout = 1;
                } else if (Animator.this.rolls >= 5) {
                    Animator.this.timeout = 2;
                } else if (Animator.this.rolls >= 0) {
                    Animator.this.timeout = 3;
                }

                updateRarity(user, 18, 9, 27);
                updateRarity(user, 19, 10, 28);
                updateRarity(user, 20, 11, 29);
                updateRarity(user, 21, 12, 30);
                updateRarity(user, 22, 13, 31);
                updateRarity(user, 23, 14, 32);
                updateRarity(user, 24, 15, 33);
                updateRarity(user, 25, 16, 34);
                updateRarity(user, 26, 17, 35);

                if (Animator.this.rolls == 0) {
                    Animator.this.task.cancel();
                    Bukkit.getScheduler().runTaskLater(GoServer.getService(), () -> {
                        Animator.this.ianimator.onTick(Animator.this.user, Animator.this.gui, items, true);
                        Bukkit.getScheduler().runTaskLater(GoServer.getService(), () -> {
                            HandlerList.unregisterAll(Animator.this.animator);
                            Animator.this.running = false;
                        }, waitAtEnd);
                    }, 20L);
                }
            }
        }, 0L, 2L);
    }

    public void updateRarity(IUser user, int slot, int upper, int lower) {
        ShopItem shopItem = GoLobby.getPlayerManager().getShopManager().getItemByItemStack(user, Animator.this.gui.getHandle().getItem(slot));
        Rarity rarity = shopItem.getRarity();
        Animator.this.gui.setItem(upper, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability(rarity.getData()).name(rarity.getRarityColor()+user.translate(rarity.getKey())).build());
        Animator.this.gui.setItem(lower, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability(rarity.getData()).name(rarity.getRarityColor()+user.translate(rarity.getKey())).build());
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (e.getInventory().equals(this.gui.getHandle()) && this.running) {
            e.setCancelled(true);
            e.setResult(Event.Result.DENY);
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        final Player p = (Player)e.getPlayer();
        if (e.getInventory().equals(this.gui.getHandle()) && this.running) {
            Bukkit.getScheduler().runTaskLater(GoServer.getService(), () -> Animator.this.gui.open(), 3L);
        }
    }
}