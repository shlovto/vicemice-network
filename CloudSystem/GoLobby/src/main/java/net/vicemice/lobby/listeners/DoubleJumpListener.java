package net.vicemice.lobby.listeners;

import net.vicemice.hector.server.player.UserManager;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.hector.utils.Rank;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleFlightEvent;

public class DoubleJumpListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onToggle(PlayerToggleFlightEvent e) {
        Player player = e.getPlayer();
        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(UserManager.getUser(player));

        if (player.getGameMode() == GameMode.ADVENTURE || player.getGameMode() == GameMode.SURVIVAL) {
            if (playerData.getNetworkPlayer().getRank().isHigherEqualsLevel(Rank.PREMIUM) && playerData.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 1) {
                e.setCancelled(true);

                player.setAllowFlight(true);
                player.setFlying(false);
                player.setVelocity(player.getLocation().getDirection().multiply(1.7D).setY(1.0D));
                player.setFallDistance(0.0F);
                player.playSound(player.getLocation(), Sound.ENDERDRAGON_WINGS, 1.0F, -5.0F);
                player.setAllowFlight(false);

                Bukkit.getScheduler().scheduleSyncDelayedTask(GoLobby.getInstance(), new Runnable() {
                    @Override
                    public void run() {
                        player.setAllowFlight(true);
                    }
                }, 20 * 2);
            }
        }
    }

    private long timeOutSettings = -1L;

    public boolean isAllowJump() {
        if (System.currentTimeMillis() > this.timeOutSettings) {
            this.timeOutSettings = System.currentTimeMillis() + 2000L;
            return true;
        }
        return false;
    }
}
