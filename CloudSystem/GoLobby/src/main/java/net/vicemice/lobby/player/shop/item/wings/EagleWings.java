package net.vicemice.lobby.player.shop.item.wings;

import net.vicemice.hector.utils.players.api.particles.old.ParticleEffect;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/*
 * Class created at 21:10 - 15.04.2020
 * Copyright (C) elrobtossohn
 */
public class EagleWings {
    private Point3D[] outline = new Point3D[]{new Point3D(0.0f, 0.0f, -0.5f), new Point3D(0.1f, 0.01f, -0.5f), new Point3D(0.3f, 0.03f, -0.5f), new Point3D(0.4f, 0.04f, -0.5f), new Point3D(0.6f, 0.1f, -0.5f), new Point3D(0.61f, 0.2f, -0.5f), new Point3D(0.62f, 0.4f, -0.5f), new Point3D(0.63f, 0.6f, -0.5f), new Point3D(0.635f, 0.7f, -0.5f), new Point3D(0.7f, 0.7f, -0.5f), new Point3D(0.9f, 0.75f, -0.5f), new Point3D(1.2f, 0.8f, -0.5f), new Point3D(1.4f, 0.9f, -0.5f), new Point3D(1.6f, 1.0f, -0.5f), new Point3D(1.8f, 1.1f, -0.5f), new Point3D(1.85f, 0.9f, -0.5f), new Point3D(1.9f, 0.7f, -0.5f), new Point3D(1.85f, 0.5f, -0.5f), new Point3D(1.8f, 0.3f, -0.5f), new Point3D(1.75f, 0.1f, -0.5f), new Point3D(1.7f, -0.1f, -0.5f), new Point3D(1.65f, -0.3f, -0.5f), new Point3D(1.55f, -0.5f, -0.5f), new Point3D(1.45f, -0.7f, -0.5f), new Point3D(1.3f, -0.75f, -0.5f), new Point3D(1.15f, -0.8f, -0.5f), new Point3D(1.0f, -0.85f, -0.5f), new Point3D(0.8f, -0.87f, -0.5f), new Point3D(0.6f, -0.7f, -0.5f), new Point3D(0.5f, -0.5f, -0.5f), new Point3D(0.4f, -0.3f, -0.5f), new Point3D(0.3f, -0.3f, -0.5f), new Point3D(0.15f, -0.3f, -0.5f), new Point3D(0.0f, -0.3f, -0.5f), new Point3D(0.9f, 0.55f, -0.5f), new Point3D(1.2f, 0.6f, -0.5f), new Point3D(1.4f, 0.7f, -0.5f), new Point3D(1.6f, 0.9f, -0.5f), new Point3D(0.9f, 0.35f, -0.5f), new Point3D(1.2f, 0.4f, -0.5f), new Point3D(1.4f, 0.5f, -0.5f), new Point3D(1.6f, 0.7f, -0.5f), new Point3D(0.9f, 0.15f, -0.5f), new Point3D(1.2f, 0.2f, -0.5f), new Point3D(1.4f, 0.3f, -0.5f), new Point3D(1.6f, 0.5f, -0.5f), new Point3D(0.9f, -0.05f, -0.5f), new Point3D(1.2f, 0.0f, -0.5f), new Point3D(1.4f, 0.1f, -0.5f), new Point3D(1.6f, 0.3f, -0.5f), new Point3D(0.7f, -0.25f, -0.5f), new Point3D(1.0f, -0.2f, -0.5f), new Point3D(1.2f, -0.1f, -0.5f), new Point3D(1.4f, 0.1f, -0.5f), new Point3D(0.7f, -0.45f, -0.5f), new Point3D(1.0f, -0.4f, -0.5f), new Point3D(1.2f, -0.3f, -0.5f), new Point3D(1.4f, -0.1f, -0.5f), new Point3D(1.3f, -0.55f, -0.5f), new Point3D(1.15f, -0.6f, -0.5f), new Point3D(1.0f, -0.65f, -0.5f)};
    private Point3D[] fill = new Point3D[]{new Point3D(1.2f, 0.6f, -0.5f), new Point3D(1.4f, 0.7f, -0.5f), new Point3D(1.1f, 0.2f, -0.5f), new Point3D(1.3f, 0.3f, -0.5f), new Point3D(1.0f, -0.2f, -0.5f), new Point3D(1.2f, -0.1f, -0.5f)};
    private Color color;

    public void display(Player player, WingData data) {
        if (data.equipped) {
            this.display(player);
        }
    }

    public EagleWings(Color color) {
        this.color = color;
    }

    public void display(Player player) {
        Location playerLocation = player.getEyeLocation();
        World playerWorld = player.getWorld();
        float x = (float)playerLocation.getX();
        float y = (float)playerLocation.getY() - 0.2f;
        float z = (float)playerLocation.getZ();
        float rot = -playerLocation.getYaw() * 0.017453292f;
        Point3D rotated = null;
        ParticleEffect.ParticleColor particleColor = new ParticleEffect.OrdinaryColor(org.bukkit.Color.fromRGB(color.getRed(), color.getGreen(), color.getBlue()));
        for (Point3D point : this.outline) {
            rotated = point.rotate(rot);
            ParticleEffect.REDSTONE.display(particleColor, new Location(playerWorld, (rotated.x + x), (rotated.y + y), (rotated.z + z)), new ArrayList<>(Bukkit.getOnlinePlayers()));

            point.z *= -1.0f;
            rotated = point.rotate(rot + 3.1415f);
            point.z *= -1.0f;
            ParticleEffect.REDSTONE.display(particleColor, new Location(playerWorld, (rotated.x + x), (rotated.y + y), (rotated.z + z)), new ArrayList<>(Bukkit.getOnlinePlayers()));
        }
        for (Point3D point : this.fill) {
            rotated = point.rotate(rot);
            ParticleEffect.REDSTONE.display(particleColor, new Location(playerWorld, (rotated.x + x), (rotated.y + y), (rotated.z + z)), new ArrayList<>(Bukkit.getOnlinePlayers()));
            point.z *= -1.0f;
            rotated = point.rotate(rot + 3.1415f);
            point.z *= -1.0f;
            ParticleEffect.REDSTONE.display(particleColor, new Location(playerWorld, (rotated.x + x), (rotated.y + y), (rotated.z + z)), new ArrayList<>(Bukkit.getOnlinePlayers()));
        }
    }

    public void sendFly(Player player, Player all) {
        Location playerLocation = player.getEyeLocation();
        World playerWorld = player.getWorld();
        float x = (float)playerLocation.getX();
        float y = (float)playerLocation.getY() - 0.2f;
        float z = (float)playerLocation.getZ();
        float rot = -playerLocation.getYaw() * 0.017453292f;
        Point3D rotated = null;
        ParticleEffect.ParticleColor particleColor = new ParticleEffect.OrdinaryColor(org.bukkit.Color.fromRGB(color.getRed(), color.getGreen(), color.getBlue()));
        for (Point3D point : this.outline) {
            rotated = point.rotate(rot);
            ParticleEffect.REDSTONE.display(particleColor, new Location(playerWorld, (rotated.x + x), (rotated.y + y), (rotated.z + z)), all);
            point.z *= -1.0f;
            rotated = point.rotate(rot + 3.1415f);
            point.z *= -1.0f;
            ParticleEffect.REDSTONE.display(particleColor, new Location(playerWorld, (rotated.x + x), (rotated.y + y), (rotated.z + z)), all);
        }
        for (Point3D point : this.fill) {
            rotated = point.rotate(rot);
            ParticleEffect.REDSTONE.display(particleColor, new Location(playerWorld, (rotated.x + x), (rotated.y + y), (rotated.z + z)), all);
            point.z *= -1.0f;
            rotated = point.rotate(rot + 3.1415f);
            point.z *= -1.0f;
            ParticleEffect.REDSTONE.display(particleColor, new Location(playerWorld, (rotated.x + x), (rotated.y + y), (rotated.z + z)), all);
        }
    }

    public String getName() {
        return "eagle";
    }
}