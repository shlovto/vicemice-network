package net.vicemice.lobby.netty.sign;

import io.netty.channel.Channel;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.lobby.GoLobby;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.server.SignPacket;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;

public class SignPacketListener extends PacketGetter {

    public void receivePacket(PacketHolder initPacket) {
        if (GoLobby.getInstance().isEnabled()) {
            Bukkit.getServer().getScheduler().runTask(GoLobby.getInstance(), () -> {
                if (initPacket.getKey().equals(PacketType.SIGN_PACKET.getKey())) {
                    SignPacket signPacket = (SignPacket) initPacket.getValue();
                    Location location = new Location(Bukkit.getServer().getWorld(signPacket.getWorld()), signPacket.getX(), signPacket.getY(), signPacket.getZ());
                    if (signPacket.isAdd()) {
                        GoLobby.getPlayerManager().getServerSigns().put(location, signPacket.getServerName());
                    } else {
                        GoLobby.getPlayerManager().getServerSigns().remove(location);
                    }
                    if (location.getBlock().getType() == Material.WALL_SIGN) {
                        Sign sign = (Sign) location.getBlock().getState();
                        sign.setLine(0, signPacket.getLines()[0]);
                        sign.setLine(1, signPacket.getLines()[1]);
                        sign.setLine(2, signPacket.getLines()[2]);
                        sign.setLine(3, signPacket.getLines()[3]);
                        sign.update();
                    }
                }
            });
        }
    }

    public void channelActive(Channel channel) {
    }
}
