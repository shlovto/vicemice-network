package net.vicemice.lobby.player.shop;

import lombok.Getter;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.shop.item.ShopItem;
import net.vicemice.lobby.player.shop.utils.Category;
import net.vicemice.lobby.player.shop.utils.EntityClearupRunnable;
import net.vicemice.lobby.player.shop.utils.Rarity;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.MongoManager;
import net.vicemice.lobby.player.shop.utils.SneakRunnable;
import org.bson.Document;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/*
 * Class created at 09:50 - 02.04.2020
 * Copyright (C) elrobtossohn
 */
public class ShopManager {

    public ShopManager() {
        coinBombers = new HashMap<>();
    }

    @Getter
    private ArrayList<ShopItem> items;
    @Getter
    private final HashMap<String, Location> coinBombers;

    @Getter
    private EntityClearupRunnable entityClearupRunnable;

    public void init() {
        SneakRunnable.getInstance().start();
        this.entityClearupRunnable = new EntityClearupRunnable();
        Bukkit.getScheduler().runTaskTimer(GoLobby.getInstance(), this.entityClearupRunnable, 10L, 10L);

        items = new ArrayList<>();
        ArrayList<Document> documents = GoServer.getService().getMongoManager().getDocuments(MongoManager.Database.LOBBY, "lobbyshop_items");

        for (Document document : documents) {
            Category category = Category.valueOf(document.getString("category"));
            String id = document.getString("id");
            String languageKey = document.getString("languageKey");

            Document document1 = document.get("values", Document.class);

            Rarity rarity = Rarity.COMMON;
            Long price = 0L;
            boolean giveAble = false;
            boolean sellAble = false;
            boolean findAble = false;
            HashMap<String, String> value = new HashMap<>();

            if (document1 != null && !document1.isEmpty()) {
                rarity = Rarity.valueOf(document1.getString("rarity"));
                price = document1.getLong("price");
                giveAble = document1.getBoolean("giveAble");
                sellAble = document1.getBoolean("sellAble");
                findAble = document1.getBoolean("findAble");

                value.put("material", document1.getString("material"));

                if (category == Category.BOOTS) {
                    value.put("type", document1.getString("type"));
                    value.put("material", document1.getString("material"));
                    if (document1.containsKey("color")) {
                        Document document2 = document1.get("color", Document.class);
                        String color = document2.getInteger("red")+";"+document2.getInteger("green")+";"+document2.getInteger("blue");
                        value.put("color", color);
                    }
                    if (document1.containsKey("effectId")) {
                        value.put("effectId", document1.getString("effectId"));
                    }
                    if (document1.containsKey("secEffectId")) {
                        value.put("secEffectId", document1.getString("secEffectId"));
                    }
                    if (document1.containsKey("dropId")) {
                        value.put("dropId", document1.getString("dropId"));
                    }
                    value.put("actionId", document1.getString("actionId"));
                } else if (category == Category.PARTICLE_EFFECTS) {
                    if (document1.getString("type").equalsIgnoreCase("wings")) {
                        value.put("type", document1.getString("type"));
                        value.put("material", document1.getString("material"));
                        value.put("wings", document1.getString("wings"));
                    }
                } else if (category == Category.PETS || category == Category.MOUNTS) {
                    value.put("id", document1.getString("id"));
                    value.put("texture", document1.getString("texture"));
                    value.put("entity", document1.getString("entity"));
                }
            }

            items.add(new ShopItem(category, rarity, id, languageKey, price, giveAble, sellAble, findAble, value));
        }
    }

    public ShopItem getItemById(String id) {
        for (ShopItem shopItem : this.getItems()) {
            if (!shopItem.getId().equals(id)) continue;
            return shopItem;
        }
        return null;
    }

    public ShopItem getItemByItemStack(IUser user, ItemStack itemStack) {
        for (ShopItem shopItem : this.getItems()) {
            if (!shopItem.getDefaultItem(user).equals(itemStack)) continue;
            return shopItem;
        }
        return null;
    }

    public List<ShopItem> getCommonItems() {
        List<ShopItem> items = new ArrayList<>();
        for(ShopItem item : this.getItems()) {
            if(!item.isFindAble()) continue;
            if(item.getRarity() == Rarity.COMMON) {
                items.add(item);
            }
        }
        return items;
    }

    public List<ShopItem> getUncommonItems() {
        List<ShopItem> items = new ArrayList<>();
        for(ShopItem item : this.getItems()) {
            if(!item.isFindAble()) continue;
            if(item.getRarity() == Rarity.UNCOMMON) {
                items.add(item);
            }
        }
        return items;
    }

    public List<ShopItem> getRareItems() {
        List<ShopItem> items = new ArrayList<>();
        for(ShopItem item : this.getItems()) {
            if(!item.isFindAble()) continue;
            if(item.getRarity() == Rarity.RARE) {
                items.add(item);
            }
        }
        return items;
    }

    public List<ShopItem> getEpicItems() {
        List<ShopItem> items = new ArrayList<>();
        for(ShopItem item : this.getItems()) {
            if(!item.isFindAble()) continue;
            if(item.getRarity() == Rarity.EPIC) {
                items.add(item);
            }
        }
        return items;
    }

    public List<ShopItem> getLegendaryItems() {
        List<ShopItem> items = new ArrayList<>();
        for(ShopItem item : this.getItems()) {
            if(!item.isFindAble()) continue;
            if(item.getRarity() == Rarity.LEGENDARY) {
                items.add(item);
            }
        }
        return items;
    }

    public List<ShopItem> getWinAbleItems() {
        List<ShopItem> items = new ArrayList<>();
        for(ShopItem item : this.getItems()) {
            if(!item.isFindAble()) continue;
            items.add(item);
        }
        return items;
    }

    public void createBomb(final String bomber, final Location loc) {
        this.getCoinBombers().put(bomber, loc);
        final ArrayList<Item> it = new ArrayList<>();
        Bukkit.getScheduler().scheduleSyncDelayedTask(GoLobby.getInstance(), new Runnable() {
            public void run() {
                final ArmorStand armorStand = (ArmorStand)loc.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);
                armorStand.setHelmet(new ItemStack(Material.GOLD_BLOCK));
                armorStand.setGravity(false);
                armorStand.setVisible(false);

                List<Player> players = new ArrayList<>();
                players.addAll(Bukkit.getOnlinePlayers());

                new BukkitRunnable() {
                    int step = 0;
                    int step2 = 3;
                    int coins = 0;
                    public void run() {
                        this.step += 1;
                        this.step2 += 1;
                        if(this.step <= 200) {
                            armorStand.teleport(armorStand.getLocation().add(0.0D, 0.05D, 0.0D));
                        }
                        if (this.step >= 200) {
                            armorStand.getWorld().playEffect(armorStand.getEyeLocation(), Effect.SMOKE, 10);
                        }
                        if(this.step >= 360) {
                            Item i = armorStand.getLocation().getWorld().dropItem(armorStand.getLocation(), addItem(Material.GOLD_INGOT, "§6§lCoins " + new Random().nextInt(99999), 1));
                            it.add(i);
                            Vector direction = new Vector();
                            direction.setX(0.0 + Math.random() - Math.random());
                            direction.setZ(0.0 + Math.random() - Math.random());
                            direction.setY(0.7);
                            i.setVelocity(direction);
                            armorStand.getWorld().playSound(armorStand.getLocation(), Sound.EXPLODE, 1.0F, 1.0F);
                            //if (Main.this.getConfig().getBoolean("bomb.firework.randomcolor")) {
                            Random r = new Random();
                            int r1i = r.nextInt(17) + 1;
                            int r2i = r.nextInt(17) + 1;
                            Color c1 = getColor(r1i);
                            Color c2 = getColor(r2i);
                            Firework firework = armorStand.getLocation().getWorld().spawn(armorStand.getLocation(), Firework.class);
                            FireworkMeta data = firework.getFireworkMeta();
                            data.addEffects(new FireworkEffect[]{FireworkEffect.builder().withColor(c1).withFade(c2).with(FireworkEffect.Type.STAR).build()});
                            data.setPower(1);
                            firework.setFireworkMeta(data);
                            detonate(firework, 1L);
                            this.coins += 1;

                            if (coins == 30) {
                                armorStand.remove();
                                cancel();
                                getCoinBombers().remove(bomber);
                            }
                        }
                    }
                }.runTaskTimer(GoLobby.getInstance(), 0L, 1L);
            }
        }, 20*3L);
    }

    private void detonate(final Firework fw, long ticks) {
        Bukkit.getScheduler().runTaskLater(GoLobby.getInstance(), new Runnable(){

            @Override
            public void run() {
                fw.detonate();
            }
        }, ticks);
    }

    public ItemStack addItem(Material material, String itemname, int amount) {
        ItemStack item = new ItemStack(material, amount);
        ItemMeta im = item.getItemMeta();
        im.setDisplayName(itemname.replace("&", "\u00a7"));
        item.setItemMeta(im);
        return item;
    }

    public void addItem(Player player, Material material, String itemname, int amount, int slot) {
        ItemStack item = new ItemStack(material, amount);
        ItemMeta im = item.getItemMeta();
        im.setDisplayName(itemname.replace("&", "\u00a7"));
        item.setItemMeta(im);
        player.getInventory().setItem(slot, item);
    }

    private Color getColor(int i) {
        Color c = null;
        if (i == 1) {
            c = Color.AQUA;
        }
        if (i == 2) {
            c = Color.BLACK;
        }
        if (i == 3) {
            c = Color.BLUE;
        }
        if (i == 4) {
            c = Color.FUCHSIA;
        }
        if (i == 5) {
            c = Color.GRAY;
        }
        if (i == 6) {
            c = Color.GREEN;
        }
        if (i == 7) {
            c = Color.LIME;
        }
        if (i == 8) {
            c = Color.MAROON;
        }
        if (i == 9) {
            c = Color.NAVY;
        }
        if (i == 10) {
            c = Color.OLIVE;
        }
        if (i == 11) {
            c = Color.ORANGE;
        }
        if (i == 12) {
            c = Color.PURPLE;
        }
        if (i == 13) {
            c = Color.RED;
        }
        if (i == 14) {
            c = Color.SILVER;
        }
        if (i == 15) {
            c = Color.TEAL;
        }
        if (i == 16) {
            c = Color.WHITE;
        }
        if (i == 17) {
            c = Color.YELLOW;
        }
        return c;
    }
}