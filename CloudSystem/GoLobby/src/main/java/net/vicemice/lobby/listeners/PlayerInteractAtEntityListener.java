package net.vicemice.lobby.listeners;

import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.lobby.player.inventorys.shop.dailyreward.DailyRewardInventory;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

/*
 * Class created at 00:49 - 17.04.2020
 * Copyright (C) elrobtossohn
 */
public class PlayerInteractAtEntityListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEntityEvent event) {
        IUser user = UserManager.getUser(event.getPlayer());
        event.setCancelled(true);

        if (event.getRightClicked().getType() == EntityType.VILLAGER) {
            new DailyRewardInventory().create(user).open();
        }
    }
}