package net.vicemice.lobby.player.shop.utils;

/*
 * Enum created at 10:16 - 02.04.2020
 * Copyright (C) elrobtossohn
 */
public enum Found {
    CHEST_OPENING,
    SHOP,
    TEAM;
}