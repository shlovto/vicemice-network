package net.vicemice.lobby.listeners;

import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.utils.labymod.event.LabyModPlayerJoinEvent;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import java.util.Random;

/*
 * Class created at 20:04 - 02.05.2020
 * Copyright (C) elrobtossohn
 */
public class LabyModPlayerJoinListener implements Listener {

    @EventHandler
    public void onJoin(LabyModPlayerJoinEvent event) {
        IUser user = UserManager.getUser(event.getPlayer());

        int coins = new Random().nextInt(1000);
        boolean collected = false;
        if (user.getStatistic("user_labymod_reward", StatisticPeriod.DAY) == 0) {
            user.increaseStatistic("user_labymod_reward", 1);
            user.increaseCoins(coins);
            collected = true;
        }

        if (collected) {
            int i = new Random().nextInt(2);

            if (i == 0) {
                user.sendMessage("labymod-reward-collection-one");
            } else if (i == 1) {
                user.sendMessage("labymod-reward-collection-two");
            } else if (i == 2) {
                user.sendMessage("labymod-reward-collection-three");
            }

            user.sendMessage("labymod-reward-collected", coins);
        }
    }
}