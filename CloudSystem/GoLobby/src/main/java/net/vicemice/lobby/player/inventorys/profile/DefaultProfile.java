package net.vicemice.lobby.player.inventorys.profile;

import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.inventorys.profile.friends.FriendsInventory;
import net.vicemice.lobby.player.inventorys.profile.replays.ReplayInventory;
import net.vicemice.lobby.player.inventorys.profile.settings.SettingsInventory;
import net.vicemice.lobby.player.utils.InventoryType;
import net.vicemice.hector.utils.players.api.internal.SkullChanger;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import java.util.ArrayList;
import java.util.Arrays;

public class DefaultProfile {

    public GUI create(IUser user, InventoryType inventoryGUI) {
        GUI gui = user.createGUI("§7Loading...", 6);
        if (inventoryGUI == InventoryType.FRIENDS) {
            gui = user.createGUI(ChatColor.stripColor(user.translate("profile-friends")), 6);
        } else if (inventoryGUI == InventoryType.SETTINGS) {
            gui = user.createGUI(ChatColor.stripColor(user.translate("profile-settings")), 6);
        } else if (inventoryGUI == InventoryType.REPLAYS_SAVED || inventoryGUI == InventoryType.REPLAYS_RECENT) {
            gui = user.createGUI(ChatColor.stripColor(user.translate("profile-replays")), 6);
        }

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        if (inventoryGUI == InventoryType.FRIENDS) {
            gui.setItem(36, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability(5).name("§0 ").build());
            GoLobby.getPlayerManager().getPlayer(user).setLastProfileType(InventoryType.FRIENDS);
        } else if (inventoryGUI == InventoryType.SETTINGS) {
            gui.setItem(37, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability(5).name("§0 ").build());
            GoLobby.getPlayerManager().getPlayer(user).setLastProfileType(InventoryType.SETTINGS);
        } else if (inventoryGUI == InventoryType.REPLAYS_SAVED || inventoryGUI == InventoryType.REPLAYS_RECENT) {
            gui.setItem(38, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability(5).name("§0 ").build());
            GoLobby.getPlayerManager().getPlayer(user).setLastProfileType(inventoryGUI);
        }

        gui.setItem(45, ItemBuilder.create(SkullChanger.getSkull(user.getBukkitPlayer())).name(user.translate("profile-friends")).lore(new ArrayList<>(Arrays.asList((user.translate("profile-friends-description").split("\n"))))).build(), e -> {
            new FriendsInventory().create(user, 1).open();
        });
        gui.setItem(46, ItemBuilder.create(Material.REDSTONE_COMPARATOR).name(user.translate("profile-settings")).lore(new ArrayList<>(Arrays.asList((user.translate("profile-settings-description").split("\n"))))).build(), e -> {
            new SettingsInventory().create(user).open();
        });
        gui.setItem(47, ItemBuilder.create(Material.BOOK).name(user.translate("profile-replays")).lore(new ArrayList<>(Arrays.asList((user.translate("profile-replays-description").split("\n"))))).build(), e -> {
            new ReplayInventory().create(user, 1, false).open();
        });

        return gui;
    }

    public GUI create(IUser user, String name, int rows) {
        GUI gui = user.createGUI(name, rows);

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        return gui;
    }

    public GUI createView(IUser user, String name) {
        GUI gui = user.createGUI(name, 3);

        gui.fill(0, 53, user.getUIColor());

        return gui;
    }
}