package net.vicemice.lobby.player.inventorys.profile.settings.options;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.lobby.player.inventorys.profile.settings.SettingsInventory;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.Rank;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;

public class MovementSettingsInventory {

    public GUI create(IUser user) {
        GUI gui = user.createGUI(ChatColor.stripColor(user.translate("settings-change-movement")), 5);
        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(user);

        gui.fill(0, 9, ItemBuilder.create(user.getUIColor()).name("§0 ").build());
        gui.fill(36, 45, ItemBuilder.create(user.getUIColor()).name("§0 ").build());

        gui.setItem(8, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 14 ? 1 : 14)).name("§c✗").lore(new ArrayList<>(Arrays.asList(user.translate("gui-return").split("\n")))).build(), e -> {
            new SettingsInventory().create(user).open();
        });

        gui.setItem(20, ItemBuilder.create(Material.LEATHER_BOOTS).name(user.translate("settings-movement-walk")).lore((user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 0 ? user.translate("already-selected") : user.translate("settings-movement-select")), user.translate("settings-movement-walk-description")).glow((user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 0)).build(), e -> {
            if (!user.getNetworkPlayer().getRank().isHigherEqualsLevel(Rank.MEMBER)) return;
            if (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 0) return;
            user.getNetworkPlayer().getSettingsPacket().setSetting("lobby-movement", 0);
            user.sendSettings();
            user.getBukkitPlayer().setAllowFlight(false);
            user.getBukkitPlayer().setFlying(false);
            this.create(user).open();
        });

        gui.setItem(22, ItemBuilder.create(Material.SLIME_BALL).name(user.translate("settings-movement-doublejump")).lore((user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 1 ? user.translate("already-selected") : (user.getNetworkPlayer().getRank().isHigherEqualsLevel(Rank.PREMIUM) ? user.translate("settings-movement-select") : user.translate("settings-not-selectable"))), user.translate("settings-movement-doublejump-description")).glow((user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 1)).build(), e -> {
            if (!user.getNetworkPlayer().getRank().isHigherEqualsLevel(Rank.PREMIUM)) return;
            if (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 1) return;
            user.getNetworkPlayer().getSettingsPacket().setSetting("lobby-movement", 1);
            user.sendSettings();
            user.getBukkitPlayer().setAllowFlight(true);
            user.getBukkitPlayer().setFlying(false);
            this.create(user).open();
        });

        gui.setItem(24, ItemBuilder.create(Material.FEATHER).name(user.translate("settings-movement-fly")).lore((user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 2 ? user.translate("already-selected") : (user.getNetworkPlayer().getRank().isHigherEqualsLevel(Rank.VIP) ? user.translate("settings-movement-select") : user.translate("settings-not-selectable"))), user.translate("settings-movement-fly-description")).glow((user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 2)).build(), e -> {
            if (!user.getNetworkPlayer().getRank().isHigherEqualsLevel(Rank.VIP)) return;
            if (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) == 2) return;
            user.getNetworkPlayer().getSettingsPacket().setSetting("lobby-movement", 2);
            user.sendSettings();
            user.getBukkitPlayer().setAllowFlight(true);
            this.create(user).open();
        });

        return gui;
    }
}