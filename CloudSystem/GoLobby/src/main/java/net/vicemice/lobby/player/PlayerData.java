package net.vicemice.lobby.player;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.shop.item.PlayerShopItem;
import net.vicemice.lobby.player.shop.item.pets.Pet;
import net.vicemice.lobby.player.shop.item.pets.PetUtils;
import net.vicemice.lobby.player.shop.item.wings.WingManager;
import net.vicemice.lobby.player.shop.utils.Category;
import net.vicemice.lobby.player.shop.utils.Found;
import net.vicemice.lobby.player.utils.InventoryType;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.scoreboard.PlayerScoreboard;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.server.utils.MongoManager;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.players.api.hologram.Hologram;
import net.vicemice.hector.utils.players.api.hologram.HologramAPI;
import net.vicemice.hector.utils.players.api.particles.Particle;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.players.NetworkPlayer;
import net.vicemice.hector.utils.players.lobby.LobbyFriendData;
import net.vicemice.lobby.utils.Boots;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Getter
public class PlayerData {

    private final IUser user;
    private PlayerScoreboard packetScoreboard;
    @Setter
    private InventoryType lastProfileType, lastShopType, lastInventoryType;
    @Setter
    private Locale locale;
    @Setter
    private boolean settingsChanged = false, nickChanged = false, scoreboardCreated;

    private final List<String> buyedData;
    private Map<String, String> petNames;
    private Map<Category, Map<Integer, PlayerShopItem>> items;

    @Setter
    private String activeBootsId;
    @Setter
    private Boots boots;
    @Setter
    private String activeParticleId;
    @Setter
    private Particle particle;

    @Getter
    private final Hologram lottery, dailyReward, itemChest, privateServer;

    public PlayerData(IUser user) {
        this.user = user;
        this.lastProfileType = InventoryType.FRIENDS;
        this.lastShopType = InventoryType.LOTTERY;
        this.lastInventoryType = InventoryType.PETS;

        if (this.getNetworkPlayer() != null) {
            this.locale = this.getNetworkPlayer().getLocale();
        } else {
            this.locale = Locale.forLanguageTag(GoServer.getService().getMongoManager().getDocument(MongoManager.Database.CLOUD, "players", new Document("uniqueId", String.valueOf(user.getUniqueId()))).getString("locale"));
        }

        this.buyedData = new ArrayList<>();
        this.petNames = new HashMap<>();

        this.loadShopData();
        this.loadPets();
        this.createLastShopItems();

        lottery = user.createHologram(new Location(Bukkit.getWorld("world"), 0.5, 14.0, 42.5), new ArrayList<>(Arrays.asList(this.getUser().translate("hologram-lottery").split("\n"))));
        lottery.display();
        dailyReward = user.createHologram(new Location(Bukkit.getWorld("world"), 2.5, 15.0, 41.5), new ArrayList<>(Arrays.asList(this.getUser().translate("hologram-dailyreward").split("\n"))));
        dailyReward.display();
        itemChest = user.createHologram(new Location(Bukkit.getWorld("world"), 4.5, 14.0, 42.5), new ArrayList<>(Arrays.asList(this.getUser().translate("hologram-item-chest").split("\n"))));
        itemChest.display();
        privateServer = user.createHologram(new Location(Bukkit.getWorld("world"), 4.5, 15.0, 50.5), new ArrayList<>(Arrays.asList(this.getUser().translate("hologram-private-servers").split("\n"))));
        privateServer.display();
    }

    public void updateHolos() {
        lottery.update(new ArrayList<>(Arrays.asList(this.getUser().translate("hologram-lottery").split("\n"))));
        dailyReward.update(new ArrayList<>(Arrays.asList(this.getUser().translate("hologram-dailyreward").split("\n"))));
        itemChest.update(new ArrayList<>(Arrays.asList(this.getUser().translate("hologram-item-chest").split("\n"))));
    }

    public void loadShopData() {
        this.items = new HashMap<>();
        this.items.put(Category.PETS, new HashMap<>());
        this.items.put(Category.MOUNTS, new HashMap<>());
        this.items.put(Category.BOOTS, new HashMap<>());
        this.items.put(Category.PARTICLE_EFFECTS, new HashMap<>());

        this.items.get(Category.PARTICLE_EFFECTS).put(0, new PlayerShopItem("ADMINRAINBOWWINGSAREVERYCOOLANDLOOKSLIKEHACKSBUTTHISISNOTAHACKBITCH", "RAINBOW_WINGS", Found.TEAM));

        Bukkit.getScheduler().runTaskLaterAsynchronously(GoLobby.getInstance(), () -> {
            List<Document> documents = GoServer.getService().getMongoManager().getDocumentsWithFilter(MongoManager.Database.LOBBY, "lobbyshop_player_items", new Document("user", String.valueOf(user.getUniqueId())));

            for (Document document : documents) {
                String id = document.getString("id");
                String itemId = document.getString("itemId");
                Found found = Found.valueOf(document.getString("found"));
                if (found == null) {
                    found = Found.TEAM;
                }

                PlayerShopItem psi = new PlayerShopItem(itemId, id, found);

                if (!buyedData.contains(itemId)) {
                    items.get(psi.getShopItem().getCategory()).put((items.get(psi.getShopItem().getCategory()).size() + 1), psi);
                    buyedData.add(itemId);
                }
            }
        }, 20L);
    }

    public void loadPets() {
        this.petNames = new HashMap<>();

        Bukkit.getScheduler().runTaskLaterAsynchronously(GoLobby.getInstance(), () -> {
            List<Document> documents = GoServer.getService().getMongoManager().getDocumentsWithFilter(MongoManager.Database.LOBBY, "lobbyshop_player_items_petnames", new Document("user", String.valueOf(user.getUniqueId())));

            for (Document document : documents) {
                String itemId = document.getString("itemId");
                String name = document.getString("name");
                petNames.put(itemId, name);
            }
        }, 20L);
    }

    public PlayerShopItem getShopItemById(Category category, String itemId) {
        for (PlayerShopItem playerShopItem : this.getItems().get(category).values()) {
            if (!playerShopItem.getId().equalsIgnoreCase(itemId)) continue;
            return playerShopItem;
        }
        return null;
    }

    public void createLastShopItems() {
        if (this.getNetworkPlayer().getSettingsPacket().getSetting("last-active-lobby-pet", String.class) != null) {
            PlayerShopItem playerShopItem = this.getShopItemById(Category.PETS, this.getNetworkPlayer().getSettingsPacket().getSetting("last-active-lobby-pet", String.class));
            if (playerShopItem != null) {
                String name = this.getUser().translate(playerShopItem.getShopItem().getLanguageKey());

                if (this.getPetNames().containsKey(playerShopItem.getId())) {
                    name = this.getPetNames().get(playerShopItem.getId());
                }

                PetUtils.spawnPet(user, new Pet(EntityType.valueOf(playerShopItem.getShopItem().getValues().get("entity")), name, user.getUniqueId(), playerShopItem.getId()));
            }
        }
        if (this.getNetworkPlayer().getSettingsPacket().getSetting("last-active-lobby-effect", String.class) != null) {
            PlayerShopItem playerShopItem = this.getShopItemById(Category.PARTICLE_EFFECTS, this.getNetworkPlayer().getSettingsPacket().getSetting("last-active-lobby-effect", String.class));
            if (playerShopItem != null) {
                if (playerShopItem.getShopItem().getValues().get("type").equals("wings")) {
                    WingManager.Wings wings = WingManager.Wings.valueOf(playerShopItem.getShopItem().getValues().get("wings"));

                    if (wings != null) {
                        this.activateWings(wings, playerShopItem.getId());
                        user.sendMessage("shop-effect-activated", this.getUser().translate(playerShopItem.getShopItem().getLanguageKey()));
                    }
                } else {
                    this.setParticle(Particle.valueOf(playerShopItem.getShopItem().getValues().get("effectid")));

                    user.sendMessage("shop-effect-activated", this.getUser().translate(playerShopItem.getShopItem().getLanguageKey()));
                }
            }
        }
    }

    public void saveAll() {
        this.saveShopData();
        this.savePetSettings();
        this.saveLastShopItems();
    }

    public void saveShopData() {
        for (Category category : this.getItems().keySet()) {
            for (PlayerShopItem playerShopItem : this.getItems().get(category).values()) {
                if (playerShopItem.getId().equalsIgnoreCase("ADMINRAINBOWWINGSAREVERYCOOLANDLOOKSLIKEHACKSBUTTHISISNOTAHACKBITCH"))
                    continue;
                Document exist = GoServer.getService().getMongoManager().getDocument(MongoManager.Database.LOBBY, "lobbyshop_player_items", new Document("itemId", playerShopItem.getId()).append("user", String.valueOf(user.getUniqueId())));

                if (exist != null && !exist.isEmpty()) {
                    Document document = new Document("id", playerShopItem.getShopItem().getId());
                    document.append("found", playerShopItem.getFound().name().toUpperCase());
                    Bson filter = new Document("itemId", playerShopItem.getId());
                    GoServer.getService().getMongoManager().getLobbyService().getCollection("lobbyshop_player_items").updateOne(filter, new Document("$set", document));
                } else {
                    Document document = new Document("id", playerShopItem.getShopItem().getId());
                    document.append("user", String.valueOf(user.getUniqueId()));
                    document.append("itemId", playerShopItem.getId());
                    document.append("found", playerShopItem.getFound().name().toUpperCase());
                    GoServer.getService().getMongoManager().getLobbyService().getCollection("lobbyshop_player_items").insertOne(document);
                }
            }
        }
    }

    public void savePetSettings() {
        for (String itemId : this.getPetNames().keySet()) {
            Document exist = GoServer.getService().getMongoManager().getDocument(MongoManager.Database.LOBBY, "lobbyshop_player_items_petnames", new Document("itemId", itemId).append("user", String.valueOf(user.getUniqueId())));

            if (exist != null && !exist.isEmpty()) {
                Document document = new Document("name", this.getPetNames().get(itemId));
                Bson filter = new Document("itemId", itemId).append("user", String.valueOf(user.getUniqueId()));
                GoServer.getService().getMongoManager().getLobbyService().getCollection("lobbyshop_player_items_petnames").updateOne(filter, new Document("$set", document));
            } else {
                Document document = new Document("user", String.valueOf(user.getUniqueId()));
                document.append("itemId", itemId);
                document.append("name", this.getPetNames().get(itemId));
                GoServer.getService().getMongoManager().getLobbyService().getCollection("lobbyshop_player_items_petnames").insertOne(document);
            }
        }
    }

    public void saveLastShopItems() {
        if (PetUtils.pets.containsKey(user)) {
            this.getNetworkPlayer().getSettingsPacket().setSetting("last-active-lobby-pet", PetUtils.pets.get(user).getItemId());
        } else {
            this.getNetworkPlayer().getSettingsPacket().setSetting("last-active-lobby-pet", null);
        }
        this.getNetworkPlayer().getSettingsPacket().setSetting("last-active-lobby-effect", this.getActiveParticleId());
        this.getNetworkPlayer().getSettingsPacket().setSetting("last-active-lobby-boots", this.getActiveBootsId());
        this.sendSettings();
    }

    private void createBoard() {
        if (scoreboardCreated && packetScoreboard != null && !this.getNetworkPlayer().getLocale().toLanguageTag().equalsIgnoreCase(this.getLocale().toLanguageTag()))
            return;
        this.packetScoreboard = new PlayerScoreboard(user.getBukkitPlayer());
        this.packetScoreboard.remove();
        this.packetScoreboard.sendTitle();
        this.packetScoreboard.setLine(1, user.translate("scoreboard-playtime"));
        this.packetScoreboard.setLine(2, "§2");
        this.packetScoreboard.setLine(4, user.translate("scoreboard-coins"));
        this.packetScoreboard.setLine(5, "§3");
        this.packetScoreboard.setLine(7, user.translate("scoreboard-friends"));
        this.packetScoreboard.setLine(8, "§4");
        this.packetScoreboard.setLine(10, user.translate("scoreboard-rank"));
        this.packetScoreboard.setLine(11, "§5");
        this.scoreboardCreated = true;
        this.setLocale(this.getNetworkPlayer().getLocale());
    }

    public void updateBoard() {
        this.createBoard();

        this.packetScoreboard.setLine(0, "§7" + (this.getNetworkPlayer().getPlayTime() / 60) + " " + this.getUser().translate("HOURS"));
        this.packetScoreboard.setLine(3, "§7" + GoAPI.formatDecimal(Locale.GERMANY, this.getNetworkPlayer().getCoins()));

        AtomicInteger online = new AtomicInteger();
        this.getNetworkPlayer().getFriends().forEach(e -> {
            if (e.isOnline()) online.getAndIncrement();
        });

        this.packetScoreboard.setLine(6, user.translate("scoreboard-friends-format", online.get(), this.getNetworkPlayer().getFriends().size()));
        this.packetScoreboard.setLine(9, "§7" + this.getNetworkPlayer().getRank().getRankColor() + this.getNetworkPlayer().getRank().getFullName());
    }

    public NetworkPlayer getNetworkPlayer() {
        return GoServer.getService().getPlayerManager().getPlayer(this.user.getUniqueId());
    }

    public Locale getLocale() {
        return GoServer.getService().getPlayerManager().getPlayer(this.user.getUniqueId()).getLocale();
    }

    public void updatePlayers() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            this.handlePlayerJoinSync(player);
        }
    }

    private void handlePlayerJoinSync(Player player) {
        if (this.canSeePlayer(player)) {
            this.getUser().getBukkitPlayer().showPlayer(player);
        } else {
            this.getUser().getBukkitPlayer().hidePlayer(player);
        }
    }

    public boolean canSeePlayer(Player player) {
        if (this.getNetworkPlayer().getSettingsPacket().getSetting("player-visibility", Integer.class) == 0) {
            return true;
        }
        if (this.getNetworkPlayer().getSettingsPacket().getSetting("player-visibility", Integer.class) == 1) {
            if (user.getRank().isHigherEqualsLevel(Rank.VIP)) {
                return true;
            }

            for (LobbyFriendData lobbyFriendData : this.getNetworkPlayer().getFriends()) {
                if (!lobbyFriendData.getName().equalsIgnoreCase(player.getName())) continue;
                return true;
            }
        }
        if (this.getNetworkPlayer().getSettingsPacket().getSetting("player-visibility", Integer.class) == 2) {
            return false;
        }
        return true;
    }

    public void sendSettings() {
        this.setSettingsChanged(true);
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.UPDATE_PLAYER_SETTINGS, this.getNetworkPlayer().getSettingsPacket()), API.PacketReceiver.SLAVE);
    }

    public void activateWings(WingManager.Wings wings, String itemId) {
        this.activeParticleId = itemId;
        WingManager.equipWings(wings, user.getBukkitPlayer());
    }

    public void disableWings() {
        this.activeParticleId = null;
        WingManager.unequipWings(user.getBukkitPlayer());
    }
}