package net.vicemice.lobby.listeners;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.inventorys.privateserver.PrivateServerInventory;
import net.vicemice.lobby.player.inventorys.server.ServerInventory;
import net.vicemice.lobby.player.inventorys.shop.dailyreward.DailyRewardInventory;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.CommandPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.players.api.internal.fakemobs.event.PlayerInteractFakeMobEvent;
import net.vicemice.hector.utils.players.api.internal.fakemobs.util.FakeMob;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.utils.CommandType;
import net.vicemice.hector.utils.Rank;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class NPCInteractListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractFakeMobEvent event) {
        FakeMob npc = event.getMob();
        IUser user = UserManager.getUser(event.getPlayer());

        if (GoLobby.getPlayerManager().getBuilders().contains(user.getUniqueId())) {
            if (user.getRank().isHigherEqualsLevel(Rank.ADMIN)) {
                user.sendRawMessage("The NPC UniqueID is '" + npc.getUniqueID() + "'");

                if (user.getBukkitPlayer().isSneaking()) {
                    if (npc.isSitting()) {
                        npc.setSitting(false);
                        user.sendRawMessage("§cNot longer sitting.");
                    } else {
                        npc.setSitting(true);
                        user.sendRawMessage("§aNow sitting.");
                    }

                    npc.updateMetadata();
                } else {
                    if (npc.isPlayerLook()) {
                        npc.setPlayerLook(false);
                        user.sendRawMessage("§cNot longer player looking.");
                    } else {
                        npc.setPlayerLook(true);
                        user.sendRawMessage("§aNow playing looking.");
                    }
                }
            }
            return;
        }

        if (npc.getEntityId() == GoLobby.getBwRanked().getEntityId()) {
            if (event.getAction() == PlayerInteractFakeMobEvent.Action.RIGHT_CLICK) {
                new ServerInventory().create(user, "bedwars", "EloBedWars", 1).open();
            } else if (event.getAction() == PlayerInteractFakeMobEvent.Action.LEFT_CLICK) {
                GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.PLAY, new String[]{"EloBedWars"})), API.PacketReceiver.SLAVE);
            }
        } else if (npc.getEntityId() == GoLobby.getBwUnranked().getEntityId()) {
            if (event.getAction() == PlayerInteractFakeMobEvent.Action.RIGHT_CLICK) {
                new ServerInventory().create(user, "bedwars", "BedWars", 1).open();
            } else if (event.getAction() == PlayerInteractFakeMobEvent.Action.LEFT_CLICK) {
                GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.PLAY, new String[]{"BedWars"})), API.PacketReceiver.SLAVE);
            }
        } else if (npc.getEntityId() == GoLobby.getQsgRanked().getEntityId()) {
            if (event.getAction() == PlayerInteractFakeMobEvent.Action.RIGHT_CLICK) {
                new ServerInventory().create(user, "qsg", "EloQSG", 1).open();
            } else if (event.getAction() == PlayerInteractFakeMobEvent.Action.LEFT_CLICK) {
                GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.PLAY, new String[]{"EloQSG"})), API.PacketReceiver.SLAVE);
            }
        } else if (npc.getEntityId() == GoLobby.getQsgUnranked().getEntityId()) {
            if (event.getAction() == PlayerInteractFakeMobEvent.Action.RIGHT_CLICK) {
                new ServerInventory().create(user, "qsg", "QSG", 1).open();
            } else if (event.getAction() == PlayerInteractFakeMobEvent.Action.LEFT_CLICK) {
                GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.PLAY, new String[]{"QSG"})), API.PacketReceiver.SLAVE);
            }
        } else if (npc.getEntityId() == GoLobby.getHeisenberg().getEntityId()) {
            new DailyRewardInventory().create(user).open();
        } else if (npc.getEntityId() == GoLobby.getPrivateServer().getEntityId()) {
            new PrivateServerInventory().create(user, 1).open();
        }
    }
}
