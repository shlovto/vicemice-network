package net.vicemice.lobby.commands;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

/*
 * Class created at 03:27 - 03.05.2020
 * Copyright (C) elrobtossohn
 */
public class DevCommand extends Command {

    public DevCommand() {
        super("dev");
    }

    @Override
    public boolean execute(CommandSender commandSender, String label, String[] args) {
        IUser user = UserManager.getUser(Bukkit.getPlayer(commandSender.getName()));

        if (user.getRank().isLowerLevel(Rank.ADMIN)) return false;

        if (args.length > 0) {
            if (args[0].equalsIgnoreCase("prototypeEffects")) {
                if (GoServer.getService().getPlayerManager().isPrototypeEffects()) {
                    GoServer.getService().getPlayerManager().setPrototypeEffects(false);
                    user.sendRawMessage("not longer prototyped");
                } else {
                    GoServer.getService().getPlayerManager().setPrototypeEffects(true);
                    user.sendRawMessage("now prototyped");
                }
            } else if (args[0].equalsIgnoreCase("iamonline")) {
                user.sendRawMessage("Hector Registered? "+UserManager.getConnectedUsers().containsKey(user.getUniqueId()));
                user.sendRawMessage("Lobby Registered? "+GoLobby.getPlayerManager().getPlayers().containsKey(user.getUniqueId()));
            }
        } else {
            user.sendRawMessage("§cUnknown Command");
        }

        return false;
    }
}
