package net.vicemice.lobby.commands;

import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.player.inventorys.server.ServerInventory;
import net.vicemice.hector.utils.Rank;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ServersCommand extends Command {

    public ServersCommand() {
        super("servers");
    }

    @Override
    public boolean execute(CommandSender commandSender, String label, String[] args) {
        IUser user = UserManager.getUser((Player) commandSender);
        if (user.getRank().isLowerLevel(Rank.ADMIN)) {
            user.sendMessage("command-no-permission");
            return true;
        }

        new ServerInventory().create(user, 1).open();

        return true;
    }
}