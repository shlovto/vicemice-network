package net.vicemice.lobby.utils;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class Locations {

    @Getter
    private static Location spawnLocation = new Location(Bukkit.getWorld("world"), -3.5, 14.0, 48.5, -64.5f, 0.0f);
    @Getter
    private static Location bedWarsLocation = new Location(Bukkit.getWorld("world"), 16.5, 13.0, 67.5, -45.0f, 0.0f);
    @Getter
    private static Location qsgLocation = new Location(Bukkit.getWorld("world"), 35.5, 12.5, 4.5, -130.0f, 0.0f);
}