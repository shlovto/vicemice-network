package net.vicemice.lobby.player.inventorys.profile.settings.options;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.inventorys.profile.DefaultProfile;
import net.vicemice.lobby.player.inventorys.profile.settings.SettingsInventory;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.CommandPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.CommandType;
import net.vicemice.hector.utils.players.utils.Pagination;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

public class LocaleSettingsInventory {

    public GUI create(IUser user, int page) {
        Pagination<Locale> pagination = new Pagination<>(new ArrayList<>(GoServer.getLocaleManager().getLocales().keySet()), 27);
        GUI gui = new DefaultProfile().create(user, ChatColor.stripColor(user.translate("settings-change-language")), (pagination.getPages() > 1 ? 6 : 5));

        gui.setItem(8, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 14 ? 1 : 14)).name("§c✗").lore(new ArrayList<>(Arrays.asList(user.translate("gui-return").split("\n")))).build(), e -> {
            new SettingsInventory().create(user).open();
        });

        if (pagination.getElementsFor(page).isEmpty()) {
            gui.setItem(22, ItemBuilder.create(Material.BARRIER).name(user.translate("settings-language-no-languages")).build());
        }

        AtomicInteger i = new AtomicInteger(9);
        pagination.printPage(page, locale -> {
            boolean own = (locale.toLanguageTag().equalsIgnoreCase(user.getLocale().toLanguageTag()));

            gui.setItem(i.get(), ItemBuilder.create(Material.PAPER).name((own ? "&a" : "&6")+locale.getDisplayName(user.getLocale())).lore((own ? user.translate("already-selected") : user.translate("settings-language-select"))).glow(own).build(), e -> {
                if (own) return;
                GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.LANGUAGE, new String[]{locale.toLanguageTag()})), API.PacketReceiver.SLAVE);
                new SettingsInventory().create(user).open();
            });
            i.set(i.get() + 1);
        });

        if (!pagination.getElementsFor((page-1)).isEmpty()) {
            gui.setItem(48, ItemBuilder.create(GUI.Items.getLeft()).name(user.translate("previous-page")).build(), e -> {
                this.create(user, (page-1)).open();
            });
        }
        if (!pagination.getElementsFor((page+1)).isEmpty()) {
            gui.setItem(50, ItemBuilder.create(GUI.Items.getRight()).name(user.translate("next-page")).build(), e -> {
                this.create(user, (page+1)).open();
            });
        }

        return gui;
    }
}