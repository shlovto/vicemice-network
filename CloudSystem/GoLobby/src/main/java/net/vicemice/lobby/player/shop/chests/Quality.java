package net.vicemice.lobby.player.shop.chests;

/*
 * Class created at 10:05 - 02.04.2020
 * Copyright (C) elrobtossohn
 */
public enum Quality {
    COMMON(6, 3, 2, 1),
    RARE(3, 2, 1, 1),
    EPIC(1, 2, 2, 2),
    LEGENDARY(0, 1, 2, 4);

    private int common;
    private int rare;
    private int epic;
    private int legendary;

    private Quality(int common, int rare, int epic, int legendary) {
        this.common = common;
        this.rare = rare;
        this.epic = epic;
        this.legendary = legendary;
    }

    public int getCommon() {
        return this.common;
    }

    public int getRare() {
        return this.rare;
    }

    public int getEpic() {
        return this.epic;
    }

    public int getLegendary() {
        return this.legendary;
    }
}