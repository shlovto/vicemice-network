package net.vicemice.lobby.player.inventorys.shop.chests.type;

import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.player.shop.item.ShopItem;
import net.vicemice.hector.utils.players.api.gui.GUI;
import org.bukkit.entity.Player;
import java.util.List;

/*
 * Interface created at 02:05 - 29.04.2020
 * Copyright (C) elrobtossohn
 */
public interface IAnimator {
    public GUI setupInventory(IUser var1);

    public void onTick(IUser var1, GUI var2, List<ShopItem> var3, boolean var4);
}