package net.vicemice.lobby.player.inventorys.shop.lottery;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.shop.utils.Rarity;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.scheduler.ScheduledExecution;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/*
 * Class created at 21:53 - 01.04.2020
 * Copyright (C) elrobtossohn
 */
public class OpenTicketInventory {

    public static Map<UUID, ScheduledExecution> scheduledExecution = new HashMap<>();
    public static Map<IUser, Picked> pickedMap = new HashMap<>();
    public static Map<IUser, String> coinsMap = new HashMap<>();
    public static Map<IUser, String> countMap = new HashMap<>();
    public static List<IUser> canNotContinue = new ArrayList<>();
    public static List<UUID> resultOpen = new ArrayList<>();

    public GUI create(IUser user) {
        GUI gui = user.createGUI(user.translate("lottery-ticket-title"), 5);

        gui.fill(0, 54, ItemBuilder.create(Material.PAPER).name(user.translate("lottery-click")).build(), e -> {
            if (!canNotContinue.contains(user) && e.getCurrentItem().getType().equals(Material.PAPER)) {
                int slot = e.getRawSlot();

                int count = 0;
                if (countMap.containsKey(user)) {
                    count = Integer.parseInt(countMap.get(user));
                }
                if (count > 5)
                    return;
                count++;
                countMap.put(user, Integer.toString(count));

                if (!pickedMap.containsKey(user))
                    pickedMap.put(user, new Picked());

                int randomNumber = getRndCoins();
                ItemBuilder itemBuilder = ItemBuilder.create(Material.GOLD_INGOT).name("&6"+randomNumber+" &7Coins");

                if(randomNumber >= 600) {
                    user.playSound(user.getLocation(), Sound.WITHER_DEATH, 10L, 10L);
                    e.getInventory().setItem(slot, itemBuilder.glow(true).build());
                } else {
                    e.getInventory().setItem(slot, itemBuilder.build());
                }
                int coins = 0;
                if (coinsMap.containsKey(user)) {
                    coins = Integer.parseInt(coinsMap.get(user));
                }
                coins = coins + randomNumber;
                coinsMap.put(user, Integer.toString(coins));

                if (pickedMap.get(user).getOne() == 0) {
                    pickedMap.get(user).setOne(randomNumber);
                } else if (pickedMap.get(user).getTwo() == 0) {
                    pickedMap.get(user).setTwo(randomNumber);
                } else if (pickedMap.get(user).getThree() == 0) {
                    pickedMap.get(user).setThree(randomNumber);
                } else if (pickedMap.get(user).getFour() == 0) {
                    pickedMap.get(user).setFour(randomNumber);
                } else if (pickedMap.get(user).getFive() == 0) {
                    pickedMap.get(user).setFive(randomNumber);
                }

                if (count >= 5) {
                    canNotContinue.add(user);
                    countMap.remove(user);
                    coinsMap.remove(user);

                    pickedMap.get(user).setCoins(coins);

                    endLottery(gui);
                }
            }
        });

        return gui;
    }

    public GUI result(IUser user) {
        GUI gui = user.createGUI(user.translate("lottery-ticket-title"), 6);

        gui.fill(0, 9, user.getUIColor());
        gui.fill(45, 54, user.getUIColor());

        gui.setItem(13, ItemBuilder.create(Material.GOLD_INGOT).durability(0).name("§a§l"+pickedMap.get(user).getCoins()+" Coins").glow(true).build());

        Rarity rarityOne = getRarity(pickedMap.get(user).getOne());
        gui.setItem(20, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability(rarityOne.getData()).name(rarityOne.getRarityColor()+user.translate(rarityOne.getKey())).glow((rarityOne == Rarity.LEGENDARY)).build());
        Rarity rarityTwo = getRarity(pickedMap.get(user).getTwo());
        gui.setItem(21, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability(rarityTwo.getData()).name(rarityTwo.getRarityColor()+user.translate(rarityTwo.getKey())).glow((rarityTwo == Rarity.LEGENDARY)).build());
        Rarity rarityThree = getRarity(pickedMap.get(user).getThree());
        gui.setItem(22, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability(rarityThree.getData()).name(rarityThree.getRarityColor()+user.translate(rarityThree.getKey())).glow((rarityThree == Rarity.LEGENDARY)).build());
        Rarity rarityFour = getRarity(pickedMap.get(user).getFour());
        gui.setItem(23, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability(rarityFour.getData()).name(rarityFour.getRarityColor()+user.translate(rarityFour.getKey())).glow((rarityFour == Rarity.LEGENDARY)).build());
        Rarity rarityFive = getRarity(pickedMap.get(user).getFive());
        gui.setItem(24, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability(rarityFive.getData()).name(rarityFive.getRarityColor()+user.translate(rarityFive.getKey())).glow((rarityFive == Rarity.LEGENDARY)).build());

        gui.setItem(29, ItemBuilder.create(Material.GOLD_INGOT).name(((rarityOne == Rarity.LEGENDARY) ? "§6§l"+pickedMap.get(user).getOne() : "§a"+pickedMap.get(user).getOne())).glow((pickedMap.get(user).getOne() > 1000)).build());
        gui.setItem(30, ItemBuilder.create(Material.GOLD_INGOT).name(((rarityTwo == Rarity.LEGENDARY) ? "§6§l"+pickedMap.get(user).getTwo() : "§a"+pickedMap.get(user).getTwo())).glow((pickedMap.get(user).getTwo() > 1000)).build());
        gui.setItem(31, ItemBuilder.create(Material.GOLD_INGOT).name(((rarityThree == Rarity.LEGENDARY) ? "§6§l"+pickedMap.get(user).getThree() : "§a"+pickedMap.get(user).getThree())).glow((pickedMap.get(user).getThree() > 1000)).build());
        gui.setItem(32, ItemBuilder.create(Material.GOLD_INGOT).name(((rarityFour == Rarity.LEGENDARY) ? "§6§l"+pickedMap.get(user).getFour() : "§a"+pickedMap.get(user).getFour())).glow((pickedMap.get(user).getFour() > 1000)).build());
        gui.setItem(33, ItemBuilder.create(Material.GOLD_INGOT).name(((rarityFive == Rarity.LEGENDARY) ? "§6§l"+pickedMap.get(user).getFive() : "§a"+pickedMap.get(user).getFive())).glow((pickedMap.get(user).getFive() > 1000)).build());

        ItemStack paper = new ItemBuilder(Material.PAPER).name(gui.getUser().translate("shop-play-again")).lore(new ArrayList<>(Arrays.asList(gui.getUser().translate((gui.getUser().getStatistic("lobby_lottery_tickets", StatisticPeriod.GLOBAL) == 1 ? "shop-play-again-one-ticket-left" : "shop-play-again-tickets-left"), gui.getUser().getStatistic("lobby_lottery_tickets", StatisticPeriod.GLOBAL)).split("\n")))).build();
        gui.setItem(45, paper, e -> {
            if (gui.getUser().getStatistic("lobby_lottery_tickets", StatisticPeriod.GLOBAL) < 1) return;
            gui.getUser().decreaseStatistic("lobby_lottery_tickets", 1);

            create(user).open();
            resultOpen.remove(gui.getUser().getBukkitPlayer().getUniqueId());
        });

        if(pickedMap.get(user).getCoins() >= 100000) {
            user.playSound(Sound.WITHER_DEATH);
        } else {
            user.playSound(Sound.LEVEL_UP);
        }

        user.increaseCoins(pickedMap.get(user).getCoins());
        user.sendMessage("receive-coins", pickedMap.get(user).getCoins());
        canNotContinue.remove(user);
        pickedMap.remove(user);

        return gui;
    }

    public void endLottery(GUI gui) {
        AtomicInteger left = new AtomicInteger(gui.getHandle().getSize() - 5);
        scheduledExecution.put(gui.getUser().getBukkitPlayer().getUniqueId(), new ScheduledExecution(() -> {
            while (left.get() > 0) {
                if (gui.getUser().getBukkitPlayer().getOpenInventory() == null || gui.getUser().getBukkitPlayer().getOpenInventory() != null && !gui.getUser().getBukkitPlayer().getOpenInventory().getTitle().equals(GoAPI.getUserAPI().translate(gui.getUser().getBukkitPlayer().getUniqueId(), "lottery-ticket-title"))) {
                    if (gui.getUser().getBukkitPlayer().getOpenInventory() != null)
                        gui.getUser().getBukkitPlayer().closeInventory();
                    break;
                }
                final int random = randInt(0, gui.getHandle().getSize() - 1);
                if (gui.getHandle().getItem(random) != null && gui.getHandle().getItem(random).getType() != Material.GOLD_INGOT) {
                    if (gui.getHandle().getItem(random).getType() == Material.STAINED_GLASS_PANE) {
                        continue;
                    }
                    try {
                        Thread.sleep(50L);
                    } catch (InterruptedException ignored) {}
                    gui.getUser().playSound(Sound.CHICKEN_EGG_POP);
                    gui.getHandle().setItem(random, null);
                    left.decrementAndGet();
                    if (left.get() > 0) {
                        continue;
                    }

                    resultOpen.add(gui.getUser().getBukkitPlayer().getUniqueId());
                    gui.getUser().getBukkitPlayer().closeInventory();
                    result(gui.getUser()).open();

                    if (scheduledExecution.containsKey(gui.getUser().getBukkitPlayer().getUniqueId()))
                        scheduledExecution.get(gui.getUser().getBukkitPlayer().getUniqueId()).shutdown();
                }
            }
        }, 1, 1, TimeUnit.SECONDS));
    }

    public static int randInt(final int min, final int max) {
        final Random rand = new Random();
        return rand.nextInt(max - min + 1) + min;
    }

    private Rarity getRarity(int number) {
        if (number == 100000) return Rarity.LEGENDARY;
        if (number >= 10000) return Rarity.EPIC;
        if (number >= 1000) return Rarity.RARE;
        if (number >= 500) return Rarity.UNCOMMON;
        return Rarity.COMMON;
    }

    private int getRndCoins() {
        Random random = new Random();
        int percent = random.nextInt(100);
        if (percent == 100) return 100000;
        int bounds = 100;
        if (percent >= 90) bounds = random.nextInt(1067);
        if (percent >= 60 && percent < 90) bounds = random.nextInt(780);;
        if (percent >= 30 && percent < 60) bounds = random.nextInt(450);
        if (percent < 30) bounds = random.nextInt(290);
        return random.nextInt(bounds);
    }

    public class Picked {
        @Getter
        @Setter
        int one = 0, two = 0, three = 0, four = 0, five = 0, coins = 0;
    }
}
