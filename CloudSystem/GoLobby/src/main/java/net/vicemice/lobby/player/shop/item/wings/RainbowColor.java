package net.vicemice.lobby.player.shop.item.wings;

import org.bukkit.entity.Player;

/*
 * Class created at 21:08 - 15.04.2020
 * Copyright (C) elrobtossohn
 */
public class RainbowColor {
    private Player player;
    private int red;
    private int green;
    private int blue;
    private boolean convert;
    private boolean redcount;
    private boolean greencount;
    private boolean bluecount;

    public RainbowColor(Player player) {
        this.player = player;
        this.red = 0;
        this.green = 0;
        this.blue = 0;
        this.convert = false;
        this.redcount = true;
        this.greencount = false;
        this.bluecount = false;
    }

    public void update() {
        if (this.convert) {
            if (this.redcount) {
                --this.red;
                if (this.red == 0) {
                    this.redcount = false;
                    this.greencount = true;
                }
            } else if (this.greencount) {
                --this.green;
                if (this.green == 0) {
                    this.greencount = false;
                    this.bluecount = true;
                }
            } else if (this.bluecount) {
                --this.blue;
                if (this.blue == 0) {
                    this.bluecount = false;
                    this.convert = false;
                }
            }
        } else if (this.redcount) {
            ++this.red;
            if (this.red == 255) {
                this.redcount = false;
                this.greencount = true;
            }
        } else if (this.greencount) {
            ++this.green;
            if (this.green == 255) {
                this.greencount = false;
                this.bluecount = true;
            }
        } else if (this.bluecount) {
            ++this.blue;
            if (this.blue == 255) {
                this.bluecount = false;
                this.convert = true;
            }
        }
    }

    public Color toColor() {
        return new Color(this.red, this.green, this.blue);
    }
}
