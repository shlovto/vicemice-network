package net.vicemice.lobby.player.inventorys.profile.settings.options;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.lobby.player.inventorys.profile.settings.SettingsInventory;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;

public class ClanSettingsInventory {

    public GUI create(IUser user) {
        GUI gui = user.createGUI(ChatColor.stripColor(user.translate("settings-clan-settings")), 4);

        gui.fill(0, 9, user.getUIColor());
        gui.fill(27, 36, user.getUIColor());

        gui.setItem(8, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 14 ? 1 : 14)).name("§c✗").lore(new ArrayList<>(Arrays.asList(user.translate("gui-return").split("\n")))).build(), e -> {
            new SettingsInventory().create(user).open();
        });

        if (user.getNetworkPlayer().getSettingsPacket().getSetting("advanced-player-settings", Integer.class) == 0) {
            gui.setItem(10, ItemBuilder.create(Material.BOOK_AND_QUILL).name(user.translate("settings-clan-invitations")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-clan-invitations-description").split("\n")))).build());
            gui.setItem(12, ItemBuilder.create(Material.SIGN).name(user.translate("settings-clan-messages")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-clan-messages-description").split("\n")))).build());
            gui.setItem(14, ItemBuilder.create(Material.PAPER).name(user.translate("settings-clan-chat-messages")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-clan-chat-messages-description").split("\n")))).build());
            gui.setItem(16, ItemBuilder.create(Material.ENDER_PEARL).name(user.translate("settings-clan-jump")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-clan-jump-description").split("\n")))).build());

            boolean invitations = (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-invites", Integer.class) == 1);
            gui.setItem(19, ItemBuilder.create(Material.INK_SACK).durability((invitations ? 10 : 8)).name((invitations ? "§a✔" : "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-invites", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("clan-invites", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-invites", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("clan-invites", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
            boolean onlineOfflineMessages = (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-messages", Integer.class) == 1);
            gui.setItem(21, ItemBuilder.create(Material.INK_SACK).durability((onlineOfflineMessages ? 10 : 8)).name((onlineOfflineMessages ? "§a✔" : "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-messages", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("clan-messages", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-messages", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("clan-messages", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
            boolean clanMessages = (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-chat-messages", Integer.class) == 1);
            gui.setItem(23, ItemBuilder.create(Material.INK_SACK).durability((clanMessages ? 10 : 8)).name((clanMessages ? "§a✔" : "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-chat-messages", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("clan-chat-messages", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-chat-messages", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("clan-chat-messages", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
            boolean jump = (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-jump", Integer.class) == 1);
            gui.setItem(25, ItemBuilder.create(Material.INK_SACK).durability((jump ? 10 : 8)).name((jump ? "§a✔" : "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-jump", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("clan-jump", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-jump", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("clan-jump", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
        } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("advanced-player-settings", Integer.class) == 1) {
            gui.setItem(11, ItemBuilder.create(Material.SIGN).name(user.translate("settings-clan-messages")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-clan-messages-description").split("\n")))).build());
            gui.setItem(13, ItemBuilder.create(Material.PAPER).name(user.translate("settings-clan-chat-messages")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-clan-chat-messages-description").split("\n")))).build());
            gui.setItem(15, ItemBuilder.create(Material.ENDER_PEARL).name(user.translate("settings-clan-jump")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-clan-jump-description").split("\n")))).build());

            boolean onlineOfflineMessages = (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-messages", Integer.class) == 1);
            gui.setItem(20, ItemBuilder.create(Material.INK_SACK).durability((onlineOfflineMessages ? 10 : 8)).name((onlineOfflineMessages ? "§a✔" : "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-messages", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("clan-messages", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-messages", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("clan-messages", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
            boolean clanMessages = (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-chat-messages", Integer.class) == 1);
            gui.setItem(22, ItemBuilder.create(Material.INK_SACK).durability((clanMessages ? 10 : 8)).name((clanMessages ? "§a✔" : "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-chat-messages", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("clan-chat-messages", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-chat-messages", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("clan-chat-messages", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
            boolean jump = (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-jump", Integer.class) == 1);
            gui.setItem(24, ItemBuilder.create(Material.INK_SACK).durability((jump ? 10 : 8)).name((jump ? "§a✔" : "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-jump", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("clan-jump", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("clan-jump", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("clan-jump", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
        }

        return gui;
    }
}
