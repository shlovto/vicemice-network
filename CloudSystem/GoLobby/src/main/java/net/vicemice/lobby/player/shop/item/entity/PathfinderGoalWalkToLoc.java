package net.vicemice.lobby.player.shop.item.entity;

import net.minecraft.server.v1_8_R3.EntityInsentient;
import net.minecraft.server.v1_8_R3.Navigation;
import net.minecraft.server.v1_8_R3.PathEntity;
import net.minecraft.server.v1_8_R3.PathfinderGoal;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

/*
 * Class created at 16:50 - 15.04.2020
 * Copyright (C) elrobtossohn
 */
public class PathfinderGoalWalkToLoc extends PathfinderGoal {
    private double speed;
    private int timer = 5;
    private EntityInsentient entity;
    private Player player;
    private Navigation navigation;

    public PathfinderGoalWalkToLoc(EntityInsentient entity, Player player, double speed) {
        this.entity = entity;
        this.player = player;
        this.navigation = (Navigation)this.entity.getNavigation();
        this.speed = speed;
    }

    public void c() {
        PathEntity pathEntity = this.navigation.a(((CraftPlayer)this.player).getHandle());
        this.navigation.a(pathEntity, speed);
    }

    public boolean a() {
        return this.player != null && this.player.isOnline() && !(this.entity.h(((CraftPlayer)this.player).getHandle()) <= 5.0);
    }

    public boolean b() {
        return this.player != null && this.player.isOnline() && !(this.entity.h(((CraftPlayer)this.player).getHandle()) <= 5.0);
    }

    public void d() {
        this.navigation.n();
    }

    public void e() {
        if (--this.timer <= 0) {
            this.timer = 5;
            if (this.entity.h(((CraftPlayer)this.player).getHandle()) > 40.0 && !this.player.isFlying()) {
                this.entity.getBukkitEntity().teleport(this.player);
            }
            if (this.entity.getGoalTarget() == null) {
                this.entity.getNavigation().a(((CraftPlayer)this.player).getHandle(), speed);
            }
        }
    }
}
