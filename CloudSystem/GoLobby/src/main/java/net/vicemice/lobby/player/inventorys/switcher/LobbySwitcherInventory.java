package net.vicemice.lobby.player.inventorys.switcher;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.utils.LobbyServer;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.ConnectPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.players.api.internal.SkullChanger;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.players.utils.Pagination;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class LobbySwitcherInventory {

    public GUI create(IUser user, int page) {
        Pagination<LobbyServer> pagination = new Pagination<>(new ArrayList<>(GoLobby.getPlayerManager().getLobbyServerList().values()), 27);
        GUI gui = user.createGUI(user.translate("lobbyswitcher"), (pagination.getPages() > 1 ? 6 : 5));

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        if (page > 1)
            this.create(user, (page-1)).open();
        if (pagination.getElementsFor(page).isEmpty()) {
            gui.setItem(22, ItemBuilder.create(Material.BARRIER).name(user.translate("lobbyswitcher-no-lobbys")).build());
        }

        AtomicInteger i = new AtomicInteger(9);
        pagination.printPage(page, lobbyServer -> {
            String ironId = "99e42a6d-44c4-4b62-a2f8-eabe9cdf2933";
            String ironValue = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZGYyYzlkYzMxNGJlZmM4ZDEyZjJlODc4NmM5NDM1YmZkMjRkOTExNmZjOTRjNWVjNDVmZDgzYmQyZGM4NGE0ZiJ9fX0=";

            int viewPlayers = Integer.parseInt(lobbyServer.getPlayers());
            if (viewPlayers > 64) {
                viewPlayers = 64;
            }

            String players = "";
            if (Integer.parseInt(lobbyServer.getPlayers()) == 0 || Integer.parseInt(lobbyServer.getPlayers()) > 1) {
                players += user.translate("lobbyswitcher-players", lobbyServer.getPlayers());
            } else {
                players += user.translate("lobbyswitcher-player");
            }

            String[] lore = new String[] {(lobbyServer.isOwn() ? user.translate("lobbyswitcher-here") : user.translate("interaction-connect")), players};

            if (lobbyServer.isOwn()) {
                gui.setItem(i.get(), ItemBuilder.create(SkullChanger.getSkull(user.getBukkitPlayer())).name("§e"+lobbyServer.getName().replace("-", " ")).lore(lore).build());
            } else {
                gui.setItem(i.get(), ItemBuilder.create(SkullChanger.getSkullByTexture(ironId, ironValue)).name("§e"+lobbyServer.getName().replace("-", " ")).lore(lore).build(), e -> {
                    e.getView().close();
                    user.connectServer(lobbyServer.getName());
                });
            }
            i.incrementAndGet();
        });

        if (!pagination.getElementsFor((page-1)).isEmpty()) {
            gui.setItem(48, ItemBuilder.create(GUI.Items.getLeft()).name(user.translate("previous-page")).build(), e -> {
                this.create(user, (page-1)).open();
            });
        }
        if (!pagination.getElementsFor((page+1)).isEmpty()) {
            gui.setItem(50, ItemBuilder.create(GUI.Items.getRight()).name(user.translate("next-page")).build(), e -> {
                this.create(user, (page+1)).open();
            });
        }

        return gui;
    }
}