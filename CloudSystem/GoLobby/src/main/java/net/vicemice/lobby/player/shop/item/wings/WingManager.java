package net.vicemice.lobby.player.shop.item.wings;

import net.vicemice.lobby.GoLobby;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

/*
 * Class created at 21:09 - 15.04.2020
 * Copyright (C) elrobtossohn
 */
public class WingManager {
    private static int taskID;
    public static ArrayList<Player> wingPlayers;
    public static ConcurrentHashMap<Player, EagleWings> wingsByFromPlayer;
    public static ConcurrentHashMap<Player, Wings> wingType;
    public static ConcurrentHashMap<Player, RainbowColor> rainbowWings;

    public static void unequipWings(Player player) {
        wingsByFromPlayer.remove(player);
        wingPlayers.remove(player);
        wingType.remove(player);
        rainbowWings.remove(player);
        if (wingPlayers.size() == 0) {
            WingManager.stopWingUpdater();
        }
    }

    public static void equipWings(Wings wings, Player player) {
        if (wingPlayers.size() == 0) {
            WingManager.startWingUpdater();
        }
        if (wingPlayers.contains(player)) {
            WingManager.unequipWings(player);
        }
        EagleWings eagleWings = new EagleWings(wings.getColor());
        wingPlayers.add(player);
        wingsByFromPlayer.put(player, eagleWings);
        wingType.put(player, wings);
    }

    public static void startWingUpdater() {
        taskID = Bukkit.getScheduler().scheduleAsyncRepeatingTask(GoLobby.getInstance(), () -> {
            for (Player all : wingPlayers) {
                Wings wings = wingType.get(all);
                if (wings == Wings.RAINBOW) {
                    int index = 0;
                    int x = 0;
                    java.awt.Color rainbow = WingManager.rainbowEffect((long)index + (long)x * 20000000L, 1.0f);
                    EagleWings ew = new EagleWings(new Color(rainbow.getRed(), rainbow.getGreen(), rainbow.getBlue()));
                    for (Player onlinePlayers : Bukkit.getOnlinePlayers()) {
                        if (!onlinePlayers.canSee(all)) continue;
                        ew.sendFly(all, onlinePlayers);
                    }
                    continue;
                }
                EagleWings ew = wingsByFromPlayer.get(all);
                for (Player onlinePlayers : Bukkit.getOnlinePlayers()) {
                    if (!onlinePlayers.canSee(all)) continue;
                    ew.sendFly(all, onlinePlayers);
                }
            }
        }, 0L, 3L);
    }

    public static void stopWingUpdater() {
        Bukkit.getScheduler().cancelTask(taskID);
    }

    private static java.awt.Color rainbowEffect(long offset, float fade) {
        float hue = (float)(System.nanoTime() + offset) / 1.0E10f % 1.0f;
        long color = Long.parseLong(Integer.toHexString(java.awt.Color.HSBtoRGB(hue, 1.0f, 1.0f)), 16);
        java.awt.Color c = new java.awt.Color((int)color);
        return new java.awt.Color((float)c.getRed() / 255.0f * fade, (float)c.getGreen() / 255.0f * fade, (float)c.getBlue() / 255.0f * fade, (float)c.getAlpha() / 255.0f);
    }

    static {
        wingPlayers = new ArrayList<>();
        wingsByFromPlayer = new ConcurrentHashMap<>();
        wingType = new ConcurrentHashMap<>();
        rainbowWings = new ConcurrentHashMap<>();
    }

    public static enum Wings {
        RED(new Color(255, 0, 0)),
        BLUE(new Color(0, 102, 255)),
        GREEN(new Color(0, 153, 51)),
        YELLOW(new Color(255, 255, 0)),
        PINK(new Color(255, 51, 204)),
        PURPLE(new Color(153, 0, 204)),
        ORANGE(new Color(255, 153, 0)),
        BLACK(new Color(0, 0, 0)),
        LIGHT_GREEN(new Color(0, 255, 0)),
        LIGHT_BLUE(new Color(0, 255, 255)),
        RAINBOW(new Color(0, 0, 0));

        private Color color;

        private Wings(Color color) {
            this.color = color;
        }

        public Color getColor() {
            return this.color;
        }
    }
}