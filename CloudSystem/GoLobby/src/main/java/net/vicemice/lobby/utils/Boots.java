package net.vicemice.lobby.utils;

import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.particles.Particle;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import java.util.Random;

/*
 * Class created at 21:10 - 30.04.2020
 * Copyright (C) elrobtossohn
 */
public abstract class Boots {
    protected Random random = new Random();
    private Particle particle, secParticle;
    private Material drop;
    private float particleSpeed;
    private int count, actionId;

    public Boots(Particle particle, Particle secParticle, Material drop, float particleSpeed, int count, int actionId) {
        this.particle = particle;
        this.secParticle = secParticle;
        this.drop = drop;
        this.particleSpeed = particleSpeed;
        this.count = count;
        this.actionId = actionId;
    }

    public abstract void onPlayerSneak(IUser var1, boolean var2);

    public Random getRandom() {
        return this.random;
    }

    public Particle getParticle() {
        return particle;
    }

    public Particle getSecParticle() {
        return secParticle;
    }

    public Material getDrop() {
        return drop;
    }

    public float getParticleSpeed() {
        return this.particleSpeed;
    }

    public int getCount() {
        return this.count;
    }

    public int getActionId() {
        return actionId;
    }

    public void setParticle(Particle particle) {
        this.particle = particle;
    }

    public void setSecParticle(Particle secParticle) {
        this.secParticle = secParticle;
    }

    public void setDrop(Material drop) {
        this.drop = drop;
    }

    public void setParticleSpeed(float particleSpeed) {
        this.particleSpeed = particleSpeed;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }
}