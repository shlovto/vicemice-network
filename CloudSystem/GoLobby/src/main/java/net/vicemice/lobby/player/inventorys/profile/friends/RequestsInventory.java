package net.vicemice.lobby.player.inventorys.profile.friends;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.player.inventorys.profile.DefaultProfile;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.CommandPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.CommandType;
import net.vicemice.hector.utils.players.lobby.LobbyRequestData;
import net.vicemice.hector.utils.players.utils.Pagination;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import java.util.concurrent.atomic.AtomicInteger;

public class RequestsInventory {

    public GUI create(IUser user, int page) {
        Pagination<LobbyRequestData> pagination = new Pagination<>(user.getNetworkPlayer().getRequests(), 27);
        GUI gui = new DefaultProfile().create(user, user.translate("friends-request-title"), (pagination.getPages() > 1 ? 6 : 5));

        AtomicInteger i = new AtomicInteger(9);
        pagination.printPage(page, lobbyRequestData -> {
            gui.setItem(i.get(), ItemBuilder.create(Material.SKULL_ITEM).name(lobbyRequestData.getRank().getRankColor()+lobbyRequestData.getName()).lore(user.translate("friends-request-accept"), user.translate("friends-request-decline")).build(), e -> {
                if (e.getAction() == InventoryAction.PICKUP_ALL) {
                    GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.FRIENDS, new String[]{"accept", lobbyRequestData.getName()})), API.PacketReceiver.SLAVE);
                } else if (e.getAction() == InventoryAction.PICKUP_HALF) {
                    GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.FRIENDS, new String[]{"deny", lobbyRequestData.getName()})), API.PacketReceiver.SLAVE);
                }
            });
            i.set(i.get() + 1);
        });

        if (!pagination.getElementsFor((page-1)).isEmpty()) {
            gui.setItem(48, ItemBuilder.create(GUI.Items.getLeft()).name(user.translate("previous-page")).build(), e -> {
                this.create(user, (page-1)).open();
            });
        }
        if (!pagination.getElementsFor((page+1)).isEmpty()) {
            gui.setItem(50, ItemBuilder.create(GUI.Items.getRight()).name(user.translate("next-page")).build(), e -> {
                this.create(user, (page+1)).open();
            });
        }

        return gui;
    }
}