package net.vicemice.lobby.player.inventorys.profile.replays;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.inventorys.profile.DefaultProfile;
import net.vicemice.lobby.player.utils.InventoryType;
import net.vicemice.hector.packets.types.player.replay.PacketReplayEntries;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.replay.type.ReplayActionInventory;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.TimeManager;
import net.vicemice.hector.utils.players.utils.Pagination;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import static org.bukkit.Material.BARRIER;
import static org.bukkit.Material.BOOK;

/*
 * Class created at 15:12 - 17.04.2020
 * Copyright (C) elrobtossohn
 */
public class ReplayInventory {

    public GUI create(IUser user, int page, boolean recent) {
        GUI gui = new DefaultProfile().create(user, InventoryType.REPLAYS_RECENT);
        
        Pagination<PacketReplayEntries.PacketReplayEntry> pagination = new Pagination<>((recent ? user.getNetworkPlayer().getRecentReplays() : user.getNetworkPlayer().getSavedReplays()), 27);
        AtomicInteger slot = new AtomicInteger(9);
        AtomicInteger id = new AtomicInteger(1+(27*(page-1)));
        pagination.printPage(page, replayEntry -> {
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            Date date = new Date(replayEntry.getTimestamp());

            gui.setItem(slot.getAndSet((slot.get()+1)), ItemBuilder.create(BOOK).name(user.translate("replay-single", id.getAndSet((slot.get()+1)))).lore(new ArrayList<>(Arrays.asList((user.translate("gui-profile-replay-description", dateFormat.format(date), timeFormat.format(date), replayEntry.getGameType()+" "+replayEntry.getMapType(), replayEntry.getMap(), GoAPI.getTimeManager().getTimeFromSeconds(GoServer.getLocaleManager(), user.getLocale(), replayEntry.getGameLength(), new ArrayList<>(Arrays.asList(TimeManager.TimeNames.HOURS, TimeManager.TimeNames.MINUTES, TimeManager.TimeNames.SECONDS)))).split("\n"))))).build(), e -> {
                new ReplayActionInventory().create(user, replayEntry, gui).open();
            });
        });

        if (pagination.getElementsFor(page).isEmpty()) {
            gui.setItem(22, ItemBuilder.create(BARRIER).name(user.translate((recent ? "replays-no-games" : "replays-no-saves"))).build());
        }

        gui.setItem(50, ItemBuilder.create(Material.BOOK_AND_QUILL).name(user.translate("gui-replay-favorites")).lore(new ArrayList<>(Arrays.asList((user.translate("gui-replay-favorites-description-self").split("\n"))))).glow(!recent).build(), e -> {
            if (!recent) return;
            this.create(user, 1, false).open();
        });
        gui.setItem(51, ItemBuilder.create(Material.EYE_OF_ENDER).name(user.translate("gui-replay-new-replays")).lore(new ArrayList<>(Arrays.asList((user.translate("gui-replay-new-replays-description-self").split("\n"))))).glow(recent).build(), e -> {
            if (recent) return;
            this.create(user, 1, true).open();
        });

        gui.setItem(52, ItemBuilder.create(GUI.Items.getLeft()).name(user.translate("previous-page")).build(), e -> {
            if (!pagination.getElementsFor((page-1)).isEmpty()) return;
            this.create(user, (page-1), recent).open();
        });
        gui.setItem(53, ItemBuilder.create(GUI.Items.getRight()).name(user.translate("next-page")).build(), e -> {
            if (!pagination.getElementsFor((page+1)).isEmpty()) return;
            this.create(user, (page+1), recent).open();
        });

        return gui;
    }
}
