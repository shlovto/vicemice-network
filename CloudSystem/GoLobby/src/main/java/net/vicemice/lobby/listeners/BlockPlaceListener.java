package net.vicemice.lobby.listeners;

import net.vicemice.lobby.GoLobby;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlaceListener implements Listener {

    @EventHandler
    public void onPlace(BlockPlaceEvent event) {
        if (!GoLobby.getPlayerManager().getBuilders().contains(event.getPlayer().getUniqueId())) event.setCancelled(true);
    }
}
