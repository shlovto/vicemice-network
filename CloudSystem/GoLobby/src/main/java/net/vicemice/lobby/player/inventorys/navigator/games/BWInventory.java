package net.vicemice.lobby.player.inventorys.navigator.games;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.inventorys.navigator.NavigatorInventory;
import net.vicemice.lobby.player.inventorys.profile.DefaultProfile;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.CommandPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.CommandType;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;

public class BWInventory {

    public GUI create(IUser user) {
        GUI gui = new DefaultProfile().create(user, user.translate("navigator-title"), 5);

        gui.setItem(20, ItemBuilder.create(Material.INK_SACK).durability(8).name(user.translate("navigator-unranked")).lore(new ArrayList<>(Arrays.asList(user.translate("navigator-unranked-description", "0").split("\n")))).build(), e -> {
            GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.PLAY, new String[]{"BedWars"})), API.PacketReceiver.SLAVE);
        });

        gui.setItem(22, ItemBuilder.create(Material.BED).name(user.translate("navigator-bedwars")).lore(new ArrayList<>(Arrays.asList(user.translate("navigator-bedwars-legacy-description", (GoLobby.getPlayerManager().getGamePlayers("bedwars") == 1 ? "1 "+user.translate("player") : GoLobby.getPlayerManager().getGamePlayers("bedwars")+" "+user.translate("players"))).split("\n")))).build(), e -> {
            new NavigatorInventory().create(user).open();
        });

        gui.setItem(24, ItemBuilder.create(Material.INK_SACK).durability(6).name(user.translate("navigator-ranked")).lore(new ArrayList<>(Arrays.asList(user.translate("navigator-ranked-description", "0").split("\n")))).build(), e -> {
            GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.PLAY, new String[]{"EloBedWars"})), API.PacketReceiver.SLAVE);
        });

        return gui;
    }
}