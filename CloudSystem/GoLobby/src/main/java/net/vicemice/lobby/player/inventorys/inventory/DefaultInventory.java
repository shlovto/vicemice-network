package net.vicemice.lobby.player.inventorys.inventory;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.api.util.MinecraftUtil;
import net.vicemice.hector.utils.math.MathUtils;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.lobby.player.inventorys.inventory.type.BootsInventory;
import net.vicemice.lobby.player.inventorys.inventory.type.ParticleEffectsInventory;
import net.vicemice.lobby.player.inventorys.inventory.type.PetsInventory;
import net.vicemice.lobby.player.shop.item.PlayerShopItem;
import net.vicemice.lobby.player.shop.item.pets.Pet;
import net.vicemice.lobby.player.shop.item.pets.PetUtils;
import net.vicemice.lobby.player.shop.item.wings.WingManager;
import net.vicemice.lobby.player.shop.utils.Category;
import net.vicemice.lobby.player.shop.utils.Found;
import net.vicemice.lobby.player.utils.InventoryType;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.ItemCreator;
import net.vicemice.hector.utils.players.api.anvil.AnvilGUI;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.players.api.particles.Particle;
import net.vicemice.lobby.utils.Boots;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

/*
 * Class created at 20:29 - 01.04.2020
 * Copyright (C) elrobtossohn
 */
public class DefaultInventory {

    public GUI create(IUser user, InventoryType inventoryGUI) {
        GUI gui = user.createGUI("Generate GUI", 6);
        if (inventoryGUI == InventoryType.NONE) {
            return this.categorys(user);
        } else if (inventoryGUI == InventoryType.PETS) {
            gui = user.createGUI(ChatColor.stripColor(user.translate("shop-pets")), 6);
        } else if (inventoryGUI == InventoryType.MOUNTS) {
            gui = user.createGUI(ChatColor.stripColor(user.translate("shop-mounts")), 6);
        } else if (inventoryGUI == InventoryType.BOOTS) {
            gui = user.createGUI(ChatColor.stripColor(user.translate("shop-boots")), 6);
        } else if (inventoryGUI == InventoryType.PARTICLE_EFFECTS) {
            gui = user.createGUI(ChatColor.stripColor(user.translate("shop-particle-effects")), 6);
        }

        GoLobby.getPlayerManager().getPlayer(user.getUniqueId()).setLastInventoryType(inventoryGUI);

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        if (inventoryGUI == InventoryType.PETS) {
            gui.setItem(49, ItemBuilder.create(Material.MONSTER_EGG).name(user.translate("shop-pets")).build(), e -> {
                this.categorys(user).open();
            });
        } else if (inventoryGUI == InventoryType.MOUNTS) {
            gui.setItem(49, ItemBuilder.create(Material.SADDLE).name(user.translate("shop-mounts")).build(), e -> {
                this.categorys(user).open();
            });
        } else if (inventoryGUI == InventoryType.BOOTS) {
            gui.setItem(49, ItemBuilder.create(Material.LEATHER_BOOTS).name(user.translate("shop-boots")).build(), e -> {
                this.categorys(user).open();
            });
        } else if (inventoryGUI == InventoryType.PARTICLE_EFFECTS) {
            gui.setItem(49, ItemBuilder.create(Material.REDSTONE).name(user.translate("shop-particle-effects")).build(), e -> {
                this.categorys(user).open();
            });
        }

        return gui;
    }

    public GUI categorys(IUser user) {
        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(user.getUniqueId());
        GUI gui = user.createGUI(user.translate("shop-choose-category"), 5);

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        gui.setItem(19, ItemBuilder.create(Material.MONSTER_EGG).name(user.translate("shop-pets")).glow((playerData.getLastInventoryType() == InventoryType.PETS)).lore((playerData.getLastInventoryType() == InventoryType.PETS ? user.translate("already-selected") : user.translate("category-select"))).build(), e -> {
            new PetsInventory().create(user, 1).open();
        });
        gui.setItem(21, ItemBuilder.create(Material.SADDLE).name(user.translate("shop-mounts")).glow((playerData.getLastInventoryType() == InventoryType.MOUNTS)).lore((playerData.getLastInventoryType() == InventoryType.MOUNTS ? user.translate("already-selected") : user.translate("category-select"))).build(), e -> {
            this.create(user, InventoryType.MOUNTS).open();
        });
        gui.setItem(23, ItemBuilder.create(Material.LEATHER_BOOTS).name(user.translate("shop-boots")).glow((playerData.getLastInventoryType() == InventoryType.BOOTS)).lore((playerData.getLastInventoryType() == InventoryType.BOOTS ? user.translate("already-selected") : user.translate("category-select"))).build(), e -> {
            new BootsInventory().create(user, 1).open();
        });
        gui.setItem(25, ItemBuilder.create(Material.REDSTONE).name(user.translate( "shop-particle-effects")).glow((playerData.getLastInventoryType() == InventoryType.PARTICLE_EFFECTS)).lore((playerData.getLastInventoryType() == InventoryType.PARTICLE_EFFECTS ? user.translate("already-selected") : user.translate("category-select"))).build(), e -> {
            new ParticleEffectsInventory().create(user, 1).open();
        });

        return gui;
    }

    public GUI generateView(IUser user, ItemStack item, Category category, String itemName, Integer itemId) {
        GUI inventory = user.createGUI(ChatColor.stripColor(itemName), 3);
        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(user);
        PlayerShopItem playerShopItem = playerData.getItems().get(category).get(itemId);

        if (playerShopItem.getShopItem().isSellAble() && playerShopItem.getFound() == Found.CHEST_OPENING) {
            inventory.setItem(10, new ItemCreator(new ItemStack(Material.BOOK_AND_QUILL)).displayName("§c" + user.translate("sell")).lore(new ArrayList<>(Arrays.asList(user.translate("shop-item-sell-description", playerShopItem.getShopItem().getPrice()).split("\n")))).build(), e -> {
                generateSellConfirm(user, item, category, itemName, playerShopItem.getId(), itemId).open();
            });
        }

        inventory.setItem(13, item);

        inventory.setItem(16, ItemBuilder.create(Material.INK_SACK).durability(10).name("§a" + user.translate("use")).build(), e -> {
            if (category == Category.BOOTS) {
                e.getView().close();
                playerData.setActiveBootsId(playerShopItem.getId());

                Particle particle = Particle.valueOf(playerShopItem.getShopItem().getValues().get("effectId"));
                Particle secParticle = null;
                if (playerShopItem.getShopItem().getValues().containsKey("secEffectId")) {
                    secParticle = Particle.valueOf(playerShopItem.getShopItem().getValues().get("secEffectId"));
                }
                Material drop = null;
                if (playerShopItem.getShopItem().getValues().containsKey("dropId")) {
                    drop = Material.valueOf(playerShopItem.getShopItem().getValues().get("dropId"));
                }
                float particleSpeed = (MathUtils.isFloat(playerShopItem.getShopItem().getValues().get("particleSpeed")) ? Float.parseFloat(playerShopItem.getShopItem().getValues().get("particleSpeed")) : 0.0f);
                int count = (MathUtils.isInt(playerShopItem.getShopItem().getValues().get("particleCounts")) ? Integer.parseInt(playerShopItem.getShopItem().getValues().get("particleCounts")) : 1);
                int actionId = (MathUtils.isInt(playerShopItem.getShopItem().getValues().get("actionId")) ? Integer.parseInt(playerShopItem.getShopItem().getValues().get("actionId")) : -1);

                Boots boots = new Boots(particle, secParticle, drop, particleSpeed, count, actionId) {
                    @Override
                    public void onPlayerSneak(IUser user, boolean sneaking) {
                        if (getActionId() == 0) {
                            if (sneaking) {
                                Location loc = user.getEyeLocation().clone().add(0.0, 0.5, 0.0);
                                for (int i = 0; i < 5; ++i) {
                                    double x = this.random.nextBoolean() ? this.random.nextDouble() : this.random.nextDouble() * -1.0;
                                    double y = this.random.nextBoolean() ? this.random.nextDouble() : this.random.nextDouble() * -1.0;
                                    double z = this.random.nextBoolean() ? this.random.nextDouble() : this.random.nextDouble() * -1.0;
                                    Particle.WATER_DROPLET.builder().ofAmount(1).atSpeed(0.0f).at(loc.clone().add(x, y, z)).show();
                                }
                            }
                        } else if (getActionId() == 1) {
                            if (sneaking) {
                                user.getBukkitPlayer().setFireTicks(Integer.MAX_VALUE);
                                user.getBukkitPlayer().setNoDamageTicks(20);
                            } else {
                                user.getBukkitPlayer().setFireTicks(0);
                            }
                        } else if (getActionId() == 2) {
                            if (sneaking) {
                                Particle.SMOKE.builder().atSpeed(0.0f).ofAmount(3).at(user.getLocation()).show();
                                Particle.SMALL_SMOKE.builder().atSpeed(0.0f).ofAmount(3).at(user.getLocation()).show();
                            }
                        } else if (getActionId() == 3) {
                            if (sneaking) {
                                Particle.HEART.builder().ofAmount(1).atSpeed(0.0f).at(user.getEyeLocation().clone().add(0.0, 0.5, 0.0)).show();
                            }
                        } else if (getActionId() == 4) {
                            if (sneaking) {
                                user.playSound(user.getEyeLocation(), Sound.NOTE_PIANO, 1.0f, 1.0f);
                            }
                        } else if (getActionId() == 5) {
                            if (sneaking) {
                                user.playSound(user.getEyeLocation(), Sound.DIG_STONE, 1.0f, 1.0f);
                                //TODO: Fix this
                                /*for (int i = 0; i < 3; ++i) {
                                    Particle.BLOCK_BREAK.setBlockId(Integer.valueOf(79)).setMaterialId(Integer.valueOf(0)).broadcastEffect(player.getEyeLocation(), 0.5f, 5);
                                }*/
                            }
                        } else if (getActionId() == 6) {
                            if (sneaking) {
                                Particle.ANGRY_VILLAGER.builder().ofAmount(1).atSpeed(0.0f).at(user.getEyeLocation().clone().add(0.0, 0.25, 0.0)).show();
                                user.playSound(user.getEyeLocation(), Sound.VILLAGER_NO, 1.0f, 1.0f);
                            }
                        } else if (getActionId() == 7) {
                            if (sneaking) {
                                Location loc = user.getEyeLocation().clone().add(0.0, 0.5, 0.0);
                                for (int i = 0; i < 5; ++i) {
                                    double x = this.random.nextBoolean() ? this.random.nextDouble() : this.random.nextDouble() * -1.0;
                                    double y = this.random.nextBoolean() ? this.random.nextDouble() : this.random.nextDouble() * -1.0;
                                    double z = this.random.nextBoolean() ? this.random.nextDouble() : this.random.nextDouble() * -1.0;
                                    Particle.LAVA_DRIP.builder().ofAmount(1).atSpeed(0.0f).at(loc.clone().add(x, y, z)).show();
                                }
                            }
                        } else if (getActionId() == 8) {
                            if (sneaking) {
                                user.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 0));
                            } else {
                                user.getBukkitPlayer().removePotionEffect(PotionEffectType.INVISIBILITY);
                            }
                        } else if (getActionId() == 9) {
                            if (sneaking && MinecraftUtil.isNotInAir(user)) {
                                user.getBukkitPlayer().setVelocity(new Vector(0.0, 6.0, 0.0));
                            }
                        } else if (getActionId() == 10) {
                            if (sneaking && MinecraftUtil.isNotInAir(user)) {
                                user.getBukkitPlayer().setExp((float) ((double) user.getBukkitPlayer().getExp() + 0.1));
                                if (user.getBukkitPlayer().getExp() >= 1.0f) {
                                    user.playSound(user.getEyeLocation(), Sound.EXPLODE, 0.5f, 1.0f);
                                    Vector dir = user.getEyeLocation().getDirection().normalize();
                                    dir.multiply(8);
                                    dir.setY(1.5);
                                    user.getBukkitPlayer().setVelocity(dir);
                                    user.getBukkitPlayer().setExp(0.0f);
                                }
                            } else {
                                user.getBukkitPlayer().setExp(0.0f);
                            }
                        } else if (getActionId() == 11) {
                            if (sneaking) {
                                user.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 60));
                            } else {
                                user.getBukkitPlayer().removePotionEffect(PotionEffectType.SPEED);
                            }
                        } else if (getActionId() == 12) {
                            if (sneaking) {
                                user.getBukkitPlayer().setExp((float) ((double) user.getBukkitPlayer().getExp() + 0.1));
                                if (user.getBukkitPlayer().getExp() >= 1.0f) {
                                    user.playSound(user.getEyeLocation(), Sound.ENDERMAN_TELEPORT, 0.5f, 1.0f);
                                    Vector vector = user.getEyeLocation().getDirection().normalize().clone();
                                    vector.multiply(200);
                                    user.getBukkitPlayer().setVelocity(vector);
                                    Bukkit.getScheduler().runTaskLater(GoLobby.getInstance(), () -> user.getBukkitPlayer().setVelocity(new Vector(0.0, 0.0, 0.0)), 3L);
                                    user.getBukkitPlayer().setExp(0.0f);
                                }
                            } else {
                                user.getBukkitPlayer().setExp(0.0f);
                            }
                        } else if (getActionId() == 13) {
                            if (sneaking && !MinecraftUtil.isNotInAir(user)) {
                                Vector dir = user.getEyeLocation().getDirection().normalize();
                                dir.multiply(10);
                                dir.setY(1.5);
                                user.getBukkitPlayer().setVelocity(dir);
                            }
                        } else if (getActionId() == 14) {
                            if (sneaking) {
                                user.getBukkitPlayer().setExp(1.0f);
                                Vector dir = user.getEyeLocation().getDirection().normalize();
                                dir.setY(0.25);
                                user.getBukkitPlayer().setVelocity(dir);
                            } else {
                                user.getBukkitPlayer().setExp(0.0f);
                            }
                        } else if (getActionId() == 15) {
                            if (sneaking) {
                                user.getBukkitPlayer().setExp(1.0f);
                                Vector dir = user.getEyeLocation().getDirection().normalize();
                                dir.setY(0.25);
                                user.getBukkitPlayer().setVelocity(dir);
                                Location loc = user.getLocation();
                                ItemStack lehm = new ItemStack(Material.STAINED_CLAY, 1, (short) this.random.nextInt(16));
                                double x = this.random.nextDouble() - 0.5;
                                double z = this.random.nextDouble() - 0.5;
                                Item entity = loc.getWorld().dropItem(loc.clone().add(x, 0.0, z), lehm);
                                GoLobby.getPlayerManager().getShopManager().getEntityClearupRunnable().getEntitiesToCleanup().put(entity, System.currentTimeMillis());
                            } else {
                                user.getBukkitPlayer().setExp(0.0f);
                            }
                        } else if (getActionId() == 95215646) {
                            if (sneaking && user.getBukkitPlayer().isFlying()) {
                                if (playerData.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) != 1 && playerData.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) != 2) {
                                    user.getBukkitPlayer().setAllowFlight(false);
                                }
                                user.getBukkitPlayer().setFlying(false);
                            }
                        }
                    }
                };

                playerData.setBoots(boots);

                user.getBukkitPlayer().getInventory().setBoots(playerShopItem.getShopItem().getDefaultItem(user));
                user.sendMessage("shop-boots-wearing", user.translate(playerShopItem.getShopItem().getLanguageKey()));
            } else if (category == Category.PARTICLE_EFFECTS) {
                if (playerShopItem.getShopItem().getValues().get("type").equals("wings")) {
                    e.getView().close();
                    WingManager.Wings wings = WingManager.Wings.valueOf(playerShopItem.getShopItem().getValues().get("wings"));

                    if (wings != null) {
                        playerData.activateWings(wings, playerShopItem.getId());
                        user.sendMessage("shop-effect-activated", user.translate(playerShopItem.getShopItem().getLanguageKey()));
                    }
                } else {
                    e.getView().close();
                    playerData.setParticle(Particle.valueOf(playerShopItem.getShopItem().getValues().get("effectid")));

                    user.sendMessage("shop-effect-activated", user.translate(playerShopItem.getShopItem().getLanguageKey()));
                }
            } /*else if (category == Category.OTHER) {
                if (playerShopItem.getShopItem().getValues().get("type").equals("booster")) {
                    e.getView().close();
                    //TODO player.sendMessage(GoLobby.getPlayerManager().getLanguageManager().getLanguages().get(playerData.getLanguage()).getYI_OTHER_BOOSTER_ONLY_GAME().replace("&", "§").replace("%PREFIX%", GoAPI.getShopPrefix()));
                    return;
                } else if (playerShopItem.getShopItem().getValues().get("type").equals("rank")) {
                    e.getView().close();
                    GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_SHOP_RANK, new PlayerShopRankPacket(player.getUniqueId(), playerShopItem.getShopItem().getValues().get("timeUnit"), Rank.valueOf(playerShopItem.getShopItem().getValues().get("rank")), Long.parseLong(playerShopItem.getShopItem().getValues().get("end")))), API.PacketReceiver.SLAVE);

                    playerData.getBuyedData().remove(playerShopItem.getId());
                    playerData.getItems().get(playerShopItem.getShopItem().getCategory()).remove(itemId);
                    playerShopItem.delete();
                    return;
                } else if (playerShopItem.getShopItem().getValues().get("type").equals("coinsbomb")) {
                    e.getView().close();
                    GoLobby.getPlayerManager().getShopManager().createBomb(player.getName(), player.getLocation());

                    playerData.getBuyedData().remove(playerShopItem.getId());
                    playerData.getItems().get(playerShopItem.getShopItem().getCategory()).remove(itemId);
                    playerShopItem.delete();

                    for (Player online : Bukkit.getOnlinePlayers()) {
                        TextComponent tc = new TextComponent();
                        tc.setText(user.translate("player-throw-coinsbomb", player.getName()));
                        tc.setBold(true);
                        tc.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND,"/cb " + player.getName()));
                        online.spigot().sendMessage(tc);
                    }

                    return;
                }

                e.getView().close();
                player.sendMessage(user.translate("shop-item-can-not-use"));
            }*/
        });

        return inventory;
    }

    public GUI generateViewPets(IUser user, ItemStack item, Category category, String itemName, Integer itemId) {
        GUI inventory = user.createGUI(ChatColor.stripColor(itemName), 3);
        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(user.getUniqueId());
        PlayerShopItem playerShopItem = playerData.getItems().get(category).get(itemId);

        if (playerShopItem.getShopItem().isSellAble() && playerShopItem.getFound() == Found.CHEST_OPENING) {
            inventory.setItem(10, new ItemCreator(new ItemStack(Material.BOOK_AND_QUILL)).displayName("§c" + user.translate("sell")).lore(new ArrayList<>(Arrays.asList(user.translate("shop-item-sell-description", GoAPI.formatDecimal(playerData.getLocale(), playerShopItem.getShopItem().getPrice())).split("\n")))).build(), e -> {
                generateSellConfirm(user, item, category, itemName, playerShopItem.getId(), itemId).open();
            });
        }

        inventory.setItem(12, item);

        inventory.setItem(14, new ItemCreator(new ItemStack(Material.NAME_TAG)).displayName(user.translate("shop-item-new-name")).lore(new ArrayList<>(Arrays.asList((playerData.getNetworkPlayer().getCoins() >= 2000 ? user.translate("shop-item-new-name-description", GoAPI.formatDecimal(playerData.getLocale(), 2000)) : user.translate("shop-item-new-name-description-missing-coins", GoAPI.formatDecimal(playerData.getLocale(), (2000 - user.getCoins())))).split("\n")))).build(), e -> {
            if (category == Category.PETS) {
                new AnvilGUI.Builder()
                        .onClose(p2 -> {                   //called when the inventory is closing
                            //p2.sendMessage("You closed the inventory.");
                        })
                        .onComplete((p2, text) -> {        //called when the inventory output slot is clicked
                            if (!(user.getCoins() >= 2000)) {
                                p2.sendMessage(user.translate("shop-need-more-coins-rename", GoAPI.formatDecimal(Locale.GERMANY, (2000 - user.getCoins()))));
                                return AnvilGUI.Response.text(null);
                            }

                            AtomicBoolean enough = new AtomicBoolean(false);

                            user.getCoins((o -> {
                                long coins = o;
                                if (!(coins >= 2000)) {
                                    p2.sendMessage(user.translate("shop-need-more-coins-rename", GoAPI.formatDecimal(Locale.GERMANY, (2000 - user.getCoins()))));
                                    enough.set(true);
                                }
                            }));

                            if (enough.get()) {
                                return AnvilGUI.Response.close();
                            }

                            user.decreaseCoins(2000);

                            if (playerData.getPetNames().get(playerShopItem.getId()) != null) {
                                playerData.getPetNames().remove(playerShopItem.getId());
                            }
                            playerData.getPetNames().put(playerShopItem.getId(), "§f" + text.replace("&", "§"));
                            p2.sendMessage(user.translate("shop-item-name-changed", "§f" + text.replace("&", "§")));

                            if (PetUtils.pets.containsKey(user) && PetUtils.pets.get(user).getItemId().equalsIgnoreCase(playerShopItem.getId())) {
                                PetUtils.entities.get(user).setCustomName("§f" + text.replace("&", "§"));
                            }

                            playerData.savePetSettings();

                            return AnvilGUI.Response.text(text);
                        })
                        //.preventClose()                        //prevents the inventory from being closed
                        .text(itemName)  //sets the text the GUI should start with
                        .plugin(GoLobby.getInstance())              //set the plugin instance
                        .open(user.getBukkitPlayer());                       //opens the GUI for the player provided
            }
        });

        inventory.setItem(16, ItemBuilder.create(Material.INK_SACK).durability(10).name("§a" + user.translate("use")).build(), e -> {
            if (category == Category.PETS) {
                e.getView().close();

                PetUtils.deletePet(user);

                String name = user.translate(playerShopItem.getShopItem().getLanguageKey());

                if (playerData.getPetNames().containsKey(playerShopItem.getId())) {
                    name = playerData.getPetNames().get(playerShopItem.getId());
                }

                Pet pet = new Pet(EntityType.valueOf(playerShopItem.getShopItem().getValues().get("entity")), name, user.getUniqueId(), playerShopItem.getId());

                if (!PetUtils.spawnPet(user, pet)) {
                    user.sendMessage("shop-pet-spawn-error");
                }
            } else {
                user.sendMessage("§cUnsupported operation");
            }
        });

        return inventory;
    }

    public GUI generateSellConfirm(IUser user, ItemStack item, Category category, String itemName, String playerItemId, Integer itemId) {
        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(user);
        GUI inventory = user.createGUI(user.translate("shop-sell-confirm", ChatColor.stripColor(itemName)), 3);
        PlayerShopItem playerShopItem = playerData.getItems().get(category).get(itemId);

        if (playerShopItem.getShopItem().isSellAble()) {
            inventory.setItem(10, ItemBuilder.create(Material.INK_SACK).durability(10).name("§a" + user.translate("confirm")).build(), e -> {
                user.increaseCoins(Integer.parseInt(String.valueOf(playerShopItem.getShopItem().getPrice())));
                playerShopItem.delete();
                playerData.getPetNames().remove(playerShopItem.getId());
                playerData.getBuyedData().remove(playerShopItem.getId());
                playerData.getItems().get(playerShopItem.getShopItem().getCategory()).remove(itemId);

                if (category == Category.PETS) {
                    new PetsInventory().create(user, 1).open();
                    if (PetUtils.pets.containsKey(user) && PetUtils.pets.get(user).getItemId().equalsIgnoreCase(playerItemId)) {
                        PetUtils.deletePet(user);
                    }
                } else if (category == Category.BOOTS) {
                    new BootsInventory().create(user, 1).open();
                    if (playerData.getActiveBootsId() != null && playerData.getActiveBootsId().equalsIgnoreCase(playerItemId)) {
                        user.getBukkitPlayer().getInventory().setBoots(new ItemStack(Material.AIR));
                        if (playerData.getBoots().getActionId() == 95215646) {
                            if (playerData.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) != 1 && playerData.getNetworkPlayer().getSettingsPacket().getSetting("lobby-movement", Integer.class) != 2) {
                                playerData.getUser().getBukkitPlayer().setAllowFlight(false);
                            }
                            playerData.getUser().getBukkitPlayer().setFlying(false);
                        }
                        playerData.setBoots(null);
                        playerData.setActiveBootsId(null);
                    }
                } else if (category == Category.PARTICLE_EFFECTS) {
                    new ParticleEffectsInventory().create(user, 1).open();
                    if (playerData.getActiveParticleId() != null && playerData.getParticle() != null) {
                        //TODO: Remove Particles
                    } else if (playerData.getActiveParticleId() != null && playerData.getActiveParticleId().equalsIgnoreCase(playerItemId) && playerData.getParticle() == null) {
                        playerData.disableWings();
                    }
                }
                user.playSound(Sound.LEVEL_UP, 10L, 10L);

                user.sendMessage("shop-item-sold", user.translate(playerShopItem.getShopItem().getLanguageKey()), playerShopItem.getShopItem().getPrice());
            });

            inventory.setItem(16, ItemBuilder.create(Material.INK_SACK).durability(1).name("§c" + user.translate("cancel")).build(), e -> {
                this.generateView(user, item, category, itemName, itemId).open();
            });
        }


        return inventory;
    }
}