package net.vicemice.lobby.player.inventorys.profile.settings.options;

import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.lobby.player.inventorys.profile.settings.SettingsInventory;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.Rank;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import java.util.ArrayList;
import java.util.Arrays;

/*
 * Class created at 12:55 - 18.04.2020
 * Copyright (C) elrobtossohn
 */
public class FurtherSettingsInventory {


    public GUI create(IUser user) {
        GUI gui = new GUI(user, ChatColor.stripColor(user.translate("settings-further-settings")), 4);
        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(user);

        gui.fill(0, 9, ItemBuilder.create(user.getUIColor()).name("§0 ").build());
        gui.fill(27, 36, ItemBuilder.create(user.getUIColor()).name("§0 ").build());

        gui.setItem(8, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 14 ? 1 : 14)).name("§c✗").lore(new ArrayList<>(Arrays.asList(user.translate("gui-return").split("\n")))).build(), e -> {
            new SettingsInventory().create(user).open();
        });

        if (user.getRank().isHigherEqualsLevel(Rank.PREMIUM)) {
            gui.setItem(10, ItemBuilder.create(Material.COMMAND).name(user.translate("settings-further-advanced-player-settings")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-further-advanced-player-settings-description").split("\n")))).build());
            gui.setItem(12, ItemBuilder.create(Material.FIREWORK).name(user.translate("settings-further-lobby-particle")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-further-lobby-particle-description").split("\n")))).build());
            gui.setItem(14, ItemBuilder.create(Material.RECORD_3).name(user.translate("settings-further-lobby-sounds")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-further-lobby-sounds-description").split("\n")))).build());
            gui.setItem(16, ItemBuilder.create(Material.PAPER).name(user.translate("settings-further-daily-reward")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-further-daily-reward-description").split("\n")))).build());

            boolean advancedPlayerSettings = (user.getNetworkPlayer().getSettingsPacket().getSetting("advanced-player-settings", Integer.class) == 1);
            gui.setItem(19, ItemBuilder.create(Material.INK_SACK).durability((advancedPlayerSettings ? 10 : 8)).name((advancedPlayerSettings ? "§a✔" : "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("advanced-player-settings", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("advanced-player-settings", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("advanced-player-settings", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("advanced-player-settings", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
            boolean lobbyEffects = (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-particle", Integer.class) == 1);
            gui.setItem(21, ItemBuilder.create(Material.INK_SACK).durability((lobbyEffects ? 10 : 8)).name((lobbyEffects ? "§a✔" : "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-particle", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("lobby-particle", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-particle", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("lobby-particle", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
            boolean lobbySounds = (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-sounds", Integer.class) == 1);
            gui.setItem(23, ItemBuilder.create(Material.INK_SACK).durability((lobbySounds ? 10 : 8)).name((lobbySounds ? "§a✔" : "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-sounds", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("lobby-sounds", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-sounds", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("lobby-sounds", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
            boolean dailyReward = (user.getNetworkPlayer().getSettingsPacket().getSetting("collect-daily-reward-automatically", Integer.class) == 1);
            gui.setItem(25, ItemBuilder.create(Material.INK_SACK).durability((dailyReward ? 10 : 8)).name((dailyReward ? "§a✔" : "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("collect-daily-reward-automatically", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("collect-daily-reward-automatically", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("collect-daily-reward-automatically", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("collect-daily-reward-automatically", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
        } else {
            gui.setItem(11, ItemBuilder.create(Material.COMMAND).name(user.translate("settings-further-advanced-player-settings")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-further-advanced-player-settings-description").split("\n")))).build());
            gui.setItem(13, ItemBuilder.create(Material.FIREWORK).name(user.translate("settings-further-lobby-particle")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-further-lobby-particle-description").split("\n")))).build());
            gui.setItem(15, ItemBuilder.create(Material.RECORD_3).name(user.translate("settings-further-lobby-sounds")).lore(new ArrayList<>(Arrays.asList(user.translate("settings-further-lobby-sounds-description").split("\n")))).build());

            boolean advancedPlayerSettings = (user.getNetworkPlayer().getSettingsPacket().getSetting("advanced-player-settings", Integer.class) == 1);
            gui.setItem(20, ItemBuilder.create(Material.INK_SACK).durability((advancedPlayerSettings ? 10 : 8)).name((advancedPlayerSettings ? "§a✔" : "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("advanced-player-settings", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("advanced-player-settings", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("advanced-player-settings", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("advanced-player-settings", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
            boolean lobbyEffects = (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-particle", Integer.class) == 1);
            gui.setItem(22, ItemBuilder.create(Material.INK_SACK).durability((lobbyEffects ? 10 : 8)).name((lobbyEffects ? "§a✔" : "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-particle", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("lobby-particle", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-particle", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("lobby-particle", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
            boolean lobbySounds = (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-sounds", Integer.class) == 1);
            gui.setItem(24, ItemBuilder.create(Material.INK_SACK).durability((lobbySounds ? 10 : 8)).name((lobbySounds ? "§a✔" : "§c✘")).build(), e -> {
                if (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-sounds", Integer.class) == 0) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("lobby-sounds", 1);
                } else if (user.getNetworkPlayer().getSettingsPacket().getSetting("lobby-sounds", Integer.class) == 1) {
                    user.getNetworkPlayer().getSettingsPacket().setSetting("lobby-sounds", 0);
                }
                user.sendSettings();
                this.create(user).open();
            });
        }

        return gui;
    }
}
