package net.vicemice.lobby.netty.info;

import io.netty.channel.Channel;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.lobby.GoLobby;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.lobby.LobbyInfoPacket;

public class LobbyInfoListener extends PacketGetter {

    public void receivePacket(PacketHolder initPacket) {
        if (initPacket.getKey().equals(PacketType.LOBBY_INFO.getKey())) {
            LobbyInfoPacket lobbyInfoPacket = (LobbyInfoPacket) initPacket.getValue();
            GoLobby.getPlayerManager().setGames(lobbyInfoPacket.getGames());
            GoLobby.getPlayerManager().setServerData(lobbyInfoPacket.getServerData());
        }
    }

    public void channelActive(Channel channel) {
    }
}
