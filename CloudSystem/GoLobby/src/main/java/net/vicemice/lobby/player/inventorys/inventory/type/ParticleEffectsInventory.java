package net.vicemice.lobby.player.inventorys.inventory.type;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.lobby.player.inventorys.inventory.DefaultInventory;
import net.vicemice.lobby.player.shop.item.PlayerShopItem;
import net.vicemice.lobby.player.shop.utils.Category;
import net.vicemice.lobby.player.utils.InventoryType;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.players.utils.Pagination;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

/*
 * Class created at 20:51 - 15.04.2020
 * Copyright (C) elrobtossohn
 */
public class ParticleEffectsInventory {

    public GUI create(IUser user, int page) {
        Pagination<Integer> pagination = new Pagination<>(new ArrayList<>(GoLobby.getPlayerManager().getPlayer(user).getItems().get(Category.PARTICLE_EFFECTS).keySet()), 27);
        GUI gui = new DefaultInventory().create(user, InventoryType.PARTICLE_EFFECTS);
        PlayerData playerData = GoLobby.getPlayerManager().getPlayer(user.getUniqueId());

        AtomicInteger i = new AtomicInteger(9);
        pagination.printPage(page, itemId -> {
            PlayerShopItem item = GoLobby.getPlayerManager().getPlayer(user.getUniqueId()).getItems().get(Category.PARTICLE_EFFECTS).get(itemId);
            if (item == null) return;
            boolean glow = (playerData.getActiveParticleId() != null && playerData.getActiveParticleId().equalsIgnoreCase(item.getId()));
            gui.setItem(i.get(), ItemBuilder.create(item.getShopItem().getDefaultItem(user)).name(user.translate(item.getShopItem().getLanguageKey())).lore(new ArrayList<>(Arrays.asList((user.translate("shop-item-description", user.translate(glow ? "shop-effect-selected" : "shop-effect-selectable"), item.getShopItem().getRarity().getRarityColor()+user.translate(item.getShopItem().getRarity().getKey()), user.translate("damage-new"), user.translate(item.getShopItem().getLanguageKey()+"-description"))).split("\n")))).glow(glow).build(), e -> {
                new DefaultInventory().generateView(user, item.getShopItem().getDefaultItem(user), item.getShopItem().getCategory(), user.translate(item.getShopItem().getLanguageKey()), itemId).open();
            });
            i.set((i.get()+1));
        });

        if (playerData.getActiveParticleId() != null) {
            gui.setItem(50, ItemBuilder.create(Material.BARRIER).name(user.translate("shop-item-remove-effect")).build(), e -> {
                if (playerData.getParticle() != null) {
                    playerData.setParticle(null);
                    playerData.setActiveParticleId(null);
                } else if (playerData.getParticle() == null) {
                    playerData.disableWings();
                }
                this.create(user, page).open();
            });
        }

        if (pagination.getElementsFor(page).isEmpty()) {
            gui.setItem(22, ItemBuilder.create(Material.BARRIER).name(user.translate("shop-nothing-available")).build());
        }

        if (!pagination.getElementsFor((page-1)).isEmpty()) {
            gui.setItem(50, ItemBuilder.create(GUI.Items.getLeft()).name(user.translate("previous-page")).build(), e -> {
                this.create(user, (page-1)).open();
            });
        }
        if (!pagination.getElementsFor((page+1)).isEmpty()) {
            gui.setItem(52, ItemBuilder.create(GUI.Items.getRight()).name(user.translate("next-page")).build(), e -> {
                this.create(user, (page+1)).open();
            });
        }

        return gui;
    }
}
