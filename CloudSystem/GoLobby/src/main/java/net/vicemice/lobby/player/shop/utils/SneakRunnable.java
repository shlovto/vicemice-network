package net.vicemice.lobby.player.shop.utils;

import net.vicemice.hector.server.player.UserManager;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.PlayerData;
import net.vicemice.lobby.utils.Boots;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

/*
 * Class created at 22:21 - 30.04.2020
 * Copyright (C) elrobtossohn
 */
public class SneakRunnable implements Runnable {
    private static final SneakRunnable instance = new SneakRunnable();
    private BukkitTask task;

    public void start() {
        this.task = Bukkit.getScheduler().runTaskTimer(GoLobby.getInstance(), this, 0L, 5L);
    }

    @Override
    public void run() {
        for (PlayerData playerData : GoLobby.getPlayerManager().getPlayers().values()) {
            if (!playerData.getUser().isOnline()) continue;
            Boots boots = playerData.getBoots();
            if (boots == null) continue;
            boots.onPlayerSneak(playerData.getUser(), playerData.getUser().getBukkitPlayer().isSneaking());
        }
    }

    public BukkitTask getTask() {
        return this.task;
    }

    private SneakRunnable() {
    }

    public static SneakRunnable getInstance() {
        return instance;
    }
}