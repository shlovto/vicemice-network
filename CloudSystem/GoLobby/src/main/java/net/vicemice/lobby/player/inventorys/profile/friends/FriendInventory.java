package net.vicemice.lobby.player.inventorys.profile.friends;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.lobby.player.inventorys.profile.DefaultProfile;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.CommandPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.players.api.internal.SkullChanger;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.CommandType;
import net.vicemice.hector.utils.players.lobby.LobbyFriendData;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;

public class FriendInventory {

    public GUI create(IUser user, LobbyFriendData lobbyFriendData) {
        GUI gui = new DefaultProfile().createView(user, lobbyFriendData.getRank().getRankColor() + lobbyFriendData.getName());

        gui.setItem(8, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 14 ? 1 : 14)).name("§c✗").lore(new ArrayList<>(Arrays.asList(user.translate("gui-return").split("\n")))).build(), e -> {
            new FriendsInventory().create(user, 1).open();
        });

        gui.setItem(10, ItemBuilder.create((lobbyFriendData.isOnline() ? SkullChanger.getSkull(lobbyFriendData.getSignature(), lobbyFriendData.getValue()) : new ItemStack(Material.SKULL_ITEM))).name(lobbyFriendData.getRank().getRankColor() + lobbyFriendData.getName()).lore(lobbyFriendData.getMessage()).build());
        if (lobbyFriendData.isOnline()) {
            gui.setItem(11, ItemBuilder.create(Material.CAKE).name(user.translate("friends-friend-invite-party")).lore(new ArrayList<>(Arrays.asList(user.translate("friends-friend-invite-party-description", lobbyFriendData.getRank().getRankColor() + lobbyFriendData.getName()).split("\n")))).build(), e -> {
                GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.PARTY, new String[]{"invite", lobbyFriendData.getName()})), API.PacketReceiver.SLAVE);
                e.getView().close();
            });
            gui.setItem(12, ItemBuilder.create(Material.IRON_CHESTPLATE).name(user.translate("friends-friend-invite-clan")).lore(new ArrayList<>(Arrays.asList(user.translate("friends-friend-invite-clan-description", lobbyFriendData.getRank().getRankColor() + lobbyFriendData.getName()).split("\n")))).build(), e -> {
                GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.CLAN, new String[]{"invite", lobbyFriendData.getName()})), API.PacketReceiver.SLAVE);
                e.getView().close();
            });
            gui.setItem(15, ItemBuilder.create(Material.ENDER_PEARL).name(user.translate("friends-friend-jump")).lore(new ArrayList<>(Arrays.asList(user.translate("friends-friend-jump-description", lobbyFriendData.getRank().getRankColor() + lobbyFriendData.getName()).split("\n")))).build(), e -> {
                GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.FRIENDS, new String[]{"jump", lobbyFriendData.getName()})), API.PacketReceiver.SLAVE);
                e.getView().close();
            });
        }
        if (!lobbyFriendData.isFavorite()) {
            String favoriteHead = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvODY4ZjRjZWY5NDlmMzJlMzNlYzVhZTg0NWY5YzU2OTgzY2JlMTMzNzVhNGRlYzQ2ZTViYmZiN2RjYjYifX19";
            gui.setItem(13, ItemBuilder.create(SkullChanger.getSkullByTexture("bcefcc41-e997-4845-ae08-7b8a1a2d51b6", favoriteHead)).name(user.translate("friends-friend-add-favorite")).lore(new ArrayList<>(Arrays.asList(user.translate("friends-friend-add-favorite-description", lobbyFriendData.getRank().getRankColor() + lobbyFriendData.getName()).split("\n")))).build(), e -> {
                GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.FRIENDS, new String[]{"favorite", lobbyFriendData.getName()})), API.PacketReceiver.SLAVE);
                e.getView().close();
            });
        } else {
            String favoriteHead = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMjFkZmY0ODg0NmQxNTI0MjczODU5ZDcxNzcyOTU1NmY2MjZmYTVmMjE4NWExYzMyMmU3MjMzMjUyNjNmMDljIn19fQ==";
            gui.setItem(13, ItemBuilder.create(SkullChanger.getSkullByTexture("14e14fd3-bd64-4374-9858-0ad57134b162", favoriteHead)).name(user.translate("friends-friend-remove-favorite")).lore(new ArrayList<>(Arrays.asList(user.translate("friends-friend-remove-favorite-description", lobbyFriendData.getRank().getRankColor() + lobbyFriendData.getName()).split("\n")))).build(), e -> {
                GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.FRIENDS, new String[]{"unfavorite", lobbyFriendData.getName()})), API.PacketReceiver.SLAVE);
                e.getView().close();
            });
        }
        String replayHead = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMjVkODdhNGMzYmJjNDU1NjY1YTU1OTI5NzA4NGUwNTliMjk4NmU1YzIyNThiNDQ5ZWEwZjE3YTBiMTJhZGRhYyJ9fX0=";
        gui.setItem(14, ItemBuilder.create(SkullChanger.getSkullByTexture("4d240d45-9459-4822-a51d-2cd809a90327", replayHead)).name(user.translate("friends-friend-open-replays")).lore(new ArrayList<>(Arrays.asList(user.translate("friends-friend-open-replays-description", lobbyFriendData.getRank().getRankColor() + lobbyFriendData.getName()).split("\n")))).build(), e -> {
            GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.REPLAY, new String[]{"list", lobbyFriendData.getName()})), API.PacketReceiver.SLAVE);
            e.getView().close();
        });
        gui.setItem(16, ItemBuilder.create(Material.BARRIER).name(user.translate("friends-friend-remove")).lore(new ArrayList<>(Arrays.asList(user.translate("friends-friend-remove-description", lobbyFriendData.getRank().getRankColor() + lobbyFriendData.getName()).split("\n")))).build(), e -> {
            GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.FRIENDS, new String[]{"remove", lobbyFriendData.getName()})), API.PacketReceiver.SLAVE);
            new FriendsInventory().create(user, 1).open();
        });

        return gui;
    }
}