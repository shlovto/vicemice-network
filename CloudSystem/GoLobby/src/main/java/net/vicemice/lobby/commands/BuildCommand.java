package net.vicemice.lobby.commands;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.lobby.GoLobby;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.hector.utils.Rank;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BuildCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        IUser user = UserManager.getUser((Player) sender);
        if (user.getRank().isLowerLevel(Rank.ADMIN)) {
            user.sendRawMessage("command-no-permission");
            return false;
        }

        if (args.length == 1) {
            Player itarget = Bukkit.getPlayer(args[0]);
            if (itarget != null) {
                IUser target = UserManager.getUser(itarget);
                if (GoLobby.getPlayerManager().getBuilders().contains(target.getUniqueId())) {
                    GoLobby.getPlayerManager().getBuilders().remove(target.getUniqueId());
                    target.getBukkitPlayer().setGameMode(GameMode.ADVENTURE);
                    target.sendMessage("command-build-exit");
                    user.sendMessage("command-build-target-exit", (target.getRank().getRankColor()+target.getName()));
                } else {
                    GoLobby.getPlayerManager().getBuilders().add(target.getUniqueId());
                    target.getBukkitPlayer().setGameMode(GameMode.CREATIVE);
                    target.sendMessage("command-build-enter");
                    user.sendMessage("command-build-target-enter", (target.getRank().getRankColor()+target.getName()));
                }
            } else {
                user.sendMessage("player-not-found");
            }
        } else {
            if (GoLobby.getPlayerManager().getBuilders().contains(user.getUniqueId())) {
                GoLobby.getPlayerManager().getBuilders().remove(user.getUniqueId());
                user.getBukkitPlayer().setGameMode(GameMode.ADVENTURE);
                user.sendMessage("command-build-exit");
            } else {
                GoLobby.getPlayerManager().getBuilders().add(user.getUniqueId());
                user.getBukkitPlayer().setGameMode(GameMode.CREATIVE);
                user.sendMessage("command-build-enter");
            }
        }

        return false;
    }
}
