/*
 * Decompiled with CFR 0.145.
 */
package net.vicemice.hector.utils.threading;

public interface Callback<C> {
    public void call(C var1);
}

