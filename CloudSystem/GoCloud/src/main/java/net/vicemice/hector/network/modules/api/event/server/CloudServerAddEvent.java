package net.vicemice.hector.network.modules.api.event.server;

import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.utils.event.Event;

public class CloudServerAddEvent extends Event {
    private Server cloudServer;

    public Server getCloudServer() {
        return this.cloudServer;
    }

    public CloudServerAddEvent(Server cloudServer) {
        this.cloudServer = cloudServer;
    }
}