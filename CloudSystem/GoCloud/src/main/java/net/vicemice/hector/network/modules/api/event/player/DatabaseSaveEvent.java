package net.vicemice.hector.network.modules.api.event.player;

import net.vicemice.hector.utils.DatabaseQueue;
import net.vicemice.hector.utils.event.async.AsyncEvent;
import net.vicemice.hector.utils.event.async.AsyncPosterAdapter;

public class DatabaseSaveEvent extends AsyncEvent<LoginEvent> {
    private DatabaseQueue.SaveEntry saveEntry;

    public DatabaseSaveEvent(DatabaseQueue.SaveEntry saveEntry) {
        super(new AsyncPosterAdapter<>());
        this.saveEntry = saveEntry;
    }

    public DatabaseQueue.SaveEntry getSaveEntry() {
        return saveEntry;
    }
}
