package net.vicemice.hector.utils.terminal.commands;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.utils.terminal.ArgumentList;
import net.vicemice.hector.utils.terminal.CommandExecutor;
import net.vicemice.hector.utils.terminal.Terminal;

public class CommandReload implements CommandExecutor {

    @Override
    public void onCommand(String command, Terminal writer, String[] args) {
        if (args.length == 1) {
            if (args[0].equals("languages")) {
                new Thread(() -> {
                    writer.writeMessage("§6Try to reload the Languages...");
                    CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager().reloadLocales();
                    writer.writeMessage("§aClyde loaded §c" + CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager().getLocales().size() + "§7 languages!");
                }).start();
            } else if (args[0].equals("templates")) {
                new Thread(() -> {
                    writer.writeMessage("§6Try to reload the Templates...");
                    CloudService.getCloudService().getManagerService().getServerManager().reloadTemplates();
                    writer.writeMessage("§aClyde loaded §c" + CloudService.getCloudService().getManagerService().getServerManager().getTemplates().size() + "§7 Templates!");
                }).start();
            } else if (args[0].equals("permissions")) {
                new Thread(() -> {
                    writer.writeMessage("§6Try to reload the Permissions...");
                    CloudService.getCloudService().getManagerService().getPermissionManager().reloadPermissions();
                    writer.writeMessage("§aClyde loaded §c" + CloudService.getCloudService().getManagerService().getPermissionManager().getPermissionsMap().size() + "§7 Permissions!");
                }).start();
            } else if (args[0].equals("settings")) {
                new Thread(() -> {
                    writer.writeMessage("§6Try to reload the Network Settings...");
                    CloudService.getCloudService().getManagerService().getProxyManager().loadSettings();
                    writer.writeMessage("§aClyde loaded §c3 §7Network Settings!");
                }).start();
            } else if (args[0].equals("games")) {
                new Thread(() -> {
                    writer.writeMessage("§6Try to reload the Games...");
                    CloudService.getCloudService().getManagerService().getGameManager().loadGames();
                    writer.writeMessage("§aClyde loaded §c" + CloudService.getCloudService().getManagerService().getGameManager().getGameList().size() + "§7 Games!");
                }).start();
            } else if (args[0].equals("statssigns")) {
                new Thread(() -> {
                    writer.writeMessage("§6Try to reload the Stats Signs...");
                    CloudService.getCloudService().getManagerService().getSignStatsManager().loadSigns();
                    writer.writeMessage("§aClyde loaded §c" + CloudService.getCloudService().getManagerService().getSignStatsManager().getStatSigns().size() + "§7 Stats Signs!");
                }).start();
            } else if (args[0].equals("wservers")) {
                new Thread(() -> {
                    writer.writeMessage("§6Try to reload the Whitelisted Servers...");
                    CloudService.getCloudService().getManagerService().getWhitelistManager().reload();
                    writer.writeMessage("§aClyde loaded §c" + CloudService.getCloudService().getManagerService().getWhitelistManager().getServers().size() + "§7 Servers!");
                }).start();
            } else {
                writer.writeMessage("§cSubcommand not found.");
            }
        } else {
            writer.write("§aCommands: ");
            writer.write("  §7- §a/reload <object>");
        }
    }

    @Override
    public ArgumentList getArguments() {
        return ArgumentList.builder().arg(new ArgumentList.Argument("/reload", "Reload a feature of the application")).build();
    }
}
