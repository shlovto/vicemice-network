package net.vicemice.hector.utils;

import net.vicemice.hector.utils.threading.Runnabled;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class CollectionWrapper {
    private CollectionWrapper() {
    }

    public static <E, X> Collection<X> transform(Collection<E> collection, Catcher<X, E> catcher) {
        List<X> xCollection = CollectionWrapper.newCopyOnWriteArrayList();
        for (E e : collection) {
            xCollection.add(catcher.doCatch(e));
        }
        return xCollection;
    }

    public static <E> Collection<E> filterMany(Collection<E> elements, Acceptable<E> acceptable) {
        LinkedList<E> collection = new LinkedList<E>();
        for (E element : elements) {
            if (!acceptable.isAccepted(element)) continue;
            collection.add(element);
        }
        return collection;
    }

    public static <E> E filter(Collection<E> elements, Acceptable<E> acceptable) {
        for (E element : elements) {
            if (!acceptable.isAccepted(element)) continue;
            return element;
        }
        return null;
    }

    public static <E> CopyOnWriteArrayList<E> transform(Collection<E> defaults) {
        return new CopyOnWriteArrayList<E>(defaults);
    }

    public static Collection<String> toCollection(String input, String splitter) {
        return new CopyOnWriteArrayList<String>(input.split(splitter));
    }

    public static <E> void iterator(Collection<E> collection, Runnabled<E>... runnableds) {
        for (E el : collection) {
            for (Runnabled<E> runnabled : runnableds) {
                runnabled.run(el);
            }
        }
    }

    public static <E> void iterator(E[] collection, Runnabled<E> ... runnableds) {
        for (E el : collection) {
            for (Runnabled<E> runnabled : runnableds) {
                runnabled.run(el);
            }
        }
    }

    public static <E, X, C> Collection<E> getCollection(Map<X, C> map, Catcher<E, C> catcher) {
        LinkedList<E> collection = new LinkedList<E>();
        for (C values : map.values()) {
            collection.add(catcher.doCatch(values));
        }
        return collection;
    }

    public static <E> void checkAndRemove(Collection<E> collection, Acceptable<E> acceptable) {
        Object e = null;
        for (E element : collection) {
            if (!acceptable.isAccepted(element)) continue;
            e = element;
        }
        if (e != null) {
            collection.remove(e);
        }
    }

    public static <E> List<E> newCopyOnWriteArrayList() {
        return new CopyOnWriteArrayList();
    }

    public static <E> void iterator(E[] values, Runnabled<E> handled) {
        for (E value : values) {
            handled.run(value);
        }
    }

    public static <E> boolean equals(E[] array, E value) {
        for (E a : array) {
            if (!a.equals(value)) continue;
            return true;
        }
        return false;
    }

    public static <E> int filled(E[] array) {
        int i = 0;
        for (E element : array) {
            if (element == null) continue;
            ++i;
        }
        return i;
    }

    public static <E> boolean isEmpty(E[] array) {
        for (E element : array) {
            if (element == null) continue;
            return false;
        }
        return true;
    }

    public static <E> void remove(E[] array, E element) {
        int i = CollectionWrapper.index(array, element);
        array[i] = null;
    }

    public static <E> Object[] dynamicArray(Class<E> clazz) {
        return (Object[]) Array.newInstance(clazz, Integer.MAX_VALUE);
    }

    public static <E> int index(E[] array, E element) {
        for (int i = 0; i < array.length; ++i) {
            if (!array[i].equals(element)) continue;
            return i;
        }
        return 0;
    }
}