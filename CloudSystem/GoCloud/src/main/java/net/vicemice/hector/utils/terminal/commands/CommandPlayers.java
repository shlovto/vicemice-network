package net.vicemice.hector.utils.terminal.commands;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.packets.types.player.settings.SettingsPacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.beforejoined.BeforePlayer;
import net.vicemice.hector.utils.JSONUtil;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.language.Language;
import net.vicemice.hector.utils.terminal.ArgumentList;
import net.vicemice.hector.utils.terminal.CommandExecutor;
import net.vicemice.hector.utils.terminal.Terminal;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.UUID;

/*
 * Class created at 23:11 - 01.03.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class CommandPlayers implements CommandExecutor {

    @Override
    public void onCommand(String command, Terminal writer, String[] args) {
        if (args.length >= 1) {

            if (args[0].equals("checknicks")) {
                CloudService.getCloudService().getManagerService().getNickNameManager().checkValid(null);
            } else if (args[0].equals("create") && args.length == 2) {
                if (CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]) != null) {
                    writer.writeMessage("§cPlayer already exists in the database");
                    return;
                }

                JSONUtil JSONUtil = new JSONUtil(net.vicemice.hector.utils.JSONUtil.API.MCSTATS, args[1]);

                String name = null;
                UUID uniqueId = null;
                String skinSignature = null;
                String skinValue = null;

                try {
                    JSONObject jsonObject = JSONUtil.getObject();
                    if (jsonObject.getJSONObject("system").getInt("status") == 404) {
                        writer.writeMessage("§cPlayer was not found in minecraft!");
                        return;
                    }
                    name = jsonObject.getJSONObject("response").getString("name");
                    uniqueId = UUID.fromString(jsonObject.getJSONObject("response").getString("UUID"));
                    skinSignature = jsonObject.getJSONObject("response").getJSONObject("skin").getString("signature");
                    skinValue = jsonObject.getJSONObject("response").getJSONObject("skin").getString("value");
                } catch (JSONException ex) {
                    ex.printStackTrace();
                    writer.writeMessage("Result (CommandPlayers:37) -> " + JSONUtil.getResult());
                    CloudService.shutdown();
                }

                if (name == null || uniqueId == null || skinValue == null || skinSignature == null) {
                    writer.writeMessage("§cPlayer options not valid! (null)");
                    return;
                }

                CloudPlayer cloudPlayer = new CloudPlayer(uniqueId, name, "german", "de-DE", "0.0.0.0", Rank.MEMBER, System.currentTimeMillis(), System.currentTimeMillis(), 0, new HashMap<>(), new HashMap<>(), 0, null);
                cloudPlayer.setSettingsPacket(new SettingsPacket(uniqueId, null));
                cloudPlayer.setForumId(-1);
                cloudPlayer.setTeamSpeakId("noid");
                cloudPlayer.setDiscordId("noid");
                CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().put(uniqueId, cloudPlayer);
                CloudService.getCloudService().getManagerService().getPlayerManager().getNameCloudPlayers().put(args[1].toLowerCase(), cloudPlayer);

                cloudPlayer.setSkinValue(skinValue);
                cloudPlayer.setSkinSignature(skinSignature);
                cloudPlayer.setCountry("DE");

                CloudService.getCloudService().getTerminal().writeMessage("§e§l"+name+" §3connected the first time to the network");
                BeforePlayer beforePlayer = CloudService.getCloudService().getManagerService().getBeforeManager().getPlayer(name);
                if (beforePlayer != null) {
                    if (beforePlayer.getRank() != null) {
                        cloudPlayer.setRank(Rank.fromString(beforePlayer.getRank()), 0);
                    }
                    if (beforePlayer.getPermissions() != null) {
                        cloudPlayer.setPermissions(beforePlayer.getPermissions());
                    }
                    CloudService.getCloudService().getManagerService().getBeforeManager().getBeforePlayers().remove(name.toLowerCase());
                    beforePlayer.remove();
                }
                try {
                    cloudPlayer.setLanguage(Language.LanguageType.GERMAN);
                    cloudPlayer.sendMessage("change-language");
                } catch (Exception e) {
                    cloudPlayer.setLanguage(Language.LanguageType.GERMAN);
                }
                cloudPlayer.setFullLoaded(true);
                cloudPlayer.updateToDatabase(true);
                CloudService.getCloudService().getTerminal().writeMessage("§e§lPlayer created");
            } else if (args[0].equals("accounts") && args.length == 4) {
                CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
                if (cloudPlayer != null) {
                    if (args[2].equalsIgnoreCase("teamspeak")) {
                        cloudPlayer.setTeamSpeakId(args[3]);
                    } else if (args[2].equalsIgnoreCase("discord")) {
                        cloudPlayer.setDiscordId(args[3]);
                    } else if (args[2].equalsIgnoreCase("forum")) {
                        cloudPlayer.setForumId(Integer.valueOf(args[3]));
                    }
                    cloudPlayer.updateToDatabase(false);
                    writer.writeMessage("§aPlayer updated! (Account '" + args[2] + "')");
                } else {
                    writer.writeMessage("§cPlayer was not found in database!");
                }
            } else if (args[0].equals("updateNames")) {
                CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().values().forEach(e -> {
                    if (!e.getUniqueId().toString().replace("-", "").equalsIgnoreCase("336991c81199457c80bb649042a489f5")) {
                        JSONUtil JSONUtil = new JSONUtil(net.vicemice.hector.utils.JSONUtil.API.MCSTATS, e.getUniqueId().toString());
                        try {
                            String checkName = JSONUtil.getObject().getJSONObject("response").getString("name");

                            if (!e.getName().equals(checkName)) {
                                CloudService.getCloudService().getTerminal().writeMessage("'" + e.getName() + "' has new Name: '" + checkName + "'");

                                CloudService.getCloudService().getManagerService().getPlayerManager().getNameCloudPlayers().remove(e.getName());
                                CloudService.getCloudService().getManagerService().getPlayerManager().getNameCloudPlayers().put(checkName, e);

                                e.setName(checkName);
                                e.updateToDatabase(false);
                            }
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            CloudService.getCloudService().getTerminal().writeMessage("Result (CommandPlayers:127) -> " + JSONUtil.getResult());
                        }
                    }
                });
            } else if (args[0].equals("saveAll")) {
                CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().values().forEach(e -> {
                    e.updateToDatabase(false);
                });
            } else if (args[0].equals("insertAll")) {
                CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().values().forEach(e -> {
                    e.updateToDatabase(true);
                });
            } else if (args[0].equals("save")) {
                CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
                if (cloudPlayer != null) {
                    cloudPlayer.updateToDatabase(false);
                    writer.writeMessage("§aPlayer saved!");
                } else {
                    writer.writeMessage("§cPlayer was not found in database!");
                }
            } else {
                writer.writeMessage("§cSubcommand not found.");
            }
        } else {
            writer.write("§aCommands: ");
            writer.write("  §7- §a/players <object>");
        }
    }

    @Override
    public ArgumentList getArguments() {
        return ArgumentList.builder().arg(new ArgumentList.Argument("/players", "Manage Players")).build();
    }
}