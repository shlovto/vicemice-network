package net.vicemice.hector.utils.gameid;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.utils.RandomStringGenerator;
import org.bson.Document;

public class GameIDGenerator {

    public static String getGameID() {
        try {
            FindIterable<Document> cursor;
            String gameID;
            MongoCollection<Document> collection;
            do {
                gameID = RandomStringGenerator.generateRandomString(8, RandomStringGenerator.Mode.ALPHANUMERIC);
            } while ((cursor = (collection = CloudService.getCloudService().getManagerService().getMongoManager().getGamesDatabase().getCollection("games")).find(Filters.eq("gameID", gameID))).first() != null);

            CloudService.getCloudService().getTerminal().writeMessage("Generate GameID for a Server: " + gameID);

            return gameID;
        } catch (Exception exc) {
            exc.printStackTrace();
            return null;
        }
    }
}