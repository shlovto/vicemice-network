package net.vicemice.hector.manager.game;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.network.stats.Game;
import org.bson.Document;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/*
 * Class created at 23:52 - 02.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GameManager {
    private List<Game> gameList = new CopyOnWriteArrayList<Game>();

    public void loadGames() {
        gameList.clear();
        try {
            ArrayList<Document> list = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("stats");

            for (Document document : list) {
                gameList.add(new Game(document.getString("modi"), document.getString("prefix"), document.getString("rankingField"), document.getString("signTypes")));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Game getGameByPrefix(String prefix) {
        for (Game game : gameList) {
            if (!game.getPrefix().equalsIgnoreCase(prefix)) continue;
            return game;
        }
        return null;
    }

    public Game getGameByName(String name) {
        for (Game game : gameList) {
            if (!game.getModiName().equalsIgnoreCase(name)) continue;
            return game;
        }
        return null;
    }

    public List<Game> getGameList() {
        return gameList;
    }
}