package net.vicemice.hector.utils.event.async;

public class AsyncPosterAdapter<E extends AsyncEvent<?>> implements AsyncPoster<E> {
    @Override
    public void onPreCall(E event) {
    }

    @Override
    public void onPostCall(E event) {
    }
}

