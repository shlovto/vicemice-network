package net.vicemice.hector.utils;

import net.vicemice.hector.player.CloudPlayer;

import java.util.ArrayList;
import java.util.List;

public class MessageGroup {
    private List<CloudPlayer> receivers = new ArrayList<CloudPlayer>();
    private List<String> messages = new ArrayList<String>();

    public MessageGroup() {
    }

    public MessageGroup(List<CloudPlayer> receivers) {
        this.receivers = receivers;
    }

    public MessageGroup(CloudPlayer receiver) {
        this.receivers.add(receiver);
    }

    public void addReceiver(CloudPlayer cloudPlayer) {
        this.receivers.add(cloudPlayer);
    }

    public void addMessage(String message) {
        this.messages.add(message.replace("&", "§"));
    }


    public void send() {
        String[] messageArray = new String[this.messages.size()];
        messageArray = this.messages.toArray(messageArray);
        for (CloudPlayer cloudPlayer : this.receivers) {
            cloudPlayer.sendMessages(messageArray);
        }
        this.receivers.clear();
        this.messages.clear();
    }

    public List<String> getMessages() {
        return this.messages;
    }
}

