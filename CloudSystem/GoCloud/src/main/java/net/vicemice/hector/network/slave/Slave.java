package net.vicemice.hector.network.slave;

import io.netty.channel.Channel;
import lombok.Data;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.slave.SlaveServerKill;

@Data
public class Slave {
    private String host;
    private String name;
    private int usedRam;
    private int maxRam;
    private Channel nettyChannel;
    private long timeout = -1;
    private Server currentStartingServer;
    private boolean active = true;
    private String forceTemplate;
    private double cpuUsage;
    private double averageCPU;
    private int usedMemory;
    private int freeMemory;
    private int totalMemory;

    public Slave(String host, String name, int maxRam) {
        this.host = host;
        this.name = name;
        this.maxRam = maxRam;
        this.usedRam = 0;

        if (CloudService.getCloudService().isDevMode()) CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().sendAdminMessage("cloud-slave-connected", name);
    }

    public void sendPacket(PacketHolder initPacket) {
        this.nettyChannel.writeAndFlush(initPacket);
    }

    public void calculateRam() {
        int ram = 0;
        for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
            if (!server.isCloudServer() || !server.getSlave().equals(this)) continue;
            ram += server.getServerTemplate().getMaxRamProServer();
        }
        this.usedRam = ram;
    }

    public boolean isReady() {
        if (this.getNettyChannel() == null || !this.getNettyChannel().isOpen() || !this.getNettyChannel().isActive()) {
            return false;
        }
        return true;
    }

    public void remove() {
        for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
            if (!server.isCloudServer() || !server.getSlave().getName().equalsIgnoreCase(this.getName())) continue;
            server.remove();
        }

        if (CloudService.getCloudService().isDevMode()) CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().sendAdminMessage("cloud-slave-disconnected", name);
    }

    public void killServer(String name, boolean delete) {
        this.nettyChannel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.SLAVE_SERVER_KILL, new SlaveServerKill(name, delete)));
    }
}