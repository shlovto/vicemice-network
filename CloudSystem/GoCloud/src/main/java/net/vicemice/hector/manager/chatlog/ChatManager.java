package net.vicemice.hector.manager.chatlog;

import net.vicemice.hector.CloudService;
import org.bson.Document;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class ChatManager {
    public ArrayList<Message> messages;

    public ChatManager() {

    }

    public void init() {
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                reload();
            }
        }, 30000, 150000);
    }

    /**
     Save all Messages from which should be saved
     */
    private void reload() {
        if (messages.size() == 0) {
            return;
        }

        for (Message message : this.messages) {
            try {
                Document document = new Document("uuid", message.getAuthor().toString());
                document.append("message", message.getMessage());
                document.append("server", message.getServer());
                document.append("ip", message.getIp());
                document.append("time", message.getTime());

                CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("chat").insertOne(document);
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }
        this.messages.clear();
    }

    /**
     Add an Message to the ChatLog-System
     @param message defined the message author, timestamp and the message self
     */
    public void addMessage(Message message) {
        if (this.messages.size() > 30) {
            this.reload();
        }

        this.messages.add(message);
    }

    public static class Message {
        private UUID author;
        private String server;
        private String message;
        private String ip;
        private long time;

        /**
         @param author defined the uuid from the author
         @param server defined the server where the message was sended
         @param ip defined the ip from the author
         @param message defined the message from the author
         @param time defined the time where the message was sended
         */
        public Message(UUID author, String server, String ip, String message, long time) {
            this.author = author;
            this.server = server;
            this.ip = ip;
            this.message = message;
            this.time = time;
        }

        /**
         @return the Author
         */
        public UUID getAuthor() {
            return this.author;
        }

        /**
         @return the Server
         */
        public String getServer() {
            return this.server;
        }

        /**
         @return the Message
         */
        public String getMessage() {
            return this.message;
        }

        /**
         @return the IP from the Author
         */
        public String getIp() {
            return this.ip;
        }

        /**
         @return the Timestamp
         */
        public long getTime() {
            return this.time;
        }
    }
}