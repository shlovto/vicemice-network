package net.vicemice.hector.utils.event;

public interface Cancelable {
    public boolean isCancelled();

    public void setCancelled(boolean var1);
}

