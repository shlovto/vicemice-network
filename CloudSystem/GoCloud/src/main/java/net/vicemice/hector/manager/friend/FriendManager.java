package net.vicemice.hector.manager.friend;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.players.lobby.LobbyFriendData;
import net.vicemice.hector.utils.players.lobby.LobbyRequestData;
import net.vicemice.hector.utils.players.utils.PlayerState;

import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

public class FriendManager {

    public List<LobbyFriendData> getFriendsArray(CloudPlayer cloudPlayer) {
        List<LobbyFriendData> friendArray = new ArrayList<>();
        List<CloudPlayer> players = new ArrayList<>();
        Map<UUID, Boolean> friendFavorites = new HashMap<>();
        cloudPlayer.getFriends().forEach((k,friendData) -> {
            CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(friendData.getUniqueId());
            if (getPlayer == null) {
                cloudPlayer.getFriends().remove(k);
                return;
            }
            players.add(getPlayer);
            friendFavorites.put(getPlayer.getUniqueId(), friendData.isFavorite());
        });

        List<CloudPlayer> allPlayers = new ArrayList<>();
        List<CloudPlayer> onlinePlayers = players.stream().map(e -> CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayer(e.getUniqueId())).filter(e -> e.isPresent()).map(e -> e.get()).filter(e -> e.isOnline()).collect(Collectors.toList());
        List<CloudPlayer> offlinePlayers = players.stream().map(e -> CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayer(e.getUniqueId())).filter(e -> e.isPresent()).map(e -> e.get()).filter(e -> !e.isOnline()).collect(Collectors.toList());
        if (cloudPlayer.getSetting("friend-sorting", Integer.class) == 0) {
            allPlayers.addAll(players);
            allPlayers.sort(Collections.reverseOrder());
        } else if (cloudPlayer.getSetting("friend-sorting", Integer.class) == 1) {
            allPlayers.addAll(players);
            Collections.sort(allPlayers);
        } else if (cloudPlayer.getSetting("friend-sorting", Integer.class) == 2) {
            allPlayers.addAll(players);
            allPlayers.sort((o1, o2) -> Boolean.compare(friendFavorites.get(o2.getUniqueId()), friendFavorites.get(o1.getUniqueId())));
        } else if (cloudPlayer.getSetting("friend-sorting", Integer.class) == 3) {
            onlinePlayers.sort((o1, o2) -> Boolean.compare(o2.isOnline(), o1.isOnline()));
            offlinePlayers.sort((o1, o2) -> Long.compare(o2.getLastLogout(), o1.getLastLogout()));
            allPlayers.addAll(onlinePlayers);
            allPlayers.addAll(offlinePlayers);
        } else if (cloudPlayer.getSetting("friend-sorting", Integer.class) == 4) {
            offlinePlayers.sort((o1, o2) -> Long.compare(o2.getLastLogout(), o1.getLastLogout()));
            onlinePlayers.sort((o1, o2) -> Boolean.compare(friendFavorites.get(o2.getUniqueId()), friendFavorites.get(o1.getUniqueId())));
            allPlayers.addAll(offlinePlayers);
            allPlayers.addAll(onlinePlayers);
        }

        for (CloudPlayer online : allPlayers) {
            if (online.isOnline()) {
                if (online.getServer() == null) {
                    offlinePlayers.add(online);
                    continue;
                }
                String server = cloudPlayer.getLanguageMessage("is-on-unknown");
                String status = cloudPlayer.getLanguageMessage("status-online");
                if (online.getServer() != null) {
                    server = cloudPlayer.getLanguageMessage("is-on-server", online.getServer().getName().toUpperCase().replace("-", ""));
                    if (online.getPlayerState() == PlayerState.INGAME && online.getServer().getServerState() == ServerState.INGAME) {
                        status = cloudPlayer.getLanguageMessage("status-ingame");
                    }
                    if (online.getPlayerState() == PlayerState.SPECTATOR && online.getServer().getServerState() == ServerState.INGAME) {
                        status = cloudPlayer.getLanguageMessage("status-spectator");
                    }
                    if (online.isAfk()) {
                        status = cloudPlayer.getLanguageMessage("status-afk");
                    }
                }
                String message = MessageFormat.format(cloudPlayer.getLanguageMessage("lobby-friend-list-online"), status, server);

                LobbyFriendData lobbyFriendData = new LobbyFriendData(online.getName(), friendFavorites.get(online.getUniqueId()), online.getRank(), message, "Coming soon", 0L, online.getSkinValue(), online.getSkinSignature(), true);
                friendArray.add(lobbyFriendData);
            } else {
                String last = GoAPI.getTimeManager().getRemainingTimeString1(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), cloudPlayer.getLocale(), online.getLastLogout());
                String message = "";
                if (online.getSetting("hide-last-offline", Integer.class) == 1) {
                    message = MessageFormat.format(cloudPlayer.getLanguageMessage("lobby-friend-list-offline-hidden"), last);
                } else {
                    message = MessageFormat.format(cloudPlayer.getLanguageMessage("lobby-friend-list-offline"), last);
                }

                LobbyFriendData lobbyFriendData = new LobbyFriendData(online.getName(), friendFavorites.get(online.getUniqueId()), online.getRank(), message, "Coming soon", 0L, online.getSkinValue(), online.getSkinSignature(), false);
                friendArray.add(lobbyFriendData);
            }
        }
        onlinePlayers.clear();
        offlinePlayers.clear();
        return friendArray;
    }

    public List<LobbyRequestData> getRequestArrays(CloudPlayer cloudPlayer) {
        List<LobbyRequestData> friendArray = new ArrayList<>();
        cloudPlayer.getRequests().forEach((k,friendData) -> {
            CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(friendData.getUniqueId());
            if (getPlayer == null) {
                cloudPlayer.getRequests().remove(k);
                return;
            }
            LobbyRequestData lobbyRequestData = new LobbyRequestData(getPlayer.getName(), getPlayer.getRank(), getPlayer.getSkinValue(), getPlayer.getSkinSignature());
            friendArray.add(lobbyRequestData);
        });

        return friendArray;
    }

    public void handleLogin(CloudPlayer cloudPlayer) {
        new Thread(() -> {
            if (cloudPlayer.getFriends().size() == 0) return;
            cloudPlayer.getFriends().forEach((k,friendData) -> {
                CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(friendData.getUniqueId());
                if (getPlayer == null) {
                    cloudPlayer.getFriends().remove(k);
                    return;
                }
                if (getPlayer.isOnline() && getPlayer.getSetting("friend-messages", Integer.class) == 1) {
                    getPlayer.sendMessage("friends-user-is-now-online", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()));
                    getPlayer.updateFriendInv();
                }
            });
        }).start();
    }

    public void handleLogOut(CloudPlayer cloudPlayer) {
        new Thread(() -> {
            if (cloudPlayer.getFriends().size() == 0) return;
            cloudPlayer.getFriends().forEach((k,friendData) -> {
                CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(friendData.getUniqueId());
                if (getPlayer == null) {
                    cloudPlayer.getFriends().remove(k);
                    return;
                }
                if (getPlayer.isOnline() && getPlayer.getSetting("friend-messages", Integer.class) == 1) {
                    getPlayer.sendMessage("friends-user-is-now-offline", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()));
                    getPlayer.updateFriendInv();
                }
            });
        }).start();
    }

    public void handleServerSwitch(CloudPlayer cloudPlayer) {
        new Thread(() -> {
            if (cloudPlayer.getFriends().size() == 0) return;
            cloudPlayer.getFriends().forEach((k,friendData) -> {
                CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(friendData.getUniqueId());
                if (getPlayer == null) {
                    cloudPlayer.getFriends().remove(k);
                    return;
                }
                if (getPlayer.isOnline()) {
                    getPlayer.updateFriendInv();
                }
            });
        }).start();
    }
}