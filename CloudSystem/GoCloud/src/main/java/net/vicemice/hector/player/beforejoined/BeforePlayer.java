package net.vicemice.hector.player.beforejoined;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.CloudService;
import org.bson.Document;
import org.bson.conversions.Bson;

public class BeforePlayer {

    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String rank;
    @Getter
    @Setter
    private boolean inserted = false;
    @Getter
    @Setter
    private String permissions;

    public BeforePlayer(String name) {
        this.name = name;
    }

    public void updateToDatabase() {
        try {
            if (!this.inserted) {
                this.inserted = true;

                Document document = new Document("name", this.name);
                document.append("rank", this.rank);
                document.append("permissions", this.permissions);

                CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("bPlayers").insertOne(document);
                return;
            }

            Document document = new Document("rank", this.rank);
            document.append("permissions", this.permissions);

            Bson filter = new Document("name", this.name);
            CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("bPlayers").updateOne(filter, new Document("$set", document));
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public boolean addPermission(String permission, String serverGroup) {
        String permissionData = null;
        permissionData = serverGroup != null ? String.valueOf(permission.toLowerCase()) + ";" + serverGroup.toLowerCase() : String.valueOf(permission.toLowerCase()) + ";" + "nogroup";
        if (this.getPermissions() == null || !this.getPermissions().contains(permissionData)) {
            this.permissions = this.getPermissions() == null || this.getPermissions().isEmpty() ? permissionData : String.valueOf(this.permissions) + "@" + permissionData;
            this.updateToDatabase();
            return true;
        }
        return false;
    }

    public void remove() {
        new Thread(() -> {
            try {
                CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("bPlayers").deleteOne(new Document("name", this.name));
            } catch (Exception exc) {
                exc.printStackTrace();
            }
        }
        ).start();
    }
}

