package net.vicemice.hector.network.gameserver.lobby.lobbysign;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.server.SignPacket;

public class Sign {

    @Getter
    private SignPacket signPacket;
    @Getter
    private int x;
    @Getter
    private int y;
    @Getter
    private int z;
    @Getter
    private String world;
    @Getter
    private String templateName;
    @Getter
    @Setter
    private Server server;
    @Getter
    private String manualServerName;

    public Sign(int x, int y, int z, String world, String templateName, String manualServerName) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.world = world;
        this.templateName = templateName;
        this.manualServerName = manualServerName;
    }

    public void sendLines() {
        if (this.signPacket == null) {
            return;
        }
        PacketHolder signPacket = GoAPI.getPacketHelper().preparePacket(PacketType.SIGN_PACKET, this.signPacket);
        for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
            if (!server.isCloudServer() || !server.getTemplateName().equalsIgnoreCase("silent") && !server.isLobby()) continue;
            server.sendPacket(signPacket);
        }
    }

    public void updateLines() {
        if (this.server != null) {
            if (this.manualServerName == null) {
                String serverStatus = "§aLobby";
                if (this.server.getOnlinePlayers() >= this.server.getMaxPlayers()) {
                    serverStatus = "§6Lobby";
                }
                this.signPacket = new SignPacket(new String[]{"§l" + this.server.getName(), serverStatus, this.server.getMessage(), String.valueOf(this.server.getOnlinePlayers()) + "/" + this.server.getServerTemplate().getMaxPlayers()}, this.x, this.y, this.z, this.world, this.server.getName(), true);
            } else {
                String serverStatus = "§aLobby";
                if (this.server.getOnlinePlayers() >= this.server.getMaxPlayers()) {
                    serverStatus = "§6Lobby";
                }
                this.signPacket = new SignPacket(new String[]{"§l" + this.server.getName(), serverStatus, "", String.valueOf(this.server.getOnlinePlayers()) + "/" + this.server.getMaxPlayers()}, this.x, this.y, this.z, this.world, this.server.getName(), true);
            }
        } else {
            //ServerTemplate serverTemplate;
            //this.signPacket = this.manualServerName == null ? ((serverTemplate = CloudServer.getServerManager().getTemplatebyName(this.templatename)) != null ? (serverTemplate.isActive() ? new SignPacket(new String[]{"§4", "Server wird", "startet...", "§4"}, this.x, this.y, this.z, this.world, "no", false) : new SignPacket(new String[]{"§l" + serverTemplate.getName() + "-?", "§4§lModus", "§4§loffline", "§l-/-"}, this.x, this.y, this.z, this.world, "no", false)) : new SignPacket(new String[]{"§l" + this.templatename, "§4§lTemplate", "§4§lfehlt", "§l-/-"}, this.x, this.y, this.z, this.world, "no", false)) : new SignPacket(new String[]{this.manualServerName, "§4\u2589\u2589\u2589\u2589\u2589\u2589\u2589\u2589\u2589", "§cOffline", "§4\u2589\u2589\u2589\u2589\u2589\u2589\u2589\u2589\u2589"}, this.x, this.y, this.z, this.world, "no", false);
            this.signPacket = new SignPacket(new String[]{"", "Waiting for", "Server...", ""}, this.x, this.y, this.z, this.world, "waiting_for_server", false);
        }
    }
}

