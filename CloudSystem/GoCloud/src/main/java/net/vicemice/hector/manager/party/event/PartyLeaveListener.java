package net.vicemice.hector.manager.party.event;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.modules.api.event.player.party.PartyLeaveEvent;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.labymod.LabyModPacket;
import net.vicemice.hector.packets.types.player.party.PartyPacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.party.Party;
import net.vicemice.hector.utils.event.IEventListener;
import java.util.Collections;

/*
 * Class created at 07:14 - 29.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PartyLeaveListener implements IEventListener<PartyLeaveEvent> {

    @Override
    public void onCall(PartyLeaveEvent event) {
        Party party = event.getParty();
        CloudPlayer target = event.getTarget();

        if (party.getLeader() == target) {
            if (party.getMembers().size() == 1 || party.getMembers().size() == 0) {
                party.deleteParty();
            } else {
                target.sendMessage("party-player-left-target");
                party.getMembers().remove(target);

                party.sendMessageToParty("party-player-left", (target.getRank().getRankColor()+target.getName()));

                Collections.shuffle(party.getMembers());
                party.setNewPartyLeader(party.getMembers().get(0));
            }
        } else {
            target.sendMessage("party-player-left-target");
            party.getMembers().remove(target);
            party.sendMessageToParty("party-player-left", (target.getRank().getRankColor()+target.getName()));
        }

        for (CloudPlayer player : party.getMembers()) {
            if (player.getProxy() == null) continue;
            player.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.LABYMOD_PACKET, new LabyModPacket(player.getUniqueId(), true, new PartyPacket(party.getId(), party.getMembers().size(), party.getMax()), player.getJoinSecret(), player.getMatchSecret(), player.getSpecSecret())));
        }
        target.setParty(null);
    }
}
