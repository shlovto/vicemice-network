package net.vicemice.hector;

import net.vicemice.hector.manager.config.ConfigManager;
import net.vicemice.hector.utils.terminal.JLineTerminal;
import net.vicemice.hector.utils.terminal.VanillaTerminal;
import net.vicemice.hector.utils.terminal.log.JettySystemLogger;
import net.vicemice.hector.utils.terminal.log.SystemLogger;
import net.vicemice.hector.utils.JSONUtil;
import org.eclipse.jetty.util.log.Log;

import java.lang.management.ManagementFactory;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CloudStart {

    public static void main(String[] args) {
        try {
            if (!"UTF-8".equalsIgnoreCase(System.getProperty("file.encoding"))) {
                System.out.println("Changing default file-encoding to UTF-8");
                System.setProperty("file.encoding", "UTF-8");
            }
            if (!CloudService.newService()) {
                System.err.println("The Software is already running in a thread, please restart it completely.");
                return;
            }

            if (System.console() != null) {
                //CloudService.getCloudService().getTerminal().writeMessage("Setting up JLineTerminal");
                CloudService.getCloudService().setTerminal(new JLineTerminal());
            } else {
                //CloudService.getCloudService().getTerminal().writeMessage("Setting up VanillaTerminal");
                CloudService.getCloudService().setTerminal(new VanillaTerminal());
            }
            CloudService.getCloudService().getTerminal().install();
            CloudService.getCloudService().setLogger(new SystemLogger());
            Log.setLog(new JettySystemLogger("jetty"));
            CloudService.getCloudService().getTerminal().writeMessage("§f  ___ ___                 __                ");
            CloudService.getCloudService().getTerminal().writeMessage("§f /   |   \\   ____   _____/  |_  ___________ ");
            CloudService.getCloudService().getTerminal().writeMessage("§f/    ~    \\_/ __ \\_/ ___\\   __\\/  _ \\_  __ \\");
            CloudService.getCloudService().getTerminal().writeMessage("§f\\    Y    /\\  ___/\\  \\___|  | (  <_> )  | \\/");
            CloudService.getCloudService().getTerminal().writeMessage("§f \\___|_  /  \\___  >\\___  >__|  \\____/|__|   ");
            CloudService.getCloudService().getTerminal().writeMessage("§f       \\/       \\/     \\/                   ");
            CloudService.getCloudService().getTerminal().writeMessage("§3Preparing §c§lHector §3Systems");
            CloudService.getCloudService().getTerminal().writeMessage("§3Preparing to start the Hector Cloud Software §8(§eVersion " + CloudService.getVersion() + "§8)");
            if (args.length == 1 && args[0].equalsIgnoreCase("javaVersion")) {
                CloudService.getCloudService().getTerminal().writeMessage(" ");
                CloudService.getCloudService().getTerminal().writeMessage("Java Version is " + ManagementFactory.getRuntimeMXBean().getVmVersion());
            } else if (args.length == 1 && args[0].equalsIgnoreCase("locales")) {
                CloudService.getCloudService().getTerminal().writeMessage(" ");
                for (Locale locale : Locale.getAvailableLocales()) {
                    CloudService.getCloudService().getTerminal().writeMessage("{\"key\":\"" + locale.getDisplayName() + "\",\"tag\":\"" + locale.toLanguageTag() + "\"}");
                }
            } else if (args.length == 1 && args[0].equalsIgnoreCase("dateManager")) {
                ConfigManager configManager = null;
                try (ConfigManager manager = ConfigManager.newManager()) {
                    configManager = manager;
                    CloudService.getCloudService().getTerminal().write("§aSuccessfully initialized 'ConfigManager' without starting the Software completely...");
                } catch (Exception ex) {
                    CloudService.getCloudService().getTerminal().write("§cAn error occurred while initializing 'ConfigManager'");
                    ex.printStackTrace();
                    System.exit(0);
                }

                if (configManager == null) {
                    CloudService.getCloudService().getTerminal().write("§c'ConfigManager' could not be initialized, unknown error...");
                    System.exit(0);
                }

                String myDate = GoAPI.getTimeManager().getTime("yyyy/MM/dd", System.currentTimeMillis());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = sdf.parse((GoAPI.getTimeManager().getTime("yyyy/MM/dd", System.currentTimeMillis()) + " 00:00:00"));
                long millis = GoAPI.getTimeManager().addDays(date, 1).getTime();

                CloudService.getCloudService().getTerminal().write("Today date is '" + (GoAPI.getTimeManager().getTime("yyyy/MM/dd", System.currentTimeMillis())) + "'");
                CloudService.getCloudService().getTerminal().write("Next day date is '" + (new SimpleDateFormat("yyyy/MM/dd").format(millis)) + "'");
                CloudService.getCloudService().getTerminal().write("Remaining time to '" + myDate + "': " + GoAPI.getTimeManager().getRemainingTimeString(configManager.getLocaleManager(), Locale.GERMANY, millis));
            } else if (args.length == 1 && args[0].equalsIgnoreCase("imageData")) {
                //TODO: Fix this that this is working for TeamSpeak Images

                CloudService.getCloudService().getTerminal().writeMessage(" ");
                GoAPI.getForumAPI().changeAvatar(46, GoAPI.getImageData("https://minotar.net/avatar/elrobtossohn"), o -> {
                    if (!o.isNull("errors")) {
                        CloudService.getCloudService().getTerminal().writeMessage("§cUnbekannter Fehlercode! ('§7" + o.getJSONArray("errors").getJSONObject(0).getString("code") + "§c')");
                        CloudService.getCloudService().getTerminal().writeMessage(o.getJSONArray("errors").getJSONObject(0).getString("message"));
                        return;
                    }

                    if (!o.isNull("success")) {
                        CloudService.getCloudService().getTerminal().writeMessage("§aAvatar geändert!");
                        return;
                    }

                    CloudService.getCloudService().getTerminal().writeMessage("§cKonnte Avatar nicht ändern");
                });
            } else if (args.length == 1 && args[0].equalsIgnoreCase("timeMillis")) {
                CloudService.getCloudService().getTerminal().writeMessage(" ");
                CloudService.getCloudService().getTerminal().writeMessage("currentTimeMillis: " + System.currentTimeMillis());
            } else if (args.length == 1 && args[0].equalsIgnoreCase("mcstats_test")) {
                CloudService.getCloudService().getTerminal().writeMessage(" ");
                CloudService.getCloudService().getTerminal().writeMessage("Backend will be disabled... Try to start the MCStatsAPI Request");

                String checkName = new JSONUtil(JSONUtil.API.MCSTATS, "c80be485b4f946a7afa7947a50f76739").getObject().getJSONObject("response").getString("name");

                CloudService.getCloudService().getTerminal().writeMessage("Name: " + checkName);
            } else {
                try {
                    CloudService.getCloudService().getTerminal().writeMessage("§6Loading Libraries, please wait...");
                    Thread.sleep(1000);
                    CloudService.getCloudService().getTerminal().writeMessage("§aLibraries loaded");

                    CloudService.getCloudService().getTerminal().writeMessage("§6Loading startup, please wait...");
                    Thread.sleep(1000);
                    CloudService.getCloudService().getTerminal().writeMessage("§aStarting Software...");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                CloudService.getCloudService().start();
            }
        } catch (Exception ex) {
            if (CloudService.getCloudService() != null && CloudService.getCloudService().getTerminal() != null) {
                CloudService.getCloudService().getTerminal().writeMessage(" ");
                CloudService.getCloudService().getTerminal().writeMessage("§cAn error occurred while starting the software");
            } else {
                System.out.println(" ");
                System.out.println("An error occurred while starting the software");
            }
            ex.printStackTrace();
        }
    }
}