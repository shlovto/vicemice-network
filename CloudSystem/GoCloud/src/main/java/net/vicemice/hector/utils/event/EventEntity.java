package net.vicemice.hector.utils.event;

public class EventEntity<E extends Event> {
    private IEventListener<E> eventListener;
    private EventKey eventKey;
    private Class<? extends Event> eventClazz;

    public IEventListener<E> getEventListener() {
        return this.eventListener;
    }

    public EventKey getEventKey() {
        return this.eventKey;
    }

    public Class<? extends Event> getEventClazz() {
        return this.eventClazz;
    }

    public EventEntity(IEventListener<E> eventListener, EventKey eventKey, Class<? extends Event> eventClazz) {
        this.eventListener = eventListener;
        this.eventKey = eventKey;
        this.eventClazz = eventClazz;
    }
}

