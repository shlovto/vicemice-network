package net.vicemice.hector.network;

import com.google.gson.Gson;
import io.netty.channel.epoll.Epoll;

import java.text.DecimalFormat;
import java.util.concurrent.ThreadLocalRandom;

public class ClydeAPI {
    public static final boolean EPOLL = Epoll.isAvailable();
    public static final Gson GSON = new Gson();
    public static final ThreadLocalRandom RANDOM = ThreadLocalRandom.current();
    public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("##.##");
    public static final String DEV_PROPERTY = "_PLOXH_DEV_SERVICE_UNIQUEID_";
    public static final String EMPTY_STRING = "";
    public static final String SPACE_STRING = " ";
    public static final String SLASH_STRING = "/";
    private static final char[] VALUES = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    public static String randomString(int size) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < size; i = (int)((short)(i + 1))) {
            stringBuilder.append(VALUES[RANDOM.nextInt(VALUES.length)]);
        }
        return stringBuilder.substring(0);
    }
}