package net.vicemice.hector.nettyserver.rank;

import io.netty.channel.Channel;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.PermissionsPacket;
import net.vicemice.hector.utils.Rank;
import org.bson.Document;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RankManager {

    private HashMap<String, List<String>> permissionsMap = new HashMap();

    public void reloadPermissions() {
        try {
            ArrayList<Document> list = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("permissions");

            for (Document document : list) {
                String getRank = document.getString("rank");
                String permission = document.getString("permission");
                Rank rank = Rank.fromStringExact(getRank);
                if (rank != null) {
                    List<String> permissions;
                    if (!this.permissionsMap.containsKey(rank.getRankName())) {
                        this.permissionsMap.put(rank.getRankName(), new ArrayList());
                    }
                    if (!(permissions = this.permissionsMap.get(rank.getRankName())).contains(permission)) {
                        permissions.add(permission);
                        this.permissionsMap.put(rank.getRankName(), permissions);
                        continue;
                    }
                    System.out.println("Permission '" + permission + "' is a duplicate in Rank '" + rank.getRankName() + "'");
                    continue;
                }
                System.out.println(getRank + " does not exists in Enumeration!");
            }

            this.permissionsMap.clear();
            for (Channel channel : CloudService.getCloudService().getNettyService().getRankServer().getRankClients()) {
                if (!channel.isActive() || !channel.isOpen()) continue;
                channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.PERMISSIONS_PACKET, this.getPermissionsPacket()));
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public PermissionsPacket getPermissionsPacket() {
        return new PermissionsPacket(this.permissionsMap);
    }
}