package net.vicemice.hector.network.modules;

import net.vicemice.hector.network.modules.exception.ModuleLoadException;

import java.io.File;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ModuleDetector {
    public Set<ModuleConfig> detectAvailable(File dir) {
        HashSet<ModuleConfig> moduleConfigs = new HashSet<ModuleConfig>();
        File[] files = dir.listFiles(pathname -> pathname.isFile() && pathname.exists() && pathname.getName().endsWith(".jar"));
        if (files == null) {
            return moduleConfigs;
        }
        for (File file : files) {
            try {
                try (JarFile jarFile = new JarFile(file);){
                    JarEntry jarEntry = jarFile.getJarEntry("module.properties");
                    if (jarEntry == null) {
                        throw new ModuleLoadException("Cannot find \"module.properties\" file");
                    }
                    try (InputStreamReader reader = new InputStreamReader(jarFile.getInputStream(jarEntry), StandardCharsets.UTF_8);){
                        Properties properties = new Properties();
                        properties.load(reader);
                        ModuleConfig moduleConfig = new ModuleConfig(file, properties.getProperty("name"), properties.getProperty("version"), properties.getProperty("author"), properties.getProperty("main"), Boolean.valueOf(properties.getProperty("reloadAble")));
                        moduleConfigs.add(moduleConfig);
                    }
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return moduleConfigs;
    }
}

