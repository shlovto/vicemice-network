package net.vicemice.hector.network.modules.api.event.discord;

import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.event.Event;

public class DiscordCheckGroupsEvent extends Event {
    private CloudPlayer cloudPlayer;

    public DiscordCheckGroupsEvent(CloudPlayer cloudPlayer) {
        this.cloudPlayer = cloudPlayer;
    }

    public CloudPlayer getCloudPlayer() {
        return this.cloudPlayer;
    }
}