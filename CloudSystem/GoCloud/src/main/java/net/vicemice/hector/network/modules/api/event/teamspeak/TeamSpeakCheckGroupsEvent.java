package net.vicemice.hector.network.modules.api.event.teamspeak;

import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.event.Event;

public class TeamSpeakCheckGroupsEvent extends Event {
    private CloudPlayer cloudPlayer;

    public TeamSpeakCheckGroupsEvent(CloudPlayer cloudPlayer) {
        this.cloudPlayer = cloudPlayer;
    }

    public CloudPlayer getCloudPlayer() {
        return this.cloudPlayer;
    }
}