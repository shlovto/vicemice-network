package net.vicemice.hector.network.modules.api.event.player.party;

import net.vicemice.hector.player.party.Party;
import net.vicemice.hector.utils.event.async.AsyncEvent;
import net.vicemice.hector.utils.event.async.AsyncPosterAdapter;

/*
 * Class created at 07:07 - 29.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PartyDeleteEvent extends AsyncEvent<PartyDeleteEvent> {
    private Party party;

    public PartyDeleteEvent(Party party) {
        super(new AsyncPosterAdapter<>());
        this.party = party;
    }

    public Party getParty() {
        return party;
    }
}
