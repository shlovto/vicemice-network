package net.vicemice.hector.player.utils;

import lombok.Getter;
import lombok.Setter;

import java.util.concurrent.ConcurrentHashMap;

/*
 * Class created at 17:43 - 01.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class Stats {
    @Getter
    @Setter
    private ConcurrentHashMap<String, Long> globalStats = new ConcurrentHashMap<>();
    @Getter
    @Setter
    private ConcurrentHashMap<String, Long> monthlyStats = new ConcurrentHashMap<>();
    @Getter
    @Setter
    private ConcurrentHashMap<String, Long> dailyStats = new ConcurrentHashMap<>();
    @Getter
    @Setter
    private ConcurrentHashMap<String, Long> globalRanking = new ConcurrentHashMap<>();
    @Getter
    @Setter
    private ConcurrentHashMap<String, Long> monthlyRanking = new ConcurrentHashMap<>();
    @Getter
    @Setter
    private ConcurrentHashMap<String, Long> dailyRanking = new ConcurrentHashMap<>();

    public long getDailyStatsValue(String key) {
        if (this.dailyStats.containsKey(key)) {
            return this.dailyStats.get(key);
        }
        return 0;
    }

    public long getMonthlyStatsValue(String key) {
        if (this.monthlyStats.containsKey(key)) {
            return this.monthlyStats.get(key);
        }
        return 0;
    }

    public long getGlobalStatsValue(String key) {
        if (this.globalStats.containsKey(key)) {
            return this.globalStats.get(key);
        }
        return 0;
    }
}