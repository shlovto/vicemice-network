package net.vicemice.hector.utils.utility.scheduler;

import net.vicemice.hector.utils.threading.Callback;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Deque;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TaskScheduler {
    private static final TaskScheduler RUNTIME_SCHEDULER = new TaskScheduler(Runtime.getRuntime().availableProcessors());
    protected final ThreadGroup threadGroup = new ThreadGroup("TaskScheduler-Group-" + new Random().nextLong());
    protected final AtomicLong threadId = new AtomicLong(0L);
    protected final String name = this.threadGroup.getName();
    protected final long sleepThreadSwitch;
    protected final boolean dynamicWorkerCount;
    protected final long threadLiveMillis;
    protected int maxThreads = 0;
    protected Logger logger;
    protected Deque<TaskEntry<?>> taskEntries = new ConcurrentLinkedDeque();
    protected Collection<Worker> workers = new ConcurrentLinkedQueue<Worker>();

    public TaskScheduler() {
        this(Runtime.getRuntime().availableProcessors());
    }

    public TaskScheduler(long sleepThreadSwitch) {
        this(Runtime.getRuntime().availableProcessors(), sleepThreadSwitch);
    }

    public TaskScheduler(Logger logger) {
        this(Runtime.getRuntime().availableProcessors(), logger);
    }

    public TaskScheduler(Logger logger, long sleepThreadSwitch) {
        this(Runtime.getRuntime().availableProcessors(), logger, sleepThreadSwitch);
    }

    public TaskScheduler(Collection<TaskEntry<?>> entries) {
        this(Runtime.getRuntime().availableProcessors(), entries);
    }

    public TaskScheduler(Collection<TaskEntry<?>> entries, long sleepThreadSwtich) {
        this(Runtime.getRuntime().availableProcessors(), entries, sleepThreadSwtich);
    }

    public TaskScheduler(Collection<TaskEntry<?>> entries, Logger logger) {
        this(Runtime.getRuntime().availableProcessors(), entries, logger);
    }

    public TaskScheduler(Collection<TaskEntry<?>> entries, Logger logger, long sleepThreadSwtich) {
        this(Runtime.getRuntime().availableProcessors(), entries, logger, sleepThreadSwtich);
    }

    public TaskScheduler(int maxThreads) {
        this(maxThreads, (Logger)null);
    }

    public TaskScheduler(int maxThreads, boolean dynamicWorkerCount) {
        this(maxThreads, (Logger)null, dynamicWorkerCount);
    }

    public TaskScheduler(int maxThreads, long sleepThreadSwitch) {
        this(maxThreads, (Logger)null, sleepThreadSwitch);
    }

    public TaskScheduler(int maxThreads, Collection<TaskEntry<?>> entries) {
        this(maxThreads, entries, null);
    }

    public TaskScheduler(int maxThreads, Collection<TaskEntry<?>> entries, long sleepThreadSwitch) {
        this(maxThreads, entries, null, sleepThreadSwitch);
    }

    public TaskScheduler(int maxThreads, Logger logger) {
        this(maxThreads, null, logger);
    }

    public TaskScheduler(int maxThreads, Logger logger, boolean dynamicWorkerCount) {
        this(maxThreads, null, logger, 10L, dynamicWorkerCount);
    }

    public TaskScheduler(int maxThreads, Logger logger, long sleepThreadSwitch) {
        this(maxThreads, null, logger, sleepThreadSwitch);
    }

    public TaskScheduler(int maxThreads, Collection<TaskEntry<?>> entries, Logger logger) {
        this(maxThreads, entries, logger, 10L);
    }

    public TaskScheduler(int maxThreads, Collection<TaskEntry<?>> entries, Logger logger, boolean dynamicWorkerCount) {
        this(maxThreads, entries, logger, 10L, dynamicWorkerCount);
    }

    public TaskScheduler(int maxThreads, Collection<TaskEntry<?>> entries, Logger logger, long sleepThreadSwitch) {
        this(maxThreads, entries, logger, sleepThreadSwitch, false);
    }

    public TaskScheduler(int maxThreads, Collection<TaskEntry<?>> entries, Logger logger, long sleepThreadSwitch, boolean dynamicThreadCount) {
        this(maxThreads, entries, logger, sleepThreadSwitch, dynamicThreadCount, 10000L);
    }

    public TaskScheduler(int maxThreads, Collection<TaskEntry<?>> entries, Logger logger, long sleepThreadSwitch, boolean dynamicThreadCount, long threadLiveMillis) {
        this.sleepThreadSwitch = sleepThreadSwitch;
        this.dynamicWorkerCount = dynamicThreadCount;
        this.threadLiveMillis = threadLiveMillis;
        this.maxThreads = maxThreads <= 0 ? Runtime.getRuntime().availableProcessors() : maxThreads;
        Logger logger2 = this.logger = logger != null ? logger : Logger.getLogger("TaskScheduler-Logger@" + this.threadGroup.getName());
        if (entries != null) {
            this.taskEntries.addAll(entries);
        }
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable) {
        return this.schedule(runnable, (Callback<Void>)null);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Date timeout) {
        return this.schedule(runnable, timeout.getTime() - System.currentTimeMillis());
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, LocalDate localDate, LocalTime localTime) {
        return this.schedule(runnable, null, localDate, localTime);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, LocalDateTime localDateTime) {
        return this.schedule(runnable, null, localDateTime);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, ZonedDateTime zonedDateTime) {
        return this.schedule(runnable, null, zonedDateTime);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Instant instant) {
        return this.schedule(runnable, null, instant);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Callback<Void> callback) {
        return this.schedule(runnable, callback, 0L);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Callback<Void> callback, LocalDate localDate, LocalTime localTime) {
        return this.schedule(runnable, callback, localDate, localTime, 0L);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Callback<Void> callback, LocalDateTime localDateTime) {
        return this.schedule(runnable, callback, localDateTime, 0L);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Callback<Void> callback, LocalDateTime localDateTime, long repeats) {
        return this.schedule(runnable, callback, localDateTime.atZone(ZoneId.systemDefault()), repeats);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Callback<Void> callback, LocalDate localDate, LocalTime localTime, long repeats) {
        return this.schedule(runnable, callback, LocalDateTime.of(localDate, localTime), repeats);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Callback<Void> callback, ZonedDateTime zonedDateTime) {
        return this.schedule(runnable, callback, zonedDateTime, 0L);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Callback<Void> callback, Instant instant) {
        return this.schedule(runnable, callback, instant, 0L);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Callback<Void> callback, Date timeout) {
        return this.schedule(runnable, callback, timeout.getTime() - System.currentTimeMillis());
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, long delay) {
        return this.schedule(runnable, (Callback<Void>)null, delay);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, long delay, TimeUnit timeUnit) {
        return this.schedule(runnable, (Callback<Void>)null, timeUnit.toMillis(delay));
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Callback<Void> callback, long delay) {
        return this.schedule(runnable, callback, delay, 0L);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Callback<Void> callback, long delay, TimeUnit timeUnit) {
        return this.schedule(runnable, (Callback<Void>)null, timeUnit.toMillis(delay));
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, long delay, long repeats) {
        return this.schedule(runnable, null, delay, repeats);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Callback<Void> callback, ZonedDateTime zonedDateTime, long repeats) {
        return this.schedule(runnable, callback, zonedDateTime.toInstant(), repeats);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Callback<Void> callback, Instant instant, long repeats) {
        return this.schedule(runnable, callback, instant.toEpochMilli() - System.currentTimeMillis(), repeats);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Date timeout, long repeats) {
        return this.schedule(runnable, timeout.getTime() - System.currentTimeMillis(), repeats);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, long delay, TimeUnit timeUnit, long repeats) {
        return this.schedule(runnable, null, timeUnit.toMillis(delay), repeats);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Callback<Void> callback, Date timeout, long repeats) {
        return this.schedule(runnable, callback, timeout.getTime() - System.currentTimeMillis(), repeats);
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Callback<Void> callback, long delay, long repeats) {
        return this.schedule(new VoidTaskEntry(runnable, callback, delay, repeats));
    }

    public TaskEntryFuture<Void> schedule(Runnable runnable, Callback<Void> callback, long delay, TimeUnit timeUnit, long repeats) {
        return this.schedule(runnable, callback, timeUnit.toMillis(delay), repeats);
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable) {
        return this.schedule(callable, (Callback)null);
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, LocalDate localDate, LocalTime localTime) {
        return this.schedule(callable, null, localDate, localTime);
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, LocalDateTime localDateTime) {
        return this.schedule(callable, null, localDateTime);
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, ZonedDateTime zonedDateTime) {
        return this.schedule(callable, null, zonedDateTime);
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, Instant instant) {
        return this.schedule(callable, null, instant);
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, long delay) {
        return this.schedule(callable, null, delay);
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, long delay, TimeUnit timeUnit) {
        return this.schedule(callable, null, timeUnit.toMillis(delay));
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, Callback<V> callback) {
        return this.schedule(callable, callback, 0L);
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, Callback<V> callback, LocalDate localDate, LocalTime localTime) {
        return this.schedule(callable, callback, localDate, localTime, 0L);
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, Callback<V> callback, LocalDateTime localDateTime) {
        return this.schedule(callable, callback, localDateTime, 0L);
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, Callback<V> callback, ZonedDateTime zonedDateTime) {
        return this.schedule(callable, callback, zonedDateTime, 0L);
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, Callback<V> callback, Instant instant) {
        return this.schedule(callable, callback, instant, 0L);
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, Callback<V> callback, long delay) {
        return this.schedule(callable, callback, delay, 0L);
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, Callback<V> callback, long delay, TimeUnit timeUnit) {
        return this.schedule(callable, callback, timeUnit.toMillis(delay));
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, Callback<V> callback, long delay, long repeats) {
        return this.schedule(new TaskEntry<V>(callable, callback, delay, repeats));
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, Callback<V> callback, long delay, TimeUnit timeUnit, long repeats) {
        return this.schedule(callable, callback, timeUnit.toMillis(delay), repeats);
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, Callback<V> callback, LocalDate localDate, LocalTime localTime, long repeats) {
        return this.schedule(callable, callback, LocalDateTime.of(localDate, localTime), repeats);
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, Callback<V> callback, LocalDateTime localDateTime, long repeats) {
        return this.schedule(callable, callback, localDateTime.atZone(ZoneId.systemDefault()), 0L);
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, Callback<V> callback, ZonedDateTime zonedDateTime, long repeats) {
        return this.schedule(callable, callback, zonedDateTime.toInstant(), 0L);
    }

    public <V> TaskEntryFuture<V> schedule(Callable<V> callable, Callback<V> callback, Instant instant, long repeats) {
        return this.schedule(callable, callback, instant.toEpochMilli(), 0L);
    }

    public <V> TaskEntryFuture<V> schedule(TaskEntry<V> taskEntry) {
        return this.offerEntry(taskEntry);
    }

    public <V> Collection<TaskEntryFuture<V>> schedule(Collection<TaskEntry<V>> threadEntries) {
        ArrayList<TaskEntryFuture<V>> TaskEntryFutures = new ArrayList<TaskEntryFuture<V>>();
        for (TaskEntry<V> entry : threadEntries) {
            TaskEntryFutures.add(this.offerEntry(entry));
        }
        return TaskEntryFutures;
    }

    protected void newWorker() {
        Worker worker = new Worker();
        this.workers.add(worker);
        worker.start();
    }

    public Collection<TaskEntry<?>> shutdown() {
        for (Worker worker : this.workers) {
            try {
                worker.interrupt();
                worker.stop();
            }
            catch (ThreadDeath th) {
                this.workers.remove(worker);
            }
        }
        ArrayList entries = new ArrayList(this.taskEntries);
        this.taskEntries.clear();
        this.workers.clear();
        this.threadId.set(0L);
        return entries;
    }

    public TaskScheduler chargeThreadLimit(short threads) {
        this.maxThreads += threads;
        return this;
    }

    public int getCurrentThreadSize() {
        return this.workers.size();
    }

    public int getMaxThreads() {
        return this.maxThreads;
    }

    public ThreadGroup getThreadGroup() {
        return this.threadGroup;
    }

    public String getName() {
        return this.name;
    }

    public Deque<TaskEntry<?>> getThreadEntries() {
        return new ConcurrentLinkedDeque();
    }

    public Logger getLogger() {
        return this.logger;
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    private void checkEnougthThreads() {
        Worker worker = this.hasFreeWorker();
        if (this.getCurrentThreadSize() < this.maxThreads || this.dynamicWorkerCount && this.maxThreads > 1 && this.taskEntries.size() > this.getCurrentThreadSize() && this.taskEntries.size() <= this.getMaxThreads() * 2 && worker == null) {
            this.newWorker();
        }
    }

    private Worker hasFreeWorker() {
        for (Worker worker : this.workers) {
            if (!worker.isFreeWorker()) continue;
            return worker;
        }
        return null;
    }

    private <V> TaskEntryFuture<V> offerEntry(TaskEntry<V> entry) {
        this.taskEntries.offer(entry);
        this.checkEnougthThreads();
        return entry.drop();
    }

    public static TaskScheduler runtimeScheduler() {
        return RUNTIME_SCHEDULER;
    }

    private final class VoidTaskEntry
    extends TaskEntry<Void> {
        public VoidTaskEntry(Callable<Void> pTask, Callback<Void> pComplete, long pDelay, long pRepeat) {
            super(pTask, pComplete, pDelay, pRepeat);
        }

        public VoidTaskEntry(final Runnable ptask, Callback<Void> pComplete, long pDelay, long pRepeat) {
            super(new Callable<Void>(){

                @Override
                public Void call() throws Exception {
                    if (ptask != null) {
                        ptask.run();
                    }
                    return null;
                }
            }, pComplete, pDelay, pRepeat);
        }

    }

    public class Worker
    extends Thread {
        volatile TaskEntry<?> taskEntry;
        private long liveTimeStamp;

        Worker() {
            super(TaskScheduler.this.threadGroup, TaskScheduler.this.threadGroup.getName() + "#" + TaskScheduler.this.threadId.addAndGet(1L));
            this.taskEntry = null;
            this.liveTimeStamp = System.currentTimeMillis();
            this.setDaemon(true);
        }

        public boolean isFreeWorker() {
            return this.taskEntry == null;
        }

        @Override
        public synchronized void run() {
            while (this.liveTimeStamp + TaskScheduler.this.threadLiveMillis > System.currentTimeMillis()) {
                this.execute();
                this.sleepUninterruptedly(TaskScheduler.this.sleepThreadSwitch);
            }
            TaskScheduler.this.workers.remove(this);
        }

        public synchronized void execute() {
            while (!TaskScheduler.this.taskEntries.isEmpty() && !this.isInterrupted()) {
                this.taskEntry = TaskScheduler.this.taskEntries.poll();
                if (this.taskEntry == null || this.taskEntry.task == null) continue;
                this.liveTimeStamp = System.currentTimeMillis();
                if (this.taskEntry.delayTimeOut != 0L && System.currentTimeMillis() < this.taskEntry.delayTimeOut) {
                    if (TaskScheduler.this.maxThreads != 1) {
                        long difference = this.taskEntry.delayTimeOut - System.currentTimeMillis();
                        if (difference > TaskScheduler.this.sleepThreadSwitch) {
                            this.sleepUninterruptedly(TaskScheduler.this.sleepThreadSwitch - 1L);
                            this.offerEntry(this.taskEntry);
                            continue;
                        }
                        this.sleepUninterruptedly(difference);
                    } else {
                        this.sleepUninterruptedly(TaskScheduler.this.sleepThreadSwitch);
                        this.offerEntry(this.taskEntry);
                        continue;
                    }
                }
                try {
                    this.taskEntry.invoke();
                }
                catch (Exception e) {
                    TaskScheduler.this.logger.log(Level.SEVERE, "Error on handling Task on Thread [" + this.getName() + "]", e);
                }
                if (!this.checkEntry()) continue;
                this.taskEntry = null;
            }
        }

        public TaskEntry<?> getTaskEntry() {
            return this.taskEntry;
        }

        private void offerEntry(TaskEntry<?> entry) {
            TaskScheduler.this.taskEntries.offer(this.taskEntry);
            this.taskEntry = null;
        }

        private boolean checkEntry() {
            if (this.taskEntry.repeat == -1L) {
                this.offerEntry(this.taskEntry);
                return false;
            }
            if (this.taskEntry.repeat > 0L) {
                this.offerEntry(this.taskEntry);
                return false;
            }
            return true;
        }

        private synchronized void sleepUninterruptedly(long millis) {
            try {
                Thread.sleep(millis);
            }
            catch (InterruptedException interruptedException) {
                // empty catch block
            }
        }
    }

}

