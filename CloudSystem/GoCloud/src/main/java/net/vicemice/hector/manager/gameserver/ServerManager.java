package net.vicemice.hector.manager.gameserver;

import io.netty.channel.Channel;
import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.network.gameserver.lobby.Lobby;
import net.vicemice.hector.network.slave.Slave;
import net.vicemice.hector.network.proxy.Proxy;
import net.vicemice.hector.packets.types.ErrorPacket;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.ServerCallBackInfos;
import net.vicemice.hector.packets.types.lobby.LobbyIngameServerPacket;
import net.vicemice.hector.packets.types.player.PlayerTitlePacket;
import net.vicemice.hector.packets.types.proxy.ProxyAddServerPacket;
import net.vicemice.hector.packets.types.proxy.ProxyPlayerPacket;
import net.vicemice.hector.packets.types.proxy.ProxyRemoveServerPacket;
import net.vicemice.hector.packets.types.server.GameIDPacket;
import net.vicemice.hector.packets.types.server.PrivateOwnerPacket;
import net.vicemice.hector.packets.types.server.ServerPacket;
import net.vicemice.hector.packets.types.server.StatSignPacket;
import net.vicemice.hector.packets.types.slave.SlaveServerStart;
import net.vicemice.hector.packets.types.slave.SlaveTemplatePacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.ServerType;
import net.vicemice.hector.utils.scheduler.ScheduledExecution;
import net.vicemice.hector.utils.slave.ServerData;
import net.vicemice.hector.utils.slave.TemplateData;
import org.bson.Document;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ServerManager {

    private List<Server> servers = new CopyOnWriteArrayList<>();
    private List<TemplateData> templates = new CopyOnWriteArrayList<>();
    private boolean restarting = false;
    private ConcurrentHashMap<String, Long> nameReservation = new ConcurrentHashMap<>();
    @Getter
    private ConcurrentHashMap<String, ServerData> staticServers = new ConcurrentHashMap<>();

    private ArrayList<CloudPlayer> startingPrivateServer = new ArrayList<>();

    private double maxCpuAverage = 75.0;

    public ServerManager() {
        new Timer().scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                ServerManager.this.runTimer();
                for (String name : ServerManager.this.nameReservation.keySet()) {
                    long timeout = ServerManager.this.nameReservation.get(name);
                    if (System.currentTimeMillis() <= timeout) continue;
                    ServerManager.this.nameReservation.remove(name);
                }
            }
        }, 1000, 1000);
    }

    public void loadServers() {
        ArrayList<Document> servers = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("servers");

        for (Document server : servers) {
            ServerData serverData = new ServerData(server.getString("slave"), server.getString("name"), server.getInteger("port"), ServerType.valueOf(server.getString("type")));
            staticServers.put(server.getString("name"), serverData);
        }
    }

    public void addReservation(String name) {
        this.nameReservation.put(name.toLowerCase(), System.currentTimeMillis() + 10000);
    }

    public Server findServerByTemplate(String template) {
        for (Server server : getServers()) {
            if (server.getTemplateName() == null) continue;
            if (server.getTemplateName().equals("template")) {
                if (server.getServerState() != ServerState.INGAME) {
                    return server;
                }
            }
        }

        return null;
    }

    public void runTimer() {
        boolean checkServers = true;
        if (GoAPI.getTimeManager().getHour().equalsIgnoreCase("04")) {
            if (!this.restarting) {
                checkServers = false;
                this.restarting = true;
                for (Server next : this.servers) {
                    if (!next.isCloudServer()) continue;
                    next.remove();
                }
            }
        } else {
            this.restarting = false;
        }
        HashMap<String, Object> lobbySignData = new HashMap<String, Object>();
        lobbySignData.put("bedwars", new ArrayList<>());
        lobbySignData.put("qsg", new ArrayList<>());
        for (Server server : this.servers) {
            if (server.getTimeout() != -1 && System.currentTimeMillis() > server.getTimeout()) {
                CloudService.getCloudService().getTerminal().writeMessage("§e§l"+server.getName()+" §3is timed out...");
                server.remove();
                checkServers = false;
                continue;
            }
            if (server.getTimeout() != -1 && server.getServerState() == ServerState.STARTUP && server.getStateTimeout() != -1 && System.currentTimeMillis() > server.getStateTimeout()) {
                CloudService.getCloudService().getTerminal().writeMessage("§e§l"+server.getName()+" §3is timed out...");
                server.remove();
                checkServers = false;
                continue;
            }
            if (server.getTimeout() == -1 && server.getStartupTimeout() != -1 && System.currentTimeMillis() > server.getStartupTimeout()) {
                server.remove();
                checkServers = false;
                continue;
            }
            if (!server.isCloudServer() || server.getServerState() != ServerState.INGAME) continue;
            String templateName = server.getTemplateName();
            if (templateName.toLowerCase().startsWith("bw")) {
                templateName = "bedwars";
            }
            if (templateName.toLowerCase().startsWith("sw")) {
                templateName = "skywars";
            }
            if (templateName.toLowerCase().startsWith("ttt")) {
                templateName = "ttt";
            }
            if (!lobbySignData.containsKey(templateName)) {
                lobbySignData.put(templateName, new ArrayList());
            }
            List serverDatas = (List) lobbySignData.get(templateName);
            serverDatas.add(new String[]{server.getName(), server.getMessage(), String.valueOf(server.getOnlinePlayers()), String.valueOf(server.getMaxPlayers())});
            lobbySignData.put(templateName, serverDatas);
        }
        for (String serverTemplate : lobbySignData.keySet()) {
            List servers = (List) lobbySignData.get(serverTemplate);
            PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.LOBBY_INGAME_SERVERS, new LobbyIngameServerPacket(serverTemplate, servers));
            for (Lobby lobby : CloudService.getCloudService().getManagerService().getLobbyManager().getLobbyServers()) {
                lobby.getServer().sendPacket(initPacket);
            }
            for (Server server2 : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
                if (!server2.isCloudServer() || !server2.getTemplateName().equalsIgnoreCase("silent")) continue;
                server2.sendPacket(initPacket);
            }
        }
        if (checkServers) {
            this.checkServers();
        } else {
            checkServers = true;
        }
    }

    public int getServerCountOnSlave(Slave slave) {
        int count = 0;
        for (Server server : this.getServers()) {
            if (!server.isCloudServer() || !server.getSlave().equals(slave)) continue;
            ++count;
        }
        return count;
    }

    public void receiveNewPlayerPacket(ProxyPlayerPacket proxyPlayerPacket) {
        Proxy proxy = CloudService.getCloudService().getManagerService().getProxyManager().getProxyByName(proxyPlayerPacket.getProxyName());
        Server server = CloudService.getCloudService().getManagerService().getServerManager().getServerByName(proxyPlayerPacket.getServerName());
        int playerCount = proxyPlayerPacket.getPlayerSize();

        if (server != null && proxy != null) {
            CloudService.getCloudService().getManagerService().getServerManager().getServerByName(proxyPlayerPacket.getServerName()).getPlayers().put(proxy.getName(), playerCount);

            //if (CloudService.getCloudService().isDevMode()) CloudService.getCloudService().getTerminal().writeMessage("[ReceiveNewPlayerPacket] Proxy and Server are valid! {proxy='"+proxyPlayerPacket.getProxyName()+"',server='"+proxyPlayerPacket.getServerName()+"',count='"+proxyPlayerPacket.getPlayerSize()+"'}");
        }
    }

    public void handlePlayerCountOfflineProxy(Proxy proxy) {
        for (Server server : this.servers) {
            server.getPlayers().remove(proxy.getName());
        }
    }

    public void loadTemplates() {
        this.templates.clear();
        try {
            ArrayList<Document> list = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("templates");

            for (Document document : list) {
                String name = document.getString("name");
                String prefix = document.getString("prefix");
                String color = document.getString("color");
                String rank = document.getString("rank");
                int version = document.getInteger("version");
                int minServer = document.getInteger("minServer");
                int maxServer = document.getInteger("maxServer");
                int maxRamProServer = 0;
                int minRamProServer = 0;
                int lobbyServer = document.getInteger("lobbyserver");
                int maxPlayers = document.getInteger("maxPlayers");
                String statsPrefix = document.getString("statsPrefix");
                String startCommand = document.getString("startCommand");
                boolean nickAble = document.getBoolean("nickAble");
                boolean playAble = document.getBoolean("playAble");
                boolean privateTemplate = document.getBoolean("privateTemplate");
                TemplateData serverTemplate = new TemplateData(name, prefix, color, rank, version, minServer, maxServer, maxRamProServer, minRamProServer, lobbyServer, maxPlayers, statsPrefix, startCommand, nickAble, playAble, privateTemplate);
                serverTemplate.setForceSlave(document.getString("forceSlave"));
                if (name.equalsIgnoreCase("lobby") || name.equalsIgnoreCase("silent") || name.equalsIgnoreCase("fallback") || name.equalsIgnoreCase("verify")) {
                    serverTemplate.setActive(true);
                }
                this.templates.add(serverTemplate);
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void reloadTemplates() {
        this.templates.clear();
        try {
            ArrayList<Document> list = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("templates");

            for (Document document : list) {
                String name = document.getString("name");
                String prefix = document.getString("prefix");
                String color = document.getString("color");
                String rank = document.getString("rank");
                int version = document.getInteger("version");
                int minServer = document.getInteger("minServer");
                int maxServer = document.getInteger("maxServer");
                int maxRamProServer = 0;
                int minRamProServer = 0;
                int lobbyServer = document.getInteger("lobbyserver");
                int maxPlayers = document.getInteger("maxPlayers");
                String statsPrefix = document.getString("statsPrefix");
                String startCommand = document.getString("startCommand");
                boolean nickAble = document.getBoolean("nickAble");
                boolean playAble = document.getBoolean("playAble");
                boolean privateTemplate = document.getBoolean("privateTemplate");
                TemplateData serverTemplate = new TemplateData(name, prefix, color, rank, version, minServer, maxServer, maxRamProServer, minRamProServer, lobbyServer, maxPlayers, statsPrefix, startCommand, nickAble, playAble, privateTemplate);
                serverTemplate.setForceSlave(document.getString("forceSlave"));
                if (name.equalsIgnoreCase("lobby") || name.equalsIgnoreCase("silent") || name.equalsIgnoreCase("fallback") || name.equalsIgnoreCase("verify")) {
                    serverTemplate.setActive(true);
                }
                this.templates.add(serverTemplate);
                CloudService.getCloudService().getManagerService().getSlaveManager().broadcastPacket(GoAPI.getPacketHelper().preparePacket(PacketType.SLAVE_TEMPLATE_PACKET, new SlaveTemplatePacket(this.templates, false)));
            }
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void receiveTimeOut(String serverName) {
        Server server = this.getServerByName(serverName);
        if (server != null) {
            server.setTimeout(System.currentTimeMillis() + 45000);
        }
    }

    public List<String[]> getTemplatesArray(String download) {
        ArrayList<String[]> list = new ArrayList<String[]>();
        for (TemplateData serverTemplate : this.templates) {
            if (serverTemplate.getName().equalsIgnoreCase(download)) {
                list.add(serverTemplate.toStringArray(true));
                continue;
            }
            list.add(serverTemplate.toStringArray(false));
        }
        return list;
    }

    public List<String[]> getTemplatesArray() {
        ArrayList<String[]> list = new ArrayList<String[]>();
        for (TemplateData serverTemplate : this.templates) {
            list.add(serverTemplate.toStringArray(true));
        }
        return list;
    }

    public TemplateData getTemplateByName(String name) {
        for (TemplateData serverTemplate : this.templates) {
            if (!serverTemplate.getName().equalsIgnoreCase(name)) continue;
            return serverTemplate;
        }
        return null;
    }

    public List<TemplateData> getTemplatesByStatsPrefix(String prefix) {
        ArrayList<TemplateData> serverTemplates = new ArrayList<TemplateData>();
        for (TemplateData serverTemplate : this.templates) {
            if (serverTemplate.getStatsPrefix() == null || !serverTemplate.getStatsPrefix().equalsIgnoreCase(prefix))
                continue;
            serverTemplates.add(serverTemplate);
        }
        return serverTemplates;
    }

    public void handleCloudStop() {
        for (Server server : this.servers) {
            if (!server.isCloudServer()) continue;
            server.remove();
        }
    }

    public List<String> getTemplateNames() {
        ArrayList<String> names = new ArrayList<String>();
        for (TemplateData serverTemplate : this.templates) {
            names.add(serverTemplate.getName());
        }
        return names;
    }

    public void checkServers() {
        for (final TemplateData serverTemplate : this.templates) {
            if (!serverTemplate.isActive()) {
                continue;
            }
            int lobbyServer = this.getServerWithTemplate(serverTemplate, ServerState.LOBBY);
            final int startupServer = this.getServerWithTemplate(serverTemplate, ServerState.STARTUP);
            lobbyServer += startupServer;
            if (lobbyServer >= serverTemplate.getMinServer()) {
                continue;
            }
            int need = serverTemplate.getMinServer() - lobbyServer;
            this.createServer(serverTemplate);
            if (need < 0) {
                return;
            }
            while (need != 0) {
                --need;
                this.createServer(serverTemplate);
            }
        }
    }

    public void createPrivateServer(CloudPlayer player, final TemplateData serverTemplate) {
        if (player.getPrivateServer() != null && startingPrivateServer.contains(player)) {
            player.sendMessage("privateservers-already-running");
            return;
        }

        startingPrivateServer.add(player);

        final Slave slave = this.getBestSlave(serverTemplate);
        if (slave == null) {
            player.sendMessage("privateservers-no-resources");

            startingPrivateServer.remove(player);

            return;
        }

        if (!serverTemplate.isPrivateTemplate()) {
            player.sendMessage("privateservers-template-can-not-be-used");
            startingPrivateServer.remove(player);
            return;
        }

        if (!player.getRank().isHigherEqualsLevel(Rank.PREMIUM)) {
            if (!(player.getStats().getGlobalStatsValue("server_tokens") >= 1)) {
                player.sendMessage("privateservers-missing-tokens");
                return;
            }

            CloudService.getCloudService().getManagerService().getStatsManager().removePlayerStat(player, "server_tokens", 1);
        }

        final String serverName = this.findServerName(serverTemplate);
        final int port = this.getPort();
        final Server server = new Server(serverName, slave.getHost(), port, serverTemplate.getVersion(), serverTemplate.getRank(), ServerType.CLOUD_SERVER, slave, serverTemplate.getName(), ServerState.STARTUP, serverTemplate.isNickAble(), true, player);
        this.servers.add(server);
        slave.setCurrentStartingServer(server);
        server.setStartupTimeout(System.currentTimeMillis() + 90000L);
        server.setStateTimeout(System.currentTimeMillis() + 15000L);
        this.sendServerToBungees(server, PacketType.PROXY_SERVER_ADD);
        slave.calculateRam();

        AtomicInteger starting = new AtomicInteger(0);
        AtomicInteger init = new AtomicInteger(0);
        AtomicBoolean removed = new AtomicBoolean(false);

        ScheduledExecution scheduledExecution = new ScheduledExecution(() -> {
            String title = player.getLanguageMessage("privateservers-title-starting").replace("&", "§");
            PlayerTitlePacket playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§7⬛ §7⬛ §7⬛");
            if (init.get() == 0) {
                playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§7⬛ §7⬛ §7⬛");
            } else if (init.get() == 1) {
                playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§a⬛ §7⬛ §7⬛");
            } else if (init.get() == 2) {
                playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§a⬛ §a⬛ §7⬛");
            } else if (init.get() == 3) {
                playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§a⬛ §a⬛ §a⬛");
            }
            if (starting.get() >= 37) {
                playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§c✖");
                player.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_TITLE_PACKET, playerTitlePacket));
                player.getServer().remove();

                if (player.getRank().isHigherEqualsLevel(Rank.PREMIUM)) {
                    if (!removed.get()) {
                        removed.set(true);
                        CloudService.getCloudService().getManagerService().getStatsManager().addGlobalStat(player, "server_tokens", 1);
                        player.sendMessage("privateservers-receive-token");
                    }
                }
            }
            if (player.getServer().getName().equalsIgnoreCase(server.getName())) {
                return;
            }
            player.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_TITLE_PACKET, playerTitlePacket));
            starting.set((starting.get()+1));
            init.set(init.get() == 3 ? (init.get()+1) : 0);
        }, 0, 1, TimeUnit.SECONDS);

        Timer shutdown = new Timer();
        shutdown.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (starting.get() >= 37 || player.getServer().getName().equalsIgnoreCase(server.getName())) {
                    scheduledExecution.shutdown();
                    shutdown.cancel();
                }
            }
        }, 1000, 1000);

        slave.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.SLAVE_SERVER_START, new SlaveServerStart(serverName, server.getServerID(), port, serverTemplate.getName(), ServerType.CLOUD_SERVER)));
        CloudService.getCloudService().getTerminal().writeMessage("§3Creating a new Private Server on §e§l"+slave.getName()+" §8(§e§lIP: "+slave.getHost()+":"+port+"§8)");

        /*AtomicBoolean b = new AtomicBoolean();
        b.set(false);
        AtomicInteger copy = new AtomicInteger();
        copy.set(0);
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                String title = player.getLanguageMessage("privateservers-title-copy-template").replace("&", "§");
                PlayerTitlePacket playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§7⬛ §7⬛ §7⬛");
                if (copy.get() == 0 || copy.get() == 4) {
                    playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§7⬛ §7⬛ §7⬛");
                } else if (copy.get() == 1 || copy.get() == 5) {
                    playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§a⬛ §7⬛ §7⬛");
                } else if (copy.get() == 2 || copy.get() == 6) {
                    playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§a⬛ §a⬛ §7⬛");
                    if (copy.get() == 6) {
                        b.set(true);
                    }
                } else if (copy.get() == 3 || copy.get() == 7) {
                    playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§a⬛ §a⬛ §a⬛");

                    if (b.get()) {
                        initPrivateServer(player, slave, serverTemplate);
                        timer.cancel();
                    }
                }
                player.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_TITLE_PACKET, playerTitlePacket));
                copy.set((copy.get()+1));
            }
        }, 1000, 1000);*/

        player.sendMessage("privateservers-starting");
    }

    public void initPrivateServer(CloudPlayer player, Slave slave, TemplateData serverTemplate) {
        final String serverName = this.findServerName(serverTemplate);
        final int port = this.getPort();
        final Server server = new Server(serverName, slave.getHost(), port, serverTemplate.getVersion(), serverTemplate.getRank(), ServerType.CLOUD_SERVER, slave, serverTemplate.getName(), ServerState.STARTUP, serverTemplate.isNickAble(), true, player);
        this.servers.add(server);
        slave.setCurrentStartingServer(server);
        server.setStartupTimeout(System.currentTimeMillis() + 90000L);
        server.setStateTimeout(System.currentTimeMillis() + 15000L);
        this.sendServerToBungees(server, PacketType.PROXY_SERVER_ADD);
        slave.calculateRam();

        AtomicBoolean b = new AtomicBoolean();
        b.set(false);
        AtomicInteger configure = new AtomicInteger();
        configure.set(0);
        Timer config = new Timer();
        config.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                String title = player.getLanguageMessage("privateservers-title-configure-server").replace("&", "§");
                PlayerTitlePacket playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§7⬛ §7⬛ §7⬛");
                if (configure.get() == 0 || configure.get() == 4 || configure.get() == 8) {
                    playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§a⬛ §a⬛ §a⬛");
                } else if (configure.get() == 1 || configure.get() == 5 || configure.get() == 9) {
                    playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§7⬛ §7⬛ §7⬛");
                } else if (configure.get() == 2 || configure.get() == 6 || configure.get() == 10) {
                    playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§a⬛ §7⬛ §7⬛");
                } else if (configure.get() == 3 || configure.get() == 7 || configure.get() == 11) {
                    playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§a⬛ §a⬛ §7⬛");
                    if (configure.get() == 11) {
                        b.set(true);
                    }
                } else if (configure.get() == 12) {
                    if (b.get()) {
                        start(player, slave, server, serverName, port, serverTemplate);
                        config.cancel();
                    }
                }
                player.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_TITLE_PACKET, playerTitlePacket));
                configure.set((configure.get()+1));
            }
        }, 1000, 1000);
    }

    public void start(CloudPlayer player, Slave slave, Server server, String serverName, int port, TemplateData serverTemplate) {
        slave.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.SLAVE_SERVER_START, new SlaveServerStart(serverName, server.getServerID(), port, serverTemplate.getName(), ServerType.CLOUD_SERVER)));
        CloudService.getCloudService().getTerminal().writeMessage("§3Creating a new Private Server on §e§l"+slave.getName()+" §8(§e§lIP: "+slave.getHost()+":"+port+"§8)");

        AtomicInteger starting = new AtomicInteger();
        starting.set(0);
        AtomicBoolean removed = new AtomicBoolean(false);
        Timer start = new Timer();
        start.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                String title = player.getLanguageMessage("privateservers-title-starting").replace("&", "§");
                PlayerTitlePacket playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§7⬛ §7⬛ §7⬛");
                if (starting.get() == 0 || starting.get() == 4 || starting.get() == 8 || starting.get() == 12 || starting.get() == 16 || starting.get() == 20 || starting.get() == 24 || starting.get() == 26 || starting.get() == 30 || starting.get() == 34) {
                    playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§a⬛ §a⬛ §a⬛");
                } else if (starting.get() == 1 || starting.get() == 5 || starting.get() == 9 || starting.get() == 13 || starting.get() == 17 || starting.get() == 21 || starting.get() == 25 || starting.get() == 27 || starting.get() == 31 || starting.get() == 35) {
                    playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§7⬛ §7⬛ §7⬛");
                } else if (starting.get() == 2 || starting.get() == 6 || starting.get() == 10 || starting.get() == 14 || starting.get() == 18 || starting.get() == 22 || starting.get() == 26 || starting.get() == 28 || starting.get() == 32 || starting.get() == 36) {
                    playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§a⬛ §7⬛ §7⬛");
                } else if (starting.get() == 3 || starting.get() == 7 || starting.get() == 11 || starting.get() == 15 || starting.get() == 19 || starting.get() == 23 || starting.get() == 27 || starting.get() == 29 || starting.get() == 33 || starting.get() == 37) {
                    playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§a⬛ §a⬛ §7⬛");
                    if (starting.get() == 37) {
                        playerTitlePacket = new PlayerTitlePacket(player.getUniqueId(), title, "§c✖");
                        player.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_TITLE_PACKET, playerTitlePacket));
                        player.getServer().remove();

                        if (player.getRank().isHigherEqualsLevel(Rank.PREMIUM)) {
                            if (!removed.get()) {
                                removed.set(true);
                                CloudService.getCloudService().getManagerService().getStatsManager().addGlobalStat(player, "server_tokens", 1);
                                player.sendMessage("privateservers-receive-token");
                            }
                        }

                        start.cancel();
                    }
                }
                if (player.getServer().getName().equalsIgnoreCase(server.getName())) {
                    start.cancel();
                    return;
                }
                player.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_TITLE_PACKET, playerTitlePacket));
                starting.set((starting.get()+1));
            }
        }, 1000, 1000);
    }

    public void createServer(final TemplateData serverTemplate) {
        final Slave slave = this.getBestSlave(serverTemplate);
        if (slave == null) return;
        final String serverName = this.findServerName(serverTemplate);
        final int port = this.getPort();
        final Server server = new Server(serverName, slave.getHost(), port, serverTemplate.getVersion(), serverTemplate.getRank(), ServerType.CLOUD_SERVER, slave, serverTemplate.getName(), ServerState.STARTUP, serverTemplate.isNickAble(), false, null);
        this.servers.add(server);
        slave.setCurrentStartingServer(server);
        server.setStartupTimeout(System.currentTimeMillis() + 90000L);
        server.setStateTimeout(System.currentTimeMillis() + 15000L);
        this.sendServerToBungees(server, PacketType.PROXY_SERVER_ADD);
        slave.calculateRam();
        slave.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.SLAVE_SERVER_START, new SlaveServerStart(serverName, server.getServerID(), port, serverTemplate.getName(), ServerType.CLOUD_SERVER)));
        if (serverTemplate.getName().equalsIgnoreCase("lobby")) {
            CloudService.getCloudService().getTerminal().writeMessage("§3Creating a new Lobby Server on §e§l"+slave.getName()+" §8(§e§lIP: "+slave.getHost()+":"+port+"§8)");
            CloudService.getCloudService().getManagerService().getLobbyManager().createLobby(server);
        } else {
            CloudService.getCloudService().getTerminal().writeMessage("§3Creating a new Server on §e§l"+slave.getName()+" §8(§e§lIP: "+slave.getHost()+":"+port+"§8)");
        }
    }

    public void createServer(final ServerData serverData) {
        final Slave slave = CloudService.getCloudService().getManagerService().getSlaveManager().getSlaveByName(serverData.getSlave());
        if (slave == null) return;
        final String serverName = serverData.getName();
        final int port = serverData.getPort();
        final Server server = new Server(serverName, slave.getHost(), port, 340, "MEMBER", ServerType.CLOUD_SERVER, slave, null, ServerState.STARTUP, false, false, null);
        this.servers.add(server);
        slave.setCurrentStartingServer(server);
        server.setStartupTimeout(System.currentTimeMillis() + 90000L);
        server.setStateTimeout(System.currentTimeMillis() + 15000L);
        this.sendServerToBungees(server, PacketType.PROXY_SERVER_ADD);
        slave.calculateRam();
        slave.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.SLAVE_SERVER_START, new SlaveServerStart(serverName, server.getServerID(), port, null, ServerType.STATIC)));
        CloudService.getCloudService().getTerminal().writeMessage("§3Creating a new Server on §e§l"+slave.getName()+" §8(§e§lIP: "+slave.getHost()+":"+port+"§8)");
    }

    public void createServerManually(Channel channel, ServerPacket serverPacket) {
        Server server2;
        for (Server server : this.servers) {
            if (!server.getServerID().equalsIgnoreCase(serverPacket.getName()) && !server.getName().equalsIgnoreCase(serverPacket.getName())) continue;
            server.setChannel(channel);
            CloudService.getCloudService().getTerminal().writeMessage("§cCould not create a new Manual Server §e§l"+serverPacket.getName()+" §8(§e§lIP: "+serverPacket.getHost()+":"+serverPacket.getPort()+"§8)");
            new Thread(() -> {
                channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.ERROR_PACKET, new ErrorPacket("Could not register Server in GoNet: This serverName is already taken!")));
            }
            ).start();
            return;
        }
        if (!CloudService.getCloudService().getManagerService().getWhitelistManager().checkServer(serverPacket.getName(), serverPacket.getHost(), serverPacket.getPort(), serverPacket.getRank(), String.valueOf(serverPacket.getVersion()))) {
            String whereIsMyServer = String.valueOf(serverPacket.getName()) + "-" + System.currentTimeMillis() + "~" + serverPacket.getPort() + "@" + new SimpleDateFormat("E").format(new Date()) + ";" + serverPacket.getHost();

            CloudService.getCloudService().getTerminal().writeMessage("§e§l"+serverPacket.getName()+" §cis trying to connect to the cloud §8(§e§lIP: "+serverPacket.getHost()+":"+serverPacket.getPort()+"§8)");
            if (CloudService.getCloudService().isDevMode()) CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().sendAdminMessage("cloud-gameserver-not-whitelisted", serverPacket.getName(), whereIsMyServer);

            //TelegramManager.sendMessage("Der Server " + serverPacket.getName() + " hat versucht sich zur Cloud zu verbinden, befindet sich aber nicht auf der Whitelist. (ID: " + whereIsMyServer + ")");
            new Thread(() -> {
                channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.ERROR_PACKET, new ErrorPacket("\n\n[]====================[ WARNING ]====================[]\njava.io.IOException: An internal error occurred.\nReason: Code 98\nPlease contact an Administrator. \n\nID: " + whereIsMyServer + "\n[]====================[ WARNING ]====================[]\n\n")));
            }
            ).start();
            return;
        }
        server2 = new Server(serverPacket.getName(), serverPacket.getHost(), serverPacket.getPort(), serverPacket.getVersion(), serverPacket.getRank(), ServerType.STATIC, null, null, serverPacket.getServerState(), false, false, null);
        server2.setServerGroup(serverPacket.getServerGroup());
        server2.setMaxPlayers(serverPacket.getMaxPlayers());
        server2.setChannel(channel);
        this.servers.add(server2);
        this.sendServerToBungees(server2, PacketType.PROXY_SERVER_ADD);
    }

    public void receiveStartUp(int port, Channel channel) {
        new Thread(() -> {
            for (Server server : this.servers) {
                if (server.getPort() != port) continue;
                server.setChannel(channel);
                channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.SERVER_CALLBACK_INFOS, new ServerCallBackInfos(server.getServerID())));
                return;
            }
        }
        ).start();
    }

    public void changeServerState(ServerPacket serverPacket, Channel channel) {
        for (Server server : this.servers) {
            if (!server.getServerID().equalsIgnoreCase(serverPacket.getName()) && !server.getName().equalsIgnoreCase(serverPacket.getName())) continue;
            if (server.getChannel() != channel) {
                server.setChannel(channel);
            }
            server.setServerGroup(serverPacket.getServerGroup());
            server.setMessage(serverPacket.getMsg());
            server.setMaxPlayers(serverPacket.getMaxPlayers());
            if (serverPacket.getServerState() == ServerState.REMOVE) {
                server.remove();
            } else {
                if (serverPacket.getServerState() == ServerState.LOBBY && server.isCloudServer()) {
                    if (server.getSlave().getCurrentStartingServer() != null && server.getSlave().getCurrentStartingServer().equals(server)) {
                        server.getSlave().setCurrentStartingServer(null);
                    }
                    if (server.getServerState() == ServerState.STARTUP) {
                        CloudService.getCloudService().getTerminal().writeMessage("§e§l"+serverPacket.getName()+" §3has been started up §8(§e§lIP: "+serverPacket.getHost()+":"+serverPacket.getPort()+"§8)");

                        if (server.getGameID() != null) {
                            server.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.GAME_ID, new GameIDPacket(server.getGameUID(), server.getGameID())));
                        }

                        new Thread(() -> {
                            try {
                                Thread.sleep(5000);
                                if (server.isPrivateServer() && server.getPrivateOwner() != null) {
                                    server.getPrivateOwner().connectToServer(server.getName());
                                    server.getPrivateOwner().setPrivateServer(server);
                                    server.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PRIVATE_OWNER, new PrivateOwnerPacket(server.getPrivateOwner().getUniqueId())));

                                    startingPrivateServer.remove(server.getPrivateOwner());
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }).start();
                    }
                    //TODO: Event System

                    //if (server.isCloudServer() && (server.getTemplateName().equalsIgnoreCase("lobby") || server.getTemplateName().equalsIgnoreCase("silent"))) {
                    //    server.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.EVENT_PACKET, new EventPacket(CloudService.getCloudService().getManagerService().getLobbyManager().isEventMode(), CloudService.getCloudService().getManagerService().getLobbyManager().getType())));
                    //}
                    if (server.isCloudServer() && !server.getServerTemplate().getSignDatas().isEmpty()) {
                        TemplateData serverTemplate = server.getServerTemplate();
                        ArrayList<String[]> data = new ArrayList<String[]>();
                        for (String location : serverTemplate.getSignDatas().keySet()) {
                            data.add(serverTemplate.getSignDatas().get(location));
                        }
                        server.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.STAT_SIGN_PACKET, new StatSignPacket(data)));
                    }
                }
                server.setServerState(serverPacket.getServerState());
            }
            this.checkServers();
            return;
        }
    }

    public Server getServerByName(String serverId) {
        for (Server server : this.servers) {
            if (!server.getServerID().equalsIgnoreCase(serverId) && !server.getName().equalsIgnoreCase(serverId)) continue;
            return server;
        }
        return null;
    }

    public Server getServerByGameID(String gameID) {
        for (Server server : this.servers) {
            if (server.getGameID().equalsIgnoreCase(gameID)) {
                return server;
            }
        }
        return null;
    }

    public void removeServer(String serverName) {
        for (Server server : this.servers) {
            if (!server.getName().equalsIgnoreCase(serverName)) continue;
            server.remove();
            return;
        }
    }

    public int getPort() {
        int currentSize;
        int port = 2200;
        int size = this.servers.size();
        block0:
        do {
            currentSize = 0;
            for (Server server : this.servers) {
                if (server.getPort() == port) {
                    ++port;
                    continue block0;
                }
                ++currentSize;
            }
        } while (size != currentSize);
        return port;
    }

    public String findServerName(TemplateData serverTemplate) {
        String name;
        int current = 1;
        while (this.getServerByName(name = serverTemplate.getPrefix() + "-" + current) != null || this.nameReservation.containsKey(name.toLowerCase())) {
            ++current;
        }
        return serverTemplate.isPrivateTemplate() ? "P"+name : name;
    }

    public Slave getBestSlave(TemplateData serverTemplate) {
        Slave bestSlave = null;
        for (Slave slave : CloudService.getCloudService().getManagerService().getSlaveManager().getSlaveList()) {
            slave.calculateRam();
            if (slave.getForceTemplate() != null && serverTemplate.getForceSlave() == null || serverTemplate.getForceSlave() != null && (slave.getForceTemplate() == null || !slave.getForceTemplate().equalsIgnoreCase(serverTemplate.getName())) || slave.getCurrentStartingServer() != null || !slave.isActive() || bestSlave != null && this.getServerAmountOnSlave(slave) >= this.getServerAmountOnSlave(bestSlave) || slave.getAverageCPU() >= this.maxCpuAverage) continue;
            bestSlave = slave;
        }
        return bestSlave;
    }

    public int getServerAmountOnSlave(Slave slave) {
        int serverCount = 0;
        for (Server server : this.servers) {
            if (!server.isCloudServer() || !server.getSlave().equals(slave)) continue;
            ++serverCount;
        }
        return serverCount;
    }

    public Server getRandomFallbackServer() {
        List<Server> servers = this.getServers().stream().filter(server -> server.isCloudServer() && server.getTemplateName().equalsIgnoreCase("fallback")).collect(Collectors.toList());
        if (servers.size() == 0) {
            return null;
        }
        int random = Math.abs(new Random().nextInt()) % servers.size();
        if (servers.size() >= random) {
            return servers.get(random);
        }
        return null;
    }

    public Server getRandomRegServer() {
        List<Server> servers = this.getServers().stream().filter(server -> server.isCloudServer() && server.getTemplateName().equalsIgnoreCase("verify")).collect(Collectors.toList());
        if (servers.size() == 0) {
            return null;
        }
        int random = Math.abs(new Random().nextInt()) % servers.size();
        if (servers.size() >= random) {
            return servers.get(random);
        }
        return null;
    }

    public int getServerWithTemplate(TemplateData serverTemplate, ServerState serverState) {
        int serverCount = 0;
        for (Server server : this.servers) {
            if (!server.isCloudServer() || !server.getServerTemplate().getName().equalsIgnoreCase(serverTemplate.getName()) || !server.getServerState().equals(serverState) && !serverState.equals(ServerState.ALL))
                continue;
            ++serverCount;
        }
        return serverCount;
    }

    public List<Server> getServersByGroup(String group) {
        ArrayList<Server> servers = new ArrayList<>();
        for (Server server : this.getServers()) {
            if (server.getServerGroup() == null || !server.getServerGroup().equalsIgnoreCase(group)) continue;
            servers.add(server);
        }
        return servers;
    }

    public Integer getPlayerCountFromTemplate(String template) {
        int i = 0;
        for (Server server : this.getServers()) {
            if (server.getTemplateName() == null || !server.getTemplateName().equalsIgnoreCase(template)) continue;
            i+=server.getOnlinePlayers();
        }
        return i;
    }

    public List<Server> getServersByTemplate(String template) {
        ArrayList<Server> servers = new ArrayList<>();
        for (Server server : this.getServers()) {
            if (server.getTemplateName() == null || !server.getTemplateName().equalsIgnoreCase(template)) continue;
            servers.add(server);
        }
        return servers;
    }

    public void sendServerToBungees(Server server, PacketType packetType) {
        if (packetType.getKey().equals(PacketType.PROXY_SERVER_ADD.getKey())) {
            CloudService.getCloudService().getManagerService().getProxyManager().broadcastPacket(GoAPI.getPacketHelper().preparePacket(packetType, new ProxyAddServerPacket(server.getName(), server.getHost(), server.getPort())));
        } else if (packetType.getKey().equals(PacketType.PROXY_SERVER_REMOVE.getKey())) {
            CloudService.getCloudService().getManagerService().getProxyManager().broadcastPacket(GoAPI.getPacketHelper().preparePacket(packetType, new ProxyRemoveServerPacket(server.getName())));
        }
        CloudService.getCloudService().getManagerService().getProxyManager().broadcastPacket(GoAPI.getPacketHelper().preparePacket(PacketType.LOBBY_DATA_PACKET, CloudService.getCloudService().getManagerService().getLobbyManager().getLobbyDataPacket()));
    }

    public boolean isValidPrivateTemplate(String templateName) {
        if (templateName.startsWith("P")) {
            return true;
        }

        return false;
    }

    public String getGame(TemplateData templateData, String server, CloudPlayer player) {
        /*if (privat) {
            return CloudService.getCloudService().getManagerService().getConfigManager().getLanguages().get(language).getIS_ON_UNKNOWN().replace("&", "§");
        }*/

        String game = "§aLobby";

        if (server == null || server.equalsIgnoreCase("")) {
            return player.getLanguageMessage("is-offline");
        }

        if (templateData != null && templateData.getGame() != null) {
            game = MessageFormat.format(player.getLanguageMessage("is-on-template"), templateData.getGame());
        } else {
            game = MessageFormat.format(player.getLanguageMessage("is-on-server"), server);
        }

        return game;
    }

    public List<Server> getServers() {
        return this.servers;
    }

    public List<TemplateData> getTemplates() {
        return this.templates;
    }

    public boolean isRestarting() {
        return this.restarting;
    }

    public void setRestarting(boolean restarting) {
        this.restarting = restarting;
    }

    public ConcurrentHashMap<String, Long> getNameReservation() {
        return this.nameReservation;
    }

    public double getMaxCpuAverage() {
        return this.maxCpuAverage;
    }

    public void setMaxCpuAverage(double maxCpuAverage) {
        this.maxCpuAverage = maxCpuAverage;
    }
}