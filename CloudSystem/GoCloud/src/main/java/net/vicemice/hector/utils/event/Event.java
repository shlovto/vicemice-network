package net.vicemice.hector.utils.event;

public abstract class Event {
    protected boolean asynchronous = false;

    public boolean isAsynchronous() {
        return this.asynchronous;
    }
}