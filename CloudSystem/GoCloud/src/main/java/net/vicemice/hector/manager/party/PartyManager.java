package net.vicemice.hector.manager.party;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.utils.event.EventKey;
import net.vicemice.hector.manager.party.event.*;

/*
 * Class created at 00:49 - 07.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PartyManager extends EventKey {

    public void init() {
        CloudService.getCloudService().getManagerService().getEventManager().registerListener(this, new PartyAcceptInviteListener());
        CloudService.getCloudService().getManagerService().getEventManager().registerListener(this, new PartyDeleteListener());
        CloudService.getCloudService().getManagerService().getEventManager().registerListener(this, new PartyInviteListener());
        CloudService.getCloudService().getManagerService().getEventManager().registerListener(this, new PartyJoinPartyListener());
        CloudService.getCloudService().getManagerService().getEventManager().registerListener(this, new PartyKickListener());
        CloudService.getCloudService().getManagerService().getEventManager().registerListener(this, new PartyLeaveListener());
        CloudService.getCloudService().getManagerService().getEventManager().registerListener(this, new PartyPromoteListener());
    }
}
