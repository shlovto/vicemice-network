package net.vicemice.hector.utils.terminal;

/*
 * Class created at 20:19 - 31.12.2019
 * Copyright (C) 2016-2019 Minetasia Solutions
 */
public interface Terminal {
    public void install();

    public void uninstall();

    public void write(String var1);

    public void writeMessage(String var1);

    default public void log(String msg) {
        System.out.println(msg);
    }
}