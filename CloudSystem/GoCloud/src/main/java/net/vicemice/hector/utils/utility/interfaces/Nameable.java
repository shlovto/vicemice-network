package net.vicemice.hector.utils.utility.interfaces;

public interface Nameable {
    public String getName();
}