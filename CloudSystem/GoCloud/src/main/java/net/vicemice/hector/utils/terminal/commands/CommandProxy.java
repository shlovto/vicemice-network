package net.vicemice.hector.utils.terminal.commands;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.network.proxy.Proxy;
import net.vicemice.hector.utils.terminal.ArgumentList;
import net.vicemice.hector.utils.terminal.CommandExecutor;
import net.vicemice.hector.utils.terminal.Terminal;

public class CommandProxy implements CommandExecutor {
    @Override
    public void onCommand(String command, Terminal writer, String[] args) {
        if (args.length == 2) {
            if (args[0].equalsIgnoreCase("info")) {
                Proxy proxy = CloudService.getCloudService().getManagerService().getProxyManager().getProxyByName(args[1]);

                if (proxy == null) {
                    writer.write("§cThe Proxy was not found.");
                }

                writer.write("§7Informations about Proxy '" + proxy.getName() + "': ");
                writer.write("  §7UniqueId: §c" + proxy.getUniqueId());
                writer.write("  §7Adress: §c" + proxy.getIpAddress() + ":" + proxy.getPort());
                writer.write("  §7Online Players: §c" + proxy.getOnlinePlayers());
            } else if (args[0].equalsIgnoreCase("remove")) {
                Proxy proxy = CloudService.getCloudService().getManagerService().getProxyManager().getProxyByName(args[1]);

                if (proxy == null) {
                    writer.write("§cThe Proxy was not found.");
                }

                proxy.remove();
            } else {
                writer.write("§aCommands: ");
                writer.write("  §7- §a/proxy info <object>");
                writer.write("  §7- §a/proxy remove <object>");
            }
        } else {
            writer.write("§aCommands: ");
            writer.write("  §7- §a/proxy info <object>");
            writer.write("  §7- §a/proxy remove <object>");
        }
    }

    @Override
    public ArgumentList getArguments() {
        return ArgumentList.builder().arg(new ArgumentList.Argument("/proxy", "Proxy-Managment")).build();
    }
}
