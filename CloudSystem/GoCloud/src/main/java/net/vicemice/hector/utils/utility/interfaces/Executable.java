package net.vicemice.hector.utils.utility.interfaces;

public interface Executable {
    public boolean bootstrap() throws Exception;

    public boolean shutdown();
}