package net.vicemice.hector.commands;

import io.netty.channel.Channel;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.types.player.ComponentMessagePacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.CommandPacket;
import net.vicemice.hector.packets.types.player.MessagePacket;
import net.vicemice.hector.utils.CommandType;

import java.util.ArrayList;
import java.util.HashMap;

/*
 * Class created at 02:28 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class CommandRegistry {
    private HashMap<CommandType, Command> commands = new HashMap();

    public void removeAllCommands() {
        this.commands.clear();
    }

    public void removeCommand(CommandType key) {
        this.commands.remove(key);
    }

    public <T extends Command> T getCommand(CommandType name, Class<T> cls) {
        return (T)this.commands.get(name);
    }

    public Command getCommand(CommandType key) {
        return this.commands.get(key);
    }

    public void registerCommand(CommandType key, Class<? extends Command> cls) {
        if (this.getCommand(key) != null) {
            throw new RuntimeException("Doubler registratioon of " + (key));
        }
        Command instance = null;
        try {
            instance = cls.newInstance();
        }
        catch (Exception e) {
            CloudService.getCloudService().getTerminal().writeMessage("Cant register command " + (key) + ". (Cant create new class instance (" + e.getMessage() + "))");
            return;
        }
        this.registerCommand(key, instance);
    }

    public void registerCommand(CommandType key, Command instance) {
        if (this.getCommand(key) != null) {
            throw new RuntimeException("Doubler registratioon of " + (key));
        }
        this.commands.put(key, instance);
    }

    public CommandRegistry() {

    }

    public void resolveCommand(CommandPacket commandPacket, Channel channel, boolean bungee) {
        CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(commandPacket.getName());
        if (cloudPlayer == null) {
            return;
        }
        if (cloudPlayer.getName().equalsIgnoreCase("Network")) {
            bungee = false;
        }
        if (bungee) {
            if (cloudPlayer.getProxy() == null || cloudPlayer.getProxy().getNettyChannel() != channel) {
                if (channel != null) {
                    channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.MESSAGE_PACKET, new MessagePacket(cloudPlayer.getUniqueId(), new String[]{"§cDeine Sitzung ist ungültig, bitte joine erneut oder kontaktiere einen Admin."})));
                }
                return;
            }
        } else if (!(cloudPlayer.getServer() != null && cloudPlayer.getServer().getChannel() == channel || cloudPlayer.getName().equalsIgnoreCase("Network"))) {
            return;
        }
        if (commandPacket.getCommandType() == null) {
            CloudService.getCloudService().getTerminal().writeMessage("Tried to fire a command without a valid type (" + (commandPacket.getCommandType()) + ")");
            cloudPlayer.sendMessage("command-invalid-type", commandPacket.getCommandType());
            return;
        }
        Command command = this.getCommand(commandPacket.getCommandType());
        if (command == null) {
            CloudService.getCloudService().getTerminal().writeMessage("Tried to invoke a not know command (" + commandPacket.getCommandType() + ")!");
            cloudPlayer.sendMessage("command-no-handler", commandPacket.getCommandType());
            return;
        }
        try {
            if (!cloudPlayer.getSetting("accept-tos", Boolean.class)) {
                cloudPlayer.sendMessage("you-must-accept-tos");
                ArrayList<String[]> messageData = new ArrayList<>();
                messageData.add(new String[]{cloudPlayer.getLanguageMessage("click-to-accept-terms"), "true", "run_command", "/agree"});
                ComponentMessagePacket componentMessagePacket = new ComponentMessagePacket(cloudPlayer.getName(), messageData);
                cloudPlayer.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMPONENT_MESSAGE_PACKET, componentMessagePacket));
                return;
            }
            if (cloudPlayer.isAfk()) {
                cloudPlayer.setAfk(false);
                cloudPlayer.sendMessage("afk-not-longer-idle");
                if (cloudPlayer.getParty() != null) {
                    cloudPlayer.getParty().sendMessageToParty("party-player-unmarked-afk", (cloudPlayer.getRank().getRankColor() + cloudPlayer.getName()));
                }
            }
            command.executeCommand(commandPacket.getCommand(), cloudPlayer);
        }
        catch (Exception e) {
            cloudPlayer.sendMessage("command-error-occurred");
            e.printStackTrace();
        }
    }
}