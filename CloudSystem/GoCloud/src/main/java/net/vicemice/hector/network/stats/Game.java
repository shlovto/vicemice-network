package net.vicemice.hector.network.stats;

public class Game {
    private String modiName;
    private String prefix;
    private String rankingField;
    private String signTypes;

    public Game(String modiName, String prefix, String rankingField, String signTypes) {
        this.modiName = modiName;
        this.prefix = prefix;
        this.rankingField = rankingField;
        this.signTypes = signTypes;
    }

    public String getModiName() {
        return this.modiName;
    }

    public String getPrefix() {
        return this.prefix;
    }

    public String getRankingField() {
        return this.rankingField;
    }

    public String getSignTypes() {
        return this.signTypes;
    }
}

