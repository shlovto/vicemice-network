package net.vicemice.hector.network.stats;

import net.vicemice.hector.player.CloudPlayer;

public class RankingField implements Comparable<RankingField> {
    private long score;
    private CloudPlayer cloudPlayer;
    private Game game;

    public RankingField(long score, CloudPlayer cloudPlayer, Game game) {
        this.score = score;
        this.cloudPlayer = cloudPlayer;
        this.game = game;
    }

    @Override
    public int compareTo(RankingField o) {
        return this.score > o.score ? -1 : (this.score < o.score ? 1 : 0);
    }

    public long getScore() {
        return this.score;
    }

    public CloudPlayer getCloudPlayer() {
        return this.cloudPlayer;
    }

    public Game getGame() {
        return this.game;
    }
}

