package net.vicemice.hector.network.modules;

import java.io.File;

public class ModuleConfig {
    private File file;
    private String name;
    private String version;
    private String author;
    private String main;
    private boolean reloadAble;

    public File getFile() {
        return this.file;
    }

    public String getName() {
        return this.name;
    }

    public String getVersion() {
        return this.version;
    }

    public String getAuthor() {
        return this.author;
    }

    public String getMain() {
        return this.main;
    }

    public boolean isReloadAble() {
        return reloadAble;
    }

    public ModuleConfig(File file, String name, String version, String author, String main, boolean reloadAble) {
        this.file = file;
        this.name = name;
        this.version = version;
        this.author = author;
        this.main = main;
        this.reloadAble = reloadAble;
    }
}

