package net.vicemice.hector.utils;

/**
 * Created by Paul B. on 09.03.2019.
 */
public class VersionUtil {

    public static String fromVersionID(Integer id) {
        String version = "1.0";

        if (id == 575) {
            version = "1.15.1";
        } else if (id == 573) {
            version = "1.15";
        } else if (id == 498) {
            version = "1.14.4";
        } else if (id == 490) {
            version = "1.14.3";
        } else if (id == 485) {
            version = "1.14.2";
        } else if (id == 480) {
            version = "1.14.1";
        } else if (id == 477) {
            version = "1.14";
        } else if (id == 404) {
            version = "1.13.2";
        } else if (id == 401) {
            version = "1.13.1";
        } else if (id == 393) {
            version = "1.13";
        } else if (id == 340) {
            version = "1.12.2";
        } else if (id == 338) {
            version = "1.12.1";
        } else if (id == 335) {
            version = "1.12";
        } else if (id == 316) {
            version = "1.11.1/1.11.2";
        } else if (id == 315) {
            version = "1.11";
        } else if (id == 210) {
            version = "1.10";
        } else if (id == 110) {
            version = "1.9.3/1.9.4";
        } else if (id == 109) {
            version = "1.9.2";
        } else if (id == 108) {
            version = "1.9.1";
        } else if (id == 107) {
            version = "1.9";
        } else if (id == 47) {
            version = "1.8";
        }

        return version;
    }
}