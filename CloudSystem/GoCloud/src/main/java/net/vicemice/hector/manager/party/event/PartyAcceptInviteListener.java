package net.vicemice.hector.manager.party.event;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.modules.api.event.player.party.PartyAcceptInviteEvent;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.labymod.LabyModPacket;
import net.vicemice.hector.packets.types.player.party.PartyPacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.party.Party;
import net.vicemice.hector.utils.event.IEventListener;

/*
 * Class created at 07:12 - 29.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PartyAcceptInviteListener implements IEventListener<PartyAcceptInviteEvent> {

    @Override
    public void onCall(PartyAcceptInviteEvent event) {
        Party party = event.getParty();
        CloudPlayer target = event.getTarget();

        if (target.getParty() != null) {
            target.sendMessage("party-already-in");
            return;
        }

        if (party.getInvites().contains(target) && !party.getMembers().contains(target)) {
            party.getInvites().remove(target);

            if (party.getMembers().size() == party.getMax()) {
                target.sendMessage("party-reach-limit", party.getMembers().size(), party.getMax());
                return;
            }

            party.getMembers().add(target);
            target.setParty(party);
            party.sendMessageToParty("party-player-joined", (target.getRank().getRankColor()+target.getName()));

            for (CloudPlayer player : party.getMembers()) {
                player.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.LABYMOD_PACKET, new LabyModPacket(player.getUniqueId(), true, new PartyPacket(party.getId(), party.getMembers().size(), party.getMax()), player.getJoinSecret(), player.getMatchSecret(), player.getSpecSecret())));
            }
        } else {
            target.sendMessage("party-no-invitation");
        }
    }
}
