package net.vicemice.hector.config.cloud;

import lombok.Data;

/**
 * Created by Paul B. on 27.01.2019.
 */
@Data
public class Config {
    private String host = "smochy.de";
}