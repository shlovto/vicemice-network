package net.vicemice.hector.manager.report;

import io.netty.channel.Channel;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.report.PacketReportRevisorAction;
import net.vicemice.hector.packets.types.player.report.ReportPlayersPacket;
import net.vicemice.hector.player.CloudPlayer;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/*
 * Class created at 23:30 - 03.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ReportManager {
    private CopyOnWriteArrayList<ReportPlayersPacket.ReportEntry> editEntries = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<ReportPlayersPacket.ReportEntry> entries = new CopyOnWriteArrayList<>();
    private ConcurrentHashMap<CloudPlayer, Long> reportTimeout = new ConcurrentHashMap<>();

    public void enableReportManager() {
        new Timer().scheduleAtFixedRate(new TimerTask(){

            @Override
            public void run() {
                ArrayList<ReportPlayersPacket.DataReportEntry> entries = new ArrayList<ReportPlayersPacket.DataReportEntry>();
                /*for (ReportPlayersPacket.ReportEntry e : entries) {
                    HashMap<String, String> prefixMapping = new HashMap<String, String>();
                    CloudPlayer suspect = CloudServer.getPlayerManager().getCloudPlayerByName(e.getSuspect());
                    if (suspect == null || !suspect.isOnline()) {
                        entries.remove(e);
                        continue;
                    }
                    e.setSuspectsCurrentServer(suspect.getServer() == null ? "undefined" : suspect.getServer().getName());
                    prefixMapping.putIfAbsent(suspect.getName(), suspect.getRank().getRankColor());
                    for (ReportPlayersPacket.AccusationsReason accusation : new ArrayList<ReportPlayersPacket.AccusationsReason>(e.getAccusations())) {
                        CloudPlayer playerAccusation = CloudServer.getPlayerManager().getCloudPlayerByName(accusation.getName());
                        if (playerAccusation == null || !playerAccusation.isOnline()) {
                            e.getAccusations().remove(playerAccusation);
                            continue;
                        }
                        prefixMapping.putIfAbsent(playerAccusation.getName(), playerAccusation.getRank().getRankColor());
                    }
                    for (String revisor : new ArrayList<String>(e.getRevisors())) {
                        CloudPlayer playerRevisor = CloudServer.getPlayerManager().getCloudPlayerByName(revisor);
                        if (playerRevisor == null || !playerRevisor.isOnline()) {
                            e.getRevisors().remove(playerRevisor);
                            continue;
                        }
                        prefixMapping.putIfAbsent(revisor, playerRevisor.getRank().getRankColor());
                    }
                    entries.add(new ReportPlayersPacket.DataReportEntry(e, prefixMapping));
                }*/
                PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.REPORT_DATA_PACKET, new ReportPlayersPacket(entries));
                ArrayList<Server> sendedServers = new ArrayList<Server>();
                for (UUID uuid : CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithRank()) {
                    CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uuid);
                    if (getPlayer == null || !getPlayer.isOnline() || !getPlayer.getRank().isTeam() || getPlayer.getServer() == null || sendedServers.contains(getPlayer.getServer())) continue;
                    sendedServers.add(getPlayer.getServer());
                    getPlayer.getServer().sendPacket(initPacket);
                }
            }
        }, 500, 500);
    }

    public void reportPlayer(CloudPlayer suspect, ReportPlayersPacket.AccusationsReason reason) {
        Optional<ReportPlayersPacket.ReportEntry> oentry = CloudService.getCloudService().getManagerService().getReportManager().getReport(suspect);
        ReportPlayersPacket.ReportEntry entry = null;
        if (oentry.isPresent()) {
            entry = oentry.get();
        } else {
            entry = new ReportPlayersPacket.ReportEntry(suspect.getName(), UUID.randomUUID(), suspect.getServer() == null ? "undefined" : suspect.getServer().getName(), new ArrayList<>(), new ArrayList<>());
            entries.add(entry);
        }
        entry.getAccusations().add(reason);

        //NotificationManager.sendNotificationForRank("moderation", Arrays.asList(suspect.getName() + " was reported for " + reason.getReason()));

        //CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().sendModerationMessage(GoAPI.getTeamPrefix() + suspect.getRank().getRankColor() + suspect.getName() + " §7wurde für §e" + reason.getReason() + " §7gemeldet", Language.LanguageType.GERMAN);
        //CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().sendModerationMessage(GoAPI.getTeamPrefix() + suspect.getRank().getRankColor() + suspect.getName() + " §7was reported for §e" + reason.getReason(), Language.LanguageType.ENGLISH);
    }

    @Deprecated
    public Optional<ReportPlayersPacket.ReportEntry> getReport(String suspect) {
        return entries.stream().filter(e -> e.getSuspect().equalsIgnoreCase(suspect)).findAny();
    }

    public Optional<ReportPlayersPacket.ReportEntry> getReport(CloudPlayer suspect) {
        return entries.stream().filter(e -> e.getSuspect().equalsIgnoreCase(suspect.getName())).findAny();
    }

    public List<ReportPlayersPacket.AccusationsReason> getAccusationsFrom(CloudPlayer suspect) {
        Optional<ReportPlayersPacket.ReportEntry> e = CloudService.getCloudService().getManagerService().getReportManager().getReport(suspect);
        if (e.isPresent()) {
            return Collections.unmodifiableList(new ArrayList<>(e.get().getAccusations()));
        }
        return new ArrayList<>();
    }

    public void removeReportFrom(CloudPlayer cloudPlayer) {
        Optional<ReportPlayersPacket.ReportEntry> e = CloudService.getCloudService().getManagerService().getReportManager().getReport(cloudPlayer);
        e.ifPresent(reportEntry -> entries.remove(reportEntry));
    }

    public void removeReport(CloudPlayer cloudPlayer) {
        entries.remove(getReport(cloudPlayer).get());
    }

    public boolean hasReported(CloudPlayer suspect, String accusation) {
        for (ReportPlayersPacket.AccusationsReason r : CloudService.getCloudService().getManagerService().getReportManager().getAccusationsFrom(suspect)) {
            if (!r.getName().equalsIgnoreCase(accusation)) continue;
            return true;
        }
        return false;
    }

    public void clearReports() {
        entries.clear();
    }

    public void handlePacket(PacketHolder holder, Channel channel) {
        if (holder.getType() == PacketType.REPORT_REVISOR_ACTION) {
            PacketReportRevisorAction action = holder.getPacket(PacketReportRevisorAction.class);
            Optional<ReportPlayersPacket.ReportEntry> entry = CloudService.getCloudService().getManagerService().getReportManager().getReport(action.getSuspect());
            if (!entry.isPresent()) {
                return;
            }
            ArrayList<String> revisors = entry.get().getRevisors();
            if (action.getAction() == PacketReportRevisorAction.Action.ADD) {
                if (!revisors.contains(action.getRevisor())) {
                    revisors.add(action.getRevisor());
                }
            } else if (action.getAction() == PacketReportRevisorAction.Action.REMOVE) {
                revisors.remove(action.getRevisor());
            } else if (action.getAction() == PacketReportRevisorAction.Action.DELETE) {
                entries.remove(entry.get());
                Optional<CloudPlayer> player = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayer(action.getRevisor(), true);
                for (String revisor : entry.get().getRevisors()) {
                    if (revisor.equalsIgnoreCase(action.getRevisor())) continue;
                    CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayer(revisor, true).ifPresent(e -> {
                        //if (player.isPresent()) {
                        //    e.sendRawMessage(GoAPI.getTeamPrefix() + (player.get()).getRank().getRankColor() + (player.get()).getRank().getRankName() + " " + (player.get()).getName() + "§7 hat den Report gegen §e" + (entry.get()).getSuspect() + " §7beendet.");
                        //} else {
                        //    e.sendRawMessage(GoAPI.getTeamPrefix() + "§7Der User §e" + action.getRevisor() + "§7 hat den Report gegen §e" + (entry.get()).getSuspect() + " §abeendet.");
                        //}
                    });
                }
            }
        }
    }

    public CopyOnWriteArrayList<ReportPlayersPacket.ReportEntry> getEditEntries() {
        return editEntries;
    }

    public CopyOnWriteArrayList<ReportPlayersPacket.ReportEntry> getEntries() {
        return entries;
    }

    public ConcurrentHashMap<CloudPlayer, Long> getReportTimeout() {
        return reportTimeout;
    }
}