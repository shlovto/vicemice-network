package net.vicemice.hector.utils;

public interface Acceptable<M> {
    public boolean isAccepted(M var1);
}