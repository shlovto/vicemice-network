package net.vicemice.hector.manager.config;

import lombok.Data;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.config.cloud.Config;
import net.vicemice.hector.config.cloud.Configuration;
import net.vicemice.hector.config.mongodb.MongoConfig;
import net.vicemice.hector.config.mongodb.MongoConfiguration;
import net.vicemice.hector.utils.locale.LocaleManager;

import java.util.ArrayList;

/*
 * Class created at 19:18 - 01.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class ConfigManager implements AutoCloseable {

    private Config cloudConfig;
    private Configuration cloudConfiguration;

    private MongoConfig mongoConfig;
    private MongoConfiguration mongoConfiguration;

    private LocaleManager localeManager;

    public static ConfigManager newManager() {
        ConfigManager configManager = new ConfigManager();

        configManager.setCloudConfiguration(new Configuration());
        if (!configManager.getCloudConfiguration().exists()) {
            Config config = new Config();
            configManager.getCloudConfiguration().writeConfiguration(config);
            CloudService.getCloudService().getTerminal().writeMessage("Creating Cloud Configuration");
        }
        configManager.getCloudConfiguration().readConfiguration();
        configManager.setCloudConfig(configManager.getCloudConfiguration().getConfig());

        configManager.setMongoConfiguration(new MongoConfiguration());
        if (!configManager.getMongoConfiguration().exists()) {
            MongoConfig config = new MongoConfig();
            configManager.getMongoConfiguration().writeConfiguration(config);
            CloudService.getCloudService().getTerminal().writeMessage("Creating Mongo Configuration");
        }
        configManager.getMongoConfiguration().readConfiguration();
        configManager.setMongoConfig(configManager.getMongoConfiguration().getMongoConfig());

        ArrayList<String> s = new ArrayList<>();
        s.add("config/locales/core");
        s.add("config/locales/general");
        s.add("config/locales/replay");
        s.add("config/locales/report");

        configManager.setLocaleManager(LocaleManager.newManager(s));

        return configManager;
    }

    @Override
    public void close() throws Exception {

    }
}