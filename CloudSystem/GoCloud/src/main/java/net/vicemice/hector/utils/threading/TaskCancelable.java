/*
 * Decompiled with CFR 0.145.
 */
package net.vicemice.hector.utils.threading;

public interface TaskCancelable {
    public void cancelTask(Long var1);

    public void cancelAllTasks();
}

