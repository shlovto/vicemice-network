package net.vicemice.hector.manager.party.event;

import net.vicemice.hector.network.modules.api.event.player.party.PartyKickEvent;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.party.Party;
import net.vicemice.hector.utils.event.IEventListener;

/*
 * Class created at 07:13 - 29.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PartyKickListener implements IEventListener<PartyKickEvent> {

    @Override
    public void onCall(PartyKickEvent event) {
        CloudPlayer leader = event.getLeader(), target = event.getTarget();
        Party party = event.getLeader().getParty();

        if (party.getMembers().contains(target)) {
            party.getMembers().remove(target);
            target.setParty(null);

            target.sendMessage("party-player-kicked-target");
            party.sendMessageToParty("party-player-kicked", (target.getRank().getRankColor()+target.getName()));
        } else {
            leader.sendMessage("party-player-not-in");
        }
    }
}
