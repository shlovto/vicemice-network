package net.vicemice.hector.utils.terminal.commands;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.terminal.ArgumentList;
import net.vicemice.hector.utils.terminal.CommandExecutor;
import net.vicemice.hector.utils.terminal.Terminal;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.proxy.MaintenancePacket;

public class CommandMaintenance implements CommandExecutor {

    @Override
    public void onCommand(String command, Terminal writer, String[] args) {
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("on")) {
                writer.writeMessage("§aMaintenance was enabled!");
                PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.MAINTENANCE_PACKET, new MaintenancePacket(true, false));
                CloudService.getCloudService().getManagerService().getProxyManager().broadcastPacket(initPacket);
                CloudService.getCloudService().getManagerService().getProxyManager().setMaintenanceActive(true);
            } else if (args[0].equalsIgnoreCase("off")) {
                writer.writeMessage("§cMaintenance was disabled!");
                PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.MAINTENANCE_PACKET, new MaintenancePacket(false, false));
                CloudService.getCloudService().getManagerService().getProxyManager().broadcastPacket(initPacket);
                CloudService.getCloudService().getManagerService().getProxyManager().setMaintenanceActive(false);
            } else {
                writer.writeMessage("§c§lUnknown Command");
            }
        } else {
            writer.writeMessage("§cYou must use §lmaintenance <on|off>");
        }
    }

    @Override
    public ArgumentList getArguments() {
        return ArgumentList.builder().arg(new ArgumentList.Argument("/maintenance", "Dis/Enable the Maintenance")).build();
    }
}
