package net.vicemice.hector.nettyserver.rank;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.ReferenceCountUtil;
import net.vicemice.hector.CloudService;

public class RankHandler extends SimpleChannelInboundHandler<Object> {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        if (!CloudService.getCloudService().getNettyService().getRankServer().getRankClients().contains(ctx.channel())) {
            CloudService.getCloudService().getNettyService().getRankServer().getRankClients().add(ctx.channel());
            CloudService.getCloudService().getTerminal().writeMessage("New RankClient ist now registered: " + ctx.channel().remoteAddress());
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        CloudService.getCloudService().getNettyService().getRankServer().getRankClients().remove(ctx.channel());
        CloudService.getCloudService().getTerminal().writeMessage(ctx.channel() + " is now unregistered!");
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        CloudService.getCloudService().getNettyService().getRankServer().getRankClients().remove(ctx.channel());
        CloudService.getCloudService().getTerminal().writeMessage(ctx.channel() + " is now unregistered!");
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object o) throws Exception {
        new Thread(() -> {
            if (channelHandlerContext.channel() != null && channelHandlerContext.channel().isOpen() && channelHandlerContext.channel().isActive() && !CloudService.getCloudService().getNettyService().getRankServer().getRankClients().contains(channelHandlerContext.channel())) {
                CloudService.getCloudService().getNettyService().getRankServer().getRankClients().add(channelHandlerContext.channel());
                CloudService.getCloudService().getTerminal().writeMessage("New RankClient ist now registered: " + channelHandlerContext.channel().remoteAddress());
            }
            CloudService.getCloudService().getNettyService().getRankServer().newRankClient(channelHandlerContext.channel());
            ReferenceCountUtil.release(o);
        }
        ).start();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
    }
}