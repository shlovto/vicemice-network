package net.vicemice.hector.utils.utility.interfaces;

public interface Saveable {
    public boolean save() throws Exception;
}