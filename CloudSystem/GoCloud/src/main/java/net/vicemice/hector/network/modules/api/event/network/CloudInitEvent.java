package net.vicemice.hector.network.modules.api.event.network;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.utils.event.async.AsyncEvent;
import net.vicemice.hector.utils.event.async.AsyncPosterAdapter;

public class CloudInitEvent extends AsyncEvent<CloudInitEvent> {
    private CloudService cloudService = CloudService.getCloudService();

    public CloudInitEvent() {
        super(new AsyncPosterAdapter<>());
    }

    public CloudService getCloudService() {
        return cloudService;
    }
}

