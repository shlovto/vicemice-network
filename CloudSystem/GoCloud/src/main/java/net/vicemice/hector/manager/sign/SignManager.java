package net.vicemice.hector.manager.sign;

import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.network.gameserver.lobby.lobbysign.Sign;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.slave.TemplateData;
import org.bson.Document;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class SignManager {

    @Getter
    private List<Sign> signs = new CopyOnWriteArrayList<Sign>();
    @Getter
    private ConcurrentHashMap<TemplateData, List<Sign>> templateSigns = new ConcurrentHashMap<>();

    public SignManager() {
        new Timer().scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run() {
                for (Sign listSign : SignManager.this.signs) {
                    if (listSign.getServer() == null || CloudService.getCloudService().getManagerService().getServerManager().getServers().contains(listSign.getServer()) && listSign.getServer().getServerState() == ServerState.LOBBY) continue;
                    listSign.getServer().setSign(null);
                    listSign.setServer(null);
                }
                for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
                    SignManager.this.checkServerSign(server);
                }
                for (Sign listSign2 : SignManager.this.signs) {
                    listSign2.updateLines();
                    listSign2.sendLines();
                }
            }
        }, 500, 500);
    }

    public void checkServerSign(Server server) {
        if (server.getSign() != null) {
            if (server.getServerState() != ServerState.LOBBY && server.getSign().getManualServerName() == null) {
                Sign sign = server.getSign();
                sign.setServer(null);
                server.setSign(null);
                return;
            }
            server.getSign().updateLines();
        }
        if (server.getServerTemplate() != null && server.getServerTemplate().getLobbySigns() != 0 && server.getSign() == null && server.getServerState() == ServerState.LOBBY && server.isCloudServer()) {
            for (Sign listSign : this.signs) {
                if (listSign.getManualServerName() != null || server.getServerTemplate() == null || !listSign.getTemplateName().equalsIgnoreCase(server.getServerTemplate().getName()) || listSign.getServer() != null) {
                    System.out.println("The Server " + server.getName() + " cannot be get a sign");
                    continue;
                }
                listSign.setServer(server);
                server.setSign(listSign);
                return;
            }
        } else {
            if (server.getSign() != null) return;
            for (Sign sign : this.signs) {
                if (sign.getManualServerName() == null || !sign.getManualServerName().equalsIgnoreCase(server.getName())) continue;
                server.setSign(sign);
                sign.setServer(server);
            }
        }
    }

    public void reloadSigns() {
        this.signs.clear();
        this.getTemplateSigns().clear();
        try {
            ArrayList<Document> list = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("signs");

            for (Document document : list) {
                int x = document.getInteger("x");
                int y = document.getInteger("y");
                int z = document.getInteger("z");
                String template = document.getString("template");
                String world = document.getString("world");
                String manualServer = document.getString("manualServerName");
                Sign sign = new Sign(x, y, z, world, template, manualServer);
                sign.updateLines();
                this.signs.add(sign);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        for (TemplateData serverTemplate : CloudService.getCloudService().getManagerService().getServerManager().getTemplates()) {
            ArrayList<Sign> signs = new ArrayList<Sign>();
            for (Sign sign : this.getSigns()) {
                if (sign.getTemplateName() == null || !sign.getTemplateName().equalsIgnoreCase(serverTemplate.getName())) continue;
                signs.add(sign);
            }
            if (signs.size() <= 0) continue;
            this.getTemplateSigns().put(serverTemplate, signs);
        }
        for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
            server.setSign(null);
        }
    }
}

