package net.vicemice.hector.utils.utility.document;

import com.google.gson.JsonElement;
import net.vicemice.hector.utils.utility.interfaces.Nameable;

import java.io.File;
import java.util.Set;

public interface DocumentAbstract extends Nameable {
    public <T extends DocumentAbstract> T append(String var1, String var2);

    public <T extends DocumentAbstract> T append(String var1, Number var2);

    public <T extends DocumentAbstract> T append(String var1, Boolean var2);

    public <T extends DocumentAbstract> T append(String var1, JsonElement var2);

    @Deprecated
    public <T extends DocumentAbstract> T append(String var1, Object var2);

    public <T extends DocumentAbstract> T remove(String var1);

    public Set<String> keys();

    public String getString(String var1);

    public int getInt(String var1);

    public long getLong(String var1);

    public double getDouble(String var1);

    public boolean getBoolean(String var1);

    public float getFloat(String var1);

    public short getShort(String var1);

    public String convertToJson();

    public boolean saveAsConfig(File var1);

    public boolean saveAsConfig(String var1);

    public <T extends DocumentAbstract> T getDocument(String var1);
}

