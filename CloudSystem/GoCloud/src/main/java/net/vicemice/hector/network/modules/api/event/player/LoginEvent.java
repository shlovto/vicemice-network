package net.vicemice.hector.network.modules.api.event.player;

import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.event.async.AsyncEvent;
import net.vicemice.hector.utils.event.async.AsyncPosterAdapter;

public class LoginEvent extends AsyncEvent<LoginEvent> {
    private CloudPlayer cloudPlayer;

    public LoginEvent(CloudPlayer cloudPlayer) {
        super(new AsyncPosterAdapter<>());
        this.cloudPlayer = cloudPlayer;
    }

    public CloudPlayer getCloudPlayer() {
        return this.cloudPlayer;
    }
}

