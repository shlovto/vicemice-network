package net.vicemice.hector.player.clan;

import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.SaveEntry;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.ComponentMessagePacket;
import net.vicemice.hector.utils.players.clan.ClanRank;
import org.apache.commons.lang3.RandomStringUtils;
import org.bson.Document;
import org.bson.conversions.Bson;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

/*
 * Class created at 17:54 - 01.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class Clan implements SaveEntry {

    @Getter
    private final String id;
    @Getter
    private final UUID uniqueId;
    @Getter
    private final String name;
    @Getter
    private final String tag;

    @Getter
    private final HashMap<UUID, ClanRank> members;
    @Getter
    private List<UUID> invites = new CopyOnWriteArrayList<UUID>();

    /**
     @param name defined the clan name
     @param tag defined the clan tag
     @param cloudPlayer defined the clan cloudPlayer
     */
    public Clan(String name, String tag, CloudPlayer cloudPlayer) {
        this.id = RandomStringUtils.randomAlphanumeric(20);

        this.name = name;
        this.tag = tag;

        this.uniqueId = UUID.randomUUID();

        this.members = new HashMap<>();
        this.members.put(cloudPlayer.getUniqueId(), ClanRank.LEADER);

        cloudPlayer.sendMessage("clan-created", name, tag);

        cloudPlayer.setClan(this);
    }

    /**
     @param id defined the clanid
     @param uniqueId defined the clan uniqueId
     @param name defined the clan name
     @param tag defined the clan tag
     @param members defined the clan members with the ranks
     @param invites defined the clan invites
     */
    public Clan(String id, UUID uniqueId, String name, String tag, HashMap<UUID, ClanRank> members, List<UUID> invites) {
        this.id = id;
        this.uniqueId = uniqueId;
        this.name = name;
        this.tag = tag;
        this.members = members;
        this.invites = invites;
    }

    /**
     @param cloudPlayer defined the player which accept the invite
     */
    public void acceptInvite(CloudPlayer cloudPlayer) {
        if (this.invites.contains(cloudPlayer.getUniqueId()) && !this.members.containsKey(cloudPlayer.getUniqueId())) {
            this.invites.remove(cloudPlayer.getUniqueId());

            this.members.put(cloudPlayer.getUniqueId(), ClanRank.MEMBER);
            cloudPlayer.setClan(this);
            this.sendMessageToClan("clan-player-joined", (cloudPlayer.getRank().getRankColor() + cloudPlayer.getName()));
            this.save(false);
        } else {
            cloudPlayer.sendMessage("clan-no-invitation");
        }
    }

    /**
     @param cloudPlayer defined the invitor
     @param invited defined the player which was invited
     */
    public void inviteMember(CloudPlayer cloudPlayer, CloudPlayer invited) {
        if (invited == cloudPlayer) {
            cloudPlayer.sendMessage("player-self-interact", cloudPlayer.getLanguageMessage("party-prefix"));
            return;
        }
        if (!invited.isOnline()) {
            cloudPlayer.sendMessage("user-not-online", cloudPlayer.getLanguageMessage("party-prefix"), (invited.getRank().getRankColor() + invited.getName()));
            return;
        }


        if (invited.getSetting("advanced-player-settings", Integer.class) == 1) {
            boolean reject = false;
            if (invited.getParty() != null && invited.getParty().getMembers().contains(cloudPlayer) && invited.getSetting("clan-invites-clan", Integer.class) == 0) {
                reject = true;
            } else if (invited.isFriend(cloudPlayer) && invited.getFriends().get(cloudPlayer.getUniqueId()).isFavorite() && invited.getSetting("clan-invites-friend-favorites", Integer.class) == 0) {
                reject = true;
            } else if (invited.isFriend(cloudPlayer) && invited.getSetting("clan-invites-friends", Integer.class) == 0) {
                reject = true;
            } else if (invited.getRank().isTeam() && invited.getSetting("clan-invites-staffs", Integer.class) == 0) {
                reject = true;
            } else if (invited.getRank().equalsLevel(Rank.VIP) && invited.getSetting("clan-invites-vips", Integer.class) == 0) {
                reject = true;
            } else if (invited.getRank().equalsLevel(Rank.PREMIUM) && invited.getSetting("clan-invites-premiums", Integer.class) == 0) {
                reject = true;
            } else if (invited.getRank().equalsLevel(Rank.MEMBER) && invited.getSetting("clan-invites-members", Integer.class) == 0) {
                reject = true;
            }

            if (cloudPlayer.getClan() != null && cloudPlayer.getClan().getMembers().containsKey(invited.getUniqueId()) && invited.getSetting("clan-invites-clan", Integer.class) == 1) {
                reject = false;
            } if (invited.isFriend(cloudPlayer) && invited.getFriends().get(cloudPlayer.getUniqueId()).isFavorite() && invited.getSetting("clan-invites-friend-favorites", Integer.class) == 1) {
                reject = false;
            } if (invited.isFriend(cloudPlayer) && invited.getSetting("clan-invites-friends", Integer.class) == 1) {
                reject = false;
            } if (invited.getRank().isTeam() && invited.getSetting("clan-invites-staffs", Integer.class) == 1) {
                reject = false;
            } if (invited.getRank().equalsLevel(Rank.VIP) && invited.getSetting("clan-invites-vips", Integer.class) == 1) {
                reject = false;
            } if (invited.getRank().equalsLevel(Rank.PREMIUM) && invited.getSetting("clan-invites-premiums", Integer.class) == 1) {
                reject = false;
            } if (invited.getRank().equalsLevel(Rank.MEMBER) && invited.getSetting("clan-invites-members", Integer.class) == 1) {
                reject = false;
            }

            if (reject) {
                cloudPlayer.sendMessage("clan-invite-disabled");
                return;
            }
        } else {
            if (invited.getSettingsPacket().getSetting("clan-invites", Integer.class) == 1) {
                cloudPlayer.sendMessage("clan-invite-disabled");
                return;
            }
        }

        if (!this.invites.contains(invited.getUniqueId())) {
            this.invites.add(invited.getUniqueId());
            this.save(false);
            cloudPlayer.sendMessage("clan-invited");

            invited.sendMessage("clan-invite", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()));
            ArrayList<String[]> messageData = new ArrayList<String[]>();
            messageData.add(new String[]{"§a["+invited.getLanguageMessage("accept")+"]", "true", "run_command", "/clan accept " + cloudPlayer.getName()});
            ComponentMessagePacket componentMessagePacket = new ComponentMessagePacket(invited.getName().toLowerCase(), messageData);
            invited.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMPONENT_MESSAGE_PACKET, componentMessagePacket));

            if (invited.isAfk()) {
                cloudPlayer.sendMessage("user-is-afk", cloudPlayer.getLanguageMessage("clan-prefix"), cloudPlayer.getRank().getRankColor() + cloudPlayer.getName());
            }
        } else {
            cloudPlayer.sendMessage("clan-invite-already");
        }
    }

    /**
     @param cloudPlayer defined the cloudPlayer
     @param promoted defined the player which should be promoted
     */
    public void promotePlayer(CloudPlayer cloudPlayer, CloudPlayer promoted) {
        if (promoted != cloudPlayer) {
            if (this.members.containsKey(promoted.getUniqueId())) {
                if (this.members.get(promoted.getUniqueId()) == ClanRank.MEMBER) {
                    this.addModerator(promoted);
                } else if (this.members.get(promoted.getUniqueId()) == ClanRank.MODERATOR) {
                    this.addLeader(promoted);
                }
                this.save(false);
            } else {
                cloudPlayer.sendMessage("clan-player-not-in", (promoted.getRank().getRankColor()+promoted.getName()));
            }
        } else {
            cloudPlayer.sendMessage("player-self-interact", cloudPlayer.getLanguageMessage("clan-prefix"));
        }
    }

    /**
     @param cloudPlayer defined the cloudPlayer
     @param demoted defined the player which should be demoted
     */
    public void demotePlayer(CloudPlayer cloudPlayer, CloudPlayer demoted) {
        if (demoted != cloudPlayer) {
            if (this.members.containsKey(demoted.getUniqueId())) {
                if (this.members.get(demoted.getUniqueId()) == ClanRank.MODERATOR) {
                    this.addMember(demoted);
                } else if (this.members.get(demoted.getUniqueId()) == ClanRank.LEADER) {
                    this.addModerator(demoted);
                }
                this.save(false);
            } else {
                cloudPlayer.sendMessage("clan-player-not-in", (demoted.getRank().getRankColor()+demoted.getName()));
            }
        } else {
            cloudPlayer.sendMessage("player-self-interact", cloudPlayer.getLanguageMessage("clan-prefix"));
        }
    }

    /**
     @param cloudPlayer defined the player which are leaved
     */
    public void memberLeave(CloudPlayer cloudPlayer) {
        if (this.members.get(cloudPlayer.getUniqueId()) == ClanRank.LEADER) {
            if (this.members.size() == 1 || this.members.size() == 0) {
                this.deleteClan();
            } else {
                cloudPlayer.sendMessage("clan-player-left-invited");
                this.members.remove(cloudPlayer.getUniqueId());
                this.sendMessageToClan("clan-player-left", (cloudPlayer.getRank().getRankColor() + cloudPlayer.getName()));
            }
        } else {
            cloudPlayer.sendMessage("clan-player-left-invited");
            this.members.remove(cloudPlayer.getUniqueId());
            this.sendMessageToClan("clan-player-left", (cloudPlayer.getRank().getRankColor() + cloudPlayer.getName()));
            this.save(false);
        }

        cloudPlayer.setClan(null);
    }

    /**
     Delete the Clan
     */
    public void deleteClan() {
        this.sendMessageToClan("clan-deleted");
        for (UUID uuid : this.members.keySet()) {
            CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uuid).setClan(null);
        }
        this.members.clear();
        this.invites.clear();

        CloudService.getCloudService().getManagerService().getClanManager().getClanNames().remove(this.name);
        CloudService.getCloudService().getManagerService().getClanManager().getClanTags().remove(this.tag);
        CloudService.getCloudService().getManagerService().getClanManager().getClans().remove(this);

        this.delete();
    }

    /**
     @param leader defined the leader
     @param cloudPlayer defined the player which should be kicked
     */
    public void kickMember(CloudPlayer leader, CloudPlayer cloudPlayer) {
        if (this.members.containsKey(cloudPlayer.getUniqueId())) {
            this.members.remove(cloudPlayer.getUniqueId());
            cloudPlayer.setClan(null);
            cloudPlayer.sendMessage("clan-player-kicked-invited");
            this.sendMessageToClan("clan-player-kicked", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()));
            this.save(false);
        } else {
            leader.sendMessage("clan-player-not-in", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()));
        }
    }

    /**
     * Send a Language-Key Message to the Clan Members
     * @param message defines the message
     */
    public void sendMessageToClan(String message) {
        for (UUID uniqueId : this.members.keySet()) {
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uniqueId);
            cloudPlayer.sendMessage(message);
        }
    }

    /**
     * Send a Language-Key Message to the Clan Members
     * @param string defines the key
     * @param objects defines the objectives which replacing the {id}
     */
    public void sendMessageToClan(String string, Object... objects) {
        for (UUID uniqueId : this.members.keySet()) {
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uniqueId);
            cloudPlayer.sendMessage(string, objects);
        }
    }

    /**
     @param cloudPlayer defined the player which should be added to the clan
     */
    public void addMember(CloudPlayer cloudPlayer) {
        this.members.remove(cloudPlayer.getUniqueId());
        this.members.put(cloudPlayer.getUniqueId(), ClanRank.MEMBER);
        this.sendMessageToClan("clan-member-is-now-member", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()));
    }

    /**
     @param cloudPlayer defined the player which should be add as moderator
     */
    public void addModerator(CloudPlayer cloudPlayer) {
        this.members.remove(cloudPlayer.getUniqueId());
        this.members.put(cloudPlayer.getUniqueId(), ClanRank.MODERATOR);
        this.sendMessageToClan("clan-member-is-now-moderator", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()));
    }

    /**
     @param cloudPlayer defined the player which should be add as cloudPlayer
     */
    public void addLeader(CloudPlayer cloudPlayer) {
        this.members.remove(cloudPlayer.getUniqueId());
        this.members.put(cloudPlayer.getUniqueId(), ClanRank.LEADER);
        this.sendMessageToClan("clan-member-is-now-cloudPlayer", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()));
    }

    /**
     @param rank defined the rank to get the members
     */
    private String membersToString(ClanRank rank) {
        String array = "";

        for (UUID uuid : this.getMembers().keySet()) {
            if (this.getMembers().get(uuid) == rank) {
                array += uuid + ";";
            }
        }

        return array;
    }

    /**
     Get All Invites as String
     */
    private String invitesToString() {
        String array = "";

        for (UUID uuid : this.getInvites()) {
            array += uuid + ";";
        }

        return array;
    }

    /**
     Delete the Clan from the Database
     */
    public void delete() {
        try {
            CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("networkService").deleteOne(new Document("uuid", String.valueOf(this.getUniqueId())));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     @param insert if true, the clan will be inserted an not updated
     */
    @Override
    public void save(boolean insert) {
        try {
            if (insert) {
                Document document = new Document("id", this.getId());
                document.append("uuid", String.valueOf(this.getUniqueId()));
                document.append("name", this.getName());
                document.append("tag", this.getTag());
                document.append("invites", invitesToString());
                document.append("cloudPlayers", membersToString(ClanRank.LEADER));
                document.append("moderators", membersToString(ClanRank.MODERATOR));
                document.append("members", membersToString(ClanRank.MEMBER));

                CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("clans").insertOne(document);

                return;
            }

            Document document = new Document("name", String.valueOf(this.getName()));
            document.append("name", this.getName());
            document.append("tag", this.getTag());
            document.append("invites", invitesToString());
            document.append("cloudPlayers", membersToString(ClanRank.LEADER));
            document.append("moderators", membersToString(ClanRank.MODERATOR));
            document.append("members", membersToString(ClanRank.MEMBER));

            Bson filter = new Document("uuid", String.valueOf(this.getUniqueId()));
            CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("clans").updateOne(filter, new Document("$set", document));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}