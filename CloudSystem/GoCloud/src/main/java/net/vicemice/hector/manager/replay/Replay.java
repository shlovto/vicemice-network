package net.vicemice.hector.manager.replay;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.packets.types.player.replay.ReplayDataPacket;
import net.vicemice.hector.utils.SaveEntry;
import org.bson.Document;
import org.bson.conversions.Bson;

/*
 * Class created at 02:18 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class Replay implements SaveEntry {

    private ReplayDataPacket dataPacket;

    public Replay(ReplayDataPacket replayDataPacket) {
        this.dataPacket = replayDataPacket;
    }

    @Override
    public void save(boolean insert) {
        try {
            if (insert) {
                Document document = new Document("uid", String.valueOf(this.dataPacket.getUid()));
                document.append("gameid", this.dataPacket.getGameID());
                document.append("game", this.dataPacket.getGame());
                document.append("map", this.dataPacket.getMap());
                document.append("available", this.dataPacket.isAvailable());

                CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("replays").insertOne(document);

                if (!CloudService.getCloudService().getManagerService().getReplayManager().getUidReplays().containsKey(dataPacket.getUid())) {
                    CloudService.getCloudService().getManagerService().getReplayManager().getUidReplays().put(this.dataPacket.getUid(), dataPacket);
                }

                if (!CloudService.getCloudService().getManagerService().getReplayManager().getGameidReplays().containsKey(dataPacket.getGameID())) {
                    CloudService.getCloudService().getManagerService().getReplayManager().getGameidReplays().put(dataPacket.getGameID(), dataPacket);
                }

                return;
            }

            Document document = new Document("gameid", this.dataPacket.getGameID());
            document.append("game", this.dataPacket.getGame());
            document.append("map", this.dataPacket.getMap());
            document.append("available", this.dataPacket.isAvailable());

            Bson filter = new Document("uid", String.valueOf(this.dataPacket.getUid()));
            CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("replays").updateOne(filter, new Document("$set", document));

            if (!CloudService.getCloudService().getManagerService().getReplayManager().getUidReplays().containsKey(this.dataPacket.getUid())) {
                CloudService.getCloudService().getManagerService().getReplayManager().getUidReplays().get(this.dataPacket.getUid()).setAvailable(this.dataPacket.isAvailable());
            }

            if (!CloudService.getCloudService().getManagerService().getReplayManager().getGameidReplays().containsKey(dataPacket.getGameID())) {
                CloudService.getCloudService().getManagerService().getReplayManager().getGameidReplays().get(this.dataPacket.getGameID()).setAvailable(this.dataPacket.isAvailable());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}