package net.vicemice.hector.manager.stats.sign;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.network.gameserver.lobby.Lobby;
import net.vicemice.hector.network.stats.Game;
import net.vicemice.hector.network.stats.sign.StatSign;
import net.vicemice.hector.packets.types.server.StatSignPacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.utils.slave.TemplateData;
import org.bson.Document;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/*
 * Class created at 23:50 - 02.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SignStatsManager {
    private List<StatSign> statSigns = new CopyOnWriteArrayList<StatSign>();
    private StatSignPacket dailySignPacket;
    private StatSignPacket monthlySignPacket;
    private StatSignPacket globalSignPacket;

    public void loadSigns() {
        statSigns.clear();
        try {
            ArrayList<Document> list = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("statsSigns");

            for (Document document : list) {
                String modi = document.getString("modi");
                int rank = document.getInteger("rank");
                String location = document.getString("location");
                String mode = document.getString("mode");
                String template = document.getString("template");
                List<TemplateData> serverTemplates = CloudService.getCloudService().getManagerService().getServerManager().getTemplatesByStatsPrefix(template);
                for (TemplateData serverTemplate : serverTemplates) {
                    serverTemplate.getSignDatas().clear();
                }
                statSigns.add(new StatSign(modi, location, rank, mode, serverTemplates));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private List<StatSign> getSignByArguments(String gamemode, String mode, int rank, String gamePrefix) {
        ArrayList<StatSign> signList = new ArrayList<StatSign>();
        for (StatSign statSign : statSigns) {
            TemplateData serverTemplate = CloudService.getCloudService().getManagerService().getServerManager().getTemplatesByStatsPrefix(gamePrefix).get(0);
            if (statSign.getTemplates().size() != 0 && !statSign.getTemplates().contains(serverTemplate) || !statSign.getGameMode().equalsIgnoreCase(gamemode) || statSign.getRank() != rank || !statSign.getMode().equalsIgnoreCase(mode)) continue;
            signList.add(statSign);
        }
        return signList;
    }

    public void updateGlobalSignPacket() {
        ArrayList<String[]> data = new ArrayList<String[]>();
        ArrayList<StatSign> usedSigns = new ArrayList<StatSign>();
        for (String gameMode : CloudService.getCloudService().getManagerService().getStatsManager().getGlobalTop().keySet()) {
            Game game = CloudService.getCloudService().getManagerService().getGameManager().getGameByName(gameMode);
            String[] types = game.getSignTypes().split("@");
            if (types.length == 0) continue;
            List<String> signLines = Arrays.asList(types);
            List<CloudPlayer> topPlayers = CloudService.getCloudService().getManagerService().getStatsManager().getGlobalTop().get(gameMode);
            int rank = 1;
            for (CloudPlayer topPlayer : topPlayers) {
                List<StatSign> signs = CloudService.getCloudService().getManagerService().getSignStatsManager().getSignByArguments(gameMode, "global", rank, game.getPrefix());
                if (signs.size() == 0) continue;
                for (StatSign statSign : signs) {
                    usedSigns.add(statSign);
                    String[] lineData = new String[6];
                    lineData[0] = statSign.getLocation();
                    lineData[1] = String.valueOf(topPlayer.getSkinValue()) + ";" + topPlayer.getSkinSignature();
                    lineData[2] = topPlayer.getName();
                    lineData[3] = "Rank #" + rank;
                    int current = 4;
                    Iterator iterator = signLines.iterator();
                    while (iterator.hasNext()) {
                        String signLine = (String)iterator.next();
                        if (!signLine.contains(";")) {
                            lineData[current] = signLine;
                        } else {
                            String[] line = signLine.split(";");
                            String field = line[0];
                            String anzeige = line[1];
                            long value = 0;
                            if (topPlayer.getStats().getGlobalStats().containsKey(String.valueOf(game.getPrefix()) + "_" + field)) {
                                value = topPlayer.getStats().getGlobalStats().get(String.valueOf(game.getPrefix()) + "_" + field);
                            }
                            lineData[current] = String.valueOf(anzeige) + ": " + value;
                        }
                        ++current;
                    }
                    for (TemplateData serverTemplate : statSign.getTemplates()) {
                        serverTemplate.getSignDatas().put(statSign.getLocation(), lineData);
                    }
                    data.add(lineData);
                }
                ++rank;
            }
        }
        for (StatSign statSign : statSigns) {
            if (!statSign.getMode().equalsIgnoreCase("global") || usedSigns.contains(statSign)) continue;
            String[] lineData = new String[6];
            lineData[0] = statSign.getLocation();
            lineData[1] = String.valueOf(CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinValue()) + ";" + CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinSignature();
            lineData[2] = "";
            lineData[3] = "Keine Daten";
            lineData[4] = "gesammelt!";
            lineData[5] = "";
            data.add(lineData);
            for (TemplateData serverTemplate : statSign.getTemplates()) {
                serverTemplate.getSignDatas().put(statSign.getLocation(), lineData);
            }
        }
        globalSignPacket = new StatSignPacket(data);
    }

    public void updateDailySignPacket() {
        ArrayList<String[]> data = new ArrayList<String[]>();
        ArrayList<StatSign> usedSigns = new ArrayList<StatSign>();
        for (String gameMode : CloudService.getCloudService().getManagerService().getStatsManager().getDailyTop().keySet()) {
            Game game = CloudService.getCloudService().getManagerService().getGameManager().getGameByName(gameMode);
            String[] types = game.getSignTypes().split("@");
            if (types.length == 0) continue;
            List<String> signLines = Arrays.asList(types);
            List<CloudPlayer> topPlayers = CloudService.getCloudService().getManagerService().getStatsManager().getDailyTop().get(gameMode);
            int rank = 1;
            for (CloudPlayer topPlayer : topPlayers) {
                List<StatSign> signs = CloudService.getCloudService().getManagerService().getSignStatsManager().getSignByArguments(gameMode, "daily", rank, game.getPrefix());
                if (signs.size() == 0) continue;
                for (StatSign statSign : signs) {
                    usedSigns.add(statSign);
                    String[] lineData = new String[6];
                    lineData[0] = statSign.getLocation();
                    lineData[1] = String.valueOf(topPlayer.getSkinValue()) + ";" + topPlayer.getSkinSignature();
                    lineData[2] = topPlayer.getName();
                    lineData[3] = "Rank #" + rank;
                    int current = 4;
                    Iterator iterator = signLines.iterator();
                    while (iterator.hasNext()) {
                        String signLine = (String)iterator.next();
                        if (!signLine.contains(";")) {
                            lineData[current] = signLine;
                        } else {
                            String[] line = signLine.split(";");
                            String field = line[0];
                            String anzeige = line[1];
                            long value = 0;
                            if (topPlayer.getStats().getDailyStats().containsKey(String.valueOf(game.getPrefix()) + "_" + field)) {
                                value = topPlayer.getStats().getDailyStats().get(String.valueOf(game.getPrefix()) + "_" + field);
                            }
                            lineData[current] = String.valueOf(anzeige) + ": " + value;
                        }
                        ++current;
                    }
                    for (TemplateData serverTemplate : statSign.getTemplates()) {
                        serverTemplate.getSignDatas().put(statSign.getLocation(), lineData);
                    }
                    data.add(lineData);
                }
                ++rank;
            }
        }
        for (StatSign statSign : statSigns) {
            if (!statSign.getMode().equalsIgnoreCase("daily") || usedSigns.contains(statSign)) continue;
            String[] lineData = new String[6];
            lineData[0] = statSign.getLocation();
            lineData[1] = String.valueOf(CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinValue()) + ";" + CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinSignature();
            lineData[2] = "";
            lineData[3] = "Keine Daten";
            lineData[4] = "gesammelt!";
            lineData[5] = "";
            data.add(lineData);
            for (TemplateData serverTemplate : statSign.getTemplates()) {
                serverTemplate.getSignDatas().put(statSign.getLocation(), lineData);
            }
        }
        dailySignPacket = new StatSignPacket(data);
    }

    public void updateMonthlySignPacket() {
        ArrayList<String[]> data = new ArrayList<String[]>();
        ArrayList<StatSign> usedSigns = new ArrayList<StatSign>();
        for (String gameMode : CloudService.getCloudService().getManagerService().getStatsManager().getMonthlyTop().keySet()) {
            Game game = CloudService.getCloudService().getManagerService().getGameManager().getGameByName(gameMode);
            String[] types = game.getSignTypes().split("@");
            if (types.length == 0) continue;
            List<String> signLines = Arrays.asList(types);
            List<CloudPlayer> topPlayers = CloudService.getCloudService().getManagerService().getStatsManager().getMonthlyTop().get(gameMode);
            int rank = 1;
            for (CloudPlayer topPlayer : topPlayers) {
                List<StatSign> signs = CloudService.getCloudService().getManagerService().getSignStatsManager().getSignByArguments(gameMode, "monthly", rank, game.getPrefix());
                if (signs.size() == 0) continue;
                for (StatSign statSign : signs) {
                    usedSigns.add(statSign);
                    String[] lineData = new String[6];
                    lineData[0] = statSign.getLocation();
                    lineData[1] = String.valueOf(topPlayer.getSkinValue()) + ";" + topPlayer.getSkinSignature();
                    lineData[2] = topPlayer.getName();
                    lineData[3] = "Rank #" + rank;
                    int current = 4;
                    Iterator iterator = signLines.iterator();
                    while (iterator.hasNext()) {
                        String signLine = (String)iterator.next();
                        if (!signLine.contains(";")) {
                            lineData[current] = signLine;
                        } else {
                            String[] line = signLine.split(";");
                            String field = line[0];
                            String display = line[1];
                            long value = 0;
                            if (topPlayer.getStats().getMonthlyStats().containsKey(game.getPrefix() + "_" + field)) {
                                value = topPlayer.getStats().getMonthlyStats().get(game.getPrefix() + "_" + field);
                            }
                            lineData[current] = display + ": " + value;
                        }
                        ++current;
                    }
                    for (TemplateData serverTemplate : statSign.getTemplates()) {
                        serverTemplate.getSignDatas().put(statSign.getLocation(), lineData);
                    }
                    data.add(lineData);
                }
                ++rank;
            }
        }
        for (StatSign statSign : statSigns) {
            if (!statSign.getMode().equalsIgnoreCase("monthly") || usedSigns.contains(statSign)) continue;
            String[] lineData = new String[6];
            lineData[0] = statSign.getLocation();
            lineData[1] = String.valueOf(CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinValue()) + ";" + CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinSignature();
            lineData[2] = "";
            lineData[3] = "Keine Daten";
            lineData[4] = "gesammelt!";
            lineData[5] = "";
            data.add(lineData);
            for (TemplateData serverTemplate : statSign.getTemplates()) {
                serverTemplate.getSignDatas().put(statSign.getLocation(), lineData);
            }
        }
        monthlySignPacket = new StatSignPacket(data);
    }

    public void sendStatPacketToAllLobbies(StatSignPacket statSignPacket) {
        for (Lobby lobby : CloudService.getCloudService().getManagerService().getLobbyManager().getLobbyServers()) {
            CloudService.getCloudService().getManagerService().getSignStatsManager().sendStatPacketToLobby(lobby.getServer(), statSignPacket);
        }
    }

    public void sendStatPacketToLobby(Server server, StatSignPacket statSignPacket) {
        server.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.STAT_SIGN_PACKET, statSignPacket));
    }

    public List<StatSign> getStatSigns() {
        return statSigns;
    }

    public StatSignPacket getDailySignPacket() {
        return dailySignPacket;
    }

    public StatSignPacket getMonthlySignPacket() {
        return monthlySignPacket;
    }

    public StatSignPacket getGlobalSignPacket() {
        return globalSignPacket;
    }
}