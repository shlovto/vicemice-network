package net.vicemice.hector.utils;

import net.vicemice.hector.network.proxy.Proxy;
import net.vicemice.hector.player.CloudPlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Server {
    private String host;
    private String name;
    private int port;
    private List<CloudPlayer> players = new ArrayList<>();
    private HashMap<Proxy, Integer> playerCount = new HashMap<>();

    public Server(String name, String host, int port) {
        this.name = name;
        this.host = host;
        this.port = port;
    }

    public void sendServerMessage(String message) {
        for (CloudPlayer cloudPlayer : this.players) {
            cloudPlayer.sendMessage(message);
        }
    }

    public void sendServerMessages(String[] messages) {
        for (CloudPlayer cloudPlayer : this.players) {
            cloudPlayer.sendMessages(messages);
        }
    }

    public String getHost() {
        return this.host;
    }

    public String getName() {
        return this.name;
    }

    public int getPort() {
        return this.port;
    }

    public List<CloudPlayer> getPlayers() {
        return this.players;
    }
}

