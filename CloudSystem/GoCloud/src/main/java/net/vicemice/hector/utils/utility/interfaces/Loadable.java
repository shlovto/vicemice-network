package net.vicemice.hector.utils.utility.interfaces;

public interface Loadable {
    public boolean load() throws Exception;
}