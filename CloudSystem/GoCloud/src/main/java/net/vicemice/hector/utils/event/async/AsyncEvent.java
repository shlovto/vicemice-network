package net.vicemice.hector.utils.event.async;

import net.vicemice.hector.utils.event.Event;

public abstract class AsyncEvent<E extends AsyncEvent<?>> extends Event {
    private AsyncPoster<E> poster;

    public AsyncEvent(AsyncPoster<E> poster) {
        this.poster = poster;
        this.asynchronous = true;
    }

    public AsyncPoster<E> getPoster() {
        return this.poster;
    }
}

