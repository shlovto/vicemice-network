package net.vicemice.hector.manager.blacklist;

import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.proxy.Proxy;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.proxy.players.ProxyBlacklistedPlayersPacket;
import org.bson.Document;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BlacklistManager {

    @Getter
    private List<UUID> players;

    public void reload() {
        this.players = new ArrayList<>();

        for (Document document : CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("blacklistedPlayers")) {
            String uniqueId = document.getString("uniqueId");
            this.players.add(UUID.fromString(uniqueId));
        }

        for (Proxy proxy : CloudService.getCloudService().getManagerService().getProxyManager().getProxies()) {
            proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_BLACKLISTED_PLAYERS, new ProxyBlacklistedPlayersPacket(CloudService.getCloudService().getManagerService().getBlacklistManager().getPlayers())));
        }

        CloudService.getCloudService().getTerminal().writeMessage("§3Loaded §e§l" + this.players.size() + " Blacklisted §3Players");
    }

    public void addPlayer(UUID uniqueId) {
        if (this.players.contains(uniqueId)) return;

        for (Proxy proxy : CloudService.getCloudService().getManagerService().getProxyManager().getProxies()) {
            proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_BLACKLISTED_PLAYERS, new ProxyBlacklistedPlayersPacket(CloudService.getCloudService().getManagerService().getBlacklistManager().getPlayers())));
        }

        this.players.add(uniqueId);
        CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("blacklistedPlayers").insertOne(new Document("uniqueId", String.valueOf(uniqueId)));
    }

    public void removePlayer(UUID uniqueId) {
        if (!this.players.contains(uniqueId)) return;

        for (Proxy proxy : CloudService.getCloudService().getManagerService().getProxyManager().getProxies()) {
            proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_BLACKLISTED_PLAYERS, new ProxyBlacklistedPlayersPacket(CloudService.getCloudService().getManagerService().getBlacklistManager().getPlayers())));
        }

        this.players.remove(uniqueId);
        CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("blacklistedPlayers").deleteOne(new Document("uniqueId", String.valueOf(uniqueId)));
    }
}
