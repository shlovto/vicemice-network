package net.vicemice.hector.player.session;

import lombok.Getter;
import lombok.Setter;

import java.beans.ConstructorProperties;
import java.util.UUID;

public class Session {

    @Getter
    private UUID uuid;
    @Getter
    @Setter
    private long start, end, time;
    @Getter
    @Setter
    private String ip, version;

    @ConstructorProperties(value={"uuid", "start", "end", "time", "ip", "version"})
    public Session(UUID uuid, long start, long end, long time, String ip, String version) {
        this.uuid = uuid;
        this.start = start;
        this.end = end;
        this.time = time;
        this.ip = ip;
        this.version = version;
    }
}