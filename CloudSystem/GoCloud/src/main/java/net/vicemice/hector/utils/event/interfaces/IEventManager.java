package net.vicemice.hector.utils.event.interfaces;

import net.vicemice.hector.utils.event.Event;
import net.vicemice.hector.utils.event.EventKey;
import net.vicemice.hector.utils.event.IEventListener;

public interface IEventManager {
    public <T extends Event> void registerListener(EventKey var1, IEventListener<T> var2);

    public <T extends Event> void registerListeners(EventKey var1, IEventListener<T>... var2);

    public void unregisterListener(EventKey var1);

    public void unregisterListener(IEventListener<?> var1);

    public void unregisterListener(Class<? extends Event> var1);

    public <T extends Event> boolean callEvent(T var1);
}

