package net.vicemice.hector.player;

import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.manager.chatlog.ChatManager;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.ComponentMessagePacket;
import net.vicemice.hector.player.skin.Skin;
import net.vicemice.hector.utils.JSONUtil;
import net.vicemice.hector.utils.RandomStringGenerator;
import net.vicemice.hector.utils.language.Language;
import net.vicemice.hector.utils.Rank;
import org.bson.Document;
import org.json.JSONObject;

import java.util.*;

/*
 * Class created at 18:10 - 02.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerAPI {

    @Getter
    private String defaultSkinValue = "eyJ0aW1lc3RhbXAiOjE0NDg2MzQ1ODcyNDIsInByb2ZpbGVJZCI6IjYwNmUyZmYwZWQ3NzQ4NDI5ZDZjZTFkMzMyMWM3ODM4IiwicHJvZmlsZU5hbWUiOiJNSEZfUXVlc3Rpb24iLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzUxNjNkYWZhYzFkOTFhOGM5MWRiNTc2Y2FhYzc4NDMzNjc5MWE2ZTE4ZDhmN2Y2Mjc3OGZjNDdiZjE0NmI2In19fQ==", defaultSkinSignature = "eosGuYbzqNK8UBllFajP+cVBbak0Qnqt2NafONsp7U18WIlVajvoKl0ulnEYlAPXKmV4X2B4XF0KklKHT4jwf4sa1HNLyJfYu7lyxJPKGFrYpdGEL2LAiMRKrP7U/b2ZmQHQQ8cKmPScsf/ltLggG9J0mQBxrd4VuoSIje21Y7QxXOMU80QdGyG8MlBqoEM15zUYlsYp2+nTpK0SqaRdJLymdHK9rdShl+hAgti8FKrRgS5Sr/OjICz6QXSLUrvpis7HoZk47GL/D+SmNNmtv+pT95UZz0ObYP3iEFer7ZHsKLS4d6qGe8qcyu+CLJ3cvFf66kuqOc8YbPjzHETVT9KrZkDRkFWoyL4jgcjFMc2l70wLL4Uhz94Rb7flBVeT4BXCoXjn9hXT2oZbRdtQsXMG87Bik0MGeqoQgtqaeCi8uwOeW2kbjl4eD7iley9I4axq+J74tYZL9+734fLdK0LMLZ2ncy3vGc4Kj2NFoKSttFyA3Vei6YeUg6b2hTiDS+meVsdpdO6NkQVWG3dlpENZk0zm+p3U33KMNWcjAxXzsWmw+a68ktUjuAbj1oZniVkPI6VPMm21/a1N92mVBKFQMX2rj1oiUIByIZe/iHPQe3luRiRBLIUwU1L+0LEzdPpq0eM2R6PQj8GNz/fxbpTCy+qcf1Pe9/4HoDGLxE8=";
    
    public String createChatLog(UUID creator, UUID tracked, String server, boolean report) {
        String id = null;
        try {
            id = RandomStringGenerator.generateRandomString(12, RandomStringGenerator.Mode.ALPHANUMERIC);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        long time = System.currentTimeMillis();

        try {
            Document document = new Document("id", id);
            document.append("creator", creator.toString());
            document.append("tracked", tracked.toString());
            document.append("server", server);
            document.append("time", time);
            document.append("report", String.valueOf(report));

            CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("chatLogs").insertOne(document);
        } catch (Exception exc) {
            exc.printStackTrace();
        }

        CloudService.getCloudService().getManagerService().getChatManager().addMessage(new ChatManager.Message(creator, server, CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(creator).getIp(), "CHATLOG_CREATE", time));

        return id;
    }

    public void sendModerationMessage(String message) {
        CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().forEach(e -> {
            CloudPlayer cloudPlayer = this.getCloudPlayerByUniqueId(e);
            if (!cloudPlayer.getRank().isJrModeration() || !cloudPlayer.isNotify()) return;
            cloudPlayer.sendMessage(message);
        });
    }

    public void sendModerationMessage(String message, Language.LanguageType languageType) {
        CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().forEach(e -> {
            CloudPlayer cloudPlayer = this.getCloudPlayerByUniqueId(e);
            if (!cloudPlayer.getRank().isJrModeration() || !cloudPlayer.isNotify()) return;
            if (!(cloudPlayer.getLanguage() == languageType)) return;
            cloudPlayer.sendMessage(message);
        });
    }

    public void sendTeamMessage(String message) {
        CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().forEach(e -> {
            CloudPlayer cloudPlayer = this.getCloudPlayerByUniqueId(e);
            if (!cloudPlayer.getRank().isTeam() || !cloudPlayer.isNotify()) return;
            cloudPlayer.sendRawMessage(message);
        });
    }

    public void sendLocalTeamMessage(String server, String string, Object... objects) {
        if (server == null) return;
        CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().forEach(e -> {
            CloudPlayer cloudPlayer = this.getCloudPlayerByUniqueId(e);
            if (!cloudPlayer.getRank().isTeam() || !cloudPlayer.isNotify()) return;
            if (!cloudPlayer.getServer().getName().equalsIgnoreCase(server)) return;
            cloudPlayer.sendMessage(string, objects);
        });
    }

    public void sendTeamMessage(String string, Object... objects) {
        CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().forEach(e -> {
            CloudPlayer cloudPlayer = this.getCloudPlayerByUniqueId(e);
            if (!cloudPlayer.getRank().isTeam() || !cloudPlayer.isNotify()) return;
            cloudPlayer.sendMessage(string, objects);
        });
    }

    public void sendTeamMessage(String message, Language.LanguageType languageType) {
        CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().forEach(e -> {
            CloudPlayer cloudPlayer = this.getCloudPlayerByUniqueId(e);
            if (!cloudPlayer.getRank().isTeam() || !cloudPlayer.isNotify()) return;
            if (!(cloudPlayer.getLanguage() == languageType)) return;
            cloudPlayer.sendRawMessage(message);
        });
    }

    public void sendAdminMessage(String message) {
        CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().forEach(e -> {
            CloudPlayer cloudPlayer = this.getCloudPlayerByUniqueId(e);
            if (!cloudPlayer.getRank().isHigherEqualsLevel(Rank.ADMIN) || !cloudPlayer.isNotify()) return;
            cloudPlayer.sendRawMessage(message);
        });
    }

    public void sendAdminMessage(String message, Object... objects) {
        CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().forEach(e -> {
            CloudPlayer cloudPlayer = this.getCloudPlayerByUniqueId(e);
            if (!cloudPlayer.getRank().isHigherEqualsLevel(Rank.ADMIN) || !cloudPlayer.isNotify()) return;
            cloudPlayer.sendMessage(message, objects);
        });
    }

    public void sendAdminMessage(ComponentMessagePacket componentMessagePacket) {
        CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().forEach(e -> {
            CloudPlayer cloudPlayer = this.getCloudPlayerByUniqueId(e);
            if (!cloudPlayer.getRank().isHigherEqualsLevel(Rank.ADMIN) || !cloudPlayer.isNotify()) return;
            cloudPlayer.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMPONENT_MESSAGE_PACKET, componentMessagePacket));
        });
    }

    public List<CloudPlayer> getCloudPlayersFromServer(String server) {
        List<CloudPlayer> players = new ArrayList<>();

        for (UUID uniqueIdPlayers : CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers()) {
            CloudPlayer player = this.getCloudPlayerByUniqueId(uniqueIdPlayers);

            if (player.getServer() != null && player.getServer().getName().equals(server)) {
                players.add(player);
            }
        }

        return players;
    }

    public CloudPlayer getCloudPlayerByTeamSpeakID(String id) {
        for (UUID uuid : CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().keySet()) {
            CloudPlayer cloudPlayer = this.getCloudPlayerByUniqueId(uuid);
            if (!cloudPlayer.getTeamSpeakId().equalsIgnoreCase(id)) continue;
            return cloudPlayer;
        }
        return null;
    }

    public CloudPlayer getCloudPlayerByDiscordID(String id) {
        for (UUID uuid : CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().keySet()) {
            CloudPlayer cloudPlayer = this.getCloudPlayerByUniqueId(uuid);
            if (!cloudPlayer.getDiscordId().equalsIgnoreCase(id)) continue;
            return cloudPlayer;
        }
        return null;
    }

    public CloudPlayer getCloudPlayerByIP(String ip) {
        for (UUID uuid : CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers()) {
            CloudPlayer cloudPlayer = this.getCloudPlayerByUniqueId(uuid);
            if (!cloudPlayer.getIp().equalsIgnoreCase(ip)) continue;
            return cloudPlayer;
        }
        return null;
    }

    public ArrayList<CloudPlayer> getCloudPlayersByIP(String ip) {
        ArrayList<CloudPlayer> cloudPlayers = new ArrayList<>();
        for (UUID uuid : CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().keySet()) {
            CloudPlayer cloudPlayer = this.getCloudPlayerByUniqueId(uuid);
            if (!cloudPlayer.getIp().equalsIgnoreCase(ip)) continue;
            cloudPlayers.add(cloudPlayer);
        }
        return cloudPlayers;
    }

    public Optional<CloudPlayer> getCloudPlayer(UUID uniqueId, boolean forceOnline) {
        CloudPlayer player = this.getCloudPlayerByUniqueId(uniqueId);
        if (forceOnline && player != null && !player.isOnline()) {
            player = null;
        }
        return Optional.ofNullable(player);
    }

    public Optional<CloudPlayer> getCloudPlayer(String name, boolean forceOnline) {
        CloudPlayer player = this.getCloudPlayerByName(name);
        if (forceOnline && player != null && !player.isOnline()) {
            player = null;
        }
        return Optional.ofNullable(player);
    }

    public Optional<CloudPlayer> getCloudPlayer(UUID uuid) {
        return this.getCloudPlayer(uuid, false);
    }

    public CloudPlayer getCloudPlayerByUniqueId(UUID uuid) {
        return CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().get(uuid);
    }

    public CloudPlayer getCloudPlayerByName(String name) {
        return CloudService.getCloudService().getManagerService().getPlayerManager().getNameCloudPlayers().get(name.toLowerCase());
    }

    public String getLanguage(String ip) {
        return "de-DE";
    }

    public Skin getSkin(String uuid) {
        JSONUtil JSONUtil = new JSONUtil(net.vicemice.hector.utils.JSONUtil.API.MCSTATS, uuid);
        String value = JSONUtil.getObject().getJSONObject("response").getJSONObject("skin").getString("value");
        String signature = JSONUtil.getObject().getJSONObject("response").getJSONObject("skin").getString("signature");
        return new Skin(value, signature);
    }

    public String getPlayerName(UUID uuid) {
        JSONObject jsonObject = new JSONUtil(JSONUtil.API.MCSTATS, String.valueOf(uuid)).getObject();
        if (jsonObject.has("response") && jsonObject.getJSONObject("response").has("name")) {
            return jsonObject.getJSONObject("response").getString("name");
        }

        return null;
    }

    public UUID getPlayerUniqueId(String s) {
        JSONObject jsonObject = new JSONUtil(JSONUtil.API.MCSTATS, s).getObject();
        if (jsonObject.has("response") && jsonObject.getJSONObject("response").has("UUID")) {
            return UUID.fromString(jsonObject.getJSONObject("response").getString("UUID"));
        }

        return null;
    }
}