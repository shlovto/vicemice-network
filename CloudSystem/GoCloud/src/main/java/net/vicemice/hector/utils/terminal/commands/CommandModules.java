package net.vicemice.hector.utils.terminal.commands;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.network.modules.Module;
import net.vicemice.hector.utils.terminal.ArgumentList;
import net.vicemice.hector.utils.terminal.CommandExecutor;
import net.vicemice.hector.utils.terminal.Terminal;

public class CommandModules implements CommandExecutor {

    @Override
    public void onCommand(String command, Terminal writer, String[] args) {
        if (args.length == 1) {
            if (args[0].equals("reload")) {
                CloudService.getCloudService().getManagerService().getModuleManager().disableModules();
                writer.write("\u00a76Try to reload all Modules...");

                try {
                    CloudService.getCloudService().getManagerService().getModuleManager().loadModules();
                } catch (Exception e) {
                    CloudService.getCloudService().getTerminal().writeMessage(" ");
                    CloudService.getCloudService().getTerminal().writeMessage("Could not load Modules");
                    CloudService.getCloudService().getTerminal().writeMessage("Error: " + e.getLocalizedMessage());
                }

                writer.writeMessage("\u00a7aSuccessfully \u00a77reloaded \u00a76" + CloudService.getCloudService().getManagerService().getModuleManager().getModules().size() + " \u00a77Modules");
            } else if (args[0].equals("list")) {
                writer.writeMessage("\u00a77There are \u00a76" + CloudService.getCloudService().getManagerService().getModuleManager().getModules().size() + " \u00a77Modules:");
                for (Module module : CloudService.getCloudService().getManagerService().getModuleManager().getModules()) {
                    writer.write("\u00a78- \u00a76" + module.getName() + " \u00a77v\u00a76" + module.getVersion() + " \u00a77by \u00a76" + module.getAuthor());
                }
            } else {
                writer.writeMessage("\u00a7cSubcommand not found.");
            }
        } else if (args.length == 2) {
            if (args[0].equals("reload")) {
                Module module = CloudService.getCloudService().getManagerService().getModuleManager().getModuleByName(args[1]);
                if (module != null) {
                    CloudService.getCloudService().getManagerService().getModuleManager().disableModule(module);
                    try {
                        CloudService.getCloudService().getManagerService().getModuleManager().loadModules();
                    } catch (Exception e) {
                        writer.writeMessage("\u00a7cError while loading Modules");
                        e.printStackTrace();
                    }
                    writer.writeMessage("\u00a7aSuccessfully \u00a77reloaded '\u00a76" + module.getName() + "\u00a77'");
                } else {
                    writer.writeMessage("\u00a7cModule not found");
                }
            } else if (args[0].equals("stop")) {
                Module module = CloudService.getCloudService().getManagerService().getModuleManager().getModuleByName(args[1]);
                if (module != null) {
                    CloudService.getCloudService().getManagerService().getModuleManager().disableModule(module);
                    writer.writeMessage("\u00a7aSuccessfully \u00a77stopped '\u00a76" + module.getName() + "\u00a77'");
                } else {
                    writer.writeMessage("\u00a7cModule not found");
                }
            } else {
                writer.writeMessage("\u00a7cSubcommand not found.");
            }
        } else {
            writer.write("\u00a7aCommands: ");
            writer.writeMessage("  \u00a77- \u00a7a/modules reload");
            writer.write("  \u00a77- \u00a7a/modules list");
            writer.write("  \u00a77- \u00a7a/modules reload <Module>");
            writer.write("  \u00a77- \u00a7a/modules stop <Module>");
        }
    }

    @Override
    public ArgumentList getArguments() {
        return ArgumentList.builder().arg(new ArgumentList.Argument("/modules", "Module-Managment")).build();
    }
}