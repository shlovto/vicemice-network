package net.vicemice.hector.manager.beforejoined;

import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.player.beforejoined.BeforePlayer;
import org.bson.Document;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

/*
 * Class created at 22:10 - 02.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class BeforeManager {
    @Getter
    private ConcurrentHashMap<String, BeforePlayer> beforePlayers = new ConcurrentHashMap<>();

    public void loadAllPlayers() {
        ArrayList<Document> list = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("bPlayers");
        int count = 0;

        for (Document document : list) {
            BeforePlayer beforePlayer = new BeforePlayer(document.getString("name"));
            beforePlayer.setRank(document.getString("rank"));
            beforePlayer.setInserted(true);
            ++count;
            beforePlayers.put(beforePlayer.getName().toLowerCase(), beforePlayer);
        }

        CloudService.getCloudService().getTerminal().writeMessage("§3Loaded §e§l" + count + " §3Before Players");
    }

    public BeforePlayer getPlayer(String name) {
        if (beforePlayers.containsKey(name = name.toLowerCase())) {
            return beforePlayers.get(name);
        }
        BeforePlayer beforePlayer = new BeforePlayer(name);
        beforePlayers.put(name, beforePlayer);
        return beforePlayer;
    }

    public BeforePlayer exactPlayer(String name) {
        name = name.toLowerCase();
        return beforePlayers.get(name);
    }
}