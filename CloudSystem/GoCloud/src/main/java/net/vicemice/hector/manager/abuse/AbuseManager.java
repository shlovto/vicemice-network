package net.vicemice.hector.manager.abuse;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.proxy.Proxy;
import net.vicemice.hector.packets.types.player.abuse.PlayerBanDataPacket;
import net.vicemice.hector.packets.types.player.abuse.PlayerMuteDataPacket;
import net.vicemice.hector.packets.types.player.report.ReportPlayersPacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.utils.Abuses;
import net.vicemice.hector.utils.*;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.utils.players.punish.Punish;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/*
 * Class created at 22:03 - 03.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class AbuseManager {

    @Getter
    @Setter
    private HashMap<String, Abuses.AbuseData> banId;

    public void loadBanIDs() {
        this.banId.clear();
        try {
            int i = 0;
            ArrayList<Document> list = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("banLog");

            for (Document document : list) {
                if (document.getString("id") == null) continue;
                if (document.getString("id").isEmpty()) continue;
                UUID user = UUID.fromString(document.getString("user"));
                UUID creator = UUID.fromString(document.getString("creator"));
                String id = document.getString("id");
                PunishType punishType = PunishType.valueOf(document.getString("typ"));
                long length = document.getLong("length");
                long created = document.getLong("created");
                String reason = document.getString("reason");
                String comment = document.getString("comment");
                boolean shorted = document.getBoolean("shorted");
                boolean removed = document.getBoolean("removed");

                Abuses.AbuseData abuseData = new Abuses.AbuseData(user, creator, id, reason, punishType, length, created, comment, shorted, removed);

                this.banId.put(id, abuseData);
                ++i;
            }

            CloudService.getCloudService().getTerminal().writeMessage("§3Loaded §e§l" + i + " §3abuses");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Abuses.AbuseData getBanLogByID(String id) {
        if (this.banId == null || this.banId.isEmpty()) return null;
        return this.banId.get(id);
    }

    public void removeBanLogFromCache(String id) {
        try {
            this.banId.remove(id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void removeBanLog(String id) {
        try {
            new Thread(() -> {
                Document document = new Document("removed", true);

                Bson filter = new Document("id", String.valueOf(id));
                CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("banLog").updateOne(filter, new Document("$set", document));
            }).start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void updateBanLog(String id, UUID user, UUID creator, PunishType punishType, long created, long length, String reason, String comment, boolean shorted, boolean removed) {
        try {
            new Thread(() -> {
                Document document = new Document("user", String.valueOf(user));
                document.append("creator", String.valueOf(creator));
                document.append("typ", punishType.name().toUpperCase());
                document.append("created", created);
                document.append("length", length);
                document.append("reason", reason);
                document.append("shorted", shorted);
                document.append("removed", removed);
                if (comment != null)
                    document.append("comment", comment);

                Bson filter = new Document("id", String.valueOf(id));
                CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("banLog").updateOne(filter, new Document("$set", document));
            }).start();

            this.removeBanLogFromCache(id);
            this.banId.put(id, new Abuses.AbuseData(user, creator, id, reason, punishType, length, created, comment, shorted, removed));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String generateBanId() {
        String id = "";

        FindIterable<Document> cursor;
        MongoCollection<Document> collection;

        do {
            id = RandomStringGenerator.generateRandomString(255, RandomStringGenerator.Mode.ALPHANUMERIC);
        } while ((cursor = (collection = CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("banLog")).find(Filters.eq("id", id))).first() != null);

        return id;
    }

    public void banLog(String banId, UUID user, UUID creator, PunishType punishType, long created, long length, String reason, String comment, boolean shorted, boolean removed) {
        try {
            new Thread(() -> {
                Document document = new Document("user", String.valueOf(user));
                document.append("id", banId);
                document.append("creator", String.valueOf(creator));
                document.append("typ", punishType.name().toUpperCase());
                document.append("created", created);
                document.append("length", length);
                document.append("reason", reason);
                document.append("removed", removed);
                document.append("punish", false);
                document.append("shorted", shorted);
                document.append("comment", comment);

                CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("banLog").insertOne(document);
            }).start();

            this.banId.put(banId, new Abuses.AbuseData(user, creator, banId, reason, punishType, length, created, comment, shorted, removed));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (punishType == PunishType.BAN) {
            CloudService.getCloudService().getManagerService().getPlayerManager().getIpBanned().put(CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(user).getIp(), (TimeUnit.DAYS.toMillis(1)+System.currentTimeMillis()));
        }
    }

    public void kickPlayer(CloudPlayer cloudPlayer, String reason, CloudPlayer from) {
        cloudPlayer.kickPlayer( MessageFormat.format(cloudPlayer.getLanguageMessage("kick-screen"), reason).replace("&", "§"));
        CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().sendTeamMessage("team-kick", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()), (from.getRank().getRankColor()+from.getName()), reason);
        this.banLog("", cloudPlayer.getUniqueId(), from.getUniqueId(), PunishType.KICK, System.currentTimeMillis(), System.currentTimeMillis(), reason, "KICK", false, true);
    }

    public Return punish(CloudPlayer target, CloudPlayer sender, String reason, String comment, Long length, boolean mute, boolean silent) {
        if (!mute && target.getAbuses().getBan() != null) {
            return Return.ERROR;
        } else if (mute && target.getAbuses().getMute() != null) {
            return Return.ERROR;
        }

        if (!mute) {
            if (!silent) {
                try {
                    List<ReportPlayersPacket.AccusationsReason> auccusations = CloudService.getCloudService().getManagerService().getReportManager().getAccusationsFrom(target);
                    if (!auccusations.isEmpty()) {
                        for (ReportPlayersPacket.AccusationsReason auc : auccusations) {
                            CloudPlayer reporter = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(auc.getName());
                            if (reporter == null) continue;
                            if (reporter.isOnline()) reporter.sendMessage("report-processed-online", target.getRank().getRankColor()+target.getName());
                            if (!reporter.isOnline()) {
                                reporter.setReportsProcessed((reporter.getReportsProcessed()+1));
                                if (reporter.getReportProcessed() == null) reporter.setReportProcessed(target.getUniqueId());
                            }
                            reporter.getLevel().addXp(10);
                            reporter.addCoins(100);
                        }
                        CloudService.getCloudService().getManagerService().getReportManager().removeReport(target);
                    }
                }
                catch (Exception exc) {
                    exc.printStackTrace();
                    CloudService.getCloudService().getTerminal().writeMessage(exc.getLocalizedMessage());
                }
            }

            String banId = this.generateBanId();
            Abuses.Abuse abuse = new Abuses.Abuse(banId, sender.getUniqueId(), (reason != null ? reason : ""), (length != null ? length : 0), System.currentTimeMillis(), (comment != null ? comment : ""));

            target.getAbuses().setBan(abuse);
            target.updateToDatabase(false);

            if (!CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithBan().contains(target.getUniqueId())) {
                CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithBan().add(target.getUniqueId());
            }

            PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_BAN_DATA, new PlayerBanDataPacket(target.getUniqueId(), new net.vicemice.hector.packets.Abuses(new net.vicemice.hector.packets.Abuses.Abuse(sender.getUniqueId(), reason, length))));
            for (Proxy proxy : CloudService.getCloudService().getManagerService().getProxyManager().getProxies()) {
                proxy.sendPacket(initPacket);
            }

            if (!silent) {
                if (length == 0) {
                    CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithRank().forEach(e -> {
                        CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(e);
                        if (!cloudPlayer.getRank().isTeam() || !cloudPlayer.isNotify()) return;
                        String reasonMessage = reason;
                        if (CloudService.getCloudService().getManagerService().getPunishManager().getByWord(reason) != null) {
                            reasonMessage = cloudPlayer.getLanguageMessage(reason);
                        }
                        cloudPlayer.sendMessage("team-ban", (target.getRank().getRankColor()+target.getName()), (sender.getRank().getRankColor()+sender.getName()), "§c"+cloudPlayer.getLanguageMessage("PERMANENT"), reasonMessage);
                    });
                } else {
                    CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithRank().forEach(e -> {
                        CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(e);
                        if (!cloudPlayer.getRank().isTeam() || !cloudPlayer.isNotify()) return;
                        String reasonMessage = reason;
                        if (CloudService.getCloudService().getManagerService().getPunishManager().getByWord(reason) != null) {
                            reasonMessage = cloudPlayer.getLanguageMessage(reason);
                        }
                        cloudPlayer.sendMessage("team-ban", (target.getRank().getRankColor()+target.getName()), (sender.getRank().getRankColor()+sender.getName()), GoAPI.getTimeManager().getRemainingTimeString(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), cloudPlayer.getLocale(), length), reasonMessage);
                    });
               }
            }

            this.banLog(banId, target.getUniqueId(), sender.getUniqueId(), PunishType.BAN, System.currentTimeMillis(), length, reason, comment, false, false);

            return Return.SUCCESS;
        } else if (mute) {
            if (!silent) {
                try {
                    List<ReportPlayersPacket.AccusationsReason> auccusations = CloudService.getCloudService().getManagerService().getReportManager().getAccusationsFrom(target);
                    if (!auccusations.isEmpty()) {
                        for (ReportPlayersPacket.AccusationsReason auc : auccusations) {
                            CloudPlayer reporter = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(auc.getName());
                            if (reporter == null || !reporter.isOnline()) continue;
                            reporter.sendMessage("report-thanks");
                            reporter.addCoins(100);
                            reporter.getLevel().addXp(10);
                        }
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                    CloudService.getCloudService().getTerminal().writeMessage(exc.getLocalizedMessage());
                }
            }

            String banId = this.generateBanId();
            Abuses.Abuse abuse = new Abuses.Abuse(banId, sender.getUniqueId(), (reason != null ? reason : ""), (length != null ? length : 0), System.currentTimeMillis(), (comment != null ? comment : ""));

            target.getAbuses().setMute(abuse);
            target.updateToDatabase(false);

            if (!CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithMute().contains(target.getUniqueId())) {
                CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithMute().add(target.getUniqueId());
            }

            PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_MUTE_DATA, new PlayerMuteDataPacket(target.getUniqueId(), new net.vicemice.hector.packets.Abuses(new net.vicemice.hector.packets.Abuses.Abuse(sender.getUniqueId(), reason, length))));
            for (Proxy proxy : CloudService.getCloudService().getManagerService().getProxyManager().getProxies()) {
                proxy.sendPacket(initPacket);
            }

            if (!silent) {
                if (length == 0) {
                    CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithRank().forEach(e -> {
                        CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(e);
                        if (!cloudPlayer.getRank().isTeam() || !cloudPlayer.isNotify()) return;
                        String reasonMessage = reason;
                        if (CloudService.getCloudService().getManagerService().getPunishManager().getByWord(reason) != null) {
                            reasonMessage = cloudPlayer.getLanguageMessage(reason);
                        }
                        cloudPlayer.sendMessage("team-mute", (target.getRank().getRankColor()+target.getName()), (sender.getRank().getRankColor()+sender.getName()), "§c"+cloudPlayer.getLanguageMessage("PERMANENT"), reasonMessage);
                    });
                } else {
                    CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithRank().forEach(e -> {
                        CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(e);
                        if (!cloudPlayer.getRank().isTeam() || !cloudPlayer.isNotify()) return;
                        String reasonMessage = reason;
                        if (CloudService.getCloudService().getManagerService().getPunishManager().getByWord(reason) != null) {
                            reasonMessage = cloudPlayer.getLanguageMessage(reason);
                        }
                        cloudPlayer.sendMessage("team-mute", (target.getRank().getRankColor()+target.getName()), (sender.getRank().getRankColor()+sender.getName()), GoAPI.getTimeManager().getRemainingTimeString(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), cloudPlayer.getLocale(), length), reasonMessage);
                    });
                }
            }

            this.banLog(banId, target.getUniqueId(), sender.getUniqueId(), PunishType.MUTE, System.currentTimeMillis(), length, reason, comment, false, false);

            return Return.SUCCESS;
        }

        return Return.UNKNOWN;
    }

    public Return remove(CloudPlayer target, CloudPlayer sender, String comment, boolean mute) {
        if (sender == null) {
            sender = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(UUID.fromString("336991c8-1199-457c-80bb-649042a489f5"));
        }

        if (target.getAbuses().getBan() != null && !mute) {
            CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithBan().remove(target.getUniqueId());
            Abuses.AbuseData abuseData = this.getBanLogByID(target.getAbuses().getBan().getBanId());
            this.updateBanLog(target.getAbuses().getBan().getBanId(), target.getUniqueId(), target.getAbuses().getBan().getAuthor(), PunishType.BAN, target.getAbuses().getBan().getCreated(), target.getAbuses().getBan().getLength(), target.getAbuses().getBan().getReason(), target.getAbuses().getBan().getComment(), (abuseData != null ? abuseData.isShorted() : false), true);
            this.banLog(this.generateBanId(), target.getUniqueId(), sender.getUniqueId(), PunishType.UNBAN, System.currentTimeMillis(), System.currentTimeMillis(), "", comment, false, true);

            target.getAbuses().setBan(null);
            target.updateToDatabase(false);

            PlayerBanDataPacket playerBanDataPacket = new PlayerBanDataPacket(target.getUniqueId(), new net.vicemice.hector.packets.Abuses(null));
            PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_BAN_DATA, playerBanDataPacket);
            for (Proxy proxy : CloudService.getCloudService().getManagerService().getProxyManager().getProxies()) {
                proxy.sendPacket(initPacket);
            }

            Document document = new Document("creator", target.getForumId());
            CloudService.getCloudService().getManagerService().getMongoManager().getClient().getDatabase("slave").getCollection("banappeals").deleteOne(document);

            return Return.SUCCESS;
        } else if (target.getAbuses().getMute() != null && mute) {
            CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithMute().remove(target.getUniqueId());
            Abuses.AbuseData abuseData = this.getBanLogByID(target.getAbuses().getMute().getBanId());
            this.updateBanLog(target.getAbuses().getMute().getBanId(), target.getUniqueId(), target.getAbuses().getMute().getAuthor(), PunishType.MUTE, target.getAbuses().getMute().getCreated(), target.getAbuses().getMute().getLength(), target.getAbuses().getMute().getReason(), target.getAbuses().getMute().getComment(), (abuseData != null ? abuseData.isShorted() : false), true);
            this.banLog(this.generateBanId(), target.getUniqueId(), sender.getUniqueId(), PunishType.UNMUTE, System.currentTimeMillis(), System.currentTimeMillis(), "", comment, false, true);

            target.getAbuses().setMute(null);
            target.updateToDatabase(false);

            PlayerMuteDataPacket playerMuteDataPacket = new PlayerMuteDataPacket(target.getUniqueId(), new net.vicemice.hector.packets.Abuses(null));
            PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_MUTE_DATA, playerMuteDataPacket);
            for (Proxy proxy : CloudService.getCloudService().getManagerService().getProxyManager().getProxies()) {
                proxy.sendPacket(initPacket);
            }

            Document document = new Document("creator", target.getForumId());
            CloudService.getCloudService().getManagerService().getMongoManager().getClient().getDatabase("slave").getCollection("banappeals").deleteOne(document);

            return Return.SUCCESS;
        }

        return Return.UNKNOWN;
    }

    public void shortBan(CloudPlayer cloudPlayer, long length, Punish reason, CloudPlayer sender, String banId, String comment) {
        CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithBan().remove(cloudPlayer.getUniqueId());

        cloudPlayer.getAbuses().setBan(null);
        cloudPlayer.updateToDatabase(false);

        Abuses.Abuse abuse = new Abuses.Abuse(banId, sender.getUniqueId(), reason.getKey().toUpperCase(), length, System.currentTimeMillis(), comment);

        cloudPlayer.getAbuses().setBan(abuse);
        cloudPlayer.updateToDatabase(false);

        if (!CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithBan().contains(cloudPlayer.getUniqueId())) {
            CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithBan().add(cloudPlayer.getUniqueId());
        }

        PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_BAN_DATA, new PlayerBanDataPacket(cloudPlayer.getUniqueId(), new net.vicemice.hector.packets.Abuses(new net.vicemice.hector.packets.Abuses.Abuse(sender.getUniqueId(), reason.getKey().toUpperCase(), length))));
        for (Proxy proxy : CloudService.getCloudService().getManagerService().getProxyManager().getProxies()) {
            proxy.sendPacket(initPacket);
        }

        boolean removed = (this.getBanLogByID(cloudPlayer.getAbuses().getBan().getBanId()) != null && this.getBanLogByID(cloudPlayer.getAbuses().getBan().getBanId()).isRemoved());
        this.updateBanLog(banId, cloudPlayer.getUniqueId(), sender.getUniqueId(), PunishType.BAN, System.currentTimeMillis(), length, reason.getKey().toUpperCase(), comment, true, removed);
    }

    public void shortMute(CloudPlayer cloudPlayer, long length, Punish reason, CloudPlayer sender, String banId, String comment) {
        CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithMute().remove(cloudPlayer.getUniqueId());

        cloudPlayer.getAbuses().setMute(null);
        cloudPlayer.updateToDatabase(false);

        Abuses.Abuse abuse = new Abuses.Abuse(banId, sender.getUniqueId(), reason.getKey().toUpperCase(), length, System.currentTimeMillis(), comment);

        cloudPlayer.getAbuses().setMute(abuse);
        cloudPlayer.updateToDatabase(false);

        if (!CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithMute().contains(cloudPlayer.getUniqueId())) {
            CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithMute().add(cloudPlayer.getUniqueId());
        }

        PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_MUTE_DATA, new PlayerMuteDataPacket(cloudPlayer.getUniqueId(), new net.vicemice.hector.packets.Abuses(new net.vicemice.hector.packets.Abuses.Abuse(sender.getUniqueId(), reason.getKey().toUpperCase(), length))));
        for (Proxy proxy : CloudService.getCloudService().getManagerService().getProxyManager().getProxies()) {
            proxy.sendPacket(initPacket);
        }

        boolean removed = (this.getBanLogByID(cloudPlayer.getAbuses().getMute().getBanId()) != null && this.getBanLogByID(cloudPlayer.getAbuses().getMute().getBanId()).isRemoved());
        this.updateBanLog(banId, cloudPlayer.getUniqueId(), sender.getUniqueId(), PunishType.MUTE, System.currentTimeMillis(), length, reason.getKey().toUpperCase(), comment, true, removed);
    }

    public long getLength(long points) {
        if (points == 3) {
            return TimeUnit.MINUTES.toMillis(10);
        } else if (points <= 6) {
            return TimeUnit.MINUTES.toMillis(15);
        } else if (points <= 9) {
            return TimeUnit.MINUTES.toMillis(30);
        } else if (points <= 12) {
            return TimeUnit.HOURS.toMillis(1);
        } else if (points <= 15) {
            return TimeUnit.HOURS.toMillis(3);
        } else if (points <= 18) {
            return TimeUnit.HOURS.toMillis(6);
        } else if (points <= 21) {
            return TimeUnit.HOURS.toMillis(12);
        } else if (points <= 24) {
            return TimeUnit.DAYS.toMillis(1);
        } else if (points <= 27) {
            return TimeUnit.DAYS.toMillis(3);
        } else if (points <= 30) {
            return TimeUnit.DAYS.toMillis(6);
        } else if (points <= 33) {
            return TimeUnit.DAYS.toMillis(9);
        } else if (points <= 36) {
            return TimeUnit.DAYS.toMillis(12);
        } else if (points <= 39) {
            return TimeUnit.DAYS.toMillis(15);
        } else if (points <= 42) {
            return TimeUnit.DAYS.toMillis(20);
        } else if (points <= 45) {
            return TimeUnit.DAYS.toMillis(30);
        } else if (points <= 48) {
            return TimeUnit.DAYS.toMillis(60);
        } else if (points <= 51) {
            return TimeUnit.DAYS.toMillis(90);
        } else if (points >= 54) {
            return 0;
        } else {
            return -1;
        }
    }

    public enum Return {
        SUCCESS,
        ERROR,
        UNKNOWN;
    }
}