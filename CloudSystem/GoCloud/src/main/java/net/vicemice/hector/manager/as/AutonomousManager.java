package net.vicemice.hector.manager.as;

import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.player.utils.ASNetwork;
import org.bson.Document;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/*
 * Class created at 14:46 - 28.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class AutonomousManager {

    @Getter
    private ConcurrentHashMap<Long, ASNetwork> networks = new ConcurrentHashMap<>();

    public void init() {
        this.networks.clear();
        ArrayList<Document> list = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("autonomousSystem");

        int count = 0;

        for (Document document : list) {
            count++;

            ASNetwork asNetwork = new ASNetwork(document.getString("organisation"), document.getLong("id"), document.getLong("created"), document.getLong("connections"), (UUID) document.get("firstPlayer"), document.getBoolean("blocked"));
            this.networks.put(asNetwork.getId(), asNetwork);
        }

        CloudService.getCloudService().getTerminal().writeMessage("§3Loaded §e§l" + count + " §3AS Networks");
    }
}