package net.vicemice.hector.utils.threading;

public abstract class RunnabledTask
implements Runnable {
    protected ScheduledTask task;

    public void scheduleSynchronized(Scheduler scheduler) {
        this.task = scheduler.runTaskSync(this);
    }

    public void scheduleSynchronized(Scheduler scheduler, int delay) {
        this.task = scheduler.runTaskDelaySync(this, delay);
    }

    public void scheduleSynchronized(Scheduler scheduler, int delay, int repeat) {
        this.task = scheduler.runTaskRepeatSync(this, delay, repeat);
    }

    public void scheduleAsynchronized(Scheduler scheduler) {
        this.task = scheduler.runTaskAsync(this);
    }

    public void scheduleAsynchronized(Scheduler scheduler, int delay) {
        this.task = scheduler.runTaskDelayAsync(this, delay);
    }

    public void scheduleAsynchronized(Scheduler scheduler, int delay, int repeat) {
        this.task = scheduler.runTaskRepeatAsync(this, delay, repeat);
    }

    public void cancel() {
        if (this.task != null) {
            this.task.cancel();
        }
    }
}

