package net.vicemice.hector.utils.terminal;

import jline.TerminalFactory;
import net.vicemice.hector.CloudService;
import org.apache.commons.cli.*;
import java.io.PrintWriter;
import java.util.Arrays;

/*
 * Class created at 20:19 - 31.12.2019
 * Copyright (C) 2016-2019 Minetasia Solutions
 */
public interface CommandExecutor {
    public static final Options options = new Options();
    public static final CommandLineParser optionsParser = new BasicParser();
    public static final HelpFormatter optionsHelper = new HelpFormatter();

    public void onCommand(String var1, Terminal var2, String[] var3);

    public ArgumentList getArguments();

    default public CommandLine paradiseOptions(String[] args, int start) {
        return this.paradiseOptions(args, start, true);
    }

    default public CommandLine paradiseOptions(String[] args, int start, boolean sendHelp) {
        try {
            CommandLine line = optionsParser.parse(options, Arrays.copyOfRange(args, start, args.length));
            if (line == null) {
                throw new Exception("line == null");
            }
            return line;
        }
        catch (Exception e) {
            if (sendHelp) {
                CloudService.getCloudService().getTerminal().writeMessage("\u00a7cException: " + e.getMessage());
                optionsHelper.printUsage(new PrintWriter(new CostumSystemPrintStream(CloudService.getCloudService().getTerminal())), TerminalFactory.get().getWidth(), "Command Help", options);
            }
            return null;
        }
    }

    default public void printHelp(boolean wrongUsage) {
        if (wrongUsage) {
            CloudService.getCloudService().getTerminal().writeMessage("\u00a7cWrong command usage!");
            CloudService.getCloudService().getTerminal().writeMessage("\u00a7cAvariable options:");
        }
        if (this.getArguments() != null) {
            for (ArgumentList.Argument s : this.getArguments().getArguments()) {
                CloudService.getCloudService().getTerminal().writeMessage(s.format());
            }
        } else {
            CloudService.getCloudService().getTerminal().writeMessage("\u00a7cNo argument list/help avariable!");
        }
    }

    default public String createArgumentInfo(String args, String usage) {
        return "\u00a7c" + args + " \u00a77| \u00a7a" + usage;
    }
}

