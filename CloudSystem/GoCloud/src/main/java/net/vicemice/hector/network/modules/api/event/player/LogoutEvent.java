package net.vicemice.hector.network.modules.api.event.player;

import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.event.Event;

public class LogoutEvent extends Event {
    private CloudPlayer cloudPlayer;

    public CloudPlayer getCloudPlayer() {
        return this.cloudPlayer;
    }

    public LogoutEvent(CloudPlayer cloudPlayer) {
        this.cloudPlayer = cloudPlayer;
    }
}