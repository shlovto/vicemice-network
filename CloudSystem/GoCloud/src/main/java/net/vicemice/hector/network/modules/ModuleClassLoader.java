package net.vicemice.hector.network.modules;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class ModuleClassLoader extends URLClassLoader implements ModuleLoader {
    private ModuleConfig config;

    public ModuleClassLoader(ModuleConfig config) throws MalformedURLException {
        super(new URL[]{config.getFile().toURI().toURL()});
        this.config = config;
    }

    @Override
    public Module loadModule() throws Exception {
        Class<?> bootstrap = this.loadClass(this.config.getMain());
        Module module = (Module)bootstrap.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        module.setClassLoader(this);
        module.setModuleConfig(this.config);
        module.onLoad();
        return module;
    }

    public ModuleConfig getConfig() {
        return this.config;
    }
}

