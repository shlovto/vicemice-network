package net.vicemice.hector.manager.replay;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.packets.types.player.replay.PacketReplayEntries;
import net.vicemice.hector.utils.map.CachedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/*
 * Class created at 02:22 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ReplayHistoryManager {
    private CachedHashMap<UUID, List<PacketReplayEntries.PacketReplayEntry>> entries = new CachedHashMap<>(10, TimeUnit.MINUTES);

    public void load() {

    }

    public void addEntry(UUID uniqueId, PacketReplayEntries.PacketReplayEntry entry) {
        try {
            Document document = new Document("player", String.valueOf(uniqueId));
            document.append("gameId", entry.getGameId());
            document.append("uuid", entry.getReplayUUID().toString());
            document.append("replayStartSeconds", entry.getReplayStartSeconds());
            document.append("timestamp", entry.getTimestamp());
            document.append("length", entry.getGameLength());
            document.append("gameType", entry.getGameType());
            document.append("mapType", entry.getMapType());
            document.append("map", entry.getMap());
            document.append("favorite", entry.isFavorite());
            document.append("players", StringUtils.join(entry.getPlayers(), ";"));

            CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("ReplayHistory").insertOne(document);

            if (this.entries.containsKey(uniqueId)) {
                this.entries.get(uniqueId).add(entry);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateEntry(UUID uniqueId, PacketReplayEntries.PacketReplayEntry entry, boolean favorite) {
        try {
            Document document = new Document("favorite", favorite);

            Document filter = new Document("player", String.valueOf(uniqueId)).append("gameId", entry.getGameId()).append("uuid", entry.getReplayUUID().toString());
            CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("ReplayHistory").updateOne(filter, new Document("$set", document));

            if (this.entries.containsKey(uniqueId)) {
                this.entries.get(uniqueId).add(entry);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeEntry(UUID uniqueId, PacketReplayEntries.PacketReplayEntry entry) {
        try {
            Document filter = new Document("player", String.valueOf(uniqueId)).append("gameId", entry.getGameId()).append("uuid", entry.getReplayUUID().toString());
            CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("ReplayHistory").deleteOne(filter);

            if (this.entries.containsKey(uniqueId)) {
                this.entries.get(uniqueId).remove(entry);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<PacketReplayEntries.PacketReplayEntry> getEntries(UUID uniqueId) {
        if (this.entries.containsKey(uniqueId)) {
            return this.entries.get(uniqueId);
        }
        ArrayList<PacketReplayEntries.PacketReplayEntry> entries = new ArrayList<>();
        try {
            Document filter = new Document("player", String.valueOf(uniqueId));

            ArrayList<Document> list = CloudService.getCloudService().getManagerService().getMongoManager().getDocumentsWithFilterAndSort("ReplayHistory", filter, new Document("_id", -1));

            for (Document document : list) {
                String gameId = document.getString("gameId");
                String uuid = document.getString("uuid");
                int replayStartSeconds = document.getInteger("replayStartSeconds");
                Long timestamp = document.getLong("timestamp");
                Long length = document.getLong("length");
                String gameType = document.getString("gameType");
                String mapType = document.getString("mapType");
                String map = document.getString("map");
                boolean favorite = document.getBoolean("favorite");
                List<String> players = Arrays.asList(document.getString("players").split(";"));
                entries.add(new PacketReplayEntries.PacketReplayEntry(gameId, UUID.fromString(uuid), replayStartSeconds, timestamp, length, new ArrayList<>(players), gameType, mapType, map, favorite));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entries;
    }

    public List<PacketReplayEntries.PacketReplayEntry> getEntries() {
        ArrayList<PacketReplayEntries.PacketReplayEntry> entries = new ArrayList<>();
        try {
            ArrayList<Document> list = CloudService.getCloudService().getManagerService().getMongoManager().getDocumentsWithSort("ReplayHistory", new Document("_id", -1));

            for (Document document : list) {
                String gameId = document.getString("gameId");
                String uuid = document.getString("uuid");
                int replayStartSeconds = document.getInteger("replayStartSeconds");
                Long timestamp = document.getLong("timestamp");
                Long length = document.getLong("length");
                String gameType = document.getString("gameType");
                String mapType = document.getString("mapType");
                String map = document.getString("map");
                boolean favorite = document.getBoolean("favorite");
                List<String> players = Arrays.asList(document.getString("players").split(";"));
                entries.add(new PacketReplayEntries.PacketReplayEntry(gameId, UUID.fromString(uuid), replayStartSeconds, timestamp, length, new ArrayList<>(players), gameType, mapType, map, favorite));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entries;
    }

    public List<PacketReplayEntries.PacketReplayEntry> getRecentEntries(UUID uniqueId) {
        ArrayList<PacketReplayEntries.PacketReplayEntry> entries = new ArrayList<>();
        for (PacketReplayEntries.PacketReplayEntry entry : getEntries(uniqueId)) {
            if ((entry.getTimestamp()+TimeUnit.DAYS.toMillis(7)) < System.currentTimeMillis()) continue;
            entries.add(entry);
        }
        return entries;
    }

    public List<PacketReplayEntries.PacketReplayEntry> getSavedEntries(UUID uniqueId) {
        ArrayList<PacketReplayEntries.PacketReplayEntry> entries = new ArrayList<>();
        for (PacketReplayEntries.PacketReplayEntry entry : getEntries(uniqueId)) {
            if (!entry.isFavorite()) continue;
            entries.add(entry);
        }
        return entries;
    }

    public PacketReplayEntries.PacketReplayEntry getReplay(String gameId) {
        for (PacketReplayEntries.PacketReplayEntry entry : getEntries()) {
            if (!entry.getGameId().equalsIgnoreCase(gameId)) continue;
            return entry;
        }

        return null;
    }

    public PacketReplayEntries.PacketReplayEntry getReplay(UUID uniqueId, String gameId, ReplayType replayType) {
        for (PacketReplayEntries.PacketReplayEntry entry : (replayType == ReplayType.RECENT ? getRecentEntries(uniqueId) : getSavedEntries(uniqueId))) {
            if (!entry.getGameId().equalsIgnoreCase(gameId)) continue;
            return entry;
        }

        return null;
    }

    public enum ReplayType {
        RECENT,
        SAVED
    }
}