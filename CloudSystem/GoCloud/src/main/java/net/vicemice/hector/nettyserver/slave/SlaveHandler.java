package net.vicemice.hector.nettyserver.slave;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.ReferenceCountUtil;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.manager.replay.ReplayHistoryManager;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.network.modules.api.event.server.ServerSendPacketEvent;
import net.vicemice.hector.network.slave.Slave;
import net.vicemice.hector.network.stats.Game;
import net.vicemice.hector.packets.types.lobby.LobbyJoinPacket;
import net.vicemice.hector.packets.types.lobby.LobbyLocPacket;
import net.vicemice.hector.packets.types.player.abuse.PunishPacket;
import net.vicemice.hector.packets.types.player.coins.EditCoinPacket;
import net.vicemice.hector.packets.types.player.coins.CoinsPacket;
import net.vicemice.hector.packets.types.player.coins.GetCoinsPacket;
import net.vicemice.hector.packets.types.player.database.AddPlayerDatabasePacket;
import net.vicemice.hector.packets.types.player.database.GetPlayerDatabaseValue;
import net.vicemice.hector.packets.types.player.edit.MapEditRequestPacket;
import net.vicemice.hector.packets.types.player.level.EditXPPacket;
import net.vicemice.hector.packets.types.player.lobby.PlayerSilentLobbyPacket;
import net.vicemice.hector.packets.types.player.punish.BanIpPacket;
import net.vicemice.hector.packets.types.player.rank.PlayerShopRankPacket;
import net.vicemice.hector.packets.types.player.report.PlayerReportedPacket;
import net.vicemice.hector.packets.types.player.report.RemoveReportPacket;
import net.vicemice.hector.packets.types.player.settings.SettingsPacket;
import net.vicemice.hector.packets.types.player.state.UpdatePlayerStatePacket;
import net.vicemice.hector.packets.types.player.user.RequestUserDataPacket;
import net.vicemice.hector.packets.types.server.clan.ClanInformationPacket;
import net.vicemice.hector.packets.types.server.clan.ClanRequestPacket;
import net.vicemice.hector.packets.types.server.mapedit.MapEditInfoPacket;
import net.vicemice.hector.packets.types.slave.SlaveAlivePacket;
import net.vicemice.hector.packets.types.slave.SlaveInitPacket;
import net.vicemice.hector.packets.types.slave.SlaveStopPacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.players.utils.DataUpdateType;
import net.vicemice.hector.utils.slave.TemplateData;
import net.vicemice.hector.packets.types.ErrorPacket;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.AFKPacket;
import net.vicemice.hector.packets.types.player.CommandPacket;
import net.vicemice.hector.packets.types.player.ConnectPacket;
import net.vicemice.hector.packets.types.player.MessagePacket;
import net.vicemice.hector.packets.types.player.replay.*;
import net.vicemice.hector.packets.types.player.stats.*;
import net.vicemice.hector.packets.types.server.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class SlaveHandler extends SimpleChannelInboundHandler<Object> {

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object o) throws Exception {
        new Thread(() -> {
            try {
                PacketHolder initPacket = (PacketHolder) o;

                if (initPacket.getType() == PacketType.REQUEST_USER_DATA) {
                    RequestUserDataPacket requestUserDataPacket = (RequestUserDataPacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(requestUserDataPacket.getUniqueId());
                    cloudPlayer.connectToServer(requestUserDataPacket.getServerId().split(":")[2]);
                } else if (initPacket.getType() == PacketType.AFK) {
                    AFKPacket afkPacket = (AFKPacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(afkPacket.getUniqueId());

                    if (afkPacket.isAfk() && !cloudPlayer.isAfk()) {
                        cloudPlayer.setAfk(true);
                        cloudPlayer.sendMessage("afk-idle");
                        if (cloudPlayer.getParty() != null) {
                            cloudPlayer.getParty().sendMessageToParty("party-player-marked-afk", (cloudPlayer.getRank().getRankColor() + cloudPlayer.getName()));
                        }
                    } else if (!afkPacket.isAfk() && cloudPlayer.isAfk()) {
                        cloudPlayer.setAfk(false);
                        cloudPlayer.sendMessage("afk-not-longer-idle");
                        if (cloudPlayer.getParty() != null) {
                            cloudPlayer.getParty().sendMessageToParty("party-player-unmarked-afk", (cloudPlayer.getRank().getRankColor() + cloudPlayer.getName()));
                        }
                    }
                } else if (initPacket.getType() == PacketType.STATS_EDIT) {
                    StatisticEditPacket statisticEditPacket = (StatisticEditPacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(statisticEditPacket.getUniqueId());

                    if (statisticEditPacket.getAction() == StatisticEditPacket.Action.ADD) {
                        CloudService.getCloudService().getManagerService().getStatsManager().addPlayerStat(cloudPlayer, statisticEditPacket.getKey().toLowerCase(), statisticEditPacket.getValue());
                    } else if (statisticEditPacket.getAction() == StatisticEditPacket.Action.REMOVE) {
                        CloudService.getCloudService().getManagerService().getStatsManager().removePlayerStat(cloudPlayer, statisticEditPacket.getKey().toLowerCase(), statisticEditPacket.getValue());
                    } else if (statisticEditPacket.getAction() == StatisticEditPacket.Action.SET) {
                        CloudService.getCloudService().getManagerService().getStatsManager().setPlayerStat(cloudPlayer, statisticEditPacket.getKey().toLowerCase(), statisticEditPacket.getValue());
                    }

                    cloudPlayer.sendData(cloudPlayer.getServer(), DataUpdateType.UPDATE_SETTINGS, true);
                } else if (initPacket.getType() == PacketType.STATS_REQUEST_PLAYER_INFO) {
                    StatisticsPlayerRequestInfoPacket statisticsPlayerRequestInfoPacket = (StatisticsPlayerRequestInfoPacket)initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(statisticsPlayerRequestInfoPacket.getUniqueId());
                    if (cloudPlayer == null) return;

                    channelHandlerContext.channel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.STATS_PLAYER_INFO, CloudService.getCloudService().getManagerService().getStatsManager().createInfoPacket(cloudPlayer, statisticsPlayerRequestInfoPacket.getRankKeys())));
                } else if (initPacket.getType() == PacketType.PLAYER_SHOP_RANK) {
                    PlayerShopRankPacket playerShopRankPacket = (PlayerShopRankPacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(playerShopRankPacket.getUniqueId());
                    if (cloudPlayer != null && cloudPlayer.isOnline()) {
                        long end = TimeUnit.valueOf(playerShopRankPacket.getTimeUnit()).toMillis(playerShopRankPacket.getEnd());
                        if (end != 0L && cloudPlayer.getRank() == playerShopRankPacket.getRank()) {
                            end = cloudPlayer.getRankEnd() == 0L ? (end += System.currentTimeMillis()) : (end += cloudPlayer.getRankEnd());
                        } else if (end != 0L) {
                            end += System.currentTimeMillis();
                        }
                        cloudPlayer.setRank(playerShopRankPacket.getRank(), end);

                        cloudPlayer.sendMessage("rank-command-changed-target", (playerShopRankPacket.getRank().getRankColor() + playerShopRankPacket.getRank().getRankName()));
                        if (end != 0L) {
                            cloudPlayer.sendMessage("rank-command-changed-limited", GoAPI.getTimeManager().getRemainingTimeString(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), cloudPlayer.getLocale(), end));
                        } else {
                            cloudPlayer.sendMessage("rank-command-changed-lifetime");
                        }
                    }
                } else if (initPacket.getType() == PacketType.REPLAY_HISTORY_REQUEST) {
                    PacketReplayRecordHistoryRequest packet = initPacket.getPacket(PacketReplayRecordHistoryRequest.class);
                    CloudPlayer requester = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(packet.getRequester());
                    if (requester != null) {
                        CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(packet.getUniqueId());
                        if (cloudPlayer != null) {
                            if (cloudPlayer.isFriend(requester)) {
                                if (cloudPlayer.getSetting("replay-history-private", Integer.class) == 0) {
                                    List<PacketReplayEntries.PacketReplayEntry> entries = CloudService.getCloudService().getManagerService().getReplayHistoryManager().getEntries(initPacket.getPacket(PacketReplayRecordHistoryRequest.class).getUniqueId());
                                    channelHandlerContext.channel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.REPLAY_HISTORY_RESPONSE, new PacketReplayEntries(initPacket.getPacket(PacketReplayRecordHistoryRequest.class).getUniqueId(), new ArrayList<>(entries))));
                                } else {
                                    requester.sendMessage("replays-private", cloudPlayer.getRank().getRankColor()+cloudPlayer.getName());
                                }
                            } else {
                                requester.sendMessage("user-not-friend", requester.getLanguageMessage("replay-prefix"), cloudPlayer.getRank().getRankColor()+cloudPlayer.getName());
                            }
                        } else {
                            requester.sendMessage("user-not-exist", requester.getLanguageMessage("replay-prefix"));
                        }
                    }
                } else if (initPacket.getType() == PacketType.REPLAY_HISTORY_ADD) {
                    PacketReplayHistoryAdd add = initPacket.getPacket(PacketReplayHistoryAdd.class);
                    CloudService.getCloudService().getManagerService().getReplayHistoryManager().addEntry(add.getUniqueId(), add.getEntry());
                    //ReplayDataPacket replayDataPacket = new ReplayDataPacket(add.getEntry().getReplayUUID(), add.getEntry().getGameID(), add.getEntry().getGameType(), add.getEntry().getMapType(), add.getEntry().isAvailable());
                    //CloudServer.getReplayManager().updateToDatabase(replayDataPacket, true);
                } else if (initPacket.getType() == PacketType.REPLAY_SERVER_PACKET) {
                    ReplayServerPacket serverPacket = (ReplayServerPacket) initPacket.getValue();
                    CloudService.getCloudService().getManagerService().getReplayManager().updateToDatabase(serverPacket.getDataPacket(), serverPacket.isInsert());
                    CloudService.getCloudService().getTerminal().writeMessage("Trying to save the Replay " + serverPacket.getDataPacket().getGameID() + " (Insert: " + serverPacket.isInsert() + ")");
                } else if (initPacket.getType() == PacketType.REPLAY_HISTORY_RESPONSE) {
                    CloudService.getCloudService().getTerminal().writeMessage("An Server tried to response an Replay History?");
                } else if (initPacket.getKey().equals(PacketType.SERVER_PACKET.getKey())) {
                    ServerPacket serverPacket = (ServerPacket) initPacket.getValue();
                    if (serverPacket.isManualAdd()) {
                        CloudService.getCloudService().getManagerService().getServerManager().createServerManually(channelHandlerContext.channel(), serverPacket);
                    } else {
                        CloudService.getCloudService().getManagerService().getServerManager().changeServerState(serverPacket, channelHandlerContext.channel());
                    }
                } else if (initPacket.getKey().equals(PacketType.CLOUD_START_CALLBACK.getKey())) {
                    CloudServerCallBack cloudServerCallBack = (CloudServerCallBack) initPacket.getValue();
                    CloudService.getCloudService().getManagerService().getServerManager().receiveStartUp(cloudServerCallBack.getPort(), channelHandlerContext.channel());
                } else if (initPacket.getKey().equals(PacketType.SLAVE_INIT.getKey())) {
                    SlaveInitPacket slaveInitPacket = (SlaveInitPacket) initPacket.getValue();
                    CloudService.getCloudService().getManagerService().getSlaveManager().registerSlave(slaveInitPacket, channelHandlerContext.channel());
                } else if (initPacket.getKey().equals(PacketType.SLAVE_ALIVE_PACKET.getKey())) {
                    SlaveAlivePacket slaveAlivePacket = (SlaveAlivePacket) initPacket.getValue();
                    Slave getSlave = CloudService.getCloudService().getManagerService().getSlaveManager().getSlaveByName(slaveAlivePacket.getName());
                    if (getSlave != null) {
                        getSlave.setTimeout(System.currentTimeMillis() + 10000);
                        getSlave.setCpuUsage(slaveAlivePacket.getCpuUsage());
                        getSlave.setFreeMemory(slaveAlivePacket.getFreeMemory());
                        getSlave.setUsedMemory(slaveAlivePacket.getUsedMemory());
                        getSlave.setTotalMemory(slaveAlivePacket.getTotalMemory());
                        getSlave.setAverageCPU(slaveAlivePacket.getCpuAverage());
                    } else {
                        channelHandlerContext.channel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.ERROR_PACKET, new ErrorPacket("No Node found for this name!")));

                        //WrapperInitPacket wrapperInitPacket = new WrapperInitPacket("localhost", wrapperAlivePacket.getName(), wrapperAlivePacket.getTotalMemory());
                        //CloudServer.getNodeManager().registerUpdateWrapper(wrapperInitPacket, wrapperAlivePacket, channelHandlerContext.channel());
                    }
                } else if (initPacket.getKey().equals(PacketType.SERVER_ALIVE.getKey())) {
                    ServerAlive serverAlive = (ServerAlive) initPacket.getValue();
                    CloudService.getCloudService().getManagerService().getServerManager().receiveTimeOut(serverAlive.getName());
                } else if (initPacket.getKey().equals(PacketType.SLAVE_STOP.getKey())) {
                    SlaveStopPacket slaveStopPacket = (SlaveStopPacket) initPacket.getValue();
                    Slave slave = CloudService.getCloudService().getManagerService().getSlaveManager().getSlaveByName(slaveStopPacket.getName());
                    if (slave != null) {
                        slave.remove();
                        CloudService.getCloudService().getManagerService().getSlaveManager().removeNodeByName(slave.getName());
                    }
                } else if (initPacket.getKey().equals(PacketType.SPIGOT_SERVER_STOP.getKey())) {
                    SpigotServerStop spigotServerStop = (SpigotServerStop) initPacket.getValue();
                    Server server = CloudService.getCloudService().getManagerService().getServerManager().getServerByName(spigotServerStop.getName());
                    if (server != null) {
                        server.remove();
                    }
                } else if (initPacket.getKey().equals(PacketType.COMMAND_PACKET.getKey())) {
                    CommandPacket commandPacket = (CommandPacket) initPacket.getValue();
                    CloudService.getCloudService().getManagerService().getCommandResolver().resolveCommand(commandPacket, channelHandlerContext.channel(), false);
                } else if (initPacket.getKey().equals(PacketType.SERVER_CONNECT.getKey())) {
                    ConnectPacket connectPacket = (ConnectPacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(connectPacket.getName());
                    if (cloudPlayer.isOnline()) {
                        cloudPlayer.connectToServer(connectPacket.getServer());
                    }
                } else if (initPacket.getKey().equals(PacketType.ALERT_PACKET.getKey())) {
                    CloudService.getCloudService().getManagerService().getProxyManager().broadcastPacket(initPacket);
                } else if (initPacket.getKey().equals(PacketType.UPDATE_PLAYER_STATE.getKey())) {
                    UpdatePlayerStatePacket updatePlayerStatePacket = (UpdatePlayerStatePacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(updatePlayerStatePacket.getUniqueId());
                    if (cloudPlayer != null) {
                        cloudPlayer.setPlayerState(updatePlayerStatePacket.getPlayerState());
                    }
                } else if (initPacket.getKey().equals(PacketType.UPDATE_PLAYER_SETTINGS.getKey())) {
                    SettingsPacket settingsPacket = (SettingsPacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(settingsPacket.getUniqueId());
                    if (cloudPlayer != null) {
                        cloudPlayer.getSettingsPacket().setSettings(settingsPacket.getSettings());
                        cloudPlayer.updateToDatabase(false);
                        cloudPlayer.sendData(cloudPlayer.getServer(), DataUpdateType.UPDATE_SETTINGS, true);
                    }
                } else if (initPacket.getKey().equals(PacketType.PLAYER_ADD_STAT_PACKET.getKey())) {
                    PlayerAddStatPacket playerAddStatPacket = (PlayerAddStatPacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(playerAddStatPacket.getUniqueId());
                    if (cloudPlayer != null)
                        CloudService.getCloudService().getManagerService().getStatsManager().addPlayerStat(cloudPlayer, playerAddStatPacket.getKey().toLowerCase(), playerAddStatPacket.getValue());
                } else if (initPacket.getKey().equals(PacketType.EDIT_XP_PACKET.getKey())) {
                    EditXPPacket editXPPacket = (EditXPPacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(editXPPacket.getUniqueId());
                    if (editXPPacket.isRemove()) {
                        if (editXPPacket.getXp() != -1) {
                            cloudPlayer.getLevel().removeXp(editXPPacket.getXp());
                            cloudPlayer.sendData(cloudPlayer.getServer(), DataUpdateType.UPDATE_LEVEL, true);
                            cloudPlayer.updateToDatabase(false);
                        }
                    } else {
                        if (editXPPacket.getXp() != -1) {
                            cloudPlayer.getLevel().addXp(editXPPacket.getXp());
                            cloudPlayer.sendData(cloudPlayer.getServer(), DataUpdateType.UPDATE_LEVEL, true);
                            cloudPlayer.updateToDatabase(false);
                        }
                    }
                } else if (initPacket.getKey().equals(PacketType.EDIT_COIN_PACKET.getKey())) {
                    EditCoinPacket editCoinPacket = (EditCoinPacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(editCoinPacket.getUniqueId());
                    if (editCoinPacket.isRemove()) {
                        if (editCoinPacket.getCoins() != -1) {
                            cloudPlayer.removeCoins(editCoinPacket.getCoins());
                        }
                    } else {
                        if (editCoinPacket.getCoins() != -1) {
                            cloudPlayer.addCoins(editCoinPacket.getCoins());
                        }
                    }
                } else if (initPacket.getKey().equals(PacketType.LOBBY_LOC_PACKET.getKey())) {
                    LobbyLocPacket lobbyLocPacket = (LobbyLocPacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(lobbyLocPacket.getUniqueId());
                    if (cloudPlayer == null) {
                        if (CloudService.getCloudService().isDevMode()) System.out.println("Player is null (SlaveHandler:232) [" + lobbyLocPacket.getUniqueId() + "]");
                        return;
                    }
                    cloudPlayer.setLocX(lobbyLocPacket.getX());
                    cloudPlayer.setLocY(lobbyLocPacket.getY());
                    cloudPlayer.setLocZ(lobbyLocPacket.getZ());
                    cloudPlayer.setYaw(lobbyLocPacket.getYaw());
                    cloudPlayer.setPitch(lobbyLocPacket.getPitch());
                    cloudPlayer.updateToDatabase(false);
                    if (cloudPlayer.getServer() != null && (cloudPlayer.getServer().getName().toLowerCase().startsWith("lobby") || cloudPlayer.getServer().getName().toLowerCase().startsWith("silent"))) {
                        cloudPlayer.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.LOBBY_LOC_PACKET, new LobbyLocPacket(cloudPlayer.getUniqueId(), lobbyLocPacket.getX(), lobbyLocPacket.getY(), lobbyLocPacket.getZ(), lobbyLocPacket.getYaw(), lobbyLocPacket.getPitch())));
                    }
                } else if (initPacket.getKey().equals(PacketType.GET_PLAYER_STATS.getKey())) {
                    GetPlayerStatsPacket getPlayerStatsPacket = (GetPlayerStatsPacket) initPacket.getValue();
                    CloudPlayer statsPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(getPlayerStatsPacket.getStatsPlayer());
                    CloudPlayer forPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(getPlayerStatsPacket.getForPlayer());
                    channelHandlerContext.channel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.RECEIVE_PLAYER_STATS, new ReceivePlayerStatsPacket(forPlayer.getName(), statsPlayer.getName(), statsPlayer.getRank().getRankColor(), statsPlayer.getStats().getGlobalStats(), statsPlayer.getStats().getMonthlyStats(), statsPlayer.getStats().getDailyStats(), statsPlayer.getStats().getGlobalRanking(), statsPlayer.getStats().getMonthlyRanking(), statsPlayer.getStats().getDailyRanking())));
                } else if (initPacket.getKey().equals(PacketType.MESSAGE_PACKET.getKey())) {
                    MessagePacket messagePacket = (MessagePacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(messagePacket.getReceiverUniqueId());
                    cloudPlayer.sendMessages(messagePacket.getMessages());
                } else if (initPacket.getKey().equals(PacketType.REMOVE_REPORT_PACKET.getKey())) {
                    RemoveReportPacket removeReportPacket = (RemoveReportPacket) initPacket.getValue();
                    CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(removeReportPacket.getName());
                    if (getPlayer == null) return;
                    CloudService.getCloudService().getManagerService().getReportManager().removeReportFrom(getPlayer);
                } else if (initPacket.getKey().equals(PacketType.SILENT_LOBBY_PACKET.getKey())) {
                    PlayerSilentLobbyPacket playerSilentLobbyPacket = (PlayerSilentLobbyPacket) initPacket.getValue();
                    CloudService.getCloudService().getManagerService().getLobbyManager().sendPlayerToSilentHub(playerSilentLobbyPacket.getPlayerName());
                } else if (initPacket.getKey().equals(PacketType.LOBBY_JOIN_PACKET.getKey())) {
                    LobbyJoinPacket lobbyJoinPacket = (LobbyJoinPacket) initPacket.getValue();
                    CloudService.getCloudService().getManagerService().getLobbyManager().sendToLobby(lobbyJoinPacket.getName());
                } else if (initPacket.getKey().equals(PacketType.IP_BAN_PACKET.getKey())) {
                    BanIpPacket banIpPacket = (BanIpPacket) initPacket.getValue();
                    if (!CloudService.getCloudService().getManagerService().getProxyManager().getBannedIPs().contains(banIpPacket.getIp())) {
                        CloudService.getCloudService().getManagerService().getProxyManager().getBannedIPs().add(banIpPacket.getIp());
                        CloudService.getCloudService().getManagerService().getProxyManager().broadcastPacket(CloudService.getCloudService().getManagerService().getProxyManager().getIpBanPacket(banIpPacket.getIp(), true));
                    }
                } else if (initPacket.getKey().equals(PacketType.ADD_PLAYER_DATABASE.getKey())) {
                    AddPlayerDatabasePacket addPlayerDatabasePacket = (AddPlayerDatabasePacket) initPacket.getValue();
                    CloudService.getCloudService().getManagerService().getDatabaseManager().savePlayerData(addPlayerDatabasePacket.getUniqueId(), addPlayerDatabasePacket.getKey(), addPlayerDatabasePacket.getValue());
                } else if (initPacket.getKey().equals(PacketType.GET_PLAYER_DATABASE_VALUE.getKey())) {
                    GetPlayerDatabaseValue getPlayerDatabaseValue = (GetPlayerDatabaseValue) initPacket.getValue();
                    CloudService.getCloudService().getManagerService().getDatabaseManager().getDataFromPlayer(getPlayerDatabaseValue.getUniqueId(), getPlayerDatabaseValue.getKey(), getPlayerDatabaseValue.getCallbackID(), channelHandlerContext.channel());
                } else if (initPacket.getKey().equals(PacketType.GET_STATS_TOP.getKey())) {
                    GetStatsTopPacket getStatsTopPacket = (GetStatsTopPacket) initPacket.getValue();
                    Game game = CloudService.getCloudService().getManagerService().getGameManager().getGameByName(getStatsTopPacket.getPrefix());
                    List<String[]> topData = new ArrayList<>();
                    if (game != null) {
                        if (getStatsTopPacket.getStatsType().equals(GetStatsTopPacket.StatsType.DAILY)) {
                            int need = getStatsTopPacket.getSize();
                            if (CloudService.getCloudService().getManagerService().getStatsManager().getDailyTop().containsKey(game.getModiName())) {
                                for (CloudPlayer topPlayer : CloudService.getCloudService().getManagerService().getStatsManager().getDailyTop().get(game.getModiName())) {
                                    need--;
                                    String signature = topPlayer.getSkinSignature();
                                    String value = topPlayer.getSkinValue();
                                    if (signature == null || value == null) {
                                        signature = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinSignature();
                                        value = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinValue();
                                    }
                                    topData.add(new String[]{topPlayer.getName(), signature, value, topPlayer.getRank().getRankColor(), String.valueOf(topPlayer.getStats().getDailyStatsValue(game.getPrefix() + "_" + game.getRankingField()))});
                                    if (need == 0) break;
                                }
                            }
                        } else if (getStatsTopPacket.getStatsType().equals(GetStatsTopPacket.StatsType.MONTHLY)) {
                            int need = getStatsTopPacket.getSize();
                            if (CloudService.getCloudService().getManagerService().getStatsManager().getMonthlyTop().containsKey(game.getModiName())) {
                                for (CloudPlayer topPlayer : CloudService.getCloudService().getManagerService().getStatsManager().getMonthlyTop().get(game.getModiName())) {
                                    need--;
                                    String signature = topPlayer.getSkinSignature();
                                    String value = topPlayer.getSkinValue();
                                    if (signature == null || value == null) {
                                        signature = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinSignature();
                                        value = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinValue();
                                    }
                                    topData.add(new String[]{topPlayer.getName(), signature, value, topPlayer.getRank().getRankColor(), String.valueOf(topPlayer.getStats().getMonthlyStatsValue(game.getPrefix() + "_" + game.getRankingField()))});
                                    if (need == 0) break;
                                }
                            }
                        } else if (getStatsTopPacket.getStatsType().equals(GetStatsTopPacket.StatsType.GLOBAL)) {
                            int need = getStatsTopPacket.getSize();
                            if (CloudService.getCloudService().getManagerService().getStatsManager().getGlobalTop().containsKey(game.getModiName())) {
                                for (CloudPlayer topPlayer : CloudService.getCloudService().getManagerService().getStatsManager().getGlobalTop().get(game.getModiName())) {
                                    need--;
                                    String signature = topPlayer.getSkinSignature();
                                    String value = topPlayer.getSkinValue();
                                    if (signature == null || value == null) {
                                        signature = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinSignature();
                                        value = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinValue();
                                    }
                                    topData.add(new String[]{topPlayer.getName(), signature, value, topPlayer.getRank().getRankColor(), String.valueOf(topPlayer.getStats().getGlobalStatsValue(game.getPrefix() + "_" + game.getRankingField()))});
                                    if (need == 0) break;
                                }
                            }
                        }
                    }
                    TopStatsPacket topStatsPacket = new TopStatsPacket(getStatsTopPacket.getCallBackID(), topData);
                    channelHandlerContext.channel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.TOP_STATS_PACKET, topStatsPacket));
                } else if (initPacket.getKey().equals(PacketType.GET_PLAYER_COINS.getKey())) {
                    GetCoinsPacket getCoinsPacket = (GetCoinsPacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(getCoinsPacket.getUniqueId());
                    if (cloudPlayer == null) return;
                    CoinsPacket coinsPacket = new CoinsPacket(getCoinsPacket.getCallbackID(), cloudPlayer.getCoins());
                    channelHandlerContext.channel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.COINS_PACKET, coinsPacket));
                } else if (initPacket.getKey().equals(PacketType.PLAYER_REPORTED.getKey())) {
                    PlayerReportedPacket playerReportedPacket = (PlayerReportedPacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(playerReportedPacket.getPlayerName());
                    if (cloudPlayer == null) return;
                } else if (initPacket.getKey().equals(PacketType.WATCH_REPLAY.getKey())) {
                    WatchReplayPacket replayGameIDPacket = (WatchReplayPacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(replayGameIDPacket.getUniqueId());

                    PacketReplayEntries.PacketReplayEntry entry = CloudService.getCloudService().getManagerService().getReplayHistoryManager().getReplay(replayGameIDPacket.getGameID());
                    if (entry != null) {
                        cloudPlayer.watchReplay(replayGameIDPacket.getGameID());
                    } else {
                        cloudPlayer.sendMessage("wrong-replay-id", replayGameIDPacket.getGameID());
                    }
                } else if (initPacket.getKey().equals(PacketType.REPLAY_GAME_ID.getKey())) {
                    PacketReplaySetGameId replayGameIDPacket = (PacketReplaySetGameId) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(replayGameIDPacket.getPlayer());

                    cloudPlayer.setWatchingReplay(replayGameIDPacket.getGameID());
                } else if (initPacket.getKey().equals(PacketType.REQUIRE_REPLAY_GAMEID.getKey())) {
                    RequireReplayGameIDPacket replayGameIDPacket = (RequireReplayGameIDPacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(replayGameIDPacket.getUuid());
                    PacketReplayEntries.PacketReplayEntry entry = CloudService.getCloudService().getManagerService().getReplayHistoryManager().getReplay(cloudPlayer.getWatchingReplay());
                    if (entry != null) {
                        channelHandlerContext.channel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.VIEW_REPLAY, new ViewReplayPacket(cloudPlayer.getUniqueId(), entry)));
                    } else {
                        CloudService.getCloudService().getManagerService().getLobbyManager().sendToLobby(cloudPlayer.getName());
                        cloudPlayer.sendMessage("wrong-replay-id", cloudPlayer.getWatchingReplay());
                    }
                } else if (initPacket.getKey().equals(PacketType.MAPEDIT_REQUEST_DATA.getKey())) {
                    MapEditRequestPacket mapEditRequestPacket = (MapEditRequestPacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(mapEditRequestPacket.getUuid());
                    channelHandlerContext.channel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.MAPEDIT_INFO, new MapEditInfoPacket(cloudPlayer.getUniqueId(), cloudPlayer.getMapEditInfo().getTemplateName(), cloudPlayer.getMapEditInfo().getMapName())));
                } else if (initPacket.getKey().equals(PacketType.REPLAY_FAVORITES_EDIT.getKey())) {
                    PacketEditReplay packet = (PacketEditReplay) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(packet.getUniqueId());
                    if (cloudPlayer != null) {
                        PacketReplayEntries.PacketReplayEntry entry = CloudService.getCloudService().getManagerService().getReplayHistoryManager().getReplay(cloudPlayer.getUniqueId(), packet.getGameId(), ReplayHistoryManager.ReplayType.RECENT);
                        if (entry != null) {
                            if (packet.getEditType() == PacketEditReplay.EditType.ADD_FAVORITE) {
                                if (!entry.isFavorite()) {
                                    if (CloudService.getCloudService().getManagerService().getReplayHistoryManager().getSavedEntries(packet.getUniqueId()).size() < (cloudPlayer.getRank().isHigherEqualsLevel(Rank.PREMIUM) ? 27 : 9)) {
                                        CloudService.getCloudService().getManagerService().getReplayHistoryManager().updateEntry(cloudPlayer.getUniqueId(), entry, true);
                                        cloudPlayer.sendMessage("replay-saved", GoAPI.getTimeManager().getRemainingTimeString1(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), cloudPlayer.getLocale(), (entry.getTimestamp()+TimeUnit.DAYS.toMillis(30))));
                                    } else {
                                        cloudPlayer.sendMessage("replay-saved-limit");
                                    }
                                } else {
                                    cloudPlayer.sendMessage("replay-saved-already-added");
                                }
                            } else if (packet.getEditType() == PacketEditReplay.EditType.REMOVE_FAVORITE) {
                                if (entry.isFavorite()) {
                                    CloudService.getCloudService().getManagerService().getReplayHistoryManager().updateEntry(cloudPlayer.getUniqueId(), entry, false);
                                    cloudPlayer.sendMessage("replay-not-longer-saved", GoAPI.getTimeManager().getRemainingTimeString1(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), cloudPlayer.getLocale(), (entry.getTimestamp()+TimeUnit.DAYS.toMillis(7))));
                                } else {
                                    cloudPlayer.sendMessage("replay-saved-not-favorite");
                                }
                            } else if (packet.getEditType() == PacketEditReplay.EditType.DELETE_REPLAY) {
                                CloudService.getCloudService().getManagerService().getReplayHistoryManager().removeEntry(cloudPlayer.getUniqueId(), entry);
                                cloudPlayer.sendMessage("replay-deleted", GoAPI.getTimeManager().getRemainingTimeString1(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), cloudPlayer.getLocale(), (entry.getTimestamp()+TimeUnit.DAYS.toMillis(7))));
                                cloudPlayer.sendData(cloudPlayer.getServer(), DataUpdateType.UPDATE_REPLAYS, true);
                            }
                        } else {
                            cloudPlayer.sendMessage("wrong-replay-id");
                        }
                    }
                } else if (initPacket.getKey().equals(PacketType.PUNISH_PACKET.getKey())) {
                    PunishPacket punishPacket = (PunishPacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(punishPacket.getUuid());

                    if (punishPacket.getBanReason() != null) {
                        CloudService.getCloudService().getManagerService().getAbuseManager().punish(cloudPlayer, null, punishPacket.getBanReason().name(), (cloudPlayer.getServer().getGameID()!=null?"R:"+cloudPlayer.getServer().getGameID():""), CloudService.getCloudService().getManagerService().getAbuseManager().getLength(punishPacket.getBanReason().getBanPoints()), false, false);
                    } else if (punishPacket.getMuteReason() != null) {
                        String id = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().createChatLog(UUID.fromString("336991c8-1199-457c-80bb-649042a489f5"), cloudPlayer.getUniqueId(), cloudPlayer.getServer().getServerID(), false);
                        CloudService.getCloudService().getManagerService().getAbuseManager().punish(cloudPlayer, null, punishPacket.getMuteReason().name(), "C:"+id, CloudService.getCloudService().getManagerService().getAbuseManager().getLength(punishPacket.getMuteReason().getBanPoints()), true, false);
                    } else {
                        CloudService.getCloudService().getTerminal().write("§6GoNet §7get an error from the Punish Packet!");
                    }
                } else if (initPacket.getType() == PacketType.START_PRIVATE_SERVER) {
                    StartPrivateServerPacket privateServerPacket = (StartPrivateServerPacket) initPacket.getValue();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(privateServerPacket.getUuid());
                    TemplateData template = CloudService.getCloudService().getManagerService().getServerManager().getTemplateByName(privateServerPacket.getTemplate());

                    /*if (cloudPlayer.getRank().isHigherEqualsLevel(Rank.CADMIN)) {
                        if (cloudPlayer.getLanguage() == Language.LanguageType.GERMAN) {
                            cloudPlayer.sendMessage(CloudServer.getPrefix() + "§cDu benötigst mindestens den §4Admin §cRang um einen Privaten Server erstellen zu können!");
                        } else {
                            cloudPlayer.sendMessage(CloudServer.getPrefix() + "§cYou need at least the §4Admin §cRank to create a private server");
                        }

                        return;
                    }*/

                    if (template != null) {
                        CloudService.getCloudService().getManagerService().getServerManager().createPrivateServer(cloudPlayer, template);
                    } else {
                        cloudPlayer.sendMessage("privateservers-template-not-found");
                    }
                } else if (initPacket.getType() == PacketType.PRIVATE_OWNER) {
                    System.out.println("An Server tried to response an Private Owner?");
                } else if (initPacket.getType() == PacketType.CLAN_REQUEST) {
                    ClanRequestPacket clanRequestPacket = (ClanRequestPacket) initPacket.getValue();
                    CloudPlayer player = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(clanRequestPacket.getUuid());

                    if (player.getClan() != null) {
                        String members = "";

                        List<CloudPlayer> members2;

                        members2 = player.getClan().getMembers().keySet().stream().map(e -> CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayer(e)).filter(e -> e.isPresent()).map(e -> e.get()).filter(e -> e.isOnline()).collect(Collectors.toList());
                        Collections.sort(members2, (o1, o2) -> Long.compare(o2.getClan().getMembers().get(o2.getUniqueId()).getAccessLevel(), o1.getClan().getMembers().get(o1.getUniqueId()).getAccessLevel()));

                        for (CloudPlayer cp : members2) {
                            String a = cp.getName() + ":" + cp.getUniqueId() + ":" + cp.getRank().name().toUpperCase() + ":" + cp.getSkinSignature() + ":" + cp.getSkinValue() + ":" + cp.isOnline() + ":" + player.getClan().getMembers().get(cp.getUniqueId()).name().toUpperCase() + ";";
                            members += a;
                        }

                        String id = player.getClan().getId(), name = player.getClan().getName(), tag = player.getClan().getTag();
                        UUID uniqueId = player.getClan().getUniqueId();

                        PacketHolder packetHolder = GoAPI.getPacketHelper().preparePacket(PacketType.CLAN_INFORMATION, new ClanInformationPacket(player.getUniqueId(), id, uniqueId, name, tag, members));

                        if (packetHolder != null) {
                            channelHandlerContext.channel().writeAndFlush(packetHolder);
                        } else {
                            System.out.println("packetHolder is null (NH:352)");
                        }
                    } else {
                        PacketHolder packetHolder = GoAPI.getPacketHelper().preparePacket(PacketType.CLAN_INFORMATION, new ClanInformationPacket(player.getUniqueId()));
                        channelHandlerContext.channel().writeAndFlush(packetHolder);
                    }
                }

                CloudService.getCloudService().getManagerService().getEventManager().callEvent(new ServerSendPacketEvent(channelHandlerContext.channel(), o));
            } catch (Exception exc) {
                exc.printStackTrace();
            } finally {
                ReferenceCountUtil.release(o);
            }
        }).start();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
    }
}