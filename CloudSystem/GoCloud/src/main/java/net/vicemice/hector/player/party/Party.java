package net.vicemice.hector.player.party;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.party.PartyMemberPacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.network.modules.api.event.player.party.*;
import net.vicemice.hector.utils.players.utils.PlayerState;
import org.apache.commons.lang3.RandomStringUtils;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/*
 * Class created at 00:50 - 07.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class Party {

    @Getter
    private long timeout;
    @Getter
    private final String id;
    @Getter
    private Integer max;
    @Getter
    private CloudPlayer leader;
    @Getter
    @Setter
    private boolean publicParty = false;
    @Getter
    private final List<CloudPlayer> members = new CopyOnWriteArrayList<>();
    @Getter
    private final List<CloudPlayer> invites = new CopyOnWriteArrayList<>();

    public Party(CloudPlayer leader) {
        this.id = RandomStringUtils.randomAlphanumeric(20);
        this.max = 5;
        this.timeout = (TimeUnit.MINUTES.toMillis(5)+System.currentTimeMillis());

        if(leader.getRank().isHigherEqualsLevel(Rank.PREMIUM)) {
            this.max = 15;
        }
        if(leader.getRank().equalsLevel(Rank.VIP) && leader.getRank().isAdmin()) {
            this.max = 25;
        }

        this.leader = leader;
        this.members.add(leader);

        leader.sendMessage("party-created");
    }

    public void handleServerSwitch(Server server) {
        if (server.isCloudServer() && server.getServerTemplate().getName().equalsIgnoreCase("lobby") || server.isCloudServer() && server.getServerTemplate().getName().equalsIgnoreCase("silent")) {
            return;
        }
        List<UUID> partyMembers = this.getMembers().stream().map(CloudPlayer::getUniqueId).collect(Collectors.toList());
        server.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PARTY_MEMBER_PACKET, new PartyMemberPacket(partyMembers)));

        for (CloudPlayer cloudPlayer : this.members) {
            cloudPlayer.sendMessage("party-joined-server", "§e"+server.getName().toUpperCase().replace("-", ""));
        }

        int i = 1;
        for (CloudPlayer cloudPlayer : this.members) {
            if (cloudPlayer == this.leader) continue;
            if (cloudPlayer.equals(this.getLeader())) continue;
            if (cloudPlayer.isAfk() || cloudPlayer.getPlayerState() == PlayerState.INGAME) continue;
            cloudPlayer.connectToServer(server.getName());
            i++;
        }

        if (i < this.members.size()) {
            sendMessageToParty("party-joined-notall");
        }
    }

    public void acceptInvite(CloudPlayer target) {
        CloudService.getCloudService().getManagerService().getEventManager().callEvent(new PartyAcceptInviteEvent(this, target));
    }

    public void joinParty(CloudPlayer target) {
        CloudService.getCloudService().getManagerService().getEventManager().callEvent(new PartyJoinPartyEvent(this, target));
    }

    public void inviteMember(CloudPlayer target) {
        CloudService.getCloudService().getManagerService().getEventManager().callEvent(new PartyInviteEvent(this.leader, target));
    }

    public void promotePlayer(CloudPlayer target) {
        CloudService.getCloudService().getManagerService().getEventManager().callEvent(new PartyPromoteEvent(this.leader, target));
    }

    public void memberLeave(CloudPlayer target) {
        this.timeout = (TimeUnit.MINUTES.toMillis(5)+System.currentTimeMillis());
        CloudService.getCloudService().getManagerService().getEventManager().callEvent(new PartyLeaveEvent(this, target));
    }

    public void deleteParty() {
        CloudService.getCloudService().getManagerService().getEventManager().callEvent(new PartyDeleteEvent(this));
    }

    public void kickMember(CloudPlayer target) {
        CloudService.getCloudService().getManagerService().getEventManager().callEvent(new PartyKickEvent(this.leader, target));
    }

    public void sendMessageToParty(String string) {
        for (CloudPlayer cloudPlayer : this.members) {
            cloudPlayer.sendMessage(string);
        }
    }

    public void sendMessageToParty(String string, Object... objects) {
        for (CloudPlayer cloudPlayer : this.members) {
            cloudPlayer.sendMessage(string, objects);
        }
    }

    public void setNewPartyLeader(CloudPlayer target) {
        this.leader = target;
        sendMessageToParty("party-new-leader", (this.getLeader().getRank().getRankColor()+this.getLeader().getName()));
    }

    public void setPublic() {
        if(isPublicParty()) {
            setPublicParty(false);
            sendMessageToParty("party-public", this.getLeader().getName());
        } else {
            setPublicParty(true);
            sendMessageToParty("party-private");
        }
    }
}