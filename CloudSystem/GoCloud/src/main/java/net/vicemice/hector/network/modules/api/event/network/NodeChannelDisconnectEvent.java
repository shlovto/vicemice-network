package net.vicemice.hector.network.modules.api.event.network;

import net.vicemice.hector.network.slave.Slave;
import net.vicemice.hector.utils.event.async.AsyncEvent;
import net.vicemice.hector.utils.event.async.AsyncPosterAdapter;

public class NodeChannelDisconnectEvent extends AsyncEvent<NodeChannelDisconnectEvent> {
    private Slave slave;

    public NodeChannelDisconnectEvent(Slave slave) {
        super(new AsyncPosterAdapter<>());
        this.slave = slave;
    }

    public Slave getSlave() {
        return this.slave;
    }
}

