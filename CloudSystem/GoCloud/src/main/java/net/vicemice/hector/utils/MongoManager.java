package net.vicemice.hector.utils;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import lombok.Getter;
import net.vicemice.hector.CloudService;
import org.bson.Document;
import java.util.ArrayList;
import java.util.List;

/*
 * Class created at 20:19 - 31.12.2019
 * Copyright (C) 2016-2019 Minetasia Solutions
 */
public class MongoManager {

    @Getter
    private MongoClient client;
    @Getter
    private MongoDatabase mongoDatabase, gamesDatabase, discordDatabase;

    public MongoManager() {
        String host = CloudService.getCloudService().getManagerService().getConfigManager().getMongoConfig().getHost();
        int port = Integer.parseInt(CloudService.getCloudService().getManagerService().getConfigManager().getMongoConfig().getPort());
        String user = CloudService.getCloudService().getManagerService().getConfigManager().getMongoConfig().getUsername();
        char[] password = CloudService.getCloudService().getManagerService().getConfigManager().getMongoConfig().getPassword().toCharArray();

        ServerAddress serverAddress = new ServerAddress(host, port);
        List<MongoCredential> credentials = new ArrayList<>();
        credentials.add(
                MongoCredential.createCredential(
                        user,
                        "admin",
                        password
                )
        );
        client = new MongoClient(serverAddress, credentials);
        mongoDatabase = client.getDatabase("networkService");
        gamesDatabase = client.getDatabase("gameService");
        discordDatabase = client.getDatabase("discordService");
    }

    public ArrayList<Document> getDocuments(String collection) {
        ArrayList<Document> docs = new ArrayList<>();

        for (Document document : mongoDatabase.getCollection(collection).find()) {
            docs.add(document);
        }

        return docs;
    }

    public ArrayList<Document> getDocuments(String database, String collection) {
        ArrayList<Document> docs = new ArrayList<>();

        for (Document document : client.getDatabase(database).getCollection(collection).find()) {
            docs.add(document);
        }

        return docs;
    }

    public ArrayList<Document> getDocumentsWithFilter(String collection, Document filter) {
        ArrayList<Document> docs = new ArrayList<>();

        for (Document document : mongoDatabase.getCollection(collection).find(filter)) {
            docs.add(document);
        }

        return docs;
    }

    public ArrayList<Document> getDocumentsWithSort(String collection, Document sort) {
        ArrayList<Document> docs = new ArrayList<>();

        FindIterable<Document> o = mongoDatabase.getCollection(collection).find().sort(sort);

        if (o != null) {
            for (Document document : o) {
                docs.add(document);
            }
        }

        return docs;
    }

    public ArrayList<Document> getDocumentsWithFilterAndSort(String collection, Document filter, Document sort) {
        ArrayList<Document> docs = new ArrayList<>();

        for (Document document : mongoDatabase.getCollection(collection).find(filter).sort(sort)) {
            docs.add(document);
        }

        return docs;
    }

    public ArrayList<Document> getDocumentsWithFilterAndSortAndLimit(String collection, Document filter, Document sort, Integer limit) {
        ArrayList<Document> docs = new ArrayList<>();

        for (Document document : mongoDatabase.getCollection(collection).find(filter).sort(sort).limit(limit)) {
            docs.add(document);
        }

        return docs;
    }

    public ArrayList<Document> getDocumentsWithLimit(String collection, Integer limit) {
        ArrayList<Document> docs = new ArrayList<>();

        for (Document document : mongoDatabase.getCollection(collection).find().limit(limit)) {
            docs.add(document);
        }

        return docs;
    }

    public Document getDocument(String collection, Document filter) {
        for (Document document : mongoDatabase.getCollection(collection).find(filter)) {
            return document;
        }

        return null;
    }

    public Document getDocument(String database, String collection, Document filter) {
        for (Document document : client.getDatabase(database).getCollection(collection).find(filter)) {
            return document;
        }

        return null;
    }
}