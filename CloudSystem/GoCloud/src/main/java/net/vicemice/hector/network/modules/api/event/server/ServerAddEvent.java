package net.vicemice.hector.network.modules.api.event.server;

import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.utils.event.async.AsyncEvent;
import net.vicemice.hector.utils.event.async.AsyncPosterAdapter;

public class ServerAddEvent extends AsyncEvent<ServerAddEvent> {
    private Server minecraftServer;

    public ServerAddEvent(Server minecraftServer) {
        super(new AsyncPosterAdapter<>());
        this.minecraftServer = minecraftServer;
    }

    public Server getMinecraftServer() {
        return this.minecraftServer;
    }
}

