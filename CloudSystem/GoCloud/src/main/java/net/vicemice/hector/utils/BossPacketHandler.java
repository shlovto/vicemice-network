package net.vicemice.hector.utils;

import io.netty.channel.Channel;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.manager.chatlog.ChatManager;
import net.vicemice.hector.network.proxy.Proxy;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.lobby.LobbyJoinPacket;
import net.vicemice.hector.packets.types.player.punish.BanIpPacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.packets.types.player.*;
import net.vicemice.hector.packets.types.proxy.ProxyPacket;
import net.vicemice.hector.packets.types.proxy.ProxyPlayerPacket;
import net.vicemice.hector.packets.types.proxy.ProxyServerPlayerCountPacket;
import net.vicemice.hector.packets.types.proxy.ProxyStopPacket;

public class BossPacketHandler {
    public void handlePacket(String key, Object object, Channel channel) {
        if (key.equalsIgnoreCase(PacketType.CHAT_PACKET.getKey())) {
            ChatPacket chatPacket = (ChatPacket) object;
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(chatPacket.getUniqueId());

            CloudService.getCloudService().getManagerService().getChatManager().addMessage(new ChatManager.Message(cloudPlayer.getUniqueId(), cloudPlayer.getServer().getServerID(), cloudPlayer.getIp(), chatPacket.getMessage(), chatPacket.getTime()));
        } else  if (key.equals(PacketType.MESSAGE_PACKET.getKey())) {
            MessagePacket messagePacket = (MessagePacket) object;
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(messagePacket.getReceiverUniqueId());
            if (cloudPlayer != null && cloudPlayer.getProxy() != null) {
                cloudPlayer.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.valueOf(key), messagePacket));
            }
        } else if (key.equals(PacketType.SERVER_CONNECT.getKey())) {
            ConnectPacket connectPacket = (ConnectPacket) object;
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(connectPacket.getName());
            if (cloudPlayer != null && cloudPlayer.getProxy() != null) {
                cloudPlayer.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.valueOf(key), connectPacket));
            }
        } else if (key.equals(PacketType.PROXY_INFO_PACKET.getKey())) {
            CloudService.getCloudService().getManagerService().getProxyManager().handlePacket((ProxyPacket) object, channel);
        } else if (key.equals(PacketType.LOGIN_PACKET.getKey())) {
            LoginPacket loginPacket = (LoginPacket) object;
            Proxy proxy = CloudService.getCloudService().getManagerService().getProxyManager().getProxyByName(loginPacket.getProxyName());
            CloudService.getCloudService().getManagerService().getPlayerManager().playerLogin(loginPacket.getUniqueId(), loginPacket.getServer(), loginPacket.getName(), loginPacket.getVersion(), loginPacket.getIp(), proxy, loginPacket.getSkinValue(), loginPacket.getSkinSignature());
        } else if (key.equals(PacketType.LOGOUT_PACKET.getKey())) {
            LogoutPacket logoutPacket = (LogoutPacket) object;
            CloudService.getCloudService().getManagerService().getPlayerManager().playerLogOut(logoutPacket.getUniqueId(), channel);
        } else if (key.equals(PacketType.COMMAND_PACKET.getKey())) {
            CommandPacket commandPacket = (CommandPacket) object;
            if (commandPacket == null) {
                System.err.println("Try to invoke a command without a CommandPacket!");
                return;
            }
            CloudService.getCloudService().getManagerService().getCommandResolver().resolveCommand(commandPacket, channel, true);
        } else if (key.equals(PacketType.PROXY_PLAYER_PACKET.getKey())) {
            ProxyPlayerPacket proxyPlayerPacket = (ProxyPlayerPacket) object;
            CloudService.getCloudService().getManagerService().getServerManager().receiveNewPlayerPacket(proxyPlayerPacket);
        } else if (key.equals(PacketType.PLAYER_SWITCH_SERVER.getKey())) {
            Server server;
            PlayerSwitchServerPacket playerSwitchServerPacket = (PlayerSwitchServerPacket) object;
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(playerSwitchServerPacket.getName());
            if (cloudPlayer != null && (server = CloudService.getCloudService().getManagerService().getServerManager().getServerByName(playerSwitchServerPacket.getServerName())) != null) {
                if (CloudService.getCloudService().isDevMode()) CloudService.getCloudService().getTerminal().write("§cSWITCHING CLOUD PLAYER SERVER"+(cloudPlayer.getServer()!=null?" FROM "+cloudPlayer.getServer().getName():" ")+" TO "+server.getName());
                cloudPlayer.switchServer(server);
            }
        } else if (key.equals(PacketType.LOBBY_JOIN_PACKET.getKey())) {
            LobbyJoinPacket lobbyJoinPacket = (LobbyJoinPacket) object;
            CloudService.getCloudService().getManagerService().getLobbyManager().sendToLobby(lobbyJoinPacket.getName(), channel);
        } else if (key.equals(PacketType.PROXY_STOP_PACKET.getKey())) {
            ProxyStopPacket proxyStopPacket = (ProxyStopPacket) object;
            CloudService.getCloudService().getManagerService().getProxyManager().offlineProxy(CloudService.getCloudService().getManagerService().getProxyManager().getProxyByName(proxyStopPacket.getName()));
            System.out.println("Proxy " + proxyStopPacket.getName() + " unregistered!");
        } else if (key.equals(PacketType.PROXY_SERVER_PLAYER_COUNT_PACKET.getKey())) {
            Server server;
            ProxyServerPlayerCountPacket proxyServerPlayerCountPacket = (ProxyServerPlayerCountPacket) object;
            Proxy proxy = CloudService.getCloudService().getManagerService().getProxyManager().getProxyByName(proxyServerPlayerCountPacket.getBungeeName());
            if (proxy != null && (server = CloudService.getCloudService().getManagerService().getServerManager().getServerByName(proxyServerPlayerCountPacket.getServerName())) != null) {
                server.getPlayers().put(proxy.getName(), proxyServerPlayerCountPacket.getSize());
                if (server.getSign() != null) {
                    server.getSign().updateLines();
                    server.getSign().sendLines();
                }
            }
        } else if (key.equals(PacketType.IP_BAN_PACKET.getKey())) {
            BanIpPacket banIpPacket = (BanIpPacket) object;
            if (!CloudService.getCloudService().getManagerService().getProxyManager().getBannedIPs().contains(banIpPacket.getIp())) {
                CloudService.getCloudService().getManagerService().getProxyManager().getBannedIPs().add(banIpPacket.getIp());
                CloudService.getCloudService().getManagerService().getProxyManager().broadcastPacket(CloudService.getCloudService().getManagerService().getProxyManager().getIpBanPacket(banIpPacket.getIp(), true));
            }
        }
    }
}