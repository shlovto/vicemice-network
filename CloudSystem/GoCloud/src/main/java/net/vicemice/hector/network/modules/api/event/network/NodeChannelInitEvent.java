package net.vicemice.hector.network.modules.api.event.network;

import io.netty.channel.Channel;
import net.vicemice.hector.network.slave.Slave;
import net.vicemice.hector.utils.event.Event;

public class NodeChannelInitEvent extends Event {
    private Slave slave;
    private Channel channel;

    public Slave getSlave() {
        return this.slave;
    }

    public Channel getChannel() {
        return this.channel;
    }

    public NodeChannelInitEvent(Slave slave, Channel channel) {
        this.slave = slave;
        this.channel = channel;
    }
}

