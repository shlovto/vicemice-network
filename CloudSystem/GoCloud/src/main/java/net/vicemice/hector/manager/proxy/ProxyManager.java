package net.vicemice.hector.manager.proxy;

import io.netty.channel.Channel;
import lombok.Data;
import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.network.gameserver.WhitelistedServer;
import net.vicemice.hector.network.proxy.Proxy;
import net.vicemice.hector.packets.types.FilterPacket;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.abuse.PlayerBanDataPacket;
import net.vicemice.hector.packets.types.player.abuse.PlayerMuteDataPacket;
import net.vicemice.hector.packets.types.player.punish.BanIpPacket;
import net.vicemice.hector.packets.types.proxy.players.ProxyBlacklistedPlayersPacket;
import net.vicemice.hector.packets.types.proxy.players.ProxyWhitelistedPlayersPacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.BossPacketHandler;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.proxy.*;
import org.bson.Document;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/*
 * Class created at 19:46 - 01.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class ProxyManager {
    @Getter
    private BossPacketHandler packetResolver;
    private List<Proxy> proxies = new CopyOnWriteArrayList<>();
    private int globalOnline = 0;
    private ProxySettingsPacket proxySettingsPacket;
    private boolean maintenanceActive = true;
    private List<String> bannedIPs = new CopyOnWriteArrayList<String>();
    private boolean userCountMode = false;

    public ProxyManager() {
        packetResolver = new BossPacketHandler();
        new Timer().scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                ProxyManager.this.checkTimeOut();
                int online = 0;
                if (ProxyManager.this.userCountMode) {
                    for (Proxy proxy : ProxyManager.this.proxies) {
                        online += proxy.getOnlinePlayers();
                    }
                } else {
                    for (UUID uuid : CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers()) {
                        CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uuid);
                        if (cloudPlayer == null || cloudPlayer.getServer() != null && cloudPlayer.getServer().isCloudServer() && cloudPlayer.getServer().getTemplateName().equalsIgnoreCase("verify"))
                            continue;
                        ++online;
                    }
                }
                PacketHolder countPacket = GoAPI.getPacketHelper().preparePacket(PacketType.ONLINE_COUNT_PACKET, new OnlineCountPacket(online));
                ProxyManager.online(ProxyManager.this, online);
                for (Proxy proxy : ProxyManager.this.proxies) {
                    proxy.sendPacket(countPacket);
                }
            }
        }, 2000, 2000);
    }

    public PacketHolder getIpBanPacket(String ip, boolean ban) {
        return GoAPI.getPacketHelper().preparePacket(PacketType.IP_BAN_PACKET, new BanIpPacket(ip, ban));
    }

    public void broadcastPacket(PacketHolder pkt) {
        this.proxies.forEach(e -> e.sendPacket(pkt));
    }

    public void loadSettings() {
        String motd = null;
        String motdMaintenance = null;
        String blackListed = null;
        int maxPlayers = 0;

        Document doc1 = CloudService.getCloudService().getManagerService().getMongoManager().getDocument("network_settings", new Document("variable", "motd_text"));
        Document doc2 = CloudService.getCloudService().getManagerService().getMongoManager().getDocument("network_settings", new Document("variable", "motd_maintenance_text"));
        Document doc3 = CloudService.getCloudService().getManagerService().getMongoManager().getDocument("network_settings", new Document("variable", "slots"));

        try {
            motd = doc1.getString("value");
            motdMaintenance = doc2.getString("value");
            maxPlayers = doc3.getInteger("value");
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.proxySettingsPacket = new ProxySettingsPacket(motd, maxPlayers, motdMaintenance, blackListed);
        for (Proxy next : this.getProxies()) {
            next.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_SETTINGS_PACKET, this.proxySettingsPacket));
        }
    }

    public void handlePacket(ProxyPacket proxyPacket, Channel channel) {
        Proxy getProxy = this.getProxyByName(proxyPacket.getName());
        if (getProxy != null) {
            getProxy.setNettyChannel(channel);
            getProxy.updateTimeOut();
            getProxy.setOnlinePlayers(proxyPacket.getPlayers());
            getProxy.updatePlayingPlayers(proxyPacket.getOnlineCloudPlayers());
        } else {
            Proxy proxy = new Proxy(proxyPacket.getName(), proxyPacket.getIpAddress(), proxyPacket.getPort(), channel);
            proxy.updatePlayingPlayers(proxyPacket.getOnlineCloudPlayers());
            this.proxies.add(proxy);
            PacketHolder maintenancePacket = GoAPI.getPacketHelper().preparePacket(PacketType.MAINTENANCE_PACKET, new MaintenancePacket(this.maintenanceActive, false));
            proxy.sendPacket(maintenancePacket);
            CloudService.getCloudService().getTerminal().writeMessage("§e§l"+proxy.getName()+" §3connected as proxy");

            for (WhitelistedServer server2 : CloudService.getCloudService().getManagerService().getWhitelistManager().getServers()) {
                proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_SERVER_ADD, new ProxyAddServerPacket(server2.getName(), server2.getHost(), server2.getPort())));
            }
            for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
                proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_SERVER_ADD, new ProxyAddServerPacket(server.getName(), server.getHost(), server.getPort())));
            }

            proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_WHITELISTED_PLAYERS, new ProxyWhitelistedPlayersPacket(CloudService.getCloudService().getManagerService().getWhitelistManager().getPlayers())));
            proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_BLACKLISTED_PLAYERS, new ProxyBlacklistedPlayersPacket(CloudService.getCloudService().getManagerService().getBlacklistManager().getPlayers())));

            proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.LOBBY_DATA_PACKET, CloudService.getCloudService().getManagerService().getLobbyManager().getLobbyDataPacket()));
            
            Iterator<UUID> banPlayerIterator = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithBan().iterator();
            while (banPlayerIterator.hasNext()) {
                CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(banPlayerIterator.next());

                if (cloudPlayer.getAbuses().getBan() == null) {
                    CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithBan().remove(cloudPlayer.getUniqueId());
                    continue;
                }

                proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_BAN_DATA, new PlayerBanDataPacket(cloudPlayer.getUniqueId(), new net.vicemice.hector.packets.Abuses(new net.vicemice.hector.packets.Abuses.Abuse(cloudPlayer.getUniqueId(), cloudPlayer.getAbuses().getBan().getReason(), cloudPlayer.getAbuses().getBan().getLength())))));
            }

            Iterator<UUID> mutePlayerIterator = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithMute().iterator();
            while (mutePlayerIterator.hasNext()) {
                CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(mutePlayerIterator.next());

                if (cloudPlayer.getAbuses().getMute() == null) {
                    CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithMute().remove(cloudPlayer.getUniqueId());
                    continue;
                }

                proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_MUTE_DATA, new PlayerMuteDataPacket(cloudPlayer.getUniqueId(), new net.vicemice.hector.packets.Abuses(new net.vicemice.hector.packets.Abuses.Abuse(cloudPlayer.getUniqueId(), cloudPlayer.getAbuses().getMute().getReason(), cloudPlayer.getAbuses().getMute().getLength())))));
            }

            PacketHolder filterPacket = GoAPI.getPacketHelper().preparePacket(PacketType.FILTER_PACKET, new FilterPacket(CloudService.getCloudService().getManagerService().getFilterManager().getEntriesWord()));
            proxy.sendPacket(filterPacket);
            ArrayList<String[]> rankData = new ArrayList<String[]>();
            for (UUID uuid : CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithRank()) {
                CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uuid);
                rankData.add(new String[]{String.valueOf(uuid), cloudPlayer.getRank().name().toUpperCase()});
            }
            PacketHolder rankPacket = GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_RANK_PACKET, new ProxyRankPacket(rankData));
            proxy.sendPacket(rankPacket);
            proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_SETTINGS_PACKET, this.proxySettingsPacket));
            for (String ip : this.bannedIPs) {
                proxy.sendPacket(this.getIpBanPacket(ip, true));
            }
        }
    }

    public void checkTimeOut() {
    }

    public void offlineProxy(Proxy proxy) {
        this.proxies.remove(proxy);
        CloudService.getCloudService().getManagerService().getServerManager().handlePlayerCountOfflineProxy(proxy);
        for (UUID uuid : proxy.getCloudPlayers()) {
            CloudService.getCloudService().getManagerService().getPlayerManager().playerLogOut(uuid, null);
        }
    }

    /**
     *
     * @param name defines the proxy name
     * @return returns an valid or invalid proxy
     */
    public Proxy getProxyByName(String name) {
        for (Proxy proxy : this.proxies) {
            if (!proxy.getName().equalsIgnoreCase(name)) continue;
            return proxy;
        }
        return null;

    }

    /**
     *
     * @param proxyManager defines the proxy manager
     * @param n defines the number of online players
     */
    static void online(ProxyManager proxyManager, int n) {
        proxyManager.globalOnline = n;
    }
}