package net.vicemice.hector.network.stats.sign;

import net.vicemice.hector.utils.slave.TemplateData;

import java.util.List;

public class StatSign {
    private String gameMode;
    private String location;
    private String mode;
    private int rank;
    private List<TemplateData> templates;

    public StatSign(String gameMode, String location, int rank, String mode, List<TemplateData> serverTemplates) {
        this.gameMode = gameMode;
        this.location = location;
        this.rank = rank;
        this.mode = mode;
        this.templates = serverTemplates;
    }

    public String getGameMode() {
        return this.gameMode;
    }

    public String getLocation() {
        return this.location;
    }

    public String getMode() {
        return this.mode;
    }

    public int getRank() {
        return this.rank;
    }

    public List<TemplateData> getTemplates() {
        return this.templates;
    }
}

