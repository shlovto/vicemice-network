package net.vicemice.hector.player.utils;

import lombok.Data;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.utils.SaveEntry;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.UUID;

/*
 * Class created at 20:57 - 28.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class Filter implements SaveEntry {

    private String word;
    private Long created;
    private UUID creator;

    public Filter(String word, Long created, UUID creator) {
        this.word = word;
        this.created = created;
        this.creator = creator;
    }

    public void updateToDatabase(boolean insert) {
        CloudService.getCloudService().getDatabaseQueue().addToQueue(this, insert);
    }

    @Override
    public void save(boolean insert) {
        try {
            if (insert) {
                Document document = new Document("word", word);
                document.append("created", created);
                document.append("creator", creator);
                CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("filter").insertOne(document);

                return;
            }

            Document document = new Document("created", created);
            document.append("creator", creator);

            Bson filter = new Document("word", word);
            CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("filter").updateOne(filter, new Document("$set", document));
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void delete() {
        try {
            Bson filter = new Document("word", word);
            CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("filter").deleteOne(filter);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }
}