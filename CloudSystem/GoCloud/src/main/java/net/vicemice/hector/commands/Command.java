package net.vicemice.hector.commands;

import lombok.Getter;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.Rank;

/*
 * Class created at 10:16 - 28.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public abstract class Command {

    @Getter
    private Rank permission;
    @Getter
    private Area area;

    public Command() {
        this.permission = Rank.MEMBER;
        this.area = Area.EVERY;
    }

    public Command(Rank permission) {
        this.permission = permission;
        this.area = Area.EVERY;
    }

    public Command(Rank permission, Area area) {
        this.permission = permission;
        this.area = area;
    }

    public Command(Area area) {
        this.permission = Rank.MEMBER;
        this.area = area;
    }

    public void executeCommand(String[] args, CloudPlayer sender) {
        String arg = args.length >= 1 ? args[0] : "overview";

        if (checkPermission(sender)) {
            this.execute(args, sender);
        } else {
            sender.sendMessage("command-no-permission");
        }
    }

    public abstract void execute(String[] var1, CloudPlayer var2);

    private boolean checkPermission(CloudPlayer cs) {
        //DEBUG INFO IF HIGHER: System.out.println("Is '"+GoAPI.getRankAPI().getPlayerRankData(((Player)cs).getUniqueId()).getRankName()+"' higher as '"+rank.getRankName()+"'? -> " + Rank.valueOf(GoAPI.getRankAPI().getPlayerRankData(((Player)cs).getUniqueId()).getRankName().toUpperCase()).isHigherEqualsLevel(rank));
        if (this.area != Area.EVERY) {
            boolean o = false;
            if (cs.getRank().isAdmin()) {
                o = true;
            }
            if (this.area == Area.ADMINISTRATING) {
                if (cs.getRank().isAdmin()) {
                    o = true;
                }
            } else if (this.area == Area.MODERATING) {
                if (cs.getRank().isJrModeration()) {
                    o = true;
                }
            } else {
                if (cs.getRank().equalsLevel(this.permission)) {
                    o = true;
                }
            }
            return o;
        } else {
            return cs.getRank().isHigherEqualsLevel(this.permission);
        }
        //return this.permission == null || cs.hasPermission(this.permission + "." + arg);
    }

    public enum Area {
        EVERY,
        MODERATING,
        ADMINISTRATING
    }
}
