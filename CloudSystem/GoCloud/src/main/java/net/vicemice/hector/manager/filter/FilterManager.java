package net.vicemice.hector.manager.filter;

import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.proxy.Proxy;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.FilterPacket;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.player.utils.Filter;
import org.bson.Document;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public class FilterManager {

    @Getter
    List<Filter> entries = new CopyOnWriteArrayList<>();
    @Getter
    List<String> entriesWord = new CopyOnWriteArrayList<>();

    public void loadEntries() {
        this.entries.clear();
        try {
            ArrayList<Document> list = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("filter");

            for (Document document : list) {
                Filter filter = new Filter(document.getString("word"), document.getLong("created"), (UUID) document.get("creator"));
                this.entries.add(filter);
                this.entriesWord.add(filter.getWord());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void sendToProxy() {
        PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.FILTER_PACKET, new FilterPacket(this.entriesWord));
        for (Proxy proxy : CloudService.getCloudService().getManagerService().getProxyManager().getProxies()) {
            proxy.sendPacket(initPacket);
        }
    }

    public void removeWord(String word) {
        for (Filter filter : this.getEntries()) {
            if (!filter.getWord().equalsIgnoreCase(word)) continue;
            filter.delete();
            this.getEntries().remove(filter);
        }
        this.getEntriesWord().remove(word);
    }
}