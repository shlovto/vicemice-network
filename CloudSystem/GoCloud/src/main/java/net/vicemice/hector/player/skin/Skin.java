package net.vicemice.hector.player.skin;

import lombok.Getter;

public class Skin {

    @Getter
    private String value, signature;

    public Skin(String value, String signature) {
        this.value = value;
        this.signature = signature;
    }
}