package net.vicemice.hector.network.modules.api.event.player.party;

import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.event.async.AsyncEvent;
import net.vicemice.hector.utils.event.async.AsyncPosterAdapter;

/*
 * Class created at 01:13 - 07.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PartyKickEvent extends AsyncEvent<PartyKickEvent> {
    private CloudPlayer leader, target;

    public PartyKickEvent(CloudPlayer leader, CloudPlayer target) {
        super(new AsyncPosterAdapter<>());
        this.leader = leader;
        this.target = target;
    }

    public CloudPlayer getLeader() {
        return leader;
    }

    public CloudPlayer getTarget() {
        return target;
    }
}