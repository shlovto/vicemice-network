package net.vicemice.hector.network.modules.api.event.discord;

import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.event.Event;

public class DiscordUnverifyEvent extends Event {
    private CloudPlayer cloudPlayer;
    private boolean confirmed;

    public DiscordUnverifyEvent(CloudPlayer cloudPlayer, boolean confirmed) {
        this.cloudPlayer = cloudPlayer;
        this.confirmed = confirmed;
    }

    public CloudPlayer getCloudPlayer() {
        return this.cloudPlayer;
    }

    public boolean isConfirmed() {
        return confirmed;
    }
}
