package net.vicemice.hector.network.modules.api.event.server;

import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.utils.event.Event;

public class CloudServerRemoveEvent extends Event {
    private Server cloudServer;

    public Server getCloudServer() {
        return this.cloudServer;
    }

    public CloudServerRemoveEvent(Server cloudServer) {
        this.cloudServer = cloudServer;
    }
}

