package net.vicemice.hector.utils.event.async;

public interface AsyncPoster<E extends AsyncEvent> {
    public void onPreCall(E var1);

    public void onPostCall(E var1);
}

