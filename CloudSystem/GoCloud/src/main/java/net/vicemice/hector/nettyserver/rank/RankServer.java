package net.vicemice.hector.nettyserver.rank;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.PermissionsPacket;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class RankServer implements Runnable {

    @Getter
    private String host;
    @Getter
    private int port;
    @Getter
    private List<Channel> rankClients = new CopyOnWriteArrayList<Channel>();

    public RankServer(String host, int port) {
        this.host = host;
        this.port = port;
    }

    @Override
    public void run() {
        CloudService.getCloudService().getTerminal().writeMessage("§3Starting §eRank-Connector §8(§e§lNetty§8)");
        NioEventLoopGroup bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            (serverBootstrap.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)).childHandler(new ChannelInitializer<SocketChannel>() {

                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    socketChannel.pipeline().addLast(new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(this.getClass().getClassLoader())));
                    socketChannel.pipeline().addLast(new ObjectEncoder());
                    socketChannel.pipeline().addLast(new RankHandler());
                }
            }).option(ChannelOption.SO_BACKLOG, 128);
            ChannelFuture channelFuture = serverBootstrap.bind(this.port).sync();
            channelFuture.channel().closeFuture().sync();
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void newRankClient(Channel newClient) {
        //this.sendPacketToClient(CloudService.getCloudService().getNettyService().getRankManager().getPermissionsPacket(), newClient);
    }

    public void sendPacketToClient(PermissionsPacket permissionsPacket, Channel newClient) {
        new Thread(() -> {
            PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.PERMISSIONS_PACKET, permissionsPacket);
            newClient.writeAndFlush(initPacket);
        }
        ).start();
    }
}