package net.vicemice.hector.config.locale;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import lombok.AccessLevel;
import lombok.Getter;
import org.json.JSONObject;
import java.io.*;
import java.util.Locale;

public class LocaleConfiguration {
    private File configFile;
    @Getter(AccessLevel.PUBLIC)
    private JSONObject jsonObject;
    private Gson gson;

    public LocaleConfiguration(Locale locale) {
        this.configFile = new File("config/locales/" + locale.toLanguageTag() + ".json");
        this.gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
    }

    public boolean exists() {
        return configFile.exists();
    }

    public void readConfiguration() {
        try (FileReader fileReader = new FileReader(configFile)) {
            JsonObject json = gson.fromJson(fileReader, JsonObject.class);
            jsonObject = new JSONObject(json.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}