package net.vicemice.hector.nettyserver;


import lombok.Data;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.nettyserver.lobby.LobbyServer;
import net.vicemice.hector.nettyserver.log.LogServer;
import net.vicemice.hector.nettyserver.rank.RankServer;
import net.vicemice.hector.nettyserver.slave.SlaveServer;
import net.vicemice.hector.nettyserver.proxy.ProxyServer;

/*
 * Class created at 18:39 - 01.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class NettyService {

    private ProxyServer proxyServer;
    private RankServer rankServer;
    private SlaveServer slaveServer;
    private LobbyServer lobbyServer;
    private LogServer logServer;

    public static NettyService newService() {
        NettyService nettyService = new NettyService();
        nettyService.setProxyServer(new ProxyServer(CloudService.getCloudService().getManagerService().getConfigManager().getCloudConfig().getHost(), 1327));
        nettyService.setRankServer(new RankServer(CloudService.getCloudService().getManagerService().getConfigManager().getCloudConfig().getHost(), 1318));
        nettyService.setSlaveServer(new SlaveServer(CloudService.getCloudService().getManagerService().getConfigManager().getCloudConfig().getHost(), 1389));
        nettyService.setLobbyServer(new LobbyServer(CloudService.getCloudService().getManagerService().getConfigManager().getCloudConfig().getHost(), 1369));
        nettyService.setLogServer(new LogServer(CloudService.getCloudService().getManagerService().getConfigManager().getCloudConfig().getHost(), 1371));
        return nettyService;
    }
}