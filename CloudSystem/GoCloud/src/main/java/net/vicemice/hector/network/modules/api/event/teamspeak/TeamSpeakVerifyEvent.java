package net.vicemice.hector.network.modules.api.event.teamspeak;

import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.event.Event;

public class TeamSpeakVerifyEvent extends Event {
    private CloudPlayer cloudPlayer;
    private String uniqueId;

    public TeamSpeakVerifyEvent(CloudPlayer cloudPlayer, String uniqueId) {
        this.cloudPlayer = cloudPlayer;
        this.uniqueId = uniqueId;
    }

    public CloudPlayer getCloudPlayer() {
        return this.cloudPlayer;
    }

    public String getUniqueId() {
        return uniqueId;
    }
}