package net.vicemice.hector.network.modules.api.event.server;

import net.vicemice.hector.network.proxy.Proxy;
import net.vicemice.hector.utils.event.async.AsyncEvent;
import net.vicemice.hector.utils.event.async.AsyncPosterAdapter;

public class ProxyAddEvent extends AsyncEvent<ProxyAddEvent> {
    private Proxy proxyServer;

    public ProxyAddEvent(Proxy proxyServer) {
        super(new AsyncPosterAdapter<>());
        this.proxyServer = proxyServer;
    }

    public Proxy getProxyServer() {
        return this.proxyServer;
    }
}

