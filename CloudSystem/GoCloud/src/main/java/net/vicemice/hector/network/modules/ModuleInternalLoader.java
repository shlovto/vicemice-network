package net.vicemice.hector.network.modules;

public class ModuleInternalLoader implements ModuleLoader {
    private ModuleConfig moduleConfig;

    public ModuleInternalLoader(ModuleConfig moduleConfig) {
        this.moduleConfig = moduleConfig;
    }

    @Override
    public Module loadModule() throws Exception {
        Module module = (Module)this.getClass().getClassLoader().loadClass(this.moduleConfig.getMain()).getConstructor(new Class[0]).newInstance(new Object[0]);
        module.setModuleConfig(this.moduleConfig);
        module.setClassLoader(null);
        return module;
    }
}

