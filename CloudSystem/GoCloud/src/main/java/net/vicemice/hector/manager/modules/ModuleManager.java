package net.vicemice.hector.manager.modules;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.network.modules.Module;
import net.vicemice.hector.network.modules.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ModuleManager {
    private ModuleDetector moduleDetector = new ModuleDetector();
    private File directory = new File("modules");
    private final Collection<String> moduleNames = new ConcurrentLinkedQueue<String>();
    private final Collection<Module> modules = new ConcurrentLinkedQueue<Module>();
    private Collection<String> disabledModuleList = new ArrayList<String>();

    public ModuleManager() {
        this.directory.mkdir();
    }

    public Collection<ModuleConfig> detect() throws Exception {
        return this.detect(this.directory);
    }

    public Collection<ModuleConfig> detect(File directory) {
        Set<ModuleConfig> modules = this.moduleDetector.detectAvailable(directory);
        return modules;
    }

    public ModuleManager loadModules(File directory) throws Exception {
        Collection<ModuleConfig> configs = this.detect(directory);
        for (ModuleConfig config : configs) {
            if (this.moduleNames.contains(config.getName())) continue;
            if (this.disabledModuleList.contains(config.getName())) continue;
            CloudService.getCloudService().getTerminal().writeMessage("§3Try to load §e§l" + config.getName() + " §3(Version: §e§l"+config.getVersion()+"§3)");
            ModuleClassLoader moduleLoader = new ModuleClassLoader(config);
            Module module = moduleLoader.loadModule();
            module.setModuleLoader(moduleLoader);
            module.setDataFolder(directory);
            this.modules.add(module);
            this.moduleNames.add(module.getName());
        }
        return this;
    }

    public ModuleManager loadModules() throws Exception {
        return this.loadModules(this.directory);
    }

    public ModuleManager loadInternalModules(Set<ModuleConfig> modules) throws Exception {
        return this.loadInternalModules(modules, this.directory);
    }

    public ModuleManager loadInternalModules(Set<ModuleConfig> modules, File dataFolder) throws Exception {
        for (ModuleConfig moduleConfig : modules) {
            ModuleInternalLoader moduleLoader = new ModuleInternalLoader(moduleConfig);
            Module module = moduleLoader.loadModule();
            module.setDataFolder(dataFolder);
            module.setModuleLoader(moduleLoader);
            this.modules.add(module);
            this.moduleNames.add(module.getName());
        }
        return this;
    }

    public ModuleManager enableModules() {
        for (Module module : this.modules) {
            CloudService.getCloudService().getTerminal().writeMessage("§3Enabling module §e§l" + module.getModuleConfig().getName() + " §3(Version: §e§l"+module.getModuleConfig().getVersion()+"§3)");
            module.onBootstrap();
        }
        return this;
    }

    public ModuleManager disableModule(Module module) {
        if (!module.isReloadAble()) return this;
        CloudService.getCloudService().getTerminal().writeMessage("§3Disabling module §e§l" + module.getModuleConfig().getName() + " §3(Version: §e§l"+module.getModuleConfig().getVersion()+"§3)");
        module.onShutdown();
        this.modules.remove(module);
        this.moduleNames.remove(module.getName());
        return this;
    }

    public ModuleManager disableModules() {
        while (!this.modules.isEmpty()) {
            Module module = (Module)((Queue)this.modules).poll();
            if (!module.isReloadAble()) continue;
            CloudService.getCloudService().getTerminal().writeMessage("§3Disabling module §e§l" + module.getModuleConfig().getName() + " §3(Version: §e§l"+module.getModuleConfig().getVersion()+"§3)");
            module.onShutdown();
            this.modules.remove(module);
            this.moduleNames.remove(module.getName());
        }
        return this;
    }

    public ModuleDetector getModuleDetector() {
        return this.moduleDetector;
    }

    public File getDirectory() {
        return this.directory;
    }

    public Collection<Module> getModules() {
        return this.modules;
    }

    public Module getModuleByName(String moduleName) {
        for (Module module : this.getModules()) {
            if (module.getName().equalsIgnoreCase(moduleName)) continue;
            return module;
        }
        return null;
    }

    public Collection<String> getDisabledModuleList() {
        return this.disabledModuleList;
    }

    public void setDisabledModuleList(Collection<String> disabledModuleList) {
        this.disabledModuleList = disabledModuleList;
    }
}

