package net.vicemice.hector.utils.utility.interfaces;

public interface Connectable {
    public boolean connect() throws Exception;
}