package net.vicemice.hector.network.modules.api;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.network.modules.Module;
import net.vicemice.hector.utils.event.Event;
import net.vicemice.hector.utils.event.IEventListener;
import net.vicemice.hector.utils.terminal.CommandExecutor;
import net.vicemice.hector.utils.terminal.CommandRegistry;

public class CoreModule extends Module<CloudService> {
    public <T extends Event> void registerListener(IEventListener<T> eventListener) {
        CloudService.getCloudService().getManagerService().getEventManager().registerListener(this, eventListener);
    }

    public void appendModuleProperty(String key, Object value) {
        CloudService.getCloudService().getManagerService().getNetworkManager().getModuleProperties().append(key, value);
    }

    public void registerCommand(CommandExecutor command) {
        CommandRegistry.registerCommand(command);
    }
}

