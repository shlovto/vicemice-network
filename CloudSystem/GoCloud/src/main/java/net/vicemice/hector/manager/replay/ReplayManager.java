package net.vicemice.hector.manager.replay;

import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.packets.types.player.replay.ReplayDataPacket;
import org.bson.Document;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/*
 * Class created at 02:16 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ReplayManager {

    @Getter
    private ConcurrentHashMap<UUID, ReplayDataPacket> uidReplays = new ConcurrentHashMap<>();
    @Getter
    private ConcurrentHashMap<String, ReplayDataPacket> gameidReplays = new ConcurrentHashMap<>();

    public ReplayDataPacket getReplayByUID(UUID uid) {
        return this.uidReplays.get(uid);
    }

    public ReplayDataPacket getReplayByGameID(String id) {
        return this.gameidReplays.get(id);
    }

    public void updateToDatabase(ReplayDataPacket dataPacket, boolean insert) {
        CloudService.getCloudService().getDatabaseQueue().addToQueue(new Replay(dataPacket), insert);
    }

    public void loadAllReplays() {
        try {
            ArrayList<Document> list = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("replays");
            int count = 0;

            for (Document document : list) {
                ++count;
                UUID uid = UUID.fromString(document.getString("uid"));
                String gameID = document.getString("gameid");
                String game = document.getString("game");
                String map = document.getString("map");
                boolean available = document.getBoolean("available");

                ReplayDataPacket dataPacket = new ReplayDataPacket(uid, gameID, game, map, available);

                if (!this.uidReplays.containsKey(uid)) {
                    this.uidReplays.put(uid, dataPacket);
                }

                if (!this.gameidReplays.containsKey(gameID)) {
                    this.gameidReplays.put(gameID, dataPacket);
                }
            }
            CloudService.getCloudService().getTerminal().writeMessage("§3Loaded §e§l" + count + " §3Replays");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}