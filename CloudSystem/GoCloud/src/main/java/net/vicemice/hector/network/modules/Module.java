package net.vicemice.hector.network.modules;

import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import net.vicemice.hector.network.ClydeAPI;
import net.vicemice.hector.utils.event.EventKey;
import net.vicemice.hector.utils.utility.document.Document;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.FileAttribute;

public abstract class Module<E> extends EventKey {
    private File dataFolder;
    private File configFile;
    private File utilFile;
    private ModuleConfig moduleConfig;
    private ModuleClassLoader classLoader;
    private Configuration configuration;
    private ModuleLoader moduleLoader;

    public void onLoad() {
    }

    public void onBootstrap() {
    }

    public void onShutdown() {
    }

    public String getName() {
        return this.moduleConfig != null ? this.moduleConfig.getName() : "some_plugin-" + ClydeAPI.RANDOM.nextLong();
    }

    public File getDataFolder() {
        if (this.dataFolder == null) {
            this.dataFolder = new File("modules", this.moduleConfig.getName());
            try {
                Files.createDirectories(this.dataFolder.toPath(), new FileAttribute[0]);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        return this.dataFolder;
    }

    public String getPluginName() {
        return this.moduleConfig.getName();
    }

    public String getVersion() {
        return this.moduleConfig.getVersion();
    }

    public String getAuthor() {
        return this.moduleConfig.getAuthor();
    }

    public boolean isReloadAble() {
        return this.moduleConfig.isReloadAble();
    }

    public Configuration getConfig() {
        if (this.configuration == null) {
            this.loadConfiguration();
        }
        return this.configuration;
    }

    public Module<E> createUtils(Document document) {
        if (this.utilFile == null) {
            this.utilFile = new File("modules/" + this.moduleConfig.getName() + "/utils.json");
            if (!this.utilFile.exists()) {
                document.saveAsConfig(this.utilFile);
            }
        }
        return this;
    }

    public Document getUtils() {
        if (this.utilFile == null) {
            this.utilFile = new File("modules/" + this.moduleConfig.getName() + "/utils.json");
            if (!this.utilFile.exists()) {
                new Document().saveAsConfig(this.utilFile);
            }
        }
        return Document.loadDocument(this.utilFile);
    }

    public Module<E> saveUtils(Document document) {
        if (this.utilFile == null) {
            this.utilFile = new File("modules/" + this.moduleConfig.getName() + "/utils.json");
        }
        document.saveAsConfig(this.utilFile);
        return this;
    }

    public Module<E> saveConfig() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(this.getConfig(), this.configFile);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return this;
    }

    public Module<E> loadConfig() {
        this.loadConfiguration();
        return this;
    }

    private void loadConfiguration() {
        this.getDataFolder().mkdir();
        if (this.configFile == null) {
            this.configFile = new File("modules/" + this.moduleConfig.getName() + "/config.yml");
            if (!this.configFile.exists()) {
                try {
                    this.configFile.createNewFile();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            this.configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(this.configFile);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public File getConfigFile() {
        return this.configFile;
    }

    public File getUtilFile() {
        return this.utilFile;
    }

    public ModuleConfig getModuleConfig() {
        return this.moduleConfig;
    }

    public ModuleClassLoader getClassLoader() {
        return this.classLoader;
    }

    public Configuration getConfiguration() {
        return this.configuration;
    }

    public ModuleLoader getModuleLoader() {
        return this.moduleLoader;
    }

    public void setDataFolder(File dataFolder) {
        this.dataFolder = dataFolder;
    }

    public void setConfigFile(File configFile) {
        this.configFile = configFile;
    }

    public void setUtilFile(File utilFile) {
        this.utilFile = utilFile;
    }

    public void setModuleConfig(ModuleConfig moduleConfig) {
        this.moduleConfig = moduleConfig;
    }

    public void setClassLoader(ModuleClassLoader classLoader) {
        this.classLoader = classLoader;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public void setModuleLoader(ModuleLoader moduleLoader) {
        this.moduleLoader = moduleLoader;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Module)) {
            return false;
        }
        Module other = (Module)o;
        if (!other.canEqual(this)) {
            return false;
        }
        File this$dataFolder = this.getDataFolder();
        File other$dataFolder = other.getDataFolder();
        if (this$dataFolder == null ? other$dataFolder != null : !((Object)this$dataFolder).equals(other$dataFolder)) {
            return false;
        }
        File this$configFile = this.getConfigFile();
        File other$configFile = other.getConfigFile();
        if (this$configFile == null ? other$configFile != null : !((Object)this$configFile).equals(other$configFile)) {
            return false;
        }
        File this$utilFile = this.getUtilFile();
        File other$utilFile = other.getUtilFile();
        if (this$utilFile == null ? other$utilFile != null : !((Object)this$utilFile).equals(other$utilFile)) {
            return false;
        }
        ModuleConfig this$moduleConfig = this.getModuleConfig();
        ModuleConfig other$moduleConfig = other.getModuleConfig();
        if (this$moduleConfig == null ? other$moduleConfig != null : !this$moduleConfig.equals(other$moduleConfig)) {
            return false;
        }
        ModuleClassLoader this$classLoader = this.getClassLoader();
        ModuleClassLoader other$classLoader = other.getClassLoader();
        if (this$classLoader == null ? other$classLoader != null : !this$classLoader.equals(other$classLoader)) {
            return false;
        }
        Configuration this$configuration = this.getConfiguration();
        Configuration other$configuration = other.getConfiguration();
        if (this$configuration == null ? other$configuration != null : !this$configuration.equals(other$configuration)) {
            return false;
        }
        ModuleLoader this$moduleLoader = this.getModuleLoader();
        ModuleLoader other$moduleLoader = other.getModuleLoader();
        return !(this$moduleLoader == null ? other$moduleLoader != null : !this$moduleLoader.equals(other$moduleLoader));
    }

    protected boolean canEqual(Object other) {
        return other instanceof Module;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        File $dataFolder = this.getDataFolder();
        result = result * 59 + ($dataFolder == null ? 43 : ((Object)$dataFolder).hashCode());
        File $configFile = this.getConfigFile();
        result = result * 59 + ($configFile == null ? 43 : ((Object)$configFile).hashCode());
        File $utilFile = this.getUtilFile();
        result = result * 59 + ($utilFile == null ? 43 : ((Object)$utilFile).hashCode());
        ModuleConfig $moduleConfig = this.getModuleConfig();
        result = result * 59 + ($moduleConfig == null ? 43 : $moduleConfig.hashCode());
        ModuleClassLoader $classLoader = this.getClassLoader();
        result = result * 59 + ($classLoader == null ? 43 : $classLoader.hashCode());
        Configuration $configuration = this.getConfiguration();
        result = result * 59 + ($configuration == null ? 43 : $configuration.hashCode());
        ModuleLoader $moduleLoader = this.getModuleLoader();
        result = result * 59 + ($moduleLoader == null ? 43 : $moduleLoader.hashCode());
        return result;
    }

    public String toString() {
        return "Module(dataFolder=" + this.getDataFolder() + ", configFile=" + this.getConfigFile() + ", utilFile=" + this.getUtilFile() + ", moduleConfig=" + this.getModuleConfig() + ", classLoader=" + this.getClassLoader() + ", configuration=" + this.getConfiguration() + ", moduleLoader=" + this.getModuleLoader() + ")";
    }
}

