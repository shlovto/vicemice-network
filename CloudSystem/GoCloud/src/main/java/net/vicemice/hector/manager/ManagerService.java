package net.vicemice.hector.manager;

import lombok.Data;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.CommandRegistry;
import net.vicemice.hector.manager.abuse.AbuseManager;
import net.vicemice.hector.manager.as.AutonomousManager;
import net.vicemice.hector.manager.beforejoined.BeforeManager;
import net.vicemice.hector.manager.blacklist.BlacklistManager;
import net.vicemice.hector.manager.chatlog.ChatManager;
import net.vicemice.hector.manager.config.ConfigManager;
import net.vicemice.hector.manager.database.DatabaseManager;
import net.vicemice.hector.manager.filter.FilterManager;
import net.vicemice.hector.manager.friend.FriendManager;
import net.vicemice.hector.manager.game.GameManager;
import net.vicemice.hector.manager.lobby.LobbyManager;
import net.vicemice.hector.manager.log.LogServer;
import net.vicemice.hector.manager.modules.ModuleManager;
import net.vicemice.hector.manager.nick.NickNameManager;
import net.vicemice.hector.manager.party.PartyManager;
import net.vicemice.hector.manager.punish.PunishManager;
import net.vicemice.hector.manager.slave.SlaveManager;
import net.vicemice.hector.manager.permissions.PermissionManager;
import net.vicemice.hector.manager.player.PlayerManager;
import net.vicemice.hector.manager.proxy.ProxyManager;
import net.vicemice.hector.manager.replay.ReplayHistoryManager;
import net.vicemice.hector.manager.replay.ReplayManager;
import net.vicemice.hector.manager.report.ReportManager;
import net.vicemice.hector.manager.sign.SignManager;
import net.vicemice.hector.manager.stats.StatsManager;
import net.vicemice.hector.manager.stats.sign.SignStatsManager;
import net.vicemice.hector.manager.time.TimeManager;
import net.vicemice.hector.manager.whitelist.WhitelistManager;
import net.vicemice.hector.nettyserver.rank.RankManager;
import net.vicemice.hector.manager.gameserver.ServerManager;
import net.vicemice.hector.network.NetworkManager;
import net.vicemice.hector.manager.clan.ClanManager;
import net.vicemice.hector.utils.MongoManager;
import net.vicemice.hector.utils.event.EventManager;

import java.util.ArrayList;

/*
 * Class created at 19:06 - 01.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class ManagerService {

    private EventManager eventManager;
    private TimeManager timeManager;
    private MongoManager mongoManager;
    private DatabaseManager databaseManager;
    private ConfigManager configManager;
    private PunishManager punishManager;
    private PlayerManager playerManager;
    private AutonomousManager autonomousManager;
    private BeforeManager beforeManager;
    private PermissionManager permissionManager;
    private AbuseManager abuseManager;
    private ReplayManager replayManager;
    private ReplayHistoryManager replayHistoryManager;
    private NickNameManager nickNameManager;
    private ReportManager reportManager;
    private RankManager rankManager;
    private ServerManager serverManager;
    private ProxyManager proxyManager;
    private SlaveManager slaveManager;
    private LobbyManager lobbyManager;
    private ChatManager chatManager;
    private FilterManager filterManager;
    private FriendManager friendManager;
    private ClanManager clanManager;
    private SignManager signManager;
    private BlacklistManager blacklistManager;
    private WhitelistManager whitelistManager;
    private NetworkManager networkManager;
    private GameManager gameManager;
    private StatsManager statsManager;
    private SignStatsManager signStatsManager;
    private CommandRegistry commandResolver;
    private ModuleManager moduleManager;
    private LogServer logServer;
    private PartyManager partyManager;

    public static ManagerService newService() {
        ManagerService managerService = new ManagerService();
        managerService.setEventManager(new EventManager());
        managerService.setTimeManager(new TimeManager());
        managerService.setDatabaseManager(new DatabaseManager());
        managerService.setConfigManager(ConfigManager.newManager());
        managerService.setPunishManager(new PunishManager());
        managerService.setPlayerManager(new PlayerManager());
        managerService.setAutonomousManager(new AutonomousManager());
        managerService.setAbuseManager(new AbuseManager());
        managerService.setBeforeManager(new BeforeManager());
        managerService.setPermissionManager(new PermissionManager());
        managerService.setAbuseManager(new AbuseManager());
        managerService.setReplayManager(new ReplayManager());
        managerService.setReplayHistoryManager(new ReplayHistoryManager());
        managerService.setNickNameManager(new NickNameManager());
        managerService.setReportManager(new ReportManager());
        managerService.setRankManager(new RankManager());
        managerService.setServerManager(new ServerManager());
        managerService.setProxyManager(new ProxyManager());
        managerService.setSlaveManager(new SlaveManager());
        managerService.setLobbyManager(new LobbyManager());
        managerService.setChatManager(new ChatManager());
        managerService.setFilterManager(new FilterManager());
        managerService.setFriendManager(new FriendManager());
        managerService.setClanManager(new ClanManager());
        managerService.setSignManager(new SignManager());
        managerService.setBlacklistManager(new BlacklistManager());
        managerService.setWhitelistManager(new WhitelistManager());
        managerService.setNetworkManager(new NetworkManager());
        managerService.setGameManager(new GameManager());
        managerService.setStatsManager(new StatsManager());
        managerService.setSignStatsManager(new SignStatsManager());
        managerService.setCommandResolver(new CommandRegistry());
        managerService.setModuleManager(new ModuleManager());
        managerService.setLogServer(new LogServer());
        managerService.setPartyManager(new PartyManager());

        return managerService;
    }

    public void run() {
        new Thread(() -> {
            CloudService.getCloudService().getTerminal().writeMessage("§6Try to starting Threads and Managers");
            this.setMongoManager(new MongoManager());
            CloudService.getCloudService().getDatabaseQueue().start();
            this.getBeforeManager().loadAllPlayers();
            this.getBlacklistManager().reload();
            this.getWhitelistManager().reload();
            this.getPunishManager().init();
            this.getPlayerManager().init();
            this.getPlayerManager().loadAllCloudPlayersFromDatabase();
            this.getAutonomousManager().init();
            this.getReplayManager().loadAllReplays();
            this.getReplayHistoryManager().load();
            this.getServerManager().loadTemplates();
            this.getServerManager().loadServers();
            this.getFilterManager().loadEntries();
            this.getSignManager().reloadSigns();
            this.getProxyManager().loadSettings();
            this.getStatsManager().startRenderTimer();
            this.getGameManager().loadGames();
            this.getSignStatsManager().loadSigns();
            this.getPermissionManager().reloadPermissions();
            this.getNickNameManager().loadNickNames();
            this.getClanManager().loadAllClansFromDatabase();
            this.getChatManager().messages = new ArrayList<>();
            this.getChatManager().init();
            this.getLogServer().startLogServer();
            this.getPartyManager().init();
            new Thread(CloudService.getCloudService().getNettyService().getProxyServer()).start();
            //new Thread(CloudService.getCloudService().getNettyService().getRankServer()).start();
            new Thread(CloudService.getCloudService().getNettyService().getSlaveServer()).start();
            new Thread(CloudService.getCloudService().getNettyService().getLobbyServer()).start();
            new Thread(CloudService.getCloudService().getNettyService().getLogServer()).start();
            CloudService.getCloudService().getTerminal().writeMessage("§3Starting §eShop Manager");
            //shopManager = new ShopManager();
            this.getReportManager().enableReportManager();

            try {
                moduleManager.loadModules();
            } catch (Exception e) {
                CloudService.getCloudService().getTerminal().writeMessage("§cError while loading Module");
                e.printStackTrace();
            }
        }).start();
    }
}