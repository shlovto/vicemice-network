package net.vicemice.hector.manager.stats;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.network.stats.Game;
import net.vicemice.hector.network.stats.RankingField;
import net.vicemice.hector.packets.types.player.stats.StatisticPlayerInfoPacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.lobby.LobbyTopPacket;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.utils.DataUpdateType;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/*
 * Class created at 23:46 - 02.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class StatsManager {
    private List<CloudPlayer> globalStats = new CopyOnWriteArrayList<CloudPlayer>();
    private List<CloudPlayer> monthlyStats = new CopyOnWriteArrayList<CloudPlayer>();
    private List<CloudPlayer> dailyStats = new CopyOnWriteArrayList<CloudPlayer>();
    private ConcurrentHashMap<CloudPlayer, Integer> globalRanking = new ConcurrentHashMap<>();
    private ConcurrentHashMap<CloudPlayer, Integer> monthlyRanking = new ConcurrentHashMap<>();
    private ConcurrentHashMap<CloudPlayer, Integer> dailyRanking = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, List<CloudPlayer>> globalTop = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, List<CloudPlayer>> monthlyTop = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, List<CloudPlayer>> dailyTop = new ConcurrentHashMap<>();
    private PacketHolder lobbyTopPacket = null;
    private int currentStatsRendering = 0;
    private boolean resetting = false;

    public void startRenderTimer() {
        new Thread(() -> {
            new Timer().scheduleAtFixedRate(new TimerTask(){

                @Override
                public void run() {
                    if (currentStatsRendering == 0) {
                        CloudService.getCloudService().getManagerService().getStatsManager().renderGlobalRanking();
                        CloudService.getCloudService().getManagerService().getStatsManager().access$1(currentStatsRendering + 1);
                    } else if (currentStatsRendering == 1) {
                        CloudService.getCloudService().getManagerService().getStatsManager().renderMonthlyRanking();
                        CloudService.getCloudService().getManagerService().getStatsManager().access$1(currentStatsRendering + 1);
                    } else {
                        CloudService.getCloudService().getManagerService().getStatsManager().renderDailyRanking();
                        CloudService.getCloudService().getManagerService().getStatsManager().access$1(0);
                    }
                    if (GoAPI.getTimeManager().getHour().equalsIgnoreCase("00")) {
                        if (!resetting) {
                            CloudService.getCloudService().getManagerService().getStatsManager().access$3(true);
                            for (CloudPlayer cloudPlayer : CloudService.getCloudService().getManagerService().getStatsManager().getDailyStats()) {
                                cloudPlayer.getStats().getDailyStats().clear();
                                cloudPlayer.getStats().getDailyRanking().clear();
                                if (cloudPlayer.isOnline()) cloudPlayer.sendData(cloudPlayer.getServer(), DataUpdateType.UPDATE_VOTES, true);
                                cloudPlayer.updateToDatabase(false);
                            }
                            CloudService.getCloudService().getManagerService().getStatsManager().getDailyRanking().clear();
                            CloudService.getCloudService().getManagerService().getStatsManager().getDailyStats().clear();
                            CloudService.getCloudService().getManagerService().getStatsManager().getDailyTop().clear();
                        }
                    } else {
                        CloudService.getCloudService().getManagerService().getStatsManager().access$3(false);
                    }
                }
            }, 5000, 5000);
        }
        ).start();
    }

    public void updateTopPacket() {
        Game game;
        String value;
        String signature;
        CloudPlayer topPlayer;
        List<CloudPlayer> topPlayers;
        HashMap<String, String[]> map = new HashMap<String, String[]>();
        for (String modi22 : dailyTop.keySet()) {
            topPlayers = dailyTop.get(modi22);
            if (topPlayers.size() == 0) continue;
            topPlayer = topPlayers.get(0);
            game = CloudService.getCloudService().getManagerService().getGameManager().getGameByName(modi22);
            signature = topPlayer.getSkinSignature();
            value = topPlayer.getSkinValue();
            if (signature == null || value == null) {
                signature = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinSignature();
                value = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinValue();
            }
            map.put("daily," + modi22, new String[]{topPlayer.getName(), signature, value, topPlayer.getRank().getRankColor(), String.valueOf(topPlayer.getStats().getDailyStatsValue(game.getPrefix() + "_" + game.getRankingField()))});
        }
        for (String modi22 : monthlyTop.keySet()) {
            topPlayers = monthlyTop.get(modi22);
            if (topPlayers.size() == 0) continue;
            topPlayer = topPlayers.get(0);
            game = CloudService.getCloudService().getManagerService().getGameManager().getGameByName(modi22);
            signature = topPlayer.getSkinSignature();
            value = topPlayer.getSkinValue();
            if (signature == null || value == null) {
                signature = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinSignature();
                value = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinValue();
            }
            map.put("monthly," + modi22, new String[]{topPlayer.getName(), signature, value, topPlayer.getRank().getRankColor(), String.valueOf(topPlayer.getStats().getMonthlyStatsValue(game.getPrefix()+"_"+game.getRankingField()))});
        }
        for (String modi22 : globalTop.keySet()) {
            topPlayers = globalTop.get(modi22);
            if (topPlayers.size() == 0) continue;
            topPlayer = topPlayers.get(0);
            game = CloudService.getCloudService().getManagerService().getGameManager().getGameByName(modi22);
            signature = topPlayer.getSkinSignature();
            value = topPlayer.getSkinValue();
            if (signature == null || value == null) {
                signature = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinSignature();
                value = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getDefaultSkinValue();
            }
            map.put("global," + modi22, new String[]{topPlayer.getName(), signature, value, topPlayer.getRank().getRankColor(), String.valueOf(topPlayer.getStats().getGlobalStatsValue(game.getPrefix()+"_"+game.getRankingField()))});
        }
        lobbyTopPacket = GoAPI.getPacketHelper().preparePacket(PacketType.LOBBY_TOP_PACKET, new LobbyTopPacket(map));
        CloudService.getCloudService().getManagerService().getLobbyManager().sendPacketToAllLobbies(lobbyTopPacket);
        for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
            if (!server.isCloudServer() || !server.getTemplateName().equalsIgnoreCase("silent")) continue;
            server.sendPacket(lobbyTopPacket);
        }
    }

    public void setPlayerStat(CloudPlayer cloudPlayer, String key, long value) {
        CloudService.getCloudService().getManagerService().getStatsManager().setGlobalStat(cloudPlayer, key, value);
        CloudService.getCloudService().getManagerService().getStatsManager().setMonthlyStat(cloudPlayer, key, value);
        CloudService.getCloudService().getManagerService().getStatsManager().setDailyStat(cloudPlayer, key, value);
        cloudPlayer.updateToDatabase(false);
    }

    public void addPlayerStat(CloudPlayer cloudPlayer, String key, long value) {
        CloudService.getCloudService().getManagerService().getStatsManager().addGlobalStat(cloudPlayer, key, value);
        CloudService.getCloudService().getManagerService().getStatsManager().addMonthlyStat(cloudPlayer, key, value);
        CloudService.getCloudService().getManagerService().getStatsManager().addDailyStat(cloudPlayer, key, value);
        cloudPlayer.updateToDatabase(false);
    }

    public void removePlayerStat(CloudPlayer cloudPlayer, String key, long value) {
        CloudService.getCloudService().getManagerService().getStatsManager().removeGlobalStat(cloudPlayer, key, value);
        CloudService.getCloudService().getManagerService().getStatsManager().removeMonthlyStat(cloudPlayer, key, value);
        CloudService.getCloudService().getManagerService().getStatsManager().removeDailyStat(cloudPlayer, key, value);
        cloudPlayer.updateToDatabase(false);
    }

    public void setGlobalStat(CloudPlayer cloudPlayer, String key, long value) {
        cloudPlayer.getStats().getGlobalStats().put(key.toLowerCase(), value);
        if (!globalStats.contains(cloudPlayer)) {
            globalStats.add(cloudPlayer);
        }
    }

    public void addGlobalStat(CloudPlayer cloudPlayer, String key, long value) {
        long finalValue = 0;
        if (cloudPlayer.getStats().getGlobalStats().containsKey(key.toLowerCase())) {
            finalValue = cloudPlayer.getStats().getGlobalStats().get(key.toLowerCase());
        }
        cloudPlayer.getStats().getGlobalStats().put(key.toLowerCase(), finalValue += value);
        if (!globalStats.contains(cloudPlayer)) {
            globalStats.add(cloudPlayer);
        }
    }

    public void removeGlobalStat(CloudPlayer cloudPlayer, String key, long value) {
        long finalValue = 0;
        if (cloudPlayer.getStats().getGlobalStats().containsKey(key.toLowerCase())) {
            finalValue = cloudPlayer.getStats().getGlobalStats().get(key.toLowerCase());
        }
        long newValue = (finalValue-value);
        if (newValue < 0)
            newValue = 0;
        cloudPlayer.getStats().getGlobalStats().put(key.toLowerCase(), newValue);
        if (!globalStats.contains(cloudPlayer)) {
            globalStats.add(cloudPlayer);
        }
    }

    public void setMonthlyStat(CloudPlayer cloudPlayer, String key, long value) {
        cloudPlayer.getStats().getMonthlyStats().put(key.toLowerCase(), value);
        if (!monthlyStats.contains(cloudPlayer)) {
            monthlyStats.add(cloudPlayer);
        }
    }

    public void addMonthlyStat(CloudPlayer cloudPlayer, String key, long value) {
        long finalValue = 0;
        if (cloudPlayer.getStats().getMonthlyStats().containsKey(key.toLowerCase())) {
            finalValue = cloudPlayer.getStats().getMonthlyStats().get(key.toLowerCase());
        }
        cloudPlayer.getStats().getMonthlyStats().put(key.toLowerCase(), finalValue += value);
        if (!monthlyStats.contains(cloudPlayer)) {
            monthlyStats.add(cloudPlayer);
        }
    }

    public void removeMonthlyStat(CloudPlayer cloudPlayer, String key, long value) {
        long finalValue = 0;
        if (cloudPlayer.getStats().getMonthlyStats().containsKey(key.toLowerCase())) {
            finalValue = cloudPlayer.getStats().getMonthlyStats().get(key.toLowerCase());
        }
        long newValue = (finalValue-value);
        if (newValue < 0)
            newValue = 0;
        cloudPlayer.getStats().getMonthlyStats().put(key.toLowerCase(), newValue);
        if (!monthlyStats.contains(cloudPlayer)) {
            monthlyStats.add(cloudPlayer);
        }
    }

    public void setDailyStat(CloudPlayer cloudPlayer, String key, long value) {
        cloudPlayer.getStats().getDailyStats().put(key.toLowerCase(), value);
        if (!dailyStats.contains(cloudPlayer)) {
            dailyStats.add(cloudPlayer);
        }
    }

    public void addDailyStat(CloudPlayer cloudPlayer, String key, long value) {
        long finalValue = 0;
        if (cloudPlayer.getStats().getDailyStats().containsKey(key.toLowerCase())) {
            finalValue = cloudPlayer.getStats().getDailyStats().get(key.toLowerCase());
        }
        cloudPlayer.getStats().getDailyStats().put(key.toLowerCase(), finalValue += value);
        if (!dailyStats.contains(cloudPlayer)) {
            dailyStats.add(cloudPlayer);
        }
    }

    public void removeDailyStat(CloudPlayer cloudPlayer, String key, long value) {
        long finalValue = 0;
        if (cloudPlayer.getStats().getDailyStats().containsKey(key.toLowerCase())) {
            finalValue = cloudPlayer.getStats().getDailyStats().get(key.toLowerCase());
        }
        long newValue = (finalValue-value);
        if (newValue < 0)
            newValue = 0;
        cloudPlayer.getStats().getDailyStats().put(key.toLowerCase(), newValue);
        if (!dailyStats.contains(cloudPlayer)) {
            dailyStats.add(cloudPlayer);
        }
    }

    public void renderGlobalRanking() {
        new Thread(() -> {
            for (Game game : CloudService.getCloudService().getManagerService().getGameManager().getGameList()) {
                ArrayList<RankingField> rankingFields = new ArrayList<RankingField>();
                for (CloudPlayer cloudPlayer : globalStats) {
                    for (String statsKey : cloudPlayer.getStats().getGlobalStats().keySet()) {
                        if (game.getPrefix().equalsIgnoreCase("points")) {
                            if (!statsKey.equalsIgnoreCase(game.getPrefix())) continue;
                            long rankingValue = cloudPlayer.getStats().getGlobalStats().get(statsKey);
                            rankingFields.add(new RankingField(rankingValue, cloudPlayer, game));
                        } else {
                            String statField;
                            if (!statsKey.startsWith(String.valueOf(game.getPrefix()) + "_") || !(statField = statsKey.split("_")[1]).equals(game.getRankingField())) continue;
                            long rankingValue = cloudPlayer.getStats().getGlobalStats().get(statsKey);
                            rankingFields.add(new RankingField(rankingValue, cloudPlayer, game));
                        }
                    }
                }
                Collections.sort(rankingFields);
                int rank = 1;
                ArrayList<CloudPlayer> topPlayers = new ArrayList<CloudPlayer>();
                for (RankingField rankingField : rankingFields) {
                    rankingField.getCloudPlayer().getStats().getGlobalRanking().put(rankingField.getGame().getPrefix(), Long.valueOf(rank));
                    if (rank <= 10) {
                        topPlayers.add(rankingField.getCloudPlayer());
                    }
                    ++rank;
                }
                globalTop.put(game.getModiName(), topPlayers);
            }
            CloudService.getCloudService().getManagerService().getSignStatsManager().updateGlobalSignPacket();
            CloudService.getCloudService().getManagerService().getStatsManager().updateTopPacket();
        }
        ).start();
    }

    public void renderMonthlyRanking() {
        new Thread(() -> {
            for (Game game : CloudService.getCloudService().getManagerService().getGameManager().getGameList()) {
                ArrayList<RankingField> rankingFields = new ArrayList<RankingField>();
                for (CloudPlayer cloudPlayer : monthlyStats) {
                    for (String statsKey : cloudPlayer.getStats().getMonthlyStats().keySet()) {
                        String statField;
                        if (!statsKey.startsWith(String.valueOf(game.getPrefix()) + "_") || !(statField = statsKey.split("_")[1]).equals(game.getRankingField())) continue;
                        long rankingValue = cloudPlayer.getStats().getMonthlyStats().get(statsKey);
                        rankingFields.add(new RankingField(rankingValue, cloudPlayer, game));
                    }
                }
                Collections.sort(rankingFields);
                int rank = 1;
                ArrayList<CloudPlayer> topPlayers = new ArrayList<CloudPlayer>();
                for (RankingField rankingField : rankingFields) {
                    if (rank <= 10) {
                        topPlayers.add(rankingField.getCloudPlayer());
                    }
                    rankingField.getCloudPlayer().getStats().getMonthlyRanking().put(rankingField.getGame().getPrefix(), Long.valueOf(rank));
                    ++rank;
                }
                monthlyTop.put(game.getModiName(), topPlayers);
            }
            CloudService.getCloudService().getManagerService().getSignStatsManager().updateMonthlySignPacket();
            CloudService.getCloudService().getManagerService().getStatsManager().updateTopPacket();
        }
        ).start();
    }

    public void renderDailyRanking() {
        new Thread(() -> {
            for (Game game : CloudService.getCloudService().getManagerService().getGameManager().getGameList()) {
                ArrayList<RankingField> rankingFields = new ArrayList<RankingField>();
                for (CloudPlayer cloudPlayer : dailyStats) {
                    for (String statsKey : cloudPlayer.getStats().getDailyStats().keySet()) {
                        String statField;
                        if (!statsKey.startsWith(String.valueOf(game.getPrefix()) + "_") || !(statField = statsKey.split("_")[1]).equals(game.getRankingField())) continue;
                        long rankingValue = cloudPlayer.getStats().getDailyStats().get(statsKey);
                        rankingFields.add(new RankingField(rankingValue, cloudPlayer, game));
                    }
                }
                Collections.sort(rankingFields);
                int rank = 1;
                ArrayList<CloudPlayer> topPlayers = new ArrayList<CloudPlayer>();
                for (RankingField rankingField : rankingFields) {
                    if (rank <= 10) {
                        topPlayers.add(rankingField.getCloudPlayer());
                    }
                    rankingField.getCloudPlayer().getStats().getDailyRanking().put(rankingField.getGame().getPrefix(), Long.valueOf(rank));
                    ++rank;
                }
                dailyTop.put(game.getModiName(), topPlayers);
            }
            CloudService.getCloudService().getManagerService().getSignStatsManager().updateDailySignPacket();
            CloudService.getCloudService().getManagerService().getStatsManager().updateTopPacket();
        }
        ).start();
    }

    public StatisticPlayerInfoPacket createInfoPacket(CloudPlayer cloudPlayer, String[] rankKeys) {
        HashMap<String, HashMap<StatisticPeriod, Long>> info = new HashMap<>();
        HashMap<String, HashMap<StatisticPeriod, Long>> ranks = new HashMap<>();

        for (String key : cloudPlayer.getStats().getDailyStats().keySet()) {
            if (info.containsKey(key)) {
                HashMap<StatisticPeriod, Long> period = info.get(key);
                period.put(StatisticPeriod.DAY, cloudPlayer.getStats().getDailyStatsValue(key));
                info.put(key, period);
            } else {
                HashMap<StatisticPeriod, Long> period = new HashMap<>();
                period.put(StatisticPeriod.DAY, cloudPlayer.getStats().getDailyStatsValue(key));
                info.put(key, period);
            }
        }

        for (String key : cloudPlayer.getStats().getMonthlyStats().keySet()) {
            if (info.containsKey(key)) {
                HashMap<StatisticPeriod, Long> period = info.get(key);
                period.put(StatisticPeriod.MONTH, cloudPlayer.getStats().getMonthlyStatsValue(key));
                info.put(key, period);
            } else {
                HashMap<StatisticPeriod, Long> period = new HashMap<>();
                period.put(StatisticPeriod.MONTH, cloudPlayer.getStats().getMonthlyStatsValue(key));
                info.put(key, period);
            }
        }

        for (String key : cloudPlayer.getStats().getDailyStats().keySet()) {
            if (info.containsKey(key)) {
                HashMap<StatisticPeriod, Long> period = info.get(key);
                period.put(StatisticPeriod.GLOBAL, cloudPlayer.getStats().getGlobalStatsValue(key));
                info.put(key, period);
            } else {
                HashMap<StatisticPeriod, Long> period = new HashMap<>();
                period.put(StatisticPeriod.GLOBAL, cloudPlayer.getStats().getGlobalStatsValue(key));
                info.put(key, period);
            }
        }

        if (rankKeys != null) {
            String[] var12 = rankKeys;
            int var11 = rankKeys.length;

            for(int var10 = 0; var10 < var11; ++var10) {
                String key = var12[var10];
                HashMap<StatisticPeriod, Long> periodValues = new HashMap<>();
                periodValues.put(StatisticPeriod.DAY, cloudPlayer.getStats().getDailyRanking().get(key)); //DAILY
                periodValues.put(StatisticPeriod.MONTH, cloudPlayer.getStats().getMonthlyRanking().get(key)); //MONTH
                periodValues.put(StatisticPeriod.GLOBAL, cloudPlayer.getStats().getGlobalRanking().get(key)); //GLOBAL
                ranks.put(key, periodValues);
            }
        }

        return new StatisticPlayerInfoPacket(cloudPlayer.getUniqueId(), info, ranks);
    }

    public List<CloudPlayer> getGlobalStats() {
        return globalStats;
    }

    public List<CloudPlayer> getMonthlyStats() {
        return monthlyStats;
    }

    public List<CloudPlayer> getDailyStats() {
        return dailyStats;
    }

    public ConcurrentHashMap<CloudPlayer, Integer> getGlobalRanking() {
        return globalRanking;
    }

    public ConcurrentHashMap<CloudPlayer, Integer> getMonthlyRanking() {
        return monthlyRanking;
    }

    public ConcurrentHashMap<CloudPlayer, Integer> getDailyRanking() {
        return dailyRanking;
    }

    public ConcurrentHashMap<String, List<CloudPlayer>> getGlobalTop() {
        return globalTop;
    }

    public ConcurrentHashMap<String, List<CloudPlayer>> getMonthlyTop() {
        return monthlyTop;
    }

    public ConcurrentHashMap<String, List<CloudPlayer>> getDailyTop() {
        return dailyTop;
    }

    public PacketHolder getLobbyTopPacket() {
        return lobbyTopPacket;
    }

    /* synthetic */ void access$1(int n) {
        currentStatsRendering = n;
    }

    /* synthetic */ void access$3(boolean bl) {
        resetting = bl;
    }
}