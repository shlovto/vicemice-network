package net.vicemice.hector.manager.party.event;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.modules.api.event.player.party.PartyInviteEvent;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.ComponentMessagePacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.party.Party;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.event.IEventListener;

import java.util.ArrayList;

/*
 * Class created at 00:56 - 07.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PartyInviteListener implements IEventListener<PartyInviteEvent> {

    @Override
    public void onCall(PartyInviteEvent event) {
        CloudPlayer leader = event.getLeader(),
                target = event.getTarget();
        Party party = leader.getParty();

        if (target == leader) {
            leader.sendMessage("player-self-interact", leader.getLanguageMessage("party-prefix"));
            return;
        }
        if (party.getMembers().size() == party.getMax()) {
            leader.sendMessage("party-reach-limit", party.getMembers().size(), party.getMax());
            return;
        }
        if (!target.isOnline()) {
            leader.sendMessage("user-not-online", leader.getLanguageMessage("party-prefix"), (target.getRank().getRankColor()+target.getName()));
            return;
        }
        if (target.getSetting("advanced-player-settings", Integer.class) == 1) {
            boolean reject = false;
            if (leader.getClan() != null && leader.getClan().getMembers().containsKey(target.getUniqueId()) && target.getSetting("party-invites-clan", Integer.class) == 0) {
                reject = true;
            } else if (target.isFriend(leader) && target.getFriends().get(leader.getUniqueId()).isFavorite() && target.getSetting("party-invites-friend-favorites", Integer.class) == 0) {
                reject = true;
            } else if (target.isFriend(leader) && target.getSetting("party-invites-friends", Integer.class) == 0) {
                reject = true;
            } else if (target.getRank().isTeam() && target.getSetting("party-invites-staffs", Integer.class) == 0) {
                reject = true;
            } else if (target.getRank().equalsLevel(Rank.VIP) && target.getSetting("party-invites-vips", Integer.class) == 0) {
                reject = true;
            } else if (target.getRank().equalsLevel(Rank.PREMIUM) && target.getSetting("party-invites-premiums", Integer.class) == 0) {
                reject = true;
            } else if (target.getRank().equalsLevel(Rank.MEMBER) && target.getSetting("party-invites-members", Integer.class) == 0) {
                reject = true;
            }

            if (leader.getClan() != null && leader.getClan().getMembers().containsKey(target.getUniqueId()) && target.getSetting("party-invites-clan", Integer.class) == 1) {
                reject = false;
            } if (target.isFriend(leader) && target.getFriends().get(leader.getUniqueId()).isFavorite() && target.getSetting("party-invites-friend-favorites", Integer.class) == 1) {
                reject = false;
            } if (target.isFriend(leader) && target.getSetting("party-invites-friends", Integer.class) == 1) {
                reject = false;
            } if (target.getRank().isTeam() && target.getSetting("party-invites-staffs", Integer.class) == 1) {
                reject = false;
            } if (target.getRank().equalsLevel(Rank.VIP) && target.getSetting("party-invites-vips", Integer.class) == 1) {
                reject = false;
            } if (target.getRank().equalsLevel(Rank.PREMIUM) && target.getSetting("party-invites-premiums", Integer.class) == 1) {
                reject = false;
            } if (target.getRank().equalsLevel(Rank.MEMBER) && target.getSetting("party-invites-members", Integer.class) == 1) {
                reject = false;
            }
            
            if (reject) {
                leader.sendMessage("party-invite-disabled");
                return;
            }
        } else {
            if (target.getSettingsPacket().getSetting("party-invites", Integer.class) == 2) {
                leader.sendMessage("party-invite-disabled");
                return;
            }
            if (target.getSettingsPacket().getSetting("party-invites", Integer.class) == 1 && !target.isFriend(leader)) {
                leader.sendMessage("party-invite-friends");
                return;
            }
        }
        
        if (target.getParty() != null) {
            leader.sendMessage("party-is-in-party", (target.getRank().getRankColor()+target.getName()));
            return;
        }
        if (!party.getInvites().contains(target)) {
            party.getInvites().add(target);
            target.sendMessage("party-invite", (leader.getRank().getRankColor()+leader.getName()));
            ArrayList<String[]> messageData = new ArrayList<String[]>();
            messageData.add(new String[]{target.getLanguageMessage("party-prefix")+" "});
            messageData.add(new String[]{"§a["+target.getLanguageMessage("accept")+"]", "true", "run_command", "/party accept " + leader.getName()});
            ComponentMessagePacket componentMessagePacket = new ComponentMessagePacket(target.getName().toLowerCase(), messageData);
            target.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMPONENT_MESSAGE_PACKET, componentMessagePacket));
            leader.sendMessage("party-invited", (target.getRank().getRankColor()+target.getName()));

            if (target.isAfk()) {
                leader.sendMessage("user-is-afk", leader.getLanguageMessage("party-prefix"), target.getRank().getRankColor() + target.getName());
            }
        } else {
            leader.sendMessage("party-already-invited");
        }
    }
}
