package net.vicemice.hector.network;

import lombok.Getter;
import net.vicemice.hector.utils.utility.document.Document;

/*
 * Class created at 20:01 - 01.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class NetworkManager {
    @Getter
    private Document moduleProperties = new Document();
}
