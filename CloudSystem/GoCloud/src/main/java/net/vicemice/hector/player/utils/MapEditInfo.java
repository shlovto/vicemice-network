package net.vicemice.hector.player.utils;

import lombok.Data;

import java.io.Serializable;

/*
 * Class created at 22:19 - 09.04.2020
 * Copyright (C) elrobtossohn
 */
@Data
public class MapEditInfo implements Serializable {

    private String mapName, templateName;

    public MapEditInfo(String mapName, String templateName) {
        this.mapName = mapName;
        this.templateName = templateName;
    }
}