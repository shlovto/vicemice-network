package net.vicemice.hector.manager.whitelist;

import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.gameserver.WhitelistedServer;
import net.vicemice.hector.network.proxy.Proxy;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.proxy.players.ProxyWhitelistedPlayersPacket;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class WhitelistManager {

    @Getter
    private List<WhitelistedServer> servers;
    @Getter
    private List<UUID> players;

    public void reload() {
        this.servers = new ArrayList<>();
        this.players = new ArrayList<>();

        for (Document document : CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("whitelist")) {
            if (document.getString("type").equalsIgnoreCase("SERVER")) {
                String host = document.get("values", Document.class).getString("host");
                Integer port = document.get("values", Document.class).getInteger("port");
                String servername = document.get("values", Document.class).getString("servername");
                String rank = document.get("values", Document.class).getString("rank");
                Integer version = document.get("values", Document.class).getInteger("version");
                this.servers.add(new WhitelistedServer(host, port, servername, rank, version));
            } else if (document.getString("type").equalsIgnoreCase("PLAYER")) {
                String uniqueId = document.get("values", Document.class).getString("uniqueId");
                this.players.add(UUID.fromString(uniqueId));
            }
        }

        for (Proxy proxy : CloudService.getCloudService().getManagerService().getProxyManager().getProxies()) {
            proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_WHITELISTED_PLAYERS, new ProxyWhitelistedPlayersPacket(CloudService.getCloudService().getManagerService().getWhitelistManager().getPlayers())));
        }

        CloudService.getCloudService().getTerminal().writeMessage("§3Loaded §e§l" + this.servers.size() + " Whitelisted §3Servers");
        CloudService.getCloudService().getTerminal().writeMessage("§3Loaded §e§l" + this.players.size() + " Whitelisted §3Players");
    }

    public boolean checkServer(String servername, String host, int port, String rank, String version) {
        if (this.servers.stream().filter(e -> e.getName().equalsIgnoreCase(servername)).filter(e -> e.getHost().equalsIgnoreCase(host) && e.getPort() == port).count() > 0) {
            return true;
        }

        return false;
    }

    public void addPlayer(UUID uniqueId) {
        if (this.players.contains(uniqueId)) return;

        for (Proxy proxy : CloudService.getCloudService().getManagerService().getProxyManager().getProxies()) {
            proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_WHITELISTED_PLAYERS, new ProxyWhitelistedPlayersPacket(CloudService.getCloudService().getManagerService().getWhitelistManager().getPlayers())));
        }

        this.players.add(uniqueId);
        CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("whitelist").insertOne(new Document("type", "PLAYER").append("values", new Document("uniqueId", String.valueOf(uniqueId))));
    }

    public void removePlayer(UUID uniqueId) {
        if (!this.players.contains(uniqueId)) return;

        for (Proxy proxy : CloudService.getCloudService().getManagerService().getProxyManager().getProxies()) {
            proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_WHITELISTED_PLAYERS, new ProxyWhitelistedPlayersPacket(CloudService.getCloudService().getManagerService().getWhitelistManager().getPlayers())));
        }

        this.players.remove(uniqueId);
        CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("whitelist").deleteOne(new Document("type", "PLAYER").append("values", new Document("uniqueId", String.valueOf(uniqueId))));
    }
}