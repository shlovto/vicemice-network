package net.vicemice.hector.manager.nick;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.nick.NickHistory;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.nick.PlayerNickPacket;
import net.vicemice.hector.packets.types.player.nick.PlayerUnnickPacket;
import net.vicemice.hector.utils.JSONUtil;
import net.vicemice.hector.utils.players.Nick;
import org.bson.Document;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/*
 * Class created at 00:22 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class NickNameManager {
    @Getter
    @Setter
    private boolean available = true;
    @Getter
    private List<CloudPlayer> nickedPlayers = new CopyOnWriteArrayList<>();
    @Getter
    private List<String> nickNames = new CopyOnWriteArrayList<>();
    @Getter
    private ConcurrentHashMap<String, Nick> nicks = new ConcurrentHashMap<>();
    @Getter
    private List<NickHistory> nickHistories = new CopyOnWriteArrayList<>();

    public void loadNickNames() {
        this.nickNames.clear();
        this.nicks.clear();
        int count = 0;

        List<Document> nickList = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("gameService", "nickNames");
        for (Document document : nickList) {
            String name = document.getString("name");
            UUID uniqueId = null;
            if (document.containsKey("uniqueId"))
                uniqueId = UUID.fromString(document.getString("uniqueId"));
            if (name.length() > 16) continue;
            nickNames.add(name);
            if (uniqueId != null)
                nicks.put(name, new Nick(name, uniqueId));
            ++count;
        }

        CloudService.getCloudService().getTerminal().writeMessage("§3Loaded §e§l" + count + " §3Nicknames");
    }

    public void checkValid(CloudPlayer cloudPlayer) {
        int valid = 0,
                invalid = 0;

        for (String s : nickNames) {
            if (this.nicks.containsKey(s)) continue;

            JSONObject jsonObject = new JSONUtil(JSONUtil.API.MCSTATS, s).getObject();
            if (jsonObject.has("response") && jsonObject.getJSONObject("response").has("UUID")) {
                String uuid = jsonObject.getJSONObject("response").getString("UUID");

                if (CloudService.getCloudService().getManagerService().getMongoManager().getDocument("gameService", "nickNames", new Document("name", s)) != null) continue;
                CloudService.getCloudService().getManagerService().getMongoManager().getGamesDatabase().getCollection("nickNames").insertOne(new Document("name", s).append("uniqueId", uuid));
                valid++;
            } else {
                invalid++;
            }
        }

        if (cloudPlayer != null && cloudPlayer.isOnline())
            cloudPlayer.sendRawMessage("§aSuccessfully checked §6§l"+this.nickNames.size()+"§a, §6§l"+valid+" valid's §aand §6§l"+invalid+" invalid's");
        else
            CloudService.getCloudService().getTerminal().write("§aSuccessfully checked §6§l"+this.nickNames.size()+"§a, §6§l"+valid+" valid's §aand §6§l"+invalid+" invalid's");
    }

    public boolean addAndValidate(String s) {
        JSONObject jsonObject = new JSONUtil(JSONUtil.API.MCSTATS, s).getObject();
        if (jsonObject.has("response") && jsonObject.getJSONObject("response").has("UUID")) {
            String uuid = jsonObject.getJSONObject("response").getString("UUID");

            if (CloudService.getCloudService().getManagerService().getMongoManager().getDocument("gameService", "nickNames", new Document("name", s)) != null) return false;
            CloudService.getCloudService().getManagerService().getMongoManager().getGamesDatabase().getCollection("nickNames").insertOne(new Document("name", s).append("uniqueId", uuid));
            this.getNickNames().add(s);
            this.getNicks().put(s, new Nick(s, UUID.fromString(uuid)));
            return true;
        }

        return false;
    }

    public boolean remove(String s) {
        if (CloudService.getCloudService().getManagerService().getMongoManager().getDocument("gameService", "nickNames", new Document("name", s)) != null) {
            CloudService.getCloudService().getManagerService().getMongoManager().getGamesDatabase().getCollection("nickNames").deleteOne(new Document("name", s));
            return true;
        }

        return false;
    }

    public CloudPlayer getCloudPlayerByNickName(String nickname) {
        for (CloudPlayer nicked : CloudService.getCloudService().getManagerService().getNickNameManager().getNickedPlayers()) {
            if (nicked.getNickName() == null || !nicked.getNickName().equalsIgnoreCase(nickname)) continue;
            return nicked;
        }
        return null;
    }

    public String getNickName() {
        Collections.shuffle(nickNames);
        for (String nickName : nickNames) {
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getNickNameManager().getCloudPlayerByNickName(nickName);
            if (cloudPlayer != null) continue;
            return nickName;
        }
        return null;
    }

    public String[] getTextureData() {
        List premiumPlayers = CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().values().stream().collect(Collectors.toList());
        Collections.shuffle(premiumPlayers);
        CloudPlayer usePlayer = (CloudPlayer) premiumPlayers.get(0);
        return new String[]{usePlayer.getSkinValue(), usePlayer.getSkinSignature()};
        //return null;
    }

    public void unnickPlayer(CloudPlayer cloudPlayer) {
        if (cloudPlayer.getNickName() != null) {
            cloudPlayer.setNickName(null);
            cloudPlayer.setNickPacket(null);
            nickedPlayers.remove(cloudPlayer);
            PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_UNNICK_PACKET, new PlayerUnnickPacket(cloudPlayer.getName()));
            cloudPlayer.getServer().sendPacket(initPacket);
        }
    }

    public void nickPlayer(CloudPlayer cloudPlayer, String nickName, String[] textureData, Server server) {
        if (cloudPlayer.getNickPacket() != null) {
            if (server != null) {
                server.sendPacket(cloudPlayer.getNickPacket());
            } else {
                cloudPlayer.getServer().sendPacket(cloudPlayer.getNickPacket());
            }
            return;
        }
        if (!nickedPlayers.contains(cloudPlayer)) {
            nickedPlayers.add(cloudPlayer);
        }
        cloudPlayer.setNickName(nickName);

        UUID uuid = null;
        if (this.getNicks().containsKey(nickName)) {
            uuid = this.getNicks().get(nickName).getUuid();
        } else {
            JSONObject jsonObject = new JSONUtil(JSONUtil.API.MCSTATS, nickName).getObject();
            if (jsonObject.has("response") && jsonObject.getJSONObject("response").has("UUID"))
                uuid = UUID.fromString(jsonObject.getJSONObject("response").getString("UUID"));

            if (uuid == null)
                uuid = cloudPlayer.getUniqueId();
        }

        PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_NICK_PACKET, new PlayerNickPacket(cloudPlayer.getName(), uuid, nickName, textureData[0], textureData[1]));
        cloudPlayer.setNickPacket(initPacket);
        if (server == null) {
            cloudPlayer.getServer().sendPacket(initPacket);
        } else {
            server.sendPacket(initPacket);
        }
    }

    public List<NickHistory> getNickHistoryByNickname(String nickname) {
        ArrayList<NickHistory> list = new ArrayList<NickHistory>();
        for (NickHistory nickHistory : nickHistories) {
            if (!nickHistory.getNickname().equalsIgnoreCase(nickname)) continue;
            list.add(nickHistory);
        }
        return list;
    }

    public void addToDatabase(NickHistory nickHistory) {
        try {
            Document document = new Document("nickname", nickHistory.getNickname());
            document.append("username", nickHistory.getUsername());
            document.append("start", nickHistory.getStart());
            document.append("ended", nickHistory.getEnded());

            CloudService.getCloudService().getManagerService().getMongoManager().getGamesDatabase().getCollection("nickHistory").insertOne(document);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}