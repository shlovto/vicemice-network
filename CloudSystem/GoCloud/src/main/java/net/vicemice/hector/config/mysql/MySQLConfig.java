package net.vicemice.hector.config.mysql;

import lombok.Data;

/**
 * Created by Paul B. on 27.01.2019.
 */
@Data
public class MySQLConfig {
    private String host = "smochy.de";
    private String port = "3306";
    private String username = "root";
    private String password = "username";
    private String database = "database";
}