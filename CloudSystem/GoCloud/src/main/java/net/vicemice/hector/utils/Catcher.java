package net.vicemice.hector.utils;

public interface Catcher<E, V> {
    public E doCatch(V var1);
}