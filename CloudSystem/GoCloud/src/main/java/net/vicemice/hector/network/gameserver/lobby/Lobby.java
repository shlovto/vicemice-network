package net.vicemice.hector.network.gameserver.lobby;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.network.gameserver.Server;

public class Lobby {

    @Getter
    private Server server;
    @Setter
    private boolean active = false;

    public Lobby(Server server) {
        this.server = server;
    }

    public void lobbyDown() {
    }

    public boolean isActive() {
        return this.active;
    }
}

