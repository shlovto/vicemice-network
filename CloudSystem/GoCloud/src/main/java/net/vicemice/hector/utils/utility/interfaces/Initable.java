package net.vicemice.hector.utils.utility.interfaces;

public interface Initable extends Runnable {
    @Override
    default public void run() {
        this.init();
    }

    public void init();
}