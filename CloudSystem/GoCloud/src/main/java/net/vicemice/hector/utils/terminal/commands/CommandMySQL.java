package net.vicemice.hector.utils.terminal.commands;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.utils.terminal.ArgumentList;
import net.vicemice.hector.utils.terminal.CommandExecutor;
import net.vicemice.hector.utils.terminal.Terminal;

public class CommandMySQL implements CommandExecutor {
    @Override
    public void onCommand(String command, Terminal writer, String[] args) {
        try {
            if (CloudService.getCloudService().getManagerService().getMongoManager().getClient() != null) {
                writer.writeMessage("§aThe Database is connected!");
            } else {
                writer.writeMessage("§cThe Database is not connected!");
            }
        } catch (Exception ex) {
            writer.writeMessage("§cThe Database is not connected!");
        }
    }

    @Override
    public ArgumentList getArguments() {
        return ArgumentList.builder().arg(new ArgumentList.Argument("/mongodb", "MongoDB Information")).build();
    }
}
