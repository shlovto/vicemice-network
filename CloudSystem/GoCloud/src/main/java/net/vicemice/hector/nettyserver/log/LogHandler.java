package net.vicemice.hector.nettyserver.log;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.slave.LogPacket;
import net.vicemice.hector.packets.types.PacketHolder;
import org.bson.Document;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class LogHandler extends SimpleChannelInboundHandler<Object> {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {}

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object o) throws Exception {
        new Thread(() -> {
            block21:
            {
                PacketHolder initPacket = (PacketHolder) o;

                //CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().sendCaveMessage(CloudService.getOverWatchPrefix() + "The Log Handler Recieved an Packet with the Type §6§l" + initPacket.getType().toString().toUpperCase());

                if (initPacket.getKey().equals(PacketType.LOG_PACKET.getKey())) {
                    LogPacket logPacket;
                    boolean error;
                    logPacket = (LogPacket) initPacket.getValue();
                    File checkFile = new File("slave/logs/"+logPacket.getSlaveId()+"/"+logPacket.getServerId());
                    if (checkFile.exists()) {
                        checkFile.delete();
                    }
                    String command = "wget --quiet -O slave/logs/"+logPacket.getSlaveId()+"/"+logPacket.getServerId()+" http://"+logPacket.getHost()+":1965/"+logPacket.getServerId();
                    try {
                        Runtime.getRuntime().exec(new String[]{"bash", "-c", command}).waitFor();
                    } catch (Exception exc) {
                        exc.printStackTrace();
                    }
                    error = false;
                    BufferedReader br = null;
                    try {
                        String line;
                        br = new BufferedReader(new FileReader("slave/logs/"+logPacket.getSlaveId()+"/"+logPacket.getServerId()));
                        while ((line = br.readLine()) != null) {
                            if (!line.toLowerCase().contains("exception")) continue;
                            error = true;
                            break;
                        }
                    } catch (Exception exc) {
                        exc.printStackTrace();
                    }
                    if (CloudService.getCloudService().isDevMode()) CloudService.getCloudService().getTerminal().write("§3Downloaded §e§l"+logPacket.getServerId()+" §3log from §e§l"+logPacket.getSlaveId());
                    try {
                        Document document = new Document("serverid", logPacket.getServerId());
                        document.append("slaveId", logPacket.getSlaveId());
                        document.append("created", System.currentTimeMillis());
                        document.append("error", error);

                        CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("serverLogs").insertOne(document);
                    } catch (Exception exc) {
                        exc.printStackTrace();
                    }
                }
            }
        }).start();
    }
}