package net.vicemice.hector.utils.threading;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public final class Scheduler
implements TaskCancelable,
Runnable {
    private ConcurrentHashMap<Long, ScheduledTask> tasks = new ConcurrentHashMap<>();
    private final int ticks;
    private final Random random = new Random();

    public Scheduler(int ticks) {
        this.ticks = ticks;
    }

    public Scheduler() {
        this.ticks = 10;
    }

    public ScheduledTask runTaskSync(Runnable runnable) {
        return this.runTaskDelaySync(runnable, 0);
    }

    public ScheduledTask runTaskDelaySync(Runnable runnable, int delayTicks) {
        return this.runTaskRepeatSync(runnable, delayTicks, -1);
    }

    public ScheduledTask runTaskRepeatSync(Runnable runnable, int delayTicks, int repeatDelay) {
        long id = this.random.nextLong();
        ScheduledTask task = new ScheduledTask(id, runnable, delayTicks, repeatDelay);
        this.tasks.put(id, task);
        return task;
    }

    public ScheduledTask runTaskAsync(Runnable runnable) {
        return this.runTaskDelayAsync(runnable, 0);
    }

    public ScheduledTask runTaskDelayAsync(Runnable runnable, int delay) {
        return this.runTaskRepeatAsync(runnable, delay, -1);
    }

    public ScheduledTask runTaskRepeatAsync(Runnable runnable, int delay, int repeat) {
        long id = this.random.nextLong();
        ScheduledTaskAsync task = new ScheduledTaskAsync(id, runnable, delay, repeat, this);
        this.tasks.put(id, task);
        return task;
    }

    @Override
    public void cancelTask(Long id) {
        if (this.tasks.containsKey(id)) {
            this.tasks.get(id).cancel();
        }
    }

    @Override
    public void cancelAllTasks() {
        this.tasks.clear();
    }

    @Deprecated
    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Thread.sleep(1000 / this.ticks);
            }
            catch (InterruptedException interruptedException) {
                // empty catch block
            }
            if (this.tasks.isEmpty()) continue;
            ConcurrentHashMap<Long, ScheduledTask> tasks = this.tasks;
            for (ScheduledTask task : tasks.values()) {
                if (task.isInterrupted()) {
                    this.tasks.remove(task.getTaskId());
                    continue;
                }
                task.run();
            }
        }
    }

    public int getTicks() {
        return this.ticks;
    }

    public Random getRandom() {
        return this.random;
    }
}

