package net.vicemice.hector.network.modules;

public interface ModuleLoader {
    public Module loadModule() throws Exception;
}