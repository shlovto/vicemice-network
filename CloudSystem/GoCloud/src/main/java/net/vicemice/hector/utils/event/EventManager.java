package net.vicemice.hector.utils.event;

import net.jodah.typetools.TypeResolver;
import net.vicemice.hector.utils.event.async.AsyncEvent;
import net.vicemice.hector.utils.event.interfaces.IEventManager;
import net.vicemice.hector.utils.utility.scheduler.TaskScheduler;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class EventManager implements IEventManager {
    private final Map<Class<? extends Event>, Collection<EventEntity>> registeredListeners = new ConcurrentHashMap(0);

    @Override
    public <T extends Event> void registerListener(EventKey eventKey, IEventListener<T> eventListener) {
        Class<? extends Event> eventClazz = (Class<? extends Event>) TypeResolver.resolveRawArgument(IEventListener.class, eventListener.getClass());
        if (!this.registeredListeners.containsKey(eventClazz)) {
            this.registeredListeners.put(eventClazz, new LinkedList());
        }
        this.registeredListeners.get(eventClazz).add(new EventEntity<T>(eventListener, eventKey, eventClazz));
    }

    @SafeVarargs
    @Override
    public final <T extends Event> void registerListeners(EventKey eventKey, IEventListener<T> ... eventListeners) {
        for (IEventListener<T> eventListener : eventListeners) {
            this.registerListener(eventKey, eventListener);
        }
    }

    @Override
    public void unregisterListener(EventKey eventKey) {
        for (Map.Entry<Class<? extends Event>, Collection<EventEntity>> eventEntities : this.registeredListeners.entrySet()) {
            for (EventEntity entities : eventEntities.getValue()) {
                if (!entities.getEventKey().equals(eventKey)) continue;
                this.registeredListeners.get(eventEntities.getKey()).remove(entities);
            }
        }
    }

    public void unregisterListener(IEventListener<? extends Event> eventListener) {
        try {
            Class clazz = this.getClazz(eventListener);
            if (this.registeredListeners.containsKey(clazz)) {
                for (EventEntity eventEntity : this.registeredListeners.get(clazz)) {
                    if (!eventEntity.getEventListener().equals(eventListener)) continue;
                    this.registeredListeners.get(clazz).remove(eventEntity);
                    return;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void unregisterListener(Class<? extends Event> eventClass) {
        this.registeredListeners.remove(eventClass);
    }

    @Override
    public <T extends Event> boolean callEvent(T event) {
        if (!this.registeredListeners.containsKey(event.getClass())) {
            return true;
        }
        if (!(event instanceof AsyncEvent)) {
            for (EventEntity eventEntity : this.registeredListeners.get(event.getClass())) {
                eventEntity.getEventListener().onCall(event);
            }
        } else {
            TaskScheduler.runtimeScheduler().schedule(() -> {
                AsyncEvent asyncEvent = (AsyncEvent)event;
                asyncEvent.getPoster().onPreCall(asyncEvent);
                for (EventEntity eventEntity : this.registeredListeners.get(event.getClass())) {
                    eventEntity.getEventListener().onCall(event);
                }
                asyncEvent.getPoster().onPostCall(asyncEvent);
            });
        }
        return false;
    }

    private Class getClazz(IEventListener<?> eventListener) throws Exception {
        return eventListener.getClass().getMethod("onCall", Event.class).getParameters()[0].getType();
    }

    public void clearAll() {
        this.registeredListeners.clear();
    }
}

