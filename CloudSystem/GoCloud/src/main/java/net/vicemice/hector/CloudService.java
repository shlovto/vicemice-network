package net.vicemice.hector;

import lombok.Data;
import lombok.Getter;
import net.vicemice.hector.manager.ManagerService;
import net.vicemice.hector.nettyserver.NettyService;
import net.vicemice.hector.utils.DatabaseQueue;
import net.vicemice.hector.utils.terminal.Terminal;
import net.vicemice.hector.utils.terminal.log.SystemLogger;
import net.vicemice.hector.packets.PacketHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/*
 * Class created at 19:15 - 31.12.2019
 * Copyright (C) 2016-2019 Minetasia Solutions
 */
@Data
public class CloudService {

    @Getter
    private static CloudService cloudService;
    @Getter
    private static String version = "5.0.4";

    private long startTime;
    private boolean started, devMode, closeLock;
    private Terminal terminal;
    private SystemLogger logger;
    private NettyService nettyService;
    private ManagerService managerService;
    private DatabaseQueue databaseQueue;
    private List<UUID> shorteneds = new ArrayList<>();

    public static boolean newService() {
        CloudService service = new CloudService();
        service.setStarted(false);
        service.setStartTime(System.currentTimeMillis());
        service.setCloseLock(false);
        service.setShorteneds(new ArrayList<>());
        service.setManagerService(ManagerService.newService());

        if (cloudService != null) {
            return false;
        }

        cloudService = service;

        service.setDatabaseQueue(new DatabaseQueue());
        service.setNettyService(NettyService.newService());
        return true;
    }

    public void start() {
        Runtime.getRuntime().addShutdownHook(new Thread(CloudService::shutdown));
        this.getManagerService().run();
    }

    public static void shutdown() {
        if (getCloudService().isCloseLock()) return;

        getCloudService().setCloseLock(true);

        getCloudService().getTerminal().write("§b§lGoNet §cis going to shutdown...");
        getCloudService().getManagerService().getServerManager().handleCloudStop();
        try {
            getCloudService().getManagerService().getMongoManager().getClient().close();
        }
        catch (Exception e) {
            e.printStackTrace();
            getCloudService().getTerminal().writeMessage(e.getLocalizedMessage());
        }
    }
}