package net.vicemice.hector.network.modules.api.event.server;

import io.netty.channel.Channel;
import net.vicemice.hector.utils.event.async.AsyncEvent;
import net.vicemice.hector.utils.event.async.AsyncPosterAdapter;

public class ServerSendPacketEvent extends AsyncEvent<ServerRemoveEvent> {
    private Channel channel;
    private Object object;

    public ServerSendPacketEvent(Channel channel, Object object) {
        super(new AsyncPosterAdapter<>());
        this.channel = channel;
        this.object = object;
    }

    public Channel getChannel() {
        return channel;
    }

    public Object getObject() {
        return object;
    }
}