package net.vicemice.hector.player;

import lombok.Data;
import net.md_5.bungee.api.chat.BaseComponent;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.network.modules.api.event.discord.DiscordCheckGroupsEvent;
import net.vicemice.hector.network.modules.api.event.teamspeak.TeamSpeakCheckGroupsEvent;
import net.vicemice.hector.network.proxy.Proxy;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.labymod.LabyModPacket;
import net.vicemice.hector.packets.types.player.party.PartyPacket;
import net.vicemice.hector.packets.types.player.punish.KickPacket;
import net.vicemice.hector.packets.types.player.replay.PacketReplayEntries;
import net.vicemice.hector.packets.types.player.settings.SettingsPacket;
import net.vicemice.hector.packets.types.player.user.UserPreLoginPacket;
import net.vicemice.hector.packets.types.player.user.UserUpdatePacket;
import net.vicemice.hector.packets.types.proxy.ProxyRankPacket;
import net.vicemice.hector.packets.types.player.rank.PlayerRankPacket;
import net.vicemice.hector.player.clan.Clan;
import net.vicemice.hector.player.nick.NickHistory;
import net.vicemice.hector.player.party.Party;
import net.vicemice.hector.player.session.Session;
import net.vicemice.hector.player.utils.*;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.*;
import net.vicemice.hector.utils.language.Language;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.utils.players.FriendData;
import net.vicemice.hector.utils.players.NetworkPlayer;
import net.vicemice.hector.utils.players.clan.ClanRank;
import net.vicemice.hector.utils.players.lobby.LobbyFriendData;
import net.vicemice.hector.utils.players.lobby.LobbyRequestData;
import net.vicemice.hector.packets.types.player.*;
import net.vicemice.hector.utils.players.party.PartyRank;
import net.vicemice.hector.utils.players.utils.DataUpdateType;
import net.vicemice.hector.utils.players.utils.PlayerState;
import net.vicemice.hector.utils.players.utils.Sound;
import net.vicemice.hector.utils.scheduler.ScheduledExecution;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.Reader;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/*
 * Class created at 20:21 - 31.12.2019
 * Copyright (C) 2016-2019 Minetasia Solutions
 */
@Data
public class CloudPlayer implements Comparable<CloudPlayer>, SaveEntry {
    private ObjectId objectId;
    private int id;
    private boolean afk = false, fullLoaded = false;
    private UUID uniqueId;
    private long coins;
    private Session session;
    private String country = "undefined";
    private UUID editReport = null;
    private Language.LanguageType language;
    private Locale locale;

    private String name;
    private String ip;

    private String teamSpeakId = "noid", discordId = "noid";
    private Integer forumId = -1;

    private String skinValue;
    private String skinSignature;
    private Proxy proxy;
    private Rank rank;
    private long rankEnd;
    private Server server, lastServer, lastSendedServer, privateServer = null;
    private String watchingReplay = null, currentGameId = null;
    private long lastLogin, lastLogout, firstLogin;
    private Map<UUID, FriendData> friends = new ConcurrentHashMap<>(), requests = new ConcurrentHashMap<>();
    private int version = 0;
    private double locX = -3.5, locY = 14.0, locZ = 48.5;
    private float yaw = -64.5f, pitch = 0.0f;
    private long playTime;
    private long chatLogTimeout = -1, reportTimeout = -1, afkTimeout = -1, forumTimeout = -1;
    private String permissions;
    private CloudPlayer lastMessageReceiver;
    private ConcurrentHashMap<String, String> playerDB = new ConcurrentHashMap<>();
    private boolean notify = true;
    private UUID joinSecret, specSecret, matchSecret = null;

    private long reportsProcessed = 0;
    private UUID reportProcessed = null;

    private Level level;
    private Status status;
    private Stats stats;
    private Abuses abuses;
    private SettingsPacket settingsPacket;
    private Party party;
    //TODO: Update to new Clan System
    private Clan clan;
    private MapEditInfo mapEditInfo;

    private PlayerState playerState = PlayerState.OFFLINE;

    private String nickName;
    private long nickTimeout;
    private PacketHolder nickPacket;
    private NickHistory nickHistory;

    private ScheduledExecution scheduledExecution, scheduledExecution2;

    public void createJoinSecret() {
        this.setJoinSecret(UUID.randomUUID());
    }

    public void createSpecSecret() {
        this.setSpecSecret(UUID.randomUUID());
    }

    public void createMatchSecret() {
        this.setMatchSecret(UUID.randomUUID());
    }

    public void login() {
        this.playerState = PlayerState.ONLINE;

        if (reportsProcessed == 1) {
            CloudPlayer reported = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(reportProcessed);
            this.sendMessage("report-processed-offline-single", reported.getRank().getRankColor()+reported.getName());
            this.reportsProcessed = 0;
            this.reportProcessed = null;
        } else if (reportsProcessed > 1) {
            this.sendMessage("report-processed-offline", reportsProcessed);
            this.reportsProcessed = 0;
            this.reportProcessed = null;
        }

        scheduledExecution2 = new ScheduledExecution(() -> {
            if (this.getLevel().getExperience() >= this.getLevel().calculateNextLevel()) {
                this.getLevel().setLevel((this.getLevel().getLevel()+1));

                this.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_SOUND_PACKET, new PlayerSoundPacket(this.getUniqueId(), Sound.LEVEL_UP)));
                this.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_TITLE_PACKET, new PlayerTitlePacket(this.getUniqueId(), "§7Level §eUp", this.getLanguageMessage("level-up", this.getLevel().getLevel()), 0, 4, 0)));
                this.sendData(this.getServer(), DataUpdateType.UPDATE_LEVEL, true);
                this.updateToDatabase(false);
            }
        }, 0, 1, TimeUnit.SECONDS);
        scheduledExecution = new ScheduledExecution(() -> {
            CloudService.getCloudService().getManagerService().getStatsManager().addPlayerStat(this, "onlineTime", 1);
            if (this.isAfk()) return;
            if (this.getPlayerState() != PlayerState.INGAME) return;
            if (this.getServer().getServerState() != ServerState.INGAME) return;
            if (this.getServer().getServerTemplate() != null && this.getServer().getServerTemplate().isPlayAble()) return;
            CloudService.getCloudService().getManagerService().getStatsManager().addPlayerStat(this, "playTime", 1);
            this.playTime += 1;
        }, 0, 1, TimeUnit.MINUTES);
    }

    public void logout() {
        this.playerState = PlayerState.OFFLINE;
        if (scheduledExecution2 != null) scheduledExecution2.shutdown();
        if (scheduledExecution != null) scheduledExecution.shutdown();
    }

    public CloudPlayer(UUID uniqueId, String name, String language, String locale, String ip, Rank rank, long lastLogin, long firstLogin, long rankEnd, Map<UUID, FriendData> friends, Map<UUID, FriendData> requests, long coins, String permissions) {
        this.createJoinSecret();

        this.uniqueId = uniqueId;
        this.name = name;
        if (language.equalsIgnoreCase("german")) {
            this.language = Language.LanguageType.GERMAN;
        } else {
            this.language = Language.LanguageType.ENGLISH;
        }
        this.locale = Locale.forLanguageTag(locale);
        this.ip = ip;
        this.rank = rank;
        this.lastLogin = lastLogin;
        this.firstLogin = firstLogin;
        this.rankEnd = rankEnd;
        this.proxy = null;
        this.coins = coins;
        this.permissions = permissions;
        if (permissions == null) {
            this.permissions = "";
        }
        if (friends != null) {
            this.friends.putAll(new HashMap<>(friends));
        }
        if (requests != null) {
            this.requests.putAll(new HashMap<>(requests));
        }
        this.setAbuses(new Abuses());
        this.setStats(new Stats());
    }

    public void changeLocale(Locale locale) {
        this.setLocale(locale);
        this.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.LANGUAGE_PACKET, new LanguagePacket(this.getUniqueId(), this.getLanguage())));
        this.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.LANGUAGE_PACKET, new LanguagePacket(this.getUniqueId(), this.getLanguage())));
        this.updateToDatabase(false);
    }

    public void addCoins(long coins) {
        this.coins += coins;
        /*
        if (this.getLanguage() == Language.LanguageType.GERMAN) {
            this.sendMessage(CloudServer.getPrefix() + "§7Du hast §6" + coins + " Coins §7erhalten.");
        } else {
            this.sendMessage(CloudServer.getPrefix() + "§7You received §6" + coins + " Coins§7.");
        }
         */
        this.sendData(this.getServer(), DataUpdateType.UPDATE_COINS, true);
        this.updateToDatabase(false);
    }

    public void removeCoins(long coins) {
        long newValue = this.getCoins();
        if ((newValue -= coins) < 0) {
            newValue = 0;
        }
        this.setCoins(newValue);
        this.sendData(this.getServer(), DataUpdateType.UPDATE_COINS, true);
        this.updateToDatabase(false);
    }

    public void joinProxy(Proxy proxy) {
        if (this.getProxy() != null) {
            this.getProxy().getCloudPlayers().remove(this.getUniqueId());
        }
        this.proxy = proxy;
        this.getProxy().getCloudPlayers().add(this.getUniqueId());
        if (this.getPlayTime() >= 15) {
            this.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.ALLOW_CHAT_PACKET, new AllowChatPacket(this.getName())));
        }
    }

    public String getLanguageMessage(String key, Object... objects) {
        return MessageFormat.format(this.getLanguageMessage(key), objects);
    }

    public String getLanguageMessage(String key) {
        return CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager().getMessage(this.getLocale(), key);
    }

    public <T> T getSetting(String key, Class<T> clazz) {
        return clazz.cast(this.getSettingsPacket().getSettings().get(key));
    }

    public void setSetting(String key, Integer value) {
        this.getSettingsPacket().getSettings().remove(key);
        this.getSettingsPacket().getSettings().put(key, value);
    }

    public void setSetting(String key, String value) {
        this.getSettingsPacket().getSettings().remove(key);
        this.getSettingsPacket().getSettings().put(key, value);
    }

    public void switchServer(Server server) {
        this.setLastServer(this.server);
        this.server = server;
    }

    public void editMap(String map, String template) {
        if (this.getServer().getName().startsWith("MapEdit")) {
            this.sendRawMessage("§cYou must left this server to edit a map.");
            return;
        }
        this.setMapEditInfo(new MapEditInfo(map, template));
        for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
            if (!server.getName().startsWith("MapEdit") && server.getServerState() == ServerState.LOBBY) continue;
            if (server.getName().startsWith("MapEdit") && server.getServerState() != ServerState.LOBBY) continue;
            this.connectToServer(server.getName());
            break;
        }
        if (this.lastSendedServer != null && !this.lastSendedServer.getName().startsWith("MapEdit")) {
            this.sendRawMessage("§cA error occurred while sending you to a mapedit server");
        }
    }

    public void watchReplay(String replay) {
        setWatchingReplay(replay);

        for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
            if (!server.getName().startsWith("Replay") && server.getServerState() == ServerState.LOBBY) continue;
            if (server.getName().startsWith("Replay") && server.getServerState() != ServerState.LOBBY) continue;
            this.connectToServer(server.getName());
            break;
        }
        if (this.lastSendedServer != null && this.lastSendedServer.getName().startsWith("Replay")) {
            setWatchingReplay(replay);

            if (CloudService.getCloudService().isDevMode()) {
                CloudService.getCloudService().getTerminal().writeMessage(this.getName() + " will watch the replay " + replay);
            }
        } else {
            this.sendMessage("replay-server-error");
        }
    }

    public void sendRawMessage(String message) {
        this.sendMessages(new String[]{message.replace("&", "§")
                .replace("%prefix%", this.getLanguageMessage( "prefix"))
                .replace("%forum_prefix%", this.getLanguageMessage( "forum-prefix"))
                .replace("%cloud_prefix%", this.getLanguageMessage( "cloud-prefix"))
                .replace("%team_prefix%", this.getLanguageMessage( "team-prefix"))
                .replace("%friend_prefix%", this.getLanguageMessage( "friend-prefix"))
                .replace("%party_prefix%", this.getLanguageMessage( "party-prefix"))
                .replace("%clan_prefix%", this.getLanguageMessage( "clan-prefix"))
                .replace("%verify_prefix%", this.getLanguageMessage( "verify-prefix"))
                .replace("%nick_prefix%", this.getLanguageMessage( "nick-prefix"))
                .replace("%announcement_prefix%", this.getLanguageMessage( "announcement-prefix"))
                .replace("%report_prefix%", this.getLanguageMessage( "report-prefix"))
                .replace("%replay_prefix%", this.getLanguageMessage( "replay-prefix"))});
    }

    public void sendMessage(String string) {
        String message = this.getLanguageMessage(string).replace("&", "§");
        this.sendMessages(new String[]{message});
    }

    public void sendMessage(String string, Object... objects) {
        /*List<Object> objects1 = new ArrayList<>();
        for (Object object : objects) {
            if (CloudService.getCloudService().isDevMode()) CloudService.getCloudService().getTerminal().write("Try to replace: " + object);
            if (objects1.contains(object)) {
                if (CloudService.getCloudService().isDevMode()) CloudService.getCloudService().getTerminal().write("Tring to add: " + object);
                continue;
            }
            objects1.add(object);
        }*/
        String message = MessageFormat.format(this.getLanguageMessage(string).replace("&", "§"), objects);
        this.sendMessages(new String[]{message});
    }

    @Deprecated
    public void sendMessage(BaseComponent[] baseComponents) {
        //TODO: Function
        throw new UnsupportedOperationException("Not available");
    }

    public void sendMessage(ArrayList<String[]> messageData) {
        this.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMPONENT_MESSAGE_PACKET, new ComponentMessagePacket(this.getName(), messageData)));
    }

    public void sendMessages(String[] messages) {
        if (this.proxy != null) {
            this.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.MESSAGE_PACKET, new MessagePacket(this.getUniqueId(), messages)));
        }
    }

    public void updateFriendInv() {
        this.sendData(this.getServer(), DataUpdateType.UPDATE_FRIENDS, true);
    }

    public void connectToServer(String serverName) {
        if (this.proxy != null) {
            Server server = CloudService.getCloudService().getManagerService().getServerManager().getServerByName(serverName);
            if (server == null) {
                this.sendMessage("server-not-exist");
                return;
            }

            if (server.getServerTemplate() == null || server.getServerTemplate() != null && !server.getServerTemplate().getName().equalsIgnoreCase("Lobby")) {
                if (!this.getSetting("accept-tos", Boolean.class)) {
                    this.sendMessage("you-must-accept-tos");
                    ArrayList<String[]> messageData = new ArrayList<>();
                    messageData.add(new String[]{this.getLanguageMessage("click-to-accept-terms"), "true", "run_command", "/agree"});
                    ComponentMessagePacket componentMessagePacket = new ComponentMessagePacket(this.getName(), messageData);
                    this.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMPONENT_MESSAGE_PACKET, componentMessagePacket));
                    return;
                }
            }

            if (server.isCloudServer()) {
                if (!server.getTemplateName().equalsIgnoreCase("replay")) {
                    this.setWatchingReplay(null);
                }
                if (!server.getTemplateName().equalsIgnoreCase("mapedit")) {
                    this.setMapEditInfo(null);
                }
            }

            if (this.getVersion() < server.getVersion() && this.getVersion() >= 47) {
                CloudService.getCloudService().getTerminal().writeMessage("§7Player " + this.getName() + " joined on the Server " + serverName + " with the Version " + VersionUtil.fromVersionID(this.getVersion()) + " (ProtocolId: " + this.getVersion() + ")");
                this.sendMessage("outdated-version", VersionUtil.fromVersionID(server.getVersion()));
                return;
            }

            Rank rank = Rank.fromString(server.getRank());

            if (!this.getRank().isHigherEqualsLevel(rank)) {
                this.sendMessage("rank-required", rank.getRankColor() + rank.getRankName());
                return;
            }

            if (server.getOnlinePlayers() >= server.getMaxPlayers() && server.getServerState() == ServerState.LOBBY && !this.getRank().isHigherEqualsLevel(Rank.ADMIN)) {
                this.sendMessage("server-full");
                return;
            }

            if (this.getParty() != null && this.getParty().getLeader() == this) {
                this.getParty().handleServerSwitch(server);
            }

            if (this.getSetting("auto-nick", Integer.class) == 1 && !this.getRank().isHigherEqualsLevel(Rank.VIP)) {
                this.setSetting("auto-nick", 0);
            }

            if (server.isNickAllowed() && this.getNickName() == null && this.getSetting("auto-nick", Integer.class) == 1) {
                String nickName = CloudService.getCloudService().getManagerService().getNickNameManager().getNickName();
                String[] texture = CloudService.getCloudService().getManagerService().getNickNameManager().getTextureData();
                if (texture == null) {
                    this.sendMessage("nick-no-skin-found");
                    texture = new String[]{"eyJ0aW1lc3RhbXAiOjE0NDg2MzQ1ODcyNDIsInByb2ZpbGVJZCI6IjYwNmUyZmYwZWQ3NzQ4NDI5ZDZjZTFkMzMyMWM3ODM4IiwicHJvZmlsZU5hbWUiOiJNSEZfUXVlc3Rpb24iLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzUxNjNkYWZhYzFkOTFhOGM5MWRiNTc2Y2FhYzc4NDMzNjc5MWE2ZTE4ZDhmN2Y2Mjc3OGZjNDdiZjE0NmI2In19fQ==", "eosGuYbzqNK8UBllFajP+cVBbak0Qnqt2NafONsp7U18WIlVajvoKl0ulnEYlAPXKmV4X2B4XF0KklKHT4jwf4sa1HNLyJfYu7lyxJPKGFrYpdGEL2LAiMRKrP7U/b2ZmQHQQ8cKmPScsf/ltLggG9J0mQBxrd4VuoSIje21Y7QxXOMU80QdGyG8MlBqoEM15zUYlsYp2+nTpK0SqaRdJLymdHK9rdShl+hAgti8FKrRgS5Sr/OjICz6QXSLUrvpis7HoZk47GL/D+SmNNmtv+pT95UZz0ObYP3iEFer7ZHsKLS4d6qGe8qcyu+CLJ3cvFf66kuqOc8YbPjzHETVT9KrZkDRkFWoyL4jgcjFMc2l70wLL4Uhz94Rb7flBVeT4BXCoXjn9hXT2oZbRdtQsXMG87Bik0MGeqoQgtqaeCi8uwOeW2kbjl4eD7iley9I4axq+J74tYZL9+734fLdK0LMLZ2ncy3vGc4Kj2NFoKSttFyA3Vei6YeUg6b2hTiDS+meVsdpdO6NkQVWG3dlpENZk0zm+p3U33KMNWcjAxXzsWmw+a68ktUjuAbj1oZniVkPI6VPMm21/a1N92mVBKFQMX2rj1oiUIByIZe/iHPQe3luRiRBLIUwU1L+0LEzdPpq0eM2R6PQj8GNz/fxbpTCy+qcf1Pe9/4HoDGLxE8="};
                }
                if (nickName != null) {
                    CloudService.getCloudService().getManagerService().getNickNameManager().nickPlayer(this, nickName, texture, server);
                    MessageGroup messageGroup = new MessageGroup(this);

                    this.sendMessage("nick-playing-as", nickName);

                    messageGroup.send();
                    NickHistory nickHistory = new NickHistory(nickName, this.getName());
                    this.setNickHistory(nickHistory);
                    CloudService.getCloudService().getManagerService().getNickNameManager().getNickHistories().add(nickHistory);
                } else {
                    this.sendMessage("no-nick-found");
                }
            } else if (!server.isNickAllowed() && this.getNickName() != null && this.getSetting("auto-nick", Integer.class) == 1 || !server.isNickAllowed() && this.getNickName() != null && this.getSetting("auto-nick", Integer.class) != 1) {
                CloudService.getCloudService().getManagerService().getNickNameManager().unnickPlayer(this);
            }

            if (getServer() != null && getServer().isPrivateServer() && getServer().getOnlinePlayers() == 0) {
                getServer().remove();
            }

            net.vicemice.hector.utils.players.party.Party party = null;
            net.vicemice.hector.utils.players.clan.Clan clan = null;

            if (this.getParty() != null) {
                Map<PlayerInfo, PartyRank> members = new HashMap<>();
                this.getParty().getMembers().forEach(player -> {
                    members.put(new PlayerInfo(player.getName(), player.getUniqueId(), player.getRank(), player.getSkinSignature(), player.getSkinValue(), player.isOnline()), (this.getParty().getLeader().equals(player) ? PartyRank.LEADER : PartyRank.MEMBER));
                });
                party = new net.vicemice.hector.utils.players.party.Party(this.getParty().getId(), members);
            }

            if (this.getClan() != null) {
                Map<PlayerInfo, ClanRank> members = new HashMap<>();
                this.getClan().getMembers().forEach((uuid, clanRank) -> {
                    CloudPlayer player = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uuid);
                    members.put(new PlayerInfo(player.getName(), player.getUniqueId(), player.getRank(), player.getSkinSignature(), player.getSkinValue(), player.isOnline()), clanRank);
                });
                clan = new net.vicemice.hector.utils.players.clan.Clan(this.getClan().getId(), this.getClan().getUniqueId(), this.getClan().getName(), this.getClan().getTag(), members);
            }

            server.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.USER_SERVER_PRE_CONNECT, new UserPreLoginPacket(this.getName(), this.getUniqueId(), this.getRank(), this.getLocale(), this.getCoins(), this.getLevel().getExperience(), party, clan)));

            setLastServer(getServer());
            this.lastSendedServer = server;
            server.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_RANK_PACKET, new PlayerRankPacket(this.getName().toLowerCase(), this.getUniqueId(), this.getRank(), this.getPlayerPermissions(server))));
            this.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.SERVER_CONNECT, new ConnectPacket(this.getName(), serverName)));
            this.sendData(server, DataUpdateType.JOIN, false);

            if (server.isNickAllowed()) {
                createSpecSecret();
                createMatchSecret();
            } else {
                setMatchSecret(null);
                setSpecSecret(null);
            }

            if (this.getParty() != null) {
                PartyPacket packet = new PartyPacket(this.getParty().getId(), this.getParty().getMembers().size(), this.getParty().getMax());
                this.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.LABYMOD_PACKET, new LabyModPacket(this.getUniqueId(), true, packet, this.getJoinSecret(), this.getMatchSecret(), this.getSpecSecret())));
            } else {
                this.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.LABYMOD_PACKET, new LabyModPacket(this.getUniqueId(), false, null, this.getJoinSecret(), this.getMatchSecret(), this.getSpecSecret())));
            }

            server.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.LANGUAGE_PACKET, new LanguagePacket(this.getUniqueId(), this.getLanguage())));

            if (server.getGameID() != null) {
                this.setCurrentGameId(server.getGameID());
            } else {
                this.setCurrentGameId(null);
            }

            this.playerState = PlayerState.ONLINE;

            CloudService.getCloudService().getManagerService().getFriendManager().handleServerSwitch(this);

            //getLastServer().removePlayer(this.getName());
            //server.addPlayer(this.getName());
        }
    }

    public void removeNickHistory() {
        NickHistory nickHistory = this.nickHistory;
        nickHistory.setEnded(System.currentTimeMillis());
        CloudService.getCloudService().getManagerService().getNickNameManager().getNickHistories().remove(this.nickHistory);
        CloudService.getCloudService().getManagerService().getNickNameManager().getNickHistories().add(nickHistory);
        CloudService.getCloudService().getManagerService().getNickNameManager().addToDatabase(nickHistory);
        this.nickHistory = null;
    }

    public void sendData(Server server, DataUpdateType dataUpdateType, boolean update) {
        if (server == null) return;
        if (!this.isOnline()) return;

        UUID uniqueId = this.getUniqueId();
        long coins = this.getCoins();
        long playTime = this.getPlayTime();
        Rank rank = this.getRank();
        String message = this.getStatus().getMessage();
        long set = this.getStatus().getSet();
        long ban = this.getStatus().getBan();

        List<LobbyFriendData> friends = CloudService.getCloudService().getManagerService().getFriendManager().getFriendsArray(this);
        List<LobbyRequestData> requests = CloudService.getCloudService().getManagerService().getFriendManager().getRequestArrays(this);

        SettingsPacket settingsPacket = this.getSettingsPacket();

        ConcurrentHashMap<StatisticPeriod, ConcurrentHashMap<String, Long>> statistics = new ConcurrentHashMap<>();
        statistics.put(StatisticPeriod.GLOBAL, this.getStats().getGlobalStats());
        statistics.put(StatisticPeriod.MONTH, this.getStats().getMonthlyStats());
        statistics.put(StatisticPeriod.DAY, this.getStats().getDailyStats());

        ConcurrentHashMap<StatisticPeriod, ConcurrentHashMap<String, Long>> ranking = new ConcurrentHashMap<>();
        ranking.put(StatisticPeriod.GLOBAL, this.getStats().getGlobalRanking());
        ranking.put(StatisticPeriod.MONTH, this.getStats().getMonthlyRanking());
        ranking.put(StatisticPeriod.DAY, this.getStats().getDailyRanking());

        List<PacketReplayEntries.PacketReplayEntry> savedEntries = CloudService.getCloudService().getManagerService().getReplayHistoryManager().getSavedEntries(this.getUniqueId());
        List<PacketReplayEntries.PacketReplayEntry> recentEntries = CloudService.getCloudService().getManagerService().getReplayHistoryManager().getRecentEntries(this.getUniqueId());
        server.sendPacket(GoAPI.getPacketHelper().preparePacket((update?PacketType.UPDATE_PLAYER_SERVER_DATA:PacketType.PLAYER_SERVER_DATA), new PlayerServerDataPacket(new NetworkPlayer(uniqueId, coins, playTime, rank, locale, message, set, ban, friends, requests, settingsPacket, statistics, ranking, savedEntries, recentEntries), dataUpdateType, System.currentTimeMillis())));

        if (update) {
            net.vicemice.hector.utils.players.party.Party party = null;
            net.vicemice.hector.utils.players.clan.Clan clan = null;

            if (this.getParty() != null) {
                Map<PlayerInfo, PartyRank> members = new HashMap<>();
                this.getParty().getMembers().forEach(player -> {
                    members.put(new PlayerInfo(player.getName(), player.getUniqueId(), player.getRank(), player.getSkinSignature(), player.getSkinValue(), player.isOnline()), (this.getParty().getLeader().equals(player) ? PartyRank.LEADER : PartyRank.MEMBER));
                });
                party = new net.vicemice.hector.utils.players.party.Party(this.getParty().getId(), members);
            }

            if (this.getClan() != null) {
                Map<PlayerInfo, ClanRank> members = new HashMap<>();
                this.getClan().getMembers().forEach((uuid, clanRank) -> {
                    CloudPlayer player = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uuid);
                    members.put(new PlayerInfo(player.getName(), player.getUniqueId(), player.getRank(), player.getSkinSignature(), player.getSkinValue(), player.isOnline()), clanRank);
                });
                clan = new net.vicemice.hector.utils.players.clan.Clan(this.getClan().getId(), this.getClan().getUniqueId(), this.getClan().getName(), this.getClan().getTag(), members);
            }

            server.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.USER_SERVER_UPDATE, new UserUpdatePacket(this.getName(), this.getUniqueId(), this.getRank(), this.getLocale(), this.getCoins(), this.getLevel().getExperience(), party, clan)));
        }
    }

    public void kickPlayer(String string) {
        if (this.proxy != null) {
            this.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.KICK_PACKET, new KickPacket(this.getUniqueId(), string)));
        }
    }

    public boolean isOnline() {
        if (this.getProxy() != null) {
            return true;
        }
        return false;
    }

    public List<String> getPlayerPermissions(Server server) {
        List<String> permissions = new ArrayList<>();
        if (getServer() != null) {
            if (CloudService.getCloudService().getManagerService().getPermissionManager().getPermissionsMap().containsKey(getRank().getRankName().toLowerCase())) {
                for (String[] permissionArray : CloudService.getCloudService().getManagerService().getPermissionManager().getPermissionsMap().get(getRank().getRankName().toLowerCase())) {
                    String permission = permissionArray[0];
                    String serverGroup = permissionArray[1];
                    if (serverGroup == null || (server.getServerGroup() != null && serverGroup.equalsIgnoreCase(server.getServerGroup()))) {
                        permissions.add(permission);
                    }
                }
            }
            if (CloudService.getCloudService().getManagerService().getPermissionManager().getPermissionsMap().containsKey(Rank.MEMBER.getRankName().toLowerCase()) && getRank() != Rank.ADMIN) {
                for (String[] permissionArray : CloudService.getCloudService().getManagerService().getPermissionManager().getPermissionsMap().get(Rank.MEMBER.getRankName().toLowerCase())) {
                    String permission = permissionArray[0];
                    String serverGroup = permissionArray[1];
                    if (serverGroup == null || (server.getServerGroup() != null && serverGroup.equalsIgnoreCase(server.getServerGroup()))) {
                        permissions.add(permission);
                    }
                }
            }
            if (!getPermissions().isEmpty()) {
                for (String permissionData : getPermissions().split("@")) {
                    String[] splitData = permissionData.split(";");
                    String permission = splitData[0];
                    String serverGroup = splitData[1];
                    if (serverGroup.equalsIgnoreCase("nogroup") || (server.getServerGroup() != null && serverGroup.equalsIgnoreCase(server.getServerGroup()))) {
                        permissions.add(permission);
                    }
                }
            }
        }
        return permissions;
    }

    public void setRank(Rank rank, long rankEnd) {
        Rank oldRank = this.getRank();
        this.rank = rank;
        this.rankEnd = rankEnd;
        if (this.getServer() != null) {
            List<String> permissions = this.getPlayerPermissions(this.server);
            this.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_RANK_PACKET, new PlayerRankPacket(this.getName(), this.getUniqueId(), this.getRank(), permissions)));
        }
        if (rank == Rank.MEMBER) {
            CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithRank().remove(this.getUniqueId());
        } else if (!CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithRank().contains(this.getUniqueId())) {
            CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithRank().add(this.getUniqueId());
        }
        ArrayList<String[]> rankData = new ArrayList<String[]>();
        rankData.add(new String[]{String.valueOf(this.getUniqueId()), this.getRank().name().toUpperCase()});
        PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_RANK_PACKET, new ProxyRankPacket(rankData));
        for (Proxy proxy : CloudService.getCloudService().getManagerService().getProxyManager().getProxies()) {
            proxy.sendPacket(initPacket);
        }

        if (this.getSetting("auto-nick", Integer.class) == 1 && !this.getRank().isHigherEqualsLevel(Rank.VIP)) {
            this.setSetting("auto-nick", 0);
        }

        this.updateToDatabase(false);
        this.sendData(this.getServer(), DataUpdateType.UPDATE_RANK, true);

        checkDiscordGroups();
        checkTeamSpeakGroups();

        if (this.getForumId() != -1) {
            GoAPI.getForumAPI().changeRank(this.getForumId(), this.getRank(), o -> {
                //CloudService.getCloudService().getTerminal().writeMessage("Update Page-Rank for " + this.getName());
            });
        }
    }

    private static String readAll(Reader rd) throws IOException {
        int cp;
        StringBuilder sb = new StringBuilder();
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public boolean addPermission(String permission, String serverGroup) {
        String permissionData = null;
        permissionData = serverGroup != null ? String.valueOf(permission.toLowerCase()) + ";" + serverGroup.toLowerCase() : String.valueOf(permission.toLowerCase()) + ";" + "nogroup";
        if (!this.getPermissions().contains(permissionData)) {
            this.permissions = this.getPermissions().isEmpty() ? String.valueOf(this.permissions) + permissionData : String.valueOf(this.permissions) + "@" + permissionData;
            if (this.getServer() != null) {
                this.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_RANK_PACKET, new PlayerRankPacket(this.getName(), this.getUniqueId(), this.getRank(), this.getPlayerPermissions(this.getServer()))));
            }
            this.updateToDatabase(false);
            return true;
        }
        return false;
    }

    public void removePermission(String permission, String serverGroup) {
        String removeData = serverGroup != null ? String.valueOf(permission.toLowerCase()) + ";" + serverGroup.toLowerCase() : String.valueOf(permission.toLowerCase()) + ";" + "nogroup";
        String newData = "";
        String[] arrstring = this.getPermissions().split("@");
        int n = arrstring.length;
        int n2 = 0;
        while (n2 < n) {
            String permissionData = arrstring[n2];
            if (!permissionData.equalsIgnoreCase(removeData)) {
                newData = newData.isEmpty() ? String.valueOf(newData) + permissionData : String.valueOf(newData) + "@" + permissionData;
            }
            ++n2;
        }
        this.permissions = newData;
        if (this.getServer() != null) {
            this.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_RANK_PACKET, new PlayerRankPacket(this.getName(), this.getUniqueId(), this.getRank(), this.getPlayerPermissions(this.getServer()))));
        }
        this.updateToDatabase(false);
    }

    public void checkTeamSpeakGroups() {
        CloudService.getCloudService().getManagerService().getEventManager().callEvent(new TeamSpeakCheckGroupsEvent(this));
    }

    public void checkDiscordGroups() {
        CloudService.getCloudService().getManagerService().getEventManager().callEvent(new DiscordCheckGroupsEvent(this));
    }

    public String statsToString(ConcurrentHashMap<String, Long> statsMap) {
        String saveString = "";
        int current = 0;
        for (String saveKey : statsMap.keySet()) {
            long value = statsMap.get(saveKey);
            saveString = String.valueOf(saveString) + saveKey + ";" + value;
            if (current != statsMap.size() - 1) {
                saveString = String.valueOf(saveString) + "!";
            }
            ++current;
        }
        return saveString;
    }

    public void statsFromString(String statsString, ConcurrentHashMap destination) {
        if (statsString.contains("!")) {
            String[] data;
            String[] arrstring = data = statsString.split("!");
            int n = arrstring.length;
            int n2 = 0;
            while (n2 < n) {
                String getData = arrstring[n2];
                String[] statsData = getData.split(";");
                destination.put(statsData[0], Long.valueOf(statsData[1]));
                ++n2;
            }
        } else {
            String[] data = statsString.split(";");
            destination.put(data[0], Long.valueOf(data[1]));
        }
    }

    public void updateToDatabase(boolean insert) {
        if (!this.fullLoaded) {
            CloudService.getCloudService().getTerminal().writeMessage(this.name + " is not full loaded!");
            return;
        }
        CloudService.getCloudService().getDatabaseQueue().addToQueue(this, insert);
    }

    public void saveSession() {
        try {
            Document document = new Document("uuid", String.valueOf(this.getUniqueId()));
            document.append("start", this.getSession().getStart());
            document.append("end", this.getSession().getEnd());
            document.append("time", this.getSession().getTime());
            document.append("ip", this.getSession().getIp());
            document.append("version", this.getSession().getVersion());

            CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("sessions").insertOne(document);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void save(boolean insert) {
        try {
            if (insert) {
                Document playerDocument = new Document("id", (CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().size() + 1));
                playerDocument.append("uniqueId", String.valueOf(this.getUniqueId()));
                playerDocument.append("name", this.getName());
                playerDocument.append("version", this.getVersion());
                playerDocument.append("country", (this.getCountry().equalsIgnoreCase("undefined") ? "DE" : this.getCountry()));
                playerDocument.append("playedTime", this.getPlayTime());
                playerDocument.append("coins", this.getCoins());
                playerDocument.append("ip", this.getIp());
                playerDocument.append("language", this.getLanguage().name().toLowerCase());
                playerDocument.append("locale", this.getLocale().toLanguageTag());
                playerDocument.append("rank", this.getRank().getRankName());

                Document dates = new Document();
                dates.append("rankend", this.getRankEnd());
                dates.append("firstlogin", this.getFirstLogin());
                dates.append("lastlogin", this.getLastLogin());
                dates.append("lastlogout", this.getLastLogout());
                playerDocument.append("dates", dates);

                Document settings = new Document();
                for (String key : this.getSettingsPacket().getSettings().keySet()) {
                    settings.append(key, this.getSetting(key, Object.class));
                }

                playerDocument.append("settings", settings);
                playerDocument.append("status", new Document("status", "NONE").append("time", System.currentTimeMillis()));

                Document abuses = new Document();
                abuses.append("banPoints", this.getAbuses().getBanPoints());
                abuses.append("mutePoints", this.getAbuses().getMutePoints());
                if (this.getAbuses().getBan() != null) {
                    Document abuse = new Document();
                    abuse.append("author", String.valueOf(this.getAbuses().getBan().getAuthor()));
                    abuse.append("reason", this.getAbuses().getBan().getReason());
                    abuse.append("length", this.getAbuses().getBan().getLength());
                    abuse.append("created", this.getAbuses().getBan().getCreated());
                    abuse.append("comment", this.getAbuses().getBan().getComment());
                    abuses.append("ban", abuse);
                }
                if (this.getAbuses().getMute() != null) {
                    Document abuse = new Document();
                    abuse.append("author", String.valueOf(this.getAbuses().getMute().getAuthor()));
                    abuse.append("reason", this.getAbuses().getMute().getReason());
                    abuse.append("length", this.getAbuses().getMute().getLength());
                    abuse.append("created", this.getAbuses().getMute().getCreated());
                    abuse.append("comment", this.getAbuses().getMute().getComment());
                    abuses.append("mute", abuse);
                }
                playerDocument.append("abuses", abuses);
                playerDocument.append("permissions", this.getPermissions());

                List<Document> friends = new ArrayList<>();
                for (FriendData friend : this.getFriends().values()) {
                    Document document1 = new Document();
                    document1.append("uniqueId", String.valueOf(friend.getUniqueId()));
                    document1.append("date", friend.getDate());
                    document1.append("favorite", friend.isFavorite());
                    friends.add(document1);
                }
                playerDocument.append("friends", friends);

                List<Document> requests = new ArrayList<>();
                for (FriendData request : this.getRequests().values()) {
                    Document document1 = new Document();
                    document1.append("uniqueId", String.valueOf(request.getUniqueId()));
                    document1.append("date", request.getDate());
                    friends.add(document1);
                }
                playerDocument.append("requests", requests);

                Document connectedAccounts = new Document();
                connectedAccounts.put("teamspeak", this.getTeamSpeakId());
                connectedAccounts.put("discord", this.getDiscordId());
                if (this.getForumId() == -1) {
                    connectedAccounts.put("forumId", "noid");
                } else {
                    connectedAccounts.put("forumId", this.getForumId());
                }
                playerDocument.append("connectedAccounts", connectedAccounts);

                Document skin = new Document();
                skin.append("value", this.getSkinValue());
                skin.append("signature", this.getSkinSignature());
                playerDocument.append("skin", skin);

                Document location = new Document();
                location.append("locX", this.getLocX());
                location.append("locY", this.getLocY());
                location.append("locZ", this.getLocZ());
                location.append("yaw", this.getYaw());
                location.append("pitch", this.getPitch());
                playerDocument.append("location", location);

                Document level = new Document();
                level.append("level", this.getLevel().getLevel());
                level.append("experience", this.getLevel().getExperience());
                playerDocument.append("level", level);

                CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("players").insertOne(playerDocument);

                return;
            }

            Document playerDocument = new Document();
            playerDocument.append("name", this.getName());
            playerDocument.append("version", this.getVersion());
            playerDocument.append("country", (this.getCountry().equalsIgnoreCase("undefined") ? "DE" : this.getCountry()));
            playerDocument.append("playedTime", this.getPlayTime());
            playerDocument.append("coins", this.getCoins());
            playerDocument.append("ip", this.getIp());
            playerDocument.append("language", this.getLanguage().name().toLowerCase());
            playerDocument.append("locale", this.getLocale().toLanguageTag());
            playerDocument.append("rank", this.getRank().getRankName());

            Document dates = new Document();
            dates.append("rankend", this.getRankEnd());
            dates.append("firstlogin", this.getFirstLogin());
            dates.append("lastlogin", this.getLastLogin());
            dates.append("lastlogout", this.getLastLogout());
            playerDocument.append("dates", dates);

            Document settings = new Document();
            for (String key : this.getSettingsPacket().getSettings().keySet()) {
                settings.append(key, this.getSetting(key, Object.class));
            }

            playerDocument.append("settings", settings);
            playerDocument.append("status", new Document("status", "NONE").append("time", System.currentTimeMillis()));

            Document abuses = new Document();
            abuses.append("banPoints", this.getAbuses().getBanPoints());
            abuses.append("mutePoints", this.getAbuses().getMutePoints());
            if (this.getAbuses().getBan() != null) {
                Document abuse = new Document();
                abuse.append("author", String.valueOf(this.getAbuses().getBan().getAuthor()));
                abuse.append("reason", this.getAbuses().getBan().getReason());
                abuse.append("length", this.getAbuses().getBan().getLength());
                abuse.append("created", this.getAbuses().getBan().getCreated());
                abuse.append("comment", this.getAbuses().getBan().getComment());
                abuses.append("ban", abuse);
            }
            if (this.getAbuses().getMute() != null) {
                Document abuse = new Document();
                abuse.append("author", String.valueOf(this.getAbuses().getMute().getAuthor()));
                abuse.append("reason", this.getAbuses().getMute().getReason());
                abuse.append("length", this.getAbuses().getMute().getLength());
                abuse.append("created", this.getAbuses().getMute().getCreated());
                abuse.append("comment", this.getAbuses().getMute().getComment());
                abuses.append("mute", abuse);
            }
            playerDocument.append("abuses", abuses);

            playerDocument.append("permissions", this.getPermissions());

            List<Document> friends = new ArrayList<>();
            for (FriendData friend : this.getFriends().values()) {
                Document document1 = new Document();
                document1.append("uniqueId", String.valueOf(friend.getUniqueId()));
                document1.append("date", friend.getDate());
                document1.append("favorite", friend.isFavorite());
                friends.add(document1);
            }
            playerDocument.append("friends", friends);

            List<Document> requests = new ArrayList<>();
            for (FriendData request : this.getRequests().values()) {
                Document document1 = new Document();
                document1.append("uniqueId", String.valueOf(request.getUniqueId()));
                document1.append("date", request.getDate());
                friends.add(document1);
            }
            playerDocument.append("requests", requests);

            Document connectedAccounts = new Document();
            connectedAccounts.put("teamspeak", this.getTeamSpeakId());
            connectedAccounts.put("discord", this.getDiscordId());
            if (this.getForumId() == -1) {
                connectedAccounts.put("forumId", "noid");
            } else {
                connectedAccounts.put("forumId", this.getForumId());
            }
            playerDocument.append("connectedAccounts", connectedAccounts);

            Document skin = new Document();
            skin.append("value", this.getSkinValue());
            skin.append("signature", this.getSkinSignature());
            playerDocument.append("skin", skin);

            Document location = new Document();
            location.append("locX", this.getLocX());
            location.append("locY", this.getLocY());
            location.append("locZ", this.getLocZ());
            location.append("yaw", this.getYaw());
            location.append("pitch", this.getPitch());
            playerDocument.append("location", location);

            Document level = new Document();
            level.append("level", this.getLevel().getLevel());
            level.append("experience", this.getLevel().getExperience());
            playerDocument.append("level", level);

            String globalStats = this.statsToString(this.getStats().getGlobalStats());
            String monthlyStats = this.statsToString(this.getStats().getMonthlyStats());
            String dailyStats = this.statsToString(this.getStats().getDailyStats());
            if (!globalStats.equals("")) {
                playerDocument.append("globalStats", globalStats);
            } else {
                playerDocument.append("globalStats", null);
            }
            if (!monthlyStats.equals("")) {
                playerDocument.append("monthlyStats", monthlyStats);
            } else {
                playerDocument.append("monthlyStats", null);
            }
            if (!dailyStats.equals("")) {
                playerDocument.append("dailyStats", dailyStats);
            } else {
                playerDocument.append("dailyStats", null);
            }

            Bson filter = new Document("uniqueId", String.valueOf(this.getUniqueId()));
            CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("players").updateOne(filter, new Document("$set", playerDocument));
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void saveInfo() {
        try {
            Document document = new Document("uniqueId", String.valueOf(this.getUniqueId()));
            document.append("status", this.getStatus());
            document.append("abuses", this.getAbuses());

            CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("players2").insertOne(document);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void removeFriend(CloudPlayer cloudPlayer) {
        this.getFriends().remove(cloudPlayer.getUniqueId());
    }

    public void removeRequest(CloudPlayer cloudPlayer) {
        this.getRequests().remove(cloudPlayer.getUniqueId());
    }

    public boolean isFriend(CloudPlayer cloudPlayer) {
        return this.getFriends().containsKey(cloudPlayer.getUniqueId());
    }

    public boolean hasRequest(CloudPlayer cloudPlayer) {
        return this.getRequests().containsKey(cloudPlayer.getUniqueId());
    }

    @Override
    public int compareTo(@NotNull CloudPlayer o) {
        return o.getName().compareToIgnoreCase(this.getName());
    }
}