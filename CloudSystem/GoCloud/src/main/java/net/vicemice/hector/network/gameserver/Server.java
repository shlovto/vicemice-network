package net.vicemice.hector.network.gameserver;

import io.netty.channel.Channel;
import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.gameserver.lobby.lobbysign.Sign;
import net.vicemice.hector.network.slave.Slave;
import net.vicemice.hector.packets.types.player.ComponentMessagePacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.ServerType;
import net.vicemice.hector.utils.gameid.GameIDGenerator;
import net.vicemice.hector.utils.gameid.GameIDSaver;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.utils.slave.TemplateData;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class Server {
    Slave slave;
    private String name;
    private String host;
    private int port;
    private int version;
    private String rank;
    private ServerType serverType;
    private String message = "Loading...";
    private String templateName;
    private ServerState serverState;
    private ConcurrentHashMap<String, Integer> players = new ConcurrentHashMap<>();
    private long timeout = -1;
    private long startupTimeout = -1;
    private long stateTimeout = -1;
    private Channel channel;
    private Sign sign;
    private String serverGroup;
    private int maxPlayers = 0;
    private String serverID;
    @Getter
    private boolean nickAllowed;
    @Getter
    private boolean privateServer;
    @Getter
    private CloudPlayer privateOwner;
    @Getter
    private UUID gameUID;
    @Getter
    private String gameID;

    public Server(String name, String host, int port, int version, String rank, ServerType serverType, Slave slave, String templateName, ServerState serverState, boolean nickAllowed, boolean privateServer, CloudPlayer privateOwner) {
        this.gameUID = UUID.randomUUID();
        this.name = name;
        this.host = host;
        this.port = port;
        this.version = version;
        this.rank = rank;
        this.serverType = serverType;
        this.slave = slave;
        this.templateName = templateName;
        this.serverState = serverState;
        this.nickAllowed = nickAllowed;
        this.serverID = this.isCloudServer() ? String.valueOf(slave.getName()) + ":" + this.getGameUID() + ":" + name : "Gamehost-1:" + this.getGameUID() + ":" + name;
        this.privateServer = privateServer;
        this.privateOwner = privateOwner;


        if (templateName != null) {
            if (this.getServerTemplate().isPlayAble() && !this.getServerTemplate().isPrivateTemplate()) {
                this.gameID = GameIDGenerator.getGameID();
                GameIDSaver.saveGameID(gameID, templateName);
            } else {
                this.gameID = null;
            }
        } else {
            this.gameID = null;
        }

        if (CloudService.getCloudService().isDevMode() && !privateServer) {
            CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().forEach(e -> {
                CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(e);
                if (!cloudPlayer.getRank().isHigherEqualsLevel(Rank.ADMIN) || !cloudPlayer.isNotify()) return;

                ArrayList<String[]> messageData = new ArrayList<>();
                messageData.add(new String[]{cloudPlayer.getLanguageMessage("cloud-gameserver-added", this.getName()), "true", "run_command", "/server "+this.getName(), "true", "hover_message", "§aClick to connect"});
                ComponentMessagePacket componentMessagePacket = new ComponentMessagePacket(cloudPlayer.getName(), messageData);

                cloudPlayer.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMPONENT_MESSAGE_PACKET, componentMessagePacket));
            });
        }
    }

    public boolean isLobby() {
        if (CloudService.getCloudService().getManagerService().getLobbyManager().getLobbyServers().stream().map(e -> e.getServer()).filter(e -> e == this).count() > 0) {
            return true;
        }
        return false;
    }

    public void sendPacket(PacketHolder initPacket) {
        if (this.getChannel() != null)
            this.getChannel().writeAndFlush(initPacket);
    }

    /*public void addPlayer(String name) {
        if (!onlinePlayers.contains(name)) onlinePlayers.add(name);
    }

    public void removePlayer(String name) {
        if (onlinePlayers.contains(name)) onlinePlayers.remove(name);
    }

    public int getOnlinePlayers() {
        return onlinePlayers.size();
    }*/

    public int getOnlinePlayers() {
        int playerCount = 0;
        for (String proxy : this.players.keySet()) {
            if (proxy == null) continue;
            if (this.players.get(proxy) == null) continue;
            playerCount += this.players.get(proxy);
        }
        return playerCount;
    }

    public boolean isCloudServer() {
        if (this.slave != null && this.templateName != null) {
            return true;
        }
        return false;
    }

    public void remove() {
        new Thread(() -> {
            CloudService.getCloudService().getManagerService().getServerManager().addReservation(this.getName());
            this.setServerState(ServerState.REMOVE);
            Iterator<UUID> playerIterator = CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().iterator();
            while (playerIterator.hasNext()) {
                CloudPlayer next = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(playerIterator.next());
                if (next.getServer() == null || !next.getServer().getName().equalsIgnoreCase(this.getName())) continue;
                CloudService.getCloudService().getManagerService().getLobbyManager().sendToLobby(next.getName());
            }
            if (this.isCloudServer() && this.slave.getCurrentStartingServer() != null && this.slave.getCurrentStartingServer() == this) {
                this.slave.setCurrentStartingServer(null);
            }
            try {
                Thread.sleep(2000);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
                CloudService.getCloudService().getTerminal().writeMessage(e.getLocalizedMessage());
            }
            if (this.isCloudServer()) {
                this.slave.killServer(this.getName(), true);
            } else {
                //this.slave.killServer(this.getName(), false);
            }
            if (this.getServerTemplate() != null && this.getServerTemplate().getName().equalsIgnoreCase("lobby")) {
                CloudService.getCloudService().getManagerService().getLobbyManager().deleteLobby(this.getInstance());
            }
            CloudService.getCloudService().getManagerService().getServerManager().sendServerToBungees(this.getInstance(), PacketType.PROXY_SERVER_REMOVE);
            CloudService.getCloudService().getManagerService().getServerManager().getServers().remove(this);
            if (this.getServerType() == ServerType.STATIC) {
                //TelegramManager.sendMessage("The Server " + this.getName() + " was removed from the Cloud");
            }

            if (this.getPrivateOwner() != null) {
                this.getPrivateOwner().setPrivateServer(null);
            }

            if (CloudService.getCloudService().isDevMode() && !this.isPrivateServer()) {
                CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().sendAdminMessage("cloud-gameserver-removed", this.getName());
            }
        }
        ).start();
    }

    public Server getInstance() {
        return this;
    }

    public TemplateData getServerTemplate() {
        return CloudService.getCloudService().getManagerService().getServerManager().getTemplateByName(this.getTemplateName());
    }

    public Slave getSlave() {
        return this.slave;
    }

    public String getName() {
        return this.name;
    }

    public String getHost() {
        return this.host;
    }

    public int getPort() {
        return this.port;
    }

    public int getVersion() {
        return version;
    }

    public String getRank() {
        return rank;
    }

    public ServerType getServerType() {
        return this.serverType;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTemplateName() {
        return this.templateName;
    }

    public ServerState getServerState() {
        return this.serverState;
    }

    public void setServerState(ServerState serverState) {
        this.serverState = serverState;
    }

    public ConcurrentHashMap<String, Integer> getPlayers() {
        return players;
    }

    public long getTimeout() {
        return this.timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public long getStartupTimeout() {
        return this.startupTimeout;
    }

    public void setStartupTimeout(long startupTimeout) {
        this.startupTimeout = startupTimeout;
    }

    public long getStateTimeout() {
        return this.stateTimeout;
    }

    public void setStateTimeout(long stateTimeout) {
        this.stateTimeout = stateTimeout;
    }

    public Channel getChannel() {
        return this.channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public void setSign(Sign sign) {
        this.sign = sign;
    }

    public Sign getSign() {
        return this.sign;
    }

    public String getServerGroup() {
        return this.serverGroup;
    }

    public void setServerGroup(String serverGroup) {
        this.serverGroup = serverGroup;
    }

    public int getMaxPlayers() {
        return this.maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public String getServerID() {
        return this.serverID;
    }
}