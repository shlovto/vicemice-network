package net.vicemice.hector.utils.terminal;

import java.util.Arrays;
import java.util.Scanner;

/*
 * Class created at 20:19 - 31.12.2019
 * Copyright (C) 2016-2019 Minetasia Solutions
 */
public class VanillaTerminal implements Terminal {
    private Thread readerThread;
    private Scanner reader;

    @Override
    public void install() {
        if (System.console() == null) {
            this.reader = new Scanner(System.in);
        }
        this.readerThread = new Thread(() -> {
            do {
                String line = null;
                line = this.reader != null ? this.reader.nextLine() : System.console().readLine();
                if (line == null || line.isEmpty()) {
                    try {
                        Thread.sleep(50L);
                    }
                    catch (Exception exception) {}
                    continue;
                }
                String command = line.split(" ")[0];
                String[] args = new String[]{};
                if (line.split(" ").length > 1) {
                    args = Arrays.copyOfRange(line.split(" "), 1, line.split(" ").length);
                }
                CommandRegistry.runCommand(command, args, this);
            } while (true);
        });
        this.readerThread.start();
    }

    @Override
    public void uninstall() {
        this.readerThread.stop();
        this.readerThread = null;
    }

    @Override
    public void write(String message) {
        System.out.println(message);
    }

    @Override
    public void writeMessage(String message) {
        System.out.println(message);
    }
}

