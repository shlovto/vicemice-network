package net.vicemice.hector.player.utils;

import com.mongodb.BasicDBObject;
import lombok.Data;
import java.io.Serializable;

/*
 * Class created at 13:58 - 28.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class Status extends BasicDBObject implements Serializable {

    private String message;
    private long set, ban;

    public Status(String message, long set, long ban) {
        this.message = message;
        this.set = set;
        this.ban = ban;
    }
}
