package net.vicemice.hector.manager.player;

import io.netty.channel.Channel;
import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.api.util.GeoIP2API;
import net.vicemice.hector.manager.player.check.TimerCheck;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.network.modules.api.event.player.LoginEvent;
import net.vicemice.hector.network.modules.api.event.player.LogoutEvent;
import net.vicemice.hector.network.proxy.Proxy;
import net.vicemice.hector.packets.types.player.ComponentMessagePacket;
import net.vicemice.hector.packets.types.player.PlayerLoginPacket;
import net.vicemice.hector.packets.types.player.punish.KickPacket;
import net.vicemice.hector.packets.types.player.report.ReportPlayersPacket;
import net.vicemice.hector.packets.types.player.settings.SettingsPacket;
import net.vicemice.hector.packets.types.server.SpigotServerStop;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.PlayerAPI;
import net.vicemice.hector.player.beforejoined.BeforePlayer;
import net.vicemice.hector.player.session.Session;
import net.vicemice.hector.player.utils.ASNetwork;
import net.vicemice.hector.player.utils.Abuses;
import net.vicemice.hector.player.utils.Level;
import net.vicemice.hector.player.utils.Status;
import net.vicemice.hector.utils.language.Language;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.utils.players.FriendData;
import net.vicemice.hector.utils.PunishReason;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.players.punish.Punish;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

/*
 * Class created at 19:36 - 01.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerManager {
    private final ConcurrentHashMap<String, CloudPlayer> nameCloudPlayers = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<UUID, CloudPlayer> uniqueIdPlayers = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, String> teamSpeakUniqueIds = new ConcurrentHashMap<>();
    @Getter
    private final ConcurrentHashMap<String, Integer> ipCloudPlayers = new ConcurrentHashMap<>();
    @Getter
    private final ConcurrentHashMap<String, Long> ipBanned = new ConcurrentHashMap<>();
    private final List<UUID> onlineCloudPlayers = new CopyOnWriteArrayList<>(), playersWithRank = new CopyOnWriteArrayList<>(), playersWithBan = new CopyOnWriteArrayList<>(), playersWithMute = new CopyOnWriteArrayList<>(), loggedInUsers = new CopyOnWriteArrayList<>();
    public long lastCheck = 0;
    private int joinProcesses;
    @Getter
    private PlayerAPI playerAPI;

    public void init() {
        playerAPI = new PlayerAPI();
        CloudService.getCloudService().getManagerService().getAbuseManager().setBanId(new HashMap<>());
        CloudService.getCloudService().getManagerService().getAbuseManager().loadBanIDs();
        new Timer().scheduleAtFixedRate(new TimerCheck(), 1000, 1000);
    }

    public void loadAllCloudPlayersFromDatabase() {
        ArrayList<Document> list = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("players");
        int count = 0;

        for (Document document : list) {
            count++;
            ObjectId objectId = document.getObjectId("_id");
            int id = document.getInteger("id");
            UUID uuid = UUID.fromString(document.getString("uniqueId"));
            String name = document.getString("name");
            int version = document.getInteger("version");
            String country = document.getString("country");

            /*JSONUtil jsonUtil = new JSONUtil(JSONUtil.API.MCSTATS, uuid.toString());

            boolean save = false;

            if (!uuid.toString().replace("-", "").equalsIgnoreCase("336991c81199457c80bb649042a489f5")) {
                try {
                    String checkName = jsonUtil.getObject().getJSONObject("response").getString("name");

                    if (!name.equalsIgnoreCase(checkName)) {
                        CloudService.getCloudService().getTerminal().writeMessage("'"+name+"' has new Name: '"+checkName+"'");

                        name = checkName;

                        save = true;
                    }
                } catch (JSONException ex) {
                    ex.printStackTrace();
                    CloudService.getCloudService().getTerminal().writeMessage("Result (PlayerManager:236) -> " + jsonUtil.getResult());
                    CloudService.getCloudService().shutdown();
                    break;
                }
            }*/

            String language = document.getString("language");
            String locale = "de-DE";
            if (document.containsKey("locale")) locale = document.getString("locale");
            String ip = document.getString("ip");
            String rank = document.getString("rank");
            Long playedTime = document.getLong("playedTime");
            long coins = document.getLong("coins");
            
            Document dates = document.get("dates", Document.class);
            long firstLogin = dates.getLong("firstlogin");
            long lastLogin = dates.getLong("lastlogin");
            long lastLogout = dates.getLong("lastlogout");
            long rankEnd = dates.getLong("rankend");

            boolean forceSave = false;
            Map<UUID, FriendData> friends = new HashMap<>();
            for (Document friend : document.getList("friends", Document.class)) {
                friends.put(UUID.fromString(friend.getString("uniqueId")), new FriendData(UUID.fromString(friend.getString("uniqueId")), friend.getLong("date"), friend.containsKey("favorite") ? friend.getBoolean("favorite") : false));
                forceSave = !friend.containsKey("favorite");
            }

            Map<UUID, FriendData> requests = new HashMap<>();
            for (Document request : document.getList("requests", Document.class)) {
                requests.put(UUID.fromString(request.getString("uniqueId")), new FriendData(UUID.fromString(request.getString("uniqueId")), request.getLong("date"), false));
            }

            Document connectedAccounts = document.get("connectedAccounts", Document.class);
            String forumIdd = "";
            if (connectedAccounts.get("forumId") instanceof Integer) {
                forumIdd = String.valueOf(connectedAccounts.getInteger("forumId"));
            } else {
                forumIdd = connectedAccounts.getString("forumId");
            }
            int forumId = (forumIdd.equalsIgnoreCase("noid") ? -1 : Integer.parseInt(forumIdd));
            String teamSpeakId = connectedAccounts.getString("teamspeak");
            String discordId = connectedAccounts.getString("discord");

            String globalStats = document.getString("globalStats");
            String monthlyStats = document.getString("monthlyStats");
            String dailyStats = document.getString("dailyStats");

            String permissions = document.getString("permissions");

            Document settings = document.get("settings", Document.class);
            Map<String, Object> settingsMap = new HashMap<>();
            if (settings != null) {
                for (String key : settings.keySet()) {
                    settingsMap.put(key, settings.get(key));
                }
            }

            Document statusDocument = document.get("status", Document.class);
            String status = (statusDocument.getString("status") != null ? statusDocument.getString("status") : "NONE");
            long statusSet = (statusDocument.getLong("statusSet") != null ? statusDocument.getLong("status") : 0L);
            long statusBan = (statusDocument.getLong("statusBan") != null ? statusDocument.getLong("status") : 0L);

            Document skinDocument = document.get("skin", Document.class);
            String skinValue = skinDocument.getString("value");
            String skinSignature = skinDocument.getString("signature");

            Document abuseDocument = document.get("abuses", Document.class);
            long banPoints = abuseDocument.getLong("banPoints");
            long mutePoints = abuseDocument.getLong("mutePoints");
            Abuses.Abuse ban = null;
            Abuses.Abuse mute = null;
            if (abuseDocument.get("ban") != null) {
                Document abuse = abuseDocument.get("ban", Document.class);
                ban = new Abuses.Abuse(abuse.getString("banId"), UUID.fromString(abuse.getString("author")), abuse.getString("reason"), abuse.getLong("length"), abuse.getLong("created"), abuse.getString("comment"));
            }
            if (abuseDocument.get("mute") != null) {
                Document abuse = abuseDocument.get("mute", Document.class);
                mute = new Abuses.Abuse(abuse.getString("banId"), UUID.fromString(abuse.getString("author")), abuse.getString("reason"), abuse.getLong("length"), abuse.getLong("created"), abuse.getString("comment"));
            }

            double locX = -3.5, locY = 14.0, locZ = 48.5;
            float yaw = -64.5f, pitch = 0.0f;

            if (document.get("location") != null) {
                Document location = document.get("location", Document.class);
                locX = location.getDouble("locX");
                locY = location.getDouble("locY");
                locZ = location.getDouble("locZ");
                yaw = Float.parseFloat(String.valueOf(location.getDouble("yaw")));
                pitch = Float.parseFloat(String.valueOf(location.getDouble("pitch")));
            }

            long level = 1, experience = 0;

            if (document.get("level") != null) {
                Document levelSystem = document.get("level", Document.class);
                level = levelSystem.getLong("level");
                experience = levelSystem.getLong("experience");
            }

            //List<String> friendList = friends != null ? CloudService.getCloudService().getManagerService().getFriendManager().fromSaveString(friends) : new ArrayList<>();
            //List<String> requestList = requests != null ? CloudService.getCloudService().getManagerService().getFriendManager().fromSaveString(requests) : new ArrayList<>();
            if (this.nameCloudPlayers.containsKey(name.toLowerCase())) {
                CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("players").dropIndex(document);
                continue;
            }
            CloudPlayer cloudPlayer = new CloudPlayer(uuid, name, language, locale, ip, Rank.fromString(rank), lastLogin, firstLogin, rankEnd, friends, requests, coins, permissions);
            cloudPlayer.setLocX(locX);
            cloudPlayer.setLocY(locY);
            cloudPlayer.setLocZ(locZ);
            cloudPlayer.setYaw(yaw);
            cloudPlayer.setPitch(pitch);

            cloudPlayer.setSettingsPacket(new SettingsPacket(uuid, settingsMap));
            cloudPlayer.getSettingsPacket().create();

            for (Punish punish : CloudService.getCloudService().getManagerService().getPunishManager().getReasons()) {
                if (cloudPlayer.getSettingsPacket().getSettings().containsKey(punish.getKey().toUpperCase())) continue;
                cloudPlayer.getSettingsPacket().setSetting(punish.getKey().toUpperCase(), 0);
                forceSave = true;
            }

            if (cloudPlayer.getLevel() == null)
                cloudPlayer.setLevel(new Level(level, experience));

            if (cloudPlayer.getRank().isLowerLevel(Rank.VIP))
                cloudPlayer.setSetting("auto-nick", 0);

            cloudPlayer.setObjectId(objectId);
            cloudPlayer.setId(id);
            cloudPlayer.setVersion(version);
            cloudPlayer.setCountry(country);

            cloudPlayer.setForumId(forumId);
            cloudPlayer.setTeamSpeakId(teamSpeakId);
            cloudPlayer.setDiscordId(discordId);

            cloudPlayer.setStatus(new Status(status, statusSet, statusBan));

            cloudPlayer.setPlayTime(Integer.parseInt(String.valueOf(playedTime)));
            cloudPlayer.setLastLogout(lastLogout);
            this.uniqueIdPlayers.put(uuid, cloudPlayer);
            this.nameCloudPlayers.put(name.toLowerCase(), cloudPlayer);
            if (cloudPlayer.getRank() != Rank.MEMBER) {
                this.playersWithRank.add(cloudPlayer.getUniqueId());
            }
            if (globalStats != null) {
                cloudPlayer.statsFromString(globalStats, cloudPlayer.getStats().getGlobalStats());
                CloudService.getCloudService().getManagerService().getStatsManager().getGlobalStats().add(cloudPlayer);
            }
            if (monthlyStats != null) {
                cloudPlayer.statsFromString(monthlyStats, cloudPlayer.getStats().getMonthlyStats());
                CloudService.getCloudService().getManagerService().getStatsManager().getMonthlyStats().add(cloudPlayer);
            }
            if (dailyStats != null) {
                cloudPlayer.statsFromString(dailyStats, cloudPlayer.getStats().getDailyStats());
                CloudService.getCloudService().getManagerService().getStatsManager().getDailyStats().add(cloudPlayer);
            }

            if (skinValue != null && skinSignature != null) {
                cloudPlayer.setSkinValue(skinValue);
                cloudPlayer.setSkinSignature(skinSignature);
            }

            //TODO: Abuse Methods

            cloudPlayer.getAbuses().setBanPoints(banPoints);
            cloudPlayer.getAbuses().setMutePoints(mutePoints);
            if (ban != null) {
                cloudPlayer.getAbuses().setBan(ban);
                this.playersWithBan.add(cloudPlayer.getUniqueId());
            }
            if (mute != null) {
                cloudPlayer.getAbuses().setMute(mute);
                this.playersWithMute.add(cloudPlayer.getUniqueId());
            }

            cloudPlayer.setFullLoaded(true);
            if (forceSave) {
                cloudPlayer.updateToDatabase(false);
            }

            /*if (save) {
                cloudPlayer.updateToDatabase(false);

                if (cloudPlayer.getPageAccount() != -1) {
                    GoAPI.getForumAPI().changeName(cloudPlayer.getPageAccount(), name, o -> {
                        CloudService.getCloudService().getTerminal().writeMessage("Update Page-Name for " + cloudPlayer.getName());
                    });
                }
            }*/
        }

        CloudService.getCloudService().getTerminal().writeMessage("§3Loaded §e§l" + count + " §3players");

        int dbCount = 0;
        ArrayList<Document> pdb = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("playerdb");

        for (Document document : pdb) {
            String player = document.getString("player");
            String key = document.getString("dbkey");
            String value = document.getString("dbvalue");
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(UUID.fromString(player));
            if (cloudPlayer == null) continue;
            cloudPlayer.getPlayerDB().put(key, value);
            ++dbCount;
        }

        CloudService.getCloudService().getTerminal().writeMessage("§3Loaded §e§l" + dbCount + " §3Database Values");
    }

    public void checkOnlinePlayers() {
        new Thread(() -> {
            Iterator<UUID> cloudPlayerIterator = this.onlineCloudPlayers.iterator();
            while (cloudPlayerIterator.hasNext()) {
                CloudPlayer cloudPlayer = this.getPlayerAPI().getCloudPlayerByUniqueId(cloudPlayerIterator.next());
                if (cloudPlayer.getProxy() == null) {
                    cloudPlayer.setProxy(null);
                    CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().remove(cloudPlayer.getUniqueId());
                }
                if (cloudPlayer.getProxy() != null && !cloudPlayer.getProxy().getCloudPlayers().contains(cloudPlayer.getUniqueId())) {
                    this.playerLogOut(cloudPlayer.getUniqueId(), null);
                    ArrayList<Proxy> loggedIn = new ArrayList<Proxy>();
                    for (Proxy proxy2 : CloudService.getCloudService().getManagerService().getProxyManager().getProxies()) {
                        if (!proxy2.getCloudPlayers().contains(cloudPlayer.getUniqueId())) continue;
                        loggedIn.add(proxy2);
                    }
                    for (Proxy proxy2 : loggedIn) {
                        if(cloudPlayer.getLanguage() == Language.LanguageType.GERMAN) {
                            proxy2.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.KICK_PACKET, new KickPacket(cloudPlayer.getUniqueId(), "§cDu bist auf mehreren Proxys eingeloggt!")));
                        } else {
                            proxy2.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.KICK_PACKET, new KickPacket(cloudPlayer.getUniqueId(), "§cYou are logged in on more than one Proxy!")));
                        }
                    }
                    loggedIn.clear();
                    continue;
                }
                for (Proxy proxy : CloudService.getCloudService().getManagerService().getProxyManager().getProxies()) {
                    if (proxy.getName().equals(cloudPlayer.getProxy().getName())) continue;
                    for (UUID uuid : proxy.getCloudPlayers()) {
                        if (uuid != cloudPlayer.getUniqueId()) continue;
                        proxy.getCloudPlayers().remove(uuid);
                    }
                }
            }
        }
        ).start();
    }

    public void playerLogin(UUID uniqueId, String server, String name, Integer version, String ip, Proxy proxy, String value, String signature) {
        CloudPlayer cloudPlayer;
        ++this.joinProcesses;
        if (!this.loggedInUsers.contains(uniqueId)) {
            this.loggedInUsers.add(uniqueId);
        }
        boolean verifyProxy = proxy.getName().startsWith("Verify");

        if (!this.uniqueIdPlayers.containsKey(uniqueId)) {
            cloudPlayer = new CloudPlayer(uniqueId, name, "german", "de-DE", ip, Rank.MEMBER, System.currentTimeMillis(), System.currentTimeMillis(), 0, new HashMap<>(), new HashMap<>(),0, null);
            cloudPlayer.setSettingsPacket(new SettingsPacket(uniqueId, null));
            cloudPlayer.joinProxy(proxy);

            cloudPlayer.setLevel(new Level(1, 0));
            cloudPlayer.setStatus(new Status("NONE", 0L, 0L));

            cloudPlayer.setForumId(-1);
            cloudPlayer.setTeamSpeakId("noid");
            cloudPlayer.setDiscordId("noid");
            this.uniqueIdPlayers.put(uniqueId, cloudPlayer);
            this.nameCloudPlayers.put(name.toLowerCase(), cloudPlayer);

            if (cloudPlayer.getSkinValue() == null) {
                if (value != null) {
                    cloudPlayer.setSkinValue(value);
                } else {
                    cloudPlayer.setSkinValue(this.getPlayerAPI().getDefaultSkinValue());
                }
            }

            if (cloudPlayer.getSkinSignature() == null) {
                if (value != null) {
                    cloudPlayer.setSkinSignature(signature);
                } else {
                    cloudPlayer.setSkinSignature(this.getPlayerAPI().getDefaultSkinSignature());
                }
            }

            try {
                cloudPlayer.setCountry(GeoIP2API.getCountry(ip).getName());
            } catch (Exception e) {
                cloudPlayer.setCountry("DE");
            }

            /*if (ipCloudPlayers.get(ip) != null && getCloudPlayersByIP(ip) >= ipCloudPlayers.get(ip)) {
                proxy.sendPacket(CloudServer.getPacketHelper().preparePacket(PacketType.KICK_PACKET, new KickPacket(name, CloudServer.getLanguageManager().getLanguages().get(cloudPlayer.getLanguage()).getIP_LIMIT_REACHED())));
            } else {
                if (!this.onlineCloudPlayers.contains(cloudPlayer.getUniqueId())) {
                    this.onlineCloudPlayers.add(cloudPlayer.getUniqueId());
                }
            }*/

            if (!this.onlineCloudPlayers.contains(cloudPlayer.getUniqueId())) {
                this.onlineCloudPlayers.add(cloudPlayer.getUniqueId());
            }

            CloudService.getCloudService().getTerminal().writeMessage("§e§l"+name+" §3joined the first time on the network!");
            BeforePlayer beforePlayer = CloudService.getCloudService().getManagerService().getBeforeManager().getPlayer(name);
            if (beforePlayer != null) {
                if (beforePlayer.getRank() != null) {
                    cloudPlayer.setRank(Rank.fromString(beforePlayer.getRank()), 0);
                }
                if (beforePlayer.getPermissions() != null) {
                    cloudPlayer.setPermissions(beforePlayer.getPermissions());
                }
                CloudService.getCloudService().getManagerService().getBeforeManager().getBeforePlayers().remove(name.toLowerCase());
                beforePlayer.remove();
            }
            try {
                String key = this.getPlayerAPI().getLanguage(ip);
                Locale defaultLocale = Locale.forLanguageTag("en-US");
                if (key.contains(",")) {
                    Locale locale = Locale.forLanguageTag(key.split(",")[0]);
                    cloudPlayer.setLocale((locale != null ? (CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager().getLocales().containsKey(locale) ? locale : defaultLocale) : Locale.forLanguageTag("en-US")));
                } else {
                    if (key.contains("-")) {
                        Locale locale = Locale.forLanguageTag(key);
                        cloudPlayer.setLocale((locale != null ? (CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager().getLocales().containsKey(locale) ? locale : defaultLocale) : defaultLocale));
                    } else {
                        if (key.equalsIgnoreCase("de")) {
                            Locale locale = Locale.GERMANY;
                            cloudPlayer.setLocale((locale != null ? (CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager().getLocales().containsKey(locale) ? locale : defaultLocale) : Locale.forLanguageTag("en-US")));
                        }
                    }
                }
            } catch (Exception e) {
                cloudPlayer.setLocale(Locale.forLanguageTag("en-US"));
            }
            cloudPlayer.setFullLoaded(true);

            cloudPlayer.sendMessage("user-first-join", cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()+"§7");
            ArrayList<String[]> messageData = new ArrayList<>();
            messageData.add(new String[]{cloudPlayer.getLanguageMessage("click-to-accept-terms"), "true", "run_command", "/agree"});
            ComponentMessagePacket componentMessagePacket = new ComponentMessagePacket(cloudPlayer.getName(), messageData);
            cloudPlayer.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMPONENT_MESSAGE_PACKET, componentMessagePacket));

            if (!cloudPlayer.getName().startsWith("StressBot"))
                cloudPlayer.updateToDatabase(true);
        } else {
            cloudPlayer = this.getPlayerAPI().getCloudPlayerByUniqueId(uniqueId);

            /*if (ipCloudPlayers.get(ip) != null && getCloudPlayersByIP(ip) >= ipCloudPlayers.get(ip)) {
                proxy.sendPacket(CloudServer.getPacketHelper().preparePacket(PacketType.KICK_PACKET, new KickPacket(name, CloudServer.getLanguageManager().getLanguages().get(cloudPlayer.getLanguage()).getIP_LIMIT_REACHED().replace("&", "§"))));
            } else {
                if (!this.onlineCloudPlayers.contains(cloudPlayer.getUniqueId())) {
                    this.onlineCloudPlayers.add(cloudPlayer.getUniqueId());
                }
            }*/

            if (!this.onlineCloudPlayers.contains(cloudPlayer.getUniqueId())) {
                this.onlineCloudPlayers.add(cloudPlayer.getUniqueId());
            }

            //System.out.print("Name: " + cloudPlayer.getName());

            if (cloudPlayer.getAbuses().getBan() != null && !verifyProxy) {
                /*if (cloudPlayer.getLanguage() == Language.LanguageType.GERMAN) {
                    proxy.sendPacket(CloudServer.getPacketHelper().preparePacket(PacketType.KICK_PACKET, new KickPacket(name, "§cUps. Leider ist ein Fehler aufgetreten. \n Melde dich mit dem Code §7#1457 §cim Discord oder TeamSpeak Support.")));
                } else {
                    proxy.sendPacket(CloudServer.getPacketHelper().preparePacket(PacketType.KICK_PACKET, new KickPacket(name, "§cUps. Sorry, an error has occurred. \n Contact us with the code §7#1457 §cin our Discord or TeamSpeak Support.")));
                }*/

                String reason = cloudPlayer.getAbuses().getBan().getReason();

                if (PunishReason.Points.getPoint(cloudPlayer.getAbuses().getBan().getReason()) != null) {
                    reason = cloudPlayer.getLanguageMessage(reason);
                }

                if (cloudPlayer.getAbuses().getBan().getLength() == 0) {
                    proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.KICK_PACKET, new KickPacket(cloudPlayer.getUniqueId(), MessageFormat.format(cloudPlayer.getLanguageMessage("ban-perma-screen"), reason).replace("&", "§"))));
                } else {
                    proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.KICK_PACKET, new KickPacket(cloudPlayer.getUniqueId(), MessageFormat.format(cloudPlayer.getLanguageMessage("ban-temp-screen"), reason, GoAPI.getTimeManager().getRemainingTimeString(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), cloudPlayer.getLocale(), cloudPlayer.getAbuses().getBan().getLength())).replace("&", "§"))));
                }
                return;
            }
            if (cloudPlayer.getProxy() != null) {
                CloudService.getCloudService().getTerminal().writeMessage("§e§l"+name+" §cduplicated login");
                proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.KICK_PACKET, new KickPacket(cloudPlayer.getUniqueId(), cloudPlayer.getLanguageMessage("already-online").replace("&", "§"))));
                cloudPlayer.setProxy(null);
                onlineCloudPlayers.remove(cloudPlayer.getUniqueId());
                return;
            }
            CloudService.getCloudService().getTerminal().writeMessage("§e§l"+name+" §3joined the network");

            try {
                cloudPlayer.setCountry(GeoIP2API.getCountry(ip).getName());
            } catch (Exception e) {
                cloudPlayer.setCountry("DE");
            }

            cloudPlayer.setVersion(version);
            cloudPlayer.setIp(ip);
            cloudPlayer.setLastLogin(System.currentTimeMillis());
            cloudPlayer.joinProxy(proxy);

            cloudPlayer.setSession(new Session(uniqueId, System.currentTimeMillis(), 0L, 0L, ip, String.valueOf(version)));

            if (!this.onlineCloudPlayers.contains(cloudPlayer.getUniqueId())) {
                this.onlineCloudPlayers.add(cloudPlayer.getUniqueId());
            }
            if (!this.ipCloudPlayers.containsKey(ip)) {
                this.ipCloudPlayers.put(ip, 1);
            }
            if (!cloudPlayer.getName().equals(name)) {
                getNameCloudPlayers().remove(cloudPlayer.getName().toLowerCase());
                getUniqueIdPlayers().remove(cloudPlayer.getUniqueId());
                cloudPlayer.setName(name);
                getNameCloudPlayers().put(name.toLowerCase(), cloudPlayer);
                getUniqueIdPlayers().put(cloudPlayer.getUniqueId(), cloudPlayer);

                /*if (cloudPlayer.getDiscorduid() != "noid") {
                    Member member = CloudServer.getDiscordManager().getJda().getGuildById("521372521535242242").getMemberById(cloudPlayer.getDiscorduid());
                    CloudServer.getDiscordManager().getJda().getGuildById("521372521535242242").getController().setNickname(member, name).queue();
                }*/

                cloudPlayer.updateToDatabase(false);

                if (cloudPlayer.getForumId() != -1) {
                    GoAPI.getForumAPI().changeName(cloudPlayer.getForumId(), name, o -> {
                        //CloudService.getCloudService().getTerminal().writeMessage("Update Page-Name for " + cloudPlayer.getName());
                    });
                }
            }

            if (!cloudPlayer.getSkinValue().equals(value)) {
                if (value != null) {
                    cloudPlayer.setSkinValue(value);
                } else {
                    cloudPlayer.setSkinValue(this.getPlayerAPI().getDefaultSkinValue());
                }
            }

            if (!cloudPlayer.getSkinSignature().equals(signature)) {
                if (signature != null) {
                    cloudPlayer.setSkinSignature(signature);
                } else {
                    cloudPlayer.setSkinSignature(this.getPlayerAPI().getDefaultSkinSignature());
                }
            }

            if (!cloudPlayer.getIp().equals(ip)) {
                cloudPlayer.setIp(ip);
            }

            if (!verifyProxy) {
                CloudService.getCloudService().getManagerService().getFriendManager().handleLogin(cloudPlayer);
                CloudService.getCloudService().getManagerService().getClanManager().handleLogin(cloudPlayer);

                cloudPlayer.updateToDatabase(false);
                ArrayList<CloudPlayer> online = new ArrayList<>();

                for (FriendData friendData : cloudPlayer.getFriends().values()) {
                    CloudPlayer friend = this.getPlayerAPI().getCloudPlayerByUniqueId(friendData.getUniqueId());
                    if (friend == null || !friend.isOnline()) continue;
                    online.add(friend);
                }

                if (!online.isEmpty()) {
                    if (online.size() == 1) {
                        cloudPlayer.sendMessage("currently-friend-online-single", online.get(0).getRank().getRankColor() + online.get(0).getName());
                    } else if (online.size() == 2) {
                        cloudPlayer.sendMessage("currently-friend-online-sample-double", online.get(0).getRank().getRankColor()+online.get(0).getName(), online.get(1).getRank().getRankColor()+online.get(1).getName());
                    } else if (online.size() == 3) {
                        cloudPlayer.sendMessage("currently-friend-online-sample-extended-one", online.get(0).getRank().getRankColor()+online.get(0).getName(), online.get(1).getRank().getRankColor()+online.get(1).getName());
                    } else {
                        cloudPlayer.sendMessage("currently-friend-online-sample-extended", online.get(0).getRank().getRankColor()+online.get(0).getName(), online.get(1).getRank().getRankColor()+online.get(1).getName(), (online.size()-2));
                    }
                }
                online.clear();

                if (!cloudPlayer.getRequests().isEmpty()) {
                    if (cloudPlayer.getRequests().size() == 1) {
                        cloudPlayer.sendMessage("friend-request");
                    } else {
                        cloudPlayer.sendMessage("friend-requests", cloudPlayer.getRequests().size());
                    }
                }
            }
        }

        if (this.getIpBanned().containsKey(ip)) {
            if (cloudPlayer.getAbuses().getBan() == null) {
                for (CloudPlayer player : this.getPlayerAPI().getCloudPlayersByIP(ip)) {
                    if (player.getAbuses().getBan() != null && cloudPlayer.getAbuses().getBan().getReason().equalsIgnoreCase("BANBYPASS")) continue;

                    long newPoints = player.getAbuses().getBanPoints();
                    newPoints += 100;
                    player.getAbuses().setBanPoints(newPoints);
                    player.updateToDatabase(false);

                    //CloudService.getCloudService().getManagerService().getAbuseManager().silentBanPlayer(player, 0, points.name(), this.getPlayerAPI().getCloudPlayerByUniqueId(UUID.fromString("336991c8-1199-457c-80bb-649042a489f5")));
                    CloudService.getCloudService().getManagerService().getAbuseManager().punish(player, CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(UUID.fromString("336991c8-1199-457c-80bb-649042a489f5")), "BANBYPASS", "BANBYPASS", 0L, false, true);
                }
            }
        }

        long as = Long.parseLong((ip.equalsIgnoreCase("127.0.0.1") ? "3320" : String.valueOf(GeoIP2API.getASN(ip).getAutonomousSystemNumber())));

        if (CloudService.getCloudService().getManagerService().getAutonomousManager().getNetworks().containsKey(as)) {
            if (CloudService.getCloudService().getManagerService().getAutonomousManager().getNetworks().get(as).isBlocked()) {
                proxy.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.KICK_PACKET, new KickPacket(uniqueId, cloudPlayer.getLanguageMessage("blocked-as").replace("&", "§"))));
                return;
            }

            CloudService.getCloudService().getManagerService().getAutonomousManager().getNetworks().get(as).increaseConnections();
        } else {
            CloudService.getCloudService().getTerminal().writeMessage("§3Create new Entry for AS §e§l"+as);
            ASNetwork asNetwork = new ASNetwork((ip.equalsIgnoreCase("127.0.0.1") ? "Deutsche Telekom AG" : GeoIP2API.getASN(ip).getAutonomousSystemOrganization()), as, System.currentTimeMillis(), Long.parseLong("1"), uniqueId, false);
            asNetwork.updateToDatabase(true);
            CloudService.getCloudService().getManagerService().getAutonomousManager().getNetworks().put(as, asNetwork);
        }

        if (cloudPlayer.getRank().isJrModeration()) {
            int x = 0;
            for (ReportPlayersPacket.ReportEntry report : CloudService.getCloudService().getManagerService().getReportManager().getEntries()) {
                if (report.getVisitor() != null && report.getState() != ReportPlayersPacket.ReportState.NOT_EDITED) {
                    continue;
                }
                x++;
            }

            if (x > 0) {
                cloudPlayer.sendMessage("staff-join-reports", x);
            }
        }

        if (cloudPlayer.getSetting("collect-daily-reward-automatically", Integer.class) == 1 && cloudPlayer.getRank().isHigherEqualsLevel(Rank.PREMIUM)) {
            boolean collected = false;
            if (cloudPlayer.getStats().getDailyStatsValue("member_coin_reward") == 0) {
                CloudService.getCloudService().getManagerService().getStatsManager().addPlayerStat(cloudPlayer, "member_coin_reward", 1);
                cloudPlayer.addCoins(1000);
                collected = true;
            }
            if (cloudPlayer.getStats().getDailyStatsValue("premium_coin_reward") == 0) {
                CloudService.getCloudService().getManagerService().getStatsManager().addPlayerStat(cloudPlayer, "premium_coin_reward", 1);
                cloudPlayer.addCoins(2000);
                collected = true;
            }
            if (cloudPlayer.getStats().getDailyStatsValue("daily_chest") == 0) {
                CloudService.getCloudService().getManagerService().getStatsManager().addPlayerStat(cloudPlayer, "daily_chest", 1);
                CloudService.getCloudService().getManagerService().getStatsManager().addPlayerStat(cloudPlayer, "lobby_chests", 1);
                collected = true;
            }
            if (cloudPlayer.getStats().getDailyStatsValue("member_lottery_reward") == 0) {
                CloudService.getCloudService().getManagerService().getStatsManager().addPlayerStat(cloudPlayer, "member_lottery_reward", 1);
                CloudService.getCloudService().getManagerService().getStatsManager().addPlayerStat(cloudPlayer, "lobby_lottery_tickets", 1);
                collected = true;
            }
            if (cloudPlayer.getStats().getDailyStatsValue("premium_lottery_reward") == 0) {
                CloudService.getCloudService().getManagerService().getStatsManager().addDailyStat(cloudPlayer, "premium_lottery_reward", 1);
                CloudService.getCloudService().getManagerService().getStatsManager().addPlayerStat(cloudPlayer, "lobby_lottery_tickets", 2);
                collected = true;
            }

            if (collected) {
                int i = new Random().nextInt(3);

                if (i == 0) {
                    cloudPlayer.sendMessage("daily-reward-collection-one");
                } else if (i == 1) {
                    cloudPlayer.sendMessage("daily-reward-collection-two");
                } else if (i == 2) {
                    cloudPlayer.sendMessage("daily-reward-collection-three");
                } else if (i == 3) {
                    cloudPlayer.sendMessage("daily-reward-collection-four");
                }

                cloudPlayer.sendMessage("daily-reward-collected");
            }
        }

        if (cloudPlayer.getRankEnd() != 0 && cloudPlayer.getRankEnd() <= (System.currentTimeMillis()+ TimeUnit.DAYS.toMillis(2))) {
            cloudPlayer.sendMessage("user-rank-expires-warning", GoAPI.getTimeManager().getRemainingTimeString(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), cloudPlayer.getLocale(), cloudPlayer.getRankEnd()));
        }

        cloudPlayer.connectToServer(server);
        cloudPlayer.login();
        CloudService.getCloudService().getManagerService().getEventManager().callEvent(new LoginEvent(cloudPlayer));
    }

    public void playerLogOut(UUID uuid, Channel channel) {
        if (!this.getOnlineCloudPlayers().contains(uuid)) return;
        CloudPlayer cloudPlayer = this.getPlayerAPI().getCloudPlayerByUniqueId(uuid);
        if (cloudPlayer != null) {
            CloudService.getCloudService().getTerminal().writeMessage("§e§l"+cloudPlayer.getName()+" §3left the network");
            for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
                server.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_LOGIN_PACKET, new PlayerLoginPacket(cloudPlayer.getName(), false)));
            }
            if (cloudPlayer.getProxy() != null) {
                cloudPlayer.getProxy().getCloudPlayers().remove(uuid);
            }
            if (cloudPlayer.isOnline()) {
                CloudService.getCloudService().getManagerService().getFriendManager().handleLogOut(cloudPlayer);
                CloudService.getCloudService().getManagerService().getClanManager().handleLogOut(cloudPlayer);
                if (cloudPlayer.getParty() != null) {
                    cloudPlayer.getParty().memberLeave(cloudPlayer);
                }
            }
            cloudPlayer.logout();
            cloudPlayer.setServer(null);
            cloudPlayer.setProxy(null);
            cloudPlayer.setLastLogout(System.currentTimeMillis());

            if (cloudPlayer.getPrivateServer() != null) {
                if (this.getPlayerAPI().getCloudPlayersFromServer(cloudPlayer.getPrivateServer().getName()).size() > 0) {
                    for (CloudPlayer players : this.getPlayerAPI().getCloudPlayersFromServer(cloudPlayer.getPrivateServer().getName())) {
                        //TODO: Send Message that owner goes offline
                        /*if (players.getLanguage() == Language.LanguageType.GERMAN) {
                            players.sendRawMessage(GoAPI.getPrefix() + "§7Der Besitzer dieses Servers ist offline gegangen daher wird dieser nun gestoppt.");
                        } else {
                            players.sendRawMessage(GoAPI.getPrefix() + "§7The owner of this server has gone offline so it will now be stopped.");
                        }*/

                        CloudService.getCloudService().getManagerService().getLobbyManager().sendToLobby(players.getName());
                        cloudPlayer.getPrivateServer().remove();
                    }
                }
                cloudPlayer.getPrivateServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.SPIGOT_SERVER_STOP, new SpigotServerStop(cloudPlayer.getPrivateServer().getName())));
                cloudPlayer.setPrivateServer(null);
            }
            this.onlineCloudPlayers.remove(cloudPlayer.getUniqueId());

            if (cloudPlayer.getSession() != null) {
                cloudPlayer.getSession().setEnd(System.currentTimeMillis());
                cloudPlayer.saveSession();
                cloudPlayer.setSession(null);
            }
            cloudPlayer.setAfk(false);

            cloudPlayer.updateToDatabase(false);
            /*if (CloudServer.isTs()) {
                CloudServer.getDiscordManager().setStatus();
            }*/
        }

        CloudService.getCloudService().getManagerService().getEventManager().callEvent(new LogoutEvent(cloudPlayer));
    }

    public ConcurrentHashMap<String, CloudPlayer> getNameCloudPlayers() {
        return this.nameCloudPlayers;
    }

    public ConcurrentHashMap<UUID, CloudPlayer> getUniqueIdPlayers() {
        return uniqueIdPlayers;
    }

    public ConcurrentHashMap<String, String> getTeamSpeakUniqueIds() {
        return teamSpeakUniqueIds;
    }

    public List<UUID> getOnlineCloudPlayers() {
        return this.onlineCloudPlayers;
    }

    public List<UUID> getPlayersWithRank() {
        return this.playersWithRank;
    }

    public List<UUID> getPlayersWithBan() {
        return this.playersWithBan;
    }

    public List<UUID> getPlayersWithMute() {
        return this.playersWithMute;
    }

    public List<UUID> getLoggedInUsers() {
        return this.loggedInUsers;
    }

    public int getJoinProcesses() {
        return this.joinProcesses;
    }

    public void check(PlayerManager playerManager, long l) {
        playerManager.lastCheck = l;
    }
}