package net.vicemice.hector.utils.utility.document;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.vicemice.hector.network.ClydeAPI;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringBufferInputStream;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class Document implements DocumentAbstract {
    public static Gson GSON = new GsonBuilder().serializeNulls().setPrettyPrinting().disableHtmlEscaping().create();
    protected static final JsonParser PARSER = new JsonParser();
    protected String name;
    private File file;
    protected JsonObject dataCatcher;

    public Document(String name) {
        this.name = name;
        this.dataCatcher = new JsonObject();
    }

    public Document(String name, JsonObject source) {
        this.name = name;
        this.dataCatcher = source;
    }

    public Document(File file, JsonObject jsonObject) {
        this.file = file;
        this.dataCatcher = jsonObject;
    }

    public Document(String key, String value) {
        this.dataCatcher = new JsonObject();
        this.append(key, value);
    }

    public Document(String key, Object value) {
        this.dataCatcher = new JsonObject();
        this.append(key, value);
    }

    public Document(String key, Number value) {
        this.dataCatcher = new JsonObject();
        this.append(key, value);
    }

    public Document(Document defaults) {
        this.dataCatcher = defaults.dataCatcher;
    }

    public Document(Document defaults, String name) {
        this.dataCatcher = defaults.dataCatcher;
        this.name = name;
    }

    public Document() {
        this.dataCatcher = new JsonObject();
    }

    public Document(JsonObject source) {
        this.dataCatcher = source;
    }

    public JsonObject obj() {
        return this.dataCatcher;
    }

    public boolean contains(String key) {
        return this.dataCatcher.has(key);
    }

    public Document append(String key, String value) {
        if (value == null) {
            return this;
        }
        this.dataCatcher.addProperty(key, value);
        return this;
    }

    public Document append(String key, Number value) {
        if (value == null) {
            return this;
        }
        this.dataCatcher.addProperty(key, value);
        return this;
    }

    public Document append(String key, Boolean value) {
        if (value == null) {
            return this;
        }
        this.dataCatcher.addProperty(key, value);
        return this;
    }

    public Document append(String key, JsonElement value) {
        if (value == null) {
            return this;
        }
        this.dataCatcher.add(key, value);
        return this;
    }

    public Document append(String key, List<String> value) {
        if (value == null) {
            return this;
        }
        JsonArray jsonElements = new JsonArray();
        for (String b : value) {
            jsonElements.add(b);
        }
        this.dataCatcher.add(key, jsonElements);
        return this;
    }

    public Document append(String key, Document value) {
        if (value == null) {
            return this;
        }
        this.dataCatcher.add(key, value.dataCatcher);
        return this;
    }

    @Deprecated
    public Document append(String key, Object value) {
        if (value == null) {
            return this;
        }
        if (value instanceof Document) {
            this.append(key, (Document)value);
            return this;
        }
        this.dataCatcher.add(key, GSON.toJsonTree(value));
        return this;
    }

    public Document appendValues(Map<String, Object> values) {
        for (Map.Entry<String, Object> valuess : values.entrySet()) {
            this.append(valuess.getKey(), valuess.getValue());
        }
        return this;
    }

    public Document remove(String key) {
        this.dataCatcher.remove(key);
        return this;
    }

    @Override
    public Set<String> keys() {
        HashSet<String> c = new HashSet<String>();
        for (Map.Entry<String, JsonElement> x : this.dataCatcher.entrySet()) {
            c.add(x.getKey());
        }
        return c;
    }

    public JsonElement get(String key) {
        if (!this.dataCatcher.has(key)) {
            return null;
        }
        return this.dataCatcher.get(key);
    }

    @Override
    public String getString(String key) {
        if (!this.dataCatcher.has(key)) {
            return null;
        }
        return this.dataCatcher.get(key).getAsString();
    }

    @Override
    public int getInt(String key) {
        if (!this.dataCatcher.has(key)) {
            return 0;
        }
        return this.dataCatcher.get(key).getAsInt();
    }

    @Override
    public long getLong(String key) {
        if (!this.dataCatcher.has(key)) {
            return 0L;
        }
        return this.dataCatcher.get(key).getAsLong();
    }

    @Override
    public double getDouble(String key) {
        if (!this.dataCatcher.has(key)) {
            return 0.0;
        }
        return this.dataCatcher.get(key).getAsDouble();
    }

    @Override
    public boolean getBoolean(String key) {
        if (!this.dataCatcher.has(key)) {
            return false;
        }
        return this.dataCatcher.get(key).getAsBoolean();
    }

    @Override
    public float getFloat(String key) {
        if (!this.dataCatcher.has(key)) {
            return 0.0f;
        }
        return this.dataCatcher.get(key).getAsFloat();
    }

    @Override
    public short getShort(String key) {
        if (!this.dataCatcher.has(key)) {
            return 0;
        }
        return this.dataCatcher.get(key).getAsShort();
    }

    public <T> T getObject(String key, Class<T> class_) {
        if (!this.dataCatcher.has(key)) {
            return null;
        }
        JsonElement element = this.dataCatcher.get(key);
        return GSON.fromJson(element, class_);
    }

    public Document getDocument(String key) {
        if (!this.dataCatcher.has(key)) {
            return null;
        }
        return new Document(this.dataCatcher.get(key).getAsJsonObject());
    }

    public Document clear() {
        for (String key : this.keys()) {
            this.remove(key);
        }
        return this;
    }

    public int size() {
        return this.dataCatcher.size();
    }

    public Document loadProperties(Properties properties) {
        Enumeration<?> enumeration = properties.propertyNames();
        while (enumeration.hasMoreElements()) {
            Object x = enumeration.nextElement();
            this.append(x.toString(), properties.getProperty(x.toString()));
        }
        return this;
    }

    public boolean isEmpty() {
        return this.dataCatcher.size() == 0;
    }

    public JsonArray getArray(String key) {
        return this.dataCatcher.get(key).getAsJsonArray();
    }

    @Override
    public String convertToJson() {
        return GSON.toJson(this.dataCatcher);
    }

    public String convertToJsonString() {
        return this.dataCatcher.toString();
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    @Override
    public boolean saveAsConfig(File backend) {
        if (backend == null) {
            return false;
        }
        if (backend.exists()) {
            backend.delete();
        }
        try {
            try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(backend), "UTF-8");){
                GSON.toJson(this.dataCatcher, writer);
                boolean bl = true;
                return bl;
            }
        }
        catch (IOException ex) {
            ex.getStackTrace();
            return false;
        }
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    @Deprecated
    public boolean saveAsConfig0(File backend) {
        if (backend == null) {
            return false;
        }
        if (backend.exists()) {
            backend.delete();
        }
        try {
            try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(backend), "UTF-8");){
                ClydeAPI.GSON.toJson(this.dataCatcher, writer);
                boolean bl = true;
                return bl;
            }
        }
        catch (IOException ex) {
            ex.getStackTrace();
            return false;
        }
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    public boolean saveAsConfig(Path path) {
        try {
            try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(Files.newOutputStream(path, new OpenOption[0]), "UTF-8");){
                GSON.toJson(this.dataCatcher, outputStreamWriter);
                boolean bl = true;
                return bl;
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean saveAsConfig(String path) {
        return this.saveAsConfig(Paths.get(path, new String[0]));
    }

    public static Document loadDocument(File backend) {
        return Document.loadDocument(backend.toPath());
    }

    public static Document $loadDocument(File backend) throws Exception {
        try {
            return new Document(PARSER.parse(new String(Files.readAllBytes(backend.toPath()), StandardCharsets.UTF_8)).getAsJsonObject());
        }
        catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    /*
     * Exception decompiling
     */
    public static Document loadDocument(Path backend) {
        // This method has failed to decompile.  When submitting a bug report, please provide this stack trace, and (if you hold appropriate legal rights) the relevant class file.
        // org.benf.cfr.reader.util.ConfusedCFRException: Tried to end blocks [1[TRYBLOCK]], but top level block is 5[TRYBLOCK]
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.processEndingBlocks(Op04StructuredStatement.java:427)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op04StructuredStatement.buildNestedBlocks(Op04StructuredStatement.java:479)
        // org.benf.cfr.reader.bytecode.analysis.opgraph.Op03SimpleStatement.createInitialStructuredBlock(Op03SimpleStatement.java:607)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisInner(CodeAnalyser.java:696)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysisOrWrapFail(CodeAnalyser.java:184)
        // org.benf.cfr.reader.bytecode.CodeAnalyser.getAnalysis(CodeAnalyser.java:129)
        // org.benf.cfr.reader.entities.attributes.AttributeCode.analyse(AttributeCode.java:96)
        // org.benf.cfr.reader.entities.Method.analyse(Method.java:397)
        // org.benf.cfr.reader.entities.ClassFile.analyseMid(ClassFile.java:906)
        // org.benf.cfr.reader.entities.ClassFile.analyseTop(ClassFile.java:797)
        // org.benf.cfr.reader.Driver.doJarVersionTypes(Driver.java:225)
        // org.benf.cfr.reader.Driver.doJar(Driver.java:109)
        // org.benf.cfr.reader.CfrDriverImpl.analyse(CfrDriverImpl.java:65)
        // org.benf.cfr.reader.Main.main(Main.java:48)
        throw new IllegalStateException("Decompilation failed");
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    public Document loadToExistingDocument(File backend) {
        try {
            try (InputStreamReader reader = new InputStreamReader((InputStream)new FileInputStream(backend), "UTF-8");){
                this.dataCatcher = PARSER.parse(reader).getAsJsonObject();
                this.file = backend;
                Document document = this;
                return document;
            }
        }
        catch (Exception ex) {
            ex.getStackTrace();
            return new Document();
        }
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    public Document loadToExistingDocument(Path path) {
        try {
            try (InputStreamReader reader = new InputStreamReader(Files.newInputStream(path, new OpenOption[0]), "UTF-8");){
                this.dataCatcher = PARSER.parse(reader).getAsJsonObject();
                Document document = this;
                return document;
            }
        }
        catch (Exception ex) {
            ex.getStackTrace();
            return new Document();
        }
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    public static Document load(String input) {
        try {
            try (InputStreamReader reader = new InputStreamReader((InputStream)new StringBufferInputStream(input), "UTF-8");){
                Document document = new Document(PARSER.parse(new BufferedReader(reader)).getAsJsonObject());
                return document;
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            return new Document();
        }
    }

    public String toString() {
        return this.convertToJsonString();
    }

    public static Document load(JsonObject input) {
        return new Document(input);
    }

    public <T> T getObject(String key, Type type) {
        if (!this.contains(key)) {
            return null;
        }
        return GSON.fromJson(this.dataCatcher.get(key), type);
    }

    public byte[] toBytesAsUTF_8() {
        return this.convertToJsonString().getBytes(StandardCharsets.UTF_8);
    }

    public byte[] toBytes() {
        return this.convertToJsonString().getBytes();
    }

    @Override
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public File getFile() {
        return this.file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}

