package net.vicemice.hector.utils;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.network.modules.api.event.player.DatabaseSaveEvent;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/*
 * Class created at 19:22 - 31.12.2019
 * Copyright (C) 2016-2019 Minetasia Solutions
 */
public class DatabaseQueue extends Thread {

    private final Queue<SaveEntry> queue = new LinkedBlockingQueue<SaveEntry>();

    public DatabaseQueue() {
        super("CloudPlayer save queue");
        this.setDaemon(true);
    }

    @Override
    public void run() {
        while (!this.isInterrupted()) {
            this.blockThread();
            while (!this.queue.isEmpty()) {
                SaveEntry saveEntry = this.queue.poll();
                if (saveEntry.getSaveEntry() != null) {
                    saveEntry.getSaveEntry().save(saveEntry.isInsert());
                }

                CloudService.getCloudService().getManagerService().getEventManager().callEvent(new DatabaseSaveEvent(saveEntry));
            }
        }
    }

    public synchronized void addToQueue(net.vicemice.hector.utils.SaveEntry saveEntry, boolean insert) {
        if (saveEntry != null) {
            for (SaveEntry saveEntry1 : this.queue) {
                if (!saveEntry1.getSaveEntry().equals(saveEntry) || saveEntry1.isInsert() != insert) continue;
                return;
            }
        }
        this.queue.add(new SaveEntry(saveEntry, insert));
        this.notify();
    }

    private synchronized void blockThread() {
        try {
            while (this.queue.isEmpty()) {
                this.wait();
            }
        }
        catch (Exception var1_1) {
            // empty catch block
        }
    }

    public static class SaveEntry {
        private final net.vicemice.hector.utils.SaveEntry saveEntry;
        private final boolean insert;

        public SaveEntry(net.vicemice.hector.utils.SaveEntry saveEntry, boolean insert) {
            this.saveEntry = saveEntry;
            this.insert = insert;
        }

        public net.vicemice.hector.utils.SaveEntry getSaveEntry() {
            return saveEntry;
        }

        public boolean isInsert() {
            return this.insert;
        }
    }
}