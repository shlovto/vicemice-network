package net.vicemice.hector.utils.event;

public interface IEventListener<E extends Event> {
    public void onCall(E var1);
}