package net.vicemice.hector.manager.lobby;

import io.netty.channel.Channel;
import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.network.gameserver.lobby.Lobby;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.lobby.*;
import net.vicemice.hector.packets.types.lobby.type.FallbackDataInfo;
import net.vicemice.hector.packets.types.lobby.type.LobbyDataInfo;
import net.vicemice.hector.packets.types.lobby.type.VerifyDataInfo;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.gameserver.ServerData;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class LobbyManager {

    @Getter
    private List<Lobby> lobbyServers = new CopyOnWriteArrayList<Lobby>();
    private int nextCount = 0;
    @Setter
    private boolean eventMode = false;
    /*@Getter
    @Setter
    private EventTypes type = null;*/

    public LobbyManager() {
        new Timer().scheduleAtFixedRate(new TimerTask(){

            @Override
            public void run() {
                List<LobbyDataInfo> dataList = new ArrayList<>();

                LobbyManager.this.lobbyServers.forEach(lobby -> {
                    if (lobby.getServer() == null || lobby.getServer().getServerState() != ServerState.LOBBY) return;
                    dataList.add(new LobbyDataInfo(lobby.getServer().getName(), lobby.getServer().getOnlinePlayers()));
                });

                PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.LOBBY_PACKET, new LobbyPacket(dataList));
                PacketHolder infoPacket = GoAPI.getPacketHelper().preparePacket(PacketType.LOBBY_INFO, getLobbyInfoPacket());
                LobbyManager.this.lobbyServers.forEach(lobby -> {
                    lobby.getServer().sendPacket(initPacket);
                    lobby.getServer().sendPacket(infoPacket);
                    CloudService.getCloudService().getManagerService().getSignStatsManager().sendStatPacketToLobby(lobby.getServer(), CloudService.getCloudService().getManagerService().getSignStatsManager().getGlobalSignPacket());
                });
                for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
                    if (!server.isCloudServer() || !server.getTemplateName().equalsIgnoreCase("silent")) continue;
                    server.sendPacket(initPacket);
                }
            }
        }, 300, 300);
    }

    public LobbyInfoPacket getLobbyInfoPacket() {
        ConcurrentHashMap<String, Integer> games = new ConcurrentHashMap<>();

        int qsg1 = CloudService.getCloudService().getManagerService().getServerManager().getPlayerCountFromTemplate("EloQSG");
        int qsg2 = CloudService.getCloudService().getManagerService().getServerManager().getPlayerCountFromTemplate("QSG");
        games.put("qsg", (qsg1+qsg2));

        int bw1 = CloudService.getCloudService().getManagerService().getServerManager().getPlayerCountFromTemplate("elobw4x2");
        int bw2 = CloudService.getCloudService().getManagerService().getServerManager().getPlayerCountFromTemplate("bw4x2");
        games.put("bedwars", (bw1+bw2));

        List<ServerData> data = new ArrayList<>();
        CloudService.getCloudService().getManagerService().getServerManager().getServers().forEach(server -> {
            if (!server.isCloudServer()) return;
            data.add(new ServerData((server.getTemplateName() != null ? server.getTemplateName() : "CLOUDSERVER"), server.getName(), server.getOnlinePlayers(), server.getMaxPlayers(), server.getMessage(), server.getVersion(), Rank.valueOf(server.getRank()), (server.getServerState() == ServerState.INGAME ? ServerData.Type.INGAME : (server.getServerState() == ServerState.LOBBY ? ServerData.Type.LOBBY : ServerData.Type.RESTART)), (server.getServerTemplate() != null && server.getServerTemplate().isPlayAble() ? true : false)));
        });

        return new LobbyInfoPacket(games, data);
    }

    public LobbyDataPacket getLobbyDataPacket() {
        List<LobbyDataInfo> lobbyDataInfos = new ArrayList<>();
        List<FallbackDataInfo> fallbackDataInfos = new ArrayList<>();
        List<VerifyDataInfo> verifyDataInfos = new ArrayList<>();

        for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServersByTemplate("lobby")) {
            lobbyDataInfos.add(new LobbyDataInfo(server.getName(), server.getOnlinePlayers()));
        }

        for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServersByTemplate("fallback")) {
            fallbackDataInfos.add(new FallbackDataInfo(server.getName(), server.getOnlinePlayers()));
        }

        for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServersByTemplate("verify")) {
            fallbackDataInfos.add(new FallbackDataInfo(server.getName(), server.getOnlinePlayers()));
        }

        return new LobbyDataPacket(lobbyDataInfos, fallbackDataInfos, verifyDataInfos);
    }

    public void sendToLobby(String name) {
        this.sendToLobby(name, null);
    }

    public void sendToFallback(String name) {
        CloudPlayer player = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(name);
        if (player != null) {
            player.connectToServer("fallback");
        }
    }

    public void sendPlayerToSilentHub(String playerName) {
        CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(playerName);
        if (cloudPlayer == null) {
            return;
        }
        if (cloudPlayer.getServer() != null && cloudPlayer.getName().startsWith("slient")) {
            return;
        }
        Server silentServer = null;
        for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
            if (!server.isCloudServer() || !server.getTemplateName().equalsIgnoreCase("silent")) continue;
            silentServer = server;
            break;
        }
        if (silentServer != null) {
            cloudPlayer.connectToServer(silentServer.getName());
        } else {
            cloudPlayer.sendMessage("no-silentlobby-server-online");
        }
    }

    public void sendToLobby(String name, Channel channel) {
        long timeout = System.currentTimeMillis() + 1000;
        CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(name);
        if (cloudPlayer == null || cloudPlayer.getProxy() == null) {
            do {
                if (System.currentTimeMillis() <= timeout) continue;
                return;
            } while ((cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(name)) == null || cloudPlayer.getProxy() == null);
        }
        if (channel != null && cloudPlayer.getProxy() != null && !channel.equals(cloudPlayer.getProxy().getNettyChannel())) {
            return;
        }
        /*if (cloudPlayer.getRank().isHigherEqualsLevel(Rank.ADMIN)) {
            for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
                if (!server.isCloudServer() || !server.getTemplateName().equalsIgnoreCase("silent") || server.getServerState() != ServerState.LOBBY) continue;
                cloudPlayer.setServer(server);
                cloudPlayer.connectToServer(server.getName());
                cloudPlayer.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.LOBBY_LOC_PACKET, new LobbyLocPacket(cloudPlayer.getUniqueId(), cloudPlayer.getLocX(), cloudPlayer.getLocY(), cloudPlayer.getLocZ(), cloudPlayer.getYaw(), cloudPlayer.getPitch())));
                return;
            }
        } */

        Lobby nextLobby = null;
        if (this.nextCount > this.lobbyServers.size() - 1) {
            this.nextCount = 0;
        }
        if (this.lobbyServers.size() > 0) {
            nextLobby = this.lobbyServers.get(this.nextCount);
        }
        if (nextLobby == null || nextLobby.getServer() == null || nextLobby.getServer().getServerState() != ServerState.LOBBY) {
            this.nextCount = 0;
            nextLobby = this.findLobbyServer();
        } else {
            ++this.nextCount;
        }
        if (nextLobby != null) {
            cloudPlayer.setServer(nextLobby.getServer());
            cloudPlayer.connectToServer(nextLobby.getServer().getName());
            cloudPlayer.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.LOBBY_LOC_PACKET, new LobbyLocPacket(cloudPlayer.getUniqueId(), cloudPlayer.getLocX(), cloudPlayer.getLocY(), cloudPlayer.getLocZ(), cloudPlayer.getYaw(), cloudPlayer.getPitch())));
        } else {
            cloudPlayer.sendMessage("no-lobby-online");
            if (cloudPlayer.getServer() != null && cloudPlayer.getServer().getName().equals("fallback")) {
                cloudPlayer.sendMessage("connect-to-fallback");
            }
            Server fallback = CloudService.getCloudService().getManagerService().getServerManager().getRandomFallbackServer();
            if (fallback != null) {
                if (!cloudPlayer.getServer().getName().equals(fallback.getName())) {
                    cloudPlayer.setServer(fallback);
                    cloudPlayer.connectToServer(fallback.getName());
                }
            } else {
                cloudPlayer.kickPlayer(cloudPlayer.getLanguageMessage("no-fallback-server-online"));
            }
        }
    }

    public void createLobby(Server server) {
        Lobby lobby = new Lobby(server);
        this.lobbyServers.add(lobby);
        //PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.EVENT_PACKET, new EventPacket(this.isEventMode(), this.getType()));
        //server.sendPacket(initPacket);
    }

    public void sendPacketToAllLobbies(PacketHolder initPacket) {
        for (Lobby lobby : this.lobbyServers) {
            lobby.getServer().sendPacket(initPacket);
        }
    }

    public Lobby findLobbyServer() {
        for (Lobby lobby : this.lobbyServers) {
            if (!lobby.getServer().getServerState().equals(ServerState.LOBBY)) continue;
            return lobby;
        }
        return null;
    }

    public void deleteLobby(Server server) {
        for (Lobby lobby : this.lobbyServers) {
            if (!lobby.getServer().getName().equalsIgnoreCase(server.getName())) continue;
            this.lobbyServers.remove(lobby);
        }
    }

    public boolean isEventMode() {
        return this.eventMode;
    }
}