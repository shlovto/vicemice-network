package net.vicemice.hector.player.utils;

import lombok.Data;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.utils.SaveEntry;
import org.bson.Document;
import org.bson.conversions.Bson;
import java.util.UUID;

/*
 * Class created at 14:38 - 28.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class ASNetwork implements SaveEntry {

    private String name;
    private Long id, created, connections;
    private UUID firstPlayer;
    private boolean blocked;

    public ASNetwork(String name, Long id, Long created, Long connections, UUID firstPlayer, boolean blocked) {
        this.name = name;
        this.id = id;
        this.created = created;
        this.connections = connections;
        this.firstPlayer = firstPlayer;
        this.blocked = blocked;
    }

    public void increaseConnections() {
        this.connections = (connections+1);
        this.updateToDatabase(false);
    }

    public void decreaseConnections() {
        this.connections = (connections-1);
        this.updateToDatabase(false);
    }

    public void updateToDatabase(boolean insert) {
        CloudService.getCloudService().getDatabaseQueue().addToQueue(this, insert);
    }

    @Override
    public void save(boolean insert) {
        try {
            if (insert) {
                Document document = new Document("organisation", name);
                document.append("id", id);
                document.append("created", created);
                document.append("connections", connections);
                document.append("firstPlayer", firstPlayer);
                document.append("blocked", blocked);
                CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("autonomousSystem").insertOne(document);

                return;
            }

            Document document = new Document("connections", connections);
            document.append("blocked", blocked);

            Bson filter = new Document("id", id);
            CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("autonomousSystem").updateOne(filter, new Document("$set", document));
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }
}
