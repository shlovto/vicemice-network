package net.vicemice.hector.nettyserver.proxy;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.ReferenceCountUtil;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.packets.types.PacketHolder;

public class ProxyHandler extends SimpleChannelInboundHandler<Object> {

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object o) throws Exception {
        new Thread(() -> {
            PacketHolder initPacket = (PacketHolder) o;
            if (initPacket == null) {
                System.out.println("BungeeCordHandler: 19 NPE -> InitPacket");
                return;
            }
            try {
                CloudService.getCloudService().getManagerService().getProxyManager().getPacketResolver().handlePacket(initPacket.getKey(), initPacket.getValue(), channelHandlerContext.channel());
            } finally {
                ReferenceCountUtil.release(o);
            }
        }
        ).start();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
    }
}