package net.vicemice.hector.player.utils;

import lombok.Data;
import lombok.Getter;
import net.vicemice.hector.utils.PunishType;

import java.beans.ConstructorProperties;
import java.io.Serializable;
import java.util.UUID;

/*
 * Class created at 16:07 - 01.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class Abuses implements Serializable {

    private long banPoints, mutePoints;
    private Abuse ban, mute;

    @Getter
    public static class Abuse implements Serializable {
        private UUID author;
        private String banId, reason, comment;
        private long created, length;

        @ConstructorProperties(value={"banId","author","reason", "length", "created", "comment"})
        public Abuse(String banId, UUID author, String reason, long length, long created, String comment) {
            this.banId = banId;
            this.author = author;
            this.reason = reason;
            this.length = length;
            this.created = created;
            this.comment = comment;
        }
    }

    @Getter
    public static class AbuseData implements Serializable {
        private UUID user, author;
        private String reason, id;
        private PunishType punishType;
        private long created, length;
        private String comment;
        private boolean shorted, removed;

        @ConstructorProperties(value={"user","author", "id", "reason", "abuseType", "length", "created", "comment", "shorted", "removed"})
        public AbuseData(UUID user, UUID author, String id, String reason, PunishType punishType, long length, long created, String comment, boolean shorted, boolean removed) {
            this.user = user;
            this.author = author;
            this.id = id;
            this.reason = reason;
            this.punishType = punishType;
            this.length = length;
            this.created = created;
            this.comment = comment;
            this.shorted = shorted;
            this.removed = removed;
        }
    }
}