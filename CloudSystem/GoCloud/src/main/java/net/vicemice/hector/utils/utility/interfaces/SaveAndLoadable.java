package net.vicemice.hector.utils.utility.interfaces;

public interface SaveAndLoadable<E> {
    public boolean save(E var1);

    public E load();
}