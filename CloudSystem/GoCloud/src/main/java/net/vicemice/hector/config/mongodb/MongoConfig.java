package net.vicemice.hector.config.mongodb;

import lombok.Data;

/**
 * Created by Paul B. on 27.01.2019.
 */
@Data
public class MongoConfig {
    private String host = "127.0.0.1";
    private String port = "27017";
    private String username = "username";
    private String password = "password";
}