package net.vicemice.hector.network.modules.api.event.teamspeak;

import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.event.Event;

public class TeamSpeakUnverifyEvent extends Event {
    private CloudPlayer cloudPlayer;
    private boolean confirmed;

    public TeamSpeakUnverifyEvent(CloudPlayer cloudPlayer, boolean confirmed) {
        this.cloudPlayer = cloudPlayer;
        this.confirmed = confirmed;
    }

    public CloudPlayer getCloudPlayer() {
        return this.cloudPlayer;
    }

    public boolean isConfirmed() {
        return confirmed;
    }
}
