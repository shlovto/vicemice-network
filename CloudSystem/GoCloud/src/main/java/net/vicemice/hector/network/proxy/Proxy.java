package net.vicemice.hector.network.proxy;

import io.netty.channel.Channel;
import lombok.Data;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.api.util.players.ProxyPlayerInfo;
import net.vicemice.hector.player.CloudPlayer;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

/*
 * Class created at 19:43 - 01.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class Proxy {
    private UUID uniqueId;
    private String name;
    private String ipAddress;
    private int port;
    private long timeOut;
    private List<UUID> cloudPlayers = new CopyOnWriteArrayList<UUID>();
    private Channel nettyChannel;
    private int onlinePlayers;

    public Proxy(String name, String ipAddress, int port, Channel nettyChannel) {
        this.uniqueId = UUID.randomUUID();
        this.name = name;
        this.ipAddress = ipAddress;
        this.port = port;
        this.nettyChannel = nettyChannel;
        this.updateTimeOut();
    }

    public void updateTimeOut() {
        this.timeOut = System.currentTimeMillis() + 20000;
    }

    public void sendPacket(Object object) {
        this.getNettyChannel().writeAndFlush(object);
    }

    public void updatePlayingPlayers(List<ProxyPlayerInfo> players) {
        if (players != null) {
            players.forEach(e -> {
                CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(e.getUniqueId());
                if (cloudPlayer == null || this.cloudPlayers.contains(e.getUniqueId())) return;
                CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().remove(cloudPlayer.getUniqueId());
                CloudService.getCloudService().getManagerService().getPlayerManager().playerLogin(cloudPlayer.getUniqueId(), e.getServer(), cloudPlayer.getName(), cloudPlayer.getVersion(), cloudPlayer.getIp(), this, cloudPlayer.getSkinValue(), cloudPlayer.getSkinSignature());
            });

            this.cloudPlayers.forEach(e -> {
                CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(e);
                if (cloudPlayer == null || this.cloudPlayers.contains(cloudPlayer.getUniqueId())) return;
                this.cloudPlayers.remove(cloudPlayer.getUniqueId());
                CloudService.getCloudService().getManagerService().getPlayerManager().playerLogOut(cloudPlayer.getUniqueId(), null);
                cloudPlayer.kickPlayer("disconnected");
                if (CloudService.getCloudService().isDevMode()) CloudService.getCloudService().getTerminal().writeMessage("§e§l"+cloudPlayer.getName()+" §3was found intern but not on a proxy");
                CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().remove(cloudPlayer.getUniqueId());
            });
        }
    }

    public void remove() {
        if (CloudService.getCloudService().isDevMode()) CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().sendAdminMessage("cloud-proxy-removed", name);
        CloudService.getCloudService().getTerminal().writeMessage("§e§l"+this.getName()+" §3disconnected as proxy");
        CloudService.getCloudService().getManagerService().getProxyManager().getProxies().remove(this);
    }
}