package net.vicemice.hector.config.mysql;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Paul B. on 27.01.2019.
 */
public class MySQLConfiguration {
    private File configFile;
    @Getter(AccessLevel.PUBLIC)
    private MySQLConfig mySQLConfig;
    private Gson gson;

    public MySQLConfiguration() {
        this.configFile = new File("config/mysql.json");
        this.gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
    }

    public boolean exists() {
        return configFile.exists();
    }

    public void writeConfiguration() {
        writeConfiguration(mySQLConfig);
    }

    public void writeConfiguration(MySQLConfig mySQLConfig) {
        try (FileWriter fileWriter = new FileWriter(configFile)) {
            gson.toJson(mySQLConfig, fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readConfiguration() {
        try (FileReader fileReader = new FileReader(configFile)) {
            mySQLConfig = gson.fromJson(fileReader, MySQLConfig.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}