package net.vicemice.hector.manager.permissions;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.utils.Rank;
import org.bson.Document;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/*
 * Class created at 00:37 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PermissionManager {

    private HashMap<String, List<String[]>> permissionsMap;

    public void reloadPermissions() {
        this.permissionsMap = new HashMap<>();

        try {
            ArrayList<Document> list = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("permissions");

            this.permissionsMap.clear();

            for (Document document : list) {
                String getRank = document.getString("rank");
                String permission = document.getString("permission").toLowerCase();
                String serverGroup = document.getString("serverGroup");

                Rank rank = Rank.fromStringExact(getRank);

                if (rank != null) {
                    List<String> addPermissions = new ArrayList<>();
                    if (permission.contains(",")) {
                        List<String> permissionList = Arrays.asList(permission.split(","));
                        addPermissions.addAll(permissionList.stream().collect(Collectors.toList()));
                    } else {
                        addPermissions.add(permission);
                    }

                    if (!this.permissionsMap.containsKey(rank.getRankName().toLowerCase())) {
                        this.permissionsMap.put(rank.getRankName().toLowerCase(), new ArrayList<>());
                    }

                    List<String[]> permissions = this.permissionsMap.get(rank.getRankName().toLowerCase());
                    for (String aPermission : addPermissions) {
                        String[] data = new String[]{aPermission, serverGroup};
                        if (!permissions.contains(data)) {
                            permissions.add(data);
                        }
                    }
                    this.permissionsMap.put(rank.getRankName().toLowerCase(), permissions);
                } else {
                    CloudService.getCloudService().getTerminal().writeMessage(getRank + " does not exists in Enumeration!");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public HashMap<String, List<String[]>> getPermissionsMap() {
        return this.permissionsMap;
    }
}