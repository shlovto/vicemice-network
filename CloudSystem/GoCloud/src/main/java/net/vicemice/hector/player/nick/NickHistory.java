package net.vicemice.hector.player.nick;

import lombok.Getter;
import lombok.Setter;

/*
 * Class created at 00:23 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class NickHistory {
    @Getter
    private String nickname;
    @Getter
    private String username;
    @Getter
    private long start;
    @Getter
    @Setter
    private long ended;

    public NickHistory(String nickname, String username) {
        this.nickname = nickname;
        this.username = username;
        this.start = System.currentTimeMillis();
        this.ended = -1;
    }
}