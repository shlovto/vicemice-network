package net.vicemice.hector.utils.terminal.commands.punish;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.utils.terminal.ArgumentList;
import net.vicemice.hector.utils.terminal.CommandExecutor;
import net.vicemice.hector.utils.terminal.Terminal;

public class CommandUnban implements CommandExecutor {
    @Override
    public void onCommand(String command, Terminal writer, String[] args) {
        if (args.length == 0) {
            writer.writeMessage("§cNeed more arguments...");
        } else {
            if (CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[0]) != null) {
                if (CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[0]).getAbuses().getBan() == null) {
                    writer.writeMessage("§aThe Player is not banned!");
                    return;
                }

                CloudService.getCloudService().getManagerService().getAbuseManager().remove(CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[0]), null, "Admin Unban", false);
                writer.writeMessage("§aThe Player was unbanned!");
            }
        }
    }

    @Override
    public ArgumentList getArguments() {
        return ArgumentList.builder().arg(new ArgumentList.Argument("/unban", "Unban a Player")).build();
    }
}
