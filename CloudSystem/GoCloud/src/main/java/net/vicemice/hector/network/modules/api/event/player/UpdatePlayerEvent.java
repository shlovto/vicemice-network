package net.vicemice.hector.network.modules.api.event.player;

import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.event.async.AsyncEvent;
import net.vicemice.hector.utils.event.async.AsyncPosterAdapter;

public class UpdatePlayerEvent extends AsyncEvent<UpdatePlayerEvent> {
    private CloudPlayer offlinePlayer;

    public UpdatePlayerEvent(CloudPlayer offlinePlayer) {
        super(new AsyncPosterAdapter<>());
        this.offlinePlayer = offlinePlayer;
    }

    public CloudPlayer getOfflinePlayer() {
        return this.offlinePlayer;
    }
}