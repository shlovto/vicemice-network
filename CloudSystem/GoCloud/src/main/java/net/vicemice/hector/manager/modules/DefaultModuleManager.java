package net.vicemice.hector.manager.modules;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.network.modules.DefaultModule;
import net.vicemice.hector.utils.CollectionWrapper;
import net.vicemice.hector.utils.threading.Runnabled;

import java.io.InputStream;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;

public class DefaultModuleManager {
    private Collection<DefaultModule> modules = new CopyOnWriteArrayList<DefaultModule>();

    public DefaultModuleManager() throws Exception {
        final Properties properties = new Properties();
        try (InputStream inputStream = CloudService.class.getClassLoader().getResourceAsStream("modules/modules.properties");){
            properties.load(inputStream);
        }
        ArrayList<?> property = Collections.list(properties.propertyNames());
        CollectionWrapper.iterator(property, new Runnabled(){

            public void run(Object obj) {
                String pro = obj.toString();
                DefaultModuleManager.this.modules.add(new DefaultModule(pro, properties.getProperty(pro)));
            }
        });
        for (DefaultModule defaultModule : this.modules) {
            Path path = Paths.get("modules/" + defaultModule.getModuleName() + ".jar", new String[0]);
            Files.deleteIfExists(path);
            InputStream inputStream = defaultModule.stream();
            Throwable throwable = null;
            try {
                Files.copy(inputStream, path, new CopyOption[0]);
            }
            catch (Throwable throwable2) {
                throwable = throwable2;
                throw throwable2;
            }
            finally {
                if (inputStream == null) continue;
                if (throwable != null) {
                    try {
                        inputStream.close();
                    }
                    catch (Throwable throwable3) {
                        throwable.addSuppressed(throwable3);
                    }
                    continue;
                }
                inputStream.close();
            }
        }
    }

    public Collection<DefaultModule> getModules() {
        return this.modules;
    }
}