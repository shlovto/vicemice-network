package net.vicemice.hector.network.gameserver;

import lombok.Getter;

import java.beans.ConstructorProperties;

public class WhitelistedServer {

    @Getter
    private String host;
    @Getter
    private int port;
    @Getter
    private String name;
    @Getter
    private String rank;
    @Getter
    private Integer version;

    @ConstructorProperties(value={"host", "port", "name", "rank", "version"})
    public WhitelistedServer(String host, int port, String name, String rank, Integer version) {
        this.host = host;
        this.port = port;
        this.name = name;
        this.rank = rank;
        this.version = version;
    }
}

