package net.vicemice.hector.network.modules.exception;

public class ModuleLoadException extends RuntimeException {
    public ModuleLoadException(String message) {
        super(message);
    }
}

