package net.vicemice.hector.manager.punish;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.utils.players.punish.Punish;
import org.bson.Document;
import java.util.ArrayList;
import java.util.List;

public class PunishManager {

    @Getter
    @Setter
    private List<Punish> reasons;

    public void init() {
        this.reasons = new ArrayList<>();

        ArrayList<Document> list = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("punishTemplates");
        for (Document document : list) {
            String key = document.getString("key");
            String type = document.getString("type");
            int points = document.getInteger("points");
            boolean viewAble = document.getBoolean("viewAble");
            boolean useAble = document.getBoolean("useAble");
            boolean replayRequired = document.getBoolean("replayRequired");
            boolean chatLogRequired = document.getBoolean("chatLogRequired");
            int accessLevel = document.getInteger("accessLevel");
            List<String> keyWords = document.getList("keyWords", String.class);
            List<String> waves = document.getList("waves", String.class);

            this.reasons.add(new Punish(key, type, points, viewAble, useAble, replayRequired, chatLogRequired, accessLevel, keyWords, waves));
        }

        CloudService.getCloudService().getTerminal().writeMessage("§3Loaded §e§l" + this.reasons.size() + " §3punishment templates");
    }

    public Punish getByKey(String key) {
        for (Punish punish : reasons) {
            if (!punish.getKey().equalsIgnoreCase(key.toUpperCase())) continue;
            return punish;
        }

        return null;
    }

    public Punish getByWord(String key) {
        for (Punish punish : reasons) {
            if (!punish.getKeyWords().contains(key.toLowerCase())) continue;
            return punish;
        }

        return null;
    }
}