package net.vicemice.hector.utils.utility.scheduler;

import net.vicemice.hector.utils.threading.Callback;
import java.util.concurrent.Callable;

public class TaskEntry<T> {
    protected volatile Callable<T> task;
    protected volatile T value = null;
    protected Callback<T> callback;
    protected long delayTimeOut;
    protected long repeat;
    protected long delay;
    protected boolean completed = false;
    private final TaskEntryFuture<T> future;

    public TaskEntry(Callable<T> task, Callback<T> complete, long delay, long repeat) {
        this.task = task;
        this.callback = complete;
        this.delay = delay;
        this.delayTimeOut = System.currentTimeMillis() + delay;
        this.repeat = repeat;
        this.future = new TaskEntryFuture(this, false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void invoke() throws Exception {
        if (this.task == null) {
            return;
        }
        T val = this.task.call();
        this.value = val;
        if (this.callback != null) {
            this.callback.call(val);
        }
        if (this.repeat != -1L && this.repeat != 0L) {
            --this.repeat;
        }
        if (this.repeat != 0L) {
            this.delayTimeOut = System.currentTimeMillis() + this.delay;
        } else {
            this.completed = true;
            if (this.future.waits) {
                TaskEntryFuture<T> taskEntryFuture = this.future;
                synchronized (taskEntryFuture) {
                    this.future.notifyAll();
                }
            }
        }
    }

    public Callback<T> getCallback() {
        return this.callback;
    }

    public long getDelayTimeOut() {
        return this.delayTimeOut;
    }

    public long getRepeat() {
        return this.repeat;
    }

    protected TaskEntryFuture<T> drop() {
        return this.future;
    }

    public boolean isCompleted() {
        return this.completed;
    }
}

