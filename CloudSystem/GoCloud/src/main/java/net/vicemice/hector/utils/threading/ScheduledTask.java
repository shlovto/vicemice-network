package net.vicemice.hector.utils.threading;

public class ScheduledTask implements Runnable {
    protected long taskId;
    protected Runnable runnable;
    protected int delay;
    protected int repeatDelay;
    protected boolean interrupted;
    protected int delayTime;
    protected int repeatTime;

    public ScheduledTask(long taskId, Runnable runnable, int delay, int repeatDelay) {
        this.taskId = taskId;
        this.runnable = runnable;
        this.delay = delay != -1 && delay != 0 ? delay : 0;
        this.repeatDelay = repeatDelay != -1 ? repeatDelay : 0;
        this.interrupted = false;
        this.delayTime = this.delay;
        this.repeatTime = repeatDelay == 0 ? -1 : repeatDelay;
    }

    protected boolean isAsync() {
        return false;
    }

    @Override
    public void run() {
        if (this.interrupted) {
            return;
        }
        if (this.delay != 0 && this.delayTime != 0) {
            --this.delayTime;
            return;
        }
        if (this.repeatTime > 0) {
            --this.repeatTime;
        } else {
            try {
                this.runnable.run();
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
            if (this.repeatTime == -1) {
                this.cancel();
                return;
            }
            this.repeatTime = this.repeatDelay;
        }
    }

    public void cancel() {
        this.interrupted = true;
    }

    public long getTaskId() {
        return this.taskId;
    }

    public Runnable getRunnable() {
        return this.runnable;
    }

    public int getDelay() {
        return this.delay;
    }

    public int getRepeatDelay() {
        return this.repeatDelay;
    }

    public boolean isInterrupted() {
        return this.interrupted;
    }

    public int getDelayTime() {
        return this.delayTime;
    }

    public int getRepeatTime() {
        return this.repeatTime;
    }
}

