package net.vicemice.hector.manager.lobby.util;

public class LobbySort implements Comparable<LobbySort> {
    private int count;
    private String[] lobbyData;

    public LobbySort(int count, String[] lobbyData) {
        this.count = count;
        this.lobbyData = lobbyData;
    }

    @Override
    public int compareTo(LobbySort o) {
        return this.count < o.count ? -1 : (this.count > o.count ? 1 : 0);
    }

    public int getCount() {
        return this.count;
    }

    public String[] getLobbyData() {
        return this.lobbyData;
    }
}