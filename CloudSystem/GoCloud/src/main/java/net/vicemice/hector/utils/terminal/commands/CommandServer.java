package net.vicemice.hector.utils.terminal.commands;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.utils.slave.ServerData;
import net.vicemice.hector.utils.slave.TemplateData;
import net.vicemice.hector.utils.terminal.ArgumentList;
import net.vicemice.hector.utils.terminal.CommandExecutor;
import net.vicemice.hector.utils.terminal.Terminal;

/*
 * Class created at 09:30 - 23.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class CommandServer implements CommandExecutor {

    @Override
    public void onCommand(String command, Terminal writer, String[] args) {
        if (args.length == 3) {
            if (args[0].equalsIgnoreCase("static")) {
                ServerData serverData = CloudService.getCloudService().getManagerService().getServerManager().getStaticServers().get(args[2]);
                if (serverData != null) {
                    if (args[1].equalsIgnoreCase("start")) {
                        writer.writeMessage("Trying to start '"+args[2]+"'...");
                        CloudService.getCloudService().getManagerService().getServerManager().createServer(serverData);
                    } else if (args[1].equalsIgnoreCase("stop")) {
                        writer.writeMessage("Trying to start '"+args[2]+"'...");
                        throw new UnsupportedOperationException("Could not stop Server '"+args[2]+"'");
                    } else {
                        writer.writeMessage("Sub-Command not found.");
                    }
                } else {
                    writer.writeMessage("Static-Server not found.");
                }
            } else if (args[0].equalsIgnoreCase("template")) {
                TemplateData serverTemplate = CloudService.getCloudService().getManagerService().getServerManager().getTemplateByName(args[2]);
                if (serverTemplate != null) {
                    if (args[1].equalsIgnoreCase("start")) {
                        if (serverTemplate.isActive()) {
                            writer.writeMessage("The Template '"+args[2]+ "' is already active");
                            return;
                        }

                        serverTemplate.setActive(true);
                        writer.writeMessage("The Template '"+args[2]+ "' was started");
                        CloudService.getCloudService().getManagerService().getServerManager().checkServers();
                    } else if (args[1].equalsIgnoreCase("stop")) {
                        if (!serverTemplate.isActive()) {
                            writer.writeMessage("The Template '"+args[2]+ "' is not active");
                            return;
                        }

                        serverTemplate.setActive(false);
                        writer.writeMessage("The Template '"+args[2]+ "' was stopped");
                        CloudService.getCloudService().getManagerService().getServerManager().checkServers();
                    } else {
                        writer.writeMessage("Sub-Command not found.");
                    }
                } else {
                    writer.writeMessage("Static-Server not found.");
                }
            } else {
                writer.writeMessage("Sub-Command not found.");
            }

            return;
        }

        writer.writeMessage("Server-Management Help:");
        writer.writeMessage("/server static start <Server> - start a static server");
        writer.writeMessage("/server static stop <Server> - stop a static server");
        writer.writeMessage("/server template start <Template> - start a Template");
        writer.writeMessage("/server template stop <Template> - stop a Template");
    }

    @Override
    public ArgumentList getArguments() {
        return ArgumentList.builder().arg(new ArgumentList.Argument("/server", "Manage the Servers")).build();
    }
}