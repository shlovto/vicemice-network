package net.vicemice.hector.utils.utility.interfaces;

public interface Reloadable {
    public void reload() throws Exception;
}