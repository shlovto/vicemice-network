package net.vicemice.hector.manager.party.event;

import net.vicemice.hector.network.modules.api.event.player.party.PartyDeleteEvent;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.party.Party;
import net.vicemice.hector.utils.event.IEventListener;

/*
 * Class created at 07:13 - 29.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PartyDeleteListener implements IEventListener<PartyDeleteEvent> {

    @Override
    public void onCall(PartyDeleteEvent event) {
        Party party = event.getParty();

        party.sendMessageToParty("party-disbanded");

        for (CloudPlayer cloudPlayer : party.getMembers()) {
            cloudPlayer.setParty(null);
        }
        party.getMembers().clear();
        party.getInvites().clear();
    }
}
