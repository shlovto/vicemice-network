package net.vicemice.hector.utils.gameid;

import net.vicemice.hector.CloudService;
import org.bson.Document;

public class GameIDSaver {

    public static void saveGameID(String gameID, String template) {
        Document document = new Document("gameID", gameID);
        document.append("template", template);

        CloudService.getCloudService().getManagerService().getMongoManager().getGamesDatabase().getCollection("games").insertOne(document);

        CloudService.getCloudService().getTerminal().writeMessage("Saved GameID " + gameID + " for Template " + template);
    }

    /*public static void saveBedwarsGame(BedwarsGame bedwarsGame) {
        Document document = new Document("gameID", bedwarsGame.getGameID());
        document.append("gameType", bedwarsGame.getGameType()).append("map", bedwarsGame.getMap()).append("winnerTeam", bedwarsGame.getWinnerTeam()).append("gameStart", bedwarsGame.getGameStart()).append("gameEnd", bedwarsGame.getGameEnd());
        HashMap gameData = new HashMap();
        for (String key : bedwarsGame.getGameData().keySet()) {
            Object value = bedwarsGame.getGameData().get(key);
            String[] array = value.split(";");
            String team = array[4];
            value = value.replaceAll(";" + team, "");
            if (!gameData.containsKey(team)) {
                gameData.put(team, new HashMap());
            }
            HashMap teamDataHashMap = (HashMap)gameData.get(team);
            teamDataHashMap.put(key, value);
        }
        Document teamsDocument = new Document();
        for (String team : gameData.keySet()) {
            HashMap teamDataMap = (HashMap)gameData.get(team);
            Document teamDocument = new Document();
            for (String player : teamDataMap.keySet()) {
                String[] playerData = ((String)teamDataMap.get(player)).split(";");
                Document playerDataDocument = new Document();
                playerDataDocument.append("deaths", Integer.valueOf(playerData[0]));
                playerDataDocument.append("kills", Integer.valueOf(playerData[1]));
                playerDataDocument.append("beds", Integer.valueOf(playerData[2]));
                playerDataDocument.append("points", Integer.valueOf(playerData[3]));
                teamDocument.append(player, playerDataDocument);
            }
            teamsDocument.append(team, teamDocument);
        }
        document.append("teams", teamsDocument);
        ArrayList<String> history = new ArrayList<String>();
        for (String data : bedwarsGame.getGameHistory()) {
            String[] array = data.split(";");
            CloudServer.getTimeManager();
            String saveString = "[" + TimeManager.getEndeAnzeige(Long.valueOf((String)array[0])) + "] " + (String)array[1];
            history.add(saveString);
        }
        document.append("gameHistory", history);
        ArrayList<String> chat = new ArrayList<String>();
        for (String chatdata : bedwarsGame.getMessages()) {
            String[] array = chatdata.split(";");
            CloudServer.getTimeManager();
            String saveString = "[" + TimeManager.getEndeAnzeige(Long.valueOf(array[0])) + "] " + array[1] + ": " + array[2];
            chat.add(saveString);
        }
        document.append("messages", chat);
        MongoDB.getMongoDB().getDatabase().getCollection("games").insertOne(document);
        CloudServer.getTerminal().log("Speichere BedWars Spiel mit GameIDPacket " + bedwarsGame.getGameID());
        TelegramManager.sendMessage("Speichere BedWars Spiel mit GameIDPacket " + bedwarsGame.getGameID());
    }*/
}

