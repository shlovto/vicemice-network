package net.vicemice.hector.utils.threading;

import net.vicemice.hector.utils.utility.scheduler.TaskScheduler;

public class ScheduledTaskAsync
extends ScheduledTask {
    protected Scheduler scheduler;

    public ScheduledTaskAsync(long taskId, Runnable runnable, int delay, int repeatDelay, Scheduler scheduler) {
        super(taskId, runnable, delay, repeatDelay);
        this.scheduler = scheduler;
    }

    @Override
    public void run() {
        if (this.interrupted) {
            return;
        }
        if (this.delay != 0 && this.delayTime != 0) {
            --this.delayTime;
            return;
        }
        if (this.repeatTime > 0) {
            --this.repeatTime;
        } else {
            TaskScheduler.runtimeScheduler().schedule(this.runnable);
            if (this.repeatTime == -1) {
                this.cancel();
                return;
            }
            this.repeatTime = this.repeatDelay;
        }
    }

    @Override
    protected boolean isAsync() {
        return true;
    }
}

