package net.vicemice.hector.manager.time;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.utils.language.Language;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class TimeManager {
    static DateFormat format = new SimpleDateFormat("dd.MM.yyyy - HH:mm:ss");
    DateFormat hourFormat = new SimpleDateFormat("HH");

    public String getEndDate(long ende) {
        if (ende < 1) {
            return "-1 Error";
        }
        try {
            Date date = new Date(ende);
            return format.format(date);
        }
        catch (Exception ex) {
            CloudService.getCloudService().getTerminal().writeMessage("Konnte Datum nicht herstellen!");
            CloudService.getCloudService().getTerminal().writeMessage("Variable ende: " + ende);
            CloudService.getCloudService().getTerminal().writeMessage(Arrays.deepToString(ex.getStackTrace()));
            return "31.12.2020 - 23:59:59";
        }
    }

    public String getHour() {
        Date date = new Date(System.currentTimeMillis());
        return this.hourFormat.format(date);
    }

    public String getRemainingTimeString(Language.LanguageType language, long endTime) {
        String append;
        String[] end = this.getRemainingTime(endTime);
        StringBuilder stringBuilder = new StringBuilder();
        int days = Integer.parseInt(end[0]);
        int hours = Integer.parseInt(end[1]);
        int minutes = Integer.parseInt(end[2]);
        int seconds = Integer.parseInt(end[3]);
        if (days != 0) {
            if (language == Language.LanguageType.GERMAN) {
                append = days == 1 ? days + " Tag " : days + " Tage ";
            } else {
                append = days == 1 ? days + " day " : days + " days ";
            }
            stringBuilder.append(append);
        }
        if (hours != 0) {
            if (language == Language.LanguageType.GERMAN) {
                append = hours == 1 ? hours + " Stunde " : hours + " Stunden ";
            } else {
                append = hours == 1 ? hours + " hour " : hours + " hours ";
            }
            stringBuilder.append(append);
        }
        if (minutes != 0) {
            if (language == Language.LanguageType.GERMAN) {
                append = minutes == 1 ? minutes + " Minute " : minutes + " Minuten ";
            } else {
                append = minutes == 1 ? minutes + " minute " : minutes + " minutes ";
            }
            stringBuilder.append(append);
        }
        if (seconds != 0) {
            if (language == Language.LanguageType.GERMAN) {
                append = seconds == 1 ? seconds + " Sekunde" : seconds + " Sekunden";
            } else {
                append = seconds == 1 ? seconds + " second" : seconds + " seconds";
            }
            stringBuilder.append(append);
        }
        return stringBuilder.toString();
    }

    private String[] getRemainingTime(long milli) {
        long time = System.currentTimeMillis() > milli ? System.currentTimeMillis() - milli : milli - System.currentTimeMillis();
        int days = 0;
        int hours = 0;
        int minutens = 0;
        int seconds = 0;
        if ((time /= 1000) >= 86400) {
            days = (int)time / 86400;
        }
        if ((time -= (days * 86400)) > 3600) {
            hours = (int)time / 3600;
        }
        if ((time -= (hours * 3600)) > 60) {
            minutens = (int)time / 60;
        }
        seconds = (int)(time -= (minutens * 60));
        String[] al = new String[]{Integer.toString(days), Integer.toString(hours), Integer.toString(minutens), Integer.toString(seconds)};
        return al;
    }
}

