package net.vicemice.hector.manager.database;

import io.netty.channel.Channel;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.types.player.database.PlayerDatabasePacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.packets.PacketType;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.UUID;

/*
 * Class created at 02:32 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class DatabaseManager {
    public void getDataFromPlayer(UUID uniqueId, String key, String callBackID, Channel channel) {
        key = key.toLowerCase();
        CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uniqueId);
        if (cloudPlayer == null) {
            return;
        }
        String data = "";
        if (cloudPlayer.getPlayerDB().containsKey(key)) {
            data = cloudPlayer.getPlayerDB().get(key);
        }
        channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_DATABASE_PACKET, new PlayerDatabasePacket(callBackID, data)));
    }

    public void savePlayerData(UUID uniqueId, String key, String value) {
        key = key.toLowerCase();
        CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uniqueId);
        if (cloudPlayer == null) {
            return;
        }
        boolean insert = false;
        if (!cloudPlayer.getPlayerDB().containsKey(key)) {
            insert = true;
        }
        cloudPlayer.getPlayerDB().put(key, value);
        try {
            if (insert) {
                Document document = new Document("player", String.valueOf(cloudPlayer.getUniqueId()));
                document.append("dbvalue", value);
                document.append("dbkey", key);

                CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("playerdb").insertOne(document);
                return;
            }
            Document document = new Document("dbvalue", value);
            document.append("dbkey", key);

            Bson filter = new Document("player", String.valueOf(cloudPlayer.getUniqueId()));
            CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("playerdb").updateOne(filter, new Document("$set", document));
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void removePlayerData(UUID uniqueId, String key) {
        key = key.toLowerCase();
        CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uniqueId);
        if (cloudPlayer == null) {
            return;
        }
        cloudPlayer.getPlayerDB().remove(key);
        try {
            Bson filter = new Document("player", String.valueOf(cloudPlayer.getUniqueId())).append("dbkey", key);
            CloudService.getCloudService().getManagerService().getMongoManager().getMongoDatabase().getCollection("playerdb").deleteOne(filter);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }
}