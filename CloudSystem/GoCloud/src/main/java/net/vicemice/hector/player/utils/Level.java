package net.vicemice.hector.player.utils;

import lombok.Data;
import java.io.Serializable;

/*
 * Class created at 14:10 - 07.04.2020
 * Copyright (C) elrobtossohn
 */
@Data
public class Level implements Serializable {

    private long level, experience;

    public Level(long level, long experience) {
        this.level = level;
        this.experience = experience;
    }

    public void addXp(long xp) {
        this.experience = (this.experience+xp);
    }

    public void removeXp(long xp) {
        if ((experience-xp) < 0)
            this.experience = 0;
        else
            this.experience = (this.experience-xp);
    }

    public long calculateLevel() {
        return (5 * (this.level ^ 2) + 50 * this.level + 100);
    }

    public long calculateNextLevel() {
        return (5 * ((this.level+1) ^ 2) + 50 * (this.level+1) + 100);
    }
}
