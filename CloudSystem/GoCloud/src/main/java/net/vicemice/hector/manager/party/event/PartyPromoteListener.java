package net.vicemice.hector.manager.party.event;

import net.vicemice.hector.network.modules.api.event.player.party.PartyPromoteEvent;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.party.Party;
import net.vicemice.hector.utils.event.IEventListener;

/*
 * Class created at 07:14 - 29.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PartyPromoteListener implements IEventListener<PartyPromoteEvent> {

    @Override
    public void onCall(PartyPromoteEvent event) {
        CloudPlayer leader = event.getLeader(), target = event.getTarget();
        Party party = leader.getParty();

        if (target != leader) {
            if (party.getMembers().contains(target)) {
                party.setNewPartyLeader(target);
            } else {
                leader.sendMessage("party-player-not-in", (target.getRank().getRankColor()+target.getName()));
            }
        } else {
            leader.sendMessage("player-self-interact", leader.getLanguageMessage("party-prefix"));
        }
    }
}
