package net.vicemice.hector.utils.utility.scheduler;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class TaskEntryFuture<T> implements Future<T> {
    private TaskEntry<T> entry;
    protected volatile boolean waits;

    @Override
    public boolean cancel(boolean pMayInterruptIfRunning) {
        if (pMayInterruptIfRunning) {
            this.entry.task = null;
            this.entry.repeat = 0L;
        }
        return true;
    }

    @Override
    public boolean isCancelled() {
        return this.entry.task == null;
    }

    @Override
    public boolean isDone() {
        return this.entry.completed;
    }

    @Override
    public synchronized T get() throws InterruptedException, ExecutionException {
        this.waits = true;
        while (!this.isDone()) {
            this.wait();
        }
        return this.entry.value;
    }

    @Override
    public synchronized T get(long pTimeout, TimeUnit pUnit) throws InterruptedException, ExecutionException, TimeoutException {
        this.waits = true;
        while (!this.isDone()) {
            this.wait(pUnit.toMillis(pTimeout));
        }
        return this.entry.value;
    }

    public TaskEntry<T> getEntry() {
        return this.entry;
    }

    public boolean isWaits() {
        return this.waits;
    }

    public TaskEntryFuture(TaskEntry<T> entry, boolean waits) {
        this.entry = entry;
        this.waits = waits;
    }
}

