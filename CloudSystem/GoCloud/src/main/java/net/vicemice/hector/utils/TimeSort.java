package net.vicemice.hector.utils;

import lombok.Data;

import java.util.UUID;

/*
 * Class created at 14:50 - 05.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class TimeSort {

    public UUID uniqueId;
    public long time;

    public TimeSort(UUID uniqueId, long time) {
        this.uniqueId = uniqueId;
        this.time = time;
    }
}