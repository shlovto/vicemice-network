package net.vicemice.hector.manager.slave;

import io.netty.channel.Channel;
import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.slave.Slave;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.ErrorPacket;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.slave.SlaveInitPacket;
import net.vicemice.hector.packets.types.slave.SlaveTemplatePacket;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

public class SlaveManager {
    @Getter
    private List<Slave> slaveList = new CopyOnWriteArrayList<Slave>();

    public SlaveManager() {
        new Thread(() -> {
            new Timer().scheduleAtFixedRate(new TimerTask() {

                @Override
                public void run() {
                    SlaveManager.this.checkTimeOut();
                }
            }, 1000, 1000);
        }).start();
    }

    public Slave getSlaveByName(String name) {
        for (Slave slave : this.slaveList) {
            if (!slave.getName().equalsIgnoreCase(name)) continue;
            return slave;
        }
        return null;
    }

    public void removeNodeByName(String name) {
        for (Slave slave : this.slaveList) {
            if (!slave.getName().equalsIgnoreCase(name)) continue;
            slave.remove();
            this.slaveList.remove(name);
        }
    }

    public void checkTimeOut() {
        for (Slave slave : this.slaveList) {
            if (System.currentTimeMillis() <= slave.getTimeout()) continue;
            CloudService.getCloudService().getTerminal().writeMessage("§e§l"+slave.getName()+" §3has been timed out, stopping all servers");
            //TelegramManager.sendMessage("Node " + node.getName() + " hat die Verbindung verloren! Alle Server wurden gestoppt! (Timed out)");
            slave.remove();
            this.slaveList.remove(slave);
            CloudService.getCloudService().getManagerService().getServerManager().checkServers();
        }
    }

    public void registerSlave(SlaveInitPacket slaveInitPacket, Channel channel) {
        new Thread(() -> {
            for (Slave slave : this.slaveList) {
                if (!slave.getName().equalsIgnoreCase(slaveInitPacket.getName())) continue;
                channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.ERROR_PACKET, new ErrorPacket("This slave name is already registered!")));
                return;
            }
            Slave slave = new Slave(slaveInitPacket.getHost(), slaveInitPacket.getName(), slaveInitPacket.getMaxRam());
            slave.setNettyChannel(channel);
            slave.setTimeout(System.currentTimeMillis() + 20000);
            slave.setForceTemplate(slaveInitPacket.getForceTemplate());
            this.slaveList.add(slave);
            channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.SLAVE_TEMPLATE_PACKET, new SlaveTemplatePacket(CloudService.getCloudService().getManagerService().getServerManager().getTemplates(), true)));

            CloudService.getCloudService().getTerminal().writeMessage("§e§l"+slaveInitPacket.getName()+" §3connected as Slave");
        }).start();
    }

    public void broadcastPacket(PacketHolder packet) {
        this.slaveList.forEach(e -> {
            e.sendPacket(packet);
        });
    }
}