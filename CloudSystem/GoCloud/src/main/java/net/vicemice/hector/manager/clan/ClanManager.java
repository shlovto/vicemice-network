package net.vicemice.hector.manager.clan;

import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.clan.Clan;
import net.vicemice.hector.utils.players.clan.ClanRank;
import org.bson.Document;
import java.util.*;

public class ClanManager {

    @Getter
    private ArrayList<Clan> clans;
    @Getter
    private ArrayList<String> clanNames;
    @Getter
    private ArrayList<String> clanTags;

    public ClanManager() {
        this.clans = new ArrayList<>();
        this.clanNames = new ArrayList<>();
        this.clanTags = new ArrayList<>();
    }

    public void loadAllClansFromDatabase() {

        try {
            ArrayList<Document> list = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("clans");

            for (Document document : list) {
                HashMap<UUID, ClanRank> clanMembers = new HashMap<>();

                if (!document.getString("leaders").isEmpty() && document.getString("leaders") != null) {
                    for (String uuid : document.getString("leaders").split(";")) {
                        clanMembers.put(UUID.fromString(uuid), ClanRank.LEADER);
                    }
                }

                if (!document.getString("moderators").isEmpty() && document.getString("moderators") != null) {
                    for (String uuid : document.getString("moderators").split(";")) {
                        clanMembers.put(UUID.fromString(uuid), ClanRank.MODERATOR);
                    }
                }

                if (!document.getString("members").isEmpty() && document.getString("members") != null) {
                    for (String uuid : document.getString("members").split(";")) {
                        clanMembers.put(UUID.fromString(uuid), ClanRank.MEMBER);
                    }
                }

                List<UUID> invites = new ArrayList<>();

                if (!document.getString("invites").isEmpty() && document.getString("invites") != null) {
                    for (String uuid : document.getString("invites").split(";")) {
                        invites.add(UUID.fromString(uuid));
                    }
                }

                Clan clan = new Clan(document.getString("id"), UUID.fromString(document.getString("uuid")), document.getString("name"), document.getString("tag"), clanMembers, invites);

                for (UUID cloudPlayer : clan.getMembers().keySet()) {
                    CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(cloudPlayer).setClan(clan);
                }

                this.getClans().add(clan);
                this.getClanNames().add(document.getString("name"));
                this.getClanTags().add(document.getString("tag"));
            }

            CloudService.getCloudService().getTerminal().writeMessage("§3Loaded §e§l" + this.clans.size() + " §3Clans");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Clan getClanByName(String name) {
        for (Clan clan : this.getClans()) {
            if (!clan.getName().equalsIgnoreCase(name)) continue;
            return clan;
        }

        return null;
    }

    public Clan getClanByTag(String tag) {
        for (Clan clan : this.getClans()) {
            if (!clan.getTag().equalsIgnoreCase(tag)) continue;
            return clan;
        }

        return null;
    }

    public Clan getClanById(String id) {
        for (Clan clan : this.getClans()) {
            if (!clan.getId().equalsIgnoreCase(id)) continue;
            return clan;
        }

        return null;
    }

    public void handleLogin(CloudPlayer cloudPlayer) {
        new Thread(() -> {
            if (cloudPlayer.getClan() == null) return;
            cloudPlayer.getClan().getMembers().forEach((uniqueId,clanRank) -> {
                CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uniqueId);
                if (getPlayer.equals(cloudPlayer)) return;
                if (getPlayer.isOnline() && getPlayer.getSetting("clan-messages", Integer.class) == 1) {
                    getPlayer.sendMessage("clan-user-is-now-online", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()));
                    getPlayer.updateFriendInv();
                }
            });
        }).start();
    }

    public void handleLogOut(CloudPlayer cloudPlayer) {
        new Thread(() -> {
            if (cloudPlayer.getClan() == null) return;
            cloudPlayer.getClan().getMembers().forEach((uniqueId,clanRank) -> {
                CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uniqueId);
                if (getPlayer.equals(cloudPlayer)) return;
                if (getPlayer.isOnline() && getPlayer.getSetting("clan-messages", Integer.class) == 1) {
                    getPlayer.sendMessage("clan-user-is-now-offline", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()));
                    getPlayer.updateFriendInv();
                }
            });
        }).start();
    }
}