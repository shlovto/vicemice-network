package net.vicemice.hector.utils;

/*
 * Class created at 19:28 - 31.12.2019
 * Copyright (C) 2016-2019 Minetasia Solutions
 */
public interface SaveEntry {

    public void save(boolean insert);
}