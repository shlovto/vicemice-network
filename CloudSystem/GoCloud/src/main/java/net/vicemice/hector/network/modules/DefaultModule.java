package net.vicemice.hector.network.modules;

import net.vicemice.hector.CloudService;

import java.io.InputStream;

public final class DefaultModule {
    private String moduleName;
    private String moduleVersion;

    public InputStream stream() {
        return CloudService.class.getClassLoader().getResourceAsStream("cloud/modules/" + this.moduleName + ".jar");
    }

    public String getModuleName() {
        return this.moduleName;
    }

    public String getModuleVersion() {
        return this.moduleVersion;
    }

    public DefaultModule(String moduleName, String moduleVersion) {
        this.moduleName = moduleName;
        this.moduleVersion = moduleVersion;
    }
}