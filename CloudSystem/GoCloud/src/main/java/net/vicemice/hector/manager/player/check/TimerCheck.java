package net.vicemice.hector.manager.player.check;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.AllowChatPacket;
import net.vicemice.hector.utils.Rank;

import java.util.TimerTask;
import java.util.UUID;

/*
 * Class created at 00:03 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class TimerCheck extends TimerTask {
    @Override
    public void run() {
        CloudPlayer cloudPlayer;
        long currentTime = System.currentTimeMillis();
        if (currentTime - CloudService.getCloudService().getManagerService().getPlayerManager().lastCheck > 60000) {
            CloudService.getCloudService().getManagerService().getPlayerManager().check(CloudService.getCloudService().getManagerService().getPlayerManager(), currentTime);
            try {
                CloudService.getCloudService().getManagerService().getPlayerManager().checkOnlinePlayers();
                for (UUID uniqueId : CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithRank()) {
                    cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uniqueId);
                    //CloudServer.getTerminal().writeMessage("RANKEND: " + cloudPlayer.getName() + " -> " + (System.currentTimeMillis() <= cloudPlayer.getRankEnd()) + " -> " + System.currentTimeMillis() + " -> " + cloudPlayer.getRankEnd());
                    if (cloudPlayer.getRankEnd() <= 0 || currentTime <= cloudPlayer.getRankEnd()) continue;
                    cloudPlayer.setRank(Rank.MEMBER, 0);
                }
                for (UUID uniqueId : CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers()) {
                    cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uniqueId);
                    if (cloudPlayer.getProxy() != null && !cloudPlayer.getProxy().getCloudPlayers().contains(cloudPlayer.getUniqueId())) {
                        cloudPlayer.setProxy(null);
                    }
                    if (cloudPlayer.getServer().isCloudServer() && cloudPlayer.getServer().getTemplateName().startsWith("lobby")) continue;
                    if (cloudPlayer.isAfk()) continue;
                    if (cloudPlayer.getPlayTime() >= 15) continue;
                    if (cloudPlayer.getPlayTime() < 15) continue;
                    cloudPlayer.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.ALLOW_CHAT_PACKET, new AllowChatPacket(cloudPlayer.getName())));
                }
            } catch (Exception exc) {
                exc.printStackTrace();
                CloudService.getCloudService().getTerminal().writeMessage(exc.getLocalizedMessage());
            }
        }
        for (UUID uniqueId : CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithBan()) {
            CloudPlayer cloudPlayer2 = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uniqueId);
            if (cloudPlayer2.getAbuses().getBan() == null) {
                CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithBan().remove(cloudPlayer2.getUniqueId());
                continue;
            }

            long end = cloudPlayer2.getAbuses().getBan().getLength();
            //CloudServer.getTerminal().writeMessage("BAN: " + cloudPlayer2.getName() + " -> " + (System.currentTimeMillis() <= end) + " -> " + System.currentTimeMillis() + " -> " + end);
            if (end == 0 || System.currentTimeMillis() <= end) continue;
            CloudService.getCloudService().getTerminal().writeMessage("Unbanned Player " + cloudPlayer2.getName());
            CloudService.getCloudService().getManagerService().getPlayerManager().getIpBanned().remove(cloudPlayer2.getIp());
            CloudService.getCloudService().getManagerService().getAbuseManager().remove(cloudPlayer2, null, "Ban expired", false);
            //CloudService.getCloudService().getManagerService().getPlayerManager().getBanManager().banLog(cloudPlayer2.getUniqueId(), UUID.fromString("336991c8-1199-457c-80bb-649042a489f5"), PunishType.UNBAN, System.currentTimeMillis(), System.currentTimeMillis(), "");
        }
        for (UUID uniqueId : CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithMute()) {
            cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uniqueId);
            if (cloudPlayer.getAbuses().getMute() == null) {
                CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithMute().remove(cloudPlayer.getUniqueId());
                continue;
            }
            long end = cloudPlayer.getAbuses().getMute().getLength();
            //CloudServer.getTerminal().writeMessage("MUTE: " + cloudPlayer.getName() + " -> " + (System.currentTimeMillis() <= end) + " -> " + System.currentTimeMillis() + " -> " + end);
            if (end == 0 || System.currentTimeMillis() <= end) continue;
            CloudService.getCloudService().getTerminal().writeMessage("Unmuted Player " + cloudPlayer.getName());
            CloudService.getCloudService().getManagerService().getAbuseManager().remove(cloudPlayer, null, "Mute expired", true);
            //CloudService.getCloudService().getManagerService().getAbuseManager().banLog(cloudPlayer.getUniqueId(), UUID.fromString("336991c8-1199-457c-80bb-649042a489f5"), PunishType.UNMUTE, System.currentTimeMillis(), System.currentTimeMillis(), "");
        }
        for (String ipAddress : CloudService.getCloudService().getManagerService().getPlayerManager().getIpBanned().keySet()) {
            Long l = CloudService.getCloudService().getManagerService().getPlayerManager().getIpBanned().get(ipAddress);
            if (l > System.currentTimeMillis()) continue;
            CloudService.getCloudService().getManagerService().getPlayerManager().getIpBanned().remove(ipAddress);
            CloudService.getCloudService().getTerminal().writeMessage("Remove IP-Ban for " + ipAddress);
        }
        for (CloudPlayer cloudPlayer1 : CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().values()) {
            if (cloudPlayer1.getParty() != null) {
                if (cloudPlayer1.getParty().getTimeout() <= System.currentTimeMillis() && cloudPlayer1.getParty().getMembers().size() < 2) {
                    cloudPlayer1.getParty().sendMessageToParty("party-disbanded-auto");
                    cloudPlayer1.getParty().deleteParty();
                }
            }
            if (!cloudPlayer1.getPlayerDB().containsKey("vote_streak")) continue;
            if (System.currentTimeMillis() > Long.parseLong(cloudPlayer1.getPlayerDB().get("vote_streak"))) {
                CloudService.getCloudService().getManagerService().getStatsManager().setPlayerStat(cloudPlayer1, "vote_streak", 0);
                CloudService.getCloudService().getManagerService().getDatabaseManager().removePlayerData(cloudPlayer1.getUniqueId(), "vote_streak");
            }
        }
    }
}