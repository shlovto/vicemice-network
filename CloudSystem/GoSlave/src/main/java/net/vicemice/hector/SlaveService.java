package net.vicemice.hector;

import lombok.Data;
import lombok.Getter;
import net.vicemice.hector.packets.types.slave.SlaveStopPacket;
import net.vicemice.hector.manager.ManagerService;
import net.vicemice.hector.netty.NettyService;
import net.vicemice.hector.packets.PacketHelper;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.utils.terminal.Terminal;
import net.vicemice.hector.utils.terminal.log.SystemLogger;
import java.util.concurrent.ConcurrentHashMap;

/*
 * Class created at 22:38 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class SlaveService {

    @Getter
    private static SlaveService slaveService;
    @Getter
    private static String version = "4.2.2";

    private long startTime;
    private boolean ready, started, devMode, closeLock;

    private Terminal terminal;
    private SystemLogger logger;
    
    private PacketHelper packetHelper;
    private ManagerService managerService;
    private ConcurrentHashMap<Long, Double> cpuAverage;
    private NettyService nettyService;

    public static boolean newService() {
        SlaveService service = new SlaveService();
        service.setReady(true);
        service.setStarted(false);
        service.setStartTime(System.currentTimeMillis());
        service.setCloseLock(false);
        service.setCpuAverage(new ConcurrentHashMap<>());
        service.setPacketHelper(new PacketHelper());
        service.setNettyService(NettyService.newService());
        service.setManagerService(ManagerService.newService());

        if (slaveService != null) {
            return false;
        }

        slaveService = service;

        return true;
    }

    public void start() {
        Runtime.getRuntime().addShutdownHook(new Thread(SlaveService::shutdown));
        this.getManagerService().run();
    }

    public static void shutdown() {
        SlaveService.getSlaveService().getTerminal().write("§b§lGoNet §cis going to shutdown...");
        if (SlaveService.getSlaveService().getNettyService().getSlaveClient().getChannel() != null && SlaveService.getSlaveService().getNettyService().getSlaveClient().getChannel().isActive() && SlaveService.getSlaveService().getNettyService().getSlaveClient().getChannel().isOpen()) {
            SlaveService.getSlaveService().getNettyService().getSlaveClient().getChannel().writeAndFlush(SlaveService.getSlaveService().getPacketHelper().preparePacket(PacketType.SLAVE_STOP, new SlaveStopPacket(SlaveService.getSlaveService().getManagerService().getConfigManager().getConfig().getName())));
        }
        SlaveService.getSlaveService().getManagerService().getServerManager().killNode();
    }
}