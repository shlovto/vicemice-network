package net.vicemice.hector.utils.terminal;

/*
 * Class created at 20:19 - 31.12.2019
 * Copyright (C) 2016-2019 Minetasia Solutions
 */
public class Debugger {
    public static void printMessage(String message) {
        System.out.println("[Debugger] [" + Debugger.getLastCallerClass(new String[0]) + "] -> " + message);
    }

    public static String getLastCallerClass(String ... ex) {
        StackTraceElement[] stack = Thread.currentThread().getStackTrace();
        block0 : for (int i = 0; i < stack.length; ++i) {
            StackTraceElement currunt = stack[i];
            if (currunt.getClassName().equalsIgnoreCase(Debugger.class.getName()) || currunt.getClassName().equalsIgnoreCase("java.lang.Thread") || currunt.getClassName().contains("dev.wolveringer.dataserver.terminal.")) continue;
            for (String e : ex) {
                if (currunt.getClassName().startsWith(e)) continue block0;
            }
            return String.valueOf(currunt.getClassName()) + ":" + currunt.getLineNumber();
        }
        return "unknown:-1";
    }
}

