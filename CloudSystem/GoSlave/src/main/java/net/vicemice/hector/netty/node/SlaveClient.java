package net.vicemice.hector.netty.node;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.SlaveService;

/*
 * Class created at 22:50 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SlaveClient implements Runnable {

    private String host;
    private int port;

    @Getter
    @Setter
    private Channel channel;

    public SlaveClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    @Getter
    private EventLoopGroup eventLoopGroup;

    @Override
    public void run() {
        NioEventLoopGroup eventLoopGroup = new NioEventLoopGroup();
        try {
            try {
                Bootstrap bootstrap = new Bootstrap();
                this.eventLoopGroup = eventLoopGroup;
                ((bootstrap.group(eventLoopGroup)).channel(NioSocketChannel.class)).handler(new ChannelInitializer<SocketChannel>(){

                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        socketChannel.pipeline().addLast(new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(this.getClass().getClassLoader())));
                        socketChannel.pipeline().addLast(new ObjectEncoder());
                        socketChannel.pipeline().addLast(new SlaveHandler());
                    }
                });
                ChannelFuture channelFuture = bootstrap.connect(host, port);
                this.channel = channelFuture.channel();
                ChannelFuture closeFuture = this.channel.closeFuture();
                closeFuture.addListener(channelFuture1 -> {
                    SlaveService.getSlaveService().getTerminal().write("§cCould not reach the cloud, retrying...");
                    Thread.sleep(2000);
                    SlaveService.getSlaveService().getManagerService().getServerManager().killNode();
                    SlaveService.getSlaveService().getNettyService().startSlaveClient();
                    eventLoopGroup.shutdownGracefully();
                });
                closeFuture.sync();
            }
            catch (Exception exc) {
                exc.printStackTrace();
                eventLoopGroup.shutdownGracefully();
            }
        }
        finally {
            eventLoopGroup.shutdownGracefully();
        }
    }
}