package net.vicemice.hector.netty.log;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import net.vicemice.hector.SlaveService;

/*
 * Class created at 23:03 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class LogHandler extends SimpleChannelInboundHandler<Object> {

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        SlaveService.getSlaveService().getNettyService().getLogClient().setUseChannel(ctx.channel());
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object o) throws Exception {
    }
}
