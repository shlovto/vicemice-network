package net.vicemice.hector.netty;

import lombok.Data;
import net.vicemice.hector.SlaveService;
import net.vicemice.hector.netty.log.LogClient;
import net.vicemice.hector.netty.node.SlaveClient;

/*
 * Class created at 22:45 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class NettyService {

    private LogClient logClient;
    private SlaveClient slaveClient;

    public static NettyService newService() {
        return new NettyService();
    }

    public void startSlaveClient() {
        this.setSlaveClient(new SlaveClient(SlaveService.getSlaveService().getManagerService().getConfigManager().getConfig().getCloudHost(), SlaveService.getSlaveService().getManagerService().getConfigManager().getConfig().getCloudPort()));
        new Thread(this.getSlaveClient()).start();
    }

    public void startLogClient() {
        this.setLogClient(new LogClient(SlaveService.getSlaveService().getManagerService().getConfigManager().getConfig().getCloudHost(), SlaveService.getSlaveService().getManagerService().getConfigManager().getConfig().getLogPort()));
        new Thread(this.getLogClient()).start();
    }
}