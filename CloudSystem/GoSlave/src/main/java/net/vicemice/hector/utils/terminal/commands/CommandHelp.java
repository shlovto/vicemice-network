package net.vicemice.hector.utils.terminal.commands;

import net.vicemice.hector.utils.terminal.ArgumentList;
import net.vicemice.hector.utils.terminal.CommandExecutor;
import net.vicemice.hector.utils.terminal.CommandRegistry;
import net.vicemice.hector.utils.terminal.Terminal;
import org.apache.commons.lang3.StringUtils;

public class CommandHelp implements CommandExecutor {
    @Override
    public void onCommand(String command, Terminal writer, String[] args) {
        writer.writeMessage("\u00a7aCommands: ");
        for (CommandRegistry.CommandHolder e : CommandRegistry.getCommands()) {
            writer.writeMessage("  \u00a77- \u00a7a/" + e.getCommand() + (e.getAlias().isEmpty() ? "" : " \u00a77| \u00a7a/" + StringUtils.join(e.getAlias(), " \u00a77| \u00a7a/")));
        }
    }

    @Override
    public ArgumentList getArguments() {
        return ArgumentList.builder().arg(new ArgumentList.Argument("/help", "Shows this help page.")).build();
    }
}