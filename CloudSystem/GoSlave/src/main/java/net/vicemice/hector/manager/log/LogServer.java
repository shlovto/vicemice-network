package net.vicemice.hector.manager.log;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ResourceHandler;

/*
 * Class created at 23:35 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class LogServer {

    public void startLogServer() {
        new Thread(() -> {
            Server server = new Server(1965);
            ResourceHandler resourceHandler = new ResourceHandler();
            resourceHandler.setResourceBase("/opt/network/Slave/slave/logs");
            resourceHandler.setDirectoriesListed(true);
            ContextHandler contextHandler = new ContextHandler("/");
            contextHandler.setHandler(resourceHandler);
            server.setHandler(contextHandler);
            try {
                server.start();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            try {
                server.join();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }
}