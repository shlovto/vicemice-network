package net.vicemice.hector.manager.template;

import lombok.Getter;
import net.vicemice.hector.utils.slave.TemplateData;
import net.vicemice.hector.SlaveService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
 * Class created at 23:11 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class TemplateDownloader implements Runnable {

    int current = 0;
    @Getter
    private List<TemplateData> templates = new ArrayList<>();

    public TemplateDownloader(List<TemplateData> templates) {
        this.templates = templates;
        SlaveService.getSlaveService().setReady(false);
        SlaveService.getSlaveService().getManagerService().getTemplateManager().setTemplateDownloader(this);
    }

    @Override
    public void run() {
        this.downloadFile(this.current);
    }

    public synchronized void addTemplates(List<TemplateData> templates) {
        this.templates.addAll(new ArrayList<>(templates));
    }

    public void downloadFile(int id) {
        if (this.current == this.templates.size()) {
            SlaveService.getSlaveService().getTerminal().write("§6All Files downloaded, the Slave is ready to work.");
            SlaveService.getSlaveService().setReady(true);
            SlaveService.getSlaveService().getManagerService().getTemplateManager().setTemplateDownloader(null);
            return;
        }
        TemplateData serverTemplate = this.templates.get(id);
        if (!serverTemplate.isNewDownload()) {
            ++this.current;
            this.downloadFile(this.current);
            return;
        }
        try {
            Runtime.getRuntime().exec("rm templates/" + serverTemplate.getName() + ".zip").waitFor();
            Runtime.getRuntime().exec("rm -r templates/" + serverTemplate.getName()).waitFor();
            SlaveService.getSlaveService().getTerminal().write("§3Downloading §e§l"+ serverTemplate.getName()+"§3...");
            String command = "wget --quiet -O templates/" + serverTemplate.getName() + ".zip --http-user=Clyde --http-password=3nfLcPt6dsvJTJpw https://acp.vicemice.net/K3q6P5vzMWG8Zz7dWgwvXnFN3cPy2yqBVSC7HD8uU5BRJRvwgJzc6vPVCFgV8rjd/" + serverTemplate.getName() + ".zip";
            Process downloadProcess = Runtime.getRuntime().exec(command);
            downloadProcess.waitFor();
            Process unzip = Runtime.getRuntime().exec("unzip templates/" + serverTemplate.getName() + ".zip -d templates");
            unzip.waitFor();
            ++this.current;
            this.downloadFile(this.current);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}