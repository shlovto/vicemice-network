package net.vicemice.hector.manager.server;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.utils.ServerType;

/*
 * Class created at 22:59 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ServerData {
    @Getter
    private String name;
    @Getter
    private String id;
    @Getter
    private String template;
    @Getter
    private int port;
    @Getter
    @Setter
    private Process process;
    @Getter
    private Thread thread;
    @Getter
    private ServerType serverType;

    @Getter
    @Setter
    private int processID;

    private boolean removeSended = false;

    public ServerData(String name, String id, String template, int port, ServerType serverType) {
        this.name = name;
        this.id = id;
        this.template = template;
        this.port = port;
        this.serverType = serverType;
    }
}