package net.vicemice.hector.utils.terminal.commands;

import net.vicemice.hector.SlaveService;
import net.vicemice.hector.utils.terminal.ArgumentList;
import net.vicemice.hector.utils.terminal.CommandExecutor;
import net.vicemice.hector.utils.terminal.Terminal;

public class CommandEnd implements CommandExecutor {

    @Override
    public void onCommand(String command, Terminal writer, String[] args) {
        SlaveService.shutdown();
        System.exit(1);
    }

    @Override
    public ArgumentList getArguments() {
        return ArgumentList.builder().arg(new ArgumentList.Argument("/end", "Stops the application")).build();
    }
}