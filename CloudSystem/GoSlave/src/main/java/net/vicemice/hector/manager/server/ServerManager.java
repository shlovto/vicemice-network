package net.vicemice.hector.manager.server;

import lombok.Getter;
import net.vicemice.hector.SlaveService;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.slave.LogPacket;
import net.vicemice.hector.utils.ServerType;
import net.vicemice.hector.utils.slave.TemplateData;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/*
 * Class created at 22:55 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ServerManager {
    private ConcurrentHashMap<String, ServerData> servers = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, Long> logTimeouts = new ConcurrentHashMap<>();
    private List<ServerData> slaveServerStarts = Collections.synchronizedList(new ArrayList<>());
    @Getter
    private ServerData currentStarting;

    public ServerManager() {

    }

    public void startServer(ServerData serverData) {
        new Thread(() -> {
            if (serverData.getTemplate() != null) {
                TemplateData serverTemplate = SlaveService.getSlaveService().getManagerService().getTemplateManager().getTemplates().get(serverData.getTemplate());
                try {
                    SlaveService.getSlaveService().getTerminal().write("§e§l"+ serverData.getName()+" §3will be created");

                    Process delete = Runtime.getRuntime().exec(new String[]{"bash", "-c", "rm -r servers/"+serverData.getName()});
                    delete.waitFor();

                    Process create = Runtime.getRuntime().exec(new String[]{"bash", "-c", "mkdir servers/"+serverData.getName()});
                    create.waitFor();
                    Process copy = Runtime.getRuntime().exec(new String[]{"bash", "-c", "cp templates/"+serverTemplate.getName()+"/* -R servers/"+serverData.getName()});
                    copy.waitFor();
                    Process copyServerStuff = Runtime.getRuntime().exec(new String[]{"bash", "-c", "cp slave/servers/* -R servers/" + serverData.getName()});
                    copyServerStuff.waitFor();

                    String[] startArray = serverTemplate.getStartCommand().split(" ");
                    startArray[startArray.length - 1] = String.valueOf(serverData.getPort());
                    ProcessBuilder processBuilder = new ProcessBuilder(startArray);
                    processBuilder.directory(new File("servers/" + serverData.getName()));
                    Process process = processBuilder.start();
                    serverData.setProcess(process);

                    Class<?> cProcessImpl = process.getClass();
                    Field fPid = cProcessImpl.getDeclaredField("pid");
                    if (!fPid.isAccessible()) {
                        fPid.setAccessible(true);
                    }
                    int processID = fPid.getInt(process);
                    serverData.setProcessID(processID);
                    servers.put(serverData.getName().toLowerCase(), serverData);

                    SlaveService.getSlaveService().getTerminal().write("§e§l"+ serverData.getName()+" §3has been started on §e§l"+serverData.getPort()+" §8(§e§lPID: "+processID+"§8)");
                } catch (IOException | IllegalAccessException | InterruptedException | NoSuchFieldException e) {
                    e.printStackTrace();
                } finally {
                    currentStarting = null;
                }
            } else {
                try {
                    SlaveService.getSlaveService().getTerminal().write("§3Try to start §e§l"+ serverData.getName()+" §3and copy Plugins...");

                    Process createPluginFolder = Runtime.getRuntime().exec(new String[]{"bash", "-c", "mkdir statics/"+serverData.getName()+"/plugins"});
                    createPluginFolder.waitFor();
                    Process copyPlugins = Runtime.getRuntime().exec(new String[]{"bash", "-c", "cp slave/plugins/* -R statics/" + serverData.getName() + "/plugins"});
                    copyPlugins.waitFor();

                    String[] startArray = "java -jar server.jar -p %port%".split(" ");
                    startArray[startArray.length - 1] = String.valueOf(serverData.getPort());
                    ProcessBuilder processBuilder = new ProcessBuilder(startArray);
                    processBuilder.directory(new File("statics/" + serverData.getName()));
                    Process process = processBuilder.start();
                    serverData.setProcess(process);

                    Class<?> cProcessImpl = process.getClass();
                    Field fPid = cProcessImpl.getDeclaredField("pid");
                    if (!fPid.isAccessible()) {
                        fPid.setAccessible(true);
                    }
                    int processID = fPid.getInt(process);
                    serverData.setProcessID(processID);
                    servers.put(serverData.getName().toLowerCase(), serverData);

                    SlaveService.getSlaveService().getTerminal().write("§e§l"+ serverData.getName()+" §3has been started on §e§l"+serverData.getPort()+" §8(§e§lPID: "+processID+"§8)");
                } catch (IOException | IllegalAccessException | InterruptedException | NoSuchFieldException e) {
                    e.printStackTrace();
                } finally {
                    currentStarting = null;
                }
            }
        }).start();
    }


    public void killNode() {
        if (this.servers.size() != 0) {
            Iterator stringIterator = this.servers.keySet().iterator();
            while (stringIterator.hasNext()) {
                String name = (String) stringIterator.next();
                this.stopServer(name, false);
                stringIterator.remove();
            }
        }
    }

    public void saveLog(ServerData serverData) {
        new Thread(() -> {
            try {
                Runtime.getRuntime().exec("cp servers/" + serverData.getName() + "/logs/latest.log slave/logs/" + serverData.getId()).waitFor();
                this.logTimeouts.put(serverData.getId(), System.currentTimeMillis() + 60000);
                if (SlaveService.getSlaveService().getNettyService().getSlaveClient().getChannel() != null) {
                    SlaveService.getSlaveService().getNettyService().getLogClient().getUseChannel().writeAndFlush(SlaveService.getSlaveService().getPacketHelper().preparePacket(PacketType.LOG_PACKET, new LogPacket(SlaveService.getSlaveService().getManagerService().getConfigManager().getConfig().getName(), serverData.getId(), SlaveService.getSlaveService().getManagerService().getConfigManager().getConfig().getHost())));
                    System.out.println("Log for Server " + serverData.getId() + " is ready to download!");
                }
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
        ).start();
    }

    public void stopServer(String name, boolean delete) {
        if (this.servers.containsKey(name.toLowerCase())) {
            ServerData serverData = this.servers.get(name.toLowerCase());
            if (serverData.getServerType() == ServerType.CLOUD_SERVER) {
                int processID = serverData.getProcessID();
                if (delete) {
                    this.servers.remove(name);
                }
                try {
                    Process kill = Runtime.getRuntime().exec("kill -9 " + processID);
                    kill.waitFor();
                    this.saveLog(serverData);
                    Process deleteProcess = Runtime.getRuntime().exec(new String[]{"bash", "-c", "rm -r servers/" + serverData.getName()});
                    deleteProcess.waitFor();
                    System.out.println("The Server " + name + " will be stopped... (" + processID + ")");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else if (serverData.getServerType() == ServerType.STATIC) {
                int processID = serverData.getProcessID();
                this.servers.remove(name);
                try {
                    Process kill = Runtime.getRuntime().exec("kill -9 " + processID);
                    kill.waitFor();
                    this.saveLog(serverData);
                    System.out.println("The Server " + name + " will be stopped... (" + processID + ")");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.out.println("Cannot stop Server '"+name+"'");
        }
    }

    public ConcurrentHashMap<String, ServerData> getServers() {
        return this.servers;
    }

    public ConcurrentHashMap<String, Long> getLogTimeouts() {
        return this.logTimeouts;
    }

    public List<ServerData> getSlaveServerStarts() {
        return this.slaveServerStarts;
    }

    static void access$2(ServerManager serverManager, ServerData serverData) {
        serverManager.currentStarting = serverData;
    }
}