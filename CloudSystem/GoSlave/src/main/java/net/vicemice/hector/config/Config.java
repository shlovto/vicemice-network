package net.vicemice.hector.config;

import lombok.Data;

/**
 * Created by Paul B. on 27.01.2019.
 */
@Data
public class Config {
    private String cloudHost = "minetasia.net";
    private int cloudPort = 1389;
    private int logPort = 1371;
    private String host = "localhost";
    private String name = "Slave-01";
    private Integer maxRam = 1024;
}