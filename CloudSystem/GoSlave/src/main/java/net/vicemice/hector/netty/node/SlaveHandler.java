package net.vicemice.hector.netty.node;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import net.vicemice.hector.utils.slave.TemplateData;
import net.vicemice.hector.SlaveService;
import net.vicemice.hector.manager.server.ServerData;
import net.vicemice.hector.manager.template.TemplateDownloader;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.ErrorPacket;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.slave.*;
import java.util.ArrayList;

/*
 * Class created at 22:51 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SlaveHandler extends SimpleChannelInboundHandler<Object> {

    private Channel channel;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        SlaveInitPacket slaveInitPacket = new SlaveInitPacket(SlaveService.getSlaveService().getManagerService().getConfigManager().getConfig().getHost(), SlaveService.getSlaveService().getManagerService().getConfigManager().getConfig().getName(), SlaveService.getSlaveService().getManagerService().getConfigManager().getConfig().getMaxRam());
        slaveInitPacket.setForceTemplate(null);
        PacketHolder initPacket = SlaveService.getSlaveService().getPacketHelper().preparePacket(PacketType.SLAVE_INIT, slaveInitPacket);
        SlaveService.getSlaveService().getTerminal().write("§aSlave connected to the Cloud");
        SlaveService.getSlaveService().getManagerService().initDirectories();
        ctx.channel().writeAndFlush(initPacket);
        SlaveService.getSlaveService().getNettyService().getSlaveClient().setChannel(ctx.channel());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        SlaveService.getSlaveService().getTerminal().write("§cChannel goes inactive, stopping network");
        if (SlaveService.getSlaveService().getNettyService().getSlaveClient() != null && SlaveService.getSlaveService().getNettyService().getSlaveClient().getEventLoopGroup() != null) {
            SlaveService.getSlaveService().getNettyService().getSlaveClient().getEventLoopGroup().shutdownGracefully();
        }
        System.exit(0);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object o) throws Exception {
        PacketHolder initPacket = (PacketHolder)o;
        if (initPacket.getKey().equals(PacketType.SLAVE_TEMPLATE_PACKET.getKey())) {
            SlaveTemplatePacket slaveTemplatePacket = (SlaveTemplatePacket) initPacket.getValue();
            ArrayList<TemplateData> localTemplates = new ArrayList<>();
            for (TemplateData serverTemplate : slaveTemplatePacket.getTemplates()) {
                //ServerTemplate serverTemplate = new ServerTemplate(array[0], array[1], "USER", 0, 0, 0, Integer.valueOf(array[2]), Integer.valueOf(array[3]), 0, 0, null, array[5], false);
                /*if (Boolean.valueOf(array[4]).booleanValue()) {
                    serverTemplate.setNewDownload(true);
                }*/
                serverTemplate.setNewDownload(true);
                localTemplates.add(serverTemplate);
                SlaveService.getSlaveService().getManagerService().getTemplateManager().getTemplates().put(serverTemplate.getName(), serverTemplate);
            }
            if (!slaveTemplatePacket.isDownload()) {
                SlaveService.getSlaveService().getTerminal().write("§cReceived Template Update, implementing...");
                return;
            }
            System.out.println("A new Template will be downloaded...");
            SlaveService.getSlaveService().getTerminal().write("§6Download new Templates..");
            if (SlaveService.getSlaveService().getManagerService().getTemplateManager().getTemplateDownloader() != null) {
                SlaveService.getSlaveService().getTerminal().write("§6Add new Download to Queue");
                SlaveService.getSlaveService().getManagerService().getTemplateManager().getTemplateDownloader().addTemplates(localTemplates);
            } else {
                SlaveService.getSlaveService().getTerminal().write("§6Create a new Download-Thread");
                new Thread(new TemplateDownloader(localTemplates)).start();
            }
        } else if (initPacket.getKey().equals(PacketType.SLAVE_SERVER_START.getKey())) {
            SlaveServerStart slaveServerStart = (SlaveServerStart) initPacket.getValue();
            ServerData serverData = new ServerData(slaveServerStart.getName(), slaveServerStart.getId(), slaveServerStart.getServerTemplate(), slaveServerStart.getPort(), slaveServerStart.getServerType());
            SlaveService.getSlaveService().getManagerService().getServerManager().getSlaveServerStarts().add(serverData);
        } else if (initPacket.getKey().equals(PacketType.SLAVE_STATICS_PACKET.getKey())) {
            SlaveStaticsPacket slaveStaticsPacket = (SlaveStaticsPacket)initPacket.getValue();

            //TODO: Statics Server start
        } else if (initPacket.getKey().equals(PacketType.SLAVE_PROXYS_PACKET.getKey())) {
            SlaveProxysPacket slaveProxysPacket = (SlaveProxysPacket)initPacket.getValue();
            //ServerData serverData = new ServerData(slaveProxysPacket.getName(), wrapperServerStart.getId(), wrapperServerStart.getServerTemplate(), wrapperServerStart.getPort(), true);
            //NodeService.getNodeService().getManagerService().getServerManager().getWrapperServerStarts().add(serverData);


        } else if (initPacket.getKey().equals(PacketType.SLAVE_SERVER_KILL.getKey())) {
            SlaveServerKill slaveServerKill = (SlaveServerKill)initPacket.getValue();
            SlaveService.getSlaveService().getManagerService().getServerManager().stopServer(slaveServerKill.getName(), true);
        } else if (initPacket.getKey().equals(PacketType.ERROR_PACKET.getKey())) {
            ErrorPacket errorPacket = (ErrorPacket)initPacket.getValue();
            System.out.println(errorPacket.getErrorMessage());
            System.exit(0);
        } else if (initPacket.getKey().equals(PacketType.SAVE_LOG_PACKET.getKey())) {
            SaveLogPacket saveLogPacket = (SaveLogPacket)initPacket.getValue();
            if (SlaveService.getSlaveService().getManagerService().getServerManager().getServers().containsKey(saveLogPacket.getServerName().toLowerCase())) {
                SlaveService.getSlaveService().getManagerService().getServerManager().saveLog(SlaveService.getSlaveService().getManagerService().getServerManager().getServers().get(saveLogPacket.getServerName().toLowerCase()));
            }
        }
    }
}
