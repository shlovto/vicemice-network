package net.vicemice.hector.utils.terminal.log;

import net.vicemice.hector.SlaveService;
import org.eclipse.jetty.util.log.AbstractLogger;
import org.eclipse.jetty.util.log.Logger;
import java.beans.ConstructorProperties;
import java.util.logging.Level;

/*
 * Class created at 20:19 - 31.12.2019
 * Copyright (C) 2016-2019 Minetasia Solutions
 */
public class JettySystemLogger extends AbstractLogger {
    private final String name;

    @Override
    public void warn(String paramString, Object ... paramVarArgs) {
        SlaveService.getSlaveService().getLogger().warning(String.format(paramString, paramVarArgs));
    }

    @Override
    public void warn(Throwable paramThrowable) {
        SlaveService.getSlaveService().getLogger().log(Level.WARNING, paramThrowable.getMessage(), paramThrowable);
    }

    @Override
    public void warn(String paramString, Throwable paramThrowable) {
        SlaveService.getSlaveService().getLogger().log(Level.WARNING, paramString, paramThrowable);
    }

    @Override
    public void info(String paramString, Object ... paramVarArgs) {
        SlaveService.getSlaveService().getLogger().info(String.format(paramString, paramVarArgs));
    }

    @Override
    public void info(Throwable paramThrowable) {
        SlaveService.getSlaveService().getLogger().log(Level.INFO, paramThrowable.getMessage(), paramThrowable);
    }

    @Override
    public void info(String paramString, Throwable paramThrowable) {
        SlaveService.getSlaveService().getLogger().log(Level.INFO, paramString, paramThrowable);
    }

    @Override
    public boolean isDebugEnabled() {
        return Boolean.getBoolean("jetty.debug");
    }

    @Override
    public void setDebugEnabled(boolean paramBoolean) {
        System.setProperty("jetty.debug", String.valueOf(paramBoolean));
    }

    @Override
    public void debug(String paramString, Object ... paramVarArgs) {
        if (this.isDebugEnabled()) {
            this.info(paramString, paramVarArgs);
        }
    }

    @Override
    public void debug(Throwable paramThrowable) {
        if (this.isDebugEnabled()) {
            this.warn(paramThrowable);
        }
    }

    @Override
    public void debug(String paramString, Throwable paramThrowable) {
        if (this.isDebugEnabled()) {
            this.warn(paramString, paramThrowable);
        }
    }

    @Override
    public void ignore(Throwable paramThrowable) {
    }

    @Override
    protected Logger newLogger(String paramString) {
        return new JettySystemLogger(paramString);
    }

    @ConstructorProperties(value={"name"})
    public JettySystemLogger(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }
}

