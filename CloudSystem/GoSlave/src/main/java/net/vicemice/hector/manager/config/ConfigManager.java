package net.vicemice.hector.manager.config;

import lombok.Data;
import net.vicemice.hector.SlaveService;
import net.vicemice.hector.config.Config;
import net.vicemice.hector.config.Configuration;

/*
 * Class created at 22:47 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class ConfigManager {

    private Config config;
    private Configuration configuration;

    public static ConfigManager newManager() {
        ConfigManager configManager = new ConfigManager();

        configManager.setConfiguration(new Configuration());
        if (!configManager.getConfiguration().exists()) {
            Config config = new Config();
            configManager.getConfiguration().writeConfiguration(config);
            SlaveService.getSlaveService().getTerminal().write("§cCreate new Configuration");
        }
        configManager.getConfiguration().readConfiguration();
        configManager.setConfig(configManager.getConfiguration().getConfig());

        return configManager;
    }
}