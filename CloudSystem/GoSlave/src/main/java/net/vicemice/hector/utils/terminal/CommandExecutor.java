package net.vicemice.hector.utils.terminal;

import jline.TerminalFactory;
import net.vicemice.hector.SlaveService;
import org.apache.commons.cli.*;
import java.io.PrintWriter;
import java.util.Arrays;

/*
 * Class created at 20:19 - 31.12.2019
 * Copyright (C) 2016-2019 Minetasia Solutions
 */
public interface CommandExecutor {
    public static final Options options = new Options();
    public static final CommandLineParser optionsParser = new BasicParser();
    public static final HelpFormatter optionsHelper = new HelpFormatter();

    public void onCommand(String var1, Terminal var2, String[] var3);

    public ArgumentList getArguments();

    default public CommandLine paradiseOptions(String[] args, int start) {
        return this.paradiseOptions(args, start, true);
    }

    default public CommandLine paradiseOptions(String[] args, int start, boolean sendHelp) {
        try {
            CommandLine line = optionsParser.parse(options, Arrays.copyOfRange(args, start, args.length));
            if (line == null) {
                throw new Exception("line == null");
            }
            return line;
        }
        catch (Exception e) {
            if (sendHelp) {
                SlaveService.getSlaveService().getTerminal().writeMessage("\u00a7cException: " + e.getMessage());
                optionsHelper.printUsage(new PrintWriter(new CostumSystemPrintStream(SlaveService.getSlaveService().getTerminal())), TerminalFactory.get().getWidth(), "Command Help", options);
            }
            return null;
        }
    }

    default public void printHelp(boolean wrongUsage) {
        if (wrongUsage) {
            SlaveService.getSlaveService().getTerminal().writeMessage("\u00a7cWrong command usage!");
            SlaveService.getSlaveService().getTerminal().writeMessage("\u00a7cAvariable options:");
        }
        if (this.getArguments() != null) {
            for (ArgumentList.Argument s : this.getArguments().getArguments()) {
                SlaveService.getSlaveService().getTerminal().writeMessage(s.format());
            }
        } else {
            SlaveService.getSlaveService().getTerminal().writeMessage("\u00a7cNo argument list/help avariable!");
        }
    }

    default public String createArgumentInfo(String args, String usage) {
        return "\u00a7c" + args + " \u00a77| \u00a7a" + usage;
    }
}

