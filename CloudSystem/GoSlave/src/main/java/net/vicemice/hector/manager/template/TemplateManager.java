package net.vicemice.hector.manager.template;

import lombok.Data;
import net.vicemice.hector.utils.slave.TemplateData;

import java.util.HashMap;

/*
 * Class created at 23:00 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class TemplateManager {

    private HashMap<String, TemplateData> templates;
    private TemplateDownloader templateDownloader;

    public static TemplateManager newManager() {
        TemplateManager templateManager = new TemplateManager();

        templateManager.setTemplates(new HashMap<>());
        templateManager.setTemplateDownloader(null);

        return templateManager;
    }
}