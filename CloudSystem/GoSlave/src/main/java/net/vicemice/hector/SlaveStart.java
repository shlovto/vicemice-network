package net.vicemice.hector;

import net.vicemice.hector.utils.terminal.JLineTerminal;
import net.vicemice.hector.utils.terminal.VanillaTerminal;
import net.vicemice.hector.utils.terminal.log.JettySystemLogger;
import net.vicemice.hector.utils.terminal.log.SystemLogger;
import org.eclipse.jetty.util.log.Log;

/*
 * Class created at 22:38 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SlaveStart {

    public static void main(String[] args) {
        if (!SlaveService.newService()) {
            System.err.println("The Software is already running in a thread, please restart it completely.");
            return;
        }

        if (System.console() != null) {
            System.out.println("Setting up JLineTerminal");
            SlaveService.getSlaveService().setTerminal(new JLineTerminal());
        } else {
            System.out.println("Setting up VanillaTerminal");
            SlaveService.getSlaveService().setTerminal(new VanillaTerminal());
        }
        SlaveService.getSlaveService().getTerminal().install();
        SlaveService.getSlaveService().setLogger(new SystemLogger());
        Log.setLog(new JettySystemLogger("jetty"));

        SlaveService.getSlaveService().getTerminal().writeMessage("§f  ___ ___                 __                ");
        SlaveService.getSlaveService().getTerminal().writeMessage("§f /   |   \\   ____   _____/  |_  ___________ ");
        SlaveService.getSlaveService().getTerminal().writeMessage("§f/    ~    \\_/ __ \\_/ ___\\   __\\/  _ \\_  __ \\");
        SlaveService.getSlaveService().getTerminal().writeMessage("§f\\    Y    /\\  ___/\\  \\___|  | (  <_> )  | \\/");
        SlaveService.getSlaveService().getTerminal().writeMessage("§f \\___|_  /  \\___  >\\___  >__|  \\____/|__|   ");
        SlaveService.getSlaveService().getTerminal().writeMessage("§f       \\/       \\/     \\/                   ");
        SlaveService.getSlaveService().getTerminal().writeMessage("§3Preparing §c§lHector §3Systems");
        SlaveService.getSlaveService().getTerminal().writeMessage("§3Preparing to start the Hector Slave Software §8(§eVersion " + SlaveService.getVersion() + "§8)");
        
        if (!"UTF-8".equalsIgnoreCase(System.getProperty("file.encoding"))) {
            SlaveService.getSlaveService().getTerminal().writeMessage("Changing default file-encoding to UTF-8");
            System.setProperty("file.encoding", "UTF-8");
        }

        SlaveService.getSlaveService().start();
    }
}