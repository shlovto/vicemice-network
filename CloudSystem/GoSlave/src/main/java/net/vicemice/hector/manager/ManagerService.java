package net.vicemice.hector.manager;

import com.sun.management.OperatingSystemMXBean;
import lombok.Data;
import net.vicemice.hector.SlaveService;
import net.vicemice.hector.manager.config.ConfigManager;
import net.vicemice.hector.manager.log.LogServer;
import net.vicemice.hector.manager.server.ServerManager;
import net.vicemice.hector.manager.server.StartManager;
import net.vicemice.hector.manager.template.TemplateManager;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.slave.SlaveAlivePacket;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.util.*;

/*
 * Class created at 22:45 - 04.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class ManagerService {

    private ConfigManager configManager;
    private TemplateManager templateManager;
    private LogServer logServer;
    private ServerManager serverManager;
    private StartManager startManager;

    public static ManagerService newService() {
        ManagerService managerService = new ManagerService();

        managerService.setConfigManager(ConfigManager.newManager());
        managerService.setTemplateManager(TemplateManager.newManager());
        managerService.setLogServer(new LogServer());
        managerService.setServerManager(new ServerManager());
        managerService.setStartManager(new StartManager());

        return managerService;
    }

    public void run() {
        this.initDirectories();
        this.getLogServer().startLogServer();
        SlaveService.getSlaveService().getNettyService().startLogClient();
        SlaveService.getSlaveService().getNettyService().startSlaveClient();

        new Thread(() -> {
            new Timer().scheduleAtFixedRate(new TimerTask() {

                @Override
                public void run() {
                    new Thread(() -> {
                        if (SlaveService.getSlaveService().getNettyService().getSlaveClient().getChannel() != null && SlaveService.getSlaveService().getNettyService().getSlaveClient().getChannel().isOpen() && SlaveService.getSlaveService().getNettyService().getSlaveClient().getChannel().isActive()) {
                            double cpuUsage = ((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getSystemCpuLoad() * 100.0;
                            SlaveService.getSlaveService().getCpuAverage().put(System.currentTimeMillis(), cpuUsage);
                            int usedMemory = 0;
                            int freeMemory = 0;
                            int mb = 1048576;
                            double totalMem = ((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getTotalPhysicalMemorySize() / (long) mb;
                            int totalMemory = (int) totalMem;
                            try {
                                Process getUsedProcess = Runtime.getRuntime().exec(new String[]{"bash", "-c", "awk '/^-/ {print $3}' <(free -m)"});
                                getUsedProcess.waitFor();
                                usedMemory = Integer.parseInt(new BufferedReader(new InputStreamReader(getUsedProcess.getInputStream())).readLine());
                                Process getFreeProcess = Runtime.getRuntime().exec(new String[]{"bash", "-c", "awk '/^-/ {print $4}' <(free -m)"});
                                getFreeProcess.waitFor();
                                freeMemory = Integer.parseInt(new BufferedReader(new InputStreamReader(getFreeProcess.getInputStream())).readLine());
                            } catch (Exception e) {
                                if (SlaveService.getSlaveService().isDevMode()) {
                                    e.printStackTrace();
                                }
                            }
                            double cpuAverageUsage = -1.0;
                            try {
                                ArrayList<Double> cpuAverageCalculate = new ArrayList<Double>();
                                Iterator iterator = SlaveService.getSlaveService().getCpuAverage().keySet().iterator();
                                while (iterator.hasNext()) {
                                    long time = (Long) iterator.next();
                                    long diff = System.currentTimeMillis() - time;
                                    if (diff > 60000) {
                                        SlaveService.getSlaveService().getCpuAverage().remove(time);
                                        continue;
                                    }
                                    cpuAverageCalculate.add(SlaveService.getSlaveService().getCpuAverage().get(time));
                                }
                                cpuAverageUsage = SlaveService.getSlaveService().getManagerService().calculateAverage(cpuAverageCalculate);
                            } catch (OutOfMemoryError cpuAverageCalculate) {
                                System.out.println("OutOfMemoryError -> " + cpuAverageCalculate.getMessage());
                            }
                            SlaveService.getSlaveService().getNettyService().getSlaveClient().getChannel().writeAndFlush(SlaveService.getSlaveService().getPacketHelper().preparePacket(PacketType.SLAVE_ALIVE_PACKET, new SlaveAlivePacket(SlaveService.getSlaveService().getManagerService().getConfigManager().getConfig().getName(), cpuUsage, usedMemory, freeMemory, totalMemory, cpuAverageUsage)));
                        }
                    }
                    ).start();
                }
            }, 1000, 1000);
        }
        ).start();
    }

    public double calculateAverage(List<Double> marks) {
        Double sum = 0.0;
        if (!marks.isEmpty()) {
            for (Double mark : marks) {
                sum = sum + mark;
            }
            return sum / (double) marks.size();
        }
        return sum;
    }

    public void initDirectories() {
        try {
            Runtime.getRuntime().exec(new String[]{"bash", "-c", "rm -r templates"}).waitFor();
            Runtime.getRuntime().exec(new String[]{"bash", "-c", "mkdir templates"}).waitFor();
            Runtime.getRuntime().exec(new String[]{"bash", "-c", "rm -r servers"}).waitFor();
            Runtime.getRuntime().exec(new String[]{"bash", "-c", "mkdir servers"}).waitFor();
            Runtime.getRuntime().exec(new String[]{"bash", "-c", "rm -r logs"}).waitFor();
            Runtime.getRuntime().exec(new String[]{"bash", "-c", "mkdir logs"}).waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}