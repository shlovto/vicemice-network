package net.vicemice.hector.manager.server;

import net.vicemice.hector.SlaveService;

import java.util.Timer;
import java.util.TimerTask;

/*
 * Class created at 01:36 - 05.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class StartManager {

    public StartManager() {
        new Thread(() -> {
            new Timer().scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    if (SlaveService.getSlaveService().getManagerService().getServerManager().getCurrentStarting() == null && SlaveService.getSlaveService().getManagerService().getServerManager().getSlaveServerStarts().size() > 0 && SlaveService.getSlaveService().isReady()) {
                        ServerManager.access$2(SlaveService.getSlaveService().getManagerService().getServerManager(), SlaveService.getSlaveService().getManagerService().getServerManager().getSlaveServerStarts().get(0));
                        SlaveService.getSlaveService().getManagerService().getServerManager().getSlaveServerStarts().remove(0);
                        SlaveService.getSlaveService().getManagerService().getServerManager().startServer(SlaveService.getSlaveService().getManagerService().getServerManager().getCurrentStarting());
                    }
                    for (String serverID : SlaveService.getSlaveService().getManagerService().getServerManager().getLogTimeouts().keySet()) {
                        long timeout = SlaveService.getSlaveService().getManagerService().getServerManager().getLogTimeouts().get(serverID);
                        if (System.currentTimeMillis() <= timeout) continue;
                        SlaveService.getSlaveService().getManagerService().getServerManager().getLogTimeouts().remove(serverID);
                        try {
                            Runtime.getRuntime().exec("rm slave/logs/" + serverID).waitFor();
                            System.out.println("Deleting old Log '"+serverID+"'");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, 1000, 1000);
        }).start();
    }
}