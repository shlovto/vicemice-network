package net.vicemice.hector.utils.terminal;

import net.vicemice.hector.utils.terminal.commands.*;
import org.apache.commons.lang3.ArrayUtils;
import java.beans.ConstructorProperties;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/*
 * Class created at 20:19 - 31.12.2019
 * Copyright (C) 2016-2019 Minetasia Solutions
 */
public class CommandRegistry {
    private static CopyOnWriteArrayList<CommandHolder> cmds = new CopyOnWriteArrayList();

    static {
        CommandRegistry.registerCommand(new CommandHelp(), "help");
        CommandRegistry.registerCommand(new CommandEnd(), "end", "stop");
    }

    public static void registerCommand(CommandExecutor cmd, String ... commands) {
        for (int i = 0; i < commands.length; ++i) {
            commands[i] = commands[i].toLowerCase();
        }
        cmds.add(new CommandHolder(commands[0], Arrays.asList((String[])ArrayUtils.subarray((Object[])commands, 1, commands.length)), cmd));
    }

    public static void runCommand(String command, String[] args, Terminal writer) {
        for (CommandHolder h : cmds) {
            if (!h.accept(command)) continue;
            h.executor.onCommand(command, writer, args);
            return;
        }
        writer.writeMessage("\u00a7cCommand not found. help for more informations");
    }

    public static CopyOnWriteArrayList<CommandHolder> getCommands() {
        return cmds;
    }

    public static void help(String command, Terminal writer) {
        for (CommandHolder h : cmds) {
            if (!h.accept(command)) continue;
            h.executor.printHelp(false);
            return;
        }
        writer.writeMessage("\u00a7cCommand not found. help for more informations");
    }

    public static class CommandHolder {
        private String command;
        private List<String> alias;
        private CommandExecutor executor;

        public boolean accept(String is) {
            return this.command.equalsIgnoreCase(is) || this.alias.contains(is.toLowerCase());
        }

        @ConstructorProperties(value={"command", "alias", "executor"})
        public CommandHolder(String command, List<String> alias, CommandExecutor executor) {
            this.command = command;
            this.alias = alias;
            this.executor = executor;
        }

        public String getCommand() {
            return this.command;
        }

        public List<String> getAlias() {
            return this.alias;
        }

        public CommandExecutor getExecutor() {
            return this.executor;
        }
    }
}