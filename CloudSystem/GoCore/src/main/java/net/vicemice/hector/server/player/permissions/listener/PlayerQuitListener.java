package net.vicemice.hector.server.player.permissions.listener;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerPlayerInfo;
import net.vicemice.hector.api.protocol.wrappers.EnumWrappers;
import net.vicemice.hector.api.protocol.wrappers.PlayerInfoData;
import net.vicemice.hector.api.protocol.wrappers.WrappedChatComponent;
import net.vicemice.hector.api.protocol.wrappers.WrappedGameProfile;
import com.mojang.authlib.GameProfile;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.nick.NickManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.ArrayList;

public class PlayerQuitListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent playerQuitEvent) {
        Player player = playerQuitEvent.getPlayer();
        GoServer.getService().getPermissionManager().getRankManager().unsetPermissions(player);
        GoServer.getService().getPermissionManager().getUserRanks().remove(player.getUniqueId());
        GoServer.getService().getSpectators().remove(player.getUniqueId());

        if (NickManager.getPlayerData().containsKey(player.getUniqueId())) {
            for (Player onlinePlayer : Bukkit.getServer().getOnlinePlayers()) {
                WrapperPlayServerPlayerInfo wrapperPlayServerPlayerInfo = new WrapperPlayServerPlayerInfo();
                wrapperPlayServerPlayerInfo.setAction(EnumWrappers.PlayerInfoAction.REMOVE_PLAYER);
                wrapperPlayServerPlayerInfo.setData(new ArrayList<PlayerInfoData>() {{
                    this.add(new PlayerInfoData(WrappedGameProfile.fromHandle(new GameProfile(NickManager.getPlayerData().get(player.getUniqueId()).getUniqueId(), NickManager.getPlayerData().get(player.getUniqueId()).getNickName())),
                            0, EnumWrappers.NativeGameMode.SURVIVAL, WrappedChatComponent.fromText("")));
                }});
                wrapperPlayServerPlayerInfo.sendPacket(onlinePlayer);
            }

            NickManager.getPlayerData().remove(player.getUniqueId());
            NickManager.getBeforeJoined().remove(player.getName());
        }
    }
}