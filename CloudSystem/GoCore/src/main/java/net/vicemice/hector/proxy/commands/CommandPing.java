package net.vicemice.hector.proxy.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.Rank;

public class CommandPing extends TabableCommand {

    public CommandPing() {
        super(Rank.MEMBER, "ping");
    }

    public void execute(CommandSender commandSender, String[] strings) {
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;

        proxiedPlayer.sendMessage(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( proxiedPlayer.getUniqueId(), "command-ping", proxiedPlayer.getPing())));
    }
}