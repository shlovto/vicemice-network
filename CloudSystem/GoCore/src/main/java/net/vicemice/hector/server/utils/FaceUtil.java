package net.vicemice.hector.server.utils;

import org.bukkit.Location;
import org.bukkit.block.BlockFace;

/*
 * Class created at 16:17 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class FaceUtil {

    public static final BlockFace[] axis = {BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST};
    public static final BlockFace[] radial = {BlockFace.NORTH, BlockFace.NORTH_EAST, BlockFace.EAST, BlockFace.SOUTH_EAST, BlockFace.SOUTH, BlockFace.SOUTH_WEST, BlockFace.WEST, BlockFace.NORTH_WEST};

    public static BlockFace yawToFace(float yaw) {
        return yawToFace(yaw, true);
    }

    public static BlockFace yawToFace(float yaw, boolean useSubCardinalDirections) {
        if (useSubCardinalDirections) {
            return radial[Math.round(yaw / 45f) & 0x7];
        } else {
            return axis[Math.round(yaw / 90f) & 0x3];
        }
    }

    public static Location addForward(Location location, int count) {
        BlockFace face = yawToFace(location.getYaw(), false);
        switch (face) {
            case EAST:
                return location.add(count, 0, 0);
            case SOUTH:
                return location.add(0, 0, count);
            case NORTH:
                return location.add(0, 0, -count);
            case WEST:
                return location.add(-count, 0, 0);
        }
        return location;
    }

    public static Location addSideway(Location location, int count) {
        BlockFace face = yawToFace(location.getYaw(), false);
        switch (face) {
            case EAST:
                return location.add(0, 0, -count);
            case SOUTH:
                return location.add(count, 0, 0);
            case NORTH:
                return location.add(-count, 0, 0);
            case WEST:
                return location.add(0, 0, count);
        }
        return location;
    }
}