package net.vicemice.hector.server.player.afk;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.AFKPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.API;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/*
 * Class created at 12:11 - 27.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class AfkSession {
    private Player p;
    private Location lastLoc;
    private int afkMinutes;
    private boolean afk;

    public AfkSession(Player p) {
        this.p = p;
        this.lastLoc = p.getLocation();
        this.afkMinutes = 0;
        this.afk = false;
    }

    public void increaseMinute() {
        if (p != null && p.isOnline() && !GoServer.getService().getServerName().toLowerCase().contains("verify")) {
            if (!lastLoc.getWorld().getName().equalsIgnoreCase(p.getLocation().getWorld().getName())) lastLoc = p.getLocation();
            if (lastLoc.distanceSquared(p.getLocation()) < 3 * 3) {
                afkMinutes++;
                lastLoc = p.getLocation();
                if (afkMinutes >= 3) {
                    setAfk(true);
                }
                if (afkMinutes >= 10 && !GoServer.getService().getServerName().toLowerCase().contains("lobby") && !GoServer.getService().getServerName().toLowerCase().contains("fallback")) {
                    p.sendMessage(GoAPI.getUserAPI().translate(p.getUniqueId(), "user-inactivity-kick"));
                    p.kickPlayer("lobby");
                }
            } else {
                afkMinutes = 0;
                lastLoc = p.getLocation();
            }
        }
    }

    public void setAfk(boolean afk) {
        if (!afk) this.afkMinutes = 0;
        this.afk = afk;
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.AFK, new AFKPacket(p.getUniqueId(), afk)), API.PacketReceiver.SLAVE);
    }

    public boolean isAfk() {
        return afk;
    }

    public Player getPlayer() {
        return p;
    }
}