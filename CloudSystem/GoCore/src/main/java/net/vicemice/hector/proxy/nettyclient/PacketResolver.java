package net.vicemice.hector.proxy.nettyclient;

import io.netty.channel.Channel;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.events.ProxyUpdateLocaleEvent;
import net.vicemice.hector.packets.Abuses;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.FilterPacket;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.UpdateLocalePacket;
import net.vicemice.hector.packets.types.lobby.LobbyDataPacket;
import net.vicemice.hector.packets.types.player.abuse.PlayerBanDataPacket;
import net.vicemice.hector.packets.types.player.abuse.PlayerMuteDataPacket;
import net.vicemice.hector.packets.types.player.labymod.LabyModPacket;
import net.vicemice.hector.packets.types.player.punish.BanIpPacket;
import net.vicemice.hector.packets.types.player.punish.KickPacket;
import net.vicemice.hector.packets.types.proxy.players.ProxyBlacklistedPlayersPacket;
import net.vicemice.hector.packets.types.proxy.players.ProxyWhitelistedPlayersPacket;
import net.vicemice.hector.packets.types.server.SendPluginMessage;
import net.vicemice.hector.proxy.GoProxy;
import net.vicemice.hector.proxy.util.LobbyManager;
import net.vicemice.hector.proxy.util.labymod.LabyModAPI;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.packets.types.player.*;
import net.vicemice.hector.packets.types.proxy.*;
import net.vicemice.hector.utils.players.punish.Punish;
import net.vicemice.hector.utils.players.utils.ChatMessageParser;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.UUID;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PacketResolver extends PacketGetter {

    @Override
    public void receivePacket(PacketHolder initPacket) {
        if (initPacket.getKey().equalsIgnoreCase(PacketType.LANGUAGE_PACKET.getKey())) {
            LanguagePacket packet = (LanguagePacket) initPacket.getValue();

            ProxiedPlayer player = BungeeCord.getInstance().getPlayer(packet.getUniqueId());
            LabyModAPI.updateGame(player, "true", player.getServer().getInfo().getName());
        } else if (initPacket.getKey().equals(PacketType.UPDATE_LOCALE.getKey())) {
            UpdateLocalePacket updateLocalePacket = (UpdateLocalePacket) initPacket.getValue();

            BungeeCord.getInstance().getLogger().log(Level.INFO, "Receive Locale Update Packet at " + GoAPI.getTimeManager().getTime("dd.MM.yyyy - HH:mm", updateLocalePacket.getMillis()));
            ProxyServer.getInstance().getPluginManager().callEvent(new ProxyUpdateLocaleEvent(new Callback<ProxyUpdateLocaleEvent>() {
                @Override
                public void done(ProxyUpdateLocaleEvent proxyUpdateLocaleEvent, Throwable throwable) {

                }
            }));
        } else if (initPacket.getKey().equals(PacketType.PROXY_BLACKLISTED_PLAYERS.getKey())) {
            ProxyBlacklistedPlayersPacket proxyBlacklistedPlayersPacket = (ProxyBlacklistedPlayersPacket) initPacket.getValue();
            GoProxy.getProxyService().setBlacklistedPlayers(proxyBlacklistedPlayersPacket.getPlayers());
            System.out.println("Update Blacklisted Players");
        } else if (initPacket.getKey().equals(PacketType.PROXY_WHITELISTED_PLAYERS.getKey())) {
            ProxyWhitelistedPlayersPacket proxyWhitelistedPlayersPacket = (ProxyWhitelistedPlayersPacket) initPacket.getValue();
            GoProxy.getProxyService().setWhitelistedPlayers(proxyWhitelistedPlayersPacket.getPlayers());
            System.out.println("Update Whitelisted Players");
        } else if (initPacket.getKey().equals(PacketType.LABYMOD_PACKET.getKey())) {
            LabyModPacket packet = (LabyModPacket) initPacket.getValue();

            ProxiedPlayer player = BungeeCord.getInstance().getPlayer(packet.getUuid());

            if (packet.getJoinSecret() != null) {
                LabyModAPI.updateJoin(player, "true", packet.getJoinSecret().toString());
            } else {
                LabyModAPI.updateJoin(player, "false", UUID.randomUUID().toString());
            }

            if (packet.getSpecSecret() != null) {
                LabyModAPI.updateSpec(player, "true", packet.getSpecSecret().toString());
            } else {
                LabyModAPI.updateSpec(player, "false", UUID.randomUUID().toString());
            }

            if (packet.getMatchSecret() != null) {
                LabyModAPI.updateMatch(player, "true", packet.getJoinSecret().toString());
            } else {
                LabyModAPI.updateMatch(player, "false", UUID.randomUUID().toString());
            }

            if (packet.isHasParty()) {
                LabyModAPI.updateParty(player, "true", packet.getPartyPacket().getId(), String.valueOf(packet.getPartyPacket().getMembers()), packet.getPartyPacket().getMax().toString());
            } else {
                LabyModAPI.updateParty(player, "false", UUID.randomUUID().toString(), "0", "0");
            }
        } else if (initPacket.getKey().equals(PacketType.MESSAGE_PACKET.getKey())) {
            MessagePacket messagePacket = (MessagePacket) initPacket.getValue();
            ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(messagePacket.getReceiverUniqueId());
            if (proxiedPlayer == null) {
                return;
            }
            String[] arrstring = messagePacket.getMessages();
            int n = arrstring.length;
            int n2 = 0;
            while (n2 < n) {
                String message = arrstring[n2];

                /*
                BaseComponent[] t = TextComponent.fromLegacyText(message);
                for (BaseComponent baseComponent : t) {
                    if (baseComponent instanceof TextComponent) {
                        TextComponent textComponent = (TextComponent) baseComponent;
                        if (textComponent.getText().contains(";")) {
                            String[] message1 = textComponent.getText().split(";");
                            for (int i = 1; i < message1.length; i++) {
                                textComponent.setText(textComponent.getText().replace(message1[i], "").replace(";", ""));
                            }
                            ClickEvent.Action action1 = null;
                            try {
                                action1 = ClickEvent.Action.valueOf(message1[1]);
                            } catch (Exception ignored) {}
                            HoverEvent.Action action2 = null;
                            try {
                                action2 = HoverEvent.Action.valueOf(message1[1]);
                            } catch (Exception ignored) {}

                            if (action1 != null) {
                                textComponent.setClickEvent(new ClickEvent(action1, message1[2]));
                            } else if (action2 != null) {
                                textComponent.setHoverEvent(new HoverEvent(action2, TextComponent.fromLegacyText(message1[2])));
                            }
                            if (message1.length == 5) {
                                ClickEvent.Action action3 = null;
                                try {
                                    action3 = ClickEvent.Action.valueOf(message1[3]);
                                } catch (Exception ignored) {}
                                HoverEvent.Action action4 = null;
                                try {
                                    action4 = HoverEvent.Action.valueOf(message1[3]);
                                } catch (Exception ignored) {}
                                if (action3 != null) {
                                    textComponent.setClickEvent(new ClickEvent(action3, message1[4]));
                                } else if (action4 != null) {
                                    textComponent.setHoverEvent(new HoverEvent(action4, TextComponent.fromLegacyText(message1[4])));
                                }
                            }
                        }
                    }
                }*/

                BaseComponent[] t = fromLegacyText(message);
                proxiedPlayer.sendMessage(t);

                /*ChatMessageParser chatMessageParser = new ChatMessageParser(message);
                messagePacket.getClickEventTreeMap().forEach(chatMessageParser::addClickEvent);
                messagePacket.getHoverEventTreeMap().forEach(chatMessageParser::addHoverEvent);
                chatMessageParser.addClickEvent("CLICK_player4124125", new MessagePacket.ClickHoverEvent("SUGGEST_COMMAND", proxiedPlayer.getName()));
                chatMessageParser.addHoverEvent("HOVER_player4124125", new MessagePacket.ClickHoverEvent("SHOW_TEXT", proxiedPlayer.getName()));
                proxiedPlayer.sendMessage(chatMessageParser.parse());*/
                ++n2;
            }
        } else if (initPacket.getKey().equals(PacketType.SERVER_CONNECT.getKey())) {
            ConnectPacket connectPacket = (ConnectPacket) initPacket.getValue();
            ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(connectPacket.getName());
            if (proxiedPlayer == null) {
                return;
            }
            ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo(connectPacket.getServer());
            if (serverInfo != null) {
                proxiedPlayer.connect(serverInfo);
            } else {
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText("§cThis server does not exist!"));
            }
        } else if (initPacket.getKey().equals(PacketType.KICK_PACKET.getKey())) {
            KickPacket kickPacket = (KickPacket) initPacket.getValue();
            ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(kickPacket.getUniqueId());
            if (proxiedPlayer == null) {
                return;
            }
            proxiedPlayer.disconnect(TextComponent.fromLegacyText(kickPacket.getReason()));
        } else if (initPacket.getKey().equals(PacketType.PROXY_SERVER_ADD.getKey())) {
            ProxyAddServerPacket proxyAddServerPacket = (ProxyAddServerPacket) initPacket.getValue();
            InetSocketAddress targetAddress = new InetSocketAddress(proxyAddServerPacket.getHost(), proxyAddServerPacket.getPort());
            if (ProxyServer.getInstance().getServerInfo(proxyAddServerPacket.getName()) != null) {
                ServerInfo info = ProxyServer.getInstance().getServerInfo(proxyAddServerPacket.getName());
                if (info.getAddress().equals(targetAddress)) {
                    return;
                }
                System.out.println("[Cloud] Update a Server Entry (" + info.getName() + ")");
                ProxyServer.getInstance().getServers().remove(info.getName());
            } else {
                System.out.println("[Cloud] Add a new Server Entry (" + proxyAddServerPacket.getName() + ")");
            }
            ProxyServer.getInstance().getServers().put(proxyAddServerPacket.getName(), ProxyServer.getInstance().constructServerInfo(proxyAddServerPacket.getName(), targetAddress, "", false));
        } else if (initPacket.getKey().equals(PacketType.PROXY_SERVER_REMOVE.getKey())) {
            ProxyRemoveServerPacket proxyRemoveServerPacket = (ProxyRemoveServerPacket) initPacket.getValue();
            ProxyServer.getInstance().getServers().remove(proxyRemoveServerPacket.getName());
            System.out.println("[Cloud] Remove an old Server (" + proxyRemoveServerPacket.getName() + ")");
        } else if (initPacket.getKey().equalsIgnoreCase(PacketType.ONLINE_COUNT_PACKET.getKey())) {
            OnlineCountPacket onlineCountPacket = (OnlineCountPacket) initPacket.getValue();
            GoProxy.getProxyService().setOnlinePlayers(onlineCountPacket.getOnline());
        } else if (initPacket.getKey().equalsIgnoreCase(PacketType.PLAYER_BAN_DATA.getKey())) {
            PlayerBanDataPacket playerBanDataPacket = (PlayerBanDataPacket) initPacket.getValue();

            Abuses.Abuse punishData = playerBanDataPacket.getAbuses().getData();
            if (playerBanDataPacket.getAbuses().getData() == null) {
                GoProxy.getProxyService().getBanData().remove(playerBanDataPacket.getUser());
                return;
            }

            ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(playerBanDataPacket.getUser());
            if (proxiedPlayer != null) {
                Punish punish = GoProxy.getProxyService().getPunishManager().getByKey(punishData.getReason());
                String reason = punishData.getReason();

                if (punish != null) {
                    reason = GoAPI.getUserAPI().translate(proxiedPlayer.getUniqueId(), reason.toUpperCase());
                }

                proxiedPlayer.disconnect(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate(proxiedPlayer.getUniqueId(), "kick-ban-screen", reason)));
            }
            GoProxy.getProxyService().getBanData().put(playerBanDataPacket.getUser(), punishData);

            GoProxy.getProxyService().setLastPacketReceive(System.currentTimeMillis());
        } else if (initPacket.getKey().equalsIgnoreCase(PacketType.PLAYER_MUTE_DATA.getKey())) {
            PlayerMuteDataPacket playerMuteDataPacket = (PlayerMuteDataPacket) initPacket.getValue();

            Abuses.Abuse punishData = playerMuteDataPacket.getAbuses().getData();
            if (playerMuteDataPacket.getAbuses().getData() == null) {
                GoProxy.getProxyService().getMuteData().remove(playerMuteDataPacket.getUser());
                return;
            }

            GoProxy.getProxyService().getMuteData().put(playerMuteDataPacket.getUser(), punishData);
            GoProxy.getProxyService().setLastPacketReceive(System.currentTimeMillis());
        } else if (initPacket.getKey().equals(PacketType.FILTER_PACKET.getKey())) {
            FilterPacket filterPacket = (FilterPacket) initPacket.getValue();
            GoProxy.getProxyService().setChatBlocked(filterPacket.getEntries());
        } else if (initPacket.getKey().equals(PacketType.PROXY_RANK_PACKET.getKey())) {
            ProxyRankPacket proxyRankPacket = (ProxyRankPacket) initPacket.getValue();
            for (String[] rankData : proxyRankPacket.getRanks()) {
                //System.out.println("-> " + rankData[0] + ";" + rankData[1]);
                UUID uuid = UUID.fromString(rankData[0]);
                Rank rank = Rank.fromString(rankData[1]);
                if (rank == Rank.MEMBER) {
                    GoProxy.getProxyService().getPlayerRanks().remove(uuid);
                    continue;
                }
                GoProxy.getProxyService().getPlayerRanks().put(uuid, rank);
            }
        } else if (initPacket.getKey().equals(PacketType.MAINTENANCE_PACKET.getKey())) {
            if (!GoProxy.getProxyService().getProxyManager().isVerify()) {
                MaintenancePacket maintenancePacket = (MaintenancePacket) initPacket.getValue();
                if (maintenancePacket.isKickAll()) {
                    for (ProxiedPlayer proxiedPlayer : ProxyServer.getInstance().getPlayers()) {
                        if (GoProxy.getProxyService().getProxyManager().getPlayerRank(proxiedPlayer.getUniqueId()).isTeam())
                            continue;
                        proxiedPlayer.disconnect(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate(proxiedPlayer.getUniqueId(), "login-maintenance")));
                    }
                } else {
                    GoProxy.getProxyService().getProxyManager().setMaintenance(maintenancePacket.isEnabled());
                }
            } else {
                GoProxy.getProxyService().getProxyManager().setMaintenance(false);
            }
        } else if (initPacket.getKey().equals(PacketType.ALERT_PACKET.getKey())) {
            AlertPacket alertPacket = (AlertPacket) initPacket.getValue();
            ProxyServer.getInstance().broadcast(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', alertPacket.getMessage())));
        } else if (initPacket.getKey().equals(PacketType.PROXY_SETTINGS_PACKET.getKey())) {
            GoProxy.getProxyService().setProxySettingsPacket((ProxySettingsPacket) initPacket.getValue());
            String[] splittedMaintenanceMOTD = GoProxy.getProxyService().getProxySettingsPacket().getMaintenanceMOTD().split(";");
            GoProxy.getProxyService().getProxyManager().setMaintenanceMOTD(splittedMaintenanceMOTD[0] + "\n" + splittedMaintenanceMOTD[1]);
            String[] splittedMOTD = GoProxy.getProxyService().getProxySettingsPacket().getMotd().split(";");
            GoProxy.getProxyService().getProxyManager().setMOTD(splittedMOTD[0] + "\n" + splittedMOTD[1]);
            GoProxy.getProxyService().getNameBlackList().clear();

            ProxyServer.getInstance().getScheduler().runAsync(GoProxy.getService(), () -> {
                for (ProxiedPlayer proxiedPlayer : ProxyServer.getInstance().getPlayers()) {
                    String name = proxiedPlayer.getName();
                    for (String blackList : GoProxy.getProxyService().getNameBlackList()) {
                        if (!name.toLowerCase().contains(blackList)) continue;
                        proxiedPlayer.disconnect(TextComponent.fromLegacyText(" "));
                        return;
                    }
                }
            });
        } else if (initPacket.getKey().equals(PacketType.COMPONENT_MESSAGE_PACKET.getKey())) {
            ComponentMessagePacket componentMessagePacket = (ComponentMessagePacket) initPacket.getValue();
            if (componentMessagePacket.getName() != null) {
                ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(componentMessagePacket.getName());
                if (proxiedPlayer != null) {
                    BaseComponent[] baseComponents = new BaseComponent[componentMessagePacket.getTextComponentMessages().size()];
                    int current = 0;
                    for (String[] data : componentMessagePacket.getTextComponentMessages()) {
                        String message = data[0];
                        TextComponent textComponent = new TextComponent(message);
                        if (data.length > 1) {
                            if (Boolean.parseBoolean(data[1]) && data[2].equalsIgnoreCase("run_command")) {
                                textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, data[3]));
                            } else if (Boolean.parseBoolean(data[1]) && data[2].equalsIgnoreCase("open_url")) {
                                textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, data[3]));
                            } else if (Boolean.parseBoolean(data[1]) && data[2].equalsIgnoreCase("suggest_command")) {
                                textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, data[3]));
                            } else if (Boolean.parseBoolean(data[1]) && data[2].equalsIgnoreCase("copy_to_clipboard")) {
                                textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.COPY_TO_CLIPBOARD, data[3]));
                            } else if (Boolean.parseBoolean(data[1]) && data[2].equalsIgnoreCase("hover_message")) {
                                textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent(data[4])}));
                            }

                            if (data.length == 7) {
                                if (data[5].equalsIgnoreCase("hover_message")) {
                                    textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[]{new TextComponent(data[6])}));
                                }
                            }
                        }
                        baseComponents[current] = textComponent;
                        ++current;
                    }
                    proxiedPlayer.sendMessage(baseComponents);
                }
            } else {
                BaseComponent[] baseComponents = new BaseComponent[componentMessagePacket.getTextComponentMessages().size()];
                int current = 0;
                for (String[] data : componentMessagePacket.getTextComponentMessages()) {
                    String message = data[0];
                    TextComponent textComponent = new TextComponent(message);
                    if (Boolean.parseBoolean(data[1])) {
                        if (data[2].equalsIgnoreCase("run_command")) {
                            textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, data[3]));
                        } else if (data[2].equalsIgnoreCase("open_url")) {
                            textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, data[3]));
                        }
                    }
                    baseComponents[current] = textComponent;
                    ++current;
                }
                for (ProxiedPlayer proxiedPlayer : ProxyServer.getInstance().getPlayers()) {
                    proxiedPlayer.sendMessage(baseComponents);
                }
            }
        } else if (initPacket.getKey().equals(PacketType.LOBBY_DATA_PACKET.getKey())) {
            LobbyDataPacket lobbyDataPacket = (LobbyDataPacket) initPacket.getValue();
            LobbyManager.receiveData(lobbyDataPacket);
        } else if (initPacket.getKey().equals(PacketType.IP_BAN_PACKET.getKey())) {
            BanIpPacket banIpPacket = (BanIpPacket) initPacket.getValue();
            if (banIpPacket.isBan()) {
                GoProxy.getProxyService().getBannedIPS().add(banIpPacket.getIp());
                for (ProxiedPlayer proxiedPlayer : ProxyServer.getInstance().getPlayers()) {
                    if (!proxiedPlayer.getPendingConnection().getVirtualHost().getAddress().getHostAddress().equalsIgnoreCase(banIpPacket.getIp()))
                        continue;
                    proxiedPlayer.disconnect(TextComponent.fromLegacyText(" "));
                }
            } else {
                GoProxy.getProxyService().getBannedIPS().remove(banIpPacket.getIp());
            }
        } else if (initPacket.getKey().equals(PacketType.ALLOW_CHAT_PACKET.getKey())) {
            AllowChatPacket allowChatPacket = (AllowChatPacket) initPacket.getValue();
            GoProxy.getProxyService().getAllowedChat().add(allowChatPacket.getPlayer());
        } else if (initPacket.getType() == PacketType.SEND_PLUGIN_MESSAGE) {
            SendPluginMessage pkt = initPacket.getPacket(SendPluginMessage.class);
            ProxiedPlayer player = ProxyServer.getInstance().getPlayer(pkt.getPlayer());
            if (player == null || !player.isConnected()) {
                System.err.println("Tried to send a plugin message to a not connected player!");
                return;
            }
            if (pkt.getTarget() == SendPluginMessage.Target.CLIENT) {
                System.err.println("tried to send a plugin message to the client but its not implemented yet!");
                return;
            }
            if (pkt.getTarget() == SendPluginMessage.Target.SERVER) {
                if (player.getServer() == null) {
                    System.err.println("Try to send a plugin message for a player, but the player isnt on any server");
                    return;
                }
                player.getServer().sendData("ReplaySystem", pkt.getData());
            }
        } else {
            System.err.println("Could not find packet type " + (initPacket.getType()) + "/" + initPacket.getKey());
        }
    }

    @Override
    public void channelActive(Channel var1) {

    }

    public BaseComponent[] fromLegacyText(String message) {
        return fromLegacyText(message, ChatColor.WHITE);
    }

    private final Pattern url = Pattern.compile("^(?:(https?)://)?([-\\w_\\.]{2,}\\.[a-z]{2,4})(/\\S*)?$");

    private BaseComponent[] fromLegacyText(String message, ChatColor defaultColor) {
        ArrayList<BaseComponent> components = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        TextComponent component = new TextComponent();
        Matcher matcher = url.matcher(message);

        for (int i = 0; i < message.length(); ++i) {
            char c = message.charAt(i);
            TextComponent old;
            if (c == 167) {
                ++i;
                if (i >= message.length()) {
                    break;
                }

                c = message.charAt(i);
                if (c >= 'A' && c <= 'Z') {
                    c = (char) (c + 32);
                }

                ChatColor format = ChatColor.getByChar(c);
                if (format != null) {
                    if (builder.length() > 0) {
                        old = component;
                        if (isChatMessage32(component.getText()) && i == getSecondPercentCharId(old.getText()))
                            component = new TextComponent(component);
                        old.setText(builder.toString());
                        if (isChatMessage32(component.getText())) old.setText(old.getText().replace("%", ""));
                        if (isChatMessage32(component.getText()) && i == getSecondPercentCharId(old.getText()))
                            builder = new StringBuilder();
                        components.add(old);
                    }

                    switch (format) {
                        case BOLD:
                            component.setBold(true);
                            break;
                        case ITALIC:
                            component.setItalic(true);
                            break;
                        case UNDERLINE:
                            component.setUnderlined(true);
                            break;
                        case STRIKETHROUGH:
                            component.setStrikethrough(true);
                            break;
                        case MAGIC:
                            component.setObfuscated(true);
                            break;
                        case RESET:
                            format = defaultColor;
                        default:
                            if (!isChatMessage32(component.getText())) component = new TextComponent();
                            component.setColor(format);
                    }
                }
            } else {
                int pos = message.indexOf(32, i);
                if (pos == -1) {
                    pos = message.length();
                }

                if (matcher.region(i, pos).find()) {
                    if (builder.length() > 0) {
                        old = component;
                        component = new TextComponent(component);
                        old.setText(builder.toString());
                        builder = new StringBuilder();
                        components.add(old);
                    }

                    old = component;
                    component = new TextComponent(component);
                    String urlString = message.substring(i, pos);
                    component.setText(urlString);
                    component.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, urlString.startsWith("http") ? urlString : "http://" + urlString));
                    components.add(component);
                    i += pos - i - 1;
                    component = old;
                } else {
                    builder.append(c);
                }
            }
        }

        component.setText(builder.toString());
        components.add(component);
        return (BaseComponent[]) components.toArray(new BaseComponent[components.size()]);
    }

    private boolean isChatMessage32(String message) {
        int count = 0;
        for (int i = 0; i < message.length(); i++) {
            if (message.charAt(i) == '%') {
                count++;
            }
        }

        return (count == 2);
    }

    private int getSecondPercentCharId(String message) {
        for (int i = 0; i < message.length(); i++) {
            if (i == 0) continue;
            return i;
        }

        return -1;
    }
}