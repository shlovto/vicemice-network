package net.vicemice.hector.server.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.ArrayList;
import java.util.List;

/*
 * Class created at 16:19 - 03.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class LocationSerializer {
    public static String toString(Location location) {
        return location.getWorld().getName() + ";" + location.getX() + ";" + location.getY() + ";" + location.getZ() + ";" + location.getYaw() + ";" + location.getPitch();
    }

    public static Location fromString(String string) {
        String[] array = string.split(";");
        World world = Bukkit.getWorld(array[0]);
        if (world != null) {
            return new Location(world, Double.parseDouble(array[1]), Double.parseDouble(array[2]), Double.parseDouble(array[3]), Float.parseFloat(array[4]), Float.parseFloat(array[5]));
        }
        return null;
    }

    public static Location fromStringWithoutYawAndPitch(String string) {
        String[] array = string.split(";");
        World world = Bukkit.getWorld(array[0]);
        if (world != null) {
            return new Location(world, Double.parseDouble(array[1]), Double.parseDouble(array[2]), Double.parseDouble(array[3]));
        }
        return null;
    }

    public static List<String> toStringList(List<Location> locations) {
        List<String> list = new ArrayList<>();
        for (Location location : locations) {
            list.add((location.getWorld().getName() + ";" + location.getX() + ";" + location.getY() + ";" + location.getZ() + ";" + location.getYaw() + ";" + location.getPitch()));
        }

        return list;
    }

    public static List<String> toStringListWithoutYawAndPitch(List<Location> locations) {
        List<String> list = new ArrayList<>();
        for (Location location : locations) {
            list.add((location.getWorld().getName() + ";" + location.getX() + ";" + location.getY() + ";" + location.getZ()));
        }

        return list;
    }

    public static List<Location> fromList(List<String> string) {
        List<Location> list = new ArrayList<>();
        for (String s : string) {
            String[] array = s.split(";");
            World world = Bukkit.getWorld(array[0]);
            if (world != null) {
                list.add(new Location(world, Double.parseDouble(array[1]), Double.parseDouble(array[2]), Double.parseDouble(array[3]), Float.parseFloat(array[4]), Float.parseFloat(array[5])));
            }
        }

        return list;
    }

    public static List<Location> fromListWithoutYawAndPitch(List<String> string) {
        List<Location> list = new ArrayList<>();
        for (String s : string) {
            String[] array = s.split(";");
            World world = Bukkit.getWorld(array[0]);
            if (world != null) {
                list.add(new Location(world, Double.parseDouble(array[1]), Double.parseDouble(array[2]), Double.parseDouble(array[3])));
            }
        }

        return list;
    }
}