package net.vicemice.hector.server.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class FixListener implements Listener {

    @EventHandler
    public void onCommand(final PlayerCommandPreprocessEvent e) {
        if (e.getMessage().startsWith("/mv") || e.getMessage().startsWith("/multiverse-core:mv")) {
            e.setMessage(e.getMessage().replace(".", " "));
        }
    }
}