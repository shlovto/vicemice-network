package net.vicemice.hector.server.apihandler;

import io.netty.channel.Channel;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.server.ServerPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.types.ServerAPI;
import net.vicemice.hector.types.ServerState;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

public class CloudServerAPI implements ServerAPI {

    public int getServerMaxPlayers() {
        return GoServer.getService().getGoConfig().getMAX_PLAYERS();
    }

    public void changeServer(ServerState cloudServerState, String msg) {
        String setGroup = null;
        if (!GoServer.getService().getGoConfig().getSERVER_GROUP().equals("no")) {
            setGroup = GoServer.getService().getGoConfig().getSERVER_GROUP();
        }
        ServerPacket serverPacket = new ServerPacket(GoServer.getService().getServerName(), null, 0, "USER", 477, ServerState.valueOf(cloudServerState.name()), false, msg, GoServer.getService().getGoConfig().getMAX_PLAYERS(), setGroup);
        Channel useChannel = GoServer.getService().getGoAPI().getNettyClient().getChannel();
        PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.SERVER_PACKET, serverPacket);
        if (useChannel != null && useChannel.isActive() && useChannel.isOpen()) {
            GoServer.getService().getApi().sendPacket(initPacket, API.PacketReceiver.SLAVE);
        } else {
            GoServer.getService().getGoAPI().getNettyClient().getBeforeActive().add(initPacket);
        }
    }

    public String getServerName() {
        return GoServer.getService().getServerName();
    }

    public void setScoreboard(Scoreboard scoreboard) {
        if (scoreboard == null) {
            GoServer.getService().getPermissionManager().setSetScoreboard(null);
            for (Player player : Bukkit.getServer().getOnlinePlayers()) {
                player.setScoreboard(GoServer.getService().getPermissionManager().getRankScoreboard());
            }
        } else {
            GoServer.getService().getPermissionManager().setSetScoreboard(scoreboard);
        }
    }

    public void setKick(boolean kick) {
        GoServer.getService().setKickPlayers(kick);
    }
}