package net.vicemice.hector.proxy.util.mongodb;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import lombok.Getter;
import net.vicemice.hector.proxy.GoProxy;
import org.bson.Document;
import java.util.ArrayList;
import java.util.List;

public class MongoManager {
    @Getter
    private MongoClient client;
    @Getter
    private MongoDatabase mongoDatabase;

    public MongoManager() {
        String host = GoProxy.getProxyService().getMongoConfig().getHost();
        int port = Integer.parseInt(GoProxy.getProxyService().getMongoConfig().getPort());
        String user = GoProxy.getProxyService().getMongoConfig().getUsername();
        char[] password = GoProxy.getProxyService().getMongoConfig().getPassword().toCharArray();

        ServerAddress serverAddress = new ServerAddress(host, port);
        List<MongoCredential> credentials = new ArrayList<>();
        credentials.add(
                MongoCredential.createCredential(
                        user,
                        "admin",
                        password
                )
        );
        client = new MongoClient(serverAddress, credentials);
        mongoDatabase = client.getDatabase("networkService");
    }

    public ArrayList<Document> getDocuments(String collection) {
        ArrayList<Document> docs = new ArrayList<>();

        for (Document document : mongoDatabase.getCollection(collection).find()) {
            docs.add(document);
        }

        return docs;
    }

    public ArrayList<Document> getDocumentsWithFilter(String collection, Document filter) {
        ArrayList<Document> docs = new ArrayList<>();

        for (Document document : mongoDatabase.getCollection(collection).find(filter)) {
            docs.add(document);
        }

        return docs;
    }

    public ArrayList<Document> getDocumentsWithSort(String collection, Document sort) {
        ArrayList<Document> docs = new ArrayList<>();

        FindIterable<Document> o = mongoDatabase.getCollection(collection).find().sort(sort);

        if (o != null) {
            for (Document document : o) {
                docs.add(document);
            }
        }

        return docs;
    }

    public ArrayList<Document> getDocumentsWithFilterAndSort(String collection, Document filter, Document sort) {
        ArrayList<Document> docs = new ArrayList<>();

        for (Document document : mongoDatabase.getCollection(collection).find(filter).sort(sort)) {
            docs.add(document);
        }

        return docs;
    }

    public ArrayList<Document> getDocumentsWithFilterAndSortAndLimit(String collection, Document filter, Document sort, Integer limit) {
        ArrayList<Document> docs = new ArrayList<>();

        for (Document document : mongoDatabase.getCollection(collection).find(filter).sort(sort).limit(limit)) {
            docs.add(document);
        }

        return docs;
    }

    public ArrayList<Document> getDocumentsWithLimit(String collection, Integer limit) {
        ArrayList<Document> docs = new ArrayList<>();

        for (Document document : mongoDatabase.getCollection(collection).find().limit(limit)) {
            docs.add(document);
        }

        return docs;
    }

    public Document getDocument(String collection, Document filter) {
        for (Document document : mongoDatabase.getCollection(collection).find(filter)) {
            return document;
        }

        return null;
    }
}