package net.vicemice.hector.proxy.listener;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.Abuses;
import net.vicemice.hector.proxy.GoProxy;
import net.vicemice.hector.proxy.util.mongodb.PlayerAPI;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.players.punish.Punish;

import java.util.Locale;
import java.util.UUID;

public class LoginListener implements Listener {

    @EventHandler
    public void onLogin(LoginEvent event) {
        try {
            UUID uuid = event.getConnection().getUniqueId();
            PlayerAPI player = new PlayerAPI(uuid);
            if (GoProxy.getProxyService().getProxySettingsPacket() == null) {
                event.setCancelled(true);
                event.setCancelReason(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( event.getConnection().getUniqueId(), "login-cloud-error")));
                return;
            }
            if (event.getConnection().getVersion() < 47) {
                event.setCancelled(true);
                event.setCancelReason(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( event.getConnection().getUniqueId(), "login-old-version")));
                return;
            }
            if (GoProxy.getProxyService().getProxyManager().isMaintenance() && !GoProxy.getProxyService().getWhitelistedPlayers().contains(uuid) && !GoProxy.getProxyService().getProxyManager().getPlayerRank(uuid).isHigherEqualsLevel(Rank.MODERATOR) && !GoProxy.getProxyService().getProxyManager().isVerify()) {
                event.setCancelled(true);
                event.setCancelReason(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( event.getConnection().getUniqueId(), "login-maintenance")));
                return;
            }
            if (GoProxy.getProxyService().getBlacklistedPlayers().contains(uuid)) {
                event.setCancelled(true);
                event.setCancelReason(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( event.getConnection().getUniqueId(), "login-connection-not-established")));
                return;
            }

            if (GoProxy.getProxyService().getBanData().containsKey(uuid) && !GoProxy.getProxyService().getProxyManager().isVerify()) {
                Abuses.Abuse banData = GoProxy.getProxyService().getBanData().get(uuid);
                long length = banData.getLength();

                Punish punish = GoProxy.getProxyService().getPunishManager().getByKey(banData.getReason());
                String reason = banData.getReason();

                if (punish != null) {
                    reason = GoAPI.getUserAPI().translate( event.getConnection().getUniqueId(), reason.toUpperCase(), reason);
                }

                if (length == 0) {
                    event.setCancelled(true);
                    event.setCancelReason(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( event.getConnection().getUniqueId(), "ban-perma-screen", reason)));
                } else {
                    event.setCancelled(true);
                    event.setCancelReason(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( event.getConnection().getUniqueId(), "ban-temp-screen", reason, GoAPI.getTimeManager().getRemainingTimeString(GoProxy.getLocaleManager(), Locale.forLanguageTag(new PlayerAPI(uuid).get("locale")), length))));
                }
            }

            ProxyServer.getInstance().getScheduler().runAsync(GoProxy.getService(), () -> {
                if (GoProxy.getProxyService().getProxyManager().isVerify()) {
                    return;
                }
                for (String blackList : GoProxy.getProxyService().getNameBlackList()) {
                    if (!event.getConnection().getName().toLowerCase().contains(blackList)) continue;
                    return;
                }
                if (!GoProxy.getProxyService().getProxyManager().isConnected()) {
                    event.setCancelled(true);
                    event.setCancelReason(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( event.getConnection().getUniqueId(), "login-cloud-error")));
                }
                if (GoProxy.getProxyService().getOnlinePlayers() >= GoProxy.getProxyService().getProxySettingsPacket().getMaxPlayers()) {
                    event.setCancelled(true);
                    event.setCancelReason(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( event.getConnection().getUniqueId(), "login-network-full", GoProxy.getProxyService().getProxySettingsPacket().getMaxPlayers())));
                }
            });
        } catch (Exception ex) {
            event.setCancelled(true);
            event.setCancelReason(TextComponent.fromLegacyText("§cThere is an error occurred while connecting"));
            ex.printStackTrace();
        }
    }
}