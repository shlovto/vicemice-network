package net.vicemice.hector.proxy.config.language;

import lombok.Getter;
import net.vicemice.hector.utils.language.Language;

import java.util.HashMap;

public class LanguageManager {

    @Getter
    private HashMap<Language.LanguageType, Config> languages;

    public LanguageManager() {
        languages = new HashMap<>();

        for (Language.LanguageType type : Language.LanguageType.values()) {
            Configuration configuration = new Configuration(type);
            if (!configuration.exists()) {
                Config config = new Config();
                configuration.writeConfiguration(config);
                System.out.println("Create Language file for " + type);
            }
            configuration.readConfiguration();
            Config config = configuration.getConfig();

            languages.put(type, config);
        }
    }

    public void reload() {
        languages.clear();

        for (Language.LanguageType type : Language.LanguageType.values()) {
            Configuration configuration = new Configuration(type);
            if (!configuration.exists()) {
                Config config = new Config();
                configuration.writeConfiguration(config);
                System.out.println("Create Language file for " + type);
            }
            configuration.readConfiguration();
            Config config = configuration.getConfig();

            languages.put(type, config);
        }
    }
}