package net.vicemice.hector.server.utils.labymod.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import net.vicemice.hector.api.labymod.Addon;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.List;

/*
 * Class created at 01:44 - 23.04.2020
 * Copyright (C) elrobtossohn
 */
@AllArgsConstructor
@Getter
public class LabyModPlayerJoinEvent extends Event {

    @Getter
    private final static HandlerList handlerList = new HandlerList();

    private Player player;
    private String modVersion;
    private boolean chunkCachingEnabled;
    private int chunkCachingVersion;
    private List<Addon> addons;

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

}