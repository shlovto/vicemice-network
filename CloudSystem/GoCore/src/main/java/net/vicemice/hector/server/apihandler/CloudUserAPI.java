package net.vicemice.hector.server.apihandler;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.coins.EditCoinPacket;
import net.vicemice.hector.packets.types.player.coins.GetCoinsPacket;
import net.vicemice.hector.packets.types.player.level.EditXPPacket;
import net.vicemice.hector.packets.types.player.replay.PacketEditReplay;
import net.vicemice.hector.packets.types.player.replay.PacketReplayEntries;
import net.vicemice.hector.packets.types.player.replay.PacketReplayHistoryAdd;
import net.vicemice.hector.packets.types.player.replay.WatchReplayPacket;
import net.vicemice.hector.packets.types.player.stats.GetStatsTopPacket;
import net.vicemice.hector.packets.types.player.stats.StatisticEditPacket;
import net.vicemice.hector.packets.types.server.clan.ClanRequestPacket;
import net.vicemice.hector.packets.types.server.party.PartyRequestPacket;
import net.vicemice.hector.packets.types.server.party.RequestPublicPartiesPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.nick.NickManager;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.server.utils.CoinsManager;
import net.vicemice.hector.server.utils.TopStatsManager;
import net.vicemice.hector.types.Callback;
import net.vicemice.hector.types.UserAPI;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.clan.Clan;
import net.vicemice.hector.utils.players.party.Party;
import org.bukkit.Bukkit;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;

/*
 * Class created at 18:54 - 31.03.2020
 * Copyright (C) elrobtossohn
 */
public class CloudUserAPI implements UserAPI {

    @Override
    public void requestParty(UUID uniqueId) {
        GoServer.getService().getGoAPI().getNettyClient().getChannel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.PARTY_REQUEST, new PartyRequestPacket(uniqueId)));
    }

    @Override
    public Party getParty(UUID uniqueId) {
        return null;
    }

    @Override
    public void getPublicParties(Callback<ArrayList<Party>> callback) {
        String callbackID = UUID.randomUUID().toString();
        CoinsManager.getCoinsCallbacks().put(callbackID, callback);
        GoServer.getService().getGoAPI().getNettyClient().getChannel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.PUBLIC_PARTIES_REQUEST, new RequestPublicPartiesPacket(callbackID)));
    }

    @Override
    public void requestClan(UUID uniqueId) {
        GoServer.getService().getGoAPI().getNettyClient().getChannel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.CLAN_REQUEST, new ClanRequestPacket(uniqueId)));
    }

    @Override
    public Clan getClan(UUID uniqueId) {
        return null;
    }

    @Override
    public PlayerRankData getRankData(UUID uniqueId) {
        if (GoServer.getService().getPermissionManager().getUserRanks().containsKey(uniqueId)) {
            Rank rank = GoServer.getService().getPermissionManager().getUserRanks().get(uniqueId).getRank();
            return new PlayerRankData(rank.getRankName(), rank.getRankColor(), rank.getAccessLevel());
        }
        return new PlayerRankData(Rank.MEMBER.getRankName(), Rank.MEMBER.getRankColor(), Rank.MEMBER.getAccessLevel());
    }

    @Override
    public void addXP(UUID uniqueId, long xp) {
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.EDIT_XP_PACKET, new EditXPPacket(uniqueId, xp, false)), API.PacketReceiver.SLAVE);
    }

    @Override
    public void removeXP(UUID uniqueId, long xp) {
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.EDIT_XP_PACKET, new EditXPPacket(uniqueId, xp, false)), API.PacketReceiver.SLAVE);
    }

    @Override
    public void addCoins(UUID uniqueId, long coins) {
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.EDIT_COIN_PACKET, new EditCoinPacket(uniqueId, coins, false)), API.PacketReceiver.SLAVE);
    }

    @Override
    public void removeCoins(UUID uniqueId, long coins) {
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.EDIT_COIN_PACKET, new EditCoinPacket(uniqueId, coins, true)), API.PacketReceiver.SLAVE);
    }

    @Override
    public void getCoins(UUID uniqueId, Callback<Long> callback) {
        String callbackID = UUID.randomUUID().toString();
        CoinsManager.getCoinsCallbacks().put(callbackID, callback);
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.GET_PLAYER_COINS, new GetCoinsPacket(uniqueId, callbackID)), API.PacketReceiver.SLAVE);
    }

    @Override
    public void watchGame(UUID uniqueId, String gameId) {
        GoServer.getService().getGoAPI().getNettyClient().getChannel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.WATCH_REPLAY, new WatchReplayPacket(uniqueId, gameId)));
    }

    @Override
    public void addRecordedGame(UUID uniqueId, String gameId, UUID replayUUID, int replayStartSeconds, long timestamp, long gameLength, ArrayList<String> players, String gameType, String mapType, String map, boolean save) {
        GoServer.getService().getGoAPI().getNettyClient().getChannel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.REPLAY_HISTORY_ADD, new PacketReplayHistoryAdd(uniqueId, new PacketReplayEntries.PacketReplayEntry(gameId, replayUUID, replayStartSeconds, timestamp, gameLength, players, gameType, mapType, map, save))));
    }

    @Override
    public void editReplayFavorites(UUID uniqueId, String gameId, PacketEditReplay.EditType editType) {
        GoServer.getService().getGoAPI().getNettyClient().getChannel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.REPLAY_FAVORITES_EDIT, new PacketEditReplay(uniqueId, gameId, editType)));
    }

    @Override
    public Locale getLocale(UUID uniqueId) {
        if (!GoServer.getService().getPlayerManager().getPlayers().containsKey(uniqueId)) return Locale.GERMANY;
        return (GoServer.getService().getPlayerManager().getPlayers().containsKey(uniqueId) ? GoServer.getService().getPlayerManager().getPlayer(uniqueId).getLocale() : Locale.US);
    }

    @Override
    public String translate(UUID uniqueId, String string) {
        return GoServer.getLocaleManager().getMessage((GoServer.getService().getPlayerManager().getPlayers().containsKey(uniqueId) ? GoServer.getService().getPlayerManager().getPlayer(uniqueId).getLocale() : Locale.US), string);
    }

    @Override
    public String translate(UUID uniqueId, String string, Object... objects) {
        return MessageFormat.format(GoServer.getLocaleManager().getMessage((GoServer.getService().getPlayerManager().getPlayers().containsKey(uniqueId) ? GoServer.getService().getPlayerManager().getPlayer(uniqueId).getLocale() : Locale.US), string), objects);
    }

    @Override
    public void getGameTop(String prefix, String type, int tops, Callback callback) {
        String callBackID = UUID.randomUUID().toString();
        TopStatsManager.getCallbackHashMap().put(callBackID, callback);
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.GET_STATS_TOP, new GetStatsTopPacket(prefix, GetStatsTopPacket.StatsType.valueOf(type), callBackID, tops)), API.PacketReceiver.SLAVE);
    }

    @Override
    public void increaseStatistic(UUID uniqueId, String key, long value) {
        long global = (GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.GLOBAL).containsKey(key) ? GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.GLOBAL).get(key) : 0);
        long monthly = (GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.MONTH).containsKey(key) ? GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.MONTH).get(key) : 0);
        long daily = (GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.DAY).containsKey(key) ? GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.DAY).get(key) : 0);
        if (value < 0) {
            value = 0;
        }
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.GLOBAL).remove(key);
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.MONTH).remove(key);
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.DAY).remove(key);
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.GLOBAL).put(key, (global+value));
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.MONTH).put(key, (monthly+value));
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.DAY).put(key, (daily+value));
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.STATS_EDIT, new StatisticEditPacket(uniqueId, StatisticEditPacket.Action.ADD, key, value)), API.PacketReceiver.SLAVE);
    }

    @Override
    public void decreaseStatistic(UUID uniqueId, String key, long value) {
        long global = (GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.GLOBAL).containsKey(key) ? GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.GLOBAL).get(key) : 0);
        long monthly = (GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.MONTH).containsKey(key) ? GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.MONTH).get(key) : 0);
        long daily = (GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.DAY).containsKey(key) ? GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.DAY).get(key) : 0);
        if (value < 0) {
            global = 0;
            monthly = 0;
            daily = 0;
            value = 0;
        }
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.GLOBAL).remove(key);
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.MONTH).remove(key);
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.DAY).remove(key);
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.GLOBAL).put(key, (global-value));
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.MONTH).put(key, (monthly-value));
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.DAY).put(key, (daily-value));
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.STATS_EDIT, new StatisticEditPacket(uniqueId, StatisticEditPacket.Action.REMOVE, key, value)), API.PacketReceiver.SLAVE);
    }

    @Override
    public void setStatistic(UUID uniqueId, String key, long value) {
        if (value < 0) value = 0;
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.GLOBAL).remove(key);
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.MONTH).remove(key);
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.DAY).remove(key);
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.GLOBAL).put(key, value);
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.MONTH).put(key, value);
        GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(StatisticPeriod.DAY).put(key, value);
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.STATS_EDIT, new StatisticEditPacket(uniqueId, StatisticEditPacket.Action.SET, key, value)), API.PacketReceiver.SLAVE);
    }

    @Override
    public Long getPlayerStatistic(UUID uniqueId, String key, StatisticPeriod period) {
        long value = 0;
        if (GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().containsKey(period)) {
            if (GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(period).containsKey(key)) {
                value = GoServer.getService().getPlayerManager().getPlayer(uniqueId).getStatistics().get(period).get(key);
            }
        }
        return value;
    }

    @Override
    public Long getPlayerRanking(UUID uniqueId, String key, StatisticPeriod period) {
        long value = -1;
        if (GoServer.getService().getPlayerManager().getPlayer(uniqueId).getRanking().containsKey(period)) {
            if (GoServer.getService().getPlayerManager().getPlayer(uniqueId).getRanking().get(period).containsKey(key)) {
                value = GoServer.getService().getPlayerManager().getPlayer(uniqueId).getRanking().get(period).get(key);
            }
        }
        return value;
    }

    @Override
    public UUID getUniqueId(UUID uniqueId) {
        if (NickManager.getPlayerData().containsKey(uniqueId)) {
            return NickManager.getPlayerData().get(uniqueId).getUniqueId();
        }
        return Bukkit.getPlayer(uniqueId).getUniqueId();
    }

    @Override
    public String getNickName(UUID uniqueId) {
        if (NickManager.getPlayerData().containsKey(uniqueId)) {
            return NickManager.getPlayerData().get(uniqueId).getNickName();
        }
        return Bukkit.getPlayer(uniqueId).getName();
    }

    @Override
    public boolean isNicked(UUID uniqueId) {
        return NickManager.getPlayerData().containsKey(uniqueId);
    }
}
