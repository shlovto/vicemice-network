package net.vicemice.hector.proxy.util;

import lombok.Getter;

import java.util.concurrent.ConcurrentHashMap;

public class TimeOutManager {

    @Getter
    private ConcurrentHashMap<String, Long> playerTimeout = new ConcurrentHashMap<>();

    public boolean checkTimeout(String name) {
        if (this.playerTimeout.containsKey(name) && this.playerTimeout.get(name) > System.currentTimeMillis()) {
            return false;
        }
        this.playerTimeout.put(name, System.currentTimeMillis() + 500);
        return true;
    }

    public void logOut(String name) {
        this.playerTimeout.remove(name);
    }
}

