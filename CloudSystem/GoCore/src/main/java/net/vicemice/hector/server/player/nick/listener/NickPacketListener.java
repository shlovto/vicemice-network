package net.vicemice.hector.server.player.nick.listener;

import net.vicemice.hector.api.protocolwrapper.WrapperPlayServerPlayerInfo;
import net.vicemice.hector.api.protocol.wrappers.EnumWrappers;
import net.vicemice.hector.api.protocol.wrappers.PlayerInfoData;
import net.vicemice.hector.api.protocol.wrappers.WrappedChatComponent;
import net.vicemice.hector.api.protocol.wrappers.WrappedGameProfile;
import com.mojang.authlib.GameProfile;
import io.netty.channel.Channel;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.events.PlayerNickEvent;
import net.vicemice.hector.events.PlayerNickedEvent;
import net.vicemice.hector.events.PlayerUnnickEvent;
import net.vicemice.hector.events.PlayerUnnickedEvent;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.nick.PlayerNickPacket;
import net.vicemice.hector.packets.types.player.nick.PlayerUnnickPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.player.nick.NickManager;
import net.vicemice.hector.server.player.nick.utils.PlayerNickData;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class NickPacketListener extends PacketGetter {

    @Override
    public void receivePacket(PacketHolder initPacket) {
        if (initPacket.getKey().equals(PacketType.PLAYER_NICK_PACKET.getKey())) {
            Bukkit.getServer().getScheduler().runTask(GoServer.getService(), () -> {
                PlayerNickPacket playerNickPacket = (PlayerNickPacket) initPacket.getValue();
                PlayerNickData playerNickData = new PlayerNickData(playerNickPacket.getName(), playerNickPacket.getUniqueId(), playerNickPacket.getNickname(), playerNickPacket.getValue(), playerNickPacket.getSignature());
                Player player = Bukkit.getServer().getPlayer(playerNickData.getPlayerName());
                if (player != null) {
                    for (Player onlinePlayer : Bukkit.getServer().getOnlinePlayers()) {
                        if (onlinePlayer.getUniqueId().equals(player.getUniqueId())) continue;
                        WrapperPlayServerPlayerInfo wrapperPlayServerPlayerInfo = new WrapperPlayServerPlayerInfo();
                        wrapperPlayServerPlayerInfo.setAction(EnumWrappers.PlayerInfoAction.REMOVE_PLAYER);
                        wrapperPlayServerPlayerInfo.setData(new ArrayList<PlayerInfoData>() {{
                            this.add(new PlayerInfoData(WrappedGameProfile.fromHandle(new GameProfile(player.getUniqueId(), player.getName())),
                                    0, EnumWrappers.NativeGameMode.SURVIVAL, WrappedChatComponent.fromText("")));
                        }});
                        wrapperPlayServerPlayerInfo.sendPacket(onlinePlayer);
                    }

                    PlayerNickEvent playerNickEvent = new PlayerNickEvent(UserManager.getUser(player), playerNickData.getNickName());
                    NickManager.getPlayerData().put(player.getUniqueId(), playerNickData);
                    Bukkit.getServer().getPluginManager().callEvent(playerNickEvent);
                    for (Player onlinePlayer : Bukkit.getServer().getOnlinePlayers()) {
                        onlinePlayer.hidePlayer(player);
                        onlinePlayer.showPlayer(player);
                    }

                    PlayerNickedEvent playerNickedEvent = new PlayerNickedEvent(UserManager.getUser(player));
                    Bukkit.getServer().getPluginManager().callEvent(playerNickedEvent);
                } else {
                    NickManager.getBeforeJoined().put(playerNickData.getPlayerName(), playerNickData);
                }
            });
        } else if (initPacket.getKey().equals(PacketType.PLAYER_UNNICK_PACKET.getKey())) {
            Bukkit.getServer().getScheduler().runTask(GoServer.getService(), () -> {
                PlayerUnnickPacket playerUnnickPacket = (PlayerUnnickPacket) initPacket.getValue();
                Player player = Bukkit.getServer().getPlayer(playerUnnickPacket.getUsername());
                if (player != null) {
                    if (NickManager.getPlayerData().containsKey(player.getUniqueId())) {
                        for (Player onlinePlayer : Bukkit.getServer().getOnlinePlayers()) {
                            if (onlinePlayer.getUniqueId().equals(player.getUniqueId())) continue;
                            WrapperPlayServerPlayerInfo wrapperPlayServerPlayerInfo = new WrapperPlayServerPlayerInfo();
                            wrapperPlayServerPlayerInfo.setAction(EnumWrappers.PlayerInfoAction.REMOVE_PLAYER);
                            wrapperPlayServerPlayerInfo.setData(new ArrayList<PlayerInfoData>() {{
                                this.add(new PlayerInfoData(WrappedGameProfile.fromHandle(new GameProfile(NickManager.getPlayerData().get(player.getUniqueId()).getUniqueId(), NickManager.getPlayerData().get(player.getUniqueId()).getNickName())),
                                        0, EnumWrappers.NativeGameMode.SURVIVAL, WrappedChatComponent.fromText("")));
                            }});
                            wrapperPlayServerPlayerInfo.sendPacket(onlinePlayer);
                        }

                        PlayerNickData playerNickData = NickManager.getPlayerData().get(player.getUniqueId());
                        PlayerUnnickEvent playerUnnickEvent = new PlayerUnnickEvent(UserManager.getUser(player), playerNickData.getNickName());
                        NickManager.getPlayerData().remove(player.getUniqueId());
                        NickManager.getBeforeJoined().remove(player.getName());
                        Bukkit.getServer().getPluginManager().callEvent(playerUnnickEvent);
                    }
                    for (Player onlinePlayer : Bukkit.getServer().getOnlinePlayers()) {
                        onlinePlayer.hidePlayer(player);
                        onlinePlayer.showPlayer(player);
                    }
                    PlayerUnnickedEvent playerUnnickedEvent = new PlayerUnnickedEvent(UserManager.getUser(player));
                    Bukkit.getServer().getPluginManager().callEvent(playerUnnickedEvent);
                }
            });
        }
    }

    @Override
    public void channelActive(Channel channel) {
    }
}