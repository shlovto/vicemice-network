package net.vicemice.hector.proxy.util.labymod;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.proxy.GoProxy;

import java.util.UUID;

public class LabyModAPI {

    public static void sendMiddleClickActions(ProxiedPlayer player) {
        // List of all action menu entries
        JsonArray array = new JsonArray();

        // Add entries
        JsonObject entry = new JsonObject();
        entry.addProperty("displayName", GoAPI.getUserAPI().translate(player.getUniqueId(), "labymod-add-friend"));
        entry.addProperty("type", EnumActionType.RUN_COMMAND.name());
        entry.addProperty("value", "friend add {name}"); // {name} will be replaced with the players name
        array.add(entry);

        entry = new JsonObject();
        entry.addProperty("displayName", GoAPI.getUserAPI().translate(player.getUniqueId(), "labymod-report-player"));
        entry.addProperty("type", EnumActionType.RUN_COMMAND.name());
        entry.addProperty("value", "report {name}"); // {name} will be replaced with the players name
        array.add(entry);

        entry = new JsonObject();
        entry.addProperty("displayName", GoAPI.getUserAPI().translate(player.getUniqueId(), "labymod-open-stats"));
        entry.addProperty("type", EnumActionType.OPEN_BROWSER.name());
        entry.addProperty("value", "https://vicemice.net/stats/{name}"); // {name} will be replaced with the players name
        array.add(entry);

        // Send to LabyMod using the API
        GoProxy.getProxyService().getLabyService().sendServerMessage(player, "user_menu_actions", array);
    }

    public static void sendCurrentPlayingGamemode(ProxiedPlayer player, boolean visible, String gamemodeName) {
        JsonObject object = new JsonObject();
        object.addProperty("show_gamemode", visible); // Gamemode visible for everyone
        object.addProperty("gamemode_name", gamemodeName); // Name of the current playing gamemode

        // Send to LabyMod using the API
        GoProxy.getProxyService().getLabyService().sendServerMessage(player, "server_gamemode", object);
    }

    public static void updateGame(ProxiedPlayer player, String hasGame, String gameMode) {
        JsonObject gameObject = new JsonObject();
        gameObject.addProperty("hasGame", hasGame);
        gameObject.addProperty("smallImageText", "ViceMice.net");
        gameObject.addProperty("game_startTime", System.currentTimeMillis());
        gameObject.addProperty("game_endTime", "0");
        gameObject.addProperty("game_mode", toLabyGame(player.getUniqueId(), gameMode));

        GoProxy.getProxyService().getLabyService().sendServerMessage(player, "discord_rpc", gameObject);
    }

    public static void updateParty(ProxiedPlayer player, String hasParty, String partyId, String size, String max) {
        JsonObject partyObject = new JsonObject();
        partyObject.addProperty("hasParty", hasParty);
        partyObject.addProperty("partyId", partyId);
        partyObject.addProperty("party_size", size);
        partyObject.addProperty("party_max", max);

        GoProxy.getProxyService().getLabyService().sendServerMessage(player, "discord_rpc", partyObject);
    }

    public static void updateMatch(ProxiedPlayer player, String hasMatchSecret, String matchSecret) {
        JsonObject matchObject = new JsonObject();
        matchObject.addProperty("hasMatchSecret", hasMatchSecret);
        matchObject.addProperty("matchSecret", matchSecret + ":.vicemice.net");

        GoProxy.getProxyService().getLabyService().sendServerMessage(player, "discord_rpc", matchObject);
    }

    public static void updateSpec(ProxiedPlayer player, String hasSpectateSecret, String specSecret) {
        JsonObject specObject = new JsonObject();
        specObject.addProperty("hasSpectateSecret", hasSpectateSecret);
        specObject.addProperty("specSecret", specSecret + ":.vicemice.net");

        GoProxy.getProxyService().getLabyService().sendServerMessage(player, "discord_rpc", specObject);
    }

    public static void updateJoin(ProxiedPlayer player, String hasJoinSecret, String joinSecret) {
        JsonObject joinObject = new JsonObject();
        joinObject.addProperty("hasJoinSecret", hasJoinSecret);
        joinObject.addProperty("joinSecret", joinSecret + ":.vicemice.net");

        GoProxy.getProxyService().getLabyService().sendServerMessage(player, "discord_rpc", joinObject);
    }

    private static String toLabyGame(UUID uuid, String server) {
        return GoAPI.getUserAPI().translate(uuid, "discord-is-on", server.replace("-", "").toUpperCase());

        /*if (server.contains("Holodeck")) {
            if (player.get("language").equalsIgnoreCase("german")) {
                game = "Spielt gerade auf " + server;
            } else {
                game = "Just playing on " + server;
            }
        } else if (server.contains("MineWar")) {
            if (player.get("language").equalsIgnoreCase("german")) {
                game = "Spielt gerade auf " + server;
            } else {
                game = "Just playing on " + server;
            }
        } else if (server.contains("FFA")) {
            if (player.get("language").equalsIgnoreCase("german")) {
                game = "Spielt gerade auf " + server;
            } else {
                game = "Just playing on " + server;
            }
        } else if (server.contains("KBFFA")) {
            if (player.get("language").equalsIgnoreCase("german")) {
                game = "Spielt gerade auf " + server;
            } else {
                game = "Just playing on " + server;
            }
        } else if (server.contains("Space")) {
            if (player.get("language").equalsIgnoreCase("german")) {
                game = "Spielt gerade auf " + server;
            } else {
                game = "Just playing on " + server;
            }
        } else if (server.contains("Replay")) {
            if (player.get("language").equalsIgnoreCase("german")) {
                game = "befindet sich auf " + server;
            } else {
                game = "playing on " + server;
            }
        } else if (server.contains("BauServer")) {
            if (player.get("language").equalsIgnoreCase("german")) {
                game = "befindet sich auf " + server;
            } else {
                game = "playing on " + server;
            }
        } else if (server.contains("Lobby")) {
            if (player.get("language").equalsIgnoreCase("german")) {
                game = "befindet sich auf " + server;
            } else {
                game = "playing on " + server;
            }
        } else if (server.contains("back")) {
            if (player.get("language").equalsIgnoreCase("german")) {
                game = "befindet sich auf " + server;
            } else {
                game = "playing on " + server;
            }
        } else if (server.contains("Verify")) {
            if (player.get("language").equalsIgnoreCase("german")) {
                game = "befindet sich auf " + server;
            } else {
                game = "playing on " + server;
            }
        } else if (server.startsWith("CW")) {
            if (player.get("language").equalsIgnoreCase("german")) {
                game = "Spielt gerade ClanWar";
            } else {
                game = "Just playing ClanWar";
            }
        } else if (server.contains("Dev")) {
            if (player.get("language").equalsIgnoreCase("german")) {
                game = "befindet sich auf " + server;
            } else {
                game = "playing on " + server;
            }
        } else if (server.startsWith("P")) {
            if (player.get("language").equalsIgnoreCase("german")) {
                game = "Spielt gerade auf einem Private Server";
            } else {
                game = "Just playing on a Private Server";
            }
        } else {
            if (player.get("language").equalsIgnoreCase("german")) {
                game = "befindet sich auf " + server;
            } else {
                game = "playing on " + server;
            }
        }

        return game;*/
    }

    enum EnumActionType {
        NONE,
        CLIPBOARD,
        RUN_COMMAND,
        SUGGEST_COMMAND,
        OPEN_BROWSER
    }
}