package net.vicemice.hector.server.netty;

import io.netty.channel.Channel;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.server.ServerAlive;
import net.vicemice.hector.server.GoServer;
import org.bukkit.Bukkit;

public class AliveSender implements Runnable {
    private int nextCheck = 30;

    @Override
    public void run() {
        this.sendAlivePacket();
        if (GoServer.getService().getServerName() != null && GoServer.getService().getServerName().equalsIgnoreCase("buycraft-1")) {
            if (this.nextCheck == 0) {
                this.nextCheck = 30;
                Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "buycraft forcecheck");
            } else {
                --this.nextCheck;
            }
        }
        if (GoServer.getService().getServerName() != null && GoServer.getService().getServerName().equalsIgnoreCase("silent-1")) {
            if (this.nextCheck == 0) {
                this.nextCheck = 30;
                Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "silent forcecheck");
            } else {
                --this.nextCheck;
            }
        }
    }

    public void sendAlivePacket() {
        Bukkit.getServer().getScheduler().runTaskAsynchronously(GoServer.getService(), () -> {
            Channel channel = GoServer.getService().getGoAPI().getNettyClient().getChannel();
            if (GoServer.getService().getServerName() != null && channel != null && channel.isActive() && channel.isOpen()) {
                channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.SERVER_ALIVE, new ServerAlive(GoServer.getService().getServerName())));
                //channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.SLAVE_PLAYER_SIZE_PACKET, new SlavePlayerSizePacket(GoServer.getService().getServerName(), Bukkit.getOnlinePlayers().size())));
            }
        });
    }
}