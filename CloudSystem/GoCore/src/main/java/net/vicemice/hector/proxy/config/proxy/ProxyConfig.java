package net.vicemice.hector.proxy.config.proxy;

import lombok.Data;

/**
 * Created by Paul B. on 27.01.2019.
 */
@Data
public class ProxyConfig {
    private String host = "127.0.0.1";
    private String port = "25565";
    private String proxyid = "Proxy-XX";
    private boolean dev = false;
    private boolean beta = false;
    private boolean verify = false;
}