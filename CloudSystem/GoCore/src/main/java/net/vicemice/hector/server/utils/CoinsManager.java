package net.vicemice.hector.server.utils;

import lombok.Getter;
import net.vicemice.hector.types.Callback;

import java.util.concurrent.ConcurrentHashMap;

/*
 * Class created at 12:18 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class CoinsManager {
    @Getter
    private static ConcurrentHashMap<String, Callback> coinsCallbacks = new ConcurrentHashMap<>();
}