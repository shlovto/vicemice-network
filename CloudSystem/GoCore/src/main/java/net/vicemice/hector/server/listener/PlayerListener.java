package net.vicemice.hector.server.listener;

import net.vicemice.hector.events.PlayerNickEvent;
import net.vicemice.hector.events.PlayerNickedEvent;
import net.vicemice.hector.events.PlayerUnnickEvent;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.player.nick.NickManager;
import net.vicemice.hector.server.player.nick.utils.PlayerNickData;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        boolean nicked;
        Player player = event.getPlayer();
        if (GoServer.getService().getGoAPI().getPlayerCommands().containsKey(player.getName().toLowerCase())) {
            for (String command : GoServer.getService().getGoAPI().getPlayerCommands().get(player.getName().toLowerCase())) {
                Bukkit.getServer().dispatchCommand((Bukkit.getServer().getConsoleSender()), command.replaceAll("/", ""));
            }
            GoServer.getService().getGoAPI().getPlayerCommands().remove(player.getName().toLowerCase());
        }
        if (nicked = NickManager.getBeforeJoined().containsKey(player.getName())) {
            NickManager.getPlayerData().put(player.getUniqueId(), NickManager.getBeforeJoined().get(player.getName()));
            PlayerNickData playerNickData = NickManager.getPlayerData().get(player.getUniqueId());
            PlayerNickEvent playerNickEvent = new PlayerNickEvent(UserManager.getUser(player), playerNickData.getNickName());
            Bukkit.getServer().getPluginManager().callEvent(playerNickEvent);
        }
        if (nicked) {
            for (Player onlinePlayer : Bukkit.getServer().getOnlinePlayers()) {
                if (onlinePlayer == player) continue;
                onlinePlayer.hidePlayer(player);
                onlinePlayer.showPlayer(player);
            }
            PlayerNickedEvent playerNickedEvent = new PlayerNickedEvent(UserManager.getUser(player));
            Bukkit.getServer().getPluginManager().callEvent(playerNickedEvent);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (NickManager.getPlayerData().containsKey(event.getPlayer().getUniqueId())) {
            PlayerNickData playerNickData = NickManager.getPlayerData().get(event.getPlayer().getUniqueId());
            PlayerUnnickEvent playerUnnickEvent = new PlayerUnnickEvent(UserManager.getUser(event.getPlayer()), playerNickData.getNickName());
            NickManager.getPlayerData().remove(event.getPlayer().getUniqueId());
            NickManager.getBeforeJoined().remove(event.getPlayer().getName());
            Bukkit.getServer().getPluginManager().callEvent(playerUnnickEvent);
        }
    }
}