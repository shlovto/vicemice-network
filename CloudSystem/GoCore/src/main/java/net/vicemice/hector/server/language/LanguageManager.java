package net.vicemice.hector.server.language;

import lombok.Getter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.language.Language;
import net.vicemice.hector.utils.language.LanguageAction;

import java.util.HashMap;
import java.util.UUID;

public class LanguageManager {

    @Getter
    private HashMap<UUID, Language.LanguageType> languages;

    public void init() {
        this.languages = new HashMap<>();
        GoServer.getService().setLanguageAction(new LanguageAction() {
            @Override
            public void done(UUID uuid) {
                languages.put(uuid, GoAPI.getLanguageAPI().getLanguage(uuid));
            }
        });
    }
}
