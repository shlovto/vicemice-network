package net.vicemice.hector.server.language;

import io.netty.channel.Channel;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.LanguagePacket;
import net.vicemice.hector.utils.language.Language;

import java.util.HashMap;
import java.util.UUID;

public class LanguageHandler extends PacketGetter {

    public HashMap<UUID, Language.LanguageType> languages;

    @Override
    public void receivePacket(PacketHolder initPacket) {
        if (initPacket.getKey().equals(PacketType.LANGUAGE_PACKET.getKey())) {
            LanguagePacket languagePacket = (LanguagePacket) initPacket.getValue();

            /*if (!languages.containsKey(languagePacket.getUniqueId())) {
                languages.put(languagePacket.getUniqueId(), languagePacket.getLanguage());
                GoServer.getService().getLanguageAction().done(languagePacket.getUniqueId());
            } else {
                languages.remove(languagePacket.getUniqueId());
                languages.put(languagePacket.getUniqueId(), languagePacket.getLanguage());
                GoServer.getService().getLanguageAction().done(languagePacket.getUniqueId());
            }*/
        }
    }

    @Override
    public void channelActive(Channel channel) {
    }
}