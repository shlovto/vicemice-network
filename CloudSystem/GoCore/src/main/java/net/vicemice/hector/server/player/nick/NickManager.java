package net.vicemice.hector.server.player.nick;

import net.vicemice.hector.server.player.nick.utils.PlayerNickData;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.*;

public class NickManager {
    private static HashMap<String, PlayerNickData> beforeJoined = new HashMap<>();
    private static HashMap<UUID, PlayerNickData> playerData = new HashMap<>();

    public static List<Player> getAllOnlinePlayersAsList() {
        ArrayList<Player> list = new ArrayList<Player>();
        list.addAll(Bukkit.getOnlinePlayers());
        return list;
    }

    public static List<String> getUsedNicknames() {
        ArrayList<String> names = new ArrayList<String>();
        for (UUID uuid : playerData.keySet()) {
            names.add(playerData.get(uuid).getNickName());
        }
        return Collections.unmodifiableList(names);
    }

    public static HashMap<String, PlayerNickData> getBeforeJoined() {
        return beforeJoined;
    }

    public static HashMap<UUID, PlayerNickData> getPlayerData() {
        return playerData;
    }
}