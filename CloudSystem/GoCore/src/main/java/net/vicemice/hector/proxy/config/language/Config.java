package net.vicemice.hector.proxy.config.language;

import lombok.Getter;

/**
 * Created by Paul B. on 27.01.2019.
 */
@Getter
public class Config {
    private String COMMAND_TIMEOUT = "&cPlease wait a moment before you execute this command again.";
    private String CURRENTLY_PLAYING = "%PREFIX% &7Currently playing &c%PLAYERS% &7Players!";

    private String PLAYER_NOT_EXIST = "%PREFIX% &cThis player does not exist!";
    private String PLAYER_NOT_ONLINE = "%PREFIX% &cThis player is not online!";
    private String USER_NOT_ONLINE = "%PREFIX% %USER% &cis not online!";
    private String PLAYER_NOT_RECEIVE_MSG = "%PREFIX% %USER% &cdoes not receive messages.";
    private String PLAYER_NOT_FRIEND = "%PREFIX% &cYou are not friended of this player!";
    private String OTHER_USER_WITH_IP_ONLINE = "&cThere is already someone with the same IP on the network, \n if this should be a mistake please contact an administrator or developer.";
    private String IP_LIMIT_REACHED = "&cThe limit for your IP is reached, to increase the limit, contact one of our administrators.";

    private String[] CHAT_MUTE_PERM = new String[] { "&cYou have been muted &4&lPERMANENTLY&c!", "&cReason: &7%REASON%" };
    private String[] CHAT_MUTE_TEMP = new String[] { "&cYou have been muted &4&lTEMPORARY&c!", "&cReason: &7%REASON%", "&cRemaining Time: &7%TIME%" };
    private String CHAT_REPEATING = "&cYou are repeating yourself!";
    private String CHAT_INVALID_CHARS = "&cYour message contains invalid characters.";
    private String CHAT_BEHAVIOR = "&cWatch your chat behavior!";

    private String KICK_SCREEN = "&cYou were kicked from the network\n\n&7Reason: &c%REASON%";
    private String LOGIN_BAN_TEMP_SCREEN = "&cYou have been banned from the network &4&lTEMPORARY&c!\n&7Reason: &c%REASON%\n\n&7Remaining time: &c%TIME%\n\n" + "&cYou can create an Appeal on &eViceMice.net/appeal&c.";
    private String LOGIN_BAN_PERM_SCREEN = "&cYou have been banned from the network &4&lPERMANENTLY&c!\n&7Reason: &c%REASON%\n\n&cYou can create an Appeal on &eViceMice.net/appeal&c.";
    private String LOGIN_ALREADY_ONLINE = "&cYou are already online on the network! \n\n &cIf this error does not disappear after 10 minutes, \n &cplease contact a staffmember";
    private String LOGIN_CLOUD_ERROR = "&cThis proxy is not connected to the cloud!\n&cEnter our forum to find out more.\n&6https://ViceMice.net";
    private String LOGIN_MAINTENANCE = "&cThe network is currently in maintenance!\n&cEnter our forum to be always up to date!\n&6https://ViceMice.net";
    private String LOGIN_OLD_VERSION = "&cThere is no connection possible!\n&cYou are using an older Minecraft version.\n&cYou think this is a mistake?\n&cContact us on our TeamSpeak or Discord Server.";
    private String LOGIN_NETWORK_FULL = "&cThe player limit of &e%LIMIT% &cis reached.\n&3You need the &2Premium &3rank to be able to &ejoin &3anyway\n\n&aFurther information at &ehttps://ViceMice.net";
}