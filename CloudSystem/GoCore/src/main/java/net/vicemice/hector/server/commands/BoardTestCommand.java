package net.vicemice.hector.server.commands;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.math.MathUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/*
 * Class created at 20:46 - 06.04.2020
 * Copyright (C) elrobtossohn
 */
public class BoardTestCommand extends Command {

    public BoardTestCommand() {
        super("board");
    }

    @Override
    public boolean execute(CommandSender commandSender, String label, String[] args) {
        Player player = (Player) commandSender;

        if (GoAPI.getUserAPI().getRankData(player.getUniqueId()).getAccess_level() < Rank.MODERATOR.getAccessLevel()) {
            player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-no-permission"));
            return true;
        }

        if (args.length == 1 && args[0].equalsIgnoreCase("reset")) {
            if (GoServer.getService().getPlayerManager().getPacketScoreboard(player.getUniqueId()) == null) {
                player.sendMessage("§cYou can't reset a board which not exist");
                return true;
            }

            for (int i : GoServer.getService().getPlayerManager().getPacketScoreboard(player.getUniqueId()).getLines().keySet()) {
                GoServer.getService().getPlayerManager().getPacketScoreboard(player.getUniqueId()).removeLine(i);
            }

            player.sendMessage("§aDone!");
            return true;
        }

        if (args.length == 1 && args[0].equalsIgnoreCase("remove")) {
            if (GoServer.getService().getPlayerManager().getPacketScoreboard(player.getUniqueId()) == null) {
                player.sendMessage("§cYou can't reset a board which not exist");
                return true;
            }

            GoServer.getService().getPlayerManager().getPacketScoreboard(player.getUniqueId()).remove();
            GoServer.getService().getPlayerManager().getScorePlayers().remove(player.getUniqueId());
            player.sendMessage("§cRemoved!");
            return true;
        }

        if (args.length < 3) {
            player.sendMessage("§cSyntax: /board <create|set|remove> <Line|0> <Text (max. 16 Chars)>");
            return true;
        }

        StringBuilder sb = new StringBuilder();
        int i = 2;
        while (i < args.length) {
            if (i < args.length) {
                sb.append(String.valueOf(args[i]) + " ");
            } else {
                sb.append(args[i]);
            }
            ++i;
        }
        String message = sb.toString();

        if (GoServer.getService().getPlayerManager().getPacketScoreboard(player.getUniqueId()) == null) {
            if (args[0].equalsIgnoreCase("create")) {
                if (GoServer.getService().getPlayerManager().createScoreboard(player.getUniqueId(), message.replace("&", "§"))) {
                    GoServer.getService().getPlayerManager().getPacketScoreboard(player.getUniqueId()).setLine(0, "§a§lCHANGEABLE");
                    player.sendMessage("§aBoard has been created!");
                } else {
                    player.sendMessage("§cA board already exist.");
                }
            } else {
                player.sendMessage("§cYou can't edit a board which are not exist!");
                player.sendMessage("§cSyntax: /board create 0 <Title>");
            }
            return true;
        }

        if (!MathUtils.isInt(args[1])) {
            player.sendMessage("§cThe line must be an number");
            return true;
        }

        int line = Integer.parseInt(args[1]);

        if (args[0].equalsIgnoreCase("set")) {
            GoServer.getService().getPlayerManager().getPacketScoreboard(player.getUniqueId()).updateLine(line, message.replace("&", "§"));
        } else if (args[0].equalsIgnoreCase("remove")) {
            GoServer.getService().getPlayerManager().getPacketScoreboard(player.getUniqueId()).removeLine(line);
        } else {
            player.sendMessage("§cSyntax: /board <set|remove> <Line> <Text (max. 16 Chars)>");
        }

        return false;
    }
}