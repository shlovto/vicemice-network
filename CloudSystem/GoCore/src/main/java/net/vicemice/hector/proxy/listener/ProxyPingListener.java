package net.vicemice.hector.proxy.listener;

import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.proxy.GoProxy;

import java.util.UUID;

public class ProxyPingListener implements Listener {

    @EventHandler
    public void onProxyPing(ProxyPingEvent event) {
        String[] extraData;
        ServerPing serverPing = event.getResponse();
        UUID uniqueId = UUID.fromString("336991c8-1199-457c-80bb-649042a489f5");

        String protocolNoConnection = GoAPI.getUserAPI().translate( uniqueId, "protocol-no-connection");
        String protocolVerifyNetwork = GoAPI.getUserAPI().translate( uniqueId, "protocol-verify-network");
        String protocolMaintenance = GoAPI.getUserAPI().translate( uniqueId, "protocol-maintenance");
        String motdOldVersion = GoAPI.getUserAPI().translate( uniqueId, "motd-old-version");
        String motdNoConnection = GoAPI.getUserAPI().translate( uniqueId, "motd-no-cloud-connection");
        String motdVerifyNetwork = GoAPI.getUserAPI().translate( uniqueId, "motd-verify-network");

        if(event.getConnection().getVersion() < 47) {
            serverPing.setVersion(new ServerPing.Protocol(protocolNoConnection, 2));
            ServerPing.Players players = new ServerPing.Players(0, 0, null);
            serverPing.setPlayers(players);
            serverPing.setDescription(GoProxy.getProxyService().getProxyManager().getFirstLine()+motdOldVersion);
            event.setResponse(serverPing);
            return;
        }

        if (GoProxy.getProxyService().getProxySettingsPacket() == null && !GoProxy.getProxyService().getProxyManager().isConnected()) {
            serverPing.setVersion(new ServerPing.Protocol(protocolNoConnection, 2));
            ServerPing.Players players = new ServerPing.Players(0, 0, null);
            serverPing.setPlayers(players);
            serverPing.setDescription(GoProxy.getProxyService().getProxyManager().getFirstLine()+motdNoConnection);
            event.setResponse(serverPing);
            return;
        }

        if (GoProxy.getProxyService().getProxyManager().isConnected() && GoProxy.getProxyService().getProxyManager().isVerify()) {
            serverPing.setVersion(new ServerPing.Protocol(protocolVerifyNetwork, 2));
            ServerPing.Players players = new ServerPing.Players(0, 0, null);
            serverPing.setPlayers(players);
            serverPing.setDescription(GoProxy.getProxyService().getProxyManager().getFirstLine()+motdVerifyNetwork);
            event.setResponse(serverPing);
            return;
        }

        if (GoProxy.getProxyService().getProxyManager().isConnected() && GoProxy.getProxyService().getProxyManager().isMaintenance()) {
            serverPing.setVersion(new ServerPing.Protocol(protocolMaintenance, 2));
            ServerPing.Players players = new ServerPing.Players(0, 0, null);
            serverPing.setPlayers(players);
            serverPing.setDescription(GoProxy.getProxyService().getProxyManager().getMaintenanceMOTD().replace("%PROXY%", GoProxy.getProxyService().getProxyConfig().getProxyid()));
            event.setResponse(serverPing);
            return;
        }

        String[] arrstring = extraData = event.getConnection().getVirtualHost() != null && event.getConnection().getVirtualHost().getHostName() != null ? event.getConnection().getVirtualHost().getHostName().split("-", 2) : new String[]{};

        if (extraData.length > 1 && extraData[1].equalsIgnoreCase("bungeeOnly")) {
            return;
        }
        ServerPing.Players players = new ServerPing.Players(GoProxy.getProxyService().getProxySettingsPacket().getMaxPlayers(), GoProxy.getProxyService().getOnlinePlayers(), null);
        serverPing.setPlayers(players);
        serverPing.setDescription(GoProxy.getProxyService().getProxyManager().getMOTD().replace("%PROXY%", GoProxy.getProxyService().getProxyConfig().getProxyid()));
        event.setResponse(serverPing);
    }
}

