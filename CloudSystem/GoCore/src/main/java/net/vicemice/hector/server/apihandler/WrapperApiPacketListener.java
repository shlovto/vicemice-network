package net.vicemice.hector.server.apihandler;

import io.netty.channel.Channel;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.coins.CoinsPacket;
import net.vicemice.hector.packets.types.player.stats.TopStatsPacket;
import net.vicemice.hector.server.utils.CoinsManager;
import net.vicemice.hector.server.utils.TopStatsManager;

public class WrapperApiPacketListener extends PacketGetter {

    @Override
    public void receivePacket(PacketHolder initPacket) {
        if (initPacket.getKey().equals(PacketType.COINS_PACKET.getKey())) {
            CoinsPacket coinsPacket = (CoinsPacket) initPacket.getValue();
            if (CoinsManager.getCoinsCallbacks().containsKey(coinsPacket.getCallBackID())) {
                CoinsManager.getCoinsCallbacks().get(coinsPacket.getCallBackID()).done(coinsPacket.getCoins());
                CoinsManager.getCoinsCallbacks().remove(coinsPacket.getCallBackID());
            }
        } else if (initPacket.getKey().equals(PacketType.TOP_STATS_PACKET.getKey())) {
            TopStatsPacket topStatsPacket = (TopStatsPacket) initPacket.getValue();
            if (TopStatsManager.getCallbackHashMap().containsKey(topStatsPacket.getCallBackID())) {
                TopStatsManager.getCallbackHashMap().get(topStatsPacket.getCallBackID()).done(topStatsPacket.getStatsData());
                TopStatsManager.getCallbackHashMap().remove(topStatsPacket.getCallBackID());
            }
        }
    }

    @Override
    public void channelActive(Channel channel) {
    }
}