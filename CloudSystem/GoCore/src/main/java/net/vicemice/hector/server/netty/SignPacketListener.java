package net.vicemice.hector.server.netty;

import io.netty.channel.Channel;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.server.StatSignPacket;

public class SignPacketListener extends PacketGetter {
    @Override
    public void receivePacket(PacketHolder initPacket) {
        if (initPacket.getKey().equals(PacketType.STAT_SIGN_PACKET.getKey())) {
            StatSignPacket statSignPacket = (StatSignPacket) initPacket.getValue();
            StatsManager.receiveStatPacket(statSignPacket);
        }
    }

    @Override
    public void channelActive(Channel channel) {
    }
}