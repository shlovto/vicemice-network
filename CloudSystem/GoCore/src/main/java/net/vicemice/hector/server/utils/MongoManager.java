package net.vicemice.hector.server.utils;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import lombok.Getter;
import net.vicemice.hector.server.GoServer;
import org.bson.Document;
import java.util.ArrayList;
import java.util.List;

/*
 * Class created at 23:19 - 04.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class MongoManager {
    @Getter
    private MongoClient client;
    @Getter
    private MongoDatabase mongoDatabase, lobbyService, gameDatabase, citybuildDatabase;

    public MongoManager() {
        String host = GoServer.getService().getMongoConfig().getHost();
        int port = Integer.parseInt(GoServer.getService().getMongoConfig().getPort());
        String user = GoServer.getService().getMongoConfig().getUsername();
        char[] password = GoServer.getService().getMongoConfig().getPassword().toCharArray();

        ServerAddress serverAddress = new ServerAddress(host, port);
        List<MongoCredential> credentials = new ArrayList<>();
        credentials.add(
                MongoCredential.createCredential(
                        user,
                        "admin",
                        password
                )
        );
        client = new MongoClient(serverAddress, credentials);
        mongoDatabase = client.getDatabase("networkService");
        lobbyService = client.getDatabase("lobbyService");
        gameDatabase = client.getDatabase("gameService");
    }

    public ArrayList<Document> getDocuments(Database database, String collection) {
        ArrayList<Document> docs = new ArrayList<>();

        for (Document document : client.getDatabase(database.getDatabase()).getCollection(collection).find()) {
            docs.add(document);
        }

        return docs;
    }

    public ArrayList<Document> getDocumentsWithFilter(Database database, String collection, Document filter) {
        ArrayList<Document> docs = new ArrayList<>();

        for (Document document : client.getDatabase(database.getDatabase()).getCollection(collection).find(filter)) {
            docs.add(document);
        }

        return docs;
    }

    public ArrayList<Document> getDocumentsWithSort(Database database, String collection, Document sort) {
        ArrayList<Document> docs = new ArrayList<>();

        FindIterable<Document> o = client.getDatabase(database.getDatabase()).getCollection(collection).find().sort(sort);

        if (o != null) {
            for (Document document : o) {
                docs.add(document);
            }
        }

        return docs;
    }

    public ArrayList<Document> getDocumentsWithFilterAndSort(Database database, String collection, Document filter, Document sort) {
        ArrayList<Document> docs = new ArrayList<>();

        for (Document document : client.getDatabase(database.getDatabase()).getCollection(collection).find(filter).sort(sort)) {
            docs.add(document);
        }

        return docs;
    }

    public ArrayList<Document> getDocumentsWithFilterAndSortAndLimit(Database database, String collection, Document filter, Document sort, Integer limit) {
        ArrayList<Document> docs = new ArrayList<>();

        for (Document document : client.getDatabase(database.getDatabase()).getCollection(collection).find(filter).sort(sort).limit(limit)) {
            docs.add(document);
        }

        return docs;
    }

    public ArrayList<Document> getDocumentsWithLimit(Database database, String collection, Integer limit) {
        ArrayList<Document> docs = new ArrayList<>();

        for (Document document : client.getDatabase(database.getDatabase()).getCollection(collection).find().limit(limit)) {
            docs.add(document);
        }

        return docs;
    }

    public ArrayList<Document> getDocumentsWithFilterAndLimit(Database database, String collection, Document filter, Integer limit) {
        ArrayList<Document> docs = new ArrayList<>();

        for (Document document : client.getDatabase(database.getDatabase()).getCollection(collection).find(filter).limit(limit)) {
            docs.add(document);
        }

        return docs;
    }

    public Document getDocumentWithFilterAndLimit(Database database, String collection, Document filter, Integer limit) {
        for (Document document : client.getDatabase(database.getDatabase()).getCollection(collection).find(filter).limit(limit)) {
            return document;
        }

        return null;
    }

    public Document getDocument(Database database, String collection, Document filter) {
        for (Document document : client.getDatabase(database.getDatabase()).getCollection(collection).find(filter)) {
            return document;
        }

        return null;
    }

    public enum Database {
        CLOUD("cloud"),
        LOBBY("lobbyService"),
        GAMES("gameService");

        private Database(String database) {
            this.database = database;
        }

        @Getter
        private String database;
    }
}