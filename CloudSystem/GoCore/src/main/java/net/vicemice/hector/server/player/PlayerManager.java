package net.vicemice.hector.server.player;

import io.netty.channel.Channel;
import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.events.PlayerLoginEvent;
import net.vicemice.hector.events.PlayerUpdateDataEvent;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.PlayerServerDataPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.scoreboard.PacketScoreboard;
import net.vicemice.hector.utils.players.NetworkPlayer;
import org.bukkit.Bukkit;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class PlayerManager extends PacketGetter {

    @Getter
    private final ConcurrentHashMap<UUID, NetworkPlayer> players;
    @Getter
    private final ConcurrentHashMap<UUID, PacketScoreboard> scorePlayers;
    @Getter
    @Setter
    private boolean prototypeEffects;

    public PlayerManager() {
        this.players = new ConcurrentHashMap<>();
        this.scorePlayers = new ConcurrentHashMap<>();
    }

    public NetworkPlayer getPlayer(UUID uniqueId) {
        return this.players.get(uniqueId);
    }

    public PacketScoreboard getPacketScoreboard(UUID uniqueId) {
        return this.scorePlayers.get(uniqueId);
    }

    public boolean createScoreboard(UUID uniqueId, String title) {
        if (scorePlayers.containsKey(uniqueId)) return false;
        this.scorePlayers.put(uniqueId, new PacketScoreboard(Bukkit.getPlayer(uniqueId)));
        this.scorePlayers.get(uniqueId).sendSidebar(title);
        return true;
    }

    public boolean createScoreboard(UUID uniqueId, PacketScoreboard packetScoreboard) {
        if (scorePlayers.containsKey(uniqueId)) {
            scorePlayers.get(uniqueId).remove();
            scorePlayers.remove(uniqueId);
        }
        this.scorePlayers.put(uniqueId, packetScoreboard);
        return true;
    }

    @Override
    public void receivePacket(PacketHolder var1) {
        if (var1.getType() == PacketType.PLAYER_SERVER_DATA) {
            PlayerServerDataPacket playerServerDataPacket = (PlayerServerDataPacket) var1.getValue();
            this.players.remove(playerServerDataPacket.getNetworkPlayer().getUniqueId());
            this.players.put(playerServerDataPacket.getNetworkPlayer().getUniqueId(), playerServerDataPacket.getNetworkPlayer());
            Bukkit.getScheduler().runTask(GoServer.getService(), () -> Bukkit.getPluginManager().callEvent(new PlayerLoginEvent(playerServerDataPacket.getNetworkPlayer().getUniqueId())));
        }
        if (var1.getType() == PacketType.UPDATE_PLAYER_SERVER_DATA) {
            PlayerServerDataPacket playerServerDataPacket = (PlayerServerDataPacket) var1.getValue();
            this.players.remove(playerServerDataPacket.getNetworkPlayer().getUniqueId());
            this.players.put(playerServerDataPacket.getNetworkPlayer().getUniqueId(), playerServerDataPacket.getNetworkPlayer());
            Bukkit.getScheduler().runTask(GoServer.getService(), () -> Bukkit.getPluginManager().callEvent(new PlayerUpdateDataEvent(playerServerDataPacket.getNetworkPlayer().getUniqueId(), playerServerDataPacket.getDataUpdateType())));
        }
    }

    @Override
    public void channelActive(Channel var1) {

    }
}
