package net.vicemice.hector.server.player.replay.type;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.types.player.replay.OpenReplayHistoryPacket;
import net.vicemice.hector.packets.types.player.replay.PacketReplayEntries;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.replay.DefaultReplayInventory;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.TimeManager;
import net.vicemice.hector.utils.players.utils.Pagination;
import org.bukkit.entity.Player;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import static org.bukkit.Material.BARRIER;
import static org.bukkit.Material.BOOK;

public class ReplayListInventory {

    public GUI create(IUser user, OpenReplayHistoryPacket openReplayHistoryPacket, int page, boolean saved) {
        Pagination<PacketReplayEntries.PacketReplayEntry> pagination = new Pagination<>((saved ? openReplayHistoryPacket.getSavedEntries() : openReplayHistoryPacket.getRecentEntries()), 27);
        GUI gui = new DefaultReplayInventory().create(user, (saved ? ReplayInventoryType.FAVORITES : ReplayInventoryType.NEW_REPLAYS), openReplayHistoryPacket);

        AtomicInteger slot = new AtomicInteger(9);
        AtomicInteger id = new AtomicInteger(1 + (27 * (page - 1)));
        pagination.printPage(page, replayEntry -> {
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            Date date = new Date(replayEntry.getTimestamp());

            gui.setItem(slot.getAndSet((slot.get() + 1)), ItemBuilder.create(BOOK).name(user.translate("replay-single", id.getAndSet((slot.get() + 1)))).lore(new ArrayList<>(Arrays.asList((user.translate("gui-profile-replay-description", dateFormat.format(date), timeFormat.format(date), replayEntry.getGameType() + " " + replayEntry.getMapType(), replayEntry.getMap(), GoAPI.getTimeManager().getTimeFromSeconds(GoServer.getLocaleManager(), user.getLocale(), replayEntry.getGameLength(), new ArrayList<>(Arrays.asList(TimeManager.TimeNames.HOURS, TimeManager.TimeNames.MINUTES, TimeManager.TimeNames.SECONDS)))).split("\n"))))).build(), e -> {
                new ReplayActionInventory().create(user, replayEntry, gui).open();
            });
        });

        if (pagination.getElementsFor(page).isEmpty()) {
            gui.setItem(22, ItemBuilder.create(BARRIER).name(user.translate((saved?"replays-other-no-saves":"replays-other-no-games"), openReplayHistoryPacket.getTargetRank().getRankColor() + openReplayHistoryPacket.getTargetName())).build());
        }

        gui.setItem(52, ItemBuilder.create(GUI.Items.getLeft()).name(user.translate("previous-page")).build(), e -> {
            if (pagination.getElementsFor((page - 1)).isEmpty()) return;
            this.create(user, openReplayHistoryPacket, (page - 1), saved).open();
        });
        gui.setItem(53, ItemBuilder.create(GUI.Items.getRight()).name(user.translate("next-page")).build(), e -> {
            if (pagination.getElementsFor((page + 1)).isEmpty()) return;
            this.create(user, openReplayHistoryPacket, (page + 1), saved).open();
        });

        return gui;
    }
}