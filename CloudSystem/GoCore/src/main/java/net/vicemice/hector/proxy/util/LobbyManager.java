package net.vicemice.hector.proxy.util;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.vicemice.hector.packets.types.lobby.type.FallbackDataInfo;
import net.vicemice.hector.packets.types.lobby.type.LobbyDataInfo;
import net.vicemice.hector.packets.types.lobby.LobbyDataPacket;
import net.vicemice.hector.packets.types.lobby.type.VerifyDataInfo;
import net.vicemice.hector.proxy.GoProxy;

import java.beans.ConstructorProperties;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class LobbyManager {

    private static final List<Lobby> lobbies = new CopyOnWriteArrayList<>();
    private static final List<Fallback> fallbacks = new CopyOnWriteArrayList<>();
    private static final List<Verify> verifies = new CopyOnWriteArrayList<>();
    private static int nextLobby = 0;

    public static void connect(ProxiedPlayer p) {
        if (GoProxy.getProxyService().getProxyConfig().isVerify()) {
            if (!verifies.isEmpty()) {
                Collections.shuffle(verifies);
                p.getPendingConnection().getListener().getServerPriority().clear();
                p.getPendingConnection().getListener().getServerPriority().add(verifies.get(0).getName());
            }
        }

        Collections.shuffle(lobbies);
        Collections.shuffle(fallbacks);
        if (!lobbies.isEmpty()) {
            p.getPendingConnection().getListener().getServerPriority().clear();
            p.getPendingConnection().getListener().getServerPriority().add(lobbies.get(0).getName());
        } else {
            if (!fallbacks.isEmpty()) {
                p.getPendingConnection().getListener().getServerPriority().clear();
                p.getPendingConnection().getListener().getServerPriority().add(fallbacks.get(0).getName());
            }
        }
    }

    public static String getLobby() {
        if (lobbies.size() == 0) {
            return null;
        }
        if (nextLobby > lobbies.size() - 1) {
            nextLobby = 0;
        }
        Lobby returnLobby = lobbies.get(nextLobby);
        ++nextLobby;
        return returnLobby.getName();
    }

    public static void receiveData(LobbyDataPacket lobbyDataPacket) {
        lobbies.clear();
        fallbacks.clear();
        verifies.clear();
        for (LobbyDataInfo data : lobbyDataPacket.getLobbyDataInfos()) {
            String name = data.getName();
            Lobby getLobby = LobbyManager.getLobbyByName(name);
            if (getLobby == null) {
                lobbies.add(new Lobby(name));
            }
        }

        for (FallbackDataInfo data : lobbyDataPacket.getFallbackDataInfos()) {
            String name = data.getName();
            Fallback getLobby = LobbyManager.getFallbackByName(name);
            if (getLobby == null) {
                fallbacks.add(new Fallback(name));
            }
        }

        for (VerifyDataInfo data : lobbyDataPacket.getVerifyDataInfos()) {
            String name = data.getName();
            Verify getLobby = LobbyManager.getVerifyByName(name);
            if (getLobby == null) {
                verifies.add(new Verify(name));
            }
        }
    }

    public static Lobby getLobbyByName(String name) {
        for (Lobby lobby : lobbies) {
            if (!lobby.getName().equalsIgnoreCase(name)) continue;
            return lobby;
        }
        return null;
    }

    public static Fallback getFallbackByName(String name) {
        for (Fallback fallback : fallbacks) {
            if (!fallback.getName().equalsIgnoreCase(name)) continue;
            return fallback;
        }
        return null;
    }

    public static Verify getVerifyByName(String name) {
        for (Verify verify : verifies) {
            if (!verify.getName().equalsIgnoreCase(name)) continue;
            return verify;
        }
        return null;
    }

    public static List<Lobby> getLobbies() {
        return lobbies;
    }

    public static class Lobby {
        private final String name;

        @ConstructorProperties(value = {"name"})
        public Lobby(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }
    }

    public static class Fallback {
        private final String name;

        @ConstructorProperties(value = {"name"})
        public Fallback(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }
    }

    public static class Verify {
        private final String name;

        @ConstructorProperties(value = {"name"})
        public Verify(String name) {
            this.name = name;
        }

        public String getName() {
            return this.name;
        }
    }
}