package net.vicemice.hector.server.player.permissions.listener;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class PlayerChatListener implements Listener {

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        IUser user = UserManager.getUser(event.getPlayer());
        event.setCancelled(true);

        if (!user.getNetworkPlayer().getSettingsPacket().getSetting("accept-tos", Boolean.class)) {
            user.sendMessage("you-must-accept-tos");
            TextComponent textComponent = new TextComponent(user.translate("click-to-accept-terms"));
            textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/agree"));
            event.getPlayer().spigot().sendMessage(textComponent);
            return;
        }

        Rank rank = user.getRank();
        String message = rank.getRankColor() + user.getName() + "§7: §f" + event.getMessage();

        if (user.isNicked()) message = Rank.MEMBER.getRankColor()+user.getNickName()+"§7: §f"+event.getMessage();

        CloudChatEvent cloudChatEvent = new CloudChatEvent(user, message, event.getMessage());
        Bukkit.getServer().getPluginManager().callEvent(cloudChatEvent);
        message = cloudChatEvent.getMessage();
        if (cloudChatEvent.isCancelled()) {
            return;
        }
        if (cloudChatEvent.getReceiver() != null) {
            for (Player onlinePlayer : cloudChatEvent.getReceiver()) {
                onlinePlayer.sendMessage(message);
            }
        } else {
            for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
                onlinePlayer.sendMessage(message);
            }
        }
    }
}