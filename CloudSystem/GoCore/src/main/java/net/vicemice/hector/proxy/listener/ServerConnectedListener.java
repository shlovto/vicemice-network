package net.vicemice.hector.proxy.listener;

import io.netty.channel.Channel;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.PlayerSwitchServerPacket;
import net.vicemice.hector.packets.types.proxy.ProxyServerPlayerCountPacket;
import net.vicemice.hector.proxy.GoProxy;
import net.vicemice.hector.proxy.util.labymod.LabyModAPI;
import net.vicemice.hector.proxy.util.mongodb.PlayerAPI;

import java.util.UUID;

public class ServerConnectedListener implements Listener {

    @EventHandler
    public void onServerConnected(ServerConnectedEvent event) {
        ProxiedPlayer proxiedPlayer = event.getPlayer();
        LabyModAPI.sendMiddleClickActions(event.getPlayer());
        LabyModAPI.updateGame(proxiedPlayer, "true", event.getServer().getInfo().getName());
        proxiedPlayer.setTabHeader(TextComponent.fromLegacyText("\n  §b§lVice§f§lMice §7" + toTabGame(event.getPlayer().getUniqueId(), event.getServer().getInfo().getName(), false) + " \n"), TextComponent.fromLegacyText("\n"+GoAPI.getUserAPI().translate(proxiedPlayer.getUniqueId(), "tab-footer")+"\n"));
        ProxyServer.getInstance().getScheduler().runAsync(GoProxy.getService(), () -> {
            Channel channel = GoProxy.getProxyService().getNettyClient().getChannel();
            if (GoProxy.getProxyService().getProxyManager().isConnected()) {
                int playerSize = event.getServer().getInfo().getPlayers().size();
                channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_SWITCH_SERVER, new PlayerSwitchServerPacket(proxiedPlayer.getName(), event.getServer().getInfo().getName())));
                channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_SERVER_PLAYER_COUNT_PACKET, new ProxyServerPlayerCountPacket(event.getServer().getInfo().getName(), GoProxy.getProxyService().getName(), ++playerSize)));

                /*if (event.getServer().getInfo().getName().contains("-")) {
                    String game = "§a§lLobby";

                    if (event.getServer().getInfo().getName().split("-")[0].equalsIgnoreCase("QSG")) {
                        game = "§b§lQSG";
                    } else if (event.getServer().getInfo().getName().split("-")[0].equalsIgnoreCase("BedWars")) {
                        game = "§c§lBedWars";
                    }

                    LabyModAPI.sendCurrentPlayingGamemode(proxiedPlayer, true, game);
                } else {
                    LabyModAPI.sendCurrentPlayingGamemode(proxiedPlayer, true, event.getServer().getInfo().getName());
                }*/
            }
        });
    }

    /*public static String toTabGame(UUID uuid, String server) {
        PlayerAPI playerAPI = new PlayerAPI(uuid);
        String game = "§cLoading Information...";

        if(playerAPI.get("language").equalsIgnoreCase("german")) {
            game = "§cDu spielst derzeit auf einem uns Unbekannten Server";
        } else {
            game = "§cYou are currently playing on an unknown server";
        }

        if (server.contains("ClickIt")) {
            if(playerAPI.get("language").equalsIgnoreCase("german")) {
                game = "§7Du spielst derzeit §bClickIt";
            } else {
                game = "§7You are playing §bClickIt";
            }
        } else if (server.contains("Murderer")) {
            if(playerAPI.get("language").equalsIgnoreCase("german")) {
                game = "§7Du spielst derzeit §cMurderer";
            } else {
                game = "§7You are playing §cMurderer";
            }
        } else if (server.contains("JumpHero")) {
            if(playerAPI.get("language").equalsIgnoreCase("german")) {
                game = "§7Du spielst derzeit §6Mine§fwatch";
            } else {
                game = "§7You are playing §6Mine§fwatch";
            }
        } else if (server.contains("Replay")) {
            if(playerAPI.get("language").equalsIgnoreCase("german")) {
                game = "§7Du spielst derzeit auf einem §6Replay §7Server";
            } else {
                game = "§7You are playing on a §6Replay §7Server";
            }
        } else if (server.contains("Community")) {
            if(playerAPI.get("language").equalsIgnoreCase("german")) {
                game = "§7Du spielst derzeit auf der §6Community";
            } else {
                game = "§7You are playing on the §6Community";
            }
        } else if (server.contains("BauServer")) {
            if(playerAPI.get("language").equalsIgnoreCase("german")) {
                game = "§7Du spielst derzeit auf einem §eBuild Team §7Server";
            } else {
                game = "§7Du spielst derzeit on a §eBuild Team §7Server";
            }
        } else if (server.contains("Lobby")) {
            if(playerAPI.get("language").equalsIgnoreCase("german")) {
                game = "§7Du spielst derzeit auf einem §aLobby §7Server";
            } else {
                game = "§7You are playing on a §aLobby §7Server";
            }
        } else if (server.contains("back")) {
            if(playerAPI.get("language").equalsIgnoreCase("german")) {
                game = "§7Du spielst derzeit auf einem §cFallback §7Server";
            } else {
                game = "§7You are playing on a §cFallback §7Server";
            }
        } else if (server.contains("Verify")) {
            if(playerAPI.get("language").equalsIgnoreCase("german")) {
                game = "§7Du spielst derzeit auf einem §aVerify §7Server";
            } else {
                game = "§7You are playing on a §aVerify §7Server";
            }
        } else if (server.startsWith("CW")) {
            if(playerAPI.get("language").equalsIgnoreCase("german")) {
                game = "§7Du spielst derzeit auf einem §e§lCW §7Server";
            } else {
                game = "§7You are playing on a §e§lCW §7Server";
            }
        } else if (server.contains("Dev")) {
            if(playerAPI.get("language").equalsIgnoreCase("german")) {
                game = "§7Du spielst derzeit auf einem §bDeveloper §7Server";
            } else {
                game = "§7You are playing on a §bDeveloper §7Server";
            }
        } else if (server.startsWith("P")) {
            if(playerAPI.get("language").equalsIgnoreCase("german")) {
                game = "§7Du spielst derzeit auf einem §cPrivaten §7Server";
            } else {
                game = "§7You are playing on a §cPrivate §7Server";
            }
        } else {
            if(playerAPI.get("language").equalsIgnoreCase("german")) {
                game = "§cDu spielst derzeit auf einem uns Unbekannten Server";
            } else {
                game = "§cYou are currently playing on an unknown server";
            }
        }

        return game;
    }*/

    public static String toTabGame(UUID uuid, String server, boolean footer) {
        PlayerAPI playerAPI = new PlayerAPI(uuid);
        String game = "§cLoading Information...";

        if (footer) {
            if(playerAPI.get("language").equalsIgnoreCase("german")) {
                return "§7Du hast einen §cHacker §7gefunden? Melde ihn mit §c/report";
            } else {
                return "§7You found a §cHacker§7? Report him with §c/report";
            }
        }

        if(playerAPI.get("language").equalsIgnoreCase("german")) {
            game = "§cUNBEKANNT";
        } else {
            game = "§cUNKNOW";
        }

        if (server.contains("PBedWars")) {
            game = "§c§lPrivate §c§lBedWars";
        } else if (server.contains("PBuilding")) {
            game = "§c§lPrivate §b§lBuilding";
        } else if (server.contains("QSG")) {
            game = "§a§lQSG";
        } else if (server.contains("BedWars")) {
            game = "§c§lBedWars";
        } else if (server.contains("Replay")) {
            game = "§6§lReplay";
        } else if (server.startsWith("Team") || server.contains("BauServer")) {
            game = "§cTeam";
        } else if (server.contains("Lobby")) {
            game = "§e§lLobby";
        } else if (server.contains("back")) {
            game = "§c§lFallback";
        } else if (server.contains("Verify")) {
            game = "§a§lVerify";
        } else if (server.contains("Dev")) {
            game = "§b§lDevelopment";
        } else if (server.startsWith("P")) {
            game = "§c§lPrivate";
        } else {
            game = "§cUnbekannt";
        }

        return game;
    }
}