package net.vicemice.hector.proxy.util.thread;

public interface ThreadRunner {
    public void start();

    public void stop();

    public Thread getThread();

    public Runnable getOriginalRunable();
}