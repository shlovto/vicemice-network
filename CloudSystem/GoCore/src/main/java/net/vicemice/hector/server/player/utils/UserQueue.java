package net.vicemice.hector.server.player.utils;

import net.vicemice.hector.utils.players.IUser;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/*
 * Class created at 21:18 - 02.05.2020
 * Copyright (C) elrobtossohn
 */
public class UserQueue extends Thread {

    private final Queue<QueueEntry> queue = new LinkedBlockingQueue<QueueEntry>();

    public UserQueue() {
        super("User action queue");
        this.setDaemon(true);
    }

    @Override
    public void run() {
        while (!this.isInterrupted()) {
            this.blockThread();
            while (!this.queue.isEmpty()) {
                QueueEntry queueEntry = this.queue.poll();
                if (queueEntry.getAction() != null) {
                    queueEntry.getAction().done();
                }
            }
        }
    }

    public synchronized void addToQueue(IUser.Action action, boolean insert) {
        if (action != null) {
            for (QueueEntry queueEntry : this.queue) {
                if (!queueEntry.getAction().equals(action)) continue;
                return;
            }
        }
        this.queue.add(new QueueEntry(action, insert));
        this.notify();
    }

    private synchronized void blockThread() {
        try {
            while (this.queue.isEmpty()) {
                this.wait();
            }
        }
        catch (Exception var1_1) {
            // empty catch block
        }
    }

    public static class QueueEntry {
        private final IUser.Action action;
        private final boolean insert;

        public QueueEntry(IUser.Action action, boolean insert) {
            this.action = action;
            this.insert = insert;
        }

        public IUser.Action getAction() {
            return action;
        }

        public boolean isInsert() {
            return this.insert;
        }
    }
}
