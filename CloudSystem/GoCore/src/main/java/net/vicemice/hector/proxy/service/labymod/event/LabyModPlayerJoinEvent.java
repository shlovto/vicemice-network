package net.vicemice.hector.proxy.service.labymod.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Event;
import net.vicemice.hector.api.labymod.Addon;

import java.util.List;

/*
 * Class created at 01:34 - 23.04.2020
 * Copyright (C) elrobtossohn
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class LabyModPlayerJoinEvent extends Event {

    private ProxiedPlayer player;
    private String modVersion;
    private boolean chunkCachingEnabled;
    private int chunkCachingVersion;
    private List<Addon> addons;

}