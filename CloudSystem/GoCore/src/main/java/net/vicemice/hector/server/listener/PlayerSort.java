package net.vicemice.hector.server.listener;

import org.bukkit.entity.Player;

public class PlayerSort implements Comparable<PlayerSort> {
    private Player player;
    private int level;

    public PlayerSort(Player player, int level) {
        this.player = player;
        this.level = level;
    }

    @Override
    public int compareTo(PlayerSort o) {
        return this.level < o.level ? -1 : (this.level > o.level ? 1 : 0);
    }

    public Player getPlayer() {
        return this.player;
    }

    public int getLevel() {
        return this.level;
    }
}