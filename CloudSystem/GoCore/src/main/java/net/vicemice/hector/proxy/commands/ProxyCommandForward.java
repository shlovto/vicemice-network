package net.vicemice.hector.proxy.commands;

import lombok.NonNull;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.protocol.packet.Chat;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.proxy.GoProxy;
import net.vicemice.hector.utils.CommandType;

import java.util.*;

public class ProxyCommandForward extends TabableCommand {

    @NonNull
    private CommandType type;
    private List<String> excludedServerPattern = new ArrayList<String>();
    private List<String> includedServerPattern = new ArrayList<String>();

    public ProxyCommandForward exclude(String pattern) {
        this.excludedServerPattern.add(pattern);
        return this;
    }

    public ProxyCommandForward include(String pattern) {
        this.includedServerPattern.add(pattern);
        return this;
    }

    public /* varargs */ ProxyCommandForward(CommandType type, String ... commands) {
        super(type.getAccessLevel(), commands);
        this.type = type;
    }

    private String join(String del, String[] args) {
        Iterator<String> it = Arrays.asList(args).iterator();
        String out = "";
        while (it.hasNext()) {
            out = String.valueOf(out) + it.next();
            if (!it.hasNext()) continue;
            out = String.valueOf(out) + del;
        }
        return out;
    }

    public void execute(CommandSender commandSender, String[] strings) {
        String serverName = ((ProxiedPlayer)commandSender).getServer().getInfo().getName();
        if (!this.includedServerPattern.isEmpty()) {
            for (String pattern : this.includedServerPattern) {
                if (serverName.matches(pattern)) continue;
                commandSender.sendMessage(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( ((ProxiedPlayer)commandSender).getUniqueId(), "command-exclude")));
                return;
            }
        }
        if (!this.excludedServerPattern.isEmpty()) {
            for (String pattern : this.excludedServerPattern) {
                if (!serverName.matches(pattern)) continue;
                System.out.println("Forward command '" + this.getCommands()[0] + "' on server " + pattern + " (" + serverName + ") for player " + commandSender.getName());
                ProxiedPlayer player = (ProxiedPlayer)commandSender;
                player.getServer().unsafe().sendPacket(new Chat("/" + this.getCommands()[0] + " " + this.join(" ", strings)));
                return;
            }
        }
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer)commandSender;
        GoProxy.getProxyService().getCommandManager().runCommand(this.type, proxiedPlayer.getName(), strings, commandSender);
    }
}