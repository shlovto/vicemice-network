package net.vicemice.hector.proxy.listener;

import io.netty.channel.ChannelFuture;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.LogoutPacket;
import net.vicemice.hector.proxy.GoProxy;

import java.util.UUID;

public class PlayerDisconnectListener implements Listener {

    @EventHandler
    public void onPlayerDisconnect(PlayerDisconnectEvent event) {
        GoProxy.getProxyService().getPlayerMessages().remove(event.getPlayer());
        GoProxy.getProxyService().getAllowedChat().remove(event.getPlayer().getName());
        GoProxy.getProxyService().getTimeOutManager().logOut(event.getPlayer().getName());
        String name = event.getPlayer().getName();
        UUID uuid = event.getPlayer().getUniqueId();
        if (GoProxy.getProxyService().getProxyManager().isConnected()) {
            ProxyServer.getInstance().getScheduler().runAsync(GoProxy.getService(), () -> {
                ChannelFuture channelFuture = GoProxy.getProxyService().getNettyClient().getChannel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.LOGOUT_PACKET, new LogoutPacket(uuid)));
            });
        }
    }
}