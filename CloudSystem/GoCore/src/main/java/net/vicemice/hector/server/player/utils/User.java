package net.vicemice.hector.server.player.utils;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.ConnectPacket;
import net.vicemice.hector.packets.types.player.coins.EditCoinPacket;
import net.vicemice.hector.packets.types.player.coins.GetCoinsPacket;
import net.vicemice.hector.packets.types.player.level.EditXPPacket;
import net.vicemice.hector.packets.types.player.replay.PacketEditReplay;
import net.vicemice.hector.packets.types.player.replay.PacketReplayEntries;
import net.vicemice.hector.packets.types.player.replay.PacketReplayHistoryAdd;
import net.vicemice.hector.packets.types.player.replay.WatchReplayPacket;
import net.vicemice.hector.packets.types.player.stats.StatisticEditPacket;
import net.vicemice.hector.packets.types.player.user.UserUpdatePacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.nick.NickManager;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.server.utils.CoinsManager;
import net.vicemice.hector.types.Callback;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.NetworkPlayer;
import net.vicemice.hector.utils.players.api.actionbat.ActionBarAPI;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.players.api.hologram.Hologram;
import net.vicemice.hector.utils.players.api.hologram.HologramAPI;
import net.vicemice.hector.utils.players.clan.Clan;
import net.vicemice.hector.utils.players.party.Party;
import org.apache.commons.lang.ObjectUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/*
 * Class created at 20:53 - 02.05.2020
 * Copyright (C) elrobtossohn
 */
public class User implements IUser {
    
    private String name;
    private UUID uniqueId;
    private Rank rank;
    private Locale locale;
    private boolean online;
    private long coins;
    private long xp;
    private Party party;
    private Clan clan;
    private final List<Action> actions = new ArrayList<>();
    
    public User(String name, UUID uniqueId, Rank rank, Locale locale, boolean online, long coins, long xp, Party party, Clan clan) {
        this.name = name;
        this.uniqueId = uniqueId;
        this.rank = rank;
        this.locale = locale;
        this.online = online;
        this.coins = coins;
        this.xp = xp;
        this.party = party;
        this.clan = clan;
    }
    
    @Override
    public String getName() {
        return name;
    }

    @Override
    public UUID getUniqueId() {
        return uniqueId;
    }

    @Override
    public Player getBukkitPlayer() {
        return Bukkit.getPlayer(this.getUniqueId());
    }

    @Override
    public void sendActionBarMessage(String message) {
        ActionBarAPI.sendActionBar(this.getBukkitPlayer(), message);
    }

    @Override
    public void sendActionBarMessage(String key, Object... objects) {
        ActionBarAPI.sendActionBar(this.getBukkitPlayer(), this.translate(key, objects));
    }

    @Override
    public void sendMessage(String key) {
        this.sendMessage(key, new Object[0]);
    }

    @Override
    public void sendMessage(String key, Object... objects) {
        Bukkit.getPlayer(uniqueId).sendMessage(translate(key, objects));
    }

    @Override
    public void sendRawMessage(String message) {
        Bukkit.getPlayer(uniqueId).sendMessage(message);
    }

    @Override
    public String translate(String key) {
        return GoServer.getLocaleManager().getMessage(this.getLocale(), key);
    }

    @Override
    public String translate(String key, Object... objects) {
        return MessageFormat.format(translate(key), objects);
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public long getCoins() { 
        return coins;
    }

    @Override
    public void getCoins(Callback<Long> callback) {
        String callbackID = UUID.randomUUID().toString();
        CoinsManager.getCoinsCallbacks().put(callbackID, coinsValue -> {
            coins = (long)coinsValue;
            callback.done((long)coinsValue);
        });
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.GET_PLAYER_COINS, new GetCoinsPacket(uniqueId, callbackID)), API.PacketReceiver.SLAVE);
    }

    @Override
    public void increaseCoins(long coins) {
        this.coins+=coins;
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.EDIT_COIN_PACKET, new EditCoinPacket(uniqueId, coins, false)), API.PacketReceiver.SLAVE);
    }

    @Override
    public void decreaseCoins(long coins) {
        this.coins-=coins;
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.EDIT_COIN_PACKET, new EditCoinPacket(uniqueId, coins, true)), API.PacketReceiver.SLAVE);
    }

    @Override
    public String getNickName() {
        if (NickManager.getPlayerData().containsKey(uniqueId)) {
            return NickManager.getPlayerData().get(uniqueId).getNickName();
        }
        return Bukkit.getPlayer(uniqueId).getName();
    }

    @Override
    public boolean isNicked() {
        return NickManager.getPlayerData().containsKey(uniqueId);
    }

    @Override
    public void increaseStatistic(String key, long value) {
        long global = (getNetworkPlayer().getStatistics().get(StatisticPeriod.GLOBAL).containsKey(key) ? getNetworkPlayer().getStatistics().get(StatisticPeriod.GLOBAL).get(key) : 0);
        long monthly = (getNetworkPlayer().getStatistics().get(StatisticPeriod.MONTH).containsKey(key) ? getNetworkPlayer().getStatistics().get(StatisticPeriod.MONTH).get(key) : 0);
        long daily = (getNetworkPlayer().getStatistics().get(StatisticPeriod.DAY).containsKey(key) ? getNetworkPlayer().getStatistics().get(StatisticPeriod.DAY).get(key) : 0);
        if (value < 0) {
            value = 0;
        }
        getNetworkPlayer().getStatistics().get(StatisticPeriod.GLOBAL).remove(key);
        getNetworkPlayer().getStatistics().get(StatisticPeriod.MONTH).remove(key);
        getNetworkPlayer().getStatistics().get(StatisticPeriod.DAY).remove(key);
        getNetworkPlayer().getStatistics().get(StatisticPeriod.GLOBAL).put(key, (global+value));
        getNetworkPlayer().getStatistics().get(StatisticPeriod.MONTH).put(key, (monthly+value));
        getNetworkPlayer().getStatistics().get(StatisticPeriod.DAY).put(key, (daily+value));
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.STATS_EDIT, new StatisticEditPacket(uniqueId, StatisticEditPacket.Action.ADD, key, value)), API.PacketReceiver.SLAVE);
    }

    @Override
    public void decreaseStatistic(String key, long value) {
        long global = (getNetworkPlayer().getStatistics().get(StatisticPeriod.GLOBAL).containsKey(key) ? getNetworkPlayer().getStatistics().get(StatisticPeriod.GLOBAL).get(key) : 0);
        long monthly = (getNetworkPlayer().getStatistics().get(StatisticPeriod.MONTH).containsKey(key) ? getNetworkPlayer().getStatistics().get(StatisticPeriod.MONTH).get(key) : 0);
        long daily = (getNetworkPlayer().getStatistics().get(StatisticPeriod.DAY).containsKey(key) ? getNetworkPlayer().getStatistics().get(StatisticPeriod.DAY).get(key) : 0);
        if (value < 0) {
            global = 0;
            monthly = 0;
            daily = 0;
            value = 0;
        }
        getNetworkPlayer().getStatistics().get(StatisticPeriod.GLOBAL).remove(key);
        getNetworkPlayer().getStatistics().get(StatisticPeriod.MONTH).remove(key);
        getNetworkPlayer().getStatistics().get(StatisticPeriod.DAY).remove(key);
        getNetworkPlayer().getStatistics().get(StatisticPeriod.GLOBAL).put(key, (global-value));
        getNetworkPlayer().getStatistics().get(StatisticPeriod.MONTH).put(key, (monthly-value));
        getNetworkPlayer().getStatistics().get(StatisticPeriod.DAY).put(key, (daily-value));
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.STATS_EDIT, new StatisticEditPacket(uniqueId, StatisticEditPacket.Action.REMOVE, key, value)), API.PacketReceiver.SLAVE);
    }

    @Override
    public void setStatistic(String key, long value) {
        if (value < 0) value = 0;
        getNetworkPlayer().getStatistics().get(StatisticPeriod.GLOBAL).remove(key);
        getNetworkPlayer().getStatistics().get(StatisticPeriod.MONTH).remove(key);
        getNetworkPlayer().getStatistics().get(StatisticPeriod.DAY).remove(key);
        getNetworkPlayer().getStatistics().get(StatisticPeriod.GLOBAL).put(key, value);
        getNetworkPlayer().getStatistics().get(StatisticPeriod.MONTH).put(key, value);
        getNetworkPlayer().getStatistics().get(StatisticPeriod.DAY).put(key, value);
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.STATS_EDIT, new StatisticEditPacket(uniqueId, StatisticEditPacket.Action.SET, key, value)), API.PacketReceiver.SLAVE);
    }

    @Override
    public Long getStatistic(String key, StatisticPeriod period) {
        long value = 0;
        if (getNetworkPlayer().getStatistics().containsKey(period)) {
            if (getNetworkPlayer().getStatistics().get(period).containsKey(key)) {
                value = getNetworkPlayer().getStatistics().get(period).get(key);
            }
        }
        return value;
    }

    @Override
    public Long getRankPosition(String key, StatisticPeriod period) {
        long value = -1;
        if (getNetworkPlayer().getRanking().containsKey(period)) {
            if (getNetworkPlayer().getRanking().get(period).containsKey(key)) {
                value = getNetworkPlayer().getRanking().get(period).get(key);
            }
        }
        return value;
    }

    @Override
    public void connectServer(String server) {
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.SERVER_CONNECT, new ConnectPacket(this.getName(), server)), API.PacketReceiver.SLAVE);
    }

    @Override
    public Rank getRank() {
        return rank;
    }

    @Override
    public long getXP() {
        return xp;
    }

    @Override
    public void addXP(long xp) {
        this.xp+=xp;
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.EDIT_XP_PACKET, new EditXPPacket(uniqueId, xp, false)), API.PacketReceiver.SLAVE);
    }

    @Override
    public void removeXP(long xp) {
        this.xp-=xp;
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.EDIT_XP_PACKET, new EditXPPacket(uniqueId, xp, true)), API.PacketReceiver.SLAVE);
    }

    @Override
    public Party getParty() {
        return party;
    }

    @Override
    public Clan getClan() {
        return clan;
    }

    @Override
    public void editReplayFavorites(String gameId, PacketEditReplay.EditType editType) {
        GoServer.getService().getGoAPI().getNettyClient().getChannel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.REPLAY_FAVORITES_EDIT, new PacketEditReplay(uniqueId, gameId, editType)));
    }

    @Override
    public void watchGame(String gameId) {
        GoServer.getService().getGoAPI().getNettyClient().getChannel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.WATCH_REPLAY, new WatchReplayPacket(uniqueId, gameId)));
    }

    @Override
    public void addRecordedGame(String gameId, UUID replayUUID, int replayStartSeconds, long timestamp, long gameLength, ArrayList<String> players, String gameType, String mapType, String map, boolean save) {
        GoServer.getService().getGoAPI().getNettyClient().getChannel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.REPLAY_HISTORY_ADD, new PacketReplayHistoryAdd(uniqueId, new PacketReplayEntries.PacketReplayEntry(gameId, replayUUID, replayStartSeconds, timestamp, gameLength, players, gameType, mapType, map, save))));
    }

    @Override
    public boolean isOnline() {
        return online;
    }

    @Override
    public void setOnline(boolean online) {
        this.online = online;
    }

    @Override
    public void update(UserUpdatePacket userUpdatePacket) {
        this.name = userUpdatePacket.getName();
        this.uniqueId = userUpdatePacket.getUniqueId();
        this.rank = userUpdatePacket.getRank();
        this.locale = userUpdatePacket.getLocale();
        this.coins = userUpdatePacket.getCoins();
        this.xp = userUpdatePacket.getXp();
        this.party = userUpdatePacket.getParty();
        this.clan = userUpdatePacket.getClan();
    }

    @Override
    public NetworkPlayer getNetworkPlayer() {
        return GoServer.getService().getPlayerManager().getPlayer(uniqueId);
    }

    @Override
    public void sendSettings() {
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.UPDATE_PLAYER_SETTINGS, this.getNetworkPlayer().getSettingsPacket()), API.PacketReceiver.SLAVE);
    }

    @Override
    public Hologram createHologram(Location location, List<String> text) {
        Hologram hologram = HologramAPI.getNewHologram();
        if (hologram == null) throw new NullPointerException("Unsupported server version");
        hologram.setupPlayerHologram(Bukkit.getPlayer(this.getUniqueId()), location, new ArrayList<>(text));
        return hologram;
    }

    @Override
    public GUI createGUI(String title, int rows) {
        return new GUI(this, title, rows);
    }

    @Override
    public void teleport(Location location) {
        this.getBukkitPlayer().teleport(location);
    }

    @Override
    public void teleport(Entity entity) {
        this.getBukkitPlayer().teleport(entity);
    }

    @Override
    public void playSound(Location location, Sound sound, float volume, float pitch) {
        Bukkit.getPlayer(this.getUniqueId()).playSound(location, sound, volume, pitch);
    }

    @Override
    public void playSound(Location location, Sound sound) {
        this.playSound(location, sound, 10L, 10L);
    }

    @Override
    public void playSound(Sound sound, float volume, float pitch) {
        this.playSound(getLocation(), sound, volume, pitch);
    }

    @Override
    public void playSound(Sound sound, float volume) {
        this.playSound(getLocation(), sound, volume, 10L);
    }

    @Override
    public void playSound(Sound sound) {
        this.playSound(getLocation(), sound, 10L, 10L);
    }

    @Override
    public Location getLocation() {
        return Bukkit.getPlayer(this.getUniqueId()).getLocation();
    }

    @Override
    public Location getEyeLocation() {
        return Bukkit.getPlayer(this.getUniqueId()).getEyeLocation();
    }

    @Override
    public ItemStack getUIColor() {
        ItemBuilder itemBuilder = ItemBuilder.create(Material.STAINED_GLASS_PANE).name("§0 ");
        int ui = this.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class);
        if (ui == 0)
            return itemBuilder.durability(0).build();
        else if (ui == 1)
            return itemBuilder.durability(1).build();
        else if (ui == 2)
            return itemBuilder.durability(2).build();
        else if (ui == 3)
            return itemBuilder.durability(3).build();
        else if (ui == 4)
            return itemBuilder.durability(4).build();
        else if (ui == 5)
            return itemBuilder.durability(5).build();
        else if (ui == 6)
            return itemBuilder.durability(6).build();
        else if (ui == 7)
            return itemBuilder.durability(7).build();
        else if (ui == 8)
            return itemBuilder.durability(8).build();
        else if (ui == 9)
            return itemBuilder.durability(9).build();
        else if (ui == 10)
            return itemBuilder.durability(10).build();
        else if (ui == 11)
            return itemBuilder.durability(11).build();
        else if (ui == 12)
            return itemBuilder.durability(12).build();
        else if (ui == 13)
            return itemBuilder.durability(13).build();
        else if (ui == 14)
            return itemBuilder.durability(14).build();
        else if (ui == 15)
            return itemBuilder.durability(15).build();
        else
            return itemBuilder.durability(0).build();
    }

    @Override
    public ItemStack getUIColor2() {
        return new ItemBuilder(getUIColor()).material(Material.STAINED_GLASS).build();
    }

    @Override
    public void performAction(IUser.Action action) {
        actions.add(action);
    }
}