package net.vicemice.hector.proxy.listener;

import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.vicemice.hector.events.ProxyUpdateLocaleEvent;
import net.vicemice.hector.proxy.GoProxy;
import net.vicemice.hector.utils.locale.LocaleManager;

import java.util.ArrayList;

public class ProxyUpdateLocaleListener implements Listener {

    @EventHandler
    public void onUpdate(ProxyUpdateLocaleEvent event) {
        ArrayList<String> s = new ArrayList<>();
        s.add("cloud/locales/core");
        s.add("cloud/locales/proxy");
        GoProxy.setLocaleManager(LocaleManager.newManager(s));
    }
}