package net.vicemice.hector.server.listener;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.PlayerRankData;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import java.util.ArrayList;
import java.util.Collections;

public class LoginListener implements Listener {

    @EventHandler
    public void onLogin(PlayerLoginEvent event) {
        if (!GoServer.getService().isKickPlayers()) {
            return;
        }
        Player player = event.getPlayer();
        PlayerRankData playerRankData = GoAPI.getUserAPI().getRankData(player.getUniqueId());
        if (playerRankData.getAccess_level() <= 10 && Bukkit.getOnlinePlayers().size() >= GoServer.getService().getGoConfig().getMAX_PLAYERS()) {
            event.disallow(PlayerLoginEvent.Result.KICK_OTHER, GoAPI.getUserAPI().translate(player.getUniqueId(), "server-full"));
            return;
        }
        if (Bukkit.getOnlinePlayers().size() >= GoServer.getService().getGoConfig().getMAX_PLAYERS()) {
            Player toKick = null;
            ArrayList<PlayerSort> playerSorts = new ArrayList<PlayerSort>();
            for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
                playerSorts.add(new PlayerSort(onlinePlayer, GoAPI.getUserAPI().getRankData(onlinePlayer.getUniqueId()).getAccess_level()));
            }
            Collections.sort(playerSorts);
            for (PlayerSort playerSort : playerSorts) {
                if (playerSort.getLevel() >= playerRankData.getAccess_level()) continue;
                toKick = playerSort.getPlayer();
                break;
            }
            if (toKick != null) {
                toKick.sendMessage(GoAPI.getUserAPI().translate(player.getUniqueId(), "user-higher-rank-kick"));
                toKick.kickPlayer("lobby");
            } else {
                event.disallow(PlayerLoginEvent.Result.KICK_OTHER, GoAPI.getUserAPI().translate(player.getUniqueId(), "no-free-place"));
            }
        }

        player.spigot().setCollidesWithEntities(false);
    }
}