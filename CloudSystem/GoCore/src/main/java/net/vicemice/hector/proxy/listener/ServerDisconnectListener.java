package net.vicemice.hector.proxy.listener;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.ServerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.proxy.ProxyServerPlayerCountPacket;
import net.vicemice.hector.proxy.GoProxy;

public class ServerDisconnectListener implements Listener {
    @EventHandler
    public void onServerDisconnect(ServerDisconnectEvent event) {
        ProxyServer.getInstance().getScheduler().runAsync(GoProxy.getService(), () -> {
            if (GoProxy.getProxyService().getProxyManager().isConnected()) {
                int playerSize = event.getTarget().getPlayers().size();
                GoProxy.getProxyService().getNettyClient().getChannel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_SERVER_PLAYER_COUNT_PACKET, new ProxyServerPlayerCountPacket(event.getTarget().getName(), GoProxy.getProxyService().getName(), playerSize)));
            }
        });
    }
}