package net.vicemice.hector.server.player.nick.listener;

import net.vicemice.hector.server.player.nick.NickManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class NickTabCompleteListener implements Listener {
    public static List<String> filterCompletions(String message, List<String> completions) {
        ListIterator<String> iterator = completions.listIterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            Player target = Bukkit.getPlayerExact(next);
            if (target == null || !NickManager.getPlayerData().containsKey(target.getUniqueId())) continue;
            iterator.remove();
        }
        List<String> nickCompletions = NickManager.getUsedNicknames();
        nickCompletions = NickTabCompleteListener.getPossibleCompletionsForGivenArgs(new String[]{message}, nickCompletions.toArray(new String[nickCompletions.size()]));
        for (String s : nickCompletions) {
            completions.add(ChatColor.stripColor(s));
        }
        return completions;
    }

    public static List<String> getPossibleCompletionsForGivenArgs(String[] args, String[] possibilitiesOfCompletion) {
        String argumentToFindCompletionFor = args[args.length - 1];
        ArrayList<String> listOfPossibleCompletions = new ArrayList<String>();
        for (int i = 0; i < possibilitiesOfCompletion.length; ++i) {
            try {
                if (possibilitiesOfCompletion[i] == null || !possibilitiesOfCompletion[i].regionMatches(true, 0, argumentToFindCompletionFor, 0, argumentToFindCompletionFor.length()))
                    continue;
                listOfPossibleCompletions.add(possibilitiesOfCompletion[i]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Collections.sort(listOfPossibleCompletions);
        return listOfPossibleCompletions;
    }

    @EventHandler
    public void onTabComplete(PlayerChatTabCompleteEvent event) {
        NickTabCompleteListener.filterCompletions(event.getChatMessage(), (List<String>) event.getTabCompletions());
    }
}