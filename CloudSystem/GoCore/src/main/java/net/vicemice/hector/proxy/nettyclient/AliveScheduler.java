package net.vicemice.hector.proxy.nettyclient;

import io.netty.channel.Channel;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.api.util.players.ProxyPlayerInfo;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.proxy.ProxyPacket;
import net.vicemice.hector.packets.types.proxy.ProxyPlayerPacket;
import net.vicemice.hector.proxy.GoProxy;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class AliveScheduler {
    private int toSendAllPlayers = 20;
    private int toSendServerCount = 5;

    public AliveScheduler() {
        ProxyServer.getInstance().getScheduler().schedule(GoProxy.getService(), this::sendAlivePacket, 1, 1, TimeUnit.SECONDS);
    }

    public void sendAlivePacket() {
        ProxyServer.getInstance().getScheduler().runAsync(GoProxy.getService(), () -> {
            Channel channel;
            if (GoProxy.getProxyService().getNettyClient().getChannel() != null && (channel = GoProxy.getProxyService().getNettyClient().getChannel()).isActive() && channel.isOpen()) {
                if (this.toSendServerCount == 0) {
                    this.toSendServerCount = 5;
                    for (String name : ProxyServer.getInstance().getServers().keySet()) {
                        ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo(name);
                        channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_PLAYER_PACKET, new ProxyPlayerPacket(serverInfo.getName(), GoProxy.getProxyService().getProxyConfig().getProxyid(), serverInfo.getPlayers().size())));
                    }
                } else {
                    --this.toSendServerCount;
                }
                if (this.toSendAllPlayers == 0) {
                    this.toSendAllPlayers = 10;
                    ArrayList<ProxyPlayerInfo> players = new ArrayList<>();
                    for (ProxiedPlayer proxiedPlayer : ProxyServer.getInstance().getPlayers()) {
                        players.add(new ProxyPlayerInfo(proxiedPlayer.getUniqueId(), proxiedPlayer.getServer().getInfo().getName()));
                    }
                    channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_INFO_PACKET, new ProxyPacket(GoProxy.getProxyService().getProxyConfig().getProxyid(), ProxyServer.getInstance().getOnlineCount(), players, GoProxy.getProxyService().getProxyConfig().getHost(), Integer.parseInt(GoProxy.getProxyService().getProxyConfig().getPort()))));
                } else {
                    channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_INFO_PACKET, new ProxyPacket(GoProxy.getProxyService().getProxyConfig().getProxyid(), ProxyServer.getInstance().getOnlineCount(), null, GoProxy.getProxyService().getProxyConfig().getHost(), Integer.parseInt(GoProxy.getProxyService().getProxyConfig().getPort()))));
                    --this.toSendAllPlayers;
                }
            }
        });
    }
}