package net.vicemice.hector.api.clickableitems.spigot;

import net.vicemice.hector.api.clickableitems.ClickableItem;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public abstract class ClickableItemSpigot extends ClickableItem<IUser> {
    private final ItemStack itemStack;

    public ClickableItemSpigot(String identifier, boolean description, ItemStack itemStack) {
        super(identifier, description);
        this.itemStack = itemStack;
    }

    public ItemStack getItemStack() {
        return this.itemStack;
    }
}
