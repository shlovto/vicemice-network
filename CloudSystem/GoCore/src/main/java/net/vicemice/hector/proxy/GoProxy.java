package net.vicemice.hector.proxy;

import com.google.common.reflect.ClassPath;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.proxy.ProxyStopPacket;
import net.vicemice.hector.proxy.apihandler.CloudUserAPI;
import net.vicemice.hector.proxy.config.mongo.MongoConfig;
import net.vicemice.hector.proxy.config.mongo.MongoConfiguration;
import net.vicemice.hector.proxy.config.proxy.ProxyConfig;
import net.vicemice.hector.proxy.config.proxy.ProxyConfiguration;
import net.vicemice.hector.proxy.nettyclient.AliveScheduler;
import net.vicemice.hector.proxy.nettyclient.PacketResolver;
import net.vicemice.hector.proxy.service.ProxyService;
import net.vicemice.hector.proxy.service.labymod.LabyService;
import net.vicemice.hector.proxy.service.punish.PunishManager;
import net.vicemice.hector.proxy.util.ProxyManager;
import net.vicemice.hector.proxy.util.TimeOutManager;
import net.vicemice.hector.proxy.util.labymod.LabyResponse;
import net.vicemice.hector.proxy.util.mongodb.MongoManager;
import net.vicemice.hector.service.server.NettyClient;
import net.vicemice.hector.utils.CommandType;
import net.vicemice.hector.proxy.commands.*;
import net.vicemice.hector.utils.locale.LocaleManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/*
 * Class created at 11:19 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GoProxy extends Plugin {
    @Getter
    private static GoProxy service;
    @Getter
    private static ProxyService proxyService;
    @Getter
    @Setter
    private static LocaleManager localeManager;
    private boolean running = true;
    private boolean startedUp = false;

    public void onEnable() {
        System.out.println("  ___ ___                 __                ");
        System.out.println(" /   |   \\   ____   _____/  |_  ___________ ");
        System.out.println("/    ~    \\_/ __ \\_/ ___\\   __\\/  _ \\_  __ \\");
        System.out.println("\\    Y    /\\  ___/\\  \\___|  | (  <_> )  | \\/");
        System.out.println(" \\___|_  /  \\___  >\\___  >__|  \\____/|__|   ");
        System.out.println("       \\/       \\/     \\/                   ");
        System.out.println("Preparing Hector Systems");
        System.out.println("Preparing to start the Hector Proxy Software (Version " + getDescription().getVersion() + ")");

        GoAPI.setUserAPI(new CloudUserAPI());

        service = this;
        proxyService = new ProxyService();
        ArrayList<String> s = new ArrayList<>();
        s.add("cloud/locales/core");
        s.add("cloud/locales/proxy");
        localeManager = LocaleManager.newManager(s);
        getProxyService().setProxyManager(new ProxyManager());
        getProxyService().setPunishManager(new PunishManager());

        getProxyService().setProxyConfiguration(new ProxyConfiguration());
        if (!getProxyService().getProxyConfiguration().exists()) {
            ProxyConfig config = new ProxyConfig();
            getProxyService().getProxyConfiguration().writeConfiguration(config);
            System.out.println("Proxy Config File not found and will be created...");
            System.out.println("Proxy Config File was created!");
            System.exit(0);
        }
        getProxyService().getProxyConfiguration().readConfiguration();
        getProxyService().setProxyConfig(getProxyService().getProxyConfiguration().getProxyConfig());

        getProxyService().setMongoConfiguration(new MongoConfiguration());
        if (!getProxyService().getMongoConfiguration().exists()) {
            MongoConfig config = new MongoConfig();
            getProxyService().getMongoConfiguration().writeConfiguration(config);
            System.out.println("Mongo Config File not found and will be created...");
            System.out.println("Mongo Config File was created!");
            System.exit(0);
        }
        getProxyService().getMongoConfiguration().readConfiguration();
        getProxyService().setMongoConfig(getProxyService().getMongoConfiguration().getMongoConfig());
        
        getProxyService().setMongoManager(new MongoManager());
        getProxyService().getPunishManager().init();
        getProxyService().getProxyManager().setVerify(getProxyService().getProxyConfig().isVerify());

        System.out.println("Load all Helper and Manager...");

        getProxyService().setLabyService(new LabyService());
        getProxyService().setAliveScheduler(new AliveScheduler());
        getProxyService().setTimeOutManager(new TimeOutManager());
        getProxyService().setCommandManager(new CommandManager());

        System.out.println("Load all Listeners...");
        PluginManager pluginManager = ProxyServer.getInstance().getPluginManager();
        try {
            for (ClassPath.ClassInfo classInfo : ClassPath.from(getClass().getClassLoader()).getTopLevelClassesRecursive("net.vicemice.hector.proxy.listener")) {
                Class clazz = Class.forName(classInfo.getName());

                if (Listener.class.isAssignableFrom(clazz)) {
                    pluginManager.registerListener(this, (Listener) clazz.newInstance());
                }
            }
        } catch (IOException | ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
            ex.printStackTrace();
        }
        pluginManager.registerListener(this, new LabyResponse());

        System.out.println("Load all Commands...");
        //pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.ACP, "acp"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.HELP, "help","hilfe"));

        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.AFK, "afk"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.ALERT, "alert"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.CLOUD, "cloud"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.LANGUAGE, "sprache", "lang", "language"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.CHATLOG, "chatlog", "cl"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.GOTO, "goto", "gs"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.RANG, "rank", "rang"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.HUB, "hub", "lobby", "l"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.NOTIFY, "notify"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.PLAYTIME, "playtime", "spielzeit"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.REPORT, "report"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.SEND, "send"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.ONLINE, "online", "glist", "players"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.SERVER, "server"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.NICK, "nick"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.MAPEDIT, "mapedit"));

        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.XP, "xp"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.LEVEL, "level"));

        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.KICK, "kick"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.TEMPBAN, "tempban", "tban"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.TEMPMUTE, "tempmute", "tmute"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.BANLOG, "banlog", "blog", "bl"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.BANINFO, "baninfo", "binfo", "bi"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.PUNISH, "ban", "mute", "punish", "warn"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.UNBAN, "unban"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.UNMUTE, "unmute"));

        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.LOCALTEAMCHAT, "localteamchat", "ltc"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.TEAMCHAT, "teamchat", "tc"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.TEAM, "team"));

        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.REPLAY, "replay"));
        pluginManager.registerCommand(this, new CommandPing());
        pluginManager.registerCommand(this, new CommandMSG());
        pluginManager.registerCommand(this, new CommandMSGReplay());

        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.CLAN, "clan"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.FRIENDS, "friend", "friends", "f"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.PARTY, "party"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.CLANCHAT, "clanchat", "cc", "c"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.PARTYCHAT, "partychat", "pc", "p"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.LINK, "link"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.UNLINK, "unlink"));

        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.PLAY, "play"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.FORUM, "forum"));
        pluginManager.registerCommand(this, new ProxyCommandForward(CommandType.VOTE, "vote"));

        this.connectNetty();

        ProxyServer.getInstance().getScheduler().schedule(this, () -> {
                    long diff;
                    if (!this.startedUp && getProxyService().getLastPacketReceive() != 0 && System.currentTimeMillis() > getProxyService().getLastPacketReceive() && (diff = System.currentTimeMillis() - getProxyService().getLastPacketReceive()) >= 2000) {
                        this.startedUp = true;
                    }
                    if (getProxyService().getNettyClient().getChannel() == null || !getProxyService().getNettyClient().getChannel().isActive() || !getProxyService().getNettyClient().getChannel().isOpen()) {
                        getProxyService().getProxyManager().setConnected(false);

                /*System.out.println("Try to reconnect to cloud...");

                this.connectNetty();
                 */
                    } else {
                        getProxyService().getProxyManager().setConnected(true);
                    }
                }
                , 1, 1, TimeUnit.SECONDS);
        getProxyService().setName(getProxyService().getProxyConfig().getProxyid());
    }

    public void onDisable() {
        this.running = false;
        if (getProxyService().getNettyClient().getChannel() != null && getProxyService().getNettyClient().getChannel().isOpen() && getProxyService().getNettyClient().getChannel().isActive()) {
            getProxyService().getNettyClient().getChannel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.PROXY_STOP_PACKET, new ProxyStopPacket(getProxyService().getProxyConfig().getProxyid())));
        }
    }

    public void connectNetty() {
        if (!this.running) {
            return;
        }
        getProxyService().getMuteData().clear();
        getProxyService().getBanData().clear();
        getProxyService().getPlayerRanks().clear();
        getProxyService().getPremiumUsers().clear();
        getProxyService().getChatBlocked().clear();
        getProxyService().getBannedIPS().clear();
        System.out.println("[!] Try to connect to the cloud...");
        getProxyService().setNettyClient(new NettyClient("91.218.67.206", 1327));
        ProxyServer.getInstance().getScheduler().runAsync(this, () -> {
            getProxyService().getNettyClient().startClient();
            getProxyService().getNettyClient().addGetter(new PacketResolver());
        });
    }
}
