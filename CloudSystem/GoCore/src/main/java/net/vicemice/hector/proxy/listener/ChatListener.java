package net.vicemice.hector.proxy.listener;

import io.netty.channel.Channel;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.Abuses;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.ChatPacket;
import net.vicemice.hector.proxy.GoProxy;
import net.vicemice.hector.proxy.util.mongodb.PlayerAPI;
import net.vicemice.hector.utils.players.punish.Punish;

import java.util.Locale;

public class ChatListener implements Listener {

    @EventHandler
    public void onChat(ChatEvent chatEvent) {
        if (chatEvent.getMessage().toLowerCase().startsWith("/")) {
            return;
        }
        String message = chatEvent.getMessage().toLowerCase();
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) chatEvent.getSender();
        if (GoProxy.getProxyService().getMuteData().containsKey(proxiedPlayer.getUniqueId())) {
            chatEvent.setCancelled(true);
            Abuses.Abuse muteData = GoProxy.getProxyService().getMuteData().get(proxiedPlayer.getUniqueId());
            long length = muteData.getLength();
            Punish punish = GoProxy.getProxyService().getPunishManager().getByKey(muteData.getReason());
            String reason = muteData.getReason();

            if (punish != null) {
                reason = GoAPI.getUserAPI().translate( proxiedPlayer.getUniqueId(), reason.toUpperCase());
            }

            if (length == 0) {
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( proxiedPlayer.getUniqueId(), "mute-perma", reason)));
            } else {
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( proxiedPlayer.getUniqueId(), "mute-temp", reason, GoAPI.getTimeManager().getRemainingTimeString(GoProxy.getLocaleManager(), Locale.forLanguageTag(new PlayerAPI(proxiedPlayer.getUniqueId()).get("locale")), length))));
            }
            return;
        }
        if (proxiedPlayer.getServer().getInfo().getName().toLowerCase().startsWith("verify-")) {
            return;
        }
        if (GoProxy.getProxyService().getPlayerMessages().containsKey(proxiedPlayer)) {
            if (!message.equalsIgnoreCase("gg")) {
                if (GoProxy.getProxyService().getPlayerMessages().get(proxiedPlayer).equalsIgnoreCase(message) && !GoProxy.getProxyService().getPlayerRanks().get(proxiedPlayer.getUniqueId()).isAdmin()) {
                    chatEvent.setCancelled(true);
                    proxiedPlayer.sendMessage(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( proxiedPlayer.getUniqueId(), "chat-repeating")));
                    return;
                }
                GoProxy.getProxyService().getPlayerMessages().put(proxiedPlayer, message);
            }
        } else if (!message.equalsIgnoreCase("gg")) {
            GoProxy.getProxyService().getPlayerMessages().put(proxiedPlayer, message);
        }
        double d;

        if (chatEvent.getMessage().length() >= 5 && (d = this.getUppercasePercentage(chatEvent.getMessage())) > 40.0 && !GoProxy.getProxyService().getPlayerRanks().get(proxiedPlayer.getUniqueId()).isAdmin()) {
            chatEvent.setMessage(chatEvent.getMessage().toLowerCase());
        }

        if (!characterCheck(chatEvent.getMessage())) {
            chatEvent.setCancelled(true);
            proxiedPlayer.sendMessage(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( proxiedPlayer.getUniqueId(), "chat-invalid-characters")));
            return;
        }

        message = getRawMessage(message);

        if (!GoProxy.getProxyService().getPlayerRanks().get(proxiedPlayer.getUniqueId()).isAdmin()) {
            for (String word : GoProxy.getProxyService().getChatBlocked()) {
                if (!message.contains(word.toLowerCase())) continue;
                chatEvent.setCancelled(true);
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( proxiedPlayer.getUniqueId(), "chat-behaviour")));
                return;
            }
        }

        Channel channel = GoProxy.getProxyService().getNettyClient().getChannel();
        channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.CHAT_PACKET, new ChatPacket(proxiedPlayer.getUniqueId(), chatEvent.getMessage(), System.currentTimeMillis())));
    }

    private boolean isUppercase(String string) {
        return "ABCDEFGHIJKLMNOPQRSTUVWXYZ".contains(string);
    }

    private double getUppercasePercentage(String string) {
        double d = 0.0;
        for (int i = 0; i < string.length(); ++i) {
            if (!this.isUppercase(string.substring(i, i + 1))) continue;
            d += 1.0;
        }
        return d / (double)string.length() * 100.0;
    }

    public String getRawMessage(String message) {
        return message.replace("#", "").replace("ö", "").replace("Ö", "")
                .replace("ä", "").replace("Ä", "").replace("ü", "").replace("€", "~")
                .replace('´', ' ').replace('´', ' ').replace("²", "").replace("³", "")
                .replace(",", "").replace(";", "").replace(".", "").replace(":", "")
                .replace("_", "").replace("-", "").replace("!", "").replace('"', ' ')
                .replace("$", "").replace("%", "").replace("&", "").replace("/", "")
                .replace("(", "").replace(")", "").replace("=", "").replace("?", "")
                .replace("^", "").replace("°", "").replace("@", "").replace("ß", "")
                .replace("{", "}").replace("<", "").replace(">", "").replace("|", "")
                .replace("+", "").replace("*", "").replace("", "").replace("'", "")
                .replace(" ", "");
    }

    public boolean characterCheck(String name) {
        for (char c : name.toCharArray()) {
            if ('0' <= c && c <= '9') continue;
            if ('a' <= c && c <= 'z') continue;
            if ('A' <= c && c <= 'Z') continue;
            if (c == ' ' || c == 'ö' ||  c == 'Ö' ||  c == 'ä' || c == 'Ä' ||  c == 'ü' ||  c == 'Ü' || c == '€' || c == '~' || c == '`') continue;
            if (c == '#' || c == '²' ||  c == '³') continue;
            if (c == ',' ||  c == ';' ||  c == '.' || c == ':' ||  c == '_' ||  c == '-' ||  c == '!' ||  c == '"' ||  c == '$' ||  c == '%') continue;
            if (c == '&' || c == '/' || c == '(' ||  c == ')' ||  c == '=' || c == '?' ||  c == '´' ||  c == '^' ||  c == '°' ||  c == '@') continue;
            if (c == 'ß' || c == '{' || c == '[' ||  c == ']' ||  c == '}' || c == '<' ||  c == '>' ||  c == '|' ||  c == '+' ||  c == '*' || String.valueOf(c).equals("'")) continue;
            return false;
        }
        return true;
    }
}