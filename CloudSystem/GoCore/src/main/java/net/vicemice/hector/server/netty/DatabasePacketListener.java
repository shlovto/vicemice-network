package net.vicemice.hector.server.netty;

import io.netty.channel.Channel;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.database.PlayerDatabasePacket;
import net.vicemice.hector.server.utils.DatabaseManager;

public class DatabasePacketListener extends PacketGetter {

    @Override
    public void receivePacket(PacketHolder initPacket) {
        if (initPacket.getKey().equals(PacketType.PLAYER_DATABASE_PACKET.getKey())) {
            DatabaseManager.receiveDatabasePacket((PlayerDatabasePacket)initPacket.getValue());
        }
    }

    @Override
    public void channelActive(Channel channel) {
    }
}