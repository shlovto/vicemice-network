package net.vicemice.hector.proxy.config.mongo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.AccessLevel;
import lombok.Getter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Paul B. on 27.01.2019.
 */
public class MongoConfiguration {
    private File configFile;
    @Getter(AccessLevel.PUBLIC)
    private MongoConfig mongoConfig;
    private Gson gson;

    public MongoConfiguration() {
        this.configFile = new File("cloud/mongodb.json");
        this.gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
    }

    public boolean exists() {
        return configFile.exists();
    }

    public void writeConfiguration() {
        writeConfiguration(mongoConfig);
    }

    public void writeConfiguration(MongoConfig mongoConfig) {
        try (FileWriter fileWriter = new FileWriter(configFile)) {
            gson.toJson(mongoConfig, fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readConfiguration() {
        try (FileReader fileReader = new FileReader(configFile)) {
            mongoConfig = gson.fromJson(fileReader, MongoConfig.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}