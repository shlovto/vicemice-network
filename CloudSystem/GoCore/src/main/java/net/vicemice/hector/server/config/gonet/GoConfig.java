package net.vicemice.hector.server.config.gonet;

import lombok.Data;
import org.apache.commons.lang.RandomStringUtils;

/*
 * Class created at 23:12 - 04.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class GoConfig {
    private boolean MANUAL_SERVER = false;
    private boolean AUTO_LOBBY_STATE = false;
    private String HOST = "localhost";
    private String NAME = "GoNet-Server-"+RandomStringUtils.randomNumeric(8);
    private int PORT = 25565;
    private int VERSION = 47;
    private int MAX_PLAYERS = 50;
    private String MOTD = "This is a GoNet Server from Minetasia.net";
    private String SERVER_GROUP = "no";
    private boolean BETA = false;
    private boolean PROTOCOLLIB_NPC = false;
}
