package net.vicemice.hector.server.internal;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.stats.PlayerAddStatPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.API;

import java.util.UUID;

public class InternalAPI {

    public static void addPacketListener(PacketGetter packetGetter, ListenerRegistry.NettyServer nettyServer) {
        if (nettyServer.equals(ListenerRegistry.NettyServer.WRAPPER)) {
            GoServer.getService().getGoAPI().getNettyClient().addGetter(packetGetter);
        } else if (nettyServer.equals(ListenerRegistry.NettyServer.LOBBY)) {
            GoServer.getService().getGoAPI().getNettyClient().addGetter(packetGetter);
        }
    }

    public static void sendStatPacket(UUID uniqueId, String key, long value) {
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.PLAYER_ADD_STAT_PACKET, new PlayerAddStatPacket(uniqueId, key, value)), API.PacketReceiver.SLAVE);
    }
}