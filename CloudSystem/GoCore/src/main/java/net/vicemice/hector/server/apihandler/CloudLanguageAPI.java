package net.vicemice.hector.server.apihandler;

import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.types.LanguageAPI;
import net.vicemice.hector.utils.language.Language;

import java.util.UUID;

public class CloudLanguageAPI implements LanguageAPI {

    @Override
    public Language.LanguageType getLanguage(UUID uuid) {
        if (GoServer.getService().getLanguageHandler().languages.containsKey(uuid)) {
            return GoServer.getService().getLanguageHandler().languages.get(uuid);
        }
        return Language.LanguageType.GERMAN;
    }
}