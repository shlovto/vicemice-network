package net.vicemice.hector.api.clickableitems;

public abstract class ClickableItem<P> {
    private final String identifier;
    private final boolean description;

    public ClickableItem(String identifier, boolean description) {
        this.identifier = identifier;
        this.description = description;
    }

    public String getNameTranslationKey() {
        return String.format("clickable-item-%s-name", this.identifier);
    }

    public String getDescriptionTranslationKey() {
        return String.format("clickable-item-%s-description", this.identifier);
    }

    public abstract void onClick(P var1, ClickType var2);

    public String getIdentifier() {
        return this.identifier;
    }

    public boolean isDescription() {
        return this.description;
    }
}