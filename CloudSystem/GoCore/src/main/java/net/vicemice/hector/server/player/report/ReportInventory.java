package net.vicemice.hector.server.player.report;

import lombok.Getter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.CommandPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.internal.SkullChanger;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.CommandType;
import net.vicemice.hector.utils.players.ReportPlayer;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import java.util.ArrayList;
import java.util.Arrays;

/*
 * Class created at 19:14 - 16.04.2020
 * Copyright (C) elrobtossohn
 */
public class ReportInventory {

    public GUI create(IUser user, ReportPlayer target, ItemStack reason, String offense, boolean confirmation) {
        if (target == null) return user.createGUI(" ", 0);
        if (confirmation && reason != null && offense != null) {
            GUI gui = user.createGUI(user.translate("report-confirm-title"), 3);

            String playerName = GoAPI.getUserAPI().getRankData(target.getUniqueId()).getRankColor()+ target.getName();
            gui.setItem(10, ItemBuilder.create(SkullChanger.getSkull(target.getSignature(), target.getValue())).name(playerName).lore(new ArrayList<>(Arrays.asList(user.translate("report-player-description", playerName).split("\n")))).build());

            gui.setItem(13, reason);

            gui.setItem(16, ItemBuilder.create(Material.INK_SACK).durability(10).name("§a"+user.translate("report-confirm-name")).lore(new ArrayList<>(Arrays.asList(user.translate("report-confirm-description", playerName, reason.getItemMeta().getDisplayName()).split("\n")))).build(), e -> {
                e.getView().close();
                GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(user.getName(), CommandType.REPORT, new String[]{"confirm", target.getName(),offense})), API.PacketReceiver.SLAVE);
            });

            return gui;
        }

        ServerType serverType = ServerType.OTHER;
        if (target.getServer().contains("Lobby")) serverType = ServerType.LOBBY;
        if (target.getServer().contains("QSG")) serverType = ServerType.QSG;
        if (target.getServer().contains("BedWars")) serverType = ServerType.BEDWARS;
        GUI gui = user.createGUI(user.translate("report-gui-title"), serverType.getRows());

        gui.setItem(10, ItemBuilder.create(Material.IRON_SWORD).name("§e"+user.translate("report-hacking-name")).lore(new ArrayList<>(Arrays.asList(user.translate("report-hacking-description").split("\n")))).build(), e -> {
            this.create(user, target, e.getCurrentItem(), "HACKING", true).open();
        });
        gui.setItem(11, ItemBuilder.create(Material.BOOK_AND_QUILL).name("§e"+user.translate("report-chat-name")).lore(new ArrayList<>(Arrays.asList(user.translate("report-chat-description").split("\n")))).build(), e -> {
            this.create(user, target, e.getCurrentItem(), "CHAT", true).open();
        });
        gui.setItem(12, ItemBuilder.create(Material.SKULL_ITEM).durability(3).name("§e"+user.translate("report-skin-name")).lore(new ArrayList<>(Arrays.asList(user.translate("report-skin-description").split("\n")))).build(), e -> {
            this.create(user, target, e.getCurrentItem(), "SKIN", true).open();
        });
        gui.setItem(13, ItemBuilder.create(Material.NAME_TAG).name("§e"+user.translate("report-username-name")).lore(new ArrayList<>(Arrays.asList(user.translate("report-username-description").split("\n")))).build(), e -> {
            this.create(user, target, e.getCurrentItem(), "NAME", true).open();
        });
        gui.setItem(14, ItemBuilder.create(Material.IRON_CHESTPLATE).name("§e"+user.translate("report-clan-name")).lore(new ArrayList<>(Arrays.asList(user.translate("report-clan-description").split("\n")))).build(), e -> {
            this.create(user, target, e.getCurrentItem(), "CLAN", true).open();
        });
        gui.setItem(15, ItemBuilder.create(Material.PISTON_STICKY_BASE).name("§e"+user.translate("report-bug-using-name")).lore(new ArrayList<>(Arrays.asList(user.translate("report-bug-using-description").split("\n")))).build(), e -> {
            this.create(user, target, e.getCurrentItem(), "BUGUSING", true).open();
        });
        if (serverType == ServerType.LOBBY) {
            gui.setItem(16, ItemBuilder.create(Material.MONSTER_EGG).name("§e"+user.translate("report-pets-name")).lore(new ArrayList<>(Arrays.asList(user.translate("report-pets-description").split("\n")))).build(), e -> {
                this.create(user, target, e.getCurrentItem(), "PETS", true).open();
            });
        }
        if (serverType == ServerType.QSG) {
            gui.setItem(16, ItemBuilder.create(Material.FLINT_AND_STEEL).name("§e"+user.translate("report-trolling-name")).lore(new ArrayList<>(Arrays.asList(user.translate("report-trolling-description").split("\n")))).build(), e -> {
                this.create(user, target, e.getCurrentItem(), "TROLLING", true).open();
            });
            gui.setItem(19, ItemBuilder.create(Material.BED).name("§e"+user.translate("report-teaming-name")).lore(new ArrayList<>(Arrays.asList(user.translate("report-teaming-description").split("\n")))).build(), e -> {
                this.create(user, target, e.getCurrentItem(), "TEAMING", true).open();
            });
            gui.setItem(20, ItemBuilder.create(Material.GOLD_INGOT).name("§e"+user.translate("report-trolling-name")).lore(new ArrayList<>(Arrays.asList(user.translate("report-trolling-description").split("\n")))).build(), e -> {
                this.create(user, target, e.getCurrentItem(), "TROLLING", true).open();
            });
        }
        if (serverType == ServerType.BEDWARS) {
            gui.setItem(16, ItemBuilder.create(Material.FLINT_AND_STEEL).name("§e"+user.translate("report-trolling-name")).lore(new ArrayList<>(Arrays.asList(user.translate("report-trolling-description").split("\n")))).build(), e -> {
                this.create(user, target, e.getCurrentItem(), "TROLLING", true).open();
            });
            gui.setItem(19, ItemBuilder.create(Material.BED).name("§e"+user.translate("report-teaming-name")).lore(new ArrayList<>(Arrays.asList(user.translate("report-teaming-description").split("\n")))).build(), e -> {
                this.create(user, target, e.getCurrentItem(), "TEAMING", true).open();
            });
            gui.setItem(20, ItemBuilder.create(Material.GOLD_INGOT).name("§e"+user.translate("report-trolling-name")).lore(new ArrayList<>(Arrays.asList(user.translate("report-trolling-description").split("\n")))).build(), e -> {
                this.create(user, target, e.getCurrentItem(), "TROLLING", true).open();
            });
            gui.setItem(21, ItemBuilder.create(Material.SANDSTONE).name("§e"+user.translate("report-building-name")).lore(new ArrayList<>(Arrays.asList(user.translate("report-building-description").split("\n")))).build(), e -> {
                this.create(user, target, e.getCurrentItem(), "BUILDING", true).open();
            });
            gui.setItem(22, ItemBuilder.create(Material.SANDSTONE).name("§e"+user.translate("report-spawn-trapping-name")).lore(new ArrayList<>(Arrays.asList(user.translate("report-spawn-trapping-description").split("\n")))).build(), e -> {
                this.create(user, target, e.getCurrentItem(), "SPAWNTRAPPING", true).open();
            });
        }

        return gui;
    }

    public enum ServerType {
        LOBBY(3),
        QSG(4),
        BEDWARS(4),
        OTHER(3);

        private ServerType(int rows) {
            this.rows = rows;
        }

        @Getter
        private int rows;
    }
}
