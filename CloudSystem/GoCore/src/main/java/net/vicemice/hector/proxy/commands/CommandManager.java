package net.vicemice.hector.proxy.commands;

import io.netty.channel.Channel;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.CommandPacket;
import net.vicemice.hector.proxy.GoProxy;
import net.vicemice.hector.utils.CommandType;

public class CommandManager {

    public void runCommand(CommandType commandType, String name, String[] args, CommandSender commandSender) {
        ProxyServer.getInstance().getScheduler().runAsync(GoProxy.getService(), () -> {
            if (!GoProxy.getProxyService().getProxyManager().isConnected()) {
                return;
            }

            ProxiedPlayer proxiedPlayer = (ProxiedPlayer)commandSender;

            if (!GoProxy.getProxyService().getTimeOutManager().checkTimeout(name)) {
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( proxiedPlayer.getUniqueId(), "command-timeout")));
                return;
            }

            if (!commandType.equals(CommandType.FORUM) && !commandType.equals(CommandType.LINK) && !commandType.equals(CommandType.UNLINK) && proxiedPlayer.getServer().getInfo().getName().equalsIgnoreCase("VerifyServer")) {
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( proxiedPlayer.getUniqueId(), "command-not-available")));
                return;
            }
            Channel channel = GoProxy.getProxyService().getNettyClient().getChannel();
            channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.COMMAND_PACKET, new CommandPacket(name, commandType, args)));
        });
    }
}