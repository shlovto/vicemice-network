package net.vicemice.hector.server.player.scoreboard;

import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.PacketContainer;
import net.minecraft.server.v1_8_R3.IScoreboardCriteria;
import net.minecraft.server.v1_8_R3.PacketPlayOutScoreboardScore;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class PacketScoreboard {
    private Player player;

    private String displayedName;
    private Map<Integer, String> lines = new HashMap<>();

    private boolean displayed;

    /**
     *
     * @param player defines the player
     */
    public PacketScoreboard(Player player) {
        this.player = player;
        this.displayed = false;
    }

    public void sendSidebar() {
        this.sendSidebar((this.displayedName != null ? displayedName : "§c§lNo Displayname set!"));
    }

    /**
     * Send the Scoreboard to the Player
     * @param displayedName defines the display name of the scoreboard
     */
    public void sendSidebar(String displayedName) {
        if (this.displayed)
            return;
        this.remove();
        this.displayedName = displayedName;

        PacketContainer scoreboard = new PacketContainer(PacketType.Play.Server.SCOREBOARD_OBJECTIVE);
        scoreboard.getStrings().write(0, "sideboard").write(1, displayedName);
        scoreboard.getIntegers().write(0, 0);
        scoreboard.getSpecificModifier(IScoreboardCriteria.EnumScoreboardHealthDisplay.class).write(0, IScoreboardCriteria.EnumScoreboardHealthDisplay.INTEGER);

        PacketContainer playOut = new PacketContainer(PacketType.Play.Server.SCOREBOARD_DISPLAY_OBJECTIVE);
        playOut.getIntegers().write(0, 1);
        playOut.getStrings().write(0, "sideboard");

        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(this.player, scoreboard);
            ProtocolLibrary.getProtocolManager().sendServerPacket(this.player, playOut);

            this.displayed = true;
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            this.displayed = false;
        }
    }

    /**
     * Remove the exist Scoreboard
     */
    public void remove() {
        PacketContainer unsend = new PacketContainer(PacketType.Play.Server.SCOREBOARD_OBJECTIVE);
        unsend.getStrings().write(0, "sideboard").write(1, "");
        unsend.getIntegers().write(0, 1);

        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(this.player, unsend);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        this.displayed = false;
    }

    /**
     * Add a Line to the Scoreboard
     * @param line defines the line
     * @param text defines the text on the line
     */
    public void setLine(Integer line, String text) {
        if (!this.displayed)
            return;
        if (text.length() > 40)
            text = text.substring(0, 40);

        PacketContainer score = new PacketContainer(PacketType.Play.Server.SCOREBOARD_SCORE);
        score.getStrings().write(0, text).write(1, "sideboard");
        score.getIntegers().write(0, line);
        score.getSpecificModifier(PacketPlayOutScoreboardScore.EnumScoreboardAction.class).write(0, PacketPlayOutScoreboardScore.EnumScoreboardAction.CHANGE);

        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(this.player, score);
            this.lines.put(line, text);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * Remove a Line from a Scoreboard
     * @param line defines the line
     */
    public void removeLine(Integer line) {
        if (!this.displayed)
            return;
        if (!this.lines.containsKey(line))
            return;
        String text = lines.get(line);

        PacketContainer score = new PacketContainer(PacketType.Play.Server.SCOREBOARD_SCORE);
        score.getStrings().write(0, text).write(1, "sideboard");
        score.getIntegers().write(0, line);
        score.getSpecificModifier(PacketPlayOutScoreboardScore.EnumScoreboardAction.class).write(0, PacketPlayOutScoreboardScore.EnumScoreboardAction.REMOVE);

        try {
            ProtocolLibrary.getProtocolManager().sendServerPacket(this.player, score);
            this.lines.remove(line);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set the Display Name of the Scoreboard
     * @param displayedName defines the new Name
     */
    public void setDisplayedName(String displayedName) {
        this.displayedName = displayedName;
    }

    /**
     * Update a Line
     * @param line defines the line
     * @param text defines the text on the line
     */
    public void updateLine(Integer line, String text) {
        this.removeLine(line);
        this.setLine(line, text);
    }

    /**
     * Get the Message of a Line
     * @param line defines the line
     * @return return the message
     */
    public String getLine(Integer line) {
        return this.lines.get(line);
    }

    /**
     * Get all lines
     * @return return the lines
     */
    public Map<Integer, String> getLines() {
        return this.lines;
    }
}