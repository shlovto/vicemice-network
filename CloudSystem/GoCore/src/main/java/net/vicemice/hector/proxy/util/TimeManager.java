package net.vicemice.hector.proxy.util;

import net.vicemice.hector.utils.language.Language;

public class TimeManager {

    public String getRemainingTimeString(Language.LanguageType language, long endTime) {
        String append;
        String[] end = this.getRemainingTime(endTime);
        StringBuilder stringBuilder = new StringBuilder();
        int days = Integer.valueOf(end[0]);
        int hours = Integer.valueOf(end[1]);
        int minutes = Integer.valueOf(end[2]);
        int seconds = Integer.valueOf(end[3]);
        if (days != 0) {
            if (language == Language.LanguageType.GERMAN) {
                append = days == 1 ? days + " Tag " : days + " Tage ";
            } else {
                append = days == 1 ? days + " day " : days + " days ";
            }
            stringBuilder.append(append);
        }
        if (hours != 0) {
            if (language == Language.LanguageType.GERMAN) {
                append = hours == 1 ? hours + " Stunde " : hours + " Stunden ";
            } else {
                append = hours == 1 ? hours + " hour " : hours + " hours ";
            }
            stringBuilder.append(append);
        }
        if (minutes != 0) {
            if (language == Language.LanguageType.GERMAN) {
                append = minutes == 1 ? minutes + " Minute " : minutes + " Minuten ";
            } else {
                append = minutes == 1 ? minutes + " minute " : minutes + " minutes ";
            }
            stringBuilder.append(append);
        }
        if (seconds != 0) {
            if (language == Language.LanguageType.GERMAN) {
                append = seconds == 1 ? seconds + " Sekunde" : seconds + " Sekunden";
            } else {
                append = seconds == 1 ? seconds + " second" : seconds + " seconds";
            }
            stringBuilder.append(append);
        }
        return stringBuilder.toString();
    }

    private String[] getRemainingTime(long milli) {
        long time = 0;
        time = System.currentTimeMillis() > milli ? System.currentTimeMillis() - milli : milli - System.currentTimeMillis();
        int days = 0;
        int hours = 0;
        int minutes = 0;
        int seconds = 0;
        if ((time /= 1000) >= 86400) {
            days = (int)time / 86400;
        }
        if ((time -= (long)(days * 86400)) > 3600) {
            hours = (int)time / 3600;
        }
        if ((time -= (long)(hours * 3600)) > 60) {
            minutes = (int)time / 60;
        }
        seconds = (int)(time -= (long)(minutes * 60));
        String[] al = new String[]{Integer.toString(days), Integer.toString(hours), Integer.toString(minutes), Integer.toString(seconds)};
        return al;
    }
}

