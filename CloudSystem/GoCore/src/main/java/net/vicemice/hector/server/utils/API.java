package net.vicemice.hector.server.utils;

import io.netty.channel.Channel;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.server.GoServer;

/*
 * Class created at 11:56 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class API {
    public void sendPacket(PacketHolder initPacket, PacketReceiver packetReceiver) {
        if (packetReceiver.equals(PacketReceiver.SLAVE)) {
            this.sendPacket(initPacket, GoServer.getService().getGoAPI().getNettyClient().getChannel());
        }
    }

    private void sendPacket(PacketHolder initPacket, Channel channel) {
        if (channel != null && channel.isOpen() && channel.isActive()) {
            channel.writeAndFlush(initPacket);
        }
    }

    public static enum PacketReceiver {
        SLAVE;
    }
}