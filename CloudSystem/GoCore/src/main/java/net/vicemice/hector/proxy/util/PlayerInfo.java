package net.vicemice.hector.proxy.util;

import net.md_5.bungee.BungeeCord;
import net.vicemice.hector.utils.Rank;

import java.beans.ConstructorProperties;

public class PlayerInfo {
    private final String name;
    private boolean unloadAble;
    private boolean premium;
    private String skinValue;
    private String skinSignature;
    private String nickName;
    private String nickSkinValue;
    private String nickSkinSignature;
    private Rank rank;
    private boolean chatAvariable;

    public PlayerInfo(String name, boolean unloadAble) {
        this.name = name;
        this.unloadAble = unloadAble;
    }

    public void reset() {
        this.chatAvariable = false;
        this.skinValue = null;
        this.skinSignature = null;
        this.nickName = null;
        this.nickSkinValue = null;
        this.nickSkinSignature = null;
    }

    public boolean isNicked() {
        return this.nickName != null;
    }

    public void setRank(Rank target) {
        this.rank = this.rank == Rank.MEMBER ? null : target;
        this.updateUnloadAble();
    }

    public void setPremium(boolean flag) {
        this.premium = flag;
        this.updateUnloadAble();
    }

    @Deprecated
    public void updateUnloadAble() {
        boolean bl = this.unloadAble = !this.premium && this.rank == null;
        if (this.unloadAble && BungeeCord.getInstance().getPlayer(this.name) == null) {
            PlayerInfoManager.unloadPlayer(this.name);
        }
    }

    public Rank getRank() {
        return this.rank == null ? Rank.MEMBER : this.rank;
    }

    public String getName() {
        return this.name;
    }

    public boolean isUnloadAble() {
        return this.unloadAble;
    }

    public boolean isPremium() {
        return this.premium;
    }

    public String getSkinValue() {
        return this.skinValue;
    }

    public String getSkinSignature() {
        return this.skinSignature;
    }

    public String getNickName() {
        return this.nickName;
    }

    public String getNickSkinValue() {
        return this.nickSkinValue;
    }

    public String getNickSkinSignature() {
        return this.nickSkinSignature;
    }

    public boolean isChatAvariable() {
        return this.chatAvariable;
    }

    public void setUnloadAble(boolean unloadAble) {
        this.unloadAble = unloadAble;
    }

    public void setSkinValue(String skinValue) {
        this.skinValue = skinValue;
    }

    public void setSkinSignature(String skinSignature) {
        this.skinSignature = skinSignature;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setNickSkinValue(String nickSkinValue) {
        this.nickSkinValue = nickSkinValue;
    }

    public void setNickSkinSignature(String nickSkinSignature) {
        this.nickSkinSignature = nickSkinSignature;
    }

    public void setChatAvariable(boolean chatAvariable) {
        this.chatAvariable = chatAvariable;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof PlayerInfo)) {
            return false;
        }
        PlayerInfo other = (PlayerInfo) o;
        if (!other.canEqual(this)) {
            return false;
        }
        String this$name = this.getName();
        String other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) {
            return false;
        }
        if (this.isUnloadAble() != other.isUnloadAble()) {
            return false;
        }
        if (this.isPremium() != other.isPremium()) {
            return false;
        }
        String this$skinValue = this.getSkinValue();
        String other$skinValue = other.getSkinValue();
        if (this$skinValue == null ? other$skinValue != null : !this$skinValue.equals(other$skinValue)) {
            return false;
        }
        String this$skinSignature = this.getSkinSignature();
        String other$skinSignature = other.getSkinSignature();
        if (this$skinSignature == null ? other$skinSignature != null : !this$skinSignature.equals(other$skinSignature)) {
            return false;
        }
        String this$nickName = this.getNickName();
        String other$nickName = other.getNickName();
        if (this$nickName == null ? other$nickName != null : !this$nickName.equals(other$nickName)) {
            return false;
        }
        String this$nickSkinValue = this.getNickSkinValue();
        String other$nickSkinValue = other.getNickSkinValue();
        if (this$nickSkinValue == null ? other$nickSkinValue != null : !this$nickSkinValue.equals(other$nickSkinValue)) {
            return false;
        }
        String this$nickSkinSignature = this.getNickSkinSignature();
        String other$nickSkinSignature = other.getNickSkinSignature();
        if (this$nickSkinSignature == null ? other$nickSkinSignature != null : !this$nickSkinSignature.equals(other$nickSkinSignature)) {
            return false;
        }
        Rank this$rank = this.getRank();
        Rank other$rank = other.getRank();
        if (this$rank == null ? other$rank != null : !this$rank.equals((Object) other$rank)) {
            return false;
        }
        if (this.isChatAvariable() != other.isChatAvariable()) {
            return false;
        }
        return true;
    }

    protected boolean canEqual(Object other) {
        return other instanceof PlayerInfo;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        String $name = this.getName();
        result = result * 59 + ($name == null ? 43 : $name.hashCode());
        result = result * 59 + (this.isUnloadAble() ? 79 : 97);
        result = result * 59 + (this.isPremium() ? 79 : 97);
        String $skinValue = this.getSkinValue();
        result = result * 59 + ($skinValue == null ? 43 : $skinValue.hashCode());
        String $skinSignature = this.getSkinSignature();
        result = result * 59 + ($skinSignature == null ? 43 : $skinSignature.hashCode());
        String $nickName = this.getNickName();
        result = result * 59 + ($nickName == null ? 43 : $nickName.hashCode());
        String $nickSkinValue = this.getNickSkinValue();
        result = result * 59 + ($nickSkinValue == null ? 43 : $nickSkinValue.hashCode());
        String $nickSkinSignature = this.getNickSkinSignature();
        result = result * 59 + ($nickSkinSignature == null ? 43 : $nickSkinSignature.hashCode());
        Rank $rank = this.getRank();
        result = result * 59 + ($rank == null ? 43 : $rank.hashCode());
        result = result * 59 + (this.isChatAvariable() ? 79 : 97);
        return result;
    }

    public String toString() {
        return "PlayerInfo(name=" + this.getName() + ", unloadAble=" + this.isUnloadAble() + ", premium=" + this.isPremium() + ", skinValue=" + this.getSkinValue() + ", skinSignature=" + this.getSkinSignature() + ", nickName=" + this.getNickName() + ", nickSkinValue=" + this.getNickSkinValue() + ", nickSkinSignature=" + this.getNickSkinSignature() + ", rank=" + this.getRank().getRankName() + ", chatAvariable=" + this.isChatAvariable() + ")";
    }

    @ConstructorProperties(value = {"name"})
    public PlayerInfo(String name) {
        this.name = name;
    }
}