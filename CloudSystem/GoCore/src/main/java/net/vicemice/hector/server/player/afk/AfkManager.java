package net.vicemice.hector.server.player.afk;

import lombok.Getter;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.afk.listener.CloudChatListener;
import net.vicemice.hector.server.player.afk.listener.PlayerJoinListener;
import net.vicemice.hector.server.player.afk.listener.PlayerMoveListener;
import net.vicemice.hector.server.player.afk.listener.PlayerQuitListener;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/*
 * Class created at 12:20 - 27.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class AfkManager {

    @Getter
    private ConcurrentHashMap<UUID, AfkSession> afkSessions;

    public AfkManager() {
        this.afkSessions = new ConcurrentHashMap<>();
        Bukkit.getPluginManager().registerEvents(new CloudChatListener(), GoServer.getService());
        Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), GoServer.getService());
        Bukkit.getPluginManager().registerEvents(new PlayerMoveListener(), GoServer.getService());
        Bukkit.getPluginManager().registerEvents(new PlayerQuitListener(), GoServer.getService());
        getRunnable().runTaskTimer(GoServer.getService(),0,20 * 60);
    }

    public BukkitRunnable getRunnable() {
        return new BukkitRunnable() {
            @Override
            public void run() {
                for (UUID uuid : afkSessions.keySet()) {
                    AfkSession session = afkSessions.get(uuid);
                    session.increaseMinute();
                }
            }
        };
    }
}