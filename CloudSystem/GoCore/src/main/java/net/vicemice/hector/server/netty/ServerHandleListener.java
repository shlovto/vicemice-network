package net.vicemice.hector.server.netty;

import io.netty.channel.Channel;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.server.SpigotServerStop;
import org.bukkit.Bukkit;

public class ServerHandleListener extends PacketGetter {
    @Override
    public void receivePacket(PacketHolder initPacket) {
        if (initPacket.getKey().equals(PacketType.SPIGOT_SERVER_STOP.getKey())) {
            SpigotServerStop spigotServerStop = (SpigotServerStop) initPacket.getValue();
            Bukkit.shutdown();
        }
    }

    @Override
    public void channelActive(Channel channel) {
    }
}
