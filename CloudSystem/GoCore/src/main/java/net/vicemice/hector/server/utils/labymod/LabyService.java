package net.vicemice.hector.server.utils.labymod;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.Getter;
import net.vicemice.hector.api.labymod.Addon;
import net.vicemice.hector.api.labymod.LabyModAPI;
import net.vicemice.hector.api.labymod.Permission;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.labymod.event.LabyModPlayerJoinEvent;
import net.vicemice.hector.server.utils.labymod.event.MessageReceiveEvent;
import net.vicemice.hector.server.utils.labymod.event.MessageSendEvent;
import net.vicemice.hector.server.utils.labymod.event.PermissionsSendEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
 * Class created at 01:47 - 23.04.2020
 * Copyright (C) elrobtossohn
 */
public class LabyService {

    private static final JsonParser JSON_PARSER = new JsonParser();

    @Getter
    private LabyModAPI api = new LabyModAPI();

    @Getter
    private PacketUtils packetUtils;

    public void init() {
        // Initializing packet utils
        this.packetUtils = new PacketUtils();

        // The LABYMOD plugin channel is higly deprecated and shouldn't be used - we just listen to it to retrieve old labymod clients.
        // Registering the incoming plugin messages listeners
        GoServer.getService().getServer().getMessenger().registerIncomingPluginChannel(GoServer.getService(), "LABYMOD", new PluginMessageListener() {
            @Override
            public void onPluginMessageReceived(String channel, final Player player, byte[] bytes) {
                // Converting the byte array into a byte buffer
                ByteBuf buf = Unpooled.wrappedBuffer(bytes);

                try {
                    // Reading the version from the buffer
                    final String version = api.readString(buf, Short.MAX_VALUE);

                    // Calling the event synchronously
                    Bukkit.getScheduler().runTask(GoServer.getService(), new Runnable() {
                        @Override
                        public void run() {
                            // Checking whether the player is still online
                            if (!player.isOnline())
                                return;

                            // Calling the LabyModPlayerJoinEvent
                            Bukkit.getPluginManager().callEvent(new LabyModPlayerJoinEvent(player, version, false, 0, new ArrayList<Addon>()));
                        }
                    });
                } catch (RuntimeException ex) {
                }
            }
        });

        GoServer.getService().getServer().getMessenger().registerIncomingPluginChannel(GoServer.getService(), "LMC", new PluginMessageListener() {
            @Override
            public void onPluginMessageReceived(String channel, final Player player, byte[] bytes) {
                // Converting the byte array into a byte buffer
                ByteBuf buf = Unpooled.wrappedBuffer(bytes);

                try {
                    // Reading the message key
                    final String messageKey = api.readString(buf, Short.MAX_VALUE);
                    final String messageContents = api.readString(buf, Short.MAX_VALUE);
                    final JsonElement jsonMessage = JSON_PARSER.parse(messageContents);

                    // Calling the event synchronously
                    Bukkit.getScheduler().runTask(GoServer.getService(), new Runnable() {
                        @Override
                        public void run() {
                            // Checking whether the player is still online
                            if (!player.isOnline())
                                return;

                            // Listening to the INFO (join) message
                            if (messageKey.equals("INFO") && jsonMessage.isJsonObject()) {
                                JsonObject jsonObject = jsonMessage.getAsJsonObject();
                                String version = jsonObject.has("version")
                                        && jsonObject.get("version").isJsonPrimitive()
                                        && jsonObject.get("version").getAsJsonPrimitive().isString() ? jsonObject.get("version").getAsString() : "Unknown";

                                boolean chunkCachingEnabled = false;
                                int chunkCachingVersion = 0;

                                if (jsonObject.has("ccp") && jsonObject.get("ccp").isJsonObject()) {
                                    JsonObject chunkCachingObject = jsonObject.get("ccp").getAsJsonObject();

                                    if (chunkCachingObject.has("enabled"))
                                        chunkCachingEnabled = chunkCachingObject.get("enabled").getAsBoolean();

                                    if (chunkCachingObject.has("version"))
                                        chunkCachingVersion = chunkCachingObject.get("version").getAsInt();
                                }

                                Bukkit.getPluginManager().callEvent(new LabyModPlayerJoinEvent(player, version,
                                        chunkCachingEnabled, chunkCachingVersion, Addon.getAddons(jsonObject)));
                                return;
                            }

                            // Calling the MessageReceiveEvent
                            Bukkit.getPluginManager().callEvent(new MessageReceiveEvent(player, messageKey, jsonMessage));
                        }
                    });
                } catch (RuntimeException ignored) {
                }
            }
        });
    }

    public void disable() {
        // Unregistering the plugin-message listeners
        GoServer.getService().getServer().getMessenger().unregisterIncomingPluginChannel(GoServer.getService(), "LABYMOD");
        GoServer.getService().getServer().getMessenger().unregisterIncomingPluginChannel(GoServer.getService(), "LMC");
    }

    /**
     * Sends the modified permissions to the given player
     *
     * @param player the player the permissions should be sent to
     */
    public void sendPermissions(Player player) {
        Map<Permission, Boolean> modifiedPermissions = new HashMap<>();

        for (Permission permission : Permission.values()) {
            modifiedPermissions.put(permission, permission.isDefaultEnabled());
        }

        // Calling the Bukkit event
        PermissionsSendEvent sendEvent = new PermissionsSendEvent(player, modifiedPermissions, false);
        Bukkit.getPluginManager().callEvent(sendEvent);

        // Sending the packet
        if (!sendEvent.isCancelled() && sendEvent.getPermissions().size() > 0)
            packetUtils.sendPacket(player, packetUtils.getPluginMessagePacket("LMC", api.getBytesToSend(modifiedPermissions)));
    }

    /**
     * Sends a JSON server-message to the player
     *
     * @param player          the player the message should be sent to
     * @param messageKey      the message's key
     * @param messageContents the message's contents
     */
    public void sendServerMessage(Player player, String messageKey, JsonElement messageContents) {
        messageContents = cloneJson(messageContents);

        // Calling the Bukkit event
        MessageSendEvent sendEvent = new MessageSendEvent(player, messageKey, messageContents, false);
        Bukkit.getPluginManager().callEvent(sendEvent);

        // Sending the packet
        if (!sendEvent.isCancelled())
            packetUtils.sendPacket(player, packetUtils.getPluginMessagePacket("LMC", api.getBytesToSend(messageKey, messageContents.toString())));
    }

    /**
     * Clones a JsonElement
     *
     * @param cloneElement the element that should be cloned
     * @return the cloned element
     */
    public JsonElement cloneJson(JsonElement cloneElement) {
        try {
            return JSON_PARSER.parse(cloneElement.toString());
        } catch (JsonParseException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}