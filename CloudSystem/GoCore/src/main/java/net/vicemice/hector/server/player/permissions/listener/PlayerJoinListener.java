package net.vicemice.hector.server.player.permissions.listener;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.utils.labymod.LabyModAPI;
import net.vicemice.hector.utils.Rank;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent playerJoinEvent) {
        Player player = playerJoinEvent.getPlayer();

        UserManager.join(player);

        GoServer.getService().getPermissionManager().getRankManager().setRankPermission(player);

        player.spigot().setCollidesWithEntities(false);
        player.saveData();

        GoServer.getService().getSpectators().forEach(e -> {
            Player player1 = Bukkit.getPlayer(e);
            if (player1 == null) return;
            player.hidePlayer(player1);
        });

        GoServer.getService().getLabyService().sendPermissions(player);

        boolean visible = true;
        String game = "§c§lUnknown";

        if (GoServer.getService().getServerName().toLowerCase().contains("lobby")) {
            game = "§e§lLobby";
        } else if (GoServer.getService().getServerName().toLowerCase().contains("bedwars")) {
            game = "§c§lBed§f§lWars";
        } else if (GoServer.getService().getServerName().toLowerCase().contains("qsg")) {
            game = "§a§lQSG";
        } else if (GoServer.getService().getServerName().toLowerCase().contains("replay")) {
            game = "§6§lReplay";
        } else if (GoServer.getService().getServerName().toLowerCase().contains("verify")) {
            game = "§a§lVerifying";
        } else {
            visible = false;
            game = "§e§l"+GoServer.getService().getServerName();
        }

        LabyModAPI.sendCurrentPlayingGamemode(player, visible, game);

        if (GoAPI.getUserAPI().getRankData(player.getUniqueId()).getAccess_level() >= Rank.ADMIN.getAccessLevel()) {
            LabyModAPI.sendSubtitle(player, player.getUniqueId(), "§9Heisenberg");
        }
    }
}