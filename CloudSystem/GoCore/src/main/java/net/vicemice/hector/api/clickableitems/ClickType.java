package net.vicemice.hector.api.clickableitems;

public enum ClickType {
    LEFT_CLICK,
    RIGHT_CLICK;
}