package net.vicemice.hector.server.player.permissions;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.types.player.rank.PlayerRankPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.Rank;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.scoreboard.Team;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class RankManager {
    private HashMap<Player, PermissionAttachment> playerPermissions = new HashMap();

    public void unsetPermissions(Player player) {
        if (this.playerPermissions.containsKey(player)) {
            player.removeAttachment(this.playerPermissions.get(player));
            this.playerPermissions.remove(player);
        }
    }

    public void setPlayerNicked(Player player) {
        try {
            GoServer.getService().getPermissionManager().getScoreboardTeams().get(Rank.MEMBER).addEntry(GoAPI.getUserAPI().getNickName(player.getUniqueId()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (GoServer.getService().getPermissionManager().getSetScoreboard() != null) {
            player.setScoreboard(GoServer.getService().getPermissionManager().getSetScoreboard());
        } else {
            player.setScoreboard(GoServer.getService().getPermissionManager().getRankScoreboard());
        }
    }

    public void setPlayerRank(Player player) {
        PlayerRankPacket playerRankPacket = GoServer.getService().getPermissionManager().getUserRanks().get(player.getUniqueId());
        Rank playerRank = playerRankPacket != null ? playerRankPacket.getRank() : Rank.MEMBER;
        if (playerRank == Rank.ADMIN) {
            player.setOp(true);
        } else {
            player.setOp(false);
        }
        for (Rank rank : GoServer.getService().getPermissionManager().getScoreboardTeams().keySet()) {
            Team team = GoServer.getService().getPermissionManager().getScoreboardTeams().get(rank);
            if (!team.hasEntry(player.getName())) continue;
            team.removeEntry(player.getName());
        }
        try {
            GoServer.getService().getPermissionManager().getScoreboardTeams().get(playerRank).addEntry(player.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (GoServer.getService().getPermissionManager().getSetScoreboard() != null) {
            player.setScoreboard(GoServer.getService().getPermissionManager().getSetScoreboard());
        } else {
            player.setScoreboard(GoServer.getService().getPermissionManager().getRankScoreboard());
        }
    }

    public void setRankPermission(Player player) {
        this.setPlayerRank(player);
        if (!GoServer.getService().getPermissionManager().getUserRanks().containsKey(player.getUniqueId())) {
            return;
        }
        Rank rank = GoServer.getService().getPermissionManager().getUserRanks().get(player.getUniqueId()).getRank();
        if (!GoServer.getService().isSetPermissions()) {
            return;
        }
        if (this.playerPermissions.containsKey(player)) {
            player.removeAttachment(this.playerPermissions.get(player));
            this.playerPermissions.remove(player);
        }
        PlayerRankPacket playerRankPacket = GoServer.getService().getPermissionManager().getUserRanks().get(player.getUniqueId());
        boolean op = (GoServer.getService().getGoAPI().getPrivateServerOwner() != null && player.getUniqueId().equals(GoServer.getService().getGoAPI().getPrivateServerOwner()));
        PermissionAttachment permissionAttachment = player.addAttachment(GoServer.getService());
        Iterator<String> iterator = playerRankPacket.getAddedPermissions().iterator();
        while (iterator.hasNext()) {
            String permission = iterator.next();
            if (permission.equalsIgnoreCase("op")) {
                op = true;
                continue;
            }
            boolean allowed = true;
            if (permission.startsWith("-")) {
                List<String> perm = Arrays.asList(permission.split("-"));
                permission = perm.get(1);
                allowed = false;
            }
            permissionAttachment.setPermission(permission, allowed);
        }

        if (!Rank.isAdmin(rank.getAccessLevel())) {
            permissionAttachment.setPermission("minecraft.command.help", false);
            permissionAttachment.setPermission("minecraft.command.me", false);
            permissionAttachment.setPermission("minecraft.command.tell", false);
            permissionAttachment.setPermission("minecraft.command.tellraw", false);
            permissionAttachment.setPermission("bukkit.command.help", false);
            permissionAttachment.setPermission("bukkit.command.say", false);
            permissionAttachment.setPermission("bukkit.command.tell", false);
            permissionAttachment.setPermission("bukkit.command.plugins", false);
            permissionAttachment.setPermission("bukkit.command.version", false);
        }

        player.setOp(op);

        if (GoServer.getService().getServerName().equalsIgnoreCase("BuildEvent")) {
            permissionAttachment.setPermission("plots.use", true);
            permissionAttachment.setPermission("plots.info", true);
            permissionAttachment.setPermission("plots.claim", true);
            permissionAttachment.setPermission("plots.auto", true);
            permissionAttachment.setPermission("plots.home", true);
            permissionAttachment.setPermission("plots.deny", true);
            permissionAttachment.setPermission("plots.undeny", true);
            permissionAttachment.setPermission("plots.trust", true);
            permissionAttachment.setPermission("plots.untrust", true);
            permissionAttachment.setPermission("plots.remove", true);
            permissionAttachment.setPermission("plots.kick", true);
            permissionAttachment.setPermission("plots.home", true);
            permissionAttachment.setPermission("plots.visit", true);
            permissionAttachment.setPermission("plots.visit.other", true);
            permissionAttachment.setPermission("plots.plot.1", true);
            permissionAttachment.setPermission("headdb.open", true);
            permissionAttachment.setPermission("headdb.phead", true);


            permissionAttachment.setPermission("worldedit.help", true);
            permissionAttachment.setPermission("worldedit.history.undo", true);
            permissionAttachment.setPermission("worldedit.history.redo", true);
            permissionAttachment.setPermission("worldedit.history.undo.self", true);
            permissionAttachment.setPermission("worldedit.history.redo.self", true);
            permissionAttachment.setPermission("worldedit.limit", true);
            permissionAttachment.setPermission("worldedit.fast", true);
            permissionAttachment.setPermission("worldedit.brush.options.mask", true);
            permissionAttachment.setPermission("worldedit.global-mask", true);
            permissionAttachment.setPermission("worldedit.selection.pos", true);
            permissionAttachment.setPermission("worldedit.selection.hpos", true);
            permissionAttachment.setPermission("worldedit.wand", true);
            permissionAttachment.setPermission("worldedit.wand.toggle", true);
            permissionAttachment.setPermission("worldedit.clipboard.copy", true);
            permissionAttachment.setPermission("worldedit.clipboard.cut", true);
            permissionAttachment.setPermission("worldedit.clipboard.paste", true);
            permissionAttachment.setPermission("worldedit.clipboard.rotate", true);
            permissionAttachment.setPermission("worldedit.brush.sphere", true);
            permissionAttachment.setPermission("worldedit.biome.list", true);
            permissionAttachment.setPermission("worldedit.biome.info", true);
            permissionAttachment.setPermission("worldedit.biome.set", true);
            permissionAttachment.setPermission("worldedit.region.set", true);
            permissionAttachment.setPermission("worldedit.region.replace", true);
            permissionAttachment.setPermission("worldedit.region.walls", true);
            permissionAttachment.setPermission("worldedit.region.naturalize", true);
            permissionAttachment.setPermission("worldedit.region.center", true);
            permissionAttachment.setPermission("worldedit.region.smooth", true);

            permissionAttachment.setPermission("voxelsniper.brush.*", true);
            permissionAttachment.setPermission("voxelsniper.goto", true);
            permissionAttachment.setPermission("voxelsniper.sniper", true);
        }

        if (GoServer.getService().getServerName().equalsIgnoreCase("BauServer")) {
            /*
            permissionAttachment.setPermission("plots.use", true);
            permissionAttachment.setPermission("plots.info", true);
            permissionAttachment.setPermission("plots.claim", true);
            permissionAttachment.setPermission("plots.auto", true);
            permissionAttachment.setPermission("plots.home", true);
            permissionAttachment.setPermission("plots.deny", true);
            permissionAttachment.setPermission("plots.trust", true);
            permissionAttachment.setPermission("plots.untrust", true);
            permissionAttachment.setPermission("plots.undeny", true);
            permissionAttachment.setPermission("plots.kick", true);
            permissionAttachment.setPermission("plots.home", true);
            permissionAttachment.setPermission("plots.visit", true);
            permissionAttachment.setPermission("plots.plot.1", true);
             */
            if (rank.isAdmin()) {
                player.setOp(true);
            } else if (rank.isBuilder()) {
                player.setOp(true);
            }
        }
        System.out.println("[RankManager] Calculated " + playerRankPacket.getAddedPermissions() + " permissions for " + player.getName());
        this.playerPermissions.put(player, permissionAttachment);

        if (GoServer.getService().getGoAPI().getClan().get(player.getUniqueId()) != null) {
            //player.getScoreboard().getPlayerTeam(player).setSuffix(" §8[§e" + GoServer.getService().getGoAPI().getClan().get(player.getUniqueId()).getTag() + "§8]");
            //player.setPlayerListName(player.getScoreboard().getPlayerTeam(player).getPrefix() + player.getName() + " §8[§e" + GoServer.getService().getGoAPI().getClan().get(player.getUniqueId()).getTag() + "§8]");
        }

        if (rank == Rank.ADMIN) {
            player.setOp(true);
        }
    }

    public void setAllRankPermissions() {
        for (Player player : Bukkit.getServer().getOnlinePlayers()) {
            this.setRankPermission(player);
        }
    }

    public HashMap<Player, PermissionAttachment> getPlayerPermissions() {
        return this.playerPermissions;
    }
}