package net.vicemice.hector.server.player.permissions.listener;

import net.vicemice.hector.events.PlayerNickEvent;
import net.vicemice.hector.events.PlayerUnnickEvent;
import net.vicemice.hector.server.GoServer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class NickListener implements Listener {

    @EventHandler
    public void onPlayerNick(PlayerNickEvent playerNickEvent) {
        GoServer.getService().getPermissionManager().getRankManager().setPlayerNicked(playerNickEvent.getUser().getBukkitPlayer());
        /*if (playerNickEvent.isCancelled()) {
            playerNickEvent.getPlayer().setPlayerListName("§9" + playerNickEvent.getNickName());
        } else {
            playerNickEvent.getPlayer().setPlayerListName(null);
        }*/
    }

    @EventHandler
    public void onPlayerUnnick(PlayerUnnickEvent playerUnnickEvent) {
        GoServer.getService().getPermissionManager().getRankManager().setPlayerRank(playerUnnickEvent.getUser().getBukkitPlayer());

        /*if (playerUnnickEvent.isCancelled()) {
            Clan clan = GoServer.getService().getGoAPI().getClan().get(playerUnnickEvent.getPlayer().getUniqueId());

            if (clan != null) {
                //playerUnnickEvent.getPlayer().getScoreboard().getPlayerTeam(playerUnnickEvent.getPlayer()).setSuffix(" §8[§e" + clan.getTag() + "§8]");
                playerUnnickEvent.getPlayer().setPlayerListName(playerUnnickEvent.getPlayer().getScoreboard().getPlayerTeam(playerUnnickEvent.getPlayer()).getPrefix()+playerUnnickEvent.getPlayer().getName() + " §8[§e" + clan.getTag() + "§8]");
            } else {
                //playerUnnickEvent.getPlayer().getScoreboard().getPlayerTeam(playerUnnickEvent.getPlayer()).setSuffix("");
                playerUnnickEvent.getPlayer().setPlayerListName(playerUnnickEvent.getPlayer().getScoreboard().getPlayerTeam(playerUnnickEvent.getPlayer()).getPrefix()+playerUnnickEvent.getPlayer().getName());
            }
        } else {
            playerUnnickEvent.getPlayer().setPlayerListName(null);
        }*/
    }
}