package net.vicemice.hector.proxy.commands;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.Abuses;
import net.vicemice.hector.proxy.GoProxy;
import net.vicemice.hector.proxy.util.mongodb.PlayerAPI;
import net.vicemice.hector.utils.CommandType;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.players.punish.Punish;

import java.util.Locale;

public class CommandMSGReplay extends TabableCommand {

    public CommandMSGReplay() {
        super(Rank.MEMBER, "r");
    }

    public void execute(CommandSender commandSender, String[] strings) {
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;
        if (GoProxy.getProxyService().getMuteData().containsKey(proxiedPlayer.getUniqueId())) {
            Abuses.Abuse muteData = GoProxy.getProxyService().getMuteData().get(proxiedPlayer.getUniqueId());
            long length = muteData.getLength();
            Punish punish = GoProxy.getProxyService().getPunishManager().getByKey(muteData.getReason());
            String reason = muteData.getReason();

            if (punish != null) {
                reason = GoAPI.getUserAPI().translate( proxiedPlayer.getUniqueId(), reason.toUpperCase());
            }

            if (length == 0) {
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( proxiedPlayer.getUniqueId(), "mute-perma", reason)));
            } else {
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( proxiedPlayer.getUniqueId(), "mute-temp", reason, GoAPI.getTimeManager().getRemainingTimeString(GoProxy.getLocaleManager(), Locale.forLanguageTag(new PlayerAPI(proxiedPlayer.getUniqueId()).get("locale")), length))));
            }
            return;
        }
        GoProxy.getProxyService().getCommandManager().runCommand(CommandType.R, proxiedPlayer.getName(), strings, commandSender);
    }
}