package net.vicemice.hector.server.player.replay.type;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.types.player.replay.PacketEditReplay;
import net.vicemice.hector.packets.types.player.replay.PacketReplayEntries;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import net.vicemice.hector.utils.TimeManager;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/*
 * Class created at 21:38 - 24.04.2020
 * Copyright (C) elrobtossohn
 */
public class ReplayActionInventory {

    public GUI create(IUser user, PacketReplayEntries.PacketReplayEntry replayEntry, GUI returnGUI) {
        GUI gui = new GUI(user, user.translate("gui-replay-actions"), 3);

        gui.fill(0, 9, user.getUIColor());
        gui.fill(18, 27, user.getUIColor());

        gui.setItem(8, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability((user.getNetworkPlayer().getSettingsPacket().getSetting("ui-color", Integer.class) == 14 ? 1 : 14)).name("§c✗").lore(new ArrayList<>(Arrays.asList(user.translate("gui-return").split("\n")))).build(), e -> {
            returnGUI.open();
        });

        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date(replayEntry.getTimestamp());

        gui.setItem(10, ItemBuilder.create(Material.BOOK).name(user.translate("gui-replay")).lore(new ArrayList<>(Arrays.asList(user.translate("gui-replay-description", dateFormat.format(date), timeFormat.format(date), replayEntry.getGameType()+" "+replayEntry.getMapType(), replayEntry.getMap(), GoAPI.getTimeManager().getTimeFromSeconds(GoServer.getLocaleManager(), user.getLocale(), replayEntry.getGameLength(), new ArrayList<>(Arrays.asList(TimeManager.TimeNames.HOURS, TimeManager.TimeNames.MINUTES, TimeManager.TimeNames.SECONDS)))).split("\n")))).build());

        gui.setItem(12, ItemBuilder.create(Material.ENDER_PEARL).name(user.translate("gui-watch-replay")).lore(new ArrayList<>(Arrays.asList(user.translate(((TimeUnit.DAYS.toMillis(30)+replayEntry.getTimestamp()) < System.currentTimeMillis() ? "gui-description-not-available" : "gui-watch-replay-description")).split("\n")))).build(), e -> {
            user.watchGame(replayEntry.getGameId());
            e.getView().close();
        });

        if (user.getNetworkPlayer().getSavedReplayEntryByGameID(replayEntry.getGameId()) != null) {
            gui.setItem(14, ItemBuilder.create(Material.ENDER_CHEST).name(user.translate("gui-remove-favorite")).lore(new ArrayList<>(Arrays.asList(user.translate(((TimeUnit.DAYS.toMillis(30)+replayEntry.getTimestamp()) < System.currentTimeMillis() ? "gui-description-not-available" : (replayEntry.getPlayers().contains(String.valueOf(user.getUniqueId())) ? "gui-remove-favorite-description" : "gui-remove-favorite-description-not-played"))).split("\n")))).build(), e -> {
                if (replayEntry.getPlayers().contains(String.valueOf(user.getUniqueId()))) {
                    user.editReplayFavorites(replayEntry.getGameId(), PacketEditReplay.EditType.REMOVE_FAVORITE);
                    e.getView().close();
                    return;
                }

                user.playSound(Sound.ANVIL_BREAK);
            });
        } else {
            gui.setItem(14, ItemBuilder.create(Material.ENDER_CHEST).name(user.translate("gui-save-replay")).lore(new ArrayList<>(Arrays.asList(user.translate(((TimeUnit.DAYS.toMillis(30)+replayEntry.getTimestamp()) < System.currentTimeMillis() ? "gui-description-not-available" : (replayEntry.getPlayers().contains(String.valueOf(user.getUniqueId())) ? "gui-save-replay-description" : "gui-save-replay-description-not-played"))).split("\n")))).build(), e -> {
                if (replayEntry.getPlayers().contains(String.valueOf(user.getUniqueId()))) {
                    user.editReplayFavorites(replayEntry.getGameId(), PacketEditReplay.EditType.ADD_FAVORITE);
                    e.getView().close();
                    return;
                }

                user.playSound(Sound.ANVIL_BREAK);
            });
        }

        gui.setItem(16, ItemBuilder.create(Material.BARRIER).name(user.translate("gui-delete-replay")).lore(new ArrayList<>(Arrays.asList(user.translate(((TimeUnit.DAYS.toMillis(30)+replayEntry.getTimestamp()) < System.currentTimeMillis() ? "gui-not-available" : (replayEntry.getPlayers().contains(String.valueOf(user.getUniqueId())) ? "gui-delete-replay-description" : "gui-delete-replay-description-not-played"))).split("\n")))).build(), e -> {
            if (replayEntry.getPlayers().contains(String.valueOf(user.getUniqueId()))) {
                user.editReplayFavorites(replayEntry.getGameId(), PacketEditReplay.EditType.DELETE_REPLAY);
                e.getView().close();
                return;
            }

            user.playSound(Sound.ANVIL_BREAK);
        });



        return gui;
    }
}