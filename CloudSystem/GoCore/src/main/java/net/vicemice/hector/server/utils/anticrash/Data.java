package net.vicemice.hector.server.utils.anticrash;

import net.vicemice.hector.GoAPI;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Data {
    static String lastMsg = "";
    static long lastMsgTime = 0L;

    public static void saop(String s) {
        if (lastMsg.equals(s) && System.currentTimeMillis() - lastMsgTime < 1000L) {
            return;
        }
        lastMsg = s;
        lastMsgTime = System.currentTimeMillis();
        System.out.println("\u00a78\u2503 \u00a76AntiCrash \u00a78\u25cf \u00a77 " + s);
        for (Player pl : Bukkit.getOnlinePlayers()) {
            if (GoAPI.getUserAPI().getRankData(pl.getUniqueId()).getAccess_level() < 100) continue;
            Data.s(pl, s);
        }
    }

    public static void s(CommandSender pl, String s) {
        pl.sendMessage("\u00a78\u2503 \u00a76AntiCrash \u00a78\u25cf \u00a77 " + s);
    }
}