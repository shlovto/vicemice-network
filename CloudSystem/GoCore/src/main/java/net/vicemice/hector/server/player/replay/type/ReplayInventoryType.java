package net.vicemice.hector.server.player.replay.type;

/*
 * Enum created at 05:42 - 18.04.2020
 * Copyright (C) elrobtossohn
 */
public enum ReplayInventoryType {
    FAVORITES,
    NEW_REPLAYS
}
