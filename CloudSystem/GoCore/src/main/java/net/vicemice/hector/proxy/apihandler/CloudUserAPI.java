package net.vicemice.hector.proxy.apihandler;

import net.vicemice.hector.packets.types.player.replay.PacketEditReplay;
import net.vicemice.hector.proxy.GoProxy;
import net.vicemice.hector.proxy.util.mongodb.PlayerAPI;
import net.vicemice.hector.types.Callback;
import net.vicemice.hector.types.UserAPI;
import net.vicemice.hector.utils.PlayerRankData;
import net.vicemice.hector.utils.StatisticPeriod;
import net.vicemice.hector.utils.players.clan.Clan;
import net.vicemice.hector.utils.players.party.Party;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;

/*
 * Class created at 19:33 - 31.03.2020
 * Copyright (C) elrobtossohn
 */
public class CloudUserAPI implements UserAPI {


    @Override
    public void requestParty(UUID uniqueId) {

    }

    @Override
    public Party getParty(UUID uniqueId) {
        return null;
    }

    @Override
    public void getPublicParties(Callback<ArrayList<Party>> callback) {

    }

    @Override
    public void requestClan(UUID uniqueId) {

    }

    @Override
    public Clan getClan(UUID uniqueId) {
        return null;
    }

    @Override
    public PlayerRankData getRankData(UUID uniqueId) {
        return null;
    }

    @Override
    public void addXP(UUID uniqueId, long xp) {

    }

    @Override
    public void removeXP(UUID uniqueId, long xp) {

    }

    @Override
    public void addCoins(UUID uniqueId, long coins) {

    }

    @Override
    public void removeCoins(UUID uniqueId, long coins) {

    }

    @Override
    public void getCoins(UUID uniqueId, Callback<Long> callback) {

    }

    @Override
    public void watchGame(UUID uniqueId, String gameId) {

    }

    @Override
    public void addRecordedGame(UUID uniqueId, String gameId, UUID replayUUID, int replayStartSeconds, long timestamp, long gameLength, ArrayList<String> players, String gameType, String mapType, String map, boolean save) {

    }

    @Override
    public void editReplayFavorites(UUID uniqueId, String gameId, PacketEditReplay.EditType editType) {

    }

    @Override
    public Locale getLocale(UUID uniqueId) {
        PlayerAPI playerAPI = new PlayerAPI(uniqueId);
        if (!playerAPI.playerExists()) return Locale.GERMANY;
        return Locale.forLanguageTag(playerAPI.get("locale"));
    }

    @Override
    public String translate(UUID uniqueId, String string) {
        String message = GoProxy.getLocaleManager().getMessage(this.getLocale(uniqueId), string);
        return message;
    }

    @Override
    public String translate(UUID uniqueId, String string, Object... objects) {
        return MessageFormat.format(GoProxy.getLocaleManager().getMessage(this.getLocale(uniqueId), string), objects);
    }

    @Override
    public void getGameTop(String prefix, String type, int tops, Callback callback) {

    }

    @Override
    public void increaseStatistic(UUID uniqueId, String key, long value) {

    }

    @Override
    public void decreaseStatistic(UUID uniqueId, String key, long value) {

    }

    @Override
    public void setStatistic(UUID uniqueId, String key, long value) {

    }

    @Override
    public Long getPlayerStatistic(UUID uniqueId, String key, StatisticPeriod period) {
        return null;
    }

    @Override
    public Long getPlayerRanking(UUID uniqueId, String key, StatisticPeriod period) {
        return null;
    }

    @Override
    public UUID getUniqueId(UUID uniqueId) {
        return null;
    }

    @Override
    public String getNickName(UUID uniqueId) {
        return null;
    }

    @Override
    public boolean isNicked(UUID uniqueId) {
        return false;
    }
}
