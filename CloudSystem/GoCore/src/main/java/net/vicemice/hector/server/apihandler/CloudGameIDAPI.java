package net.vicemice.hector.server.apihandler;

import net.vicemice.hector.types.Callback;
import net.vicemice.hector.types.GameIDAPI;

import java.util.UUID;

public class CloudGameIDAPI implements GameIDAPI {
    private static UUID gameUID;
    private static String gameID;

    @Override
    public UUID getGameUID() {
        return gameUID;
    }

    @Override
    public String getGameID() {
        return gameID;
    }

    @Override
    public void setGameUID(UUID uid) {
        CloudGameIDAPI.gameUID = uid;
    }

    @Override
    public void setGameID(String gameID) {
        CloudGameIDAPI.gameID = gameID;
    }

    @Override
    public void getGameID(Callback var1) {

    }
}