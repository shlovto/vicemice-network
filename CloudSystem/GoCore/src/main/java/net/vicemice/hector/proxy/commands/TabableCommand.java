package net.vicemice.hector.proxy.commands;

import lombok.Getter;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.vicemice.hector.proxy.GoProxy;
import net.vicemice.hector.utils.Rank;

import java.beans.ConstructorProperties;

public abstract class TabableCommand extends Command {

    @Getter
    private int accessLevel;
    @Getter
    private String[] commands;
    @Getter
    private TabCompleteListener tabListener;

    protected static String[] alias(String[] commands) {
        String[] alias = new String[]{};
        if (commands.length > 1) {
            alias = new String[commands.length - 1];
            System.arraycopy(commands, 1, alias, 0, commands.length - 1);
        }
        return alias;
    }

    public TabableCommand(Rank rank, String... commands) {
        this(rank.getAccessLevel(), commands);
    }

    public TabableCommand(int accessLevel, String... commands) {
        super(commands[0], null, TabableCommand.alias(commands));
        this.accessLevel = accessLevel;
        this.commands = commands;
        this.tabListener = new TabCompleteListener(this);
        //ProxyServer.getInstance().getPluginManager().registerListener(Clyde.getInstance(), this.tabListener);
    }


    public static final class TabCompleteListener implements Listener {

        private final TabableCommand instance;

        @EventHandler
        public void a(TabCompleteEvent e) {
            if (!(e.getSender() instanceof ProxiedPlayer)) {
                return;
            }
            ProxiedPlayer player = (ProxiedPlayer) e.getSender();
            if (GoProxy.getProxyService().getProxyManager().getPlayerRank(player.getUniqueId()).getAccessLevel() < this.instance.getAccessLevel()) {
                return;
            }
            String[] arrstring = this.instance.getCommands();
            int n = arrstring.length;
            int n2 = 0;
            while (n2 < n) {
                String cmd = arrstring[n2];
                if (e.getCursor().indexOf(" ") == -1 && ("/" + cmd.toLowerCase()).startsWith(e.getCursor().toLowerCase()) && !cmd.startsWith("clyde")) {
                    e.getSuggestions().add("/" + cmd);
                }
                ++n2;
            }
        }

        @ConstructorProperties(value = {"instance"})
        public TabCompleteListener(TabableCommand instance) {
            this.instance = instance;
        }
    }

}