package net.vicemice.hector.server.player.afk.listener;

import net.vicemice.hector.server.GoServer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/*
 * Class created at 12:32 - 27.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerMoveListener implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        GoServer.getService().getAfkManager().getAfkSessions().get(player.getUniqueId()).setAfk(false);
    }
}
