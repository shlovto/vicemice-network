package net.vicemice.hector.server.player.nick.listener;

import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.events.ListenerPriority;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.wrappers.PlayerInfoData;
import net.vicemice.hector.api.protocol.wrappers.WrappedChatComponent;
import net.vicemice.hector.api.protocol.wrappers.WrappedGameProfile;
import com.mojang.authlib.GameProfile;
import net.minecraft.server.v1_8_R3.TileEntitySkull;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.player.nick.NickManager;
import net.vicemice.hector.server.player.nick.utils.PlayerNickData;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;

public class PlayerNicknameUpdateListener extends PacketAdapter {

    public PlayerNicknameUpdateListener() {
        super(GoServer.getService(), ListenerPriority.HIGHEST, PacketType.Play.Server.NAMED_ENTITY_SPAWN, PacketType.Play.Server.PLAYER_INFO);
        ProtocolLibrary.getProtocolManager().addPacketListener(this);
    }

    @Override
    public void onPacketSending(PacketEvent event) {
        Player player = event.getPlayer();
        UUID receiverUuid = player.getUniqueId();

        if (event.getPacket().getType() == PacketType.Play.Server.NAMED_ENTITY_SPAWN) {
            UUID uuid = event.getPacket().getSpecificModifier(UUID.class).read(0);
            IUser user = UserManager.getUser(receiverUuid);
            PlayerNickData playerNickData = NickManager.getPlayerData().get(uuid);
            AtomicBoolean isPartyMember = new AtomicBoolean(false);

            if (user.getParty() != null) {
                user.getParty().getMembers().forEach((k,v) -> {
                    if (!k.getUniqueId().equals(uuid)) return;
                    isPartyMember.set(true);
                });
            }

            if (playerNickData != null) {
                GameProfile gameProfile = getGameProfile(player, playerNickData);

                if (gameProfile != null && !uuid.equals(receiverUuid) && user.getRank().isLowerLevel(Rank.MODERATOR) && !isPartyMember.get()) {
                    event.getPacket().getSpecificModifier(UUID.class).write(0, gameProfile.getId());
                }
            }
        } else {
            event.setPacket(event.getPacket().shallowClone());
            List<PlayerInfoData> list = new ArrayList<>();

            for (Object object : event.getPacket().getPlayerInfoDataLists().read(0)) {
                PlayerInfoData playerInfoData = (PlayerInfoData) object;
                UUID uuid = playerInfoData.getProfile().getUUID();
                IUser user = UserManager.getUser(receiverUuid);
                PlayerNickData playerNickData = NickManager.getPlayerData().get(uuid);
                AtomicBoolean isPartyMember = new AtomicBoolean(false);

                if (user.getParty() != null) {
                    user.getParty().getMembers().forEach((k,v) -> {
                        if (!k.getUniqueId().equals(uuid)) return;
                        isPartyMember.set(true);
                    });
                }

                if (playerNickData != null && user.getRank().isLowerLevel(Rank.MODERATOR) && !isPartyMember.get() && !uuid.equals(receiverUuid)) {
                    GameProfile gameProfile = getGameProfile(player, playerNickData);

                    if (gameProfile != null && !uuid.equals(receiverUuid)) {
                        WrappedChatComponent chatComponent = playerInfoData.getDisplayName();

                        if (chatComponent != null) {
                            chatComponent.setJson(chatComponent.getJson().replaceAll(playerInfoData.getProfile().getName(),
                                    gameProfile.getName()));
                        }
                        playerInfoData = new PlayerInfoData(WrappedGameProfile.fromHandle(gameProfile),
                                playerInfoData.getLatency(), playerInfoData.getGameMode(), chatComponent);
                    }
                }
                list.add(playerInfoData);
            }
            event.getPacket().getPlayerInfoDataLists().write(0, list);
        }
    }

    private static GameProfile getGameProfile(Player player, PlayerNickData playerNickData) {
        GameProfile gameProfile;

        if (playerNickData != null) {
            gameProfile = new GameProfile(playerNickData.getUniqueId(), playerNickData.getNickName());
        } else {
            gameProfile = new GameProfile(player.getUniqueId(), player.getName());
        }
        try {
            gameProfile = TileEntitySkull.skinCache.get(gameProfile.getName());
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return gameProfile;
    }
}
