package net.vicemice.hector.proxy.listener;

import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.connection.InitialHandler;
import net.md_5.bungee.connection.LoginResult;
import net.md_5.bungee.event.EventHandler;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.LoginPacket;
import net.vicemice.hector.proxy.GoProxy;
import net.vicemice.hector.proxy.util.LobbyManager;
import net.vicemice.hector.proxy.util.PlayerInfo;
import net.vicemice.hector.proxy.util.PlayerInfoManager;

import java.util.UUID;

public class PostLoginListener implements Listener {

    @EventHandler
    public void onLogin(PostLoginEvent event) {
        UUID uuid = event.getPlayer().getUniqueId();
        String name = event.getPlayer().getName();
        PlayerInfo playerInfo = PlayerInfoManager.getInfo(name, true);
        InitialHandler userHandler = (InitialHandler) event.getPlayer().getPendingConnection();
        String skinValue = null;
        String skinSignature = null;
        if (userHandler.getLoginProfile() != null) {
            LoginResult.Property[] property = userHandler.getLoginProfile().getProperties();
            int n = property.length;
            int n2 = 0;
            while (n2 < n) {
                LoginResult.Property prop = property[n2];
                if (prop.getName().equalsIgnoreCase("textures")) {
                    skinValue = prop.getValue();
                    skinSignature = prop.getSignature();
                    playerInfo.setSkinValue(skinValue);
                    playerInfo.setSkinSignature(skinSignature);
                    break;
                }
                ++n2;
            }
        }
        //TODO:
        LobbyManager.connect(event.getPlayer());
        GoProxy.getProxyService().getNettyClient().getChannel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.LOGIN_PACKET, new LoginPacket(uuid, event.getPlayer().getPendingConnection().getListener().getServerPriority().get(0), name, event.getPlayer().getPendingConnection().getVersion(), event.getPlayer().getAddress().getAddress().getHostAddress(), GoProxy.getProxyService().getProxyConfig().getProxyid(), skinValue, skinSignature)));
        GoProxy.getProxyService().getLabyService().sendPermissions(event.getPlayer());
    }
}