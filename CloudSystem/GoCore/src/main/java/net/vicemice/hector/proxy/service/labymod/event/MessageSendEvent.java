package net.vicemice.hector.proxy.service.labymod.event;

import com.google.gson.JsonElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Event;

/*
 * Class created at 01:37 - 23.04.2020
 * Copyright (C) elrobtossohn
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class MessageSendEvent extends Event {

    private ProxiedPlayer player;
    private String messageKey;
    private JsonElement jsonElement;
    @Setter
    private boolean cancelled;

}