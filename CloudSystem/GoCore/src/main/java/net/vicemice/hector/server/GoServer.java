package net.vicemice.hector.server;

import net.vicemice.hector.api.executor.spigot.SpigotAsyncExecutor;
import net.vicemice.hector.api.executor.spigot.SpigotSyncExecutor;
import net.vicemice.hector.api.protocol.ProtocolLib;
import net.vicemice.hector.api.protocol.ProtocolLibrary;
import net.vicemice.hector.api.protocol.ProtocolManager;
import net.vicemice.hector.api.protocol.events.ListenerPriority;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketContainer;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.api.protocol.reflect.FieldAccessException;
import io.netty.channel.Channel;
import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.api.protocolversion.ViaVersionPlugin;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.server.SpigotServerStop;
import net.vicemice.hector.server.commands.BoardTestCommand;
import net.vicemice.hector.server.commands.TestCommand;
import net.vicemice.hector.server.config.gonet.GoConfig;
import net.vicemice.hector.server.config.gonet.GoConfiguration;
import net.vicemice.hector.server.config.mongodb.MongoConfig;
import net.vicemice.hector.server.config.mongodb.MongoConfiguration;
import net.vicemice.hector.utils.players.api.internal.fakemobs.FakeMobs;
import net.vicemice.hector.server.language.LanguageHandler;
import net.vicemice.hector.server.player.PlayerManager;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.player.afk.AfkManager;
import net.vicemice.hector.server.player.nick.listener.NickPacketListener;
import net.vicemice.hector.server.player.nick.listener.NickTabCompleteListener;
import net.vicemice.hector.server.player.nick.listener.PlayerNicknameUpdateListener;
import net.vicemice.hector.server.player.permissions.PermissionManager;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.server.utils.MongoManager;
import net.vicemice.hector.server.utils.anticrash.listener.PREVENTS_AntiCrash;
import net.vicemice.hector.utils.players.api.sound.SoundListener;
import net.vicemice.hector.utils.players.api.title.TitleListener;
import net.vicemice.hector.server.utils.blockMods.BlockMain;
import net.vicemice.hector.server.utils.labymod.LabyService;
import net.vicemice.hector.service.server.NettyClient;
import net.vicemice.hector.utils.language.LanguageAction;
import net.vicemice.hector.server.apihandler.*;
import net.vicemice.hector.server.listener.FixListener;
import net.vicemice.hector.server.listener.LoginListener;
import net.vicemice.hector.server.listener.PlayerChatTabCompleteListener;
import net.vicemice.hector.server.listener.PlayerListener;
import net.vicemice.hector.server.netty.*;
import net.vicemice.hector.utils.locale.LocaleManager;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.*;

/*
 * Class created at 11:20 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GoServer extends JavaPlugin {

    @Getter
    private static GoServer service;
    @Getter
    private GoAPI goAPI;
    @Getter
    private API api;
    @Getter
    @Setter
    private String serverName;
    @Getter
    private Integer SERVER_VERSION = 0;
    @Getter
    @Setter
    private boolean kickPlayers = false, setPermissions = false;
    @Getter
    private ProtocolManager protocolManager;
    @Getter
    private BlockMain blockMain;
    @Getter
    private LanguageHandler languageHandler;
    @Getter
    @Setter
    private LanguageAction languageAction;
    @Getter
    private PermissionManager permissionManager;
    @Getter
    private long started = System.currentTimeMillis();
    @Getter
    private static LocaleManager localeManager;
    @Getter
    private GoConfig goConfig;
    @Getter
    private MongoConfig mongoConfig;
    @Getter
    private LabyService labyService;
    @Getter
    private MongoManager mongoManager;
    @Getter
    private PlayerManager playerManager;
    @Getter
    private AfkManager afkManager;
    @Getter
    private List<UUID> spectators;
    @Getter
    private FakeMobs fakeMobs;
    @Getter
    private ProtocolLib protocolLib;
    @Getter
    private ViaVersionPlugin viaVersionPlugin;

    @Override
    public void onLoad() {
        service = this;

        this.protocolLib = new ProtocolLib();
        this.protocolLib.load();
        //this.viaVersionPlugin = new ViaVersionPlugin();
        //this.viaVersionPlugin.load();
    }

    @Override
    public void onEnable() {
        this.protocolLib.enable();
        //this.viaVersionPlugin.enable();
        localeManager = LocaleManager.newManager("cloud/locales/core");
        localeManager.addPath("cloud/locales/report");
        localeManager.addPath("cloud/locales/replay");
        this.spectators = new ArrayList<>();
        this.api = new API();
        this.goAPI = new GoAPI();

        System.out.println("  ___ ___                 __                ");
        System.out.println(" /   |   \\   ____   _____/  |_  ___________ ");
        System.out.println("/    ~    \\_/ __ \\_/ ___\\   __\\/  _ \\_  __ \\");
        System.out.println("\\    Y    /\\  ___/\\  \\___|  | (  <_> )  | \\/");
        System.out.println(" \\___|_  /  \\___  >\\___  >__|  \\____/|__|   ");
        System.out.println("       \\/       \\/     \\/                   ");
        System.out.println("Preparing Hector Systems");
        System.out.println("Preparing to start the Hector Server Software (Version " + getDescription().getVersion() + ")");

        this.protocolManager = ProtocolLibrary.getProtocolManager();

        setServerVersion();

        System.out.println("Server Version is: " + getSERVER_VERSION());

        if (getSERVER_VERSION() == 0) {
            System.out.println("Hector does not support this server version!");
            System.exit(0);
        }

        GoConfiguration goConfiguration = new GoConfiguration();
        if (!goConfiguration.exists()) {
            GoConfig config = new GoConfig();
            goConfiguration.writeConfiguration(config);
            System.out.println("GoCore Config File not found and will be created...");
            System.out.println("GoCore Config File was created!");
        }
        goConfiguration.readConfiguration();
        this.goConfig = goConfiguration.getGoConfig();

        this.labyService = new LabyService();
        this.labyService.init();

        MongoConfiguration mongoConfiguration = new MongoConfiguration();
        if (!mongoConfiguration.exists()) {
            MongoConfig config = new MongoConfig();
            mongoConfiguration.writeConfiguration(config);
            System.out.println("MongoDB Config File not found and will be created...");
            System.out.println("MongoDB Config File was created!");
        }
        mongoConfiguration.readConfiguration();
        this.mongoConfig = mongoConfiguration.getMongoConfig();
        this.mongoManager = new MongoManager();
        this.playerManager = new PlayerManager();
        this.afkManager = new AfkManager();
        this.fakeMobs = new FakeMobs();

        this.serverName = goConfig.getNAME();

        if (serverName.contains("DevServer")) {
            ((CraftServer) this.getServer()).getCommandMap().register("test", new TestCommand());
            ((CraftServer) this.getServer()).getCommandMap().register("board", new BoardTestCommand());
        }
        this.permissionManager = new PermissionManager();
        this.getGoAPI().setNettyClient(new NettyClient("91.218.67.206", 1389));
        this.getGoAPI().getNettyClient().startClient();
        Bukkit.getServer().getScheduler().runTaskTimerAsynchronously(this, new AliveSender(), 20, 20);

        //this.getCommand("reports").setExecutor(new CommandReports());

        //PlayerCommandPreProcessListener.init();

        //Fix bugged Plugins
        Bukkit.getServer().getPluginManager().registerEvents(new FixListener(), this);
        //End: Fix bugged Plugins

        Bukkit.getServer().getPluginManager().registerEvents(new PlayerListener(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new LoginListener(), this);
        //Bukkit.getServer().getPluginManager().registerEvents(new PlayerCommandPreprocessListener(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerChatTabCompleteListener(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new NickTabCompleteListener(), this);
        this.permissionManager.enable();
        GoAPI.setUserAPI(new CloudUserAPI());
        GoAPI.setServerAPI(new CloudServerAPI());
        GoAPI.setDatabaseAPI(new CloudDatabaseAPI());
        GoAPI.setLanguageAPI(new CloudLanguageAPI());
        GoAPI.setGameIDAPI(new CloudGameIDAPI());

        this.getGoAPI().getNettyClient().addGetter(this.getPlayerManager());
        this.getGoAPI().getNettyClient().addGetter(new ServerHandler());
        this.getGoAPI().getNettyClient().addGetter(new SignPacketListener());
        this.getGoAPI().getNettyClient().addGetter(new DatabasePacketListener());
        this.getGoAPI().getNettyClient().addGetter(new WrapperApiPacketListener());
        this.getGoAPI().getNettyClient().addGetter(new OwnerPacketListener());
        this.getGoAPI().getNettyClient().addGetter(new ServerHandleListener());
        this.getGoAPI().getNettyClient().addGetter(new SoundListener());
        this.getGoAPI().getNettyClient().addGetter(new TitleListener());
        this.getGoAPI().getNettyClient().addGetter(new UserManager());

        this.languageHandler = new LanguageHandler();
        this.languageHandler.languages = new HashMap<>();
        this.getGoAPI().getNettyClient().addGetter(languageHandler);
        if (this.getGoConfig().getSERVER_GROUP().equalsIgnoreCase("no")) {
            ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(this, ListenerPriority.NORMAL, new net.vicemice.hector.api.protocol.PacketType[]{net.vicemice.hector.api.protocol.PacketType.Play.Client.TAB_COMPLETE}){

                public void onPacketReceiving(PacketEvent event) {
                    if (event.getPacketType() == net.vicemice.hector.api.protocol.PacketType.Play.Client.TAB_COMPLETE) {
                        try {
                            PacketContainer packet = event.getPacket();
                            String message = (packet.getSpecificModifier(String.class).read(0)).toLowerCase();
                            if ((message.startsWith("/") || message.equalsIgnoreCase("/")) && !event.getPlayer().isOp()) {
                                event.setCancelled(true);
                            }
                        }
                        catch (FieldAccessException e) {
                            GoServer.this.getLogger().severe("Couldn't access field.");
                        }
                    }
                }
            });
        }
        if (!this.getGoConfig().getSERVER_GROUP().equalsIgnoreCase("no")) {
            this.setPermissions = true;
        }
        this.getGoAPI().getNettyClient().addGetter(new NickPacketListener());
        //ProtocolListener.setupListener();
        new PlayerNicknameUpdateListener();
        //ProtocolLibrary.getProtocolManager().addPacketListener(new ProtocolListener());

        if (getSERVER_VERSION() < 13) {
            //AntiCrash
            new PREVENTS_AntiCrash();
            //Bukkit.getPluginManager().registerEvents(new PREVENTS_Packets(), this);

            //Block Modifications
            blockMain = new BlockMain();
            blockMain.init(this);
        }

        Bukkit.getScheduler().runTaskTimerAsynchronously(this, () -> {
            if (GoServer.getService().getGoAPI().getPrivateServerOwner() != null) {
                if (Bukkit.getOnlinePlayers().size() == 0) {
                    new SpigotSyncExecutor(getService()).execute(Bukkit::shutdown);
                }
            }
        }, 20L, 20L);
    }

    private void setServerVersion() {
        if (Package.getPackage("net.minecraft.server.v1_8_R3") != null) {
            SERVER_VERSION = 8;
        } else {
            SERVER_VERSION = 0;
        }
    }

    @Override
    public void onDisable() {
        this.protocolLib.disable();
        this.labyService.disable();
        //this.viaVersionPlugin.disable();
        Channel channel = this.getGoAPI().getNettyClient().getChannel();
        if (channel != null && channel.isOpen() && channel.isActive() && this.serverName != null) {
            channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.SPIGOT_SERVER_STOP, new SpigotServerStop(this.getServerName())));
        }
    }

    public File getFile2() {
        return getFile();
    }
}