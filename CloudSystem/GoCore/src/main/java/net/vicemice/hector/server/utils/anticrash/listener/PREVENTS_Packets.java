package net.vicemice.hector.server.utils.anticrash.listener;

import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.anticrash.Data;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

public class PREVENTS_Packets implements Listener {
    ConcurrentHashMap<String, Boolean> lastFall = new ConcurrentHashMap<>();
    ConcurrentHashMap<String, Vector> lastVec = new ConcurrentHashMap<>();

    @EventHandler
    public void move(final PlayerMoveEvent ev) {
        if (!this.lastVec.containsKey(ev.getPlayer().getName())) {
            this.lastVec.put(ev.getPlayer().getName(), ev.getPlayer().getVelocity());
        }
        boolean lastf = false;
        if (this.lastFall.containsKey(ev.getPlayer().getName())) {
            lastf = this.lastFall.get(ev.getPlayer().getName());
        }
        if (-ev.getPlayer().getVelocity().getY() + this.lastVec.get(ev.getPlayer().getName()).getY() > 0.0) {
            this.lastFall.put(ev.getPlayer().getName(), true);
        } else {
            this.lastFall.put(ev.getPlayer().getName(), false);
        }
        if (lastf && !this.lastFall.get(ev.getPlayer().getName()).booleanValue()) {
            Data.s(ev.getPlayer(), "STOP");
            if (ev.getTo().getY() > ev.getFrom().getY()) {
                Data.s(ev.getPlayer(), "Illegal movement.");
                final Location to = ev.getTo().add(0.0, -1.0, 0.0);
                new BukkitRunnable(){

                    public void run() {
                        ev.getPlayer().teleport(to);
                    }
                }.runTask(GoServer.getService());
            }
        }
    }

    @EventHandler
    public void teleport(PlayerTeleportEvent ev) {
        Data.s(ev.getPlayer(), "Teleport " + ev.getCause());
    }
}
