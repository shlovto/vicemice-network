package net.vicemice.hector.server.utils.anticrash.listener;

import net.vicemice.hector.api.protocol.PacketType;
import net.vicemice.hector.api.protocol.events.ListenerPriority;
import net.vicemice.hector.api.protocol.events.PacketAdapter;
import net.vicemice.hector.api.protocol.events.PacketEvent;
import net.vicemice.hector.server.GoServer;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import java.util.concurrent.ConcurrentHashMap;

public class PREVENTS_AntiCrash {
    ConcurrentHashMap<String, Double> lastY = new ConcurrentHashMap<>();
    ConcurrentHashMap<String, Double> lastZ = new ConcurrentHashMap<>();
    String lastSlotName = "";
    long lastSlotTime = System.currentTimeMillis();

    public PREVENTS_AntiCrash() {
        GoServer.getService().getProtocolManager().addPacketListener(new PacketAdapter(GoServer.getService(), ListenerPriority.NORMAL, PacketType.Play.Client.POSITION){

            public void onPacketReceiving(PacketEvent event) {
                if (event.getPacketType() == PacketType.Play.Client.POSITION) {
                    PREVENTS_AntiCrash.this.listenPos(event);
                }
            }
        });
        GoServer.getService().getProtocolManager().addPacketListener(new PacketAdapter(GoServer.getService(), ListenerPriority.NORMAL, PacketType.Play.Client.POSITION_LOOK){

            public void onPacketReceiving(PacketEvent event) {
                if (event.getPacketType() == PacketType.Play.Client.POSITION_LOOK) {
                    PREVENTS_AntiCrash.this.listenPos(event);
                }
            }
        });
        GoServer.getService().getProtocolManager().addPacketListener(new PacketAdapter(GoServer.getService(), ListenerPriority.NORMAL, PacketType.Play.Client.CUSTOM_PAYLOAD){

            public void onPacketReceiving(PacketEvent event) {
                if (event.getPacketType() == PacketType.Play.Client.CUSTOM_PAYLOAD) {
                    PREVENTS_AntiCrash.this.listenChannel(event);
                }
            }
        });
    }

    private void listenPos(PacketEvent ev) {
        double x = ev.getPacket().getDoubles().read(0);
        double y = ev.getPacket().getDoubles().read(1);
        double z = ev.getPacket().getDoubles().read(2);
        if (!this.lastY.containsKey(ev.getPlayer().getName())) {
            this.lastY.put(ev.getPlayer().getName(), y);
        }
        if (!this.lastZ.containsKey(ev.getPlayer().getName())) {
            this.lastZ.put(ev.getPlayer().getName(), z);
        }
        if (y - this.lastY.get(ev.getPlayer().getName()) == 9.0) {
            ev.setCancelled(true);
            this.lastY.remove(ev.getPlayer().getName());
        } else if (z - this.lastZ.get(ev.getPlayer().getName()) == 9.0) {
            ev.setCancelled(true);
            this.lastZ.remove(ev.getPlayer().getName());
        }
        if (x == Double.MIN_VALUE || x == Double.MAX_VALUE || z == Double.MIN_VALUE || z == Double.MAX_VALUE) {
            ev.setCancelled(true);
            ev.getPlayer().kickPlayer("Stop!");
            //GoServer.getServer().getApi().sendPacket(GoServer.getServer().getPacketHelper().preparePacket(de.smochy.packets.PacketType.ANTI_CRASH, new AntiCrashPacket(ev.getPlayer().getUniqueId(), AntiCrash.POSITION)), Api.PacketReceiver.WRAPPER);
        }
    }

    private void listenChannel(PacketEvent ev) {
        String s;
        if (ev == null || ev.getPlayer() == null) {
            return;
        }
        if (this.lastSlotName.equals(ev.getPlayer().getName()) && ((s = ev.getPacket().getStrings().read(0)).toLowerCase().startsWith("mc|bedit") || s.toLowerCase().startsWith("mc|bsign"))) {
            ev.setCancelled(true);
            //GoServer.getServer().getApi().sendPacket(GoServer.getServer().getPacketHelper().preparePacket(de.smochy.packets.PacketType.ANTI_CRASH, new AntiCrashPacket(ev.getPlayer().getUniqueId(), AntiCrash.CUSTOM_PAYLOAD)), Api.PacketReceiver.WRAPPER);
            ev.getPlayer().getInventory().remove(new ItemStack(Material.BOOK));
        }
        this.lastSlotName = ev.getPlayer().getName();
        this.lastSlotTime = System.currentTimeMillis();
    }
}