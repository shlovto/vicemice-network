package net.vicemice.hector.server.player.permissions.util;

import io.netty.channel.Channel;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.rank.PlayerRankPacket;
import net.vicemice.hector.server.GoServer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class SlaveRankListener extends PacketGetter {

    @Override
    public void receivePacket(PacketHolder initPacket) {
        Bukkit.getServer().getScheduler().runTask(GoServer.getService(), () -> {
            if (initPacket.getKey().equals(PacketType.PLAYER_RANK_PACKET.getKey())) {
                PlayerRankPacket playerRankPacket = (PlayerRankPacket) initPacket.getValue();
                GoServer.getService().getPermissionManager().getUserRanks().put(playerRankPacket.getUniqueId(), playerRankPacket);
                System.out.println("[RankManager] Received Rank for " + playerRankPacket.getName() + ": " + playerRankPacket.getRank().name().toUpperCase());
                Player player = Bukkit.getServer().getPlayer(playerRankPacket.getUniqueId());
                if (player == null) {
                    return;
                }
                GoServer.getService().getPermissionManager().getRankManager().unsetPermissions(player);
                GoServer.getService().getPermissionManager().getRankManager().setRankPermission(player);
            }
        });
    }

    @Override
    public void channelActive(Channel channel) {
    }
}