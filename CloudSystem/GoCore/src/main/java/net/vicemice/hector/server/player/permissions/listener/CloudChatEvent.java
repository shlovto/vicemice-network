package net.vicemice.hector.server.player.permissions.listener;

import net.vicemice.hector.utils.players.IUser;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.List;

public class CloudChatEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private IUser user;
    private String message;
    private final String rawMessage;
    private boolean cancelled;
    private List<Player> receiver;

    public CloudChatEvent(IUser user, String message, String rawMessage) {
        this.user = user;
        this.message = message;
        this.cancelled = false;
        this.rawMessage = rawMessage;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public String getRawMessage() {
        return this.rawMessage;
    }

    public IUser getUser() {
        return user;
    }

    public void setUser(IUser user) {
        this.user = user;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isCancelled() {
        return this.cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public List<Player> getReceiver() {
        return this.receiver;
    }

    public void setReceiver(List<Player> receiver) {
        this.receiver = receiver;
    }

    public HandlerList getHandlers() {
        return handlers;
    }
}