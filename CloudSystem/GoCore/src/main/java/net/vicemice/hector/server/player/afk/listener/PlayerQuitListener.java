package net.vicemice.hector.server.player.afk.listener;

import net.vicemice.hector.server.GoServer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/*
 * Class created at 12:21 - 27.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        GoServer.getService().getAfkManager().getAfkSessions().remove(player.getUniqueId());
    }
}