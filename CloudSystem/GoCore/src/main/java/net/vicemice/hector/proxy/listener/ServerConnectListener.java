package net.vicemice.hector.proxy.listener;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.lobby.LobbyJoinPacket;
import net.vicemice.hector.proxy.GoProxy;
import net.vicemice.hector.proxy.util.labymod.LabyModAPI;

public class ServerConnectListener implements Listener {

    @EventHandler
    public void onServerConnect(ServerConnectEvent event) {
        if (event.getTarget().getName().equalsIgnoreCase("lobby") || event.getTarget().getName().equalsIgnoreCase("silent")) {
            event.setCancelled(true);
            if (GoProxy.getProxyService().getProxyManager().isVerify()) {
                LabyModAPI.updateGame(event.getPlayer(), "true", "Verify-01");
            } else {
                LabyModAPI.updateGame(event.getPlayer(), "true", "Fallback-01");
                ProxyServer.getInstance().getScheduler().runAsync(GoProxy.getService(), () -> {
                    if (GoProxy.getProxyService().getNettyClient().getChannel() != null) {
                        GoProxy.getProxyService().getNettyClient().getChannel().writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.LOBBY_JOIN_PACKET, new LobbyJoinPacket(event.getPlayer().getName().toLowerCase())));
                    }
                });
            }
        }
    }
}