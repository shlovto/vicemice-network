package net.vicemice.hector.server.player.afk.listener;

import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.permissions.listener.CloudChatEvent;
import net.vicemice.hector.utils.players.IUser;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/*
 * Class created at 12:33 - 27.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class CloudChatListener implements Listener {

    @EventHandler
    public void onChat(CloudChatEvent event) {
        IUser user = event.getUser();
        if (GoServer.getService().getAfkManager().getAfkSessions().get(user.getUniqueId()).isAfk()) {
            GoServer.getService().getAfkManager().getAfkSessions().get(user.getUniqueId()).setAfk(false);
        }
    }
}
