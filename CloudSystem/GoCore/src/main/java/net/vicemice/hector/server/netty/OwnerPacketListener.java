package net.vicemice.hector.server.netty;

import io.netty.channel.Channel;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.server.PrivateOwnerPacket;
import net.vicemice.hector.server.GoServer;

public class OwnerPacketListener extends PacketGetter {

    @Override
    public void receivePacket(PacketHolder initPacket) {
        if (initPacket.getKey().equals(PacketType.PRIVATE_OWNER.getKey())) {
            PrivateOwnerPacket privateOwnerPacket = (PrivateOwnerPacket) initPacket.getValue();
            GoServer.getService().getGoAPI().setPrivateServerOwner(privateOwnerPacket.getUniqueId());
        }
    }

    @Override
    public void channelActive(Channel channel) {
    }
}