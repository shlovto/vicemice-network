package net.vicemice.hector.proxy.service.labymod.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Cancellable;
import net.md_5.bungee.api.plugin.Event;
import net.vicemice.hector.api.labymod.Permission;
import java.util.EnumMap;
import java.util.Map;

/*
 * Class created at 01:37 - 23.04.2020
 * Copyright (C) elrobtossohn
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class PermissionsSendEvent extends Event implements Cancellable {

    private ProxiedPlayer player;
    private Map<Permission, Boolean> permissions = new EnumMap<>(Permission.class);
    @Setter
    private boolean cancelled;
}