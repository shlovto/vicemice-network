package net.vicemice.hector.api.clickableitems.spigot;

import net.vicemice.hector.api.clickableitems.ClickType;
import net.vicemice.hector.api.clickableitems.ClickableItemManager;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.utils.locale.LocaleManager;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ClickableItemManagerSpigot implements ClickableItemManager<ItemStack, ClickableItemSpigot>, Listener {
    private final LocaleManager localeManager;
    private final Map<String, ClickableItemSpigot> itemMap = new HashMap<String, ClickableItemSpigot>();

    public ClickableItemManagerSpigot(LocaleManager localeManager, Plugin plugin) {
        this.localeManager = localeManager;
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public ItemStack getItem(ClickableItemSpigot item, Locale locale) {
        String displayName = this.localeManager.getMessage(locale, item.getNameTranslationKey(), new Object[0]);
        if (!this.itemMap.containsKey(displayName)) {
            this.itemMap.put(displayName, item);
        }
        ItemStack itemStack = item.getItemStack().clone();
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(displayName);
        if (item.isDescription()) {
            String translatedDescription = this.localeManager.getMessage(locale, item.getDescriptionTranslationKey(), new Object[0]);
            itemMeta.setLore(Arrays.asList(translatedDescription.split("\n")));
        }
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    @EventHandler
    public void onInventoryClick(PlayerInteractEvent event) {
        ClickableItemSpigot item;
        if (event.getItem() != null && event.getItem().hasItemMeta() && event.getItem().getItemMeta().hasDisplayName() && (item = this.itemMap.get(event.getItem().getItemMeta().getDisplayName())) != null) {
            switch (event.getAction()) {
                case RIGHT_CLICK_AIR:
                case RIGHT_CLICK_BLOCK: {
                    item.onClick(UserManager.getUser(event.getPlayer()), ClickType.RIGHT_CLICK);
                    break;
                }
                case LEFT_CLICK_AIR:
                case LEFT_CLICK_BLOCK: {
                    item.onClick(UserManager.getUser(event.getPlayer()), ClickType.LEFT_CLICK);
                }
            }
        }
    }
}