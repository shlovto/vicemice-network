package net.vicemice.hector.server.utils;

import lombok.Getter;
import net.vicemice.hector.types.Callback;

import java.util.concurrent.ConcurrentHashMap;

/*
 * Class created at 12:37 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class TopStatsManager {
    @Getter
    private static ConcurrentHashMap<String, Callback> callbackHashMap = new ConcurrentHashMap<>();
}