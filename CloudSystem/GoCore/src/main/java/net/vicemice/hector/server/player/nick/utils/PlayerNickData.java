package net.vicemice.hector.server.player.nick.utils;

import java.util.UUID;

public class PlayerNickData {
    private String playerName;
    private UUID uniqueId;
    private String nickName;
    private String value;
    private String signature;

    public PlayerNickData(String playerName, UUID uniqueId, String nickName, String value, String signature) {
        this.playerName = playerName;
        this.uniqueId = uniqueId;
        this.nickName = nickName;
        this.value = value;
        this.signature = signature;
    }

    public String getPlayerName() {
        return this.playerName;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public String getNickName() {
        return this.nickName;
    }

    public String getValue() {
        return this.value;
    }

    public String getSignature() {
        return this.signature;
    }
}