package net.vicemice.hector.proxy.util.mongodb;

import net.vicemice.hector.proxy.GoProxy;
import org.bson.Document;
import java.util.UUID;

public class PlayerAPI {
    public String name;
    public UUID uuid;

    public PlayerAPI(String name) {
        this.name = name;
    }

    public PlayerAPI(UUID uuid) {
        this.uuid = uuid;
    }

    public boolean playerExists() {
        try {
            Document document = GoProxy.getProxyService().getMongoManager().getDocument("players", (uuid != null ? new Document("uniqueId", String.valueOf(uuid)) : (name != null ? new Document("name", name) : null)));
            return document.getString("uniqueId") != null;
        } catch (Exception e) {}
        return false;
    }

    public String get(String key) {
        try {
            Document document = GoProxy.getProxyService().getMongoManager().getDocument("players", (uuid != null ? new Document("uniqueId", String.valueOf(uuid)) : (name != null ? new Document("name", name) : null)));

            if (playerExists()) {
                if (document.getString(key) == null) return "german";
                return document.getString(key);
            } else {
                if (key.equals("language")) {
                    return "german";
                }
            }
        } catch (Exception e) {}

        return null;
    }
}