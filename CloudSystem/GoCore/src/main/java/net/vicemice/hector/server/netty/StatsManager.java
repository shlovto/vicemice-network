package net.vicemice.hector.server.netty;

import net.vicemice.hector.packets.types.server.StatSignPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.players.api.internal.SkullChanger;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;

public class StatsManager {
    public static void receiveStatPacket(StatSignPacket statSignPacket) {
        Bukkit.getServer().getScheduler().runTask(GoServer.getService(), () -> {
            for (String[] data : statSignPacket.getSignDatas()) {
                Block headBlock;
                int z;
                int y;
                int x;
                String locationString = data[0];
                String skinString = data[1];
                String[] lines = new String[]{data[2], data[3], data[4], data[5]};
                String[] locationArray = locationString.split(";");
                String[] skinArray = skinString.split(";");
                String worldName = locationArray[0];
                World world = Bukkit.getServer().getWorld(worldName);
                Location signLocation = new Location(world, (double) (x = Integer.valueOf(locationArray[1])), (double) (y = Integer.valueOf(locationArray[2]).intValue()), (double) (z = Integer.valueOf(locationArray[3]).intValue()));
                Block signBlock = signLocation.getBlock();
                if (signBlock.getState() instanceof Sign) {
                    Sign sign = (Sign) signBlock.getState();
                    int current = 0;
                    for (String line : lines) {
                        sign.setLine(current, line);
                        ++current;
                    }
                    sign.update();
                }
                if ((headBlock = signLocation.clone().add(0.0, 1.0, 0.0).getBlock()).getType() != Material.SKULL_ITEM)
                    continue;
                SkullChanger.setBlock(headBlock, skinArray[1], skinArray[0]);
            }
        });
    }
}