package net.vicemice.hector.proxy.util;

import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.proxy.GoProxy;
import net.vicemice.hector.utils.Rank;

import java.util.UUID;

public class ProxyManager {

    @Getter
    @Setter
    private boolean connected = false, maintenance = false, verify = false;
    @Getter
    @Setter
    private String MOTD,
            oldVersionMOTD = "§b§lVice§f§lMice §8× §3§lPvP §f§lNetwork §f[§d1.8§f]\n§8» §cBitte ändere deine Version! §8[§e1.8-1.15§8]",
            oldVersionMOTDEN = "§b§lVice§f§lMice §8× §3§lPvP §f§lNetwork §f[§d1.8§f]\n§8» §cPlease change your Version! §8[§e1.8-1.15§8]",
            maintenanceMOTD = "§b§lVice§f§lMice §8× §3§lPvP §f§lNetwork §f[§d1.8§f]\n§8» §4§lStarting Maintenance System...",
            noCSMOTD = "§b§lVice§f§lMice §8× §3§lPvP §f§lNetwork §f[§d1.8§f]\n§8» §4§lKeine Verbindung zur Cloud",
            noCSMOTDEN = "§b§lVice§f§lMice §8× §3§lPvP §f§lNetwork §f[§d1.8§f]\n§8» §4§lNo connection to the Cloud",
            developerMOTD = "§b§lVice§f§lMice §8× §3§lPvP §f§lNetwork §f[§d1.8§f]\n§8» §b§lDeveloper §7Netzwerk",
            developerMOTDEN = "§b§lVice§f§lMice §8× §3§lPvP §f§lNetwork §f[§d1.8§f]\n§8» §b§lDeveloper §7Network",
            buildMOTD = "§b§lVice§f§lMice §8× §3§lPvP §f§lNetwork §f[§d1.8§f]\n§8» §2§lBuild §7Netzwerk",
            buildMOTDEN = "§b§lVice§f§lMice §8× §3§lPvP §f§lNetwork §f[§d1.8§f]\n§8» §2§lBuild §7Network",
            verifyMOTD = "§b§lVice§f§lMice §8× §3§lPvP §f§lNetwork §f[§d1.8§f]\n§8» §a§lVerify §7Netzwerk",
            verifyMOTDEN = "§b§lVice§f§lMice §8× §3§lPvP §f§lNetwork §f[§d1.8§f]\n§8» §a§lVerify §7Network",
            firstLine = "§b§lVice§f§lMice §8× §3§lPvP §f§lNetwork §f[§d1.8§f]\n";

    public Rank getPlayerRank(UUID uuid) {
        if (GoProxy.getProxyService().getPlayerRanks().containsKey(uuid)) {
            return GoProxy.getProxyService().getPlayerRanks().get(uuid);
        }
        return Rank.MEMBER;
    }
}