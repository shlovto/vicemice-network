package net.vicemice.hector.server.player.afk.listener;

import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.afk.AfkSession;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/*
 * Class created at 12:19 - 27.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        AfkSession newSession = new AfkSession(player);
        GoServer.getService().getAfkManager().getAfkSessions().put(player.getUniqueId(), newSession);
    }
}
