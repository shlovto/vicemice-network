package net.vicemice.hector.api.clickableitems;

import java.util.Locale;

public interface ClickableItemManager<I, C extends ClickableItem> {
    public static final String ITEM_NAME_TRANSLATION_KEY = "clickable-item-%s-name";
    public static final String ITEM_DESCRIPTION_TRANSLATION_KEY = "clickable-item-%s-description";

    public I getItem(C var1, Locale var2);
}

