package net.vicemice.hector.server.utils.labymod.event;

import com.google.gson.JsonElement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/*
 * Class created at 01:45 - 23.04.2020
 * Copyright (C) elrobtossohn
 */
@AllArgsConstructor
@Getter
public class MessageReceiveEvent extends Event {

    @Getter
    private final static HandlerList handlerList = new HandlerList();

    private Player player;
    private String messageKey;
    private JsonElement jsonElement;

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }

}