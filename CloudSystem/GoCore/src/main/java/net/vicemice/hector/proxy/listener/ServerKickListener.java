package net.vicemice.hector.proxy.listener;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ServerKickListener implements Listener {
    @EventHandler
    public void onServerKick(ServerKickEvent event) {
        String reason = new TextComponent(event.getKickReasonComponent()).toPlainText();
        if (reason.toLowerCase().contains("lobby") || reason.toLowerCase().contains("closed") || event.getKickedFrom().getName().startsWith("replay-")) {
            if (event.getKickedFrom().getName().startsWith("replay-")) {
                event.getPlayer().sendMessage(event.getKickReasonComponent());
            }
            event.setCancelServer(ProxyServer.getInstance().getServerInfo("lobby"));
            event.setCancelled(true);
        }
    }
}