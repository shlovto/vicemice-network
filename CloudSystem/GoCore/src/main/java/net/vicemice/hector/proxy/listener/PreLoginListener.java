package net.vicemice.hector.proxy.listener;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.proxy.GoProxy;
import net.vicemice.hector.proxy.util.mongodb.PlayerAPI;
import net.vicemice.hector.utils.language.Language;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public class PreLoginListener implements Listener {

    private List<Long> loginTimes = new CopyOnWriteArrayList<Long>();

    @EventHandler
    public void onPreLogin(PreLoginEvent event) {
        String ip = event.getConnection().getAddress().getAddress().getHostAddress();
        UUID uuid = event.getConnection().getUniqueId();
        PlayerAPI player = new PlayerAPI(uuid);

        Language.LanguageType languageType = Language.LanguageType.byKey(player.get("language"));
        if (event.getConnection().getName().equalsIgnoreCase("Network")) {
            event.setCancelReason(TextComponent.fromLegacyText("§cPlease use a other account to join."));
            //TelegramManager.sendMessage("Versuchter Login mit dem '" + event.getConnection().getName() + "' auf dem Bungeecord '" + net.icecave.bungee.Clyde.getInstance().getBungeeName() + "'  [IP: " + event.getConnection().getAddress().getAddress().getHostAddress() + "]");
            event.setCancelled(true);
            return;
        }
        if (GoProxy.getProxyService().getNettyClient() == null || GoProxy.getProxyService().getNettyClient().getChannel() == null) {
            event.setCancelReason(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( event.getConnection().getUniqueId(), "login-still-starting")));
            event.setCancelled(true);
            return;
        }
        if (event.getConnection().getName().length() < 3 || event.getConnection().getName().length() > 16) {
            event.setCancelReason(TextComponent.fromLegacyText("§cInvalid Minecraft Name."));
            event.setCancelled(true);
            return;
        }

        if (!GoProxy.getProxyService().getProxyManager().isConnected() && GoProxy.getProxyService().getProxySettingsPacket() == null) {
            event.setCancelled(true);
            event.setCancelReason(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( event.getConnection().getUniqueId(), "login-cloud-error")));
            return;
        }

        if (event.getConnection().getVersion() < 47) {
            event.setCancelled(true);
            event.setCancelReason(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( event.getConnection().getUniqueId(), "login-old-version")));
            return;
        }

        ProxyServer.getInstance().getScheduler().runAsync(GoProxy.getService(), () -> {
            int atSecond = 0;
            Iterator<Long> timeIterator = this.loginTimes.iterator();
            while (timeIterator.hasNext()) {
                long time = timeIterator.next();
                long diff = System.currentTimeMillis() - time;
                if (diff <= 1000L) {
                    ++atSecond;
                    continue;
                }
                this.loginTimes.remove(time);
            }
            if (atSecond >= 3) {
                System.out.println("Denied Login for " + event.getConnection().getName() + " [" + atSecond + "]");
                event.setCancelReason(TextComponent.fromLegacyText(GoAPI.getUserAPI().translate( event.getConnection().getUniqueId(), "login-too-many-connecting")));
                event.setCancelled(true);
                return;
            }
            this.loginTimes.add(System.currentTimeMillis());
            int count = 0;
            for (ProxiedPlayer proxiedPlayer : ProxyServer.getInstance().getPlayers()) {
                if (!proxiedPlayer.getPendingConnection().getAddress().getAddress().getHostAddress().equalsIgnoreCase(ip)) continue;
                ++count;
            }
            if (count >= 3) {
                event.setCancelled(true);
                event.setCancelReason(TextComponent.fromLegacyText("§cYou have been banned!"));
                if (!GoProxy.getProxyService().getBannedIPS().contains(ip)) {
                    GoProxy.getProxyService().getBannedIPS().add(ip);
                }
            }
            //event.setPremiumLogin(PlayerInfoManager.getInfo(event.getConnection().getName(), true).isPremium());
        });
    }
}