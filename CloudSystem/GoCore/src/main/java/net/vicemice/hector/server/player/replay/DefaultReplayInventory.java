package net.vicemice.hector.server.player.replay;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.types.player.replay.OpenReplayHistoryPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.replay.type.ReplayInventoryType;
import net.vicemice.hector.server.player.replay.type.ReplayListInventory;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.players.api.gui.GUI;
import net.vicemice.hector.utils.players.api.gui.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;

/*
 * Class created at 05:42 - 18.04.2020
 * Copyright (C) elrobtossohn
 */
public class DefaultReplayInventory {

    public GUI create(IUser user, ReplayInventoryType replayInventoryType, OpenReplayHistoryPacket openReplayHistoryPacket) {
        boolean self = (openReplayHistoryPacket.getTargetName().equalsIgnoreCase(user.getName()));
        GUI gui = user.createGUI("§7Loading...", 6);
        if (replayInventoryType == ReplayInventoryType.FAVORITES) {
            gui = user.createGUI(ChatColor.stripColor(user.translate("gui-replay-favorites")), 6);
        } else if (replayInventoryType == ReplayInventoryType.NEW_REPLAYS) {
            gui = user.createGUI(ChatColor.stripColor(user.translate("gui-replay-new-replays")), 6);
        }

        gui.fill(0, 9, user.getUIColor());
        gui.fill(36, 45, user.getUIColor());

        if (replayInventoryType == ReplayInventoryType.FAVORITES) {
            gui.setItem(36, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability(5).name("§0 ").build());
        } else if (replayInventoryType == ReplayInventoryType.NEW_REPLAYS) {
            gui.setItem(37, ItemBuilder.create(Material.STAINED_GLASS_PANE).durability(5).name("§0 ").build());
        }

        gui.setItem(45, ItemBuilder.create(Material.BOOK_AND_QUILL).name(user.translate("gui-replay-favorites")).lore(new ArrayList<>(Arrays.asList((user.translate((self ? "gui-replay-favorites-description-self" : "gui-replay-favorites-description"), openReplayHistoryPacket.getTargetRank().getRankColor()+openReplayHistoryPacket.getTargetName()).split("\n"))))).build(), e -> {
            new ReplayListInventory().create(user, openReplayHistoryPacket, 1, true).open();
        });
        gui.setItem(46, ItemBuilder.create(Material.EYE_OF_ENDER).name(user.translate("gui-replay-new-replays")).lore(new ArrayList<>(Arrays.asList((user.translate((self ? "gui-replay-new-replays-description-self" : "gui-replay-new-replays-description"), openReplayHistoryPacket.getTargetRank().getRankColor()+openReplayHistoryPacket.getTargetName()).split("\n"))))).build(), e -> {
            new ReplayListInventory().create(user, openReplayHistoryPacket, 1, false).open();
        });

        return gui;
    }
}