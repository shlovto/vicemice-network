package net.vicemice.hector.server.apihandler;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.database.AddPlayerDatabasePacket;
import net.vicemice.hector.packets.types.player.database.GetDatabaseValuePacket;
import net.vicemice.hector.packets.types.player.database.GetPlayerDatabaseValue;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.server.utils.DatabaseManager;
import net.vicemice.hector.types.DatabaseAPI;
import net.vicemice.hector.types.DatabaseCallback;

import java.util.UUID;

public class CloudDatabaseAPI implements DatabaseAPI {

    @Override
    public void savePlayerDatabaseValue(UUID uniqueId, String key, String value) {
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.ADD_PLAYER_DATABASE, new AddPlayerDatabasePacket(uniqueId, key, value)), API.PacketReceiver.SLAVE);
    }

    @Override
    public void getPlayerDatabaseValue(UUID uniqueId, String key, DatabaseCallback databaseCallback) {
        String callbackID = UUID.randomUUID().toString();
        DatabaseManager.getCallbacks().put(callbackID, databaseCallback);
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.GET_PLAYER_DATABASE_VALUE, new GetPlayerDatabaseValue(uniqueId, key, callbackID)), API.PacketReceiver.SLAVE);
    }

    @Override
    public void getDatabaseValue(UUID uniqueId, String database, String playerKey, String key, DatabaseCallback callback) {
        String callbackID = UUID.randomUUID().toString();
        DatabaseManager.getCallbacks().put(callbackID, callback);
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.GETDATABASEVALUE_PACKET, new GetDatabaseValuePacket(uniqueId, database, playerKey, key, callbackID)), API.PacketReceiver.SLAVE);
    }
}