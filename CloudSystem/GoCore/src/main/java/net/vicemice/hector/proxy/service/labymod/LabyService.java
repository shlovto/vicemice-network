package net.vicemice.hector.proxy.service.labymod;

import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import lombok.Getter;
import net.vicemice.hector.api.labymod.LabyModAPI;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.protocol.packet.PluginMessage;
import net.vicemice.hector.api.labymod.Permission;
import net.vicemice.hector.proxy.GoProxy;
import net.vicemice.hector.proxy.service.labymod.event.MessageSendEvent;
import net.vicemice.hector.proxy.service.labymod.event.PermissionsSendEvent;

import java.util.HashMap;
import java.util.Map;

/*
 * Class created at 01:33 - 23.04.2020
 * Copyright (C) elrobtossohn
 */
public class LabyService {

    @Getter
    private LabyModAPI api = new LabyModAPI();
    private static final JsonParser JSON_PARSER = new JsonParser();

    /**
     * Sends the modified permissions to the given player
     *
     * @param player the player the permissions should be sent to
     */
    public void sendPermissions(ProxiedPlayer player) {
        Map<Permission, Boolean> modifiedPermissions = new HashMap<>();

        for (Permission permission : Permission.values()) {
            modifiedPermissions.put(permission, permission.isDefaultEnabled());
        }

        // Calling the Bukkit event
        PermissionsSendEvent sendEvent = new PermissionsSendEvent(player, modifiedPermissions, false);
        GoProxy.getService().getProxy().getPluginManager().callEvent(sendEvent);

        // Sending the packet
        if (!sendEvent.isCancelled())
            player.unsafe().sendPacket(new PluginMessage("LMC", api.getBytesToSend(modifiedPermissions), false));
    }

    /**
     * Sends a JSON server-message to the player
     *
     * @param player          the player the message should be sent to
     * @param messageKey      the message's key
     * @param messageContents the message's contents
     */
    public void sendServerMessage(ProxiedPlayer player, String messageKey, JsonElement messageContents) {
        messageContents = cloneJson(messageContents);

        // Calling the Bukkit event
        MessageSendEvent sendEvent = new MessageSendEvent(player, messageKey, messageContents, false);
        GoProxy.getService().getProxy().getPluginManager().callEvent(sendEvent);

        // Sending the packet
        if (!sendEvent.isCancelled())
            player.unsafe().sendPacket(new PluginMessage("LMC", api.getBytesToSend(messageKey, messageContents.toString()), false));
    }

    /**
     * Clones a JsonElement
     *
     * @param cloneElement the element that should be cloned
     * @return the cloned element
     */
    public JsonElement cloneJson(JsonElement cloneElement) {
        try {
            return JSON_PARSER.parse(cloneElement.toString());
        } catch (JsonParseException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
