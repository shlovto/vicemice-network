package net.vicemice.hector.proxy.service;

import lombok.Data;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.vicemice.hector.packets.Abuses;
import net.vicemice.hector.packets.types.proxy.ProxySettingsPacket;
import net.vicemice.hector.proxy.commands.CommandManager;
import net.vicemice.hector.proxy.nettyclient.AliveScheduler;
import net.vicemice.hector.proxy.service.labymod.LabyService;
import net.vicemice.hector.proxy.service.punish.PunishManager;
import net.vicemice.hector.proxy.util.ProxyManager;
import net.vicemice.hector.proxy.util.TimeOutManager;
import net.vicemice.hector.proxy.util.mongodb.MongoManager;
import net.vicemice.hector.service.server.NettyClient;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.proxy.config.mongo.MongoConfig;
import net.vicemice.hector.proxy.config.mongo.MongoConfiguration;
import net.vicemice.hector.proxy.config.proxy.ProxyConfig;
import net.vicemice.hector.proxy.config.proxy.ProxyConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/*
 * Class created at 15:34 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class ProxyService {

    private String name;
    private ProxySettingsPacket proxySettingsPacket;
    private ProxyConfiguration proxyConfiguration;
    private ProxyConfig proxyConfig;
    private MongoConfiguration mongoConfiguration;
    private MongoConfig mongoConfig;
    private MongoManager mongoManager;
    private ProxyManager proxyManager;
    private PunishManager punishManager;
    private LabyService labyService;
    private int onlinePlayers = 0;
    private List<UUID> whitelistedPlayers = new ArrayList<>(), blacklistedPlayers = new ArrayList<>();
    private ConcurrentHashMap<UUID, Abuses.Abuse> banData = new ConcurrentHashMap<>(), muteData = new ConcurrentHashMap<>();
    private ConcurrentHashMap<ProxiedPlayer, String> playerMessages = new ConcurrentHashMap<>();
    private ConcurrentHashMap<UUID, Rank> playerRanks = new ConcurrentHashMap<>();
    private List<String> bannedIPS = new CopyOnWriteArrayList<>(), allowedChat = new CopyOnWriteArrayList<>(), nameBlackList = new CopyOnWriteArrayList<>(), chatBlocked = new CopyOnWriteArrayList<>(), premiumUsers = new CopyOnWriteArrayList<>();

    private long lastPacketReceive;
    private AliveScheduler aliveScheduler;
    private NettyClient nettyClient;
    private CommandManager commandManager;
    private TimeOutManager timeOutManager;
}