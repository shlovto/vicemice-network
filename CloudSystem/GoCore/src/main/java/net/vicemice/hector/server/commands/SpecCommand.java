package net.vicemice.hector.server.commands;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.utils.Rank;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpecCommand extends Command {

    public SpecCommand() {
        super("spec");
    }

    @Override
    public boolean execute(CommandSender commandSender, String label, String[] args) {
        Player player = (Player) commandSender;

        if (GoAPI.getUserAPI().getRankData(player.getUniqueId()).getAccess_level() < Rank.MODERATOR.getAccessLevel()) {
            player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-no-permission"));
            return true;
        }

        if (args.length == 0) {
            if (GoServer.getService().getSpectators().contains(player.getUniqueId())) {
                player.setAllowFlight(false);
                player.setFlying(false);
                GoServer.getService().getSpectators().remove(player.getUniqueId());
                Bukkit.getOnlinePlayers().forEach(p -> {
                    p.showPlayer(player);
                });
                player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-spec-exit"));
            } else {
                player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-spec-usage"));
            }
            return true;
        }

        Player target = Bukkit.getPlayer(args[0]);
        if (target != null) {
            player.setAllowFlight(false);
            player.setFlying(false);
            GoServer.getService().getSpectators().add(player.getUniqueId());
            Bukkit.getOnlinePlayers().forEach(p -> {
                p.hidePlayer(player);
            });
            player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-spec-enter", (GoAPI.getUserAPI().getRankData(target.getUniqueId()).getRankColor()+target.getName())));
            player.teleport(target);
        } else {
            player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "player-not-exist", GoAPI.getUserAPI().translate( player.getUniqueId(), "prefix")));
        }

        return false;
    }
}