package net.vicemice.hector.proxy.config.language;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import net.vicemice.hector.utils.language.Language;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Paul B. on 27.01.2019.
 */
public class Configuration {
    private File configFile;
    @Getter(AccessLevel.PUBLIC)
    private Config config;
    private Gson gson;

    public Configuration(Language.LanguageType languageType) {
        this.configFile = new File("cloud/language/" + languageType.getKey() + ".json");
        this.gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
    }

    public boolean exists() {
        return configFile.exists();
    }

    public void writeConfiguration() {
        writeConfiguration(config);
    }

    public void writeConfiguration(Config config) {
        try (FileWriter fileWriter = new FileWriter(configFile)) {
            gson.toJson(config, fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readConfiguration() {
        try (FileReader fileReader = new FileReader(configFile)) {
            config = gson.fromJson(fileReader, Config.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}