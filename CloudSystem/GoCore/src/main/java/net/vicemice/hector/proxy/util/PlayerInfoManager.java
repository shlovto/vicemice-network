package net.vicemice.hector.proxy.util;

import java.util.HashMap;
import java.util.Optional;

public class PlayerInfoManager {
    private static HashMap<String, PlayerInfo> infos = new HashMap();

    public static Optional<PlayerInfo> getInfo(String player) {
        return Optional.ofNullable(PlayerInfoManager.getInfo(player.toLowerCase(), false));
    }

    public static PlayerInfo getInfo(String player, boolean _create) {
        PlayerInfo info = infos.get(player.toLowerCase());
        if (info == null && _create) {
            info = new PlayerInfo(player, true);
            infos.put(player.toLowerCase(), info);
        }
        return info;
    }

    public static PlayerInfo getStaticInfo(String player) {
        PlayerInfo info = infos.get(player.toLowerCase());
        if (info == null) {
            info = new PlayerInfo(player, false);
            infos.put(player.toLowerCase(), info);
        }
        if (info.isUnloadAble()) {
            info.setUnloadAble(false);
        }
        return info;
    }

    public static void unloadPlayer(String name) {
        Optional<PlayerInfo> info = PlayerInfoManager.getInfo(name.toLowerCase());
        if (!info.isPresent()) {
            return;
        }
        if (info.get().isUnloadAble()) {
            infos.remove(name.toLowerCase());
            return;
        }
        info.get().reset();
    }

    public static void unloadAll() {
        infos.clear();
    }

    public static int cachedPlayers() {
        return infos.size();
    }
}