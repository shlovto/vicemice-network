package net.vicemice.hector.server.utils;

import lombok.Getter;
import net.vicemice.hector.packets.types.player.database.PlayerDatabasePacket;
import net.vicemice.hector.types.DatabaseCallback;

import java.util.concurrent.ConcurrentHashMap;

/*
 * Class created at 12:19 - 21.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class DatabaseManager {
    @Getter
    private static ConcurrentHashMap<String, DatabaseCallback> callbacks = new ConcurrentHashMap<>();

    public static void receiveDatabasePacket(PlayerDatabasePacket playerDatabasePacket) {
        if (callbacks.containsKey(playerDatabasePacket.getCallBackID())) {
            callbacks.get(playerDatabasePacket.getCallBackID()).done(playerDatabasePacket.getValue());
            callbacks.remove(playerDatabasePacket.getCallBackID());
        }
    }
}