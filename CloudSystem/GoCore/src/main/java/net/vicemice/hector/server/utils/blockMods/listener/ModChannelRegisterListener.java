package net.vicemice.hector.server.utils.blockMods.listener;

import net.vicemice.hector.server.GoServer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChannelEvent;
import org.bukkit.event.player.PlayerRegisterChannelEvent;

public class ModChannelRegisterListener implements Listener {

    @EventHandler
    public void handlePlayerRegisterChannel(PlayerRegisterChannelEvent event) {
        GoServer.getService().getBlockMain().checkChannel(event.getPlayer().getUniqueId(), event.getChannel());
    }

    @EventHandler
    public void handlePlayerChannel(PlayerChannelEvent event) {
        GoServer.getService().getBlockMain().checkChannel(event.getPlayer().getUniqueId(), event.getChannel());
    }
}