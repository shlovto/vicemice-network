package net.vicemice.hector.server.player;

import io.netty.channel.Channel;
import lombok.Getter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.events.UserLoginEvent;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.player.user.RequestUserDataPacket;
import net.vicemice.hector.packets.types.player.user.UserPreLoginPacket;
import net.vicemice.hector.packets.types.player.user.UserUpdatePacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.utils.User;
import net.vicemice.hector.server.player.utils.UserQueue;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.utils.players.IUser;
import net.vicemice.hector.utils.scheduler.ScheduledExecution;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/*
 * Class created at 20:45 - 02.05.2020
 * Copyright (C) elrobtossohn
 */
public class UserManager extends PacketGetter {

    @Getter
    public static ConcurrentMap<UUID, IUser> connectedUsers = new ConcurrentHashMap<>();

    @Getter
    public static ConcurrentMap<UUID, IUser.Action> whenConnecting = new ConcurrentHashMap<>();

    @Getter
    public static UserQueue userQueue;

    public UserManager() {
        userQueue = new UserQueue();
        userQueue.start();

        new ScheduledExecution(() -> {
            for (UUID uniqueId : whenConnecting.keySet()) {
                if (Bukkit.getPlayer(uniqueId) != null) {
                    whenConnecting.get(uniqueId).done();
                    if (!getUser(uniqueId).isOnline()) {
                        getUser(uniqueId).setOnline(true);
                        Bukkit.getPluginManager().callEvent(new UserLoginEvent(getUser(uniqueId)));
                    }
                }
            }
        }, 0, 1, TimeUnit.SECONDS);

        Bukkit.getPluginManager().registerEvents(new Listener() {
            @EventHandler
            public void onQuit(PlayerQuitEvent event) {
                event.setQuitMessage(null);
                System.out.println("[UserManager] User disconnected: " + event.getPlayer().getName());
                connectedUsers.remove(event.getPlayer().getUniqueId());
            }
        }, GoServer.getService());
    }

    public static void join(Player player) {
        if (getConnectedUsers().containsKey(player.getUniqueId())) {
            if (!getUser(player).isOnline()) {
                getUser(player).setOnline(true);
                Bukkit.getPluginManager().callEvent(new UserLoginEvent(getUser(player)));
            }
        } else {
            GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.REQUEST_USER_DATA, new RequestUserDataPacket(player.getUniqueId(), GoServer.getService().getServerName())), API.PacketReceiver.SLAVE);
        }
    }

    public static boolean isConnected(UUID uniqueId) {
        return connectedUsers.containsKey(uniqueId);
    }

    public static boolean isConnected(Player player) {
        return isConnected(player.getUniqueId());
    }

    public static IUser getUser(UUID uniqueId) {
        if (!getConnectedUsers().containsKey(uniqueId)) throw new NullPointerException("The user with the uniqueId "+uniqueId+" is not online, why we should get an nulled user?");
        return connectedUsers.get(uniqueId);
    }

    public static IUser getUser(Player player) {
        return getUser(player.getUniqueId());
    }

    @Override
    public void receivePacket(PacketHolder packetHolder) {
        Bukkit.getServer().getScheduler().runTask(GoServer.getService(), () -> {
            if (packetHolder.getKey().equals(PacketType.USER_SERVER_PRE_CONNECT.getKey())) {
                UserPreLoginPacket userPreLoginPacket = (UserPreLoginPacket) packetHolder.getValue();
                System.out.println("[UserManager] Received Pre-Login for " + userPreLoginPacket.getName());
                connectedUsers.put(userPreLoginPacket.getUniqueId(), new User(userPreLoginPacket.getName(), userPreLoginPacket.getUniqueId(), userPreLoginPacket.getRank(), userPreLoginPacket.getLocale(), (Bukkit.getPlayer(userPreLoginPacket.getUniqueId()) != null && Bukkit.getPlayer(userPreLoginPacket.getUniqueId()).isOnline()), userPreLoginPacket.getCoins(), userPreLoginPacket.getXp(), userPreLoginPacket.getParty(), userPreLoginPacket.getClan()));
                System.out.println("[UserManager] Successfully register " + userPreLoginPacket.getName());

                if (Bukkit.getPlayer(userPreLoginPacket.getUniqueId()) != null && Bukkit.getPlayer(userPreLoginPacket.getUniqueId()).isOnline()) {
                    Bukkit.getPluginManager().callEvent(new UserLoginEvent(getUser(Bukkit.getPlayer(userPreLoginPacket.getUniqueId()))));
                }
            }
            if (packetHolder.getKey().equals(PacketType.USER_SERVER_UPDATE.getKey())) {
                UserUpdatePacket userUpdatePacket = (UserUpdatePacket) packetHolder.getValue();
                if (connectedUsers.containsKey(userUpdatePacket.getUniqueId())) {
                    System.out.println("[UserManager] User updated: " + userUpdatePacket.getName());
                    connectedUsers.get(userUpdatePacket.getUniqueId()).update(userUpdatePacket);
                } else {
                    System.out.println("[UserManager] Received Update for " + userUpdatePacket.getName());
                    connectedUsers.put(userUpdatePacket.getUniqueId(), new User(userUpdatePacket.getName(), userUpdatePacket.getUniqueId(), userUpdatePacket.getRank(), userUpdatePacket.getLocale(), (Bukkit.getPlayer(userUpdatePacket.getUniqueId()) != null && Bukkit.getPlayer(userUpdatePacket.getUniqueId()).isOnline()), userUpdatePacket.getCoins(), userUpdatePacket.getXp(), userUpdatePacket.getParty(), userUpdatePacket.getClan()));
                    System.out.println("[UserManager] Successfully update " + userUpdatePacket.getName());
                }
            }
        });
    }

    @Override
    public void channelActive(Channel channel) {

    }
}