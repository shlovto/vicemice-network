package net.vicemice.hector.server.commands;

import net.vicemice.hector.GoAPI;
import net.vicemice.hector.utils.Rank;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/*
 * Class created at 19:12 - 16.04.2020
 * Copyright (C) elrobtossohn
 */
public class TestCommand extends Command {

    public TestCommand() {
        super("test");
    }

    @Override
    public boolean execute(CommandSender commandSender, String label, String[] args) {
        Player player = (Player) commandSender;

        if (GoAPI.getUserAPI().getRankData(player.getUniqueId()).getAccess_level() < Rank.ADMIN.getAccessLevel()) {
            player.sendMessage(GoAPI.getUserAPI().translate( player.getUniqueId(), "command-no-permission"));
            return true;
        }

        player.sendMessage("§cThere are no test module.");

        return false;
    }
}
