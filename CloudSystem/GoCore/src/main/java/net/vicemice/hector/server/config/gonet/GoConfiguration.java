package net.vicemice.hector.server.config.gonet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.AccessLevel;
import lombok.Getter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/*
 * Class created at 23:42 - 04.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GoConfiguration {
    private File configFile;
    @Getter(AccessLevel.PUBLIC)
    private GoConfig goConfig;
    private Gson gson;

    public GoConfiguration() {
        this.configFile = new File("cloud/config.json");
        this.gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
    }

    public boolean exists() {
        return configFile.exists();
    }

    public void writeConfiguration() {
        writeConfiguration(goConfig);
    }

    public void writeConfiguration(GoConfig goConfig) {
        try (FileWriter fileWriter = new FileWriter(configFile)) {
            gson.toJson(goConfig, fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readConfiguration() {
        try (FileReader fileReader = new FileReader(configFile)) {
            goConfig = gson.fromJson(fileReader, GoConfig.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}