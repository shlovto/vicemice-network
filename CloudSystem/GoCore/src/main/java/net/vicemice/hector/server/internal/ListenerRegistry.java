package net.vicemice.hector.server.internal;

import net.vicemice.hector.backend.PacketGetter;

public class ListenerRegistry {
    public static void addPacketListener(PacketGetter packetGetter, NettyServer nettyServer) {
        InternalAPI.addPacketListener(packetGetter, nettyServer);
    }

    public static enum NettyServer {
        LOBBY,
        WRAPPER;

        private NettyServer() {
        }
    }
}