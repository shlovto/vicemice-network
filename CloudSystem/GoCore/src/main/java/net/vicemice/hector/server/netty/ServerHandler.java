package net.vicemice.hector.server.netty;

import io.netty.channel.Channel;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.backend.PacketGetter;
import net.vicemice.hector.events.ServerUpdateLocaleEvent;
import net.vicemice.hector.gameapi.manager.GameManager;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.ErrorPacket;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.ServerCallBackInfos;
import net.vicemice.hector.packets.types.UpdateLocalePacket;
import net.vicemice.hector.packets.types.player.replay.OpenReplayHistoryPacket;
import net.vicemice.hector.packets.types.player.replay.SaveInReplayHistoryPacket;
import net.vicemice.hector.packets.types.player.report.ReportInventoryPacket;
import net.vicemice.hector.packets.types.server.CloudServerCallBack;
import net.vicemice.hector.packets.types.server.GameIDPacket;
import net.vicemice.hector.packets.types.server.ServerPacket;
import net.vicemice.hector.packets.types.server.software.RunCommandPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.commands.SpecCommand;
import net.vicemice.hector.server.player.UserManager;
import net.vicemice.hector.server.player.replay.type.ReplayListInventory;
import net.vicemice.hector.server.player.report.ReportInventory;
import net.vicemice.hector.types.ServerState;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;

import java.util.logging.Level;

public class ServerHandler extends PacketGetter {
    Channel channel;

    @Override
    public void receivePacket(PacketHolder initPacket) {
        if (initPacket.getKey().equals(PacketType.GAME_ID.getKey())) {
            GameIDPacket gameIDPacket = (GameIDPacket) initPacket.getValue();

            GoAPI.getGameIDAPI().setGameUID(gameIDPacket.getGameUID());
            GoAPI.getGameIDAPI().setGameID(gameIDPacket.getGameID());
        } else if (initPacket.getKey().equals(PacketType.UPDATE_LOCALE.getKey())) {
            UpdateLocalePacket updateLocalePacket = (UpdateLocalePacket) initPacket.getValue();

            Bukkit.getLogger().log(Level.INFO, "Receive Locale Update Packet at " + GoAPI.getTimeManager().getTime("dd.MM.yyyy - HH:mm", updateLocalePacket.getMillis()));
            Bukkit.getPluginManager().callEvent(new ServerUpdateLocaleEvent());
        } else if (initPacket.getKey().equals(PacketType.RUN_COMMAND.getKey())) {
            RunCommandPacket runCommandPacket = (RunCommandPacket) initPacket.getValue();

            if (runCommandPacket.getType() == RunCommandPacket.Type.CONSOLE) {
                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), runCommandPacket.getCommand());
            } else if (runCommandPacket.getType() == RunCommandPacket.Type.PLAYER && Bukkit.getPlayer(runCommandPacket.getUniqueId()) != null) {
                Bukkit.dispatchCommand(Bukkit.getPlayer(runCommandPacket.getUniqueId()), runCommandPacket.getCommand());
            }
        } else if (initPacket.getKey().equals(PacketType.REPORT_INVENTORY_PACKET.getKey())) {
            ReportInventoryPacket reportInventoryPacket = (ReportInventoryPacket) initPacket.getValue();

            if (Bukkit.getPlayer(reportInventoryPacket.getReporter()) != null) {
                new ReportInventory().create(UserManager.getUser(reportInventoryPacket.getReporter()), reportInventoryPacket.getReportPlayer(), null, null, false).open();
            }
        } else if (initPacket.getKey().equals(PacketType.SAVE_IN_REPLAY_HISTORY.getKey())) {
            SaveInReplayHistoryPacket saveInReplayHistoryPacket = (SaveInReplayHistoryPacket) initPacket.getValue();

            if (Bukkit.getPlayer(saveInReplayHistoryPacket.getRequester()) != null) {
                GameManager.getSaveReplayPlayers().add(saveInReplayHistoryPacket.getRequester());
            }
        } else if (initPacket.getKey().equals(PacketType.OPEN_REPLAY_HISTORY.getKey())) {
            OpenReplayHistoryPacket openReplayHistoryPacket = (OpenReplayHistoryPacket) initPacket.getValue();

            if (Bukkit.getPlayer(openReplayHistoryPacket.getRequester()) != null) {
                new ReplayListInventory().create(UserManager.getUser(openReplayHistoryPacket.getRequester()), openReplayHistoryPacket, 1, true).open();
                System.out.println("open ORH!");
            }
            System.out.println("could not be open ORH!");
        } else if (initPacket.getKey().equals(PacketType.CLAN_INFORMATION.getKey())) {
            /*ClanInformationPacket clanPacket = (ClanInformationPacket) initPacket.getValue();

            HashMap<PlayerInfo, ClanRank> members = new HashMap<>();

            if (clanPacket.getMembers() != null && !clanPacket.getMembers().isEmpty()) {
                for (String s : clanPacket.getMembers().split(";")) {
                    String[] ss = s.split(":");
                    PlayerInfo playerInfo = new PlayerInfo(ss[0], UUID.fromString(ss[1]), Rank.valueOf(ss[2]), ss[3], ss[4], Boolean.valueOf(ss[5]));
                    ClanRank clanRank = ClanRank.valueOf(ss[6]);

                    members.put(playerInfo, clanRank);
                }
            }

            Clan clan = null;

            if (clanPacket.getId() != null) {
                clan = new Clan(clanPacket.getId(), clanPacket.getUuid1(), clanPacket.getName(), clanPacket.getTag(), members);
            }

            if (clan != null) {
                if (GoServer.getService().getGoAPI().getClan().containsKey(clanPacket.getUniqueId())) {
                    GoServer.getService().getGoAPI().getClan().remove(clanPacket.getUniqueId());
                }
                GoServer.getService().getGoAPI().getClan().put(clanPacket.getUniqueId(), clan);
                Bukkit.getPlayer(clanPacket.getUniqueId()).setPlayerListName(Bukkit.getPlayer(clanPacket.getUniqueId()).getScoreboard().getPlayerTeam(Bukkit.getPlayer(clanPacket.getUniqueId())).getPrefix() + Bukkit.getPlayer(clanPacket.getUniqueId()).getName() + " §8[§e" + clan.getTag() + "§8]");
                //Bukkit.getPlayer(clanPacket.getUniqueId()).getScoreboard().getPlayerTeam(Bukkit.getPlayer(clanPacket.getUniqueId())).setSuffix(" §8[§e" + clan.getTag() + "§8]");
            } else {
                if (GoServer.getService().getGoAPI().getClan().containsKey(clanPacket.getUniqueId())) {
                    GoServer.getService().getGoAPI().getClan().remove(clanPacket.getUniqueId());
                }
                //Bukkit.getPlayer(clanPacket.getUniqueId()).getScoreboard().getPlayerTeam(Bukkit.getPlayer(clanPacket.getUniqueId())).setSuffix("");
                Bukkit.getPlayer(clanPacket.getUniqueId()).setPlayerListName(Bukkit.getPlayer(clanPacket.getUniqueId()).getScoreboard().getPlayerTeam(Bukkit.getPlayer(clanPacket.getUniqueId())).getPrefix() + Bukkit.getPlayer(clanPacket.getUniqueId()).getName());
            }*/
        } else if (initPacket.getKey().equals(PacketType.ERROR_PACKET.getKey())) {
            ErrorPacket errorPacket = (ErrorPacket) initPacket.getValue();
            System.out.println(errorPacket.getErrorMessage());
        } else if (initPacket.getKey().equals(PacketType.SERVER_CALLBACK_INFOS.getKey())) {
            String setGroup = null;
            if (!GoServer.getService().getGoConfig().getSERVER_GROUP().equals("no")) {
                setGroup = GoServer.getService().getGoConfig().getSERVER_GROUP();
            }
            ServerCallBackInfos serverCallBackInfos = (ServerCallBackInfos) initPacket.getValue();
            GoServer.getService().setServerName(serverCallBackInfos.getName());

            if (!serverCallBackInfos.getName().startsWith("Replay")) {
                ((CraftServer) GoServer.getService().getServer()).getCommandMap().register("spec", new SpecCommand());
            }

            ServerState useState = ServerState.STARTUP;
            if (GoServer.getService().getGoConfig().isAUTO_LOBBY_STATE()) {
                useState = ServerState.LOBBY;
            }
            ServerPacket changeState = new ServerPacket(serverCallBackInfos.getName(), null, 0, "USER", GoServer.getService().getGoConfig().getVERSION(), useState, false, GoServer.getService().getGoConfig().getMOTD(), GoServer.getService().getGoConfig().getMAX_PLAYERS(), setGroup);
            this.channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.SERVER_PACKET, changeState));
        }
    }

    @Override
    public void channelActive(Channel channel) {
        this.channel = channel;
        String setGroup = null;
        if (!GoServer.getService().getGoConfig().getSERVER_GROUP().equals("no")) {
            setGroup = GoServer.getService().getGoConfig().getSERVER_GROUP();
        }
        if (GoServer.getService().getGoConfig().isMANUAL_SERVER()) {
            ServerState useState = ServerState.STARTUP;
            if (GoServer.getService().getGoConfig().isAUTO_LOBBY_STATE()) {
                useState = ServerState.LOBBY;
            }
            PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.SERVER_PACKET, new ServerPacket(GoServer.getService().getGoConfig().getNAME(), GoServer.getService().getGoConfig().getHOST(), GoServer.getService().getGoConfig().getPORT(), "PLAYER", GoServer.getService().getGoConfig().getVERSION(), useState, true, GoServer.getService().getGoConfig().getMOTD(), GoServer.getService().getGoConfig().getMAX_PLAYERS(), setGroup));
            channel.writeAndFlush(initPacket);
        } else {
            channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.CLOUD_START_CALLBACK, new CloudServerCallBack(Bukkit.getServer().getPort())));
        }
    }
}