package net.vicemice.hector.proxy.util.labymod;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.netty.channel.Channel;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.labymod.LabyResponsePacket;
import net.vicemice.hector.proxy.GoProxy;
import net.vicemice.hector.proxy.service.labymod.event.MessageReceiveEvent;

public class LabyResponse implements Listener {

    @EventHandler
    public void onMessageReceive(MessageReceiveEvent event) {
        ProxiedPlayer player = event.getPlayer();
        JsonElement jsonElement = event.getJsonElement();

        if (event.getMessageKey().equalsIgnoreCase("discord_rpc")) {
            JsonObject rpc = jsonElement.getAsJsonObject();

            if(rpc.get("joinSecret") != null) {
                Channel channel;
                if (GoProxy.getProxyService().getNettyClient().getChannel() != null && (channel = GoProxy.getProxyService().getNettyClient().getChannel()).isActive() && channel.isOpen()) {
                    channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.LABYRESPONSE_PACKET, new LabyResponsePacket(player.getUniqueId(), "joinSecret", rpc.get("joinSecret").getAsString())));
                }
            } else if(rpc.get("spectateSecret") != null) {
                Channel channel;
                if (GoProxy.getProxyService().getNettyClient().getChannel() != null && (channel = GoProxy.getProxyService().getNettyClient().getChannel()).isActive() && channel.isOpen()) {
                    channel.writeAndFlush(GoAPI.getPacketHelper().preparePacket(PacketType.LABYRESPONSE_PACKET, new LabyResponsePacket(player.getUniqueId(), "spectateSecret", rpc.get("spectateSecret").getAsString())));
                }
            }
        }
    }
}