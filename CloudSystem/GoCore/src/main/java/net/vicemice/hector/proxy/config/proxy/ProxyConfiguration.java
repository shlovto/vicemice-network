package net.vicemice.hector.proxy.config.proxy;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.AccessLevel;
import lombok.Getter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Paul B. on 27.01.2019.
 */
public class ProxyConfiguration {
    private File configFile;
    @Getter(AccessLevel.PUBLIC)
    private ProxyConfig proxyConfig;
    private Gson gson;

    public ProxyConfiguration() {
        this.configFile = new File("cloud/proxy.json");
        this.gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
    }

    public boolean exists() {
        return configFile.exists();
    }

    public void writeConfiguration() {
        writeConfiguration(proxyConfig);
    }

    public void writeConfiguration(ProxyConfig proxyConfig) {
        try (FileWriter fileWriter = new FileWriter(configFile)) {
            gson.toJson(proxyConfig, fileWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readConfiguration() {
        try (FileReader fileReader = new FileReader(configFile)) {
            proxyConfig = gson.fromJson(fileReader, ProxyConfig.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}