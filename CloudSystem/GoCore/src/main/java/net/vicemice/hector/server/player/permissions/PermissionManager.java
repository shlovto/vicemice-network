package net.vicemice.hector.server.player.permissions;

import net.vicemice.hector.packets.types.player.rank.PlayerRankPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.player.permissions.util.SlaveRankListener;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.server.player.permissions.listener.NickListener;
import net.vicemice.hector.server.player.permissions.listener.PlayerChatListener;
import net.vicemice.hector.server.player.permissions.listener.PlayerJoinListener;
import net.vicemice.hector.server.player.permissions.listener.PlayerQuitListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import java.util.HashMap;
import java.util.UUID;

public class PermissionManager {
    private HashMap<UUID, PlayerRankPacket> userRanks = new HashMap<>();
    private PlayerJoinListener playerJoinListener;
    private PlayerQuitListener playerQuitListener;
    private PlayerChatListener playerChatListener;
    private RankManager rankManager;
    private HashMap<Rank, Team> scoreboardTeams = new HashMap<>();
    private Scoreboard rankScoreboard;
    private Scoreboard setScoreboard;

    public void enable() {
        this.rankManager = new RankManager();
        this.playerJoinListener = new PlayerJoinListener();
        this.playerQuitListener = new PlayerQuitListener();
        this.playerChatListener = new PlayerChatListener();
        this.rankScoreboard = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
        PluginManager pluginManager = Bukkit.getServer().getPluginManager();
        pluginManager.registerEvents(this.playerJoinListener, GoServer.getService());
        pluginManager.registerEvents(this.playerQuitListener, GoServer.getService());
        pluginManager.registerEvents(this.playerChatListener, GoServer.getService());
        pluginManager.registerEvents(new NickListener(), GoServer.getService());
        GoServer.getService().getGoAPI().getNettyClient().addGetter(new SlaveRankListener());
        for (Rank rank : Rank.values()) {
            try {
                Team team = this.getRankScoreboard().registerNewTeam(rank.getScoreboardName());
                if (rank.isRankTabList()) {
                    team.setPrefix(rank.getRankColor() + rank.getShortName() + " §7| " + rank.getRankColor());
                } else {
                    team.setPrefix(rank.getRankColor());
                }
                this.scoreboardTeams.put(rank, team);
                continue;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public HashMap<UUID, PlayerRankPacket> getUserRanks() {
        return this.userRanks;
    }

    public PlayerJoinListener getPlayerJoinListener() {
        return this.playerJoinListener;
    }

    public PlayerQuitListener getPlayerQuitListener() {
        return this.playerQuitListener;
    }

    public PlayerChatListener getPlayerChatListener() {
        return this.playerChatListener;
    }

    public RankManager getRankManager() {
        return this.rankManager;
    }

    public HashMap<Rank, Team> getScoreboardTeams() {
        return this.scoreboardTeams;
    }

    public Scoreboard getRankScoreboard() {
        return this.rankScoreboard;
    }

    public Scoreboard getSetScoreboard() {
        return this.setScoreboard;
    }

    public void setSetScoreboard(Scoreboard setScoreboard) {
        this.setScoreboard = setScoreboard;
    }
}