package net.vicemice.hector.server.utils.labymod.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.vicemice.hector.api.labymod.Permission;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import java.util.EnumMap;
import java.util.Map;

/*
 * Class created at 01:46 - 23.04.2020
 * Copyright (C) elrobtossohn
 */
@AllArgsConstructor
@Getter
public class PermissionsSendEvent extends Event implements Cancellable {

    @Getter
    private final static HandlerList handlerList = new HandlerList();

    private Player player;
    private Map<Permission, Boolean> permissions = new EnumMap<>(Permission.class);
    @Setter
    private boolean cancelled;

    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
}