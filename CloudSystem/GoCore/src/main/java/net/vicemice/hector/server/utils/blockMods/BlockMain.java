package net.vicemice.hector.server.utils.blockMods;

import com.google.common.collect.Sets;
import lombok.Getter;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.abuse.BlockModificationsPacket;
import net.vicemice.hector.server.GoServer;
import net.vicemice.hector.server.utils.API;
import net.vicemice.hector.server.utils.blockMods.listener.ModChannelRegisterListener;
import net.vicemice.hector.utils.Modifications;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.messaging.Messenger;
import org.bukkit.plugin.messaging.PluginMessageListener;
import java.util.Arrays;
import java.util.Set;
import java.util.UUID;

public class BlockMain implements PluginMessageListener {

    @Getter
    private final Set<String> channels = Sets.newHashSet();

    public void init(Plugin plugin) {
        plugin.getServer().getPluginManager().registerEvents(new ModChannelRegisterListener(), plugin);
        this.channels.addAll(Arrays.asList(
                "forgewurst", "WDL", "WDL|INIT", "WDL|CONTROL",
                "5zig_Set", "BSM", "DIPermissions", "mcp",
                "LiteLoader", "liteloader", "PERMISSIONSREPL", "SERVERUI|S0", "SERVERUI|C0"));

        this.registerChannels(channels.toArray(new String[]{}));
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] bytes) {
        checkChannel(player.getUniqueId(), channel);
    }

    public void checkChannel(UUID uniqueId, String channel) {
        if (this.channels.contains(channel)) {
            if (channel.equalsIgnoreCase("forgewurst")) {
                this.kickPlayer(uniqueId, Modifications.FORGEWURST);
            } else if (channel.equalsIgnoreCase("WDL")) {
                this.kickPlayer(uniqueId, Modifications.WORLDDOWNLOADER);
            } else if (channel.equalsIgnoreCase("WDL|INIT")) {
                this.kickPlayer(uniqueId, Modifications.WORLDDOWNLOADER);
            } else if (channel.equalsIgnoreCase("WDL|CONTROL")) {
                this.kickPlayer(uniqueId, Modifications.WORLDDOWNLOADER);
            } else if (channel.equalsIgnoreCase("5zig_Set")) {
                this.kickPlayer(uniqueId, Modifications.FIVEZIG);
            } else if (channel.equalsIgnoreCase("BSM")) {
                this.kickPlayer(uniqueId, Modifications.BSM);
            } else if (channel.equalsIgnoreCase("DIPermissions")) {
                this.kickPlayer(uniqueId, Modifications.DIPERMISSIONS);
            } else if (channel.equalsIgnoreCase("mcp")) {
                this.kickPlayer(uniqueId, Modifications.MCP);
            } else if (channel.equalsIgnoreCase("LiteLoader")) {
                this.kickPlayer(uniqueId, Modifications.LITELOADER);
            } else if (channel.equalsIgnoreCase("liteloader")) {
                this.kickPlayer(uniqueId, Modifications.LITELOADER);
            } else if (channel.equalsIgnoreCase("PERMISSIONSREPL")) {
                this.kickPlayer(uniqueId, Modifications.PERMISSIONSREPL);
            } else if (channel.equalsIgnoreCase("SERVERUI|S0")) {
                this.kickPlayer(uniqueId, Modifications.SERVERUI);
            } else if (channel.equalsIgnoreCase("SERVERUI|C0")) {
                this.kickPlayer(uniqueId, Modifications.SERVERUI);
            } else {
                this.kickPlayer(uniqueId, Modifications.UNKNOWN);
            }
        }
    }

    private void registerChannels(String... channels) {
        Messenger messenger = GoServer.getService().getServer().getMessenger();
        for (String channel : channels) {
            messenger.registerIncomingPluginChannel(GoServer.getService(), channel, this);
            messenger.registerOutgoingPluginChannel(GoServer.getService(), channel);
        }
    }

    public void kickPlayer(UUID uniqueId, Modifications modifications) {
        GoServer.getService().getApi().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.BLOCK_MODIFICATIONS, new BlockModificationsPacket(uniqueId, modifications)), API.PacketReceiver.SLAVE);
    }
}
