package net.vicemice.modules.teamspeak.config;

import lombok.Data;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paul B. on 27.01.2019.
 */
@Data
public class Config {
    private String restartMode = "INTERVAL/TIMESTAMP";
    private List<String> restart_timeStamp = new ArrayList<>();
    private double restartInterval = 3.0;
}