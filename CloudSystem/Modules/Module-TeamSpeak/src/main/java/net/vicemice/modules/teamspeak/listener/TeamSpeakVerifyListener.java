package net.vicemice.modules.teamspeak.listener;

import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import net.vicemice.hector.network.modules.api.event.teamspeak.TeamSpeakVerifyEvent;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.event.IEventListener;
import net.vicemice.modules.teamspeak.Main;

public class TeamSpeakVerifyListener implements IEventListener<TeamSpeakVerifyEvent> {

    @Override
    public void onCall(TeamSpeakVerifyEvent event) {
        CloudPlayer cloudPlayer = event.getCloudPlayer();
        if (Main.getTeamSpeakBot() == null || Main.getTeamSpeakBot().getTs3Api() == null || Main.getTeamSpeakBot().getTs3Query() == null) {
            return;
        }

        ClientInfo clientInfo = Main.getTeamSpeakBot().getTs3Api().getClientInfo(Integer.parseInt(event.getUniqueId()));

        if (clientInfo != null) {
            cloudPlayer.sendMessage("teamspeak-success-added");
            Main.getTeamSpeakManager().register(cloudPlayer, clientInfo.getUniqueIdentifier());
            Main.getTeamSpeakManager().checkUserGroups(Integer.parseInt(event.getUniqueId()), Main.getTeamSpeakBot().getTs3Api(), cloudPlayer);
            Main.getTeamSpeakBot().getTs3Api().sendPrivateMessage(Integer.parseInt(event.getUniqueId()), "You have been successfully verified with Minecraft Account " + cloudPlayer.getName());
        } else {
            cloudPlayer.sendMessage("teamspeak-error-added");
        }
    }
}
