package net.vicemice.modules.teamspeak.listener;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.modules.api.event.teamspeak.TeamSpeakUnverifyEvent;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.ComponentMessagePacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.event.IEventListener;
import net.vicemice.modules.teamspeak.Main;

import java.util.ArrayList;

public class TeamSpeakUnverifyListener implements IEventListener<TeamSpeakUnverifyEvent> {

    @Override
    public void onCall(TeamSpeakUnverifyEvent event) {
        CloudPlayer cloudPlayer = event.getCloudPlayer();
        if (Main.getTeamSpeakBot() == null || Main.getTeamSpeakBot().getTs3Api() == null || Main.getTeamSpeakBot().getTs3Query() == null) {
            cloudPlayer.sendMessage("teamspeak-not-available");
            return;
        }

        if (cloudPlayer.getTeamSpeakId().equalsIgnoreCase("noid")) {
            cloudPlayer.sendMessage("teamspeak-not-connected");
            return;
        }

        if (event.isConfirmed()) {
            try {
                Main.getTeamSpeakBot().getTs3Api().getServerInfo();
            } catch (Exception e) {
                cloudPlayer.sendMessage("teamspeak-not-available");
                return;
            }
            final TS3Api ts3Api = Main.getTeamSpeakBot().getTs3Api();
            if (ts3Api != null) {
                if (ts3Api.isClientOnline(event.getCloudPlayer().getTeamSpeakId())) {
                    final ClientInfo clientInfo = ts3Api.getClientByUId(cloudPlayer.getTeamSpeakId());
                    if (clientInfo != null) {
                        Main.getTeamSpeakManager().removeAllRankGroups(ts3Api, clientInfo);
                    }
                }
            }
            Main.getTeamSpeakManager().unregister(cloudPlayer);
            cloudPlayer.sendMessage("teamspeak-success-removed");
        } else {
            String s = cloudPlayer.getName();
            final TS3Api ts3Api = Main.getTeamSpeakBot().getTs3Api();
            if (ts3Api != null) {
                if (ts3Api.isClientOnline(event.getCloudPlayer().getTeamSpeakId())) {
                    final ClientInfo clientInfo = ts3Api.getClientByUId(cloudPlayer.getTeamSpeakId());
                    s = clientInfo.getNickname();
                }
            }

            cloudPlayer.sendMessage("teamspeak-confirm-removed", s);
            ArrayList<String[]> messageData = new ArrayList<>();
            messageData.add(new String[]{cloudPlayer.getLanguageMessage("confirm-click"), "true", "run_command", "/unlink confirm teamspeak"});
            ComponentMessagePacket componentMessagePacket = new ComponentMessagePacket(cloudPlayer.getName().toLowerCase(), messageData);
            cloudPlayer.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMPONENT_MESSAGE_PACKET, componentMessagePacket));
        }
    }
}
