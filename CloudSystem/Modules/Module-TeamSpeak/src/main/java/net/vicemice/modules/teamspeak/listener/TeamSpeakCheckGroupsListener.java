package net.vicemice.modules.teamspeak.listener;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.api.exception.TS3QueryShutDownException;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import net.vicemice.hector.network.modules.api.event.teamspeak.TeamSpeakCheckGroupsEvent;
import net.vicemice.hector.utils.event.IEventListener;
import net.vicemice.modules.teamspeak.Main;

public class TeamSpeakCheckGroupsListener implements IEventListener<TeamSpeakCheckGroupsEvent> {

    @Override
    public void onCall(TeamSpeakCheckGroupsEvent event) {
        if (Main.getTeamSpeakBot() == null || Main.getTeamSpeakBot().getTs3Api() == null || Main.getTeamSpeakBot().getTs3Query() == null) {
            return;
        }
        try {
            if (event.getCloudPlayer().getTeamSpeakId() == null && event.getCloudPlayer().getTeamSpeakId() != null && event.getCloudPlayer().getTeamSpeakId().equals("noid")) {
                return;
            }
            TS3Api ts3Api = Main.getTeamSpeakBot().getTs3Api();
            if (ts3Api != null) {
                if (ts3Api.isClientOnline(event.getCloudPlayer().getTeamSpeakId())) {
                    ClientInfo clientInfo = ts3Api.getClientByUId(event.getCloudPlayer().getTeamSpeakId());
                    if (clientInfo != null) {
                        Main.getTeamSpeakManager().checkUserGroups(clientInfo.getId(), ts3Api, event.getCloudPlayer());
                    }
                }
            }
        } catch (TS3QueryShutDownException ex) {}
    }
}