package net.vicemice.modules.teamspeak.tasks;

import net.vicemice.modules.teamspeak.Main;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/*
 * Class created at 21:05 - 07.03.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class TimerThread {
    public Integer TIME;
    public Timer loopTimer, toggleTimer;
    public List<Integer> reminderList;
    public ConcurrentHashMap<Long, Boolean> closeSupport;
    boolean o = false;

    public TimerThread() {
        this.closeSupport = new ConcurrentHashMap<>();
        this.reminderList = new ArrayList<>();
        this.reminderList.add(30);
        this.reminderList.add(15);
        this.reminderList.add(10);
        this.reminderList.add(5);
        this.reminderList.add(1);
    }

    public void runLoop() {
        loopTimer = new Timer();
        loopTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                TIME--;
            }
        }, 0L, TimeUnit.SECONDS.toMillis(1));

        toggleTimer = new Timer();
        toggleTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (TIME <= 0 && !o) {
                    loopTimer.cancel();
                    toggleTimer.cancel();
                    Main.getTeamSpeakBot().toggleSupport();
                    restart();
                    o = true;
                }
            }
        }, 0L, TimeUnit.SECONDS.toMillis(1));
    }

    public void restart() {
        calculateTimer();
        runLoop();
        o = false;
    }

    public void calculateTimer() {
        // Initialize variable
        boolean doDefault = false;

        // Check restart mode
        switch (Main.getServiceConfig().getRestartMode().toUpperCase()) {
            case "TIMESTAMP":
                List<String> timestamps = Main.getServiceConfig().getRestart_timeStamp();
                List<Long> differences = new ArrayList<Long>();
                Long now = Calendar.getInstance().getTimeInMillis();

                // Convert time stamps to a list of differences
                for (String timestamp : timestamps) {
                    Long convertedTime = new TimeManager().parseTimeStamp(timestamp);

                    // Check if converted time was accepted
                    if (convertedTime == null) {
                        System.out.println(timestamp + " does not follow correct format!");
                        continue;
                    }

                    if (convertedTime < System.currentTimeMillis())
                        continue;

                    // Add converted time to test list
                    differences.add(convertedTime - now);
                }

                // Check if test list is empty
                if (differences.isEmpty()) {
                    System.out.println("There are no accepted timestamps availiable! Please check config to ensure that you have followed the correct format.");
                    doDefault = true;
                    break;
                }

                // Find smallest difference
                Long closestTime = differences.get(0);
                for (int i = 1; i < differences.size(); i++) {
                    Long test = differences.get(i);
                    if (test < closestTime) {
                        closestTime = test;
                    }
                }

                // Convert milliseconds to TIME
                TIME = (int) (closestTime / 1000l);
                break;

            default:
                // Default timer calculator
                doDefault = true;
                System.out.println("Restart mode \"" + Main.getServiceConfig().getRestartMode() + "\" was not found! Switching to interval mode!");
                break;
        }

        // Do default when requested
        if (doDefault) {

            // Calculate interval time
            TIME = (int) (Main.getServiceConfig().getRestartInterval() * 3600);
        }
    }
}