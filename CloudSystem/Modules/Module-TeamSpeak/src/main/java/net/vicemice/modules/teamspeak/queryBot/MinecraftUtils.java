package net.vicemice.modules.teamspeak.queryBot;

import net.vicemice.hector.CloudService;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;

public class MinecraftUtils {
    private static Map<String, BufferedImage> skins = new HashMap<String, BufferedImage>();

    public static BufferedImage getSkin(String name) {
        if (skins.containsKey(name) && skins.get(name) != null) {
            return skins.get(name);
        }
        URL url = null;
        try {
            url = new URL("https://minotar.net/helm/" + name + "/16.png");
        }
        catch (MalformedURLException e) {
            CloudService.getCloudService().getTerminal().writeMessage("Unable to parse skin URL");
        }
        try {
            InputStream inStream = url.openStream();
            BufferedImage image = ImageIO.read(inStream);
            skins.put(name, image);
            inStream.close();
            return image;
        }
        catch (IOException e) {
            CloudService.getCloudService().getTerminal().writeMessage("IOException while downloading skin!");
            return null;
        }
    }
}

