package net.vicemice.modules.teamspeak.tasks;

import net.vicemice.modules.teamspeak.Main;

import java.util.Calendar;

/*
 * Class created at 21:14 - 07.03.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class TimeManager {

    public Long parseTimeStamp(String timestamp) {
        // Variable initializers
        String[] stringVars = timestamp.split(":");
        Integer H = 0;
        Integer M = 0;

        // Check if time stamp format is accepted
        if (stringVars.length != 2) {
            return null;
        }

        // Try to parse string time stamp to integers
        try {
            H = Integer.parseInt(stringVars[0]);
            M = Integer.parseInt(stringVars[1]);
        } catch(NumberFormatException e) {
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            System.err.println("CalendarDeserialize.parseTimeStamp():IntegerParser");
            return null;
        }

        // Check if integers are in range
        if (H < 0 || H > 23) {
            return null;
        }
        if (M < 0 || M > 59) {
            return null;
        }

        // Calendar creator
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, H);
        cal.set(Calendar.MINUTE, M);
        cal.set(Calendar.SECOND, 0);

        if (cal.getTimeInMillis() - Calendar.getInstance().getTimeInMillis() < 0) {
            cal.setTimeInMillis(cal.getTimeInMillis() + 86400000l);
        }

        return cal.getTimeInMillis();
    }

    public HMS parseToHMS() {
        int H = Main.getTimerThread().TIME / 3600;
        int M = Main.getTimerThread().TIME / 60 - H * 60;
        int S = Main.getTimerThread().TIME - H * 3600 - M * 60;
        return new HMS(H, M, S);
    }

    public class HMS {
        public Integer H;
        public Integer M;
        public Integer S;

        public HMS(int H, int M, int S) {
            this.H = H;
            this.M = M;
            this.S = S;
        }
    }
}