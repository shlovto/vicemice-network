package net.vicemice.modules.teamspeak.queryBot;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import com.github.theholywaffle.teamspeak3.api.wrapper.ChannelInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.TeamSpeakSupportPacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.Rank;
import net.vicemice.modules.teamspeak.Main;

import java.util.*;

public class TeamSpeakManager {
    public boolean task = false;
    public int inSupport = 0;

    public TeamSpeakManager() {

    }

    public void startCheckSupport() {
        new Thread(() -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            new Timer().scheduleAtFixedRate(new TimerTask() {

                @Override
                public void run() {
                    PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.TEAMSPEAK_SUPPORT_PACKET, new TeamSpeakSupportPacket(TeamSpeakManager.this.inSupport));
                    ArrayList<Server> sendServers = new ArrayList<>();
                    for (UUID uuid : CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithRank()) {
                        CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uuid);
                        if (getPlayer == null || !getPlayer.isOnline() || !getPlayer.getRank().isTeam() || getPlayer.getServer() == null || sendServers.contains(getPlayer.getServer()))
                            continue;
                        sendServers.add(getPlayer.getServer());
                        getPlayer.getServer().sendPacket(initPacket);
                    }
                }
            }, 500, 500);
        }
        ).start();
    }

    public int getModerators() {
        if (Main.getTeamSpeakBot() != null) {
            TS3Api ts3Api = Main.getTeamSpeakBot().getTs3Api();
            int clients = 0;
            for (Client client : ts3Api.getClients()) {
                if (client.isInServerGroup(13) || client.isInServerGroup(15) || client.isInServerGroup(29))
                    clients++;
            }
            return (clients > 0 ? clients : -1);
        }
        return -1;
    }

    public int getPlayersInSupport() {
        if (Main.getTeamSpeakBot() != null) {
            TS3Api ts3Api = Main.getTeamSpeakBot().getTs3Api();
            ChannelInfo channelInfo = ts3Api.getChannelInfo(102);
            if (channelInfo != null) {
                Channel channel = ts3Api.getChannelByNameExact(channelInfo.getName(), true);
                if (channel != null) {
                    return channel.getTotalClients();
                }
                return -1;
            }
            return -1;
        }
        return -1;
    }

    public boolean isRegistered(String teamSpeakId) {
        if (teamSpeakId.equals("noid")) {
            return false;
        }
        if (CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByTeamSpeakID(teamSpeakId) != null) {
            return true;
        }
        return false;
    }

    public void unregister(CloudPlayer cloudPlayer) {
        CloudService.getCloudService().getManagerService().getPlayerManager().getTeamSpeakUniqueIds().remove(cloudPlayer.getTeamSpeakId());
        cloudPlayer.setTeamSpeakId("noid");
        cloudPlayer.updateToDatabase(false);
    }

    public void register(CloudPlayer cloudPlayer, String teamSpeakID) {
        cloudPlayer.setTeamSpeakId(teamSpeakID);
        cloudPlayer.updateToDatabase(false);
        CloudService.getCloudService().getManagerService().getPlayerManager().getTeamSpeakUniqueIds().put(teamSpeakID, cloudPlayer.getName().toLowerCase());
    }

    public void checkUserGroups(int clientID, TS3Api ts3Api, CloudPlayer cloudPlayer) {
        List<Integer> toAdd = new ArrayList<>();
        List<Integer> toRemove = new ArrayList<>();
        List<Integer> userGroups = new ArrayList<>();
        List<Integer> rankGroups = new ArrayList<>();

        for (int group : cloudPlayer.getRank().getTeamSpeakGroups()) {
            rankGroups.add(group);
        }

        ClientInfo clientInfo = null;
        if (clientInfo == null) {
            clientInfo = ts3Api.getClientInfo(clientID);
        }
        for (int groupID : clientInfo.getServerGroups()) {
            userGroups.add(groupID);
        }

        for (int groups : rankGroups) {
            if (!userGroups.contains(groups)) {
                toAdd.add(groups);
            }
        }

        for (int groupID : userGroups) {
            if (!rankGroups.contains(groupID)) {
                if(groupID != 20 && groupID != 22 && groupID != 23 && groupID != 27 && groupID != 26) {
                    toRemove.add(groupID);
                }
            }
        }

        for (int groupID : toAdd) {
            ts3Api.addClientToServerGroup(groupID, clientInfo.getDatabaseId());
        }

        for (int groupID : toRemove) {
            ts3Api.removeClientFromServerGroup(groupID, clientInfo.getDatabaseId());
        }
    }

    public void removeAllRankGroups(TS3Api ts3Api, ClientInfo clientInfo) {

        List<Integer> teamGroups = new ArrayList<>();
        for (Rank rank : Rank.values()) {
            for (int group : rank.getTeamSpeakGroups()) {
                teamGroups.add(group);
            }
        }
        teamGroups.add(19);

        for (int groupID : clientInfo.getServerGroups()) {
            if (teamGroups.contains(groupID) && groupID != 20) {
                ts3Api.removeClientFromServerGroup(groupID, clientInfo.getDatabaseId());
            }
        }
    }


    public CloudPlayer getPlayerByUid(String teamSpeakUid) {
        return CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByTeamSpeakID(teamSpeakUid);
    }

    public boolean isTask() {
        return this.task;
    }

    public int getInSupport() {
        return this.inSupport;
    }

    public void setInSupport(int inSupport) {
        this.inSupport = inSupport;
    }
}