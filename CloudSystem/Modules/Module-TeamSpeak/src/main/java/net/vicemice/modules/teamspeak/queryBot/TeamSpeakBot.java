package net.vicemice.modules.teamspeak.queryBot;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.ChannelProperty;
import com.github.theholywaffle.teamspeak3.api.TextMessageTargetMode;
import com.github.theholywaffle.teamspeak3.api.event.ChannelCreateEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelDeletedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelDescriptionEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ChannelPasswordChangedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientJoinEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientLeaveEvent;
import com.github.theholywaffle.teamspeak3.api.event.ClientMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.PrivilegeKeyUsedEvent;
import com.github.theholywaffle.teamspeak3.api.event.ServerEditedEvent;
import com.github.theholywaffle.teamspeak3.api.event.TS3Listener;
import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.ComponentMessagePacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.RandomStringGenerator;
import net.vicemice.modules.teamspeak.Main;
import java.util.*;

public class TeamSpeakBot {
    private TS3Api ts3Api;
    private TS3Query ts3Query;
    private boolean debugMode = false;

    public void registerBot() {
        new Thread(() -> {
            TS3Config config = new TS3Config();
            config.setHost("91.218.67.206");
            config.setFloodRate(TS3Query.FloodRate.UNLIMITED);
            this.ts3Query = new TS3Query(config);
            this.ts3Query.connect();
            this.ts3Api = this.ts3Query.getApi();
            this.ts3Api.login("serveradmin", "IlnOj897");
            this.ts3Api.selectVirtualServerById(1);
            this.ts3Api.setNickname("ViceMice.net");
            this.ts3Api.registerAllEvents();
            this.addListener();
        }).start();
    }

    public void unregister() {
        try {
            this.ts3Api.unregisterAllEvents();
            this.ts3Query.exit();
            this.ts3Api.logout();
        } catch (Exception ignored) {
        }
    }

    public void addListener() {
        this.ts3Api.addTS3Listeners(new TS3Listener() {

            @Override
            public void onPrivilegeKeyUsed(PrivilegeKeyUsedEvent privilegeKeyUsedEvent) {
            }

            @Override
            public void onTextMessage(TextMessageEvent e) {
                new Thread(() -> {
                    if (e.getTargetMode() != TextMessageTargetMode.CLIENT) {
                        return;
                    }
                    int clientID = e.getInvokerId();
                    String message = e.getMessage();
                    String[] args = message.split(" ");

                    if (message.equals("restart")) {
                        CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByTeamSpeakID(TeamSpeakBot.this.ts3Api.getClientInfo(clientID).getUniqueIdentifier());
                        if (cloudPlayer == null) return;
                        if (!cloudPlayer.getRank().isAdmin()) return;
                        for (Client client : TeamSpeakBot.this.ts3Api.getClients()) {
                            TeamSpeakBot.this.ts3Api.pokeClient(client.getId(), "We restart the teamspeak in few seconds.");
                        }
                        return;
                    } else if (message.equals("togglesupport")) {
                        CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByTeamSpeakID(TeamSpeakBot.this.ts3Api.getClientInfo(clientID).getUniqueIdentifier());
                        if (cloudPlayer == null) return;
                        if (!cloudPlayer.getRank().isAdmin()) return;
                        toggleSupport();
                        return;
                    } else if (message.equals("supporttime")) {
                        CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByTeamSpeakID(TeamSpeakBot.this.ts3Api.getClientInfo(clientID).getUniqueIdentifier());
                        if (cloudPlayer == null) return;
                        if (!cloudPlayer.getRank().isAdmin()) return;
                        TeamSpeakBot.this.ts3Api.sendPrivateMessage(clientID, "Remaining: " + Main.getTimerThread().TIME);
                        return;
                    }

                    String name = args[0].toLowerCase();
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(name);
                    if (cloudPlayer == null || cloudPlayer.getProxy() == null) {
                        TeamSpeakBot.this.ts3Api.sendPrivateMessage(clientID, "This player is not online on our network");
                        return;
                    }
                    ClientInfo clientInfo = TeamSpeakBot.this.ts3Api.getClientInfo(clientID);
                    if (!cloudPlayer.getIp().equals(clientInfo.getIp())) {
                        TeamSpeakBot.this.ts3Api.sendPrivateMessage(clientID, "Please verify yourself with your account and not with someone else's.");
                        TeamSpeakBot.this.ts3Api.sendPrivateMessage(clientID, "If this is your account, contact an administrator.");
                        return;
                    }
                    if (!cloudPlayer.getTeamSpeakId().equals("noid")) {
                        if (!cloudPlayer.getTeamSpeakId().equals(clientInfo.getUniqueIdentifier())) {
                            TeamSpeakBot.this.ts3Api.sendPrivateMessage(clientID, "Your teamspeak identity has already been verified");
                            TeamSpeakBot.this.ts3Api.sendPrivateMessage(clientID, "To remove the verification, please give Ingame [B]/unverify teamspeak[/B].");
                        } else {
                            TeamSpeakBot.this.ts3Api.sendPrivateMessage(clientID, "You are already verified.");
                        }
                        return;
                    }
                    TeamSpeakBot.this.ts3Api.sendPrivateMessage(clientID, "Please confirm the request on the network.");

                    cloudPlayer.sendMessage("teamspeak-verify-verification", clientInfo.getNickname());
                    ArrayList<String[]> messageData = new ArrayList<String[]>();
                    messageData.add(new String[]{cloudPlayer.getLanguageMessage("confirm-click"), "true", "run_command", "/link teamspeak " + clientID});
                    ComponentMessagePacket componentMessagePacket = new ComponentMessagePacket(cloudPlayer.getName().toLowerCase(), messageData);
                    cloudPlayer.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMPONENT_MESSAGE_PACKET, componentMessagePacket));
                }
                ).start();
            }

            @Override
            public void onServerEdit(ServerEditedEvent e) {
            }

            @Override
            public void onClientMoved(ClientMovedEvent e) {
                String uniqueIdentifier = Main.getTeamSpeakBot().getTs3Api().getClientInfo(e.getClientId()).getUniqueIdentifier();

                if (e.getTargetChannelId() == 102) {
                    Main.getTeamSpeakManager().setInSupport(Main.getTeamSpeakManager().getPlayersInSupport());

                    String name = Main.getTeamSpeakBot().getTs3Api().getClientInfo(e.getClientId()).getNickname();
                    if (CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByTeamSpeakID(uniqueIdentifier) != null) {
                        name = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByTeamSpeakID(uniqueIdentifier).getRank().getRankColor() + CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByTeamSpeakID(uniqueIdentifier).getName();
                    }

                    CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().sendTeamMessage("teamspeak-support", name);
                    if (CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByTeamSpeakID(uniqueIdentifier) != null) {
                        name = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByTeamSpeakID(uniqueIdentifier).getName();
                    }

                    int mods = Main.getTeamSpeakManager().getModerators();
                    if (mods != -1) {
                        TeamSpeakBot.this.ts3Api.sendPrivateMessage(e.getClientId(), "Currently are [B]"+mods+"[/B] Moderators online, someone will come to you in a few minutes.");
                        TeamSpeakBot.this.ts3Api.sendPrivateMessage(e.getClientId(), "[B]If you take advantage of the support, this can involve a support ban[/B]");
                    } else {
                        TeamSpeakBot.this.ts3Api.sendPrivateMessage(e.getClientId(), "Unfortunately there are no moderators online right now, please contact us on our Discord or write a moderator in the forum.");
                    }

                    Map<ChannelProperty, String> options = new HashMap<>();
                    options.put(ChannelProperty.CPID, "102");
                    String id = RandomStringGenerator.generateRandomString(6, RandomStringGenerator.Mode.ALPHANUMERIC);
                    int cid = TeamSpeakBot.this.ts3Api.createChannel("Support #"+id, options);
                    TeamSpeakBot.this.ts3Api.addChannelPermission(cid, "i_channel_needed_join_power", 100);
                    TeamSpeakBot.this.ts3Api.moveClient(e.getClientId(), cid);
                    try {
                        Thread.sleep(3L);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                    TeamSpeakBot.this.ts3Api.moveQuery(1);

                    if (!TeamSpeakBot.this.ts3Api.getClientInfo(e.getClientId()).isInServerGroup(10)) {
                        for (Client client : Main.getTeamSpeakBot().getTs3Api().getClients()) {
                            if (client.isInServerGroup(13) || client.isInServerGroup(15) || client.isInServerGroup(29))
                                TeamSpeakBot.this.ts3Api.sendPrivateMessage(client.getId(), "[B]"  + name +  "[/B] has entered the teamspeak support");
                        }
                    }
                }
            }

            @Override
            public void onClientLeave(ClientLeaveEvent e) {
            }

            @Override
            public void onClientJoin(ClientJoinEvent e) {
                new Thread(() -> {
                    String id = e.getUniqueClientIdentifier();
                    int clientID = e.getClientId();

                    /*if (!characterCheck(e.getClientNickname())) {
                        Main.getTeamSpeakBot().getTs3Api().kickClientFromServer("Es sind nur Namen mit folgenden Zeichen erlaubt: a-z 0-9", clientID);
                        return;
                    }*/

                    if (!Main.getTeamSpeakManager().isRegistered(id)) {
                        TeamSpeakBot.this.ts3Api.sendPrivateMessage(clientID, "You are not verified with a Minecraft account.");
                        TeamSpeakBot.this.ts3Api.sendPrivateMessage(clientID, "Write your Minecraft name in this chat window");
                    } else {
                        CloudPlayer cloudPlayer = Main.getTeamSpeakManager().getPlayerByUid(id);
                        Main.getTeamSpeakManager().checkUserGroups(clientID, TeamSpeakBot.this.ts3Api, cloudPlayer);
                    }
                }
                ).start();
            }

            @Override
            public void onChannelEdit(ChannelEditedEvent e) {
            }

            @Override
            public void onChannelDescriptionChanged(ChannelDescriptionEditedEvent e) {
            }

            @Override
            public void onChannelCreate(ChannelCreateEvent e) {
            }

            @Override
            public void onChannelDeleted(ChannelDeletedEvent e) {
            }

            @Override
            public void onChannelMoved(ChannelMovedEvent e) {
            }

            @Override
            public void onChannelPasswordChanged(ChannelPasswordChangedEvent e) {
            }
        });
    }

    public TS3Api getTs3Api() {
        return this.ts3Api;
    }

    public TS3Query getTs3Query() {
        return this.ts3Query;
    }

    public boolean isDebugMode() {
        return this.debugMode;
    }

    public void setDebugMode(boolean debugMode) {
        this.debugMode = debugMode;
    }

    public boolean characterCheck(String name) {
        for (char c : name.toCharArray()) {
            if (c == ' ' || c == 'ö' || c == 'Ö' || c == 'ä' || c == 'Ä' || c == '_' || c == '>' || c == '<') continue;
            if (c == 'ü' || c == 'Ü') continue;
            if ('0' <= c && c <= '9') continue;
            if ('a' <= c && c <= 'z') continue;
            if ('A' <= c && c <= 'Z') continue;
            return false;
        }
        return true;
    }

    public void toggleSupport() {
        boolean close = true;
        if (TeamSpeakBot.this.ts3Api.getChannelInfo(102).getName().equalsIgnoreCase("Queue (Closed - 14:00-22:00 UTC+1)")) {
            close = false;
        }

        Map<ChannelProperty, String> options = new HashMap<>();
        if (!close) {
            options.put(ChannelProperty.CHANNEL_NAME, "Queue (Open)");
            options.put(ChannelProperty.CHANNEL_PASSWORD, null);
            options.put(ChannelProperty.CHANNEL_FLAG_MAXCLIENTS_UNLIMITED, "1");
            options.put(ChannelProperty.CHANNEL_MAXCLIENTS, "999");
        } else {
            options.put(ChannelProperty.CHANNEL_NAME, "Queue (Closed - 14:00-22:00 UTC+1)");
            options.put(ChannelProperty.CHANNEL_PASSWORD, RandomStringGenerator.generateRandomString(30, RandomStringGenerator.Mode.ALPHANUMERIC));
            options.put(ChannelProperty.CHANNEL_FLAG_MAXCLIENTS_UNLIMITED, "0");
            options.put(ChannelProperty.CHANNEL_MAXCLIENTS, "0");
        }
        TeamSpeakBot.this.getTs3Api().editChannel(TeamSpeakBot.this.getTs3Api().getChannelInfo(102).getId(), options);
    }
}