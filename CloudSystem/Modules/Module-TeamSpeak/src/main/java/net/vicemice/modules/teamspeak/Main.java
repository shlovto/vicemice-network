package net.vicemice.modules.teamspeak;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.network.modules.api.CoreModule;
import net.vicemice.modules.teamspeak.config.Config;
import net.vicemice.modules.teamspeak.config.Configuration;
import net.vicemice.modules.teamspeak.listener.TeamSpeakCheckGroupsListener;
import net.vicemice.modules.teamspeak.listener.TeamSpeakUnverifyListener;
import net.vicemice.modules.teamspeak.listener.TeamSpeakVerifyListener;
import net.vicemice.modules.teamspeak.tasks.TimerThread;
import net.vicemice.modules.teamspeak.queryBot.TeamSpeakManager;
import net.vicemice.modules.teamspeak.queryBot.TeamSpeakBot;
import lombok.Getter;

public class Main extends CoreModule {

    @Getter
    public static TeamSpeakBot teamSpeakBot;
    @Getter
    public static TeamSpeakManager teamSpeakManager;

    @Getter
    public static TimerThread timerThread;

    @Getter
    public TeamSpeakVerifyListener teamSpeakVerifyListener = new TeamSpeakVerifyListener();
    @Getter
    public TeamSpeakUnverifyListener teamSpeakUnverifyListener = new TeamSpeakUnverifyListener();
    @Getter
    public TeamSpeakCheckGroupsListener teamSpeakCheckGroupsListener = new TeamSpeakCheckGroupsListener();

    @Getter
    public static Config serviceConfig;

    public void onLoad() {
        Configuration configuration = new Configuration();
        if (!configuration.exists()) {
            Config config = new Config();
            config.getRestart_timeStamp().add("14:00");
            config.getRestart_timeStamp().add("22:00");
            configuration.writeConfiguration(config);
        }
        configuration.readConfiguration();
        serviceConfig = configuration.getConfig();

        teamSpeakBot = new TeamSpeakBot();
        teamSpeakManager = new TeamSpeakManager();
        timerThread = new TimerThread();
        onBootstrap();
    }

    public void onBootstrap() {
        teamSpeakBot.registerBot();
        teamSpeakManager.startCheckSupport();
        timerThread.restart();

        registerListener(teamSpeakVerifyListener);
        registerListener(teamSpeakUnverifyListener);
        registerListener(teamSpeakCheckGroupsListener);
    }

    @Override
    public void onShutdown() {
        teamSpeakBot.unregister();
        CloudService.getCloudService().getManagerService().getEventManager().unregisterListener(teamSpeakVerifyListener);
        CloudService.getCloudService().getManagerService().getEventManager().unregisterListener(teamSpeakUnverifyListener);
        CloudService.getCloudService().getManagerService().getEventManager().unregisterListener(teamSpeakCheckGroupsListener);
    }
}
