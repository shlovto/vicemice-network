package net.vicemice.modules.vote;

import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.network.modules.api.CoreModule;
import net.vicemice.modules.vote.listener.VoteListener;
import net.vicemice.modules.vote.rsa.RSAIO;
import net.vicemice.modules.vote.rsa.RSAKeygen;
import net.vicemice.modules.vote.util.VoteReceiver;

import java.io.File;
import java.security.KeyPair;

public class Main extends CoreModule {

    /** Main Instance */
    @Getter
    private static Main instance;

    /** The vote receiver. */
    @Getter
    private VoteReceiver voteReceiver;

    /** The RSA key pair. */
    @Getter
    private KeyPair keyPair;

    private VoteListener voteListener;

    public void onLoad() {
        instance = this;

        // Handle configuration.
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }
        File rsaDirectory = new File(getDataFolder() + "/rsa");
        // Replace to remove a bug with Windows paths - SmilingDevil
        String listenerDirectory = getDataFolder().toString()
                .replace("\\", "/") + "/listeners";

        /*
         * Create RSA directory and keys if it does not exist; otherwise, read
         * keys.
         */
        try {
            if (!rsaDirectory.exists()) {
                rsaDirectory.mkdir();
                new File(listenerDirectory).mkdir();
                keyPair = RSAKeygen.generate(2048);
                RSAIO.save(rsaDirectory, keyPair);
            } else {
                keyPair = RSAIO.load(rsaDirectory);
            }
        } catch (Exception ex) {
            CloudService.getCloudService().getTerminal().writeMessage("\u00a7cError reading configuration file or RSA keys");
            ex.printStackTrace();
            return;
        }

        onBootstrap();
    }

    public void onBootstrap() {
        try {
            voteReceiver = new VoteReceiver("91.218.67.206", 1312);
            voteReceiver.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        voteListener = new VoteListener();
        registerListener(voteListener);
    }

    @Override
    public void onShutdown() {
        CloudService.getCloudService().getManagerService().getEventManager().unregisterListener(voteListener);

        if (voteReceiver != null) {
            voteReceiver.shutdown();
        }
    }
}