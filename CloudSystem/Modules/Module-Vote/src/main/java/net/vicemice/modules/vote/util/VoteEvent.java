package net.vicemice.modules.vote.util;

import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.event.async.AsyncEvent;
import net.vicemice.hector.utils.event.async.AsyncPosterAdapter;

public class VoteEvent extends AsyncEvent<VoteEvent> {
    private CloudPlayer cloudPlayer;
    private long timeStamp;

    public VoteEvent(CloudPlayer cloudPlayer, long timeStamp) {
        super(new AsyncPosterAdapter<>());
        this.cloudPlayer = cloudPlayer;
        this.timeStamp = timeStamp;
    }

    public CloudPlayer getCloudPlayer() {
        return this.cloudPlayer;
    }

    public long getTimeStamp() {
        return timeStamp;
    }
}
