package net.vicemice.modules.vote.listener;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.utils.RandomStringGenerator;
import net.vicemice.hector.utils.event.IEventListener;
import net.vicemice.hector.utils.players.utils.DataUpdateType;
import net.vicemice.modules.vote.util.VoteEvent;

import java.util.concurrent.TimeUnit;

public class VoteListener implements IEventListener<VoteEvent> {

    @Override
    public void onCall(VoteEvent event) {
        event.getCloudPlayer().sendMessage("vote-message");
        CloudService.getCloudService().getManagerService().getStatsManager().addPlayerStat(event.getCloudPlayer(), "vote_keys", 1);
        event.getCloudPlayer().sendData(event.getCloudPlayer().getServer(), DataUpdateType.UPDATE_VOTES, true);

        CloudService.getCloudService().getManagerService().getStatsManager().addPlayerStat(event.getCloudPlayer(), "vote_streak", 1);
        CloudService.getCloudService().getManagerService().getDatabaseManager().savePlayerData(event.getCloudPlayer().getUniqueId(), "vote_streak", String.valueOf((System.currentTimeMillis()+(TimeUnit.DAYS.toMillis(1)+TimeUnit.HOURS.toMillis(12)))));

        if (event.getCloudPlayer().getStats().getGlobalStatsValue("vote_streak") > 1) {
            try {
                long i = Long.parseLong(RandomStringGenerator.generateRandomString(3, RandomStringGenerator.Mode.NUMERIC));
                event.getCloudPlayer().addCoins(i);
                event.getCloudPlayer().sendMessage("§6+ "+i+" §aVote §7Streak §6Coins");
            } catch (Exception e) {
                event.getCloudPlayer().sendMessage("§cError while get Daily Vote Streak Coins");
                e.printStackTrace();
            }
        }
    }
}