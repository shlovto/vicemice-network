package net.vicemice.modules.discord.mongo;

import lombok.Getter;
import net.vicemice.hector.CloudService;
import org.bson.Document;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/*
 * Class created at 19:13 - 19.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class DiscordService {

    @Getter
    private ConcurrentHashMap<String, DiscordUser> users = new ConcurrentHashMap<>();

    @Getter
    private List<String> filter = new ArrayList<>();

    public void loadUsers() {
        for (Document document : CloudService.getCloudService().getManagerService().getMongoManager().getDiscordDatabase().getCollection("users").find()) {
            String id = document.getString("id");
            boolean muted = document.getBoolean("muted");
            String reason = document.getString("mute_reason");
            long end = document.getLong("mute_time");
            long points = document.getLong("points");

            this.users.put(id, new DiscordUser(id, muted, reason, end, points));
        }
    }

    public void loadFilter() {
        for (Document document : CloudService.getCloudService().getManagerService().getMongoManager().getDiscordDatabase().getCollection("filter").find()) {
            this.filter.add(document.getString("word"));
        }
    }

    public void addWord(String word) {
        CloudService.getCloudService().getManagerService().getMongoManager().getDiscordDatabase().getCollection("filter").insertOne(new Document("word", word));
        this.filter.add(word);
    }

    public void deleteWord(String word) {
        CloudService.getCloudService().getManagerService().getMongoManager().getDiscordDatabase().getCollection("filter").deleteOne(new Document("word", word));
        this.filter.remove(word);
    }

    public boolean containsWord(String message) {
        if (message.contains(" ")) {
            for (String s : message.split(" ")) {
                if (this.filter.contains(s)) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            if (this.filter.contains(message)) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }
}