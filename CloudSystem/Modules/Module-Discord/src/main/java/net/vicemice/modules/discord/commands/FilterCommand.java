package net.vicemice.modules.discord.commands;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.modules.discord.Main;
import net.vicemice.modules.discord.dep.command.Command;
import net.vicemice.modules.discord.dep.command.CommandResponse;

import java.awt.*;

/*
 * Class created at 17:55 - 12.03.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class FilterCommand extends Command {

    public FilterCommand() {
        super("filter", "", "Filter Verwaltung");
    }

    @Override
    public CommandResponse triggerCommand(Message message, String[] args) {
        CloudPlayer author = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByDiscordID(message.getAuthor().getId());
        if (author == null || !author.getRank().isAdmin()) {
            return CommandResponse.ACCEPTED;
        }

        try {
            if (args.length != 0) {
                if (args[0].equalsIgnoreCase("add")) {
                    if (args.length == 2) {
                        String word = args[1];
                        if (!Main.getDiscordService().getFilter().contains(word.toLowerCase())) {
                            Main.getDiscordService().addWord(word.toLowerCase());
                            message.getTextChannel().sendMessage(new EmbedBuilder()
                                    .setColor(Color.GREEN)
                                    .setTitle("Erfolgreich", null)
                                    .setFooter("ViceMice.net", "https://acp.vicemice.net/assets/images/favicon.png")
                                    .addField("Information", "Das Wort wurde zum Filter hinzugefügt", false)
                                    .build()
                            ).queue();
                        } else {
                            message.getTextChannel().sendMessage(new EmbedBuilder()
                                    .setColor(Color.RED)
                                    .setTitle("Fehler", null)
                                    .setFooter("ViceMice.net", "https://acp.vicemice.net/assets/images/favicon.png")
                                    .addField("Information", "Das Wort ist bereits auf der Liste", false)
                                    .build()
                            ).queue();
                        }
                    } else {
                        message.getTextChannel().sendMessage(new EmbedBuilder()
                                .setColor(Color.RED)
                                .setTitle("Fehler", null)
                                .setFooter("ViceMice.net", "https://acp.vicemice.net/assets/images/favicon.png")
                                .addField("Information", "Dieser Sub-Command benötigt genau 1 Argument", false)
                                .build()
                        ).queue();
                    }
                } else if (args[0].equalsIgnoreCase("remove")) {
                    if (args.length == 2) {
                        String word = args[1];
                        if (Main.getDiscordService().getFilter().contains(word.toLowerCase())) {
                            Main.getDiscordService().deleteWord(word.toLowerCase());
                            message.getTextChannel().sendMessage(new EmbedBuilder()
                                    .setColor(Color.GREEN)
                                    .setTitle("Erfolgreich", null)
                                    .setFooter("ViceMice.net", "https://acp.vicemice.net/assets/images/favicon.png")
                                    .addField("Information", "Das Wort wurde vom Filter entfernt", false)
                                    .build()
                            ).queue();
                        } else {
                            message.getTextChannel().sendMessage(new EmbedBuilder()
                                    .setColor(Color.RED)
                                    .setTitle("Fehler", null)
                                    .setFooter("ViceMice.net", "https://acp.vicemice.net/assets/images/favicon.png")
                                    .addField("Information", "Das Wort ist nicht auf der Liste", false)
                                    .build()
                            ).queue();
                        }
                    } else {
                        message.getTextChannel().sendMessage(new EmbedBuilder()
                                .setColor(Color.RED)
                                .setTitle("Fehler", null)
                                .setFooter("ViceMice.net", "https://acp.vicemice.net/assets/images/favicon.png")
                                .addField("Information", "Dieser Sub-Command benötigt genau 1 Argument", false)
                                .build()
                        ).queue();
                    }
                } else {
                    message.getTextChannel().sendMessage(new EmbedBuilder()
                            .setColor(Color.RED)
                            .setTitle("Fehler", null)
                            .setFooter("ViceMice.net", "https://acp.vicemice.net/assets/images/favicon.png")
                            .addField("Information", "Dieser Sub-Command wurde nicht gefunden", false)
                            .build()
                    ).queue();
                }
            } else {
                message.getTextChannel().sendMessage(new EmbedBuilder()
                        .setColor(Color.RED)
                        .setTitle("Fehler", null)
                        .setFooter("ViceMice.net", "https://acp.vicemice.net/assets/images/favicon.png")
                        .addField("Information", "Für diesen Befehl werden mehr Argumente benötigt", false)
                        .build()
                ).queue();
            }
        } catch (Exception ex) {
            message.getTextChannel().sendMessage(new EmbedBuilder()
                    .setColor(Color.RED)
                    .setTitle("Fehler", null)
                    .setFooter("ViceMice.net", "https://acp.vicemice.net/assets/images/favicon.png")
                    .addField("Information", "Der Fehler wurde bereits an unseren Entwickler übermittelt.", false)
                    .build()
            ).queue();
            Main.getDiscordBot().sendErrorReport(ex);
        }

        return CommandResponse.ACCEPTED;
    }
}
