package net.vicemice.modules.discord.listener;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.network.modules.api.event.discord.DiscordUnverifyEvent;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.ComponentMessagePacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.event.IEventListener;
import net.vicemice.hector.utils.language.Language;
import net.vicemice.modules.discord.Main;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class DiscordUnverifyListener implements IEventListener<DiscordUnverifyEvent> {

    @Override
    public void onCall(DiscordUnverifyEvent event) {
        CloudPlayer cloudPlayer = event.getCloudPlayer();
        if (Main.getDiscordBot() == null || Main.getDiscordBot().getJda() == null)

        if (cloudPlayer.getDiscordId().equalsIgnoreCase("noid")) {
            cloudPlayer.sendMessage("discord-not-connected");
            return;
        }

        Member member = Main.getDiscordBot().getJda().getGuildById("706512965096177702").getMemberById(cloudPlayer.getDiscordId());

        if (member != null) {
            if (event.isConfirmed()) {
                for (Role role : Main.getDiscordBot().getJda().getGuildById("706512965096177702").getMemberById(cloudPlayer.getDiscordId()).getRoles()) {
                    if (role.getId().equalsIgnoreCase("706518701121470505")) continue;
                    if (role.getId().equalsIgnoreCase("706512965096177702")) continue;
                    Main.getDiscordBot().getJda().getGuildById("706512965096177702").removeRoleFromMember(member, role).queue();
                }

                Role role = Main.getDiscordBot().getJda().getRoleById("706513901852033025");
                Main.getDiscordBot().getJda().getGuildById("706512965096177702").addRoleToMember(member, role).queue();

                cloudPlayer.setDiscordId("noid");
                cloudPlayer.updateToDatabase(false);
                cloudPlayer.sendMessage("discord-success-removed");
            } else {
                cloudPlayer.sendMessage("discord-confirm-removed", member.getUser().getName());
                ArrayList<String[]> messageData = new ArrayList<>();
                messageData.add(new String[]{cloudPlayer.getLanguageMessage("confirm-click"), "true", "run_command", "/unlink confirm discord"});
                ComponentMessagePacket componentMessagePacket = new ComponentMessagePacket(cloudPlayer.getName().toLowerCase(), messageData);
                cloudPlayer.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMPONENT_MESSAGE_PACKET, componentMessagePacket));
            }
        } else {
            cloudPlayer.setDiscordId("noid");
            cloudPlayer.updateToDatabase(false);
            cloudPlayer.sendMessage("discord-success-removed");
        }
    }
}
