package net.vicemice.modules.discord.dep.cooldown;

import net.dv8tion.jda.api.entities.Member;

public interface ICooldown {

    /**
     * This method is used to allow cooldown bypasses.
     *
     * @param member the member object from the JDA api.
     * @return if the member is allow to bypass the cooldown.
     */
    boolean bypassCooldown(Member member);

    /**
     * This method is used to set the cooldown duration.
     *
     * @return the duration from the cooldown in seconds.
     */
    int cooldownDuration();
}