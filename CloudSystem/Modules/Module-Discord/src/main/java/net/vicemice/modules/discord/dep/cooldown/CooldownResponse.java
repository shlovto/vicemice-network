package net.vicemice.modules.discord.dep.cooldown;

public enum CooldownResponse {
    TRUE,
    FALSE,
    BYPASS
}