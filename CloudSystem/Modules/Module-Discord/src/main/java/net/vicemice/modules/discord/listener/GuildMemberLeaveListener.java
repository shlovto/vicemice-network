package net.vicemice.modules.discord.listener;

import net.dv8tion.jda.api.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

/*
 * Class created at 19:04 - 19.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GuildMemberLeaveListener extends ListenerAdapter {

    @Override
    public void onGuildMemberLeave(GuildMemberLeaveEvent event) {
        if (event.getGuild().getId().equalsIgnoreCase("706512965096177702")) {
            event.getGuild().getTextChannelById("706518477262946412").sendMessage(event.getMember().getUser().getAsTag() + " left the discord server").queue();
        } else {
            System.out.println("§cUnsupported Discord Guild '" + event.getGuild().getId() + "'");
        }
    }
}
