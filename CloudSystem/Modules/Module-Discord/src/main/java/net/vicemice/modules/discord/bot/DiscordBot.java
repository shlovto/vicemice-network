package net.vicemice.modules.discord.bot;

import com.google.common.reflect.ClassPath;
import net.dv8tion.jda.api.*;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.PunishType;
import net.vicemice.modules.discord.Main;
import net.vicemice.modules.discord.commands.FilterCommand;
import net.vicemice.modules.discord.commands.InfoCommand;
import net.vicemice.modules.discord.commands.StatsCommand;
import net.vicemice.modules.discord.commands.UsersCommand;
import net.vicemice.modules.discord.dep.command.CommandRegistry;
import net.vicemice.modules.discord.dep.command.DefaultCommandRegistry;
import lombok.Getter;

import javax.security.auth.login.LoginException;
import java.awt.*;
import java.io.IOException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class DiscordBot {
    @Getter
    private JDA jda;
    @Getter
    private CommandRegistry commandRegistry;
    @Getter
    private static Timer autoStatus;

    public void startBot() {
        try {
            jda = new JDABuilder(AccountType.BOT)
                    .setToken("NjkwNzg2MDgxOTg1MTM0NjIz.Xq7ZHA.S5ku4YjStbEcqXRWpAyHI1STAgs")
                    .setAutoReconnect(true)
                    .setStatus(OnlineStatus.ONLINE)
                    .build();

            try {
                for (ClassPath.ClassInfo classInfo : ClassPath.from(this.getClass().getClassLoader()).getTopLevelClasses("net.vicemice.modules.discord.listener")) {
                    Class clazz = Class.forName(classInfo.getName());

                    if (ListenerAdapter.class.isAssignableFrom(clazz)) {
                        jda.addEventListener(clazz.newInstance());
                        //CloudServer.getLogger().info("Loading Class: " + clazz.getName());
                    }
                }
            } catch (IOException | ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
                ex.printStackTrace();
            }
        } catch (LoginException ex) {
            ex.printStackTrace();
        }

        commandRegistry = new DefaultCommandRegistry();
        this.commandRegistry.registerCommand(new InfoCommand());
        this.commandRegistry.registerCommand(new StatsCommand());
        this.commandRegistry.registerCommand(new FilterCommand());
        this.commandRegistry.registerCommand(new UsersCommand());

        jda.addEventListener(commandRegistry);
    }

    public void stopBot() {
        jda.shutdown();
        jda = null;
    }

    public void banLog(String cloudPlayer, PunishType punishType, String reason, String length, String punisher) {
        if (jda != null) {
            Guild guild = jda.getGuildById("620984464285106179");
            TextChannel channel = guild.getTextChannelById("544238415173124127");

            channel.sendMessage(new EmbedBuilder()
                    .setColor(Color.RED)
                    .setTitle("A Player was punished! (Test-Punish)", null)
                    .addField("Name", cloudPlayer, true)
                    .addField("Type", punishType.name().toUpperCase(), true)
                    .addField("Reason", reason, true)
                    .addField("Length", length, true)
                    .setFooter("Punished by " + punisher, "https://minotar.net/avatar/MHF_Question")
                    .build()
            ).queue();
        } else {
            CloudService.getCloudService().getTerminal().writeMessage("Cannot send the BanLog!");
        }
    }

    public void banLog(CloudPlayer cloudPlayer, PunishType punishType, String reason, String length, CloudPlayer punisher) {
        Guild guild = jda.getGuildById("620984464285106179");
        TextChannel channel = guild.getTextChannelById("544238415173124127");

        channel.sendMessage(new EmbedBuilder()
                .setColor(Color.RED)
                .setTitle("A Player was punished!", null)
                .addField("Name", cloudPlayer.getName(), true)
                .addField("Type", punishType.name().toUpperCase(), true)
                .addField("Reason", reason, true)
                .addField("Length", length, true)
                .setFooter("Punished by " + punisher.getName(), "https://minotar.net/avatar/" + punisher.getUniqueId())
                .build()
        ).queue();
    }

    public void banLog(CloudPlayer cloudPlayer, PunishType punishType, CloudPlayer punisher) {
        Guild guild = jda.getGuildById("620984464285106179");
        TextChannel channel = guild.getTextChannelById("544238415173124127");

        channel.sendMessage(new EmbedBuilder()
                .setColor(Color.RED)
                .setTitle("A Player was unpunished!", null)
                .addField("Name", cloudPlayer.getName(), true)
                .addField("Type", punishType.name().toUpperCase(), true)
                .setFooter("Unpunished by " + punisher.getName(), "https://minotar.net/avatar/" + punisher.getUniqueId())
                .build()
        ).queue();
    }

    public void setBotStatus(String name) {
        jda.getPresence().setPresence(OnlineStatus.ONLINE, Activity.watching(name), false);
    }

    public void setBotStatus(Activity.ActivityType activityType, String status) {
        jda.getPresence().setPresence(OnlineStatus.ONLINE, Activity.of(activityType, status), false);
    }

    public void setBotStatus(String name, String url) {
        jda.getPresence().setPresence(OnlineStatus.ONLINE, Activity.streaming(name, url), false);
    }

    public void autoStatus() {
        /*autoStatus = new Timer();
        autoStatus.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (jda != null) {
                    Random rnd = new Random();
                    String status = Main.getBotConfig().getStatusMessages()[rnd.nextInt(Main.getBotConfig().getStatusMessages().length)].replace("%USERS%", String.valueOf(jda.getGuildById("706512965096177702").getMembers().size()));
                    setBotStatus(status);
                }
            }
        }, 1000, 60000);*/
    }

    public void sendPrivateMessage(User user, String content) {
        user.openPrivateChannel().queue((channel) -> {
            channel.sendMessage(content).queue();
        });
    }

    public void sendPrivateMessage(User user, MessageEmbed content) {
        user.openPrivateChannel().queue((channel) -> {
            channel.sendMessage(content).queue();
        });
    }

    public boolean sendErrorReport(Exception ex) {
        if (ex == null) {
            return false;
        }

        Member member = getJda().getGuildById("706512965096177702").getMemberById("525472891035451427");

        MessageEmbed content = new EmbedBuilder()
                .setColor(Color.GREEN)
                .setTitle("Huch, Huston wir haben einen Fehler", null)
                .setFooter("Bot Version: " + Main.getInstance().getVersion(), "https://acp.vicemice.net/assets/images/favicon.png")
                .addField("Code:", ex.getMessage(), true)
                .build();

        sendPrivateMessage(member.getUser(), content);

        ex.printStackTrace();

        return true;
    }
}