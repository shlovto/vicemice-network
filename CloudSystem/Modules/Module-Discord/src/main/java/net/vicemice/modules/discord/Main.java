package net.vicemice.modules.discord;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.network.modules.api.CoreModule;
import net.vicemice.modules.discord.listener.DiscordCheckGroupsListener;
import net.vicemice.modules.discord.listener.DiscordUnverifyListener;
import net.vicemice.modules.discord.listener.DiscordVerifyListener;
import net.vicemice.modules.discord.bot.DiscordBot;
import net.vicemice.modules.discord.config.Config;
import net.vicemice.modules.discord.config.Configuration;
import lombok.Getter;
import net.vicemice.modules.discord.mongo.DiscordService;

public class Main extends CoreModule {

    @Getter
    private static Main instance;
    @Getter
    private static DiscordBot discordBot;
    @Getter
    private static DiscordService discordService;
    @Getter
    private static Configuration botConfiguration;
    @Getter
    private static Config botConfig;

    @Getter
    private DiscordVerifyListener discordVerifyListener = new DiscordVerifyListener();
    @Getter
    private DiscordUnverifyListener discordUnverifyListener = new DiscordUnverifyListener();
    @Getter
    private DiscordCheckGroupsListener discordCheckGroupsListener = new DiscordCheckGroupsListener();

    public void onLoad() {
        instance = this;

        botConfiguration = new Configuration();
        if (!botConfiguration.exists()) {
            Config config = new Config();
            botConfiguration.writeConfiguration(config);
        }
        botConfiguration.readConfiguration();
        botConfig = botConfiguration.getConfig();

        discordBot = new DiscordBot();
        discordBot.startBot();

        discordService = new DiscordService();
        discordService.loadUsers();
        discordService.loadFilter();

        onBootstrap();
    }

    public void onBootstrap() {
        registerListener(discordVerifyListener);
        registerListener(discordUnverifyListener);
        registerListener(discordCheckGroupsListener);
    }

    @Override
    public void onShutdown() {
        CloudService.getCloudService().getManagerService().getEventManager().unregisterListener(discordCheckGroupsListener);
        if (discordBot != null && discordBot.getJda() != null) {
            discordBot.stopBot();
        }
    }
}
