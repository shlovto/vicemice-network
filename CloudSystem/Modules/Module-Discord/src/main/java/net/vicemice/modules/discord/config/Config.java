package net.vicemice.modules.discord.config;

import lombok.Data;

/**
 * Created by Paul B. on 27.01.2019.
 */
@Data
public class Config {
    private String[] quitMessages = new String[] { "Dies ist ein Beispiel text und kann geändert werden mit mehreren Zeilen." };
    private String[] joinMessages = new String[] { "Dies ist ein Beispiel text und kann geändert werden mit mehreren Zeilen." };
    private String[] statusMessages = new String[] { "Bot by elrobtossohn" };
}