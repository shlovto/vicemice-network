package net.vicemice.modules.discord.listener;

import net.vicemice.hector.network.modules.api.event.discord.DiscordVerifyEvent;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.event.IEventListener;
import net.vicemice.modules.discord.Main;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;

import java.util.concurrent.atomic.AtomicBoolean;

public class DiscordVerifyListener implements IEventListener<DiscordVerifyEvent> {

    @Override
    public void onCall(DiscordVerifyEvent event) {
        CloudPlayer cloudPlayer = event.getCloudPlayer();
        if (Main.getDiscordBot() == null || Main.getDiscordBot().getJda() == null) return;
        if (!cloudPlayer.getDiscordId().equalsIgnoreCase("noid")) {
            cloudPlayer.sendMessage("discord-not-connected");
            return;
        }

        Member member = Main.getDiscordBot().getJda().getGuildById("706512965096177702").getMemberById(event.getUniqueId());

        if (member != null) {
            cloudPlayer.setDiscordId(event.getUniqueId());
            cloudPlayer.updateToDatabase(false);
            cloudPlayer.sendMessage("discord-success-added");

            for (Role role : Main.getDiscordBot().getJda().getGuildById("706512965096177702").getMemberById(event.getUniqueId()).getRoles()) {
                if (role.getId().equalsIgnoreCase("706518701121470505")) continue;
                if (role.getId().equalsIgnoreCase("706512965096177702")) continue;
                if (cloudPlayer.getRank().containsDiscordRank(role.getId())) continue;
                Main.getDiscordBot().getJda().getGuildById("706512965096177702").removeRoleFromMember(member, role).queue();
            }

            for (String s : cloudPlayer.getRank().getDiscordGroups()) {
                Role role = Main.getDiscordBot().getJda().getRoleById(s);
                Main.getDiscordBot().getJda().getGuildById("706512965096177702").addRoleToMember(member, role).queue();
            }
        } else {
            cloudPlayer.sendMessage("discord-error-added");
        }
    }
}
