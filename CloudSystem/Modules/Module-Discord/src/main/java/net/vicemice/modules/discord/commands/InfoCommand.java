package net.vicemice.modules.discord.commands;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.vicemice.hector.CloudService;
import net.vicemice.modules.discord.Main;
import net.vicemice.modules.discord.dep.command.Command;
import net.vicemice.modules.discord.dep.command.CommandResponse;
import net.vicemice.modules.discord.dep.util.HttpRequest;
import net.vicemice.modules.discord.dep.util.TimeManager;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;

import java.awt.*;
import java.io.IOException;

public class InfoCommand extends Command {

    public InfoCommand() {
        super("info", "", "Zeigt dir Informationen an");
    }

    @Override
    public CommandResponse triggerCommand(Message message, String[] args) {
        //if (message.getChannel().getId().equalsIgnoreCase("594871307967922176") ||
        //        message.getChannel().getId().equalsIgnoreCase("615681996672467009")) {
            try {
                if (args.length == 1) {
                    String id = args[0].replace("<", "").replace(">", "").replace("@", "");
                    try {
                        Member user = Main.getDiscordBot().getJda().getGuildById("620984464285106179").getMemberById(id);
                        if (user != null) {
                            HttpRequest.RequestResponse response = null;

                            try {
                                HttpRequest.RequestBuilder builder = new HttpRequest.RequestBuilder("https://api.network.smochy.de/", HttpRequest.HttpRequestMethod.GET);
                                builder.addHeader("token", "hhnBMvrepNbK2VVsNY3C68jNpkanwzDHZdc2RaZXFyut2yPbwFaPrfnAqXGCr9xM");
                                builder.addHeader("request", "player");
                                builder.addHeader("info", "discordVerify");
                                builder.addHeader("id", user.getUser().getId());
                                response = HttpRequest.performRequest(builder);
                            } catch (IOException e) {
                                e.printStackTrace();
                                return CommandResponse.ACCEPTED;
                            }

                            String json = response.getResultMessage();
                            JsonParser jsonParser = new JsonParser();
                            JsonObject jsonObject = (JsonObject)jsonParser.parse(json);

                            boolean verified = true;

                            if (jsonObject.has("message")) {
                                verified = false;
                            }

                            if (verified) {
                                String name = jsonObject.getAsJsonObject("player").getAsJsonPrimitive("username").toString();

                                message.getTextChannel().sendMessage(new EmbedBuilder()
                                        .setColor(Color.GREEN)
                                        .setTitle("User Informationen", null)
                                        .setFooter("ViceMice.net", "https://acp.vicemice.net/assets/images/favicon.png")
                                        .addField("Name", "`" + user.getEffectiveName() + "`", true)
                                        .addField("Verifiziert", "`Ja`", true)
                                        .addField("Minecraft-Name", "`" + name + "`", true)
                                        .build()
                                ).queue();
                            } else {
                                message.getTextChannel().sendMessage(new EmbedBuilder()
                                        .setColor(Color.GREEN)
                                        .setTitle("User Informationen", null)
                                        .setFooter("ViceMice.net", "https://acp.vicemice.net/assets/images/favicon.png")
                                        .addField("Name", "`" + user.getEffectiveName() + "`", true)
                                        .addField("Verifiziert", "`Nein`", true)
                                        .build()
                                ).queue();
                            }
                        } else {
                            message.getTextChannel().sendMessage(new EmbedBuilder()
                                    .setColor(Color.RED)
                                    .setTitle("User Informationen", null)
                                    .setFooter("Mhm, es scheint so auszusehen als wäre hier etwas schief gegangen... o.ö", "https://acp.vicemice.net/assets/images/favicon.png")
                                    .addField("Information", "Der User wurde nicht gefunden.", false)
                                    .build()
                            ).queue();
                        }
                    } catch (NumberFormatException ex) {
                        message.getTextChannel().sendMessage(new EmbedBuilder()
                                .setColor(Color.RED)
                                .setTitle("User Informationen", null)
                                .setFooter("Mhm, es scheint so auszusehen als wäre hier etwas schief gegangen... o.ö", "https://acp.vicemice.net/assets/images/favicon.png")
                                .addField("Information", "Der User wurde nicht gefunden.", false)
                                .build()
                        ).queue();
                    }
                } else {
                    message.getTextChannel().sendMessage(new EmbedBuilder()
                            .setColor(Color.GREEN)
                            .setTitle("Bot/Server Informationen", null)
                            .setFooter("ViceMice.net Bot v" + Main.getInstance().getVersion(), "https://acp.vicemice.net/assets/images/favicon.png")
                            .addField("Laufzeit", "`" + new TimeManager().getRemainingTimeString(CloudService.getCloudService().getStartTime()) + "`", true)
                            .addField("Version", "`" + Main.getInstance().getVersion() + "`", true)
                            .addField("Server-Name", "`" + message.getTextChannel().getGuild().getName() + "`", true)
                            .addField("Server-Besitzer", message.getTextChannel().getGuild().getOwner().getAsMention(), true)
                            .build()
                    ).queue();
                }
            } catch (Exception ex) {
                message.getTextChannel().sendMessage(new EmbedBuilder()
                        .setColor(Color.RED)
                        .setTitle("Was ist hier bitte passiert?", null)
                        .setFooter("Ich denke mal, das hier dürfte nicht sein...", "https://acp.vicemice.net/assets/images/favicon.png")
                        .addField("Information", "Der Fehler wurde bereits an unseren Entwickler übermittelt.", false)
                        .build()
                ).queue();
                Main.getDiscordBot().sendErrorReport(ex);
            }

            //return CommandResponse.ACCEPTED;
        /*} else {
            try {
                message.getTextChannel().sendMessage("Dieser Befehl funktioniert nur in " + message.getTextChannel().getGuild().getTextChannelById("615681996672467009").getAsMention()).queue();
            } catch (NullPointerException ex) {
                message.getTextChannel().sendMessage(new EmbedBuilder()
                        .setColor(Color.RED)
                        .setTitle("Was ist hier bitte passiert?", null)
                        .setFooter("Ich denke mal, das hier dürfte nicht sein...", "https://ow.ploxh.de/assets/media/logos/logo.png")
                        .addField("Information", "Der Fehler wurde bereits an unseren Entwickler übermittelt.", false)
                        .build()
                ).queue();
                DiscordBot.sendErrorReport(ex);
            }
        }*/

        //message.delete().queue();

        //return null;
        return CommandResponse.ACCEPTED;
    }
}