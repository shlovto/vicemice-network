package net.vicemice.modules.discord.commands;

import net.vicemice.modules.discord.dep.command.Command;
import net.vicemice.modules.discord.dep.command.CommandResponse;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

import java.awt.*;

public class StatsCommand extends Command {

    public StatsCommand() {
        super("stats", "", "Zeigt dir Stats an");
    }

    @Override
    public CommandResponse triggerCommand(Message message, String[] args) {
        //if (message.getChannel().getId().equalsIgnoreCase("594871307967922176") ||
        //       message.getChannel().getId().equalsIgnoreCase("615681996672467009")) {

        if (args.length == 0) {
            message.getTextChannel().sendMessage(new EmbedBuilder()
                    .setColor(Color.RED)
                    .setTitle("Was ist hier passiert?", null)
                    .setFooter("Statistics are managed by ViceMice.net", "https://acp.vicemice.net/assets/images/favicon.png")
                    .setDescription("Du musst einen Spielernamen angeben")
                    .build()
            ).queue();
            return CommandResponse.ACCEPTED;
        }

        if (args.length == 1) {
            message.getTextChannel().sendMessage(new EmbedBuilder()
                    .setColor(Color.ORANGE)
                    .setTitle("Not available here", null)
                    .setFooter("Statistics are managed by ViceMice.net", "https://acp.vicemice.net/assets/images/favicon.png")
                    .setDescription("Here you can view the statistics: https://ViceMice.net/stats/" + args[0])
                    .build()
            ).queue();
            return CommandResponse.ACCEPTED;
        }
        return CommandResponse.ACCEPTED;
    }
}