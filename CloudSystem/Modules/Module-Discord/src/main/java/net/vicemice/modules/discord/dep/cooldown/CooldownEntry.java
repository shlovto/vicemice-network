package net.vicemice.modules.discord.dep.cooldown;

import lombok.Getter;
import net.dv8tion.jda.api.entities.Member;
import java.util.HashMap;

public class CooldownEntry {

    private HashMap<String, Long> cooldowns = new HashMap<>();
    @Getter
    private int length;

    public CooldownEntry(int length) {
        this.length = length;
    }

    public void addMember(Member member) {
        if (!containsMember(member))
            cooldowns.put(member.getUser().getId(), System.currentTimeMillis());
    }

    public void removeMember(Member member) {
        if (containsMember(member))
            cooldowns.remove(member.getUser().getId());
    }

    public boolean containsMember(Member member) {
        return cooldowns.containsKey(member.getUser().getId());
    }

    public long getCooldownStart(Member member) {
        if (!containsMember(member)) return -1;
        return cooldowns.get(member.getUser().getId());
    }
}