package net.vicemice.modules.discord.listener;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.language.Language;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.ComponentMessagePacket;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.vicemice.modules.discord.Main;

import java.util.ArrayList;

public class MessageReceivedListener extends ListenerAdapter {

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        Member member = event.getMember();
        if (member.getUser().isBot()) return;

        if (Main.getDiscordService().containsWord(event.getMessage().getContentRaw().toLowerCase())) {
            event.getMessage().delete().queue();
            event.getMessage().getTextChannel().sendMessage("Watch your chat behaviour " + event.getMessage().getAuthor().getAsMention()).queue();
            return;
        }

        if(event.getChannel().getId().equalsIgnoreCase("706517236914847814")) {
            event.getMessage().delete().queue();

            CloudPlayer verified = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByDiscordID(member.getUser().getId());

            if (verified != null) {
                sendPrivateMessage(member.getUser(), verified.getLanguageMessage("discord-linked-with", verified.getName()));
                return;
            }


            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(event.getMessage().getContentRaw());

            if(cloudPlayer != null && cloudPlayer.getProxy() != null && cloudPlayer.isOnline()) {
                if (cloudPlayer.getDiscordId().equals("noid")) {
                    cloudPlayer.sendMessage("discord-verify-verification", member.getUser().getName());
                    ArrayList<String[]> messageData = new ArrayList<>();
                    messageData.add(new String[]{cloudPlayer.getLanguageMessage("confirm-click"), "true", "run_command", "/link discord " + member.getUser().getId()});
                    ComponentMessagePacket componentMessagePacket = new ComponentMessagePacket(cloudPlayer.getName().toLowerCase(), messageData);
                    cloudPlayer.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMPONENT_MESSAGE_PACKET, componentMessagePacket));

                    sendPrivateMessage(member.getUser(), "Now confirm on the network that you want to verify.");
                } else {
                    sendPrivateMessage(member.getUser(), "Sorry, an error has occurred.");
                    sendPrivateMessage(member.getUser(), "You either want to verify yourself with an account that you have already verified");
                    sendPrivateMessage(member.getUser(), "Or this account is not yours.");
                }
            } else {
                sendPrivateMessage(member.getUser(), event.getMessage().getContentRaw() + " is not on ´ViceMice.net´ or ´verify.ViceMice.net´");
            }
        }
    }

    public void sendPrivateMessage(User user, String content) {
        user.openPrivateChannel().queue((channel) -> {
            channel.sendMessage(content).queue();
        });
    }
}