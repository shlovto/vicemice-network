package net.vicemice.modules.discord.listener;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.modules.discord.Main;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.vicemice.modules.discord.mongo.DiscordUser;

public class GuildMemberJoinListener extends ListenerAdapter {

    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        Member member = event.getMember();
        if (!Main.getDiscordService().getUsers().containsKey(member.getId())) {
            Main.getDiscordService().getUsers().put(member.getId(), new DiscordUser(member.getId(), false, "", -1, 0L));
            CloudService.getCloudService().getDatabaseQueue().addToQueue(Main.getDiscordService().getUsers().get(member.getId()), true);
        }

        if (event.getGuild().getId().equalsIgnoreCase("706512965096177702")) {
            event.getGuild().getTextChannelById("706518477262946412").sendMessage(member.getAsMention() + " joined the discord server").queue();

            if (Main.getDiscordService().getUsers().containsKey(member.getId())) {
                if (Main.getDiscordService().getUsers().get(member.getId()).isMuted()) {
                    Role role = Main.getDiscordBot().getJda().getRoleById("706518701121470505");
                    Main.getDiscordBot().getJda().getGuildById("706512965096177702").addRoleToMember(member, role).queue();
                }
            }

            if (CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByDiscordID(member.getUser().getId()) != null) {
                CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByDiscordID(member.getUser().getId());

                for (String s : cloudPlayer.getRank().getDiscordGroups()) {
                    Role role = Main.getDiscordBot().getJda().getRoleById(s);
                    Main.getDiscordBot().getJda().getGuildById("706512965096177702").addRoleToMember(member, role).queue();
                }
                return;
            } else {
                Role role = Main.getDiscordBot().getJda().getRoleById("620986143596478485");
                Main.getDiscordBot().getJda().getGuildById("706512965096177702").addRoleToMember(member, role).queue();
            }

            Role role = Main.getDiscordBot().getJda().getRoleById("706512965096177702");
            Main.getDiscordBot().getJda().getGuildById("706512965096177702").addRoleToMember(member, role).queue();
        } else {
            System.out.println("§cUnsupported Discord Guild '"+ event.getGuild().getId()+"'");
        }
    }
}