package net.vicemice.modules.discord.dep.thread;

public interface ThreadRunner {
    public void start();

    public void stop();

    public Thread getThread();

    public Runnable getOriginalRunable();
}