package net.vicemice.modules.discord.dep.util;

import net.vicemice.hector.CloudService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class TimeManager {
    static DateFormat format = new SimpleDateFormat("dd.MM.yyyy - HH:mm:ss");
    DateFormat hourFormat = new SimpleDateFormat("HH");

    public String getEndeAnzeige(long ende) {
        if (ende < 1) {
            return "-1 Error";
        }
        try {
            Date date = new Date(ende);
            return format.format(date);
        }
        catch (Exception ex) {
            CloudService.getCloudService().getTerminal().writeMessage("Konnte Datum nicht herstellen!");
            CloudService.getCloudService().getTerminal().writeMessage("Variable ende: " + ende);
            CloudService.getCloudService().getTerminal().writeMessage(Arrays.deepToString(ex.getStackTrace()));
            return "31.12.2020 - 23:59:59";
        }
    }

    public String getHour() {
        Date date = new Date(System.currentTimeMillis());
        return this.hourFormat.format(date);
    }

    public String getRemainingTimeString(long endTime) {
        String append;
        String[] end = this.getRemainingTime(endTime);
        StringBuilder stringBuilder = new StringBuilder();
        int days = Integer.valueOf(end[0]);
        int hours = Integer.valueOf(end[1]);
        int minutes = Integer.valueOf(end[2]);
        int seconds = Integer.valueOf(end[3]);
        if (days != 0) {
            append = days == 1 ? days + " Tag " : days + " Tage ";
            stringBuilder.append(append);
        }
        if (hours != 0) {
            append = hours == 1 ? hours + " Stunde " : hours + " Stunden ";
            stringBuilder.append(append);
        }
        if (minutes != 0) {
            append = minutes == 1 ? minutes + " Minute " : minutes + " Minuten ";
            stringBuilder.append(append);
        }
        if (seconds != 0) {
            append = seconds == 1 ? seconds + " Sekunde" : seconds + " Sekunden";
            stringBuilder.append(append);
        }
        return stringBuilder.toString();
    }

    private String[] getRemainingTime(long milli) {
        long zeit = 0;
        zeit = System.currentTimeMillis() > milli ? System.currentTimeMillis() - milli : milli - System.currentTimeMillis();
        int tage = 0;
        int stunden = 0;
        int minuten = 0;
        int sekunden = 0;
        if ((zeit /= 1000) >= 86400) {
            tage = (int)zeit / 86400;
        }
        if ((zeit -= (tage * 86400)) > 3600) {
            stunden = (int)zeit / 3600;
        }
        if ((zeit -= (stunden * 3600)) > 60) {
            minuten = (int)zeit / 60;
        }
        sekunden = (int)(zeit -= (minuten * 60));
        String[] al = new String[]{Integer.toString(tage), Integer.toString(stunden), Integer.toString(minuten), Integer.toString(sekunden)};
        return al;
    }
}

