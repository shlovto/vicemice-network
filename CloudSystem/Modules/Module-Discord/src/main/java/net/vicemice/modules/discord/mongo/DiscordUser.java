package net.vicemice.modules.discord.mongo;

import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.utils.SaveEntry;
import org.bson.Document;

/*
 * Class created at 19:14 - 19.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Getter
public class DiscordUser implements SaveEntry {

    private String id = "-1";
    private boolean muted = false;
    private String mute_reason = "";
    private long mute_time = -1, points = 0L;

    public DiscordUser(String id, boolean muted, String mute_reason, long mute_time, long points) {
        this.id = id;
        this.muted = muted;
        this.mute_reason = mute_reason;
        this.mute_time = mute_time;
        this.points = points;
    }

    public void addPoint() {
        this.points = this.points+1;
    }

    public void removePoint() {
        if (this.points<=0) return;
        this.points = this.points-1;
    }

    @Override
    public void save(boolean insert) {
        if (insert) {
            Document document = new Document("id", id);
            document.put("muted", muted);
            document.put("mute_reason", mute_reason);
            document.put("mute_time", mute_time);
            document.put("points", points);

            CloudService.getCloudService().getManagerService().getMongoManager().getDiscordDatabase().getCollection("users").insertOne(document);
        }

        Document document = new Document("muted", muted);
        document.put("mute_reason", mute_reason);
        document.put("mute_time", mute_time);
        document.put("points", points);

        Document filter = new Document("id", id);
        CloudService.getCloudService().getManagerService().getMongoManager().getDiscordDatabase().getCollection("users").updateOne(filter, new Document("$set", document));
    }
}