package net.vicemice.modules.discord.commands;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.modules.discord.Main;
import net.vicemice.modules.discord.dep.command.Command;
import net.vicemice.modules.discord.dep.command.CommandResponse;
import net.vicemice.modules.discord.mongo.DiscordUser;

import java.awt.*;

/*
 * Class created at 15:29 - 13.03.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class UsersCommand extends Command {

    public UsersCommand() {
        super("users", "", "User Verwaltung");
    }

    @Override
    public CommandResponse triggerCommand(Message message, String[] args) {
        CloudPlayer author = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByDiscordID(message.getAuthor().getId());
        if (author == null || !author.getRank().isAdmin()) {
            return CommandResponse.ACCEPTED;
        }

        try {
            if (args.length != 0) {
                if (args[0].equalsIgnoreCase("addAll")) {
                    int i = 0;
                    for (Member member : Main.getDiscordBot().getJda().getGuildById("620984464285106179").getMembers()) {
                        if (!Main.getDiscordService().getUsers().containsKey(member.getId())) {
                            Main.getDiscordService().getUsers().put(member.getId(), new DiscordUser(member.getId(), false, "", -1, 0L));
                            CloudService.getCloudService().getDatabaseQueue().addToQueue(Main.getDiscordService().getUsers().get(member.getId()), true);
                            i++;
                        }
                    }
                    for (Member member : Main.getDiscordBot().getJda().getGuildById("687993133325090816").getMembers()) {
                        if (!Main.getDiscordService().getUsers().containsKey(member.getId())) {
                            Main.getDiscordService().getUsers().put(member.getId(), new DiscordUser(member.getId(), false, "", -1, 0L));
                            CloudService.getCloudService().getDatabaseQueue().addToQueue(Main.getDiscordService().getUsers().get(member.getId()), true);
                            i++;
                        }
                    }

                    message.getTextChannel().sendMessage(new EmbedBuilder()
                            .setColor(Color.GREEN)
                            .setTitle("Erfolgreich", null)
                            .setFooter("ViceMice.net", "https://acp.vicemice.net/assets/images/favicon.png")
                            .addField("Information", "Es wurden ´"+i+"´ User hinzugefügt!", false)
                            .build()
                    ).queue();
                } else {
                    message.getTextChannel().sendMessage(new EmbedBuilder()
                            .setColor(Color.RED)
                            .setTitle("Fehler", null)
                            .setFooter("ViceMice.net", "https://acp.vicemice.net/assets/images/favicon.png")
                            .addField("Information", "Dieser Sub-Command wurde nicht gefunden", false)
                            .build()
                    ).queue();
                }
            } else {
                message.getTextChannel().sendMessage(new EmbedBuilder()
                        .setColor(Color.RED)
                        .setTitle("Fehler", null)
                        .setFooter("ViceMice.net", "https://acp.vicemice.net/assets/images/favicon.png")
                        .addField("Information", "Für diesen Befehl werden mehr Argumente benötigt", false)
                        .build()
                ).queue();
            }
        } catch (Exception ex) {
            message.getTextChannel().sendMessage(new EmbedBuilder()
                    .setColor(Color.RED)
                    .setTitle("Fehler", null)
                    .setFooter("ViceMice.net", "https://acp.vicemice.net/assets/images/favicon.png")
                    .addField("Information", "Der Fehler wurde bereits an unseren Entwickler übermittelt.", false)
                    .build()
            ).queue();
            Main.getDiscordBot().sendErrorReport(ex);
        }

        return CommandResponse.ACCEPTED;
    }
}
