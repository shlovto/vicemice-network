package net.vicemice.modules.autobroadcaster.timer;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.language.Language;
import net.vicemice.modules.autobroadcaster.Main;

import java.util.ArrayList;
import java.util.TimerTask;
import java.util.UUID;

public class AutoTimer extends TimerTask {

    @Override
    public void run() {
        for (UUID uniqueId : CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers()) {
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uniqueId);
            if (!cloudPlayer.getServer().getName().startsWith("Lobby")) continue;
            if (cloudPlayer.getLanguage() == Language.LanguageType.GERMAN) {
                ArrayList<String[]> messageData = new ArrayList<>();
                messageData.add(Main.getMessageDataDE().get(Main.message));
                cloudPlayer.sendMessage(messageData);
            } else {
                ArrayList<String[]> messageData = new ArrayList<>();
                messageData.add(Main.getMessageDataEN().get(Main.message));
                cloudPlayer.sendMessage(messageData);
            }
        }

        if (Main.getMessageDataDE().size() == (Main.message+1)) {
            Main.message = 0;
        } else {
            Main.message++;
        }
    }
}