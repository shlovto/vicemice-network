package net.vicemice.modules.autobroadcaster;

import net.vicemice.hector.network.modules.api.CoreModule;
import net.vicemice.modules.autobroadcaster.timer.AutoTimer;
import lombok.Getter;
import java.util.ArrayList;
import java.util.Timer;

public class Main extends CoreModule {

    public static Integer message = 0;
    public Timer timer;
    @Getter
    public static ArrayList<String[]> messageDataDE, messageDataEN;

    public void onLoad() {
        onBootstrap();
    }

    public void onBootstrap() {
        messageDataDE = new ArrayList<>();
        messageDataEN = new ArrayList<>();

        timer = new Timer();
        timer.scheduleAtFixedRate(new AutoTimer(), 1000, 1200000);
    }

    @Override
    public void onShutdown() {
        timer.cancel();
    }
}