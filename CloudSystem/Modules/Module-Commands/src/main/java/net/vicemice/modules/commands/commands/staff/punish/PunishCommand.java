package net.vicemice.modules.commands.commands.staff.punish;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.packets.types.player.report.ReportPlayersPacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.players.punish.Punish;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class PunishCommand extends Command {

    public PunishCommand() {
        super(Area.MODERATING);
    }

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (args.length == 1 && args[0].equalsIgnoreCase("templates")) {
            sendCauses(sender);
            return;
        }
        if (args.length >= 2) {
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[0]);
            if (cloudPlayer != null) {
                if (cloudPlayer.getRank().isTeam() && !sender.getRank().isAdmin()) {
                    sender.sendMessage("can-not-punish");
                    return;
                }
                Punish punish = CloudService.getCloudService().getManagerService().getPunishManager().getByWord(args[1]);
                if (punish != null && punish.isViewAble()) {
                    Optional<ReportPlayersPacket.ReportEntry> entries = CloudService.getCloudService().getManagerService().getReportManager().getReport(cloudPlayer);
                    if (entries.isPresent()) {
                        sender.sendMessage("command-punish-reported");
                        return;
                    }

                    if (punish.getType().equalsIgnoreCase("BAN")) {
                        if (cloudPlayer.getAbuses().getBan() != null) {
                            sender.sendMessage("command-punish-already-punished", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()));
                            return;
                        }

                        if (sender.getRank().getAccessLevel() < punish.getAccessLevel()) {
                            sender.sendMessage("command-punish-template-not-useable");
                            return;
                        }

                        String comment = (args.length > 2 ? args[2] : (cloudPlayer.getServer() != null ? cloudPlayer.getServer().getGameID() : null));

                        if (punish.isReplayRequired() && comment == null) {
                            sender.sendMessage("command-punish-template-no-gameid");
                            return;
                        }
                        if (punish.isChatLogRequired() && comment == null) {
                            comment = "C:"+CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().createChatLog(UUID.fromString("336991c8-1199-457c-80bb-649042a489f5"), cloudPlayer.getUniqueId(), sender.getServer().getServerID(), false);
                            sender.sendMessage("command-punish-template-auto-chatlog");
                        }

                        if (comment == null) {
                            sender.sendMessage("command-punish-template-invalid-comment");
                            return;
                        }
                        if (!comment.startsWith("R:") && !comment.startsWith("YT:") && !comment.startsWith("C:") && !comment.startsWith("PRNTSC:")) {
                            sender.sendMessage("command-punish-template-invalid-comment");
                            return;
                        }

                        if (cloudPlayer.getServer() != null && punish.isReplayRequired() && !comment.equalsIgnoreCase(cloudPlayer.getServer().getGameID()))
                            sender.sendMessage("command-punish-template-self-comment");
                        if (cloudPlayer.getServer() != null && cloudPlayer.getServer().getGameID() != null && comment.equalsIgnoreCase(cloudPlayer.getServer().getGameID()))
                            sender.sendMessage("command-punish-template-auto-gameid");

                        TimeUnit time = null;
                        long value = -1L;
                        try {
                            final String duration = (cloudPlayer.getSetting(punish.getKey().toUpperCase(), Integer.class) <= (punish.getWaves().size()-1) ? punish.getWaves().get(cloudPlayer.getSetting(punish.getKey().toUpperCase(), Integer.class)) : "permanently");
                            if (duration.endsWith("s")) {
                                time = TimeUnit.SECONDS;
                                value = Long.parseLong(duration.split("s")[0]);
                            } else if (duration.endsWith("m")) {
                                time = TimeUnit.MINUTES;
                                value = Long.parseLong(duration.split("m")[0]);
                            } else if (duration.endsWith("h")) {
                                time = TimeUnit.HOURS;
                                value = Long.parseLong(duration.split("h")[0]);
                            } else if (duration.endsWith("d")) {
                                time = TimeUnit.DAYS;
                                value = Long.parseLong(duration.split("d")[0]);
                            } else if (duration.equalsIgnoreCase("permanently")) {
                                time = TimeUnit.SECONDS;
                                value = 0;
                            }
                        } catch (Exception ignored) {}

                        cloudPlayer.setSetting(punish.getKey().toUpperCase(), (cloudPlayer.getSetting(punish.getKey().toUpperCase(), Integer.class)+1));
                        CloudService.getCloudService().getManagerService().getAbuseManager().punish(cloudPlayer, sender, punish.getKey(), comment, (value == 0 ? 0 : (System.currentTimeMillis()+time.toMillis(value))), false, false);
                    } else if (punish.getType().equalsIgnoreCase("MUTE")) {
                        if (cloudPlayer.getAbuses().getMute() != null) {
                            sender.sendMessage("command-punish-already-punished", (cloudPlayer.getRank().getRankColor() + cloudPlayer.getName()));
                            return;
                        }

                        if (sender.getRank().getAccessLevel() < punish.getAccessLevel()) {
                            sender.sendMessage("command-punish-template-not-useable");
                            return;
                        }

                        String comment = (args.length > 2 ? args[2] : null);

                        if (punish.isReplayRequired() && comment == null) {
                            sender.sendMessage("command-punish-template-no-gameid");
                            return;
                        }
                        if (punish.isChatLogRequired() && comment == null) {
                            comment = "C:" + CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().createChatLog(UUID.fromString("336991c8-1199-457c-80bb-649042a489f5"), cloudPlayer.getUniqueId(), sender.getServer().getServerID(), false);
                            sender.sendMessage("command-punish-template-auto-chatlog");
                        }

                        if (comment == null) {
                            sender.sendMessage("command-punish-template-invalid-comment");
                            return;
                        }
                        if (!comment.startsWith("R:") && !comment.startsWith("YT:") && !comment.startsWith("C:") && !comment.startsWith("PRNTSC:")) {
                            sender.sendMessage("command-punish-template-invalid-comment");
                            return;
                        }

                        if (punish.isChatLogRequired() && comment != null)
                            sender.sendMessage("command-punish-template-self-comment");

                        TimeUnit time = null;
                        long value = -1L;
                        try {
                            final String duration = (cloudPlayer.getSetting(punish.getKey().toUpperCase(), Integer.class) <= (punish.getWaves().size()-1) ? punish.getWaves().get(cloudPlayer.getSetting(punish.getKey().toUpperCase(), Integer.class)) : "permanently");
                            if (duration.endsWith("s")) {
                                time = TimeUnit.SECONDS;
                                value = Long.parseLong(duration.split("s")[0]);
                            } else if (duration.endsWith("m")) {
                                time = TimeUnit.MINUTES;
                                value = Long.parseLong(duration.split("m")[0]);
                            } else if (duration.endsWith("h")) {
                                time = TimeUnit.HOURS;
                                value = Long.parseLong(duration.split("h")[0]);
                            } else if (duration.endsWith("d")) {
                                time = TimeUnit.DAYS;
                                value = Long.parseLong(duration.split("d")[0]);
                            } else if (duration.equalsIgnoreCase("permanently")) {
                                time = TimeUnit.SECONDS;
                                value = 0;
                            }
                        } catch (Exception ignored) {}

                        cloudPlayer.setSetting(punish.getKey().toUpperCase(), (cloudPlayer.getSetting(punish.getKey().toUpperCase(), Integer.class)+1));
                        CloudService.getCloudService().getManagerService().getAbuseManager().punish(cloudPlayer, sender, punish.getKey(), comment, (value == 0 ? 0 : (System.currentTimeMillis()+time.toMillis(value))), true, false);
                    }
                } else {
                    sender.sendMessage("command-punish-template-not-found", args[1]);
                }
            } else {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("team-prefix"));
            }
        } else {
            sender.sendMessage("command-punish-help");
        }
    }

    public static void sendCauses(CloudPlayer cloudPlayer) {
        cloudPlayer.sendMessage("command-punish-available-templates");

        for (Punish punish : CloudService.getCloudService().getManagerService().getPunishManager().getReasons()) {
            if (!punish.isViewAble()) continue;
            cloudPlayer.sendRawMessage("§7- §c" + cloudPlayer.getLanguageMessage(punish.getKey()) + " §8[§4" + punish.getType() + "§8]");
        }
    }
}
