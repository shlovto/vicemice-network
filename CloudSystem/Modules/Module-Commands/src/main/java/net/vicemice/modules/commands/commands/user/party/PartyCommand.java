package net.vicemice.modules.commands.commands.user.party;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.party.Party;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.language.Language;
import net.vicemice.hector.utils.players.utils.DataUpdateType;

public class PartyCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (sender.getPlayTime() < 60 && !sender.getRank().isHigherEqualsLevel(Rank.VIP)) {
            sender.sendMessage("party-min-playtime-hours", 3);
            return;
        } else if (sender.getPlayTime() < 120 && !sender.getRank().isHigherEqualsLevel(Rank.VIP)) {
            sender.sendMessage("party-min-playtime-hours", 2);
            return;
        } else if (sender.getPlayTime() == 120 && !sender.getRank().isHigherEqualsLevel(Rank.VIP)) {
            sender.sendMessage("party-min-playtime-hours", 1);
            return;
        } else if (sender.getPlayTime() < 180 && !sender.getRank().isHigherEqualsLevel(Rank.VIP)) {
            sender.sendMessage("party-min-playtime-minutes", (180- sender.getPlayTime()));
            return;
        }

        if (args.length == 0) {
            this.sendHelp(sender);
        } else if (args[0].equalsIgnoreCase("2")) {
            this.sendHelp2(sender);
        } else if (args[0].equalsIgnoreCase("toggle")) {
            if (sender.getSetting("party-invites", Integer.class) == 0) {
                sender.sendMessage("party-toggle-friends-only");
                sender.setSetting("party-invites", 1);
                return;
            } else if (sender.getSetting("party-invites", Integer.class) == 1) {
                sender.sendMessage("party-toggle-no-one");
                sender.setSetting("party-invites", 2);
                return;
            } else if (sender.getSetting("party-invites", Integer.class) == 2) {
                sender.sendMessage("party-toggle-everyone");
                sender.setSetting("party-invites", 0);
                return;
            }
            sender.sendData(sender.getServer(), DataUpdateType.UPDATE_SETTINGS, true);
        } else if (args[0].equalsIgnoreCase("public")) {
            if (sender.getParty() != null) {
                if (sender.getParty().getLeader() == sender) {
                    sender.getParty().setPublic();
                } else {
                    sender.sendMessage("party-status-leader-only");
                }
            } else {
                sender.sendMessage("party-not-in");
            }
        } else if (args[0].equalsIgnoreCase("join")) {
            if (args.length != 2) {
                sender.sendMessage("command-party-join-usage");
                return;
            }
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
            if (cloudPlayer != null) {
                if (sender.getParty() != null) {
                    sender.sendMessage("party-already-in");
                    return;
                }
                if (cloudPlayer.getParty() != null) {
                    cloudPlayer.getParty().joinParty(sender);
                } else {
                    sender.sendMessage("player-has-no-party");
                }
            } else {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("party-prefix"));
            }
        } else if (args[0].equalsIgnoreCase("accept")) {
            if (args.length != 2) {
                sender.sendMessage("command-party-accept-usage");
                return;
            }
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
            if (cloudPlayer != null) {
                if (sender.getParty() != null) {
                    sender.sendMessage("party-already-in");
                    return;
                }
                if (!cloudPlayer.isOnline()) {
                    sender.sendMessage("user-not-online", sender.getLanguageMessage("party-prefix"), (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()));
                    return;
                }
                if (cloudPlayer.getParty() != null) {
                    cloudPlayer.getParty().acceptInvite(sender);
                } else {
                    sender.sendMessage("player-has-no-party");
                }
            } else {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("party-prefix"));
            }
        } else if (args[0].equalsIgnoreCase("leave")) {
            if (sender.getParty() != null) {
                sender.getParty().memberLeave(sender);
            } else {
                sender.sendMessage("party-not-in");
            }
        } else if (args[0].equalsIgnoreCase("list")) {
            if (sender.getParty() != null) {
                StringBuilder stringBuilder = new StringBuilder();
                int current = 0;
                for (CloudPlayer cloudPlayer : sender.getParty().getMembers()) {
                    if (cloudPlayer.equals(sender.getParty().getLeader())) continue;
                    if (current == sender.getParty().getMembers().size() - 2) {
                        stringBuilder.append(cloudPlayer.getRank().getRankColor() + cloudPlayer.getName());
                    } else {
                        stringBuilder.append(cloudPlayer.getRank().getRankColor() + cloudPlayer.getName() + "§7, ");
                    }
                    ++current;
                }

                sender.sendMessage("command-party-list-format", (sender.getParty().getLeader().getRank().getRankColor()+sender.getParty().getLeader().getName()), sender.getParty().getMembers().size(), (current > 1 ? "\n"+stringBuilder.toString() : ""));
            } else {
                sender.sendMessage("party-not-in");
            }
        } else if (args[0].equalsIgnoreCase("invite")) {
            if (args.length != 2) {
                sender.sendMessage("command-party-invite-usage");
                return;
            }
            if (sender.getParty() == null) {
                sender.setParty(new Party(sender));
            }
            if (sender.getParty() != null) {
                if (sender.getParty().getLeader() == sender) {
                    CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
                    if (getPlayer == null) {
                        sender.sendMessage("user-not-exist", sender.getLanguageMessage("party-prefix"));
                        return;
                    }
                    if (!getPlayer.isOnline()) {
                        sender.sendMessage("user-not-online", sender.getLanguageMessage("party-prefix"), (getPlayer.getRank().getRankColor()+getPlayer.getName()));
                        return;
                    }
                    if (getPlayer.getParty() != null) {
                        sender.sendMessage("party-already-in");
                        return;
                    }
                    sender.getParty().inviteMember(getPlayer);
                } else {
                    sender.sendMessage("party-invite-leader-only");
                }
            } else {
                sender.sendMessage("party-not-in");
            }
        } else if (args[0].equalsIgnoreCase("kick")) {
            if (args.length != 2) {
                sender.sendMessage("command-party-kick-usage");
                return;
            }
            if (sender.getParty() != null) {
                if (sender.getParty().getLeader() == sender) {
                    CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
                    if (getPlayer == null) {
                        sender.sendMessage("user-not-exist", sender.getLanguageMessage("party-prefix"));
                        return;
                    }
                    if (!getPlayer.isOnline()) {
                        sender.sendMessage("user-not-online", sender.getLanguageMessage("party-prefix"), (getPlayer.getRank().getRankColor()+getPlayer.getName()));
                        return;
                    }
                    if (sender.getLanguage() == Language.LanguageType.GERMAN) {
                        sender.getParty().kickMember(getPlayer);
                    }
                } else {
                    sender.sendMessage("party-invite-kick-only");
                }
            } else {
                sender.sendMessage("party-not-in");
            }
        } else if (args[0].equalsIgnoreCase("promote")) {
            if (args.length != 2) {
                sender.sendMessage("command-party-promote-usage");
                return;
            }
            if (sender.getParty() != null) {
                if (sender.getParty().getLeader() == sender) {
                    CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
                    if (getPlayer == null) {
                        sender.sendMessage("user-not-exist", sender.getLanguageMessage("party-prefix"));
                        return;
                    }
                    if (!getPlayer.isOnline()) {
                        sender.sendMessage("user-not-online", sender.getLanguageMessage("party-prefix"), (getPlayer.getRank().getRankColor()+getPlayer.getName()));
                        return;
                    }
                    sender.getParty().promotePlayer(getPlayer);
                } else {
                    sender.sendMessage("command-party-promote-usage");
                }
            } else {
                sender.sendMessage("party-not-in");
            }
        } else {
            this.sendHelp(sender);
        }
    }

    public void sendHelp(CloudPlayer cloudPlayer) {
        cloudPlayer.sendMessage("command-party-help");
    }

    public void sendHelp2(CloudPlayer cloudPlayer) {
        cloudPlayer.sendMessage("command-party-help-page-two");
    }
}