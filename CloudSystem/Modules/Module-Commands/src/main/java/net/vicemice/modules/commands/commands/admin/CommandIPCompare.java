package net.vicemice.modules.commands.commands.admin;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.Rank;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class CommandIPCompare extends Command {

    private static final String IPADDRESS_PATTERN = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (!sender.getRank().isModeration()) {
            sender.sendMessage("command-no-permission");
            return;
        }
        if (args.length == 2) {
            InetAddress sourceAddress;
            CloudPlayer sourcePlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[0]);
            if (sourcePlayer == null) {
                sender.sendRawMessage("§cCant resolve source player!");
                return;
            }
            try {
                sourceAddress = InetAddress.getByName(sourcePlayer.getIp());
            }
            catch (UnknownHostException e) {
                if (sender.getRank().isHigherEqualsLevel(Rank.ADMIN)) {
                    sender.sendRawMessage("§cCant resolve source host! (" + sourcePlayer.getIp() + " -> " + e.getMessage() + ")");
                } else {
                    sender.sendRawMessage("§cCant resolve source host!");
                }
                return;
            }
            InetAddress targetAddress = null;
            if (args[1].matches(IPADDRESS_PATTERN)) {
                try {
                    targetAddress = InetAddress.getByName(args[1]);
                }
                catch (UnknownHostException e) {
                    if (sender.getRank().isHigherEqualsLevel(Rank.ADMIN)) {
                        sender.sendRawMessage("§cCant resolve target host! (" + args[1] + " -> " + e.getMessage() + ")");
                    } else {
                        sender.sendRawMessage("§cCant resolve target host!");
                    }
                    return;
                }
            }
            CloudPlayer targetPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
            if (targetPlayer == null) {
                sender.sendRawMessage("§cCant resolve target player!");
                return;
            }
            try {
                targetAddress = InetAddress.getByName(targetPlayer.getIp());
            }
            catch (UnknownHostException e) {
                if (sender.getRank().isHigherEqualsLevel(Rank.ADMIN)) {
                    sender.sendRawMessage("§cCant resolve target host! (" + targetPlayer.getIp() + " -> " + e.getMessage() + ")");
                } else {
                    sender.sendRawMessage("§cCant resolve target host!");
                }
                return;
            }
            if (targetAddress == null) {
                sender.sendRawMessage("§cCant resolve target host!");
                return;
            }
            if (sourceAddress.equals(targetAddress)) {
                sender.sendRawMessage("§aFound match. Equal IP's");
            } else {
                sender.sendRawMessage("§cCant find a match. Unequal IP's");
            }
            if (sender.getRank().isHigherEqualsLevel(Rank.ADMIN)) {
                sender.sendRawMessage("§7Host Name:");
                sender.sendRawMessage("  §7" + sourceAddress.getHostName() + " <> " + targetAddress.getHostName());
                sender.sendRawMessage("§7Host Address:");
                sender.sendRawMessage("  §7" + sourceAddress.getHostAddress() + " <> " + targetAddress.getHostAddress());
            }
            return;
        }
        sender.sendRawMessage("§cInvalid usage!");
        sender.sendRawMessage("§cUseage /ipcompare <name> <name/IPv4>");
    }
}

