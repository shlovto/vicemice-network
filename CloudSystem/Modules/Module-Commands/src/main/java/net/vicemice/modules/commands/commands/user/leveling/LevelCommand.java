package net.vicemice.modules.commands.commands.user.leveling;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;

/*
 * Class created at 14:33 - 07.04.2020
 * Copyright (C) elrobtossohn
 */
public class LevelCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (args.length == 1) {
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[0]);
            if (cloudPlayer != null) {
                sender.sendMessage("command-level-other", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()), cloudPlayer.getLevel().getLevel());
            } else {
                sender.sendMessage("player-not-exist", sender.getLanguageMessage("prefix"));
            }
        } else {
            sender.sendMessage("command-level", sender.getLevel().getLevel());
        }
    }
}
