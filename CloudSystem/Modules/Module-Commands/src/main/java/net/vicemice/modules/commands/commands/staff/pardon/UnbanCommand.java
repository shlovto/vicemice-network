package net.vicemice.modules.commands.commands.staff.pardon;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;

import java.util.concurrent.TimeUnit;

public class UnbanCommand extends Command {

    public UnbanCommand() {
        super(Area.MODERATING);
    }

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (args.length == 2) {
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[0]);

            if (cloudPlayer != null) {
                if (cloudPlayer.getAbuses().getBan() != null) {
                    if (args[1].equalsIgnoreCase("remove")) {
                        CloudService.getCloudService().getManagerService().getAbuseManager().remove(cloudPlayer, sender, args[1], false);
                        CloudService.getCloudService().getManagerService().getPlayerManager().getIpBanned().remove(cloudPlayer.getIp());
                        sender.sendMessage("command-unban-successfully-removed", cloudPlayer.getRank().getRankColor()+cloudPlayer.getName());
                    } else if (args[1].equalsIgnoreCase("short")) {
                        if (CloudService.getCloudService().getManagerService().getAbuseManager().getBanLogByID(cloudPlayer.getAbuses().getBan().getBanId()) != null && CloudService.getCloudService().getManagerService().getAbuseManager().getBanLogByID(cloudPlayer.getAbuses().getBan().getBanId()).isShorted()) {
                            sender.sendMessage("command-unban-punish-already-shortened");
                            return;
                        }

                        long end = (System.currentTimeMillis() - cloudPlayer.getAbuses().getBan().getLength());
                        if (cloudPlayer.getAbuses().getBan().getLength() != 0) {
                            long time = System.currentTimeMillis() + (end / 2);
                            CloudService.getCloudService().getManagerService().getAbuseManager().shortBan(cloudPlayer, time, CloudService.getCloudService().getManagerService().getPunishManager().getByKey("SHORTENING"), sender, cloudPlayer.getAbuses().getBan().getBanId(), args[1]);
                            CloudService.getCloudService().getManagerService().getPlayerManager().getIpBanned().remove(cloudPlayer.getIp());
                            sender.sendMessage("command-unban-successfully-shortened", (cloudPlayer.getRank().getRankColor() + cloudPlayer.getName()), GoAPI.getTimeManager().getRemainingTimeString(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), sender.getLocale(), time));
                        } else {
                            long time = System.currentTimeMillis() + TimeUnit.DAYS.toMillis(30);
                            CloudService.getCloudService().getManagerService().getAbuseManager().shortBan(cloudPlayer, time, CloudService.getCloudService().getManagerService().getPunishManager().getByKey("SHORTENING"), sender, cloudPlayer.getAbuses().getBan().getBanId(), args[1]);
                            CloudService.getCloudService().getManagerService().getPlayerManager().getIpBanned().remove(cloudPlayer.getIp());
                            sender.sendMessage("command-unban-successfully-shortened", (cloudPlayer.getRank().getRankColor() + cloudPlayer.getName()), GoAPI.getTimeManager().getRemainingTimeString(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), sender.getLocale(), time));
                        }
                    } else {
                        sender.sendMessage("command-unban-successfully-unknown-option");
                    }
                } else {
                    sender.sendMessage("command-unban-no-punish");
                }
            } else {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("team-prefix"));
            }
        } else {
            sender.sendMessage("command-unban-usage");
        }
    }
}
