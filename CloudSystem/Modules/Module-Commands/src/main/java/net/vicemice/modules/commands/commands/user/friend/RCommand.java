package net.vicemice.modules.commands.commands.user.friend;

import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.Rank;

public class RCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (sender.getPlayTime() < 30 && !sender.getRank().isHigherEqualsLevel(Rank.VIP)) {
            sender.sendMessage("friend-min-playtime-minutes", (30-sender.getPlayTime()));
            return;
        }
        if (sender.getSetting("private-messages", Integer.class) == 0) {
            sender.sendMessage("command-msg-enable-messages");
            return;
        }

        if (args.length >= 1) {
            if (sender.getLastMessageReceiver() != null && sender.getLastMessageReceiver().isOnline()) {
                CloudPlayer target = sender.getLastMessageReceiver();
                if (sender.isFriend(target)) {
                    if (!target.isOnline()) {
                        sender.sendMessage("user-not-online", sender.getLanguageMessage("friend-prefix"), target.getRank().getRankColor() + target.getName());
                        return;
                    }

                    if (target.getSetting("private-messages", Integer.class) == 1) {
                        StringBuilder sb = new StringBuilder();
                        int i = 0;
                        while (i < args.length) {
                            if (i < args.length) {
                                sb.append(args[i] + " ");
                            } else {
                                sb.append(args[i]);
                            }
                            ++i;
                        }
                        String message = sb.toString();

                        target.sendMessage("command-msg-to", (sender.getRank().getRankColor() + sender.getName()), target.getRank().getRankColor(), message);
                        sender.sendMessage("command-msg-from", sender.getRank().getRankColor(), (target.getRank().getRankColor() + target.getName()), message);

                        if (target.isAfk()) {
                            sender.sendMessage("user-is-afk", sender.getLanguageMessage("friend-prefix"), target.getRank().getRankColor() + target.getName());
                        }

                        target.setLastMessageReceiver(sender);
                    } else {
                        sender.sendMessage("player-not-receive-msg", target.getRank().getRankColor() + target.getName());
                    }
                } else {
                    sender.sendMessage("user-not-friend", sender.getLanguageMessage("friend-prefix"), target.getRank().getRankColor()+target.getName());
                }
            } else {
                sender.sendMessage("command-rmsg-last-receiver");
            }
        } else {
            sender.sendMessage("command-rmsg-usage");
        }
    }
}