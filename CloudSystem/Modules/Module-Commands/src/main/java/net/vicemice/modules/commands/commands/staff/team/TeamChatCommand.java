package net.vicemice.modules.commands.commands.staff.team;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.Rank;

public class TeamChatCommand extends Command {

    public TeamChatCommand() {
        super(Rank.MODERATOR);
    }

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (args.length > 0) {
            StringBuilder sb = new StringBuilder();
            int i = 0;
            while (i < args.length) {
                if (i < args.length) {
                    sb.append(args[i] + " ");
                } else {
                    sb.append(args[i]);
                }
                ++i;
            }

            CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().sendTeamMessage("command-teamchat-format", sender.getServer().getName().toUpperCase().replace("-", ""), (sender.getRank().getRankColor()+sender.getName()), sb.toString());
        } else {
            sender.sendMessage("command-teamchat-usage");
        }
    }
}