package net.vicemice.modules.commands.commands.user;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;

import java.util.concurrent.TimeUnit;

public class ForumCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (sender.getForumTimeout() != -1L && sender.getForumTimeout() > System.currentTimeMillis()) {
            sender.sendMessage("command-timeout");
            return;
        }

        if (args.length == 0) {
            this.sendHelp(sender);
            return;
        }
        if (args[0].equalsIgnoreCase("register")) {
            if (args.length == 3) {
                String email = args[1];
                String password = args[2];
                if (sender.getForumId() == -1) {
                    sender.setForumTimeout(System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(20L));

                    GoAPI.getForumAPI().createUser(sender.getUniqueId(), sender.getRank(), sender.getName(), email, password, o -> {
                        if (!o.isNull("errors")) {
                            if (o.getJSONArray("errors").getJSONObject(0).getString("code").equalsIgnoreCase("usernames_must_be_unique")) {
                                sender.sendMessage("command-forum-register-invalid-name");
                            } else if (o.getJSONArray("errors").getJSONObject(0).getString("code").equalsIgnoreCase("email_addresses_must_be_unique")) {
                                sender.sendMessage("command-forum-register-email-already-used");
                            } else if (o.getJSONArray("errors").getJSONObject(0).getString("code").equalsIgnoreCase("please_enter_valid_email")) {
                                sender.sendMessage("command-forum-register-invalid-email");
                            } else {
                                CloudService.getCloudService().getTerminal().write("§cUnbekannter Fehlercode! ('§7"+o.getJSONArray("errors").getJSONObject(0).getString("code")+"§c')");
                                CloudService.getCloudService().getTerminal().write("§c"+o.getJSONArray("errors").getJSONObject(0).getString("message"));

                                sender.sendMessage("command-forum-register-error");
                            }

                            return;
                        }

                        if (!o.isNull("user")) {
                            if (o.getBoolean("success")) {
                                sender.setForumId(o.getJSONObject("user").getInt("user_id"));
                                sender.updateToDatabase(false);
                                sender.sendMessage("command-forum-register-success");
                            }
                            return;
                        }

                        sender.sendMessage("command-forum-register-error");

                        if(CloudService.getCloudService().isDevMode()) CloudService.getCloudService().getTerminal().writeMessage("§7[§2Forum Registration§7] §cError: §6"+o.toString());
                    });
                } else {
                    sender.sendMessage("command-forum-register-already");
                }
            } else {
                sender.sendMessage("command-forum-register-usage");
            }
        } else {
            this.sendHelp(sender);
        }
    }

    public void sendHelp(CloudPlayer cloudPlayer) {
        cloudPlayer.sendMessage("command-forum-help");
    }
}