package net.vicemice.modules.commands.commands.staff;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.ComponentMessagePacket;
import net.vicemice.hector.packets.types.player.report.ReportInventoryPacket;
import net.vicemice.hector.packets.types.player.report.ReportPlayersPacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.players.ReportPlayer;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class ReportCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("templates")) {
                ReportCauses.sendCauses(sender);
                return;
            }
            if (sender.getReportTimeout() != -1L && sender.getReportTimeout() > System.currentTimeMillis() && !sender.getRank().isTeam()) {
                sender.sendMessage("command-report-timeout");
                return;
            }
            String name = args[0];
            CloudPlayer suspect = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(name);

            if (suspect == null) {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("report-prefix"));
                return;
            }

            if (!suspect.isOnline()) {
                sender.sendMessage("user-not-online", sender.getLanguageMessage("report-prefix"), suspect.getRank().getRankColor() + suspect.getName());
                return;
            }

            if (CloudService.getCloudService().getManagerService().getReportManager().hasReported(suspect, sender.getName())) {
                sender.sendMessage("command-report-already-reported");
                return;
            }

            if (suspect.equals(sender) && !sender.getRank().isAdmin()) {
                sender.sendMessage("command-report-cannot-self");
                return;
            }

            if (suspect.getRank().isTeam() && !sender.getRank().isAdmin()) {
                sender.sendMessage("command-report-cannot-player");
                return;
            }
            if (sender.getPlayTime() < 10L && !sender.getRank().isAdmin()) {
                sender.sendMessage("command-report-playtime");
                return;
            }

            sender.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.REPORT_INVENTORY_PACKET, new ReportInventoryPacket(sender.getUniqueId(), new ReportPlayer(suspect.getUniqueId(), suspect.getName(), suspect.getServer().getName(), suspect.getSkinValue(), suspect.getSkinSignature()))));
        } else if (args.length == 2) {
            if (sender.getReportTimeout() != -1L && sender.getReportTimeout() > System.currentTimeMillis() && !sender.getRank().isTeam()) {
                sender.sendMessage("command-report-timeout");
                return;
            }
            String name = args[0];

            String cause = args[1];
            CloudPlayer suspect = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(name);

            if (suspect == null) {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("report-prefix"));
                return;
            }

            if (!suspect.isOnline()) {
                sender.sendMessage("user-not-online", sender.getLanguageMessage("report-prefix"), suspect.getRank().getRankColor() + suspect.getName());
                return;
            }

            if (CloudService.getCloudService().getManagerService().getReportManager().hasReported(suspect, sender.getName())) {
                sender.sendMessage("command-report-already-reported");
                return;
            }

            if (suspect.equals(sender) && !sender.getRank().isAdmin()) {
                sender.sendMessage("command-report-cannot-self");
                return;
            }

            if (suspect.getRank().isTeam() && !sender.getRank().isAdmin()) {
                sender.sendMessage("command-report-cannot-player");
                return;
            }
            if (sender.getPlayTime() < 10L && !sender.getRank().isAdmin()) {
                sender.sendMessage("command-report-playtime");
                return;
            }
            ReportCauses reportCauses = ReportCauses.fromString(cause);
            if (reportCauses == null) {
                sender.sendMessage("command-report-reason-not-found");
                return;
            }
            cause = reportCauses.name();

            sender.sendMessage("report-confirm-message", suspect.getRank().getRankColor()+suspect.getName(), cause);
            ArrayList<String[]> messageData = new ArrayList<>();
            messageData.add(new String[]{sender.getLanguageMessage("report-confirm-click"), "true", "run_command", "/report confirm "+suspect.getName()+" "+cause});
            ComponentMessagePacket componentMessagePacket = new ComponentMessagePacket(sender.getName(), messageData);
            sender.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMPONENT_MESSAGE_PACKET, componentMessagePacket));
        } else if (args.length == 3 && args[0].equalsIgnoreCase("confirm")) {
            if (sender.getReportTimeout() != -1L && sender.getReportTimeout() > System.currentTimeMillis() && !sender.getRank().isTeam()) {
                sender.sendMessage("command-report-timeout");
                return;
            }
            String name = args[1];

            String cause = args[2];
            CloudPlayer suspect = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(name);

            if (suspect == null) {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("report-prefix"));
                return;
            }

            if (!suspect.isOnline()) {
                sender.sendMessage("user-not-online", sender.getLanguageMessage("report-prefix"), suspect.getRank().getRankColor() + suspect.getName());
                return;
            }

            if (CloudService.getCloudService().getManagerService().getReportManager().hasReported(suspect, sender.getName())) {
                sender.sendMessage("command-report-already-reported");
                return;
            }

            if (suspect.equals(sender) && !sender.getRank().isAdmin()) {
                sender.sendMessage("command-report-cannot-self");
                return;
            }

            if (suspect.getRank().isTeam() && !sender.getRank().isAdmin()) {
                sender.sendMessage("command-report-cannot-player");
                return;
            }
            if (sender.getPlayTime() < 10L && !sender.getRank().isAdmin()) {
                sender.sendMessage("command-report-playtime");
                return;
            }
            ReportCauses reportCauses = ReportCauses.fromString(cause);
            if (reportCauses == null) {
                sender.sendMessage("command-report-reason-not-found");
                return;
            }
            cause = reportCauses.name();

            String chatLog = null;

            if (reportCauses == ReportCauses.CHAT) {
                chatLog = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().createChatLog(sender.getUniqueId(), suspect.getUniqueId(), sender.getServer().getServerID(), true);
            }

            sender.setReportTimeout(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(5L));

            sender.sendMessage("report-success");
            CloudService.getCloudService().getManagerService().getReportManager().reportPlayer(suspect, new ReportPlayersPacket.AccusationsReason(sender.getName(), cause, sender.getServer() == null ? "undefined" : sender.getServer().getName(), sender.getCurrentGameId(), chatLog));
            sender.setReportTimeout(System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(2));
        } else {
            sender.sendMessage("command-report-help");
        }
    }
}