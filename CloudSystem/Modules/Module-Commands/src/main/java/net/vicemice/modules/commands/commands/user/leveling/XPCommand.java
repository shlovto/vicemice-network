package net.vicemice.modules.commands.commands.user.leveling;

import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;

/*
 * Class created at 14:36 - 07.04.2020
 * Copyright (C) elrobtossohn
 */
public class XPCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        sender.sendMessage("command-xp", sender.getLevel().getExperience(), (sender.getLevel().calculateNextLevel()-sender.getLevel().getExperience()));
    }
}
