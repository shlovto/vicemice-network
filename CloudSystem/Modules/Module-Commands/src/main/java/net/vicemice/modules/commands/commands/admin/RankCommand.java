package net.vicemice.modules.commands.commands.admin;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.beforejoined.BeforePlayer;
import net.vicemice.hector.utils.MessageGroup;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.terminal.ChatColor;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class RankCommand extends Command {

    private static final List<UUID> whitelistedPlayers = Arrays.asList(UUID.fromString("c80be485-b4f9-46a7-afa7-947a50f76739"), UUID.fromString("bed7ab0f-5a69-4f69-8104-d277c22f2ced"));

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (args.length == 0) {
            if (sender.getRankEnd() != 0L) {
                sender.sendMessage("command-rank-current-limited", sender.getRank().getRankColor() + sender.getRank().getRankName(), GoAPI.getTimeManager().getRemainingTimeString(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), sender.getLocale(), sender.getRankEnd()));
            } else {
                sender.sendMessage("command-rank-current-lifetime", sender.getRank().getRankColor() + sender.getRank().getRankName());
            }
            return;
        }

        if (!(sender.getRank().isAdmin()) && !(whitelistedPlayers.contains(sender.getUniqueId()))) {
            sender.sendMessage("command-no-permission");
            return;
        }

        long end = 0L;

        if (args.length == 1) {
            CloudPlayer target = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[0]);
            if (target != null && !target.getName().equalsIgnoreCase("help")) {
                MessageGroup messageGroup = new MessageGroup(sender);
                messageGroup.addMessage(sender.getLanguageMessage("team-prefix") + "§7Informations for " + target.getRank().getRankColor() + target.getName() + " §7Rank:");
                messageGroup.addMessage("§7Rank-Name: " + target.getRank().getRankColor() + target.getRank().getRankName());
                ChatColor chatColor = ChatColor.getByChar(target.getRank().getRankColor().replace("§", "").toCharArray()[0]);
                if (chatColor != null) {
                    messageGroup.addMessage("§7Rank-Color: " + target.getRank().getRankColor() + chatColor.name());
                } else {
                    messageGroup.addMessage("§7Rank-Color: §6Unknow");
                }
                if (target.getRankEnd() != 0L) {
                    messageGroup.addMessage("§7Rank-Time is §6LIMITED");
                    messageGroup.addMessage("§7Rank will end in §6" + GoAPI.getTimeManager().getRemainingTimeString(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), sender.getLocale(), target.getRankEnd()));
                } else {
                    messageGroup.addMessage("§7Rank-Time is §6LIFETIME");
                }
                messageGroup.send();
            } else {
                sender.sendMessage("command-rank-usage-one");
                sender.sendMessage("command-rank-usage-two", Rank.getRanks());
            }
            return;
        }

        Rank rank = Rank.fromStringExact(args[1]);
        if (rank == null) {
            sender.sendMessage("command-rank-rank-not-exist");
            return;
        }
        String name = args[0];
        CloudPlayer target = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(name);
        if (target == null) {
            BeforePlayer bPlayer = CloudService.getCloudService().getManagerService().getBeforeManager().getPlayer(name);
            if (bPlayer.getRank() != null && Rank.fromString(rank.getRankName()).isLowerLevel(Rank.fromString(bPlayer.getRank()))) {
                return;
            }
            bPlayer.setRank(rank.getRankName());
            bPlayer.updateToDatabase();
            sender.sendMessage("command-rank-bplayer", rank.getRankColor() + name, rank.getRankColor() + rank.getRankName());

            return;
        }
        if (args.length == 3) {
            TimeUnit time = null;
            long value = -1L;
            try {
                final String length = args[2].toLowerCase();
                if (length.endsWith("s")) {
                    time = TimeUnit.SECONDS;
                    value = Long.parseLong(length.split("s")[0]);
                } else if (length.endsWith("m")) {
                    time = TimeUnit.MINUTES;
                    value = Long.parseLong(length.split("m")[0]);
                } else if (length.endsWith("h")) {
                    time = TimeUnit.HOURS;
                    value = Long.parseLong(length.split("h")[0]);
                } else if (length.endsWith("w")) {
                    time = TimeUnit.DAYS;
                    value = 7*Long.parseLong(length.split("w")[0]);
                } else if (length.endsWith("d")) {
                    time = TimeUnit.DAYS;
                    value = Long.parseLong(length.split("d")[0]);
                } else if (length.endsWith("mm")) {
                    time = TimeUnit.DAYS;
                    value = 7*4*Long.parseLong(length.split("mm")[0]);
                } else if (length.endsWith("y")) {
                    time = TimeUnit.DAYS;
                    value = 7*4*12*Long.parseLong(length.split("y")[0]);
                }
            } catch (Exception ex) {
            }
            end = time.toMillis(value);;
            if (value == -1L || time == null) {
                sender.sendMessage("command-unknow-duration");
                return;
            }
        }
        if (end != 0L && target.getRank() == rank) {
            end = target.getRankEnd() == 0L ? (end += System.currentTimeMillis()) : (end += target.getRankEnd());
        } else if (end != 0L) {
            end += System.currentTimeMillis();
        }
        target.setRank(rank, end);

        sender.sendMessage("command-rank-changed-changer", rank.getRankColor() + target.getName(), rank.getRankColor() + rank.getRankName());
        if (end != 0L) {
            sender.sendMessage("command-rank-changed-limited", GoAPI.getTimeManager().getRemainingTimeString(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), sender.getLocale(), end));
        } else {
            sender.sendMessage("command-rank-changed-lifetime");
        }

        if (target.isOnline()) {
            target.sendMessage("command-rank-changed-target", rank.getRankColor() + rank.getRankName());
            if (end != 0L) {
                target.sendMessage("command-rank-changed-limited", GoAPI.getTimeManager().getRemainingTimeString(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), sender.getLocale(), end));
            } else {
                target.sendMessage("command-rank-changed-lifetime");
            }
        }
    }
}