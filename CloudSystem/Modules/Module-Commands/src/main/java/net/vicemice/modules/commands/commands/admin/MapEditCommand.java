package net.vicemice.modules.commands.commands.admin;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;

public class MapEditCommand extends Command {

    public MapEditCommand() {
        super(Area.ADMINISTRATING);
    }

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (args.length == 2) {
            String template = args[0];
            if (CloudService.getCloudService().getManagerService().getServerManager().getTemplateByName(template) != null) {
                sender.editMap(args[1], template);
            } else {
                sender.sendRawMessage(sender.getLanguageMessage("prefix")+" §cNo template has been found for this name");
            }
        } else {
            sender.sendRawMessage(sender.getLanguageMessage("prefix")+" §cSyntax: /mapedit <Template> <Map>");
        }
    }
}