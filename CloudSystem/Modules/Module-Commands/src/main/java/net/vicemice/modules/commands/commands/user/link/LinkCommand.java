package net.vicemice.modules.commands.commands.user.link;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.network.modules.api.event.discord.DiscordVerifyEvent;
import net.vicemice.hector.network.modules.api.event.teamspeak.TeamSpeakVerifyEvent;

import java.io.*;

public class LinkCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (args.length == 2 && args[0].equalsIgnoreCase("teamspeak")) {
            CloudService.getCloudService().getManagerService().getEventManager().callEvent(new TeamSpeakVerifyEvent(sender, args[1]));
        } else if (args.length == 2 && args[0].equalsIgnoreCase("discord")) {
            CloudService.getCloudService().getManagerService().getEventManager().callEvent(new DiscordVerifyEvent(sender, args[1]));
        } else {
            sender.sendMessage("command-link-no-request");
        }
    }

    private static String readAll(Reader rd) throws IOException {
        int cp;
        StringBuilder sb = new StringBuilder();
        while ((cp = rd.read()) != -1) {
            sb.append((char)cp);
        }
        return sb.toString();
    }
}