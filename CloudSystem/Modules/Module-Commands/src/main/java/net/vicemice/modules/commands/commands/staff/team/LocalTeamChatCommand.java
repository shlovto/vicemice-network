package net.vicemice.modules.commands.commands.staff.team;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.Rank;

import java.util.Iterator;
import java.util.UUID;

public class LocalTeamChatCommand extends Command {

    public LocalTeamChatCommand() {
        super(Rank.MODERATOR);
    }

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (args.length > 0) {
            StringBuilder sb = new StringBuilder();
            int i = 0;
            while (i < args.length) {
                if (i < args.length) {
                    sb.append(args[i] + " ");
                } else {
                    sb.append(args[i]);
                }
                ++i;
            }
            String message = sb.toString();

            Iterator<UUID> cloudPlayerIterator = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithRank().iterator();
            while (cloudPlayerIterator.hasNext()) {
                CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(cloudPlayerIterator.next());
                if (!cloudPlayer.getRank().isTeam() || !cloudPlayer.isNotify()) continue;
                if (cloudPlayer.getServer() == null || sender.getServer() == null) continue;
                if (!cloudPlayer.getServer().getName().equalsIgnoreCase(sender.getServer().getName())) continue;
                cloudPlayer.sendMessage("command-local-teamchat-format", sender.getRank().getRankColor()+sender.getName(), message);
            }
        } else {
            sender.sendMessage("command-local-teamchat-usage");
        }
    }
}
