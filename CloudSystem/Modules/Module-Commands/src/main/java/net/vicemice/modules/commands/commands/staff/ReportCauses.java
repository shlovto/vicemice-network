package net.vicemice.modules.commands.commands.staff;

import net.vicemice.hector.player.CloudPlayer;
import lombok.Getter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum ReportCauses {
    HACKING("hacking", "hacks", "hack"),
    CHAT("chat", "insult", "beleidigung", "verhalten"),
    TEAMING("teaming", "team", "teams"),
    TROLLING("trolling", "troll", "trolls"),
    SPAWNTRAPPING("spawntrapping", "spawntrap", "trap"),
    SKIN("skin", "aussehen"),
    NAME("username", "name"),
    BUILDING("building", "bauart", "build"),
    BOOSTING("boosting", "boost"),
    BUGUSING("bugusing", "bug"),
    PETS("pets", "pet"),
    CLAN("clan");

    @Getter
    private List<String> aliases;

    private ReportCauses(String... aliases) {
        this.aliases = new ArrayList<>();

        this.aliases.addAll(Arrays.asList(aliases));
    }

    public static String getCauses() {
        StringBuilder stringBuilder = new StringBuilder();
        int current = 1;
        for (net.vicemice.modules.commands.commands.staff.ReportCauses ReportCauses : values()) {
            if (current == values().length) {
                stringBuilder.append(ReportCauses);
            } else {
                stringBuilder.append(ReportCauses + ", ");
            }
            current++;
        }
        return stringBuilder.toString();
    }

    public static /*String*/ void /*getCauses*/ sendCauses(CloudPlayer cloudPlayer) {
        StringBuilder stringBuilder = new StringBuilder();
        int current = 1;
        
        cloudPlayer.sendMessage("command-report-templates");
        for (ReportCauses causes : values()) {
            cloudPlayer.sendRawMessage("§7- §c" + causes.name());

                /*if (current == values().length) {
                    stringBuilder.append(points.name());
                } else {
                    stringBuilder.append(points.name() + ", ");
                }*/
            current++;
        }
        //return stringBuilder.toString();
    }

    public static ReportCauses fromString(String cause) {
        for (ReportCauses reportCauses : ReportCauses.values()) {
            if (reportCauses.name().equalsIgnoreCase(cause)) {
                return reportCauses;
            }
            for (String alias : reportCauses.getAliases()) {
                if (alias.equalsIgnoreCase(cause)) {
                    return reportCauses;
                }
            }

        }
        return null;
    }
}