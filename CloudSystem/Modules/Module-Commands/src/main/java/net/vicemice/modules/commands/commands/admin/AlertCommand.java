package net.vicemice.modules.commands.commands.admin;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.packets.types.proxy.AlertPacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.utils.Rank;

public class AlertCommand extends Command {

    public AlertCommand() {
        super(Rank.ADMIN);
    }

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (args.length > 0) {
            StringBuilder sb = new StringBuilder();
            int i = 0;
            while (i < args.length) {
                if (i < args.length) {
                    sb.append(args[i] + " ");
                } else {
                    sb.append(args[i]);
                }
                ++i;
            }
            String msg = sb.toString();
            CloudService.getCloudService().getManagerService().getProxyManager().broadcastPacket(GoAPI.getPacketHelper().preparePacket(PacketType.ALERT_PACKET, new AlertPacket("§c" + msg)));
            //CloudServer.getProxyManager().broadcastPacket(CloudServer.getPacketHelper().preparePacket(PacketType.ALERT_PACKET, new AlertPacket(CloudServer.getAnnouncementPrefix() + "§7" + msg)));
        } else {
            sender.sendMessage("command-alert-usage");
        }
    }
}