package net.vicemice.modules.commands.commands.staff.punish;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;

import java.util.concurrent.TimeUnit;

/*
 * Class created at 21:33 - 18.04.2020
 * Copyright (C) elrobtossohn
 */
public class TempmuteCommand extends Command {

    public TempmuteCommand() {
        super(Area.ADMINISTRATING);
    }

    @Override
    public void execute(final String[] args, final CloudPlayer sender) {
        if (args.length > 2) {
            final String name = args[0];
            final CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(name);
            if (cloudPlayer == null) {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("team-prefix"));
                return;
            }
            if (cloudPlayer.getAbuses().getBan() != null) {
                sender.sendMessage("command-punish-already-punished", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()));
                return;
            }
            if (cloudPlayer.getRank().isTeam() && !sender.getRank().isAdmin()) {
                sender.sendMessage("can-not-punish");
                return;
            }

            final StringBuilder sb = new StringBuilder();
            for (int i = 2; i < args.length; ++i) {
                if (i < args.length) {
                    sb.append(args[i] + " ");
                } else {
                    sb.append(args[i]);
                }
            }
            String reason = sb.toString();

            TimeUnit time = null;
            long value = -1L;
            try {
                final String duration = args[1].toLowerCase();
                if (duration.endsWith("s")) {
                    time = TimeUnit.SECONDS;
                    value = Long.parseLong(duration.split("s")[0]);
                } else if (duration.endsWith("m")) {
                    time = TimeUnit.MINUTES;
                    value = Long.parseLong(duration.split("m")[0]);
                } else if (duration.endsWith("h")) {
                    time = TimeUnit.HOURS;
                    value = Long.parseLong(duration.split("h")[0]);
                } else if (duration.endsWith("d")) {
                    time = TimeUnit.DAYS;
                    value = Long.parseLong(duration.split("d")[0]);
                } else if (duration.equalsIgnoreCase("perma")) {
                    time = TimeUnit.SECONDS;
                    value = 0;
                }
            } catch (Exception ignored) {}
            if (value == -1L || time == null) {
                sender.sendRawMessage("§cInvalid time.");
                return;
            }
            if (value == 0L) {
                CloudService.getCloudService().getManagerService().getAbuseManager().punish(cloudPlayer, sender, reason, "Temporary-Mute", 0L, true, false);
            } else {
                final long length = System.currentTimeMillis() + time.toMillis(value);
                CloudService.getCloudService().getManagerService().getAbuseManager().punish(cloudPlayer, sender, reason, "Temporary-Mute", length, true, false);
            }
        } else {
            sender.sendRawMessage(sender.getLanguageMessage("prefix")+" §cUse /tempmute <player> <length> <reason>");
        }
    }
}
