package net.vicemice.modules.commands.commands.user;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.slave.TemplateData;

/*
 * Class created at 23:41 - 02.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (args.length == 1) {
            String name = args[0];
            TemplateData templateData = CloudService.getCloudService().getManagerService().getServerManager().getTemplateByName(name);
            if (name.equalsIgnoreCase("bedwars") || name.equalsIgnoreCase("elobedwars") || templateData != null && templateData.isPlayAble()) {
                for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
                    if (name.equalsIgnoreCase("bedwars")) {
                        if (server.isCloudServer() && server.getName().startsWith("BedWars") && server.getServerState() == ServerState.LOBBY) {
                            templateData = server.getServerTemplate();
                            if (templateData.getLobbySigns() != 0 && server.getSign() == null) continue;
                            sender.connectToServer(server.getName());
                            return;
                        }
                        continue;
                    }
                    if (name.equalsIgnoreCase("elobedwars")) {
                        if (server.isCloudServer() && server.getName().startsWith("EloBedWars") && server.getServerState() == ServerState.LOBBY) {
                            templateData = server.getServerTemplate();
                            if (templateData.getLobbySigns() != 0 && server.getSign() == null) continue;
                            sender.connectToServer(server.getName());
                            return;
                        }
                        continue;
                    }

                    if (server.isCloudServer() && server.getTemplateName().equalsIgnoreCase(templateData.getName()) && server.getServerState() == ServerState.LOBBY) {
                        if (templateData.getLobbySigns() != 0 && server.getSign() == null) continue;
                        sender.connectToServer(server.getName());
                        return;
                    }
                }
                sender.sendMessage("command-play-no-server");
            } else {
                sender.sendMessage("command-play-template-not-exist");
            }
        } else {
            sender.sendMessage("command-play-usage");
        }
    }
}