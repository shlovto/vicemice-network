package net.vicemice.modules.commands.commands.user.friend;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.commands.CommandPagination;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.ComponentMessagePacket;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.math.MathUtils;
import net.vicemice.hector.utils.players.FriendData;
import net.vicemice.hector.utils.players.utils.DataUpdateType;

import java.text.MessageFormat;
import java.util.*;

public class FriendCommand extends Command {

    @Override
    public void execute(final String[] args, final CloudPlayer sender) {
        if (sender.getPlayTime() < 30 && !sender.getRank().isHigherEqualsLevel(Rank.VIP)) {
            sender.sendMessage("friends-min-playtime-minutes", (30-sender.getPlayTime()));
            return;
        }
        
        if (args.length == 0) {
            sender.sendMessage("command-friend-help");
            return;
        }

        if (args[0].equalsIgnoreCase("2")) {
            sender.sendMessage("command-friend-help-page-2");
        } else if (args[0].equalsIgnoreCase("toggle")) {
            if (sender.getSetting("friend-add", Integer.class) == 0) {
                sender.sendMessage("command-friend-requests-enabled");
                sender.setSetting("friend-add", 1);
            } else if (sender.getSetting("friend-add", Integer.class) == 1) {
                sender.sendMessage("command-friend-requests-disabled");
                sender.setSetting("friend-add", 0);
            }
            sender.sendData(sender.getServer(), DataUpdateType.UPDATE_SETTINGS, true);
        } else if (args[0].equalsIgnoreCase("private")) {
            if (sender.getSetting("hide-last-offline", Integer.class) == 0) {
                sender.sendMessage("command-friend-private-disabled");
                sender.setSetting("hide-last-offline", 1);
            } else if (sender.getSetting("hide-last-offline", Integer.class) == 1) {
                sender.sendMessage("command-friend-private-enabled");
                sender.setSetting("hide-last-offline", 0);
            }
            sender.sendData(sender.getServer(), DataUpdateType.UPDATE_SETTINGS, true);
        } else if (args[0].equalsIgnoreCase("favorite")) {
            if (args.length != 2) {
                sender.sendMessage("command-friend-favorite-usage");
                return;
            }
            CloudPlayer target = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
            if (target == null) {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("friend-prefix"));
                return;
            }
            if (!target.isFriend(sender)) {
                sender.sendMessage("user-not-friend", sender.getLanguageMessage("friend-prefix"), target.getRank().getRankColor()+target.getName());
                return;
            }
            if (sender.getFriends().get(target.getUniqueId()).isFavorite()) {
                sender.sendMessage("command-friend-already-favorite", target.getRank().getRankColor()+target.getName());
                return;
            }
            sender.getFriends().get(target.getUniqueId()).setFavorite(true);
            sender.sendMessage("command-friend-favorite-success", target.getRank().getRankColor()+target.getName());
            sender.sendData(sender.getServer(), DataUpdateType.UPDATE_FRIENDS, true);
            sender.updateToDatabase(false);
        } else if (args[0].equalsIgnoreCase("unfavorite")) {
            if (args.length != 2) {
                sender.sendMessage("command-friend-unfavorite-usage");
                return;
            }
            CloudPlayer target = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
            if (target == null) {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("friend-prefix"));
                return;
            }
            if (!target.isFriend(sender)) {
                sender.sendMessage("user-not-friend", sender.getLanguageMessage("friend-prefix"), target.getRank().getRankColor()+target.getName());
                return;
            }
            if (!sender.getFriends().get(target.getUniqueId()).isFavorite()) {
                sender.sendMessage("command-friend-already-non-favorite", target.getRank().getRankColor()+target.getName());
                return;
            }
            sender.getFriends().get(target.getUniqueId()).setFavorite(false);
            sender.sendMessage("command-friend-unfavorite-success", target.getRank().getRankColor()+target.getName());
            sender.sendData(sender.getServer(), DataUpdateType.UPDATE_FRIENDS, true);
            sender.updateToDatabase(false);
        } else if (args[0].equalsIgnoreCase("accept")) {
            if (args.length != 2) {
                sender.sendMessage("command-friend-accept-usage");
                return;
            }
            final String name = args[1];
            final CloudPlayer target = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(name);
            if (target == null) {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("friend-prefix"));
                return;
            }
            if (!sender.hasRequest(target)) {
                sender.sendMessage("command-friend-no-request-from");
                return;
            }
            sender.removeRequest(target);
            target.removeRequest(sender);
            target.getFriends().put(sender.getUniqueId(), new FriendData(sender.getUniqueId(), System.currentTimeMillis(), false));
            sender.getFriends().put(target.getUniqueId(), new FriendData(target.getUniqueId(), System.currentTimeMillis(), false));
            sender.updateFriendInv();
            target.updateFriendInv();
            target.updateToDatabase(false);
            target.sendData(target.getServer(), DataUpdateType.UPDATE_FRIENDS, true);
            sender.updateToDatabase(false);
            sender.sendData(sender.getServer(), DataUpdateType.UPDATE_FRIENDS, true);
            sender.sendMessage("command-friend-accept-success", target.getRank().getRankColor() + target.getName());

            if (target.isOnline()) {
                target.sendMessage("command-friend-accept-success", sender.getRank().getRankColor() + sender.getName());
            }
        } else if (args[0].equalsIgnoreCase("jump")) {
            if (args.length != 2) {
                sender.sendMessage("command-friend-jump-usage");
                return;
            }
            final CloudPlayer target = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
            if (target == null) {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("friend-prefix"));
                return;
            }
            if (!target.isFriend(sender)) {
                sender.sendMessage("user-not-friend", sender.getLanguageMessage("friend-prefix"), target.getRank().getRankColor()+target.getName());
                return;
            }
            if (!target.isOnline() || target.getServer() == null) {
                sender.sendMessage("user-not-online", sender.getLanguageMessage("friend-prefix"), target.getRank().getRankColor() + target.getName());
                return;
            }
            if (target.getSetting("friend-jump", Integer.class) == 0 && sender.getRank().isLowerLevel(Rank.ADMIN)) {
                sender.sendMessage("command-friend-jump-disabled", target.getRank().getRankColor() + target.getName());
                return;
            }
            if (sender.getServer().getName().toLowerCase().equals(target.getServer().getName().toLowerCase())) {
                sender.sendMessage("command-friend-jump-same-server");
                return;
            }
            if (target.getServer().getName().startsWith("Replay-")) {
                sender.sendMessage("command-friend-jump-to-replay");
                return;
            }

            sender.sendMessage("command-friend-jump-success", target.getRank().getRankColor() + target.getName());
            sender.connectToServer(target.getServer().getName().toLowerCase());
        } else if (args[0].equalsIgnoreCase("remove")) {
            if (args.length != 2) {
                sender.sendMessage("command-friend-remove-usage");
                return;
            }
            final CloudPlayer target = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
            if (target == null) {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("friend-prefix"));
                return;
            }

            if (!sender.isFriend(target)) {
                sender.sendMessage("user-not-friend", sender.getLanguageMessage("friend-prefix"), target.getRank().getRankColor()+target.getName());
                return;
            }

            sender.removeFriend(target);
            target.removeFriend(sender);
            sender.updateFriendInv();
            target.updateFriendInv();
            target.updateToDatabase(false);
            target.sendData(target.getServer(), DataUpdateType.UPDATE_FRIENDS, true);
            sender.updateToDatabase(false);
            sender.sendData(sender.getServer(), DataUpdateType.UPDATE_FRIENDS, true);
            sender.sendMessage("command-friend-remove-success", target.getRank().getRankColor() + target.getName());
            if (target.isOnline()) {
                target.sendMessage("command-friend-remove-success", sender.getRank().getRankColor() + sender.getName());
            }
        } else if (args[0].equalsIgnoreCase("requests")) {
            if (sender.getRequests().size() != 0) {
                List<String> requests = new ArrayList<>();
                for (FriendData friendData : sender.getRequests().values()) {
                    final CloudPlayer target3 = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(friendData.getUniqueId());
                    requests.add(target3.getRank().getRankColor() + target3.getName().toLowerCase());
                }
                int page = 1;
                if (args.length == 2 && MathUtils.isInt(args[1])) page = Integer.parseInt(args[1]);

                CommandPagination<String> pagination = new CommandPagination<>(requests, 8);

                if (pagination.getElementsFor(page).size() == 0) {
                    sender.sendMessage("page-not-found", sender.getLanguageMessage("friend-prefix"));
                    return;
                }

                sender.sendMessage("command-friend-requests", requests.size());
                pagination.printPage(page, element -> sender.sendRawMessage(" §7- §9"+element));
                return;
            }

            sender.sendMessage("command-friend-no-requests");
        } else if (args[0].equalsIgnoreCase("add")) {
            if (args.length != 2) {
                sender.sendMessage("command-friend-add-success");
                return;
            }
            if (args[1].equalsIgnoreCase(String.valueOf(sender.getName()))) {
                sender.sendMessage("player-self-interact");
                return;
            }
            final CloudPlayer target = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
            if (target == null) {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("friend-prefix"));
                return;
            }
            if (sender.isFriend(target)) {
                sender.sendMessage("command-friend-is-friend");
                return;
            }
            if (target.getSetting("advanced-player-settings", Integer.class) == 1) {
                boolean reject = false;
                if (target.getClan() != null && target.getClan().getMembers().containsKey(sender.getUniqueId()) && target.getSetting("friend-add-clan", Integer.class) == 0) {
                    reject = true;
                } else if (target.getParty() != null && target.getParty().getMembers().contains(sender) && target.getSetting("friend-add-party", Integer.class) == 0) {
                    reject = true;
                } else if (target.getRank().isTeam() && target.getSetting("friend-add-staffs", Integer.class) == 0) {
                    reject = true;
                } else if (target.getRank().equalsLevel(Rank.VIP) && target.getSetting("friend-add-vips", Integer.class) == 0) {
                    reject = true;
                } else if (target.getRank().equalsLevel(Rank.PREMIUM) && target.getSetting("friend-add-premiums", Integer.class) == 0) {
                    reject = true;
                } else if (target.getRank().equalsLevel(Rank.MEMBER) && target.getSetting("friend-add-members", Integer.class) == 0) {
                    reject = true;
                }

                if (target.getClan() != null && target.getClan().getMembers().containsKey(sender.getUniqueId()) && target.getSetting("friend-add-clan", Integer.class) == 1) {
                    reject = true;
                } if (target.getParty() != null && target.getParty().getMembers().contains(sender) && target.getSetting("friend-add-party", Integer.class) == 1) {
                    reject = true;
                } if (target.getRank().isTeam() && target.getSetting("friend-add-staffs", Integer.class) == 1) {
                    reject = true;
                } if (target.getRank().equalsLevel(Rank.VIP) && target.getSetting("friend-add-vips", Integer.class) == 1) {
                    reject = true;
                } if (target.getRank().equalsLevel(Rank.PREMIUM) && target.getSetting("friend-add-premiums", Integer.class) == 1) {
                    reject = true;
                } if (target.getRank().equalsLevel(Rank.MEMBER) && target.getSetting("friend-add-members", Integer.class) == 1) {
                    reject = true;
                }

                if (reject) {
                    sender.sendMessage("command-friend-receive-no-requests");
                    return;
                }
            } else {
                if (target.getSetting("friend-add", Integer.class) == 0) {
                    sender.sendMessage("command-friend-receive-no-requests");
                    return;
                }
            }
            if (target.hasRequest(sender)) {
                sender.sendMessage("command-friend-request-already-sent");
                return;
            }
            target.getRequests().put(sender.getUniqueId(), new FriendData(sender.getUniqueId(), System.currentTimeMillis(), false));
            sender.sendMessage("command-friend-add-success", target.getRank().getRankColor()+target.getName());
            sender.updateFriendInv();

            if (target.isOnline()) {
                target.sendMessage("command-friend-add-success-from", sender.getRank().getRankColor() + sender.getName());

                ArrayList<String[]> messageData = new ArrayList<>();
                messageData.add(new String[]{sender.getLanguageMessage("friend-prefix")+" "});
                messageData.add(new String[]{"§a["+target.getLanguageMessage("accept")+"] ", "true", "run_command", "/friend accept " + sender.getName()});
                messageData.add(new String[]{"§c["+target.getLanguageMessage("decline")+"]", "true", "run_command", "/friend deny " + sender.getName()});
                ComponentMessagePacket componentMessagePacket = new ComponentMessagePacket(target.getName(), messageData);

                target.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMPONENT_MESSAGE_PACKET, componentMessagePacket));

                target.updateFriendInv();
                target.updateToDatabase(false);
            }
        } else if (args[0].equalsIgnoreCase("deny")) {
            if (args.length != 2) {
                sender.sendMessage("command-friend-deny-usage");

                return;
            }
            final CloudPlayer target = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
            if (target == null) {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("friend-prefix"));
                return;
            }

            if (!sender.hasRequest(target)) {
                sender.sendMessage("command-friend-receive-no-requests");
                return;
            }
            sender.removeRequest(target);
            target.removeRequest(sender);

            sender.sendMessage("command-friend-deny-success", target.getRank().getRankColor() + target.getName());

            sender.updateFriendInv();
            target.updateFriendInv();
            target.updateToDatabase(false);
            sender.updateToDatabase(false);
            if (target.isOnline()) {
                target.sendMessage("command-friend-deny-success", sender.getRank().getRankColor() + sender.getName());
            }
        } else if (args[0].equalsIgnoreCase("list")) {
            if (sender.getFriends().size() != 0) {
                List<CloudPlayer> friends = new ArrayList<>();
                for (FriendData friendData : sender.getFriends().values()) {
                    final CloudPlayer target = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(friendData.getUniqueId());
                    friends.add(target);
                }
                int page = 1;
                if (args.length == 2 && MathUtils.isInt(args[1])) page = Integer.parseInt(args[1]);

                friends.sort((o1, o2) -> Long.compare(o2.getLastLogin(), o1.getLastLogin()));
                friends.sort((o1, o2) -> Boolean.compare(o2.isOnline(), o1.isOnline()));

                CommandPagination<CloudPlayer> pagination = new CommandPagination<>(friends, 8);

                if (pagination.getElementsFor(page).size() == 0) {
                    sender.sendMessage("page-not-found", sender.getLanguageMessage("friend-prefix"));
                    return;
                }

                sender.sendMessage("command-friend-list", page);
                pagination.printPage(page, friend -> {
                    String name = (sender.getFriends().get(friend.getUniqueId()).isFavorite() ? "§e✪ " : "")+friend.getRank().getRankColor()+friend.getName();
                    if (friend.isOnline()) {
                        String status = sender.getLanguageMessage("status-online");
                        if (friend.getServer().getServerState() == ServerState.INGAME) {
                            status = sender.getLanguageMessage("status-ingame");
                        }
                        if (friend.isAfk()) {
                            status = sender.getLanguageMessage("status-afk");
                        }
                        sender.sendMessage("command-friend-list-friend-online", name, status, MessageFormat.format(sender.getLanguageMessage("is-on-server"), friend.getServer().getName().toUpperCase().replace("-", "")));
                    } else {
                        String last = GoAPI.getTimeManager().getRemainingTimeString1(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), sender.getLocale(), friend.getLastLogout());
                        if (friend.getSetting("hide-last-offline", Integer.class) == 1) {
                            sender.sendMessage("command-friend-list-friend-offline-hidden", name);
                        } else {
                            sender.sendMessage("command-friend-list-friend-offline", name, last);
                        }
                    }
                });
                return;
            }

            sender.sendMessage("command-friend-no-friends");
        } else {
            sender.sendMessage("command-friend-help");
        }
    }
}