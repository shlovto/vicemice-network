package net.vicemice.modules.commands;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.network.modules.api.CoreModule;
import net.vicemice.hector.utils.CommandType;
import net.vicemice.modules.commands.commands.admin.*;
import net.vicemice.modules.commands.commands.staff.punish.TempbanCommand;
import net.vicemice.modules.commands.commands.staff.punish.TempmuteCommand;
import net.vicemice.modules.commands.commands.user.*;
import net.vicemice.modules.commands.commands.user.clan.ClanChatCommand;
import net.vicemice.modules.commands.commands.user.clan.ClanCommand;
import net.vicemice.modules.commands.commands.user.friend.FriendCommand;
import net.vicemice.modules.commands.commands.user.friend.MessageCommand;
import net.vicemice.modules.commands.commands.user.friend.RCommand;
import net.vicemice.modules.commands.commands.user.leveling.LevelCommand;
import net.vicemice.modules.commands.commands.user.leveling.XPCommand;
import net.vicemice.modules.commands.commands.user.party.PartyChatCommand;
import net.vicemice.modules.commands.commands.user.party.PartyCommand;
import net.vicemice.modules.commands.commands.staff.ban.BanInfoCommand;
import net.vicemice.modules.commands.commands.staff.ban.BanLogCommand;
import net.vicemice.modules.commands.commands.staff.pardon.UnbanCommand;
import net.vicemice.modules.commands.commands.staff.pardon.UnmuteCommand;
import net.vicemice.modules.commands.commands.staff.punish.PunishCommand;
import net.vicemice.modules.commands.commands.staff.team.LocalTeamChatCommand;
import net.vicemice.modules.commands.commands.staff.team.TeamChatCommand;
import net.vicemice.modules.commands.commands.staff.team.TeamCommand;
import net.vicemice.modules.commands.commands.user.link.UnlinkCommand;
import net.vicemice.modules.commands.commands.user.link.LinkCommand;
import net.vicemice.modules.commands.commands.user.time.PlayTimeCommand;
import net.vicemice.modules.commands.commands.vip.NickCommand;
import net.vicemice.modules.commands.listener.LoginListener;
import net.vicemice.modules.commands.listener.LogoutListener;
import net.vicemice.modules.commands.commands.staff.*;

public class Main extends CoreModule {

    private LoginListener loginListener;
    private LogoutListener logoutListener;

    public void onLoad() {
        onBootstrap();
    }

    public void onBootstrap() {
        CloudService.getCloudService().getManagerService().getCommandResolver().removeAllCommands();

        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.HELP, HelpCommand.class);

        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.AFK, AFKCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.ALERT, AlertCommand.class);

        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.LEVEL, LevelCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.XP, XPCommand.class);

        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.LANGUAGE, LanguageCommand.class);

        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.CLOUD, CloudCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.GOTO, GotoCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.HUB, HubCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.IP_COMPARE, CommandIPCompare.class);

        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.MSG, MessageCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.NOTIFY, NotifyCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.PLAYTIME, PlayTimeCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.RANG, RankCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.R, RCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.ONLINE, OnlineCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.REPLAY, ReplayCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.REPORT, ReportCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.SEND, SendCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.SERVER, ServerCommand.class);

        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.MAPEDIT, MapEditCommand.class);

        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.KICK, KickCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.PUNISH, PunishCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.TEMPBAN, TempbanCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.TEMPMUTE, TempmuteCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.UNBAN, UnbanCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.UNMUTE, UnmuteCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.LOCALTEAMCHAT, LocalTeamChatCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.TEAMCHAT, TeamChatCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.TEAM, TeamCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.BANLOG, BanLogCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.BANINFO, BanInfoCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.CHATLOG, ChatLogCommand.class);

        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.FRIENDS, FriendCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.PARTY, PartyCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.PARTYCHAT, PartyChatCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.CLAN, ClanCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.CLANCHAT, ClanChatCommand.class);

        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.NICK, NickCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.LINK, LinkCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.UNLINK, UnlinkCommand.class);

        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.PLAY, PlayCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.FORUM, ForumCommand.class);
        CloudService.getCloudService().getManagerService().getCommandResolver().registerCommand(CommandType.VOTE, VoteCommand.class);

        loginListener = new LoginListener();
        logoutListener = new LogoutListener();

        registerListener(loginListener);
        registerListener(logoutListener);
    }

    @Override
    public void onShutdown() {
        CloudService.getCloudService().getManagerService().getEventManager().unregisterListener(loginListener);
        CloudService.getCloudService().getManagerService().getEventManager().unregisterListener(logoutListener);
        CloudService.getCloudService().getManagerService().getCommandResolver().removeAllCommands();
    }
}