package net.vicemice.modules.commands.commands.vip;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.nick.NickHistory;
import net.vicemice.hector.utils.MessageGroup;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.players.utils.DataUpdateType;

public class NickCommand extends Command {

    public NickCommand() {
        super(Rank.VIP);
    }

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (System.currentTimeMillis() < sender.getNickTimeout()) {
            sender.sendMessage("command-nick-waiting");
            return;
        }
        if (sender.getNickName() != null) {
            CloudService.getCloudService().getManagerService().getNickNameManager().unnickPlayer(sender);
            sender.setNickTimeout(System.currentTimeMillis() + 1000L);
            sender.sendMessage("command-nick-remove");
            sender.removeNickHistory();
            return;
        }

        if (sender.getServer().isLobby()) {
            if (sender.getSetting("auto-nick", Integer.class) == 1) {
                sender.sendMessage("command-nick-auto-disabled");
                sender.setSetting("auto-nick", 0);
            } else {
                sender.sendMessage("command-nick-auto-enabled");
                sender.setSetting("auto-nick", 1);
            }
        } else {
            if (sender.getRank().isAdmin()) {
                String nickName = CloudService.getCloudService().getManagerService().getNickNameManager().getNickName();
                String[] texture = CloudService.getCloudService().getManagerService().getNickNameManager().getTextureData();
                if (texture == null) {
                    sender.sendMessage("nick-no-skin-found");
                    texture = new String[]{"eyJ0aW1lc3RhbXAiOjE0NDg2MzQ1ODcyNDIsInByb2ZpbGVJZCI6IjYwNmUyZmYwZWQ3NzQ4NDI5ZDZjZTFkMzMyMWM3ODM4IiwicHJvZmlsZU5hbWUiOiJNSEZfUXVlc3Rpb24iLCJzaWduYXR1cmVSZXF1aXJlZCI6dHJ1ZSwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlLzUxNjNkYWZhYzFkOTFhOGM5MWRiNTc2Y2FhYzc4NDMzNjc5MWE2ZTE4ZDhmN2Y2Mjc3OGZjNDdiZjE0NmI2In19fQ==", "eosGuYbzqNK8UBllFajP+cVBbak0Qnqt2NafONsp7U18WIlVajvoKl0ulnEYlAPXKmV4X2B4XF0KklKHT4jwf4sa1HNLyJfYu7lyxJPKGFrYpdGEL2LAiMRKrP7U/b2ZmQHQQ8cKmPScsf/ltLggG9J0mQBxrd4VuoSIje21Y7QxXOMU80QdGyG8MlBqoEM15zUYlsYp2+nTpK0SqaRdJLymdHK9rdShl+hAgti8FKrRgS5Sr/OjICz6QXSLUrvpis7HoZk47GL/D+SmNNmtv+pT95UZz0ObYP3iEFer7ZHsKLS4d6qGe8qcyu+CLJ3cvFf66kuqOc8YbPjzHETVT9KrZkDRkFWoyL4jgcjFMc2l70wLL4Uhz94Rb7flBVeT4BXCoXjn9hXT2oZbRdtQsXMG87Bik0MGeqoQgtqaeCi8uwOeW2kbjl4eD7iley9I4axq+J74tYZL9+734fLdK0LMLZ2ncy3vGc4Kj2NFoKSttFyA3Vei6YeUg6b2hTiDS+meVsdpdO6NkQVWG3dlpENZk0zm+p3U33KMNWcjAxXzsWmw+a68ktUjuAbj1oZniVkPI6VPMm21/a1N92mVBKFQMX2rj1oiUIByIZe/iHPQe3luRiRBLIUwU1L+0LEzdPpq0eM2R6PQj8GNz/fxbpTCy+qcf1Pe9/4HoDGLxE8="};
                }
                if (nickName != null) {
                    CloudService.getCloudService().getManagerService().getNickNameManager().nickPlayer(sender, nickName, texture, sender.getServer());
                    MessageGroup messageGroup = new MessageGroup(sender);

                    sender.sendMessage("nick-playing-as", nickName);

                    messageGroup.send();
                    NickHistory nickHistory = new NickHistory(nickName, sender.getName());
                    sender.setNickHistory(nickHistory);
                    CloudService.getCloudService().getManagerService().getNickNameManager().getNickHistories().add(nickHistory);
                } else {
                    sender.sendMessage("no-nick-found");
                }
            } else {
                sender.sendMessage("command-nick-not-available");
            }
        }

        sender.sendData(sender.getServer(), DataUpdateType.UPDATE_NICK, true);
    }
}
