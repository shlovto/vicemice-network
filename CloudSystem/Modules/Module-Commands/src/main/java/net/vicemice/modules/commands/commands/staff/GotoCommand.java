package net.vicemice.modules.commands.commands.staff;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;

public class GotoCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (sender.getRank().isJrModeration()) {
            if (args.length == 1) {
                CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[0]);
                if (getPlayer != null) {
                    if (getPlayer.isOnline()) {
                        if (!getPlayer.getServer().equals(sender.getServer())) {
                            sender.sendMessage("command-goto-connect");
                            sender.connectToServer(getPlayer.getServer().getName());
                        } else {
                            sender.sendMessage("command-goto-same-server");
                        }
                    } else {
                        sender.sendMessage("user-not-online", sender.getLanguageMessage("team-prefix"), (getPlayer.getRank().getRankColor()+getPlayer.getName()));
                    }
                } else {
                    sender.sendMessage("user-not-exist", sender.getLanguageMessage("team-prefix"));
                }
            } else {
                sender.sendMessage("command-goto-usage");
            }
        } else {
            sender.sendMessage("command-no-permission");
        }
    }
}