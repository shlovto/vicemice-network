package net.vicemice.modules.commands.commands.user;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;

public class OnlineCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        sender.sendMessage("currently-playing", CloudService.getCloudService().getManagerService().getProxyManager().getGlobalOnline());
    }
}