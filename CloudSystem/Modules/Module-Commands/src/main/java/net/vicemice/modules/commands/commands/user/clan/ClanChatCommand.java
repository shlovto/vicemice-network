package net.vicemice.modules.commands.commands.user.clan;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;

import java.util.UUID;

public class ClanChatCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (sender.getClan() != null) {
            if (sender.getPlayTime() < 60) {
                sender.sendMessage("clan-chat-min-playtime-hours", 3);
                return;
            } else if (sender.getPlayTime() < 120) {
                sender.sendMessage("clan-chat-min-playtime-hours", 2);
                return;
            } else if (sender.getPlayTime() == 120) {
                sender.sendMessage("clan-chat-min-playtime-hours", 1);
                return;
            } else if (sender.getPlayTime() < 180) {
                sender.sendMessage("clan-chat-min-playtime-minutes", (180- sender.getPlayTime()));
                return;
            }

            if (sender.getSetting("clan-chat-messages", Integer.class) == 0) {
                sender.sendMessage("command-clanchat-enable-messages");
                return;
            }

            if (args.length == 0) {
                sender.sendMessage("command-chat-usage");
                return;
            }
            StringBuilder sb = new StringBuilder();
            int i = 0;
            while (i < args.length) {
                if (i < args.length) {
                    sb.append(args[i] + " ");
                } else {
                    sb.append(args[i]);
                }
                ++i;
            }
            String message = sb.toString();

            for (UUID uniqueId : sender.getClan().getMembers().keySet()) {
                CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uniqueId);
                if (cloudPlayer.getSetting("clan-chat-messages", Integer.class) == 0) continue;
                cloudPlayer.sendMessage("clanchat-format", (sender.getRank().getRankColor()+sender.getName()), message);
            }
        } else {
            sender.sendMessage("clan-not-in");
        }
    }
}