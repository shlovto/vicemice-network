package net.vicemice.modules.commands.commands.staff;

import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.Rank;

public class NotifyCommand extends Command {

    public NotifyCommand() {
        super(Rank.MODERATOR);
    }

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (sender.isNotify()) {
            sender.setNotify(false);
            sender.sendMessage("command-notify-disabled");
            return;
        }
        sender.setNotify(true);
        sender.sendMessage("command-notify-enabled");
    }
}