package net.vicemice.modules.commands.commands.user;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.packets.types.player.replay.OpenReplayHistoryPacket;
import net.vicemice.hector.packets.types.player.replay.PacketReplayEntries;
import net.vicemice.hector.packets.types.player.replay.SaveInReplayHistoryPacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.players.utils.DataUpdateType;
import net.vicemice.hector.utils.players.utils.PlayerState;

import java.util.List;

public class ReplayCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (sender.getServer() != null && sender.getServer().getTemplateName() != null && sender.getServer().getTemplateName().equalsIgnoreCase("Replay")) {
            sender.sendMessage("command-replay-help-in-replay");
            return;
        }

        if (args.length < 1) {
            sender.sendMessage("command-replay-help");
            return;
        }

        if (args[0].equalsIgnoreCase("play") || (args[0].equalsIgnoreCase("view")) && sender.getRank().isAdmin()) {
            if (args.length == 2) {
                PacketReplayEntries.PacketReplayEntry entry = CloudService.getCloudService().getManagerService().getReplayHistoryManager().getReplay(args[1]);
                if (entry != null) {
                    sender.watchReplay(args[1]);
                    return;
                }
                sender.sendMessage("wrong-replay-id", args[1]);
            } else {
                sender.sendMessage("no-replay-id");
            }
        } else if (args[0].equalsIgnoreCase("toggle")) {
            if (sender.getSetting("replay-history-private", Integer.class) == 0) {
                sender.sendMessage("command-replay-list-toggle-off");
                sender.setSetting("replay-history-private", 1);
            } else if (sender.getSetting("replay-history-private", Integer.class) == 1) {
                sender.sendMessage("command-replay-list-toggle-on");
                sender.setSetting("replay-history-private", 0);
            }
            sender.sendData(sender.getServer(), DataUpdateType.UPDATE_SETTINGS, true);
        } else if (args[0].equalsIgnoreCase("list")) {
            if (args.length != 2) {
                sender.sendMessage("command-replay-list-usage");
                return;
            }
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
            if (cloudPlayer != null) {
                if (sender.getServer() == null || sender.getServer() != null && sender.getServer().getTemplateName() == null || sender.getServer() != null && sender.getServer().getTemplateName() != null && !sender.getServer().getTemplateName().equalsIgnoreCase("lobby")) {
                    sender.sendMessage("command-replay-list-only-lobby", cloudPlayer.getRank().getRankColor()+cloudPlayer.getName());
                    return;
                }
                if (cloudPlayer.getName().equals(sender.getName()) || cloudPlayer.isFriend(sender)) {
                    if (cloudPlayer.getName().equals(sender.getName()) || cloudPlayer.getSetting("replay-history-private", Integer.class) == 0) {
                        List<PacketReplayEntries.PacketReplayEntry> recentEntries = CloudService.getCloudService().getManagerService().getReplayHistoryManager().getRecentEntries(cloudPlayer.getUniqueId());
                        List<PacketReplayEntries.PacketReplayEntry> savedEntries = CloudService.getCloudService().getManagerService().getReplayHistoryManager().getSavedEntries(cloudPlayer.getUniqueId());
                        sender.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.OPEN_REPLAY_HISTORY, new OpenReplayHistoryPacket(sender.getUniqueId(), cloudPlayer.getUniqueId(), cloudPlayer.getName(), cloudPlayer.getRank(), recentEntries, savedEntries)));
                    } else {
                        sender.sendMessage("replays-private", cloudPlayer.getRank().getRankColor()+cloudPlayer.getName());
                    }
                } else {
                    sender.sendMessage("user-not-friend", sender.getLanguageMessage("replay-prefix"), cloudPlayer.getRank().getRankColor()+cloudPlayer.getName());
                }
            } else {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("replay-prefix"));
            }
        } else if (args[0].equalsIgnoreCase("save")) {
            if (CloudService.getCloudService().getManagerService().getReplayHistoryManager().getSavedEntries(sender.getUniqueId()).size() >= (sender.getRank().isHigherEqualsLevel(Rank.PREMIUM) ? 27 : 9)) {
                sender.sendMessage("replay-saved-limit");
                return;
            }
            if (sender.getServer() == null) {
                sender.sendMessage("command-replay-save-error");
                return;
            }
            if (!sender.getServer().getServerTemplate().isPlayAble()) {
                sender.sendMessage("command-replay-save-not-playable");
                return;
            }
            if (sender.getServer().getServerState() == ServerState.INGAME) {
                if (sender.getPlayerState() == PlayerState.INGAME) {
                    sender.getServer().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.SAVE_IN_REPLAY_HISTORY, new SaveInReplayHistoryPacket(sender.getUniqueId())));
                    sender.sendMessage("command-replay-save-queued");
                } else {
                    sender.sendMessage("command-replay-save-not-as-spectator");
                }
            } else {
                sender.sendMessage("command-replay-save-not-ingame");
            }
        } else {
            sender.sendMessage("command-replay-help");
        }
    }
}