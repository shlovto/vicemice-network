package net.vicemice.modules.commands.commands.user.clan;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.packets.types.player.ComponentMessagePacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.clan.Clan;
import net.vicemice.hector.player.party.Party;
import net.vicemice.hector.utils.language.Language;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.utils.players.clan.ClanRank;
import net.vicemice.hector.utils.players.utils.DataUpdateType;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.UUID;

public class ClanCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        /*if (sender.getServer().getName().equalsIgnoreCase("BuildEvent")) {
            sender.sendMessageNEW("command-disabled");
            return;
        }*/

        if (args.length == 0) {
            sendHelp(sender);
        } else if (args[0].equalsIgnoreCase("2")) {
            sendHelp2(sender);
        } else if (args[0].equalsIgnoreCase("toggle")) {
            if (sender.getSetting("clan-invites", Integer.class) == 0) {
                sender.sendMessage("clan-accept-requests");
                sender.setSetting("clan-invites", 1);
                return;
            } else if (sender.getSetting("clan-invites", Integer.class) == 1) {
                sender.sendMessage("clan-accept-no-requests");
                sender.setSetting("clan-invites", 0);
                return;
            }
            sender.sendData(sender.getServer(), DataUpdateType.UPDATE_SETTINGS, true);
        } else if (args[0].equalsIgnoreCase("create")) {
            if (args.length == 3) {
                String name = args[1];
                String tag = args[2];
                if (sender.getClan() != null) {
                    sender.sendMessage("clan-not-in");
                    return;
                }
                if (!(name.length() >= 3 && name.length() < 17) || !checkCharacter(name)) {
                    sender.sendMessage("name-length-chars");
                    return;
                }
                if (!(tag.length() >= 2 && tag.length() < 6) || !checkCharacter(tag)) {
                    sender.sendMessage("clan-tag-length-chars");
                    return;
                }
                if (CloudService.getCloudService().getManagerService().getClanManager().getClanNames().contains(name.toLowerCase())) {
                    sender.sendMessage("clan-name-exist");
                    return;
                }
                if (CloudService.getCloudService().getManagerService().getClanManager().getClanTags().contains(tag.toLowerCase())) {
                    sender.sendMessage("clan-tag-exist");
                    return;
                }
                Clan clan = new Clan(name, tag, sender);
                CloudService.getCloudService().getManagerService().getClanManager().getClans().add(clan);
                CloudService.getCloudService().getManagerService().getClanManager().getClanNames().add(name.toLowerCase());
                CloudService.getCloudService().getManagerService().getClanManager().getClanTags().add(tag.toLowerCase());
                clan.save(true);
            } else {
                sender.sendMessage("command-clan-create-usage");
            }
        } else if (args[0].equalsIgnoreCase("invite")) {
            if (args.length == 2) {
                if (sender.getClan() == null) {
                    sender.sendMessage("clan-not-in");
                    return;
                }
                CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
                if (getPlayer != null) {
                    if (getPlayer.getClan() != null) {
                        sender.sendMessage("clan-is-in-clan", (getPlayer.getRank().getRankColor()+getPlayer.getName()));
                        return;
                    }
                    if (!sender.getClan().getMembers().get(sender.getUniqueId()).isHigherEquals(ClanRank.MODERATOR)) {
                        sender.sendMessage("clan-only-moderator");
                        return;
                    }

                    sender.getClan().inviteMember(sender, getPlayer);
                } else {
                    sender.sendMessage("user-not-exist", sender.getLanguageMessage("clan-prefix"));
                }
            } else {
                sender.sendMessage("command-clan-invite-usage");
            }
        } else if (args[0].equalsIgnoreCase("leave")) {
            if (args.length == 1) {
                if (sender.getClan() == null) {
                    sender.sendMessage("clan-not-in");
                    return;
                }
                if (sender.getClan().getMembers().get(sender.getUniqueId()).equals(ClanRank.LEADER)) {
                    sender.sendMessage("clan-leave-not-leader");
                    return;
                }
                sender.getClan().memberLeave(sender);
            } else {
                sender.sendMessage("command-clan-leave-usage");
            }
        } else if (args[0].equalsIgnoreCase("delete")) {
            if (args.length == 1 || args.length == 2 && args[1].equals("confirm")) {
                if (sender.getClan() == null) {
                    sender.sendMessage("clan-not-in");
                    return;
                }
                if (!sender.getClan().getMembers().get(sender.getUniqueId()).equals(ClanRank.LEADER)) {
                    sender.sendMessage("clan-only-leader");
                    return;
                }
                if (args.length == 2 && args[1].equals("confirm")) {
                    CloudService.getCloudService().getManagerService().getClanManager().getClans().remove(sender.getClan());
                    CloudService.getCloudService().getManagerService().getClanManager().getClanNames().remove(sender.getClan().getName());
                    CloudService.getCloudService().getManagerService().getClanManager().getClanTags().remove(sender.getClan().getTag());
                    sender.getClan().deleteClan();
                } else {
                    ArrayList<String[]> messageData = new ArrayList<String[]>();
                    sender.sendMessage("clan-delete-confirm");
                    messageData.add(new String[]{sender.getLanguageMessage("clan-prefix")+" "});
                    messageData.add(new String[]{"§8[§6"+sender.getLanguageMessage("confirm")+"§8]", "true", "run_command", "/clan delete confirm"});
                    ComponentMessagePacket componentMessagePacket = new ComponentMessagePacket(sender.getName().toLowerCase(), messageData);
                    sender.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMPONENT_MESSAGE_PACKET, componentMessagePacket));
                }
            } else {
                sender.sendMessage("command-clan-delete-usage");
            }
        } else if (args[0].equalsIgnoreCase("party")) {
            if (args.length == 1) {
                if (sender.getClan() == null) {
                    sender.sendMessage("clan-not-in");
                    return;
                }
                if (sender.getClan().getMembers().size() == 1) {
                    sender.sendMessage("clan-no-members");
                    return;
                }
                if (sender.getParty() == null) {
                    sender.setParty(new Party(sender));
                }
                for (UUID uuid : sender.getClan().getMembers().keySet()) {
                    CloudPlayer member = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uuid);
                    if (member.equals(sender)) continue;

                    if (!member.isOnline()) continue;
                    if (member.getParty() != null) {
                        sender.sendMessage("clan-is-in-party", (member.getRank().getRankColor()+member.getName()));
                        continue;
                    }
                    sender.getParty().inviteMember(member);
                }
            } else {
                sender.sendMessage("command-clan-party-usage");
            }
        } else if (args[0].equalsIgnoreCase("promote")) {
            if (args.length != 2) {
                sender.sendMessage("command-clan-promote-usage");
                return;
            }
            if (sender.getClan() != null) {
                if (sender.getClan().getMembers().get(sender.getUniqueId()) == ClanRank.LEADER) {
                    CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
                    if (getPlayer == null) {
                        sender.sendMessage("user-not-exist", sender.getLanguageMessage("clan-prefix"));
                        return;
                    }
                    sender.getClan().promotePlayer(sender, getPlayer);
                } else {
                    sender.sendMessage("clan-only-leader");
                }
            } else {
                sender.sendMessage("clan-not-in");
            }
        } else if (args[0].equalsIgnoreCase("demote")) {
            if (args.length != 2) {
                sender.sendMessage("command-clan-demote-usage");
                return;
            }
            if (sender.getClan() != null) {
                if (sender.getClan().getMembers().get(sender.getUniqueId()) == ClanRank.LEADER) {
                    CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
                    if (getPlayer == null) {
                        sender.sendMessage("user-not-exist", sender.getLanguageMessage("clan-prefix"));
                        return;
                    }
                    sender.getClan().demotePlayer(sender, getPlayer);
                } else {
                    sender.sendMessage("clan-only-leader");
                }
            } else {
                sender.sendMessage("clan-not-in");
            }
        } else if (args[0].equalsIgnoreCase("kick")) {
            if (args.length != 2) {
                sender.sendMessage("command-clan-kick-usage");
                return;
            }
            if (sender.getClan() != null) {
                if (sender.getClan().getMembers().get(sender.getUniqueId()) == ClanRank.LEADER) {
                    CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
                    if (getPlayer == null) {
                        sender.sendMessage("user-not-exist", sender.getLanguageMessage("clan-prefix"));
                        return;
                    }
                    if (sender.getLanguage() == Language.LanguageType.GERMAN) {
                        sender.getClan().kickMember(sender, getPlayer);
                    }
                } else {
                    sender.sendMessage("clan-only-leader");
                }
            } else {
                sender.sendMessage("clan-not-in");
            }
        } else if (args[0].equalsIgnoreCase("accept")) {
            if (args.length != 2) {
                sender.sendMessage("command-clan-accept-usage");
                return;
            }
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
            if (cloudPlayer != null) {
                if (sender.getClan() != null) {
                    sender.sendMessage("clan-already-in");
                    return;
                }
                if (cloudPlayer.getClan() != null) {
                    cloudPlayer.getClan().acceptInvite(sender);
                } else {
                    sender.sendMessage("clan-is-in-clan", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()));
                }
            } else {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("clan-prefix"));
            }
        } else if (args[0].equalsIgnoreCase("uinfo")) {
            if (args.length != 2) {
                sender.sendMessage("command-clan-userinfo-usage");
                return;
            }
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
            if (cloudPlayer != null) {
                if (cloudPlayer.getClan() != null) {
                    this.sendClanInformation(sender, cloudPlayer.getClan());
                } else {
                    sender.sendMessage("clan-not-in-clan", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()));
                }
            } else {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("clan-prefix"));
            }
        } else if (args[0].equalsIgnoreCase("ninfo")) {
            if (args.length != 2) {
                sender.sendMessage("command-clan-nameinfo-usage");
                return;
            }
            Clan clan = CloudService.getCloudService().getManagerService().getClanManager().getClanByName(args[1]);
            if (clan != null) {
                this.sendClanInformation(sender, clan);
            } else {
                sender.sendMessage("clan-not-exist");
            }
        } else if (args[0].equalsIgnoreCase("tinfo")) {
            if (args.length != 2) {
                sender.sendMessage("command-clan-taginfo-usage");
                return;
            }
            Clan clan = CloudService.getCloudService().getManagerService().getClanManager().getClanByTag(args[1]);
            if (clan != null) {
                this.sendClanInformation(sender, clan);
            } else {
                sender.sendMessage("clan-not-exist");
            }
        } else if (args[0].equalsIgnoreCase("list") || (args[0].equalsIgnoreCase("info"))) {
            if (sender.getClan() != null) {
                this.sendClanInformation(sender, sender.getClan());
            } else {
                sender.sendMessage("clan-not-in");
            }
        } else {
            sendHelp(sender);
        }
    }

    public void sendHelp(CloudPlayer cloudPlayer) {
        cloudPlayer.sendMessage("command-clan-help-page-one");
    }

    public void sendHelp2(CloudPlayer cloudPlayer) {
        cloudPlayer.sendMessage("command-clan-help-page-two");
    }

    public void sendClanInformation(CloudPlayer player, Clan clan) {
        StringBuilder leaderList = new StringBuilder();
        int leaderSize = 0;
        StringBuilder moderatorList = new StringBuilder();
        int moderatorSize = 0;
        StringBuilder memberList = new StringBuilder();
        int memberSize = 0;
        for (UUID uuid : clan.getMembers().keySet()) {
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uuid);
            if (clan.getMembers().get(uuid) == ClanRank.LEADER) {
                leaderSize++;
                leaderList.append("\n"+(MessageFormat.format(player.getLanguageMessage("command-clan-member-list-format"), cloudPlayer.getRank().getRankColor()+cloudPlayer.getName(), (cloudPlayer.isOnline() ? player.getLanguageMessage("status-online") : player.getLanguageMessage("status-offline")))).replace("&", "§"));
            }
            if (clan.getMembers().get(uuid) == ClanRank.MODERATOR) {
                moderatorSize++;
                moderatorList.append("\n"+(MessageFormat.format(player.getLanguageMessage("command-clan-member-list-format"), cloudPlayer.getRank().getRankColor()+cloudPlayer.getName(), (cloudPlayer.isOnline() ? player.getLanguageMessage("status-online") : player.getLanguageMessage("status-offline")))).replace("&", "§"));
            }
            if (clan.getMembers().get(uuid) == ClanRank.MEMBER) {
                memberSize++;
                memberList.append("\n"+(MessageFormat.format(player.getLanguageMessage("command-clan-member-list-format"), cloudPlayer.getRank().getRankColor()+cloudPlayer.getName(), (cloudPlayer.isOnline() ? player.getLanguageMessage("status-online") : player.getLanguageMessage("status-offline")))).replace("&", "§"));
            }
        }

        player.sendMessage("command-clan-information", clan.getName(), clan.getTag(), clan.getMembers().size(), leaderSize, (leaderSize > 0 ? leaderList.toString() : ""), moderatorSize, (moderatorSize > 0 ? moderatorList.toString() : ""), memberSize, (memberSize > 0 ? memberList.toString() : ""));
    }

    public boolean checkCharacter(String name) {
        for (char c : name.toCharArray()) {
            if (c == 'ö' || c == 'Ö' || c == 'ä' || c == 'Ä') continue;
            if (c == 'ü' || c == 'Ü') continue;
            if ('0' <= c && c <= '9') continue;
            if ('a' <= c && c <= 'z') continue;
            if ('A' <= c && c <= 'Z') continue;
            if (c == '$' || c == '&' || c == '#' || c == '_') continue;
            return false;
        }
        return true;
    }
}