package net.vicemice.modules.commands.commands.user;

import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;

/*
 * Class created at 12:44 - 27.02.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class AFKCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (!sender.isAfk()) {
            sender.setAfk(true);
            sender.sendMessage("afk-idle");
            if (sender.getParty() != null) {
                sender.getParty().sendMessageToParty("party-player-marked-afk", (sender.getRank().getRankColor() + sender.getName()));
            }
        } else if (sender.isAfk()) {
            sender.setAfk(false);
            sender.sendMessage("afk-not-longer-idle");
            if (sender.getParty() != null) {
                sender.getParty().sendMessageToParty("party-player-unmarked-afk", (sender.getRank().getRankColor() + sender.getName()));
            }
        }
    }
}
