package net.vicemice.modules.commands.commands.staff.ban;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.utils.Abuses;
import net.vicemice.hector.utils.PunishType;
import net.vicemice.hector.utils.math.MathUtils;
import net.vicemice.hector.utils.players.punish.Punish;
import net.vicemice.hector.utils.players.utils.Pagination;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class BanLogCommand extends Command {

    public BanLogCommand() {
        super(Area.MODERATING);
    }

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (args.length >= 1) {
            int page = 1;
            if (args.length == 2)
                if (MathUtils.isInt(args[1])) {
                    page = Integer.parseInt(args[1]);
                }
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[0]);

            if (cloudPlayer != null) {
                Pagination<Abuses.AbuseData> abusePagination = new Pagination<>(getPunishments(cloudPlayer), 9);

                sender.sendMessage("command-banlog-information", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()), ((abusePagination.getPages() == 0 || abusePagination.getElementsFor(page).isEmpty()) ? sender.getLanguageMessage("no-entries-found") : MessageFormat.format(sender.getLanguageMessage("page"), page, abusePagination.getPages())));
                if (abusePagination.getPages() > 0) {
                    abusePagination.printPage(page, data -> {
                        Punish punish = CloudService.getCloudService().getManagerService().getPunishManager().getByKey(data.getReason());
                        String reason = data.getReason();

                        if (punish != null) {
                            reason = sender.getLanguageMessage(reason.toUpperCase());
                        }

                        CloudPlayer by = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(data.getAuthor());
                        if (data.getPunishType() == PunishType.BAN || data.getPunishType() == PunishType.MUTE || data.getPunishType() == PunishType.KICK) {
                            sender.sendMessage("command-banlog-format", data.getPunishType().name().toUpperCase(), reason, (by.getRank().getRankColor()+by.getName()), data.getComment(), GoAPI.getTimeManager().getTime("dd.MM.yyyy - HH:mm", data.getCreated()));
                        } else {
                            sender.sendMessage("command-banlog-removed-format", data.getPunishType().name().toUpperCase(), (by.getRank().getRankColor()+by.getName()), data.getComment(), GoAPI.getTimeManager().getTime("dd.MM.yyyy - HH:mm", data.getCreated()));
                        }
                    });
                }
            } else {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("team-prefix"));
            }
        } else {
            sender.sendMessage("command-banlog-usage");
        }
    }

    private List<Abuses.AbuseData> getPunishments(CloudPlayer cloudPlayer) {
        List<Abuses.AbuseData> dataList = new ArrayList<>();
        CloudService.getCloudService().getManagerService().getAbuseManager().getBanId().values().forEach(e -> {
            if (!e.getUser().toString().equalsIgnoreCase(cloudPlayer.getUniqueId().toString())) return;
            dataList.add(e);
        });
        dataList.sort(new Comparator<Abuses.AbuseData>() {
            @Override
            public int compare(Abuses.AbuseData o1, Abuses.AbuseData o2) {
                return Long.compare(o2.getCreated(), o1.getCreated());
            }
        });
        return dataList;
    }
}
