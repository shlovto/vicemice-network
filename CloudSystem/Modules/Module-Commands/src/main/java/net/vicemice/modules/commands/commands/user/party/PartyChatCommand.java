package net.vicemice.modules.commands.commands.user.party;

import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.Rank;

public class PartyChatCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (sender.getParty() != null) {
            if (sender.getPlayTime() < 60 && !sender.getRank().isHigherEqualsLevel(Rank.VIP)) {
                sender.sendMessage("party-min-playtime-hours", 3);
                return;
            } else if (sender.getPlayTime() < 120 && !sender.getRank().isHigherEqualsLevel(Rank.VIP)) {
                sender.sendMessage("party-min-playtime-hours", 2);
                return;
            } else if (sender.getPlayTime() == 120 && !sender.getRank().isHigherEqualsLevel(Rank.VIP)) {
                sender.sendMessage("party-min-playtime-hours", 1);
                return;
            } else if (sender.getPlayTime() < 180 && !sender.getRank().isHigherEqualsLevel(Rank.VIP)) {
                sender.sendMessage("party-min-playtime-minutes", (180- sender.getPlayTime()));
                return;
            }

            if (args.length == 0) {
                sender.sendMessage("command-partychat-usage");
                return;
            }
            StringBuilder sb = new StringBuilder();
            int i = 0;
            while (i < args.length) {
                if (i < args.length) {
                    sb.append(args[i] + " ");
                } else {
                    sb.append(args[i]);
                }
                ++i;
            }
            String message = sb.toString();
            sender.getParty().sendMessageToParty("partychat-format", (sender.getRank().getRankColor()+sender.getName()), message);
        } else {
            sender.sendMessage("party-not-in");
        }
    }
}