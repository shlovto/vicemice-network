package net.vicemice.modules.commands.commands.user;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.packets.types.player.ComponentMessagePacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.language.Language;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.utils.players.utils.DataUpdateType;

import java.util.ArrayList;
import java.util.Locale;

public class LanguageCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (args.length == 1) {
            Locale locale = Locale.forLanguageTag(args[0]);

            if (locale != null && CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager().getLocales().containsKey(locale)) {
                sender.changeLocale(locale);
                sender.sendMessage("command-language-changed", locale.getDisplayName(locale));
                sender.sendData(sender.getServer(), DataUpdateType.UPDATE_LOCALE, true);
                sender.updateToDatabase(false);
            } else {
                sender.sendMessage("command-language-not-found", (locale != null ? locale.getDisplayName(sender.getLocale()) : args[0]));
            }
            return;
        }

        sender.sendMessage("command-language-message", Language.LanguageType.all());

        for (Locale locale : CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager().getLocales().keySet()) {
            ArrayList<String[]> messageData = new ArrayList<String[]>();
            messageData.add(new String[]{"§7- §6" + locale.getDisplayName(sender.getLocale()), "true", "run_command", "/language " + locale.toLanguageTag()});
            ComponentMessagePacket componentMessagePacket = new ComponentMessagePacket(sender.getName(), messageData);
            sender.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMPONENT_MESSAGE_PACKET, componentMessagePacket));
        }
    }
}