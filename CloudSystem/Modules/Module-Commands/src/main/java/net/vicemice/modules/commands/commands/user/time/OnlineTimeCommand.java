package net.vicemice.modules.commands.commands.user.time;

import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;

/*
 * Class created at 04:15 - 25.04.2020
 * Copyright (C) elrobtossohn
 */
public class OnlineTimeCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        sender.sendMessage("command-onlinetime", (sender.getStats().getGlobalStatsValue("onlineTime") / 60));
    }
}