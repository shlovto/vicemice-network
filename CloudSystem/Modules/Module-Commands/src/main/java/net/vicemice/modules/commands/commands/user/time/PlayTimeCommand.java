package net.vicemice.modules.commands.commands.user.time;

import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;

public class PlayTimeCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        sender.sendMessage("command-playtime", (sender.getPlayTime() / 60));
    }
}