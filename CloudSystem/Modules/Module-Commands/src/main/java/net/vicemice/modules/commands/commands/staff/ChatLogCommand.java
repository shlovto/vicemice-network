package net.vicemice.modules.commands.commands.staff;

import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;

public class ChatLogCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        sender.sendMessage("chatlog-command-message");
    }
}