package net.vicemice.modules.commands.commands.staff.team;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.MessageGroup;
import net.vicemice.hector.utils.Rank;
import net.vicemice.hector.utils.math.MathUtils;
import net.vicemice.hector.utils.players.utils.Pagination;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

public class TeamCommand extends Command {

    public TeamCommand() {
        super(Rank.MODERATOR);
    }

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (!sender.getRank().isTeam()) {
            sender.sendMessage("command-no-permission");
            return;
        }
        if (args.length == 1 && args[0].equalsIgnoreCase("list")) {
            final List<CloudPlayer> teamMembers = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithRank().stream().map(e -> CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayer(e)).filter(e -> e.isPresent()).map(e -> e.get()).filter(e -> e.getRank().isTeam()).collect(Collectors.toList());
            Collections.sort(teamMembers, new Comparator<CloudPlayer>() {

                @Override
                public int compare(CloudPlayer o1, CloudPlayer o2) {
                    if (o1.getRank() == o2.getRank()) {
                        return Boolean.compare(o2.isOnline(), o1.isOnline());
                    }
                    return Integer.compare(o2.getRank().getAccessLevel(), o1.getRank().getAccessLevel());
                }
            });

            MessageGroup messageGroup = new MessageGroup(sender);
            sender.sendMessage("command-team-all-list", teamMembers.size());
            for (CloudPlayer member : teamMembers) {
                String prefix = "§7[" + (member.isOnline() ? "§a✔" : "§c✘") + "§7] ";
                messageGroup.addMessage(prefix + member.getRank().getRankColor() + member.getName() + " §7(" + member.getRank().getRankColor() + member.getRank().getRankName() + "§7)");
            }
            messageGroup.send();
            return;
        }

        int page = 1;
        if (args.length == 1 && MathUtils.isInt(args[0]))
            page = Integer.parseInt(args[0]);

        final List<CloudPlayer> teamMembers = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithRank().stream().map(e -> CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayer(e)).filter(e -> e.isPresent()).map(e -> e.get()).filter(e -> e.isOnline()).filter(e -> e.getRank().isTeam()).collect(Collectors.toList());
        Collections.sort(teamMembers, new Comparator<CloudPlayer>() {

            @Override
            public int compare(CloudPlayer o1, CloudPlayer o2) {
                if (o1.getRank() == o2.getRank()) {
                    return Boolean.compare(o2.isOnline(), o1.isOnline());
                }
                return Integer.compare(o2.getRank().getAccessLevel(), o1.getRank().getAccessLevel());
            }
        });

        Pagination<CloudPlayer> members = new Pagination<>(teamMembers, 9);
        if (members.getElementsFor(page).size() == 0) {
            sender.sendMessage("page-not-found", sender.getLanguageMessage("prefix"));
            return;
        }

        sender.sendMessage("command-team-online-list", teamMembers.size());
        members.printPage(page, member -> {
            if (!member.isOnline()) return;
            String name = member.getRank().getRankColor()+member.getName();
            String status = sender.getLanguageMessage("status-online");
            if (member.getServer().getServerState() == ServerState.INGAME) {
                status = sender.getLanguageMessage("status-ingame");
            }
            if (member.isAfk()) {
                status = sender.getLanguageMessage("status-afk");
            }
            sender.sendMessage("command-friend-list-friend-online", name, status, MessageFormat.format(sender.getLanguageMessage("is-on-server"), member.getServer().getName().toUpperCase().replace("-", "")));
        });
    }
}