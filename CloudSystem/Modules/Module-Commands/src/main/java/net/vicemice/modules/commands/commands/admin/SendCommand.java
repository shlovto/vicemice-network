package net.vicemice.modules.commands.commands.admin;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.Rank;

public class SendCommand extends Command {

    public SendCommand() {
        super(Rank.ADMIN);
    }

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (args.length == 2) {
            CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[0]);
            if (getPlayer == null) {
                sender.sendMessage("player-not-exist");
                return;
            }
            if (!getPlayer.isOnline()) {
                sender.sendMessage("user-not-online", sender.getLanguageMessage("cloud-prefix"), getPlayer.getRank().getRankColor()+getPlayer.getName());
                return;
            }
            Server server = CloudService.getCloudService().getManagerService().getServerManager().getServerByName(args[1]);
            if (server == null) {
                sender.sendMessage("server-not-exist");
                return;
            }
            getPlayer.connectToServer(server.getName());
            sender.sendMessage("command-send-success");
        } else {
            sender.sendMessage("command-send-usage");
        }
    }
}