package net.vicemice.modules.commands.commands.staff.ban;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.players.punish.Punish;

public class BanInfoCommand extends Command {

    public BanInfoCommand() {
        super(Area.MODERATING);
    }

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (args.length == 1) {
            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[0]);

            if (cloudPlayer != null) {
                boolean has = false;

                if (cloudPlayer.getAbuses().getBan() != null) {
                    has = true;
                    Punish punish = CloudService.getCloudService().getManagerService().getPunishManager().getByKey(cloudPlayer.getAbuses().getBan().getReason());
                    String reason = cloudPlayer.getAbuses().getBan().getReason();

                    if (punish != null) {
                        reason = sender.getLanguageMessage(reason.toUpperCase());
                    }

                    CloudPlayer by = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(cloudPlayer.getAbuses().getBan().getAuthor());
                    sender.sendMessage("command-baninfo-format", "BAN", reason, (by.getRank().getRankColor()+by.getName()), cloudPlayer.getAbuses().getBan().getComment(), GoAPI.getTimeManager().getRemainingTimeString(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), sender.getLocale(), cloudPlayer.getAbuses().getBan().getLength()));
                }

                if (cloudPlayer.getAbuses().getMute() != null) {
                    has = true;
                    Punish punish = CloudService.getCloudService().getManagerService().getPunishManager().getByKey(cloudPlayer.getAbuses().getMute().getReason());
                    String reason = cloudPlayer.getAbuses().getMute().getReason();

                    if (punish != null) {
                        reason = sender.getLanguageMessage(reason.toUpperCase());
                    }

                    CloudPlayer by = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(cloudPlayer.getAbuses().getMute().getAuthor());
                    sender.sendMessage("command-baninfo-format", "MUTE", reason, (by.getRank().getRankColor()+by.getName()), cloudPlayer.getAbuses().getMute().getComment(), GoAPI.getTimeManager().getRemainingTimeString(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), sender.getLocale(), cloudPlayer.getAbuses().getMute().getLength()));
                }

                if (!has) {
                    sender.sendMessage("command-baninfo-no-punishments", (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()));
                }
            } else {
                sender.sendMessage("user-not-exist", sender.getLanguageMessage("team-prefix"));
            }
        } else {
            sender.sendMessage("command-banlog-usage");
        }
    }
}
