package net.vicemice.modules.commands.commands.admin;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.GoAPI;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.hector.network.modules.Module;
import net.vicemice.hector.network.slave.Slave;
import net.vicemice.hector.network.proxy.Proxy;
import net.vicemice.hector.packets.types.PacketHolder;
import net.vicemice.hector.packets.types.UpdateLocalePacket;
import net.vicemice.hector.packets.types.player.ComponentMessagePacket;
import net.vicemice.hector.packets.types.proxy.MaintenancePacket;
import net.vicemice.hector.packets.types.slave.SaveLogPacket;
import net.vicemice.hector.packets.types.slave.SlaveTemplatePacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.types.ServerState;
import net.vicemice.hector.utils.MessageGroup;
import net.vicemice.hector.utils.ServerType;
import net.vicemice.hector.packets.PacketType;
import net.vicemice.hector.utils.math.MathUtils;
import net.vicemice.hector.utils.players.utils.DataUpdateType;
import net.vicemice.hector.utils.players.utils.Pagination;
import net.vicemice.hector.utils.slave.TemplateData;
import java.io.File;
import java.util.*;

public class CloudCommand extends Command {

    private static final List<UUID> whitelistedPlayers = Arrays.asList(UUID.fromString("c80be485-b4f9-46a7-afa7-947a50f76739"), UUID.fromString("bed7ab0f-5a69-4f69-8104-d277c22f2ced"));

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (!sender.getRank().isAdmin() && !(whitelistedPlayers.contains(sender.getUniqueId()))) {
            sender.sendMessage("command-no-permission");
            return;
        }

        if (args.length == 0) {
            MessageGroup helpGroup = new MessageGroup(sender);
            helpGroup.addMessage(sender.getLanguageMessage("cloud-prefix")+" §c§lHector §7Managment §8(§7Page 1/3§8)");
            helpGroup.addMessage("§c/cloud info §8- §7Informations from the Hector");
            helpGroup.addMessage("§c/cloud update <Template> §8- §7Zip a template and upload it to our Slave-Host");
            helpGroup.addMessage("§c/cloud restart <Template> §8- §7Restart a specificaly Template");
            helpGroup.addMessage("§c/cloud start <Template|all> §8- §7Start a specificaly Template");
            helpGroup.addMessage("§c/cloud stop <Template> §8- §7Stop a specificaly Template");
            helpGroup.addMessage("§7To other Pages: §c/cloud [2/3]");
            helpGroup.send();
            return;
        }

        switch (args[0].toLowerCase()) {
            default:
                MessageGroup helpGroup = new MessageGroup(sender);
                helpGroup.addMessage(sender.getLanguageMessage("cloud-prefix") + " §c§lHector §7Managment §8(§7Page 1/3§8)");
                helpGroup.addMessage("§c/cloud info §8- §7Informations of Hector");
                helpGroup.addMessage("§c/cloud update <Template> §8- §7Zip a template and upload it to our Slave-Host");
                helpGroup.addMessage("§c/cloud restart <Template> §8- §7Restart a specificaly Template");
                helpGroup.addMessage("§c/cloud start <Template|all> §8- §7Start a specificaly Template");
                helpGroup.addMessage("§c/cloud stop <Template> §8- §7Stop a specificaly Template");
                helpGroup.addMessage("§c/cloud kill <Server> §8- §7Force a Server to stop");
                helpGroup.addMessage("§7To other Pages: §c/cloud [2/3]");
                helpGroup.send();
                break;

            case "2":
                MessageGroup helpGroup2 = new MessageGroup(sender);
                helpGroup2.addMessage(sender.getLanguageMessage("cloud-prefix") + " §c§lHector §7Managment §8(§7Page 2/3§8)");
                helpGroup2.addMessage("§c/cloud reload <Option> §8- §7Reload a Option");
                helpGroup2.addMessage("§c/cloud stats §8- §7Get Statistics from SkyNet");
                helpGroup2.addMessage("§c/cloud maintenance <On|Off|KickAll> §8- §7Maintenance options");
                helpGroup2.addMessage("§c/cloud sinfo <Server/Slave/Proxy> <Name> §8- §7Get Information from a Server");
                helpGroup2.addMessage("§c/cloud log <Server> §8- §7Upload a Log");
                helpGroup2.addMessage("§c/cloud check §8- §7Check all Templates");
                helpGroup2.addMessage("§7To other Pages: §c/cloud [3/3]");
                helpGroup2.send();
                break;

            case "3":
                MessageGroup helpGroup3 = new MessageGroup(sender);
                helpGroup3.addMessage(sender.getLanguageMessage("cloud-prefix") + " §c§lHector §7Managment §8(§7Page 3/3§8)");
                helpGroup3.addMessage("§c/cloud log <Server> §8- §7Upload a Log");
                helpGroup3.addMessage("§c/cloud check §8- §7Check all Templates");
                helpGroup3.addMessage("§c/cloud ram §8- §7Check the available Ram");
                helpGroup3.addMessage("§c/cloud blacklist §8- §7Blacklist System");
                helpGroup3.addMessage("§c/cloud whitelist §8- §7Whitelist System");
                helpGroup3.addMessage("§c/cloud nick §8- §7Nick System");
                helpGroup3.addMessage("§7To other Pages: §c/cloud [1/3]");
                helpGroup3.send();
                break;

            case "info":
                CloudPlayer developer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(UUID.fromString("c80be485-b4f9-46a7-afa7-947a50f76739"));

                MessageGroup infoGroup = new MessageGroup(sender);
                infoGroup.addMessage(sender.getLanguageMessage("cloud-prefix") + " §cInformations of §cHector");
                infoGroup.addMessage("§7Developer: §c" + developer.getRank().getRankColor() + developer.getName());
                infoGroup.addMessage("§7Version (Official): §c" + CloudService.getVersion());
                infoGroup.addMessage("§7Version-Channel: §6§lBETA §bVERSION");
                infoGroup.addMessage("§7Last Build: §c29.04.2020 (#20D9)");
                if (!CloudService.getCloudService().getManagerService().getModuleManager().getModules().isEmpty()) {
                    infoGroup.addMessage("§7Active Modules: (Name | Version | By)");
                    for (Module module : CloudService.getCloudService().getManagerService().getModuleManager().getModules())
                        infoGroup.addMessage("§a" + module.getName() + " §7v§e" + module.getVersion() + " §7by §b" + module.getAuthor());
                } else {
                    infoGroup.addMessage("§cCurrently are no module loaded for Hector.");
                }
                infoGroup.send();
                break;

            case "tm":
                if (args.length > 1) {
                    StringBuilder sb = new StringBuilder();
                    int i = 1;
                    while (i < args.length) {
                        sb.append(args[i]).append(" ");
                        ++i;
                    }

                    sender.sendMessages(new String[] { sb.toString() });
                } else {
                    sender.sendRawMessage("§cNo!");
                }
                break;

            case "gameid":
                if (sender.getServer().isCloudServer()) {
                    if (sender.getServer().getGameID() != null) {

                        ArrayList<String[]> messageData = new ArrayList<>();
                        messageData.add(new String[]{"§aClick to copy the game id", "true", "suggest_command", sender.getServer().getGameID()});
                        ComponentMessagePacket componentMessagePacket = new ComponentMessagePacket(sender.getName(), messageData);

                        sender.getProxy().sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.COMPONENT_MESSAGE_PACKET, componentMessagePacket));
                    } else {
                        sender.sendRawMessage("§cNo gameid could be found.");
                    }
                } else {
                    sender.sendRawMessage("§cThis server can't have a gameid.");
                }
                break;

            case "hastosagreed":
                sender.sendRawMessage("Accepted? "+(sender.getSetting("accept-tos", Boolean.class)));
                break;

            case "seeallstats":
                sender.sendRawMessage("Key | Value");
                for (String s : sender.getStats().getGlobalStats().keySet()) {
                    sender.sendRawMessage(s+" | "+sender.getStats().getGlobalStatsValue(s));
                }
                break;

            case "resetdailyreward":
                CloudService.getCloudService().getManagerService().getStatsManager().setDailyStat(sender, "member_coin_reward", 0);
                CloudService.getCloudService().getManagerService().getStatsManager().setDailyStat(sender, "premium_coin_reward", 0);
                CloudService.getCloudService().getManagerService().getStatsManager().setDailyStat(sender, "daily_chest", 0);
                CloudService.getCloudService().getManagerService().getStatsManager().setDailyStat(sender, "member_lottery_reward", 0);
                CloudService.getCloudService().getManagerService().getStatsManager().setDailyStat(sender, "premium_lottery_reward", 0);
                sender.sendRawMessage("§aDone");
                break;

            case "saveinfo":
                sender.saveInfo();
                sender.sendRawMessage("Saved Info's in 'players2'");
                break;

            case "saveall":
                for (CloudPlayer cloudPlayer : CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().values()) {
                    cloudPlayer.save(false);
                }
                break;

            case "correct":
                if (args[1].equalsIgnoreCase("coins")) {
                    long coins = 0L;
                    try {
                        coins = Long.parseLong(args[3]);

                        if (args[2].equalsIgnoreCase("*")) {
                            for (CloudPlayer cloudPlayer : CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().values()) {
                                cloudPlayer.setCoins(coins);
                                cloudPlayer.sendMessage("coins-corrected", coins);
                            }

                            sender.sendRawMessage("§c[Correction] Successfully correct the coins of §6" + CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().size() + " §cto §6" + coins + " Coins");
                        } else {
                            String name = args[2];
                            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(name);
                            if (cloudPlayer != null) {
                                cloudPlayer.setCoins(coins);
                                sender.sendRawMessage("§c[Correction] Successfully correct the coins from " + cloudPlayer.getRank().getRankColor() + cloudPlayer.getName() + " §cto §6" + coins + " Coins");
                                cloudPlayer.sendMessage("coins-corrected", coins);
                            } else {
                                sender.sendRawMessage("§c[Correction] Player not found");
                            }
                        }
                    } catch (NumberFormatException ex) {
                        sender.sendRawMessage("§c[Correction] You must enter a number as argument numero tres");
                    }
                } else if (args[1].equalsIgnoreCase("playtime")) {
                    long playtime = 0L;
                    try {
                        playtime = Long.parseLong(args[3]);

                        String name = args[2];
                        CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(name);
                        if (cloudPlayer != null) {
                            cloudPlayer.setPlayTime(playtime);
                            sender.sendRawMessage("§c[Correction] Successfully correct the onlinetime from " + cloudPlayer.getRank().getRankColor() + cloudPlayer.getName() + " §cto §6" + playtime + " Played Minutes");
                            cloudPlayer.sendMessage("coins-corrected", playtime);
                        } else {
                            sender.sendRawMessage("§c[Correction] Player not found");
                        }
                    } catch (NumberFormatException ex) {
                        sender.sendRawMessage("§c[Correction] You must enter a number as argument numero tres");
                    }
                } else {
                    sender.sendRawMessage("§c[Correction] Unknown Command");
                }
                break;

            case "add":
                if (args[1].equalsIgnoreCase("coins")) {
                    long coins = 0L;
                    try {
                        coins = Long.parseLong(args[3]);

                        if (args[2].equalsIgnoreCase("*")) {
                            for (CloudPlayer cloudPlayer : CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().values()) {
                                cloudPlayer.addCoins(coins);
                                cloudPlayer.sendMessage("coins-corrected", coins);
                            }

                            sender.sendRawMessage("§c[Correction] Successfully add §6" + coins + " Coins §cfrom §6" + CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().size());
                        } else {
                            String name = args[2];
                            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(name);
                            if (cloudPlayer != null) {
                                cloudPlayer.addCoins(coins);
                                sender.sendRawMessage("§c[Correction] Successfully add §6" + coins + " Coins §cfrom §6" + cloudPlayer.getRank().getRankColor() + cloudPlayer.getName());
                                cloudPlayer.sendMessage("coins-corrected", coins);
                            } else {
                                sender.sendRawMessage("§c[Correction] Player not found");
                            }
                        }
                    } catch (NumberFormatException ex) {
                        sender.sendRawMessage("§c[Correction] You must enter a number as argument numero tres");
                    }
                } else {
                    sender.sendRawMessage("§c[Correction] Unknown Command");
                }
                break;

            case "remove":
                if (args[1].equalsIgnoreCase("coins")) {
                    long coins = 0L;
                    try {
                        coins = Long.parseLong(args[3]);

                        if (args[2].equalsIgnoreCase("*")) {
                            for (CloudPlayer cloudPlayer : CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().values()) {
                                cloudPlayer.removeCoins(coins);
                                cloudPlayer.sendMessage("coins-corrected", coins);
                            }

                            sender.sendRawMessage("§c[Correction] Successfully remove §6" + coins + " Coins §cfrom §6" + CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().size());
                        } else {
                            String name = args[2];
                            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(name);
                            if (cloudPlayer != null) {
                                cloudPlayer.removeCoins(coins);
                                sender.sendRawMessage("§c[Correction] Successfully remove §6" + coins + " Coins §cfrom §6" + cloudPlayer.getRank().getRankColor() + cloudPlayer.getName());
                                cloudPlayer.sendMessage("coins-corrected", coins);
                            } else {
                                sender.sendRawMessage("§c[Correction] Player not found");
                            }
                        }
                    } catch (NumberFormatException ex) {
                        sender.sendRawMessage("§c[Correction] You must enter a number as argument numero tres");
                    }
                } else {
                    sender.sendRawMessage("§c[Correction] Unknown Command");
                }
                break;

            case "sendfromserver":
                if (args.length == 3) {
                    Server server = CloudService.getCloudService().getManagerService().getServerManager().getServerByName(args[1]);
                    if (server != null) {
                        Server target = CloudService.getCloudService().getManagerService().getServerManager().getServerByName(args[2]);
                        if (target != null) {
                            CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().forEach(uniqueId -> {
                                CloudPlayer player = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uniqueId);
                                if (player == null) return;
                                if (player.getServer().getServerID().equalsIgnoreCase(server.getServerID())) return;
                                player.connectToServer(target.getName());
                            });
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §aThe players has been sent to the target server");
                        } else {
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cThe target server could not be found");
                        }
                    } else {
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cThe server could not be found");
                    }
                } else {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud sendfromserver <Server> <Target Server>");
                }
                break;

            case "debug":
                if (CloudService.getCloudService().isDevMode()) {
                    sender.sendRawMessage("§7Debug-Mode: §c§lOFF");
                    CloudService.getCloudService().setDevMode(false);
                } else {
                    sender.sendRawMessage("§7Debug-Mode: §a§lON");
                    CloudService.getCloudService().setDevMode(true);
                }
                break;

            case "gettime":
                if (args.length == 2) {
                    try {
                        sender.sendRawMessage("§7Time: §e"+GoAPI.getTimeManager().getRemainingTimeString(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), sender.getLocale(), Long.valueOf(args[1])));
                    } catch (NumberFormatException ex) {
                        sender.sendRawMessage("§cYou must give a number");
                    }
                } else {
                    sender.sendRawMessage("§cUse /cloud gettime <Time>");
                }
                break;

            case "removeplayer":
                if (args.length == 2) {
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
                    if (cloudPlayer != null) {
                        if (cloudPlayer.isOnline() || CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().contains(cloudPlayer.getUniqueId())) {
                            cloudPlayer.kickPlayer("Reconnect!");
                            CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().remove(cloudPlayer.getUniqueId());
                            cloudPlayer.setProxy(null);
                            sender.sendRawMessage("§aDone");
                        } else {
                            sender.sendRawMessage("§cNot online.");
                        }
                    } else {
                        sender.sendRawMessage("§cNot exists.");
                    }
                } else {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud removeplayer <Name>");
                }
                break;

            case "removeserver":
                if (args.length == 2) {
                    Server server = CloudService.getCloudService().getManagerService().getServerManager().getServerByName(args[1]);
                    if (server != null) {
                        server.remove();
                        sender.sendRawMessage("§aDone!");
                    } else {
                        sender.sendRawMessage("§cNot exists.");
                    }
                } else {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud removeserver <Name>");
                }
                break;

            case "allonline":
                MessageGroup onlineGroup = new MessageGroup(sender);
                onlineGroup.addMessage("Size: " + CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().size());
                for (UUID uuid : CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers()) {
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uuid);
                    onlineGroup.addMessage("- " + cloudPlayer.getRank().getRankColor() + cloudPlayer.getName());
                }
                onlineGroup.send();
                break;

            case "removeonline":
                if (args.length == 2) {
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
                    if (cloudPlayer != null) {
                        if (CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().contains(cloudPlayer.getUniqueId())) {
                            cloudPlayer.kickPlayer("Reconnect!");
                            CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().remove(cloudPlayer.getUniqueId());
                            cloudPlayer.setProxy(null);
                            sender.sendRawMessage("§aDone");
                        } else {
                            sender.sendRawMessage("§cNot online.");
                        }
                    } else {
                        sender.sendRawMessage("§cNot exists.");
                    }
                } else {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud removeonline <Name>");
                }
                break;

            case "blacklist":
                if (args.length >= 2) {
                    if (args[1].equals("list")) {
                        int page = 1;
                        if (args.length == 3) {
                            page = (MathUtils.isInt(args[2]) ? Integer.parseInt(args[2]) : 1);
                        }

                        Pagination<UUID> pagination = new Pagination<>(CloudService.getCloudService().getManagerService().getBlacklistManager().getPlayers(), 9);
                        if (pagination.getElementsFor(page).isEmpty()) {
                            sender.sendRawMessage("§cThe page does not exist.");
                            return;
                        }

                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §3Blacklisted Players &8(&ePage "+page+"§7/§e"+pagination.getPages()+"§8)");
                        pagination.printPage(page, e -> {
                            String string = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getPlayerName(e);
                            if (string == null) return;
                            sender.sendRawMessage(" §7- §6"+string);
                        });
                        return;
                    } else if (args[1].equals("add")) {
                        if (args.length != 3) {
                            sender.sendRawMessage("§cYou must give a name.");
                            return;
                        }

                        UUID uniqueId = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getPlayerUniqueId(args[2]);

                        if (uniqueId == null) {
                            sender.sendRawMessage("§cThe name is invalid.");
                            return;
                        }

                        if (CloudService.getCloudService().getManagerService().getBlacklistManager().getPlayers().contains(uniqueId)) {
                            sender.sendRawMessage("§cThe name exist already.");
                            return;
                        }

                        CloudService.getCloudService().getManagerService().getBlacklistManager().addPlayer(uniqueId);
                        sender.sendRawMessage("§aThe player was added");

                        return;
                    } else if (args[1].equals("remove")) {
                        if (args.length != 3) {
                            sender.sendRawMessage("§cYou must give a name.");
                            return;
                        }

                        UUID uniqueId = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getPlayerUniqueId(args[2]);

                        if (uniqueId == null) {
                            sender.sendRawMessage("§cThe name is invalid.");
                            return;
                        }

                        if (!CloudService.getCloudService().getManagerService().getBlacklistManager().getPlayers().contains(uniqueId)) {
                            sender.sendRawMessage("§cThe name could not be found.");
                            return;
                        }

                        CloudService.getCloudService().getManagerService().getBlacklistManager().removePlayer(uniqueId);
                        sender.sendRawMessage("§aThe player was removed");

                        return;
                    }
                }

                MessageGroup blacklistHelp = new MessageGroup(sender);
                blacklistHelp.addMessage(sender.getLanguageMessage("cloud-prefix")+"  §3Blacklist §7Managment §8(§7Page 1/1§8)");
                blacklistHelp.addMessage("§c/cloud blacklist list [Page] §8- §7List all Blacklisted Players");
                blacklistHelp.addMessage("§c/cloud blacklist add <Name> §8- §7Add a Player");
                blacklistHelp.addMessage("§c/cloud blacklist remove <Template> §8- §7Remove a Player");
                blacklistHelp.send();
                break;

            case "whitelist":
                if (args.length >= 2) {
                    if (args[1].equals("list")) {
                        int page = 1;
                        if (args.length == 3) {
                            page = (MathUtils.isInt(args[2]) ? Integer.parseInt(args[2]) : 1);
                        }

                        Pagination<UUID> pagination = new Pagination<>(CloudService.getCloudService().getManagerService().getWhitelistManager().getPlayers(), 9);
                        if (pagination.getElementsFor(page).isEmpty()) {
                            sender.sendRawMessage("§cThe page does not exist.");
                            return;
                        }

                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §3Whitelisted Players &8(&ePage "+page+"§7/§e"+pagination.getPages()+"§8)");
                        pagination.printPage(page, e -> {
                            String string = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getPlayerName(e);
                            if (string == null) return;
                            sender.sendRawMessage(" §7- §6"+string);
                        });
                        return;
                    } else if (args[1].equals("add")) {
                        if (args.length != 3) {
                            sender.sendRawMessage("§cYou must give a name.");
                            return;
                        }

                        UUID uniqueId = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getPlayerUniqueId(args[2]);

                        if (uniqueId == null) {
                            sender.sendRawMessage("§cThe name is invalid.");
                            return;
                        }

                        if (CloudService.getCloudService().getManagerService().getWhitelistManager().getPlayers().contains(uniqueId)) {
                            sender.sendRawMessage("§cThe name exist already.");
                            return;
                        }

                        CloudService.getCloudService().getManagerService().getWhitelistManager().addPlayer(uniqueId);
                        sender.sendRawMessage("§aThe player was added");

                        return;
                    } else if (args[1].equals("remove")) {
                        if (args.length != 3) {
                            sender.sendRawMessage("§cYou must give a name.");
                            return;
                        }

                        UUID uniqueId = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getPlayerUniqueId(args[2]);

                        if (uniqueId == null) {
                            sender.sendRawMessage("§cThe name is invalid.");
                            return;
                        }

                        if (!CloudService.getCloudService().getManagerService().getWhitelistManager().getPlayers().contains(uniqueId)) {
                            sender.sendRawMessage("§cThe name could not be found.");
                            return;
                        }

                        CloudService.getCloudService().getManagerService().getWhitelistManager().removePlayer(uniqueId);
                        sender.sendRawMessage("§aThe player was removed");

                        return;
                    }
                }

                MessageGroup whitelistHelp = new MessageGroup(sender);
                whitelistHelp.addMessage(sender.getLanguageMessage("cloud-prefix")+"  §3Whitelist §7Managment §8(§7Page 1/1§8)");
                whitelistHelp.addMessage("§c/cloud whitelist list [Page] §8- §7List all Whitelisted Players");
                whitelistHelp.addMessage("§c/cloud whitelist add <Name> §8- §7Add a Player");
                whitelistHelp.addMessage("§c/cloud whitelist remove <Template> §8- §7Remove a Player");
                whitelistHelp.send();
                break;

            case "nick":
                if (args.length >= 2) {
                    if (args[1].equals("valid")) {
                        if (CloudService.getCloudService().getManagerService().getNickNameManager().isAvailable()) {
                            sender.sendRawMessage("§6Validation can take up to an hour, please wait...");
                            CloudService.getCloudService().getManagerService().getNickNameManager().checkValid(sender);
                        } else {
                            sender.sendRawMessage("§cThe nicknames will be already validated, please wait until it is finished.");
                        }
                        return;
                    } else if (args[1].equals("list")) {
                        int page = 1;
                        if (args.length == 3) {
                            page = (MathUtils.isInt(args[2]) ? Integer.parseInt(args[2]) : 1);
                        }

                        Pagination<String> pagination = new Pagination<>(CloudService.getCloudService().getManagerService().getNickNameManager().getNickNames(), 9);
                        if (pagination.getElementsFor(page).isEmpty()) {
                            sender.sendRawMessage("§cThe page does not exist.");
                            return;
                        }

                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §5Nicknames &8(&ePage "+page+"§7/§e"+pagination.getPages()+"§8)");
                        pagination.printPage(page, e -> {
                            sender.sendRawMessage(" §7- §6"+e);
                        });
                        return;
                    } else if (args[1].equals("remove")) {
                        if (args.length != 3) {
                            sender.sendRawMessage("§cYou must give a name.");
                            return;
                        }
                        if (!CloudService.getCloudService().getManagerService().getNickNameManager().getNickNames().contains(args[2])) {
                            sender.sendRawMessage("§cThe name could not be found.");
                            return;
                        }

                        if (CloudService.getCloudService().getManagerService().getNickNameManager().remove(args[2])) {
                            sender.sendRawMessage("§aThe nickname was removed!");
                        } else {
                            sender.sendRawMessage("§cThe nickname does not exist in the database.");
                        }
                        return;
                    } else if (args[1].equals("add")) {
                        if (args.length != 3) {
                            sender.sendRawMessage("§cYou must give a name.");
                            return;
                        }
                        if (CloudService.getCloudService().getManagerService().getNickNameManager().getNickNames().contains(args[2])) {
                            sender.sendRawMessage("§cThe name exist already.");
                            return;
                        }

                        if (CloudService.getCloudService().getManagerService().getNickNameManager().addAndValidate(args[2])) {
                            sender.sendRawMessage("§aThe nickname has been added!");
                        } else {
                            sender.sendRawMessage("§cThe nickname could not added, the name has an invalid uuid or exist already in the database.");
                        }
                        return;
                    }
                }

                MessageGroup nickHelp = new MessageGroup(sender);
                nickHelp.addMessage(sender.getLanguageMessage("cloud-prefix")+"  §5§lNick §7Managment §8(§7Page 1/1§8)");
                nickHelp.addMessage("§c/cloud nick valid §8- §7Validate all Nicknames");
                nickHelp.addMessage("§c/cloud nick list [Page] §8- §7List all Nicknames");
                nickHelp.addMessage("§c/cloud nick add <Name> §8- §7Add a Nickname");
                nickHelp.addMessage("§c/cloud nick remove <Template> §8- §7Remove a Nickname");
                nickHelp.send();
                break;

            case "openedreports":
                sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7There are §6" + CloudService.getCloudService().getManagerService().getReportManager().getEntries().size() + " Reports §7open");
                break;

            case "privateservertest":
                if (args.length == 2) {
                    TemplateData template = CloudService.getCloudService().getManagerService().getServerManager().getTemplateByName(args[1]);

                    if (template != null) {
                        CloudService.getCloudService().getManagerService().getServerManager().createPrivateServer(sender, template);
                    } else {
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+"  §cThe specified template was not found.");
                    }
                } else { 
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud privateservertest <Template>");
                }
                break;

            case "statssigns":
                sender.sendRawMessage("§aDone!");
                CloudService.getCloudService().getTerminal().writeMessage(CloudService.getCloudService().getManagerService().getSignStatsManager().getGlobalSignPacket().getSignDatas().get(0)[2] + CloudService.getCloudService().getManagerService().getSignStatsManager().getGlobalSignPacket().getSignDatas().get(0)[3]);
                break;

            case "update":
                if (args.length == 2) {
                    if (args[1].equalsIgnoreCase("all")) {
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §6Try to update all Templates...");
                        Iterator<TemplateData> serverTemplateIterator = CloudService.getCloudService().getManagerService().getServerManager().getTemplates().iterator();
                        while (serverTemplateIterator.hasNext()) {
                            TemplateData serverTemplate = serverTemplateIterator.next();
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7The Template §c" + serverTemplate.getName() + " §7will be §6geupdated§7...");
                            try {
                                File delete = new File("slave/templates/" + serverTemplate.getName() + ".zip");
                                if (delete.exists()) {
                                    delete.delete();
                                }
                                String[] zipCommand = new String[]{"zip", serverTemplate.getName() + ".zip", "-r", serverTemplate.getName()};
                                ProcessBuilder processBuilder = new ProcessBuilder(zipCommand);
                                processBuilder.directory(new File("templates"));
                                Process zip = processBuilder.start();
                                zip.waitFor();
                                sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7The Template §c" + serverTemplate.getName() + " §7was §6updated§7!");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        new Thread(() -> {
                            PacketHolder packetHolder = GoAPI.getPacketHelper().preparePacket(PacketType.SLAVE_TEMPLATE_PACKET, new SlaveTemplatePacket(CloudService.getCloudService().getManagerService().getServerManager().getTemplates(), true));
                            Iterator<Slave> slaveIterator = CloudService.getCloudService().getManagerService().getSlaveManager().getSlaveList().iterator();
                            while (slaveIterator.hasNext()) {
                                Slave slave = slaveIterator.next();
                                slave.sendPacket(packetHolder);
                            }
                        }).start();

                    } else {
                        TemplateData serverTemplate = CloudService.getCloudService().getManagerService().getServerManager().getTemplateByName(args[1]);
                        if (serverTemplate != null) {
                            try {
                                sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7The Template §c" + serverTemplate.getName() + " §7will be §6geupdated§7...");
                                File delete = new File("slave/templates/" + serverTemplate.getName() + ".zip");
                                if (delete.exists()) {
                                    delete.delete();
                                }
                                String[] zipCommand = new String[]{"zip", serverTemplate.getName() + ".zip", "-r", serverTemplate.getName()};
                                ProcessBuilder processBuilder = new ProcessBuilder(zipCommand);
                                processBuilder.directory(new File("slave/templates"));
                                Process zip = processBuilder.start();
                                zip.waitFor();
                                sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7The Template §c" + serverTemplate.getName() + " §7was §6updated§7!");
                                new Thread(() -> {
                                    PacketHolder packetHolder = GoAPI.getPacketHelper().preparePacket(PacketType.SLAVE_TEMPLATE_PACKET, new SlaveTemplatePacket(CloudService.getCloudService().getManagerService().getServerManager().getTemplates(), true));
                                    Iterator<Slave> slaveIterator = CloudService.getCloudService().getManagerService().getSlaveManager().getSlaveList().iterator();
                                    while (slaveIterator.hasNext()) {
                                        Slave slave = slaveIterator.next();
                                        slave.sendPacket(packetHolder);
                                    }
                                }).start();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cThis Template dont exist");
                        }
                    }
                } else {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud update <TemplateName|All>");
                }
                break;
            case "zip":
                if (args.length == 2) {
                    if (args[1].equalsIgnoreCase("all")) {
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §aTry to zip all Templates");
                        Iterator<TemplateData> serverTemplateIterator = CloudService.getCloudService().getManagerService().getServerManager().getTemplates().iterator();
                        while (serverTemplateIterator.hasNext()) {
                            TemplateData serverTemplate = serverTemplateIterator.next();
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7The template §c" + serverTemplate.getName() + " §7will be §6zipped§7!");
                            try {
                                File delete = new File("/opt/network/pages/acp/K3q6P5vzMWG8Zz7dWgwvXnFN3cPy2yqBVSC7HD8uU5BRJRvwgJzc6vPVCFgV8rjd/" + serverTemplate.getName() + ".zip");
                                if (delete.exists()) {
                                    delete.delete();
                                }

                                String[] zipCommand = new String[]{"zip", serverTemplate.getName() + ".zip", "-r", serverTemplate.getName()};
                                ProcessBuilder processBuilder = new ProcessBuilder(zipCommand);
                                processBuilder.directory(new File("/opt/network/pages/acp/K3q6P5vzMWG8Zz7dWgwvXnFN3cPy2yqBVSC7HD8uU5BRJRvwgJzc6vPVCFgV8rjd"));
                                Process zip = processBuilder.start();
                                zip.waitFor();
                                sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7The template §c" + serverTemplate.getName() + " §7was §azipped§7!");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        PacketHolder packetHolder = GoAPI.getPacketHelper().preparePacket(PacketType.SLAVE_TEMPLATE_PACKET, new SlaveTemplatePacket(CloudService.getCloudService().getManagerService().getServerManager().getTemplates(), true));
                        Iterator<Slave> slaveIterator = CloudService.getCloudService().getManagerService().getSlaveManager().getSlaveList().iterator();
                        while (slaveIterator.hasNext()) {
                            Slave slave = slaveIterator.next();
                            slave.sendPacket(packetHolder);
                        }
                    } else if (CloudService.getCloudService().getManagerService().getServerManager().getTemplateByName(args[1]) != null) {
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §aTry to zip all Templates");

                        TemplateData serverTemplate = CloudService.getCloudService().getManagerService().getServerManager().getTemplateByName(args[1]);
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7The template §c" + serverTemplate.getName() + " §7will be §6zipped§7!");
                        try {
                            File delete = new File("/opt/network/pages/acp/K3q6P5vzMWG8Zz7dWgwvXnFN3cPy2yqBVSC7HD8uU5BRJRvwgJzc6vPVCFgV8rjd/" + serverTemplate.getName() + ".zip");
                            if (delete.exists()) {
                                delete.delete();
                            }

                            String[] zipCommand = new String[]{"zip", serverTemplate.getName() + ".zip", "-r", serverTemplate.getName()};
                            ProcessBuilder processBuilder = new ProcessBuilder(zipCommand);
                            processBuilder.directory(new File("/opt/network/pages/acp/K3q6P5vzMWG8Zz7dWgwvXnFN3cPy2yqBVSC7HD8uU5BRJRvwgJzc6vPVCFgV8rjd"));
                            Process zip = processBuilder.start();
                            zip.waitFor();
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7The template §c" + serverTemplate.getName() + " §7was §azipped§7!");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        PacketHolder packetHolder = GoAPI.getPacketHelper().preparePacket(PacketType.SLAVE_TEMPLATE_PACKET, new SlaveTemplatePacket(CloudService.getCloudService().getManagerService().getServerManager().getTemplates(), true));
                        Iterator<Slave> slaveIterator = CloudService.getCloudService().getManagerService().getSlaveManager().getSlaveList().iterator();
                        while (slaveIterator.hasNext()) {
                            Slave slave = slaveIterator.next();
                            slave.sendPacket(packetHolder);
                        }
                    } else {
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+"§cThe template could't be found");
                    }
                } else {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud zip <Template|all>");
                }
                break;
            case "restart":
                if (args.length == 2) {
                    String name = args[1];
                    if (name.equalsIgnoreCase("all")) {
                        MessageGroup messageGroup = new MessageGroup(sender);
                        Iterator<TemplateData> serverTemplateIterator = CloudService.getCloudService().getManagerService().getServerManager().getTemplates().iterator();
                        while (serverTemplateIterator.hasNext()) {
                            TemplateData serverTemplate = serverTemplateIterator.next();
                            serverTemplate.setActive(false);

                            int stoppedCount = 0;
                            Iterator<Server> serverIterator = CloudService.getCloudService().getManagerService().getServerManager().getServers().iterator();
                            while (serverIterator.hasNext()) {
                                Server server = serverIterator.next();
                                if (server.isCloudServer()) {
                                    if (server.getServerTemplate().getName().equalsIgnoreCase(serverTemplate.getName())) {
                                        stoppedCount++;
                                        messageGroup.addMessage(sender.getLanguageMessage("cloud-prefix") + " §7The server §c" + server.getName() + " §7will be §4§lstopped§7!");
                                        server.remove();
                                    }
                                }
                            }
                            messageGroup.addMessage(sender.getLanguageMessage("cloud-prefix") + " §c" + stoppedCount + " servers §7were stopped!");

                            serverTemplate.setActive(true);
                            messageGroup.addMessage(sender.getLanguageMessage("cloud-prefix") + " §7The template §c" + serverTemplate.getName() + " §7has been §astarted§7!");
                        }
                        CloudService.getCloudService().getManagerService().getServerManager().checkServers();
                        messageGroup.send();
                    } else if (CloudService.getCloudService().getManagerService().getServerManager().getTemplateByName(name) != null) {
                        MessageGroup messageGroup = new MessageGroup(sender);
                        TemplateData serverTemplate = CloudService.getCloudService().getManagerService().getServerManager().getTemplateByName(name);
                        serverTemplate.setActive(false);

                        int stoppedCount = 0;
                        Iterator<Server> serverIterator = CloudService.getCloudService().getManagerService().getServerManager().getServers().iterator();
                        while (serverIterator.hasNext()) {
                            Server server = serverIterator.next();
                            if (server.isCloudServer()) {
                                if (server.getServerTemplate().getName().equalsIgnoreCase(serverTemplate.getName())) {
                                    stoppedCount++;
                                    messageGroup.addMessage(sender.getLanguageMessage("cloud-prefix")+" §7The server §c" + server.getName() + " §7will be §4§lstopped§7!");
                                    server.remove();
                                }
                            }
                        }
                        messageGroup.addMessage(sender.getLanguageMessage("cloud-prefix")+" §c" + stoppedCount + " servers §7were stopped!");

                        serverTemplate.setActive(true);
                        messageGroup.addMessage(sender.getLanguageMessage("cloud-prefix")+" §7The template §c" + serverTemplate.getName() + " §7has been §astarted§7!");
                        CloudService.getCloudService().getManagerService().getServerManager().checkServers();
                        messageGroup.send();
                    } else {

                    }
                } else {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud restart <Template|all>");
                }
                break;
            case "start":
                if (args.length == 2) {
                    String name = args[1];
                    if (name.equalsIgnoreCase("all")) {
                        Iterator<TemplateData> templateIterator = CloudService.getCloudService().getManagerService().getServerManager().getTemplates().iterator();
                        while (templateIterator.hasNext()) {
                            TemplateData serverTemplate = templateIterator.next();
                            serverTemplate.setActive(true);
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7The template §c" + serverTemplate.getName() + " §7has been §astarted§7!");
                            CloudService.getCloudService().getManagerService().getServerManager().checkServers();
                        }
                    } else {
                        TemplateData serverTemplate = CloudService.getCloudService().getManagerService().getServerManager().getTemplateByName(name);
                        serverTemplate.setActive(true);
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7The template §c" + serverTemplate.getName() + " §7has been §astarted§7!");
                        CloudService.getCloudService().getManagerService().getServerManager().checkServers();
                    }
                } else {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud start <Template|all>");
                }
                break;
            case "kill":
                if (args.length == 2) {
                    String name = args[1];
                    Server server = CloudService.getCloudService().getManagerService().getServerManager().getServerByName(name);
                    if (server != null && server.getServerState() != ServerState.REMOVE) {
                        CloudService.getCloudService().getManagerService().getServerManager().removeServer(name);
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7The server §c" + name + "§7 will be stopped. §8(§4§lFORCED§8)");
                    } else {
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cThe server was not found!");
                    }
                } else {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud kill <Name>");
                }
                break;
            case "stop":
                if (args.length == 2) {
                    String name = args[1];
                    TemplateData serverTemplate = CloudService.getCloudService().getManagerService().getServerManager().getTemplateByName(name);
                    if (serverTemplate != null) {
                        serverTemplate.setActive(false);
                        MessageGroup messageGroup = new MessageGroup(sender);
                        int stoppedCount = 0;
                        Iterator<Server> serverIterator = CloudService.getCloudService().getManagerService().getServerManager().getServers().iterator();
                        while (serverIterator.hasNext()) {
                            Server server = serverIterator.next();
                            if (server.isCloudServer()) {
                                if (server.getServerTemplate().getName().equalsIgnoreCase(serverTemplate.getName())) {
                                    stoppedCount++;
                                    messageGroup.addMessage(sender.getLanguageMessage("cloud-prefix")+" §7The server §c" + server.getName() + " §7will be §4§lstoppped§7!");
                                    server.remove();
                                }
                            }
                        }
                        messageGroup.addMessage(sender.getLanguageMessage("cloud-prefix")+" §c" + stoppedCount + " servers §7were stopped!");
                        messageGroup.send();
                    } else {
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cThe template could not be found!");
                    }
                } else {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud stop <Template>");
                }
                break;
            case "ram":
                int mb = 1024 * 1024;
                MessageGroup messageGroup = new MessageGroup(sender);
                messageGroup.addMessage("Total:" + Runtime.getRuntime().totalMemory() / mb);
                messageGroup.addMessage("Max: " + Runtime.getRuntime().maxMemory() / mb);
                messageGroup.addMessage("Free:" + Runtime.getRuntime().freeMemory() / mb);
                messageGroup.addMessage("Used:" + (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / mb);
                messageGroup.send();
                break;
            case "gc":
                System.gc();
                sender.sendRawMessage("Runned");
                break;
            case "reload":
                if (args.length != 2) {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud reload <Option>");
                    return;
                }
                switch (args[1].toLowerCase()) {
                    case "as":
                        new Thread(() -> {
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §6Trying to reload the AS Networks...");
                            CloudService.getCloudService().getManagerService().getAutonomousManager().init();
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7Successfully loaded §c" + CloudService.getCloudService().getManagerService().getAutonomousManager().getNetworks().size() + "§7 AS Networks!");
                        }).start();
                        break;
                    case "banid":
                        new Thread(() -> {
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §6Trying to reload the BanIDs...");
                            CloudService.getCloudService().getManagerService().getAbuseManager().loadBanIDs();
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7Successfully loaded §c" + CloudService.getCloudService().getManagerService().getAbuseManager().getBanId().size() + "§7 BanIDs!");
                        }).start();
                        break;
                    case "languages":
                        new Thread(() -> {
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §6Trying to reload the Locales...");
                            CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager().reloadLocales();
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7Successfully loaded §c" + CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager().getLocales().size() + "§7 languages!");

                            CloudService.getCloudService().getManagerService().getProxyManager().getProxies().forEach(e -> {
                                e.sendPacket(GoAPI.getPacketHelper().preparePacket(PacketType.UPDATE_LOCALE, new UpdateLocalePacket(System.currentTimeMillis())));
                            });
                        }).start();
                        break;
                    case "templates":
                        new Thread(() -> {
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §6Trying to reload the Templates...");
                            CloudService.getCloudService().getManagerService().getServerManager().reloadTemplates();
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7Successfully loaded §c" + CloudService.getCloudService().getManagerService().getServerManager().getTemplates().size() + "§7 Templates!");
                        }).start();
                        break;
                    case "permissions":
                        new Thread(() -> {
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §6Trying to reload the Permissions...");
                            CloudService.getCloudService().getManagerService().getPermissionManager().reloadPermissions();
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7Successfully loaded §c" + CloudService.getCloudService().getManagerService().getPermissionManager().getPermissionsMap().size() + "§7 Permissions!");
                        }).start();
                        break;
                    case "signs":
                        new Thread(() -> {
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §6Trying to reload the Signs...");
                            CloudService.getCloudService().getManagerService().getSignManager().reloadSigns();
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7Successfully loaded §c" + CloudService.getCloudService().getManagerService().getSignManager().getSigns().size() + "§7 Signs!");
                        }).start();
                        break;
                    case "settings":
                        new Thread(() -> {
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §6Trying to reload the Proxy-Settings...");
                            CloudService.getCloudService().getManagerService().getProxyManager().loadSettings();
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7Successfully loaded §c3 §7Proxy-Settings!");
                        }).start();
                        break;
                    case "games":
                        new Thread(() -> {
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §6Trying to reload the Games...");
                            CloudService.getCloudService().getManagerService().getGameManager().loadGames();
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7Successfully loaded §c" + CloudService.getCloudService().getManagerService().getGameManager().getGameList().size() + "§7 Games!");
                        }).start();
                        break;
                    case "statssigns":
                        new Thread(() -> {
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §6Trying to reload the Statistic Signs...");
                            CloudService.getCloudService().getManagerService().getSignStatsManager().loadSigns();
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7Successfully loaded §c" + CloudService.getCloudService().getManagerService().getSignStatsManager().getStatSigns().size() + "§7 Statistik Schilder!");
                        }).start();
                        break;
                    case "statics":
                        new Thread(() -> {
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §6Trying to reload the Static Servers...");
                            CloudService.getCloudService().getManagerService().getWhitelistManager().reload();
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7Successfully loaded §c" + CloudService.getCloudService().getManagerService().getWhitelistManager().getServers().size() + "§7 Servers!");
                        }).start();
                        break;
                    case "filter":
                        new Thread(() -> {
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §6Trying to reload the Filter...");
                            CloudService.getCloudService().getManagerService().getFilterManager().loadEntries();
                            CloudService.getCloudService().getManagerService().getFilterManager().sendToProxy();
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §7Successfully loaded §c" + CloudService.getCloudService().getManagerService().getFilterManager().getEntries().size() + "§7 Filtered Words!");
                        }).start();
                        break;
                    default:
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud reload <Option>");
                        break;
                }
                break;
            case "stats":
                MessageGroup statsGroup = new MessageGroup(sender);
                statsGroup.addMessage(sender.getLanguageMessage("cloud-prefix")+" §c§lHector §7Statistics:");
                statsGroup.addMessage("§cHector has been started on &6" + GoAPI.getTimeManager().getTime("dd.MM.yyyy - HH:mm:ss", CloudService.getCloudService().getStartTime()) + " §8(§6" + GoAPI.getTimeManager().getRemainingTimeString(CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager(), sender.getLocale(), CloudService.getCloudService().getStartTime()) + "§8)");
                statsGroup.addMessage("§7Unique Logins since startup: §c" + CloudService.getCloudService().getManagerService().getPlayerManager().getLoggedInUsers().size());
                statsGroup.addMessage("§7Logins total: §c" + CloudService.getCloudService().getManagerService().getPlayerManager().getJoinProcesses());
                statsGroup.addMessage("§7Registered Players: §c" + CloudService.getCloudService().getManagerService().getPlayerManager().getNameCloudPlayers().size());
                statsGroup.addMessage("§7Current banned Players: §c" + CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithBan().size());
                statsGroup.addMessage("§7Current muted Players: §c" + CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithMute().size());
                int normalPlayers = 0;
                for (UUID uuid : CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers()) {
                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uuid);
                    if (cloudPlayer == null) continue;
                    normalPlayers++;

                }
                statsGroup.addMessage("§7Players online: §c" + normalPlayers);
                statsGroup.send();
                break;
            case "maintenance":
                if (args.length == 2) {
                    if (args[1].equalsIgnoreCase("on")) {
                        PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.MAINTENANCE_PACKET, new MaintenancePacket(true, false));
                        CloudService.getCloudService().getManagerService().getProxyManager().broadcastPacket(initPacket);
                        CloudService.getCloudService().getManagerService().getProxyManager().setMaintenanceActive(true);
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cThe network is now in §4§lmaintenance§c!");
                    } else if (args[1].equalsIgnoreCase("off")) {
                        CloudService.getCloudService().getManagerService().getProxyManager().setMaintenanceActive(false);
                        PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.MAINTENANCE_PACKET, new MaintenancePacket(false, false));
                        CloudService.getCloudService().getManagerService().getProxyManager().broadcastPacket(initPacket);
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §aThe network is not longer in §4§lmaintenance§a!");
                    } else if (args[1].equalsIgnoreCase("kickall")) {
                        PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.MAINTENANCE_PACKET, new MaintenancePacket(false, true));
                        CloudService.getCloudService().getManagerService().getProxyManager().broadcastPacket(initPacket);
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §aAll players were kicked.");
                    } else {
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud maintenance [on|off|kickall]");
                    }
                } else {
                    if (CloudService.getCloudService().getManagerService().getProxyManager().isMaintenanceActive()) {
                        PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.MAINTENANCE_PACKET, new MaintenancePacket(true, false));
                        CloudService.getCloudService().getManagerService().getProxyManager().broadcastPacket(initPacket);
                        CloudService.getCloudService().getManagerService().getProxyManager().setMaintenanceActive(true);
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cThe network is now in §4§lmaintenance§c!");
                    } else {
                        CloudService.getCloudService().getManagerService().getProxyManager().setMaintenanceActive(false);
                        PacketHolder initPacket = GoAPI.getPacketHelper().preparePacket(PacketType.MAINTENANCE_PACKET, new MaintenancePacket(false, false));
                        CloudService.getCloudService().getManagerService().getProxyManager().broadcastPacket(initPacket);
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §aThe network is not longer in §4§lmaintenance§a!");
                    }
                }
                break;
            case "sinfo":
                if (args.length == 3) {
                    String name = args[2];
                    switch (args[1].toLowerCase()) {
                        case "server":
                            Server getServer = CloudService.getCloudService().getManagerService().getServerManager().getServerByName(name);
                            if (getServer != null) {
                                MessageGroup serverInfoGroup = new MessageGroup(sender);
                                serverInfoGroup.addMessage("§aInformation for the Server " + getServer.getName());
                                if (getServer.isCloudServer()) {
                                    serverInfoGroup.addMessage("§7CloudService: §c" + getServer.isCloudServer());
                                    serverInfoGroup.addMessage("§7Template: §c" + getServer.getServerTemplate().getName());
                                    serverInfoGroup.addMessage("§7Slave: §c" + getServer.getSlave().getName() + " (" + getServer.getSlave().getHost() + ")");
                                }
                                serverInfoGroup.addMessage("§7Host: §c" + getServer.getHost());
                                serverInfoGroup.addMessage("§7Port: §c" + getServer.getPort());
                                serverInfoGroup.addMessage("§7Players: §c" + getServer.getOnlinePlayers() + "/" + getServer.getMaxPlayers());
                                serverInfoGroup.addMessage("§7ServerState: §c" + getServer.getServerState().name());
                                serverInfoGroup.send();
                            } else {
                                sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cThere is not a server with this name.");
                            }
                            break;
                        case "slave":
                            Slave slave = CloudService.getCloudService().getManagerService().getSlaveManager().getSlaveByName(name);
                            if (slave != null) {
                                MessageGroup slaveInfo = new MessageGroup(sender);
                                slave.calculateRam();
                                slaveInfo.addMessage("§7Information for the Slave " + slave.getName());
                                slaveInfo.addMessage("§7Host: §c" + slave.getHost());
                                slaveInfo.addMessage("§7Max. Ram: §c" + slave.getMaxRam() + "MB");
                                slaveInfo.addMessage("§7Used Ram: §c" + slave.getUsedRam() + "MB");
                                if (slave.getCurrentStartingServer() != null) {
                                    slaveInfo.addMessage("§7Starting current the Server §c" + slave.getCurrentStartingServer());
                                }
                                slaveInfo.addMessage("§7Server on that Slave:");
                                for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
                                    if (server.isCloudServer() && server.getSlave().equals(slave)) {
                                        slaveInfo.addMessage("§c" + server.getName() + " §7(" + server.getPort() + ")");
                                    }
                                }
                                slaveInfo.send();
                            } else {
                                sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cThere is not a slave with this name.");
                            }
                            break;
                        case "proxy":
                            Proxy proxy = CloudService.getCloudService().getManagerService().getProxyManager().getProxyByName(name);
                            if (proxy != null) {
                                MessageGroup bungeeGroup = new MessageGroup(sender);
                                bungeeGroup.addMessage("§aInfos zum BungeeCord " + proxy.getName());
                                bungeeGroup.addMessage("§7UniqueId: §c" + proxy.getUniqueId());
                                bungeeGroup.addMessage("§7Host: §c" + proxy.getIpAddress());
                                bungeeGroup.addMessage("§7Online-Spieler: §c" + proxy.getOnlinePlayers());
                                bungeeGroup.send();
                            } else {
                                sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cThere is not a proxy with this name.");
                            }
                            break;
                        default:
                            sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud info <Server/Slave/Proxy> Name");
                            break;
                    }
                } else {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud info <Server/Slave/Proxy> Name");
                }
                break;
            case "log":
                if (args.length == 2) {
                    Server server = CloudService.getCloudService().getManagerService().getServerManager().getServerByName(args[1]);
                    if (server == null) {
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cThere is not a server with this name.");
                        return;
                    }
                    if (server.isCloudServer()) {
                        MessageGroup sendMessage = new MessageGroup(sender);
                        sendMessage.addMessage(sender.getLanguageMessage("cloud-prefix")+" §7I will download the Log from the Server §c" + server.getName() + "§7.");
                        sendMessage.addMessage(sender.getLanguageMessage("cloud-prefix")+" §7I will upload the Log to that URL:");
                        sendMessage.addMessage(sender.getLanguageMessage("cloud-prefix")+" §4http://acp.vicemice.net/log/" + server.getServerID());
                        sendMessage.send();
                        PacketHolder logPacket = GoAPI.getPacketHelper().preparePacket(PacketType.SAVE_LOG_PACKET, new SaveLogPacket(server.getName()));
                        server.getSlave().sendPacket(logPacket);
                    } else {
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cNon-Cloud Server Logs can not be uploaded.");
                    }
                } else {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud log <ServerName>");
                }
                break;
            case "template":
                if (args.length >= 2) {
                    TemplateData serverTemplate = CloudService.getCloudService().getManagerService().getServerManager().getTemplateByName(args[1]);
                    if (serverTemplate != null) {
                        if (args.length >= 3) {
                            if (args[2].equalsIgnoreCase("minserver")) {
                                serverTemplate.setMinServer(Integer.parseInt(args[3]));
                            } else if (args[2].equalsIgnoreCase("maxRamProServer")) {
                                serverTemplate.setMaxRamProServer(Integer.parseInt(args[3]));
                            } else if (args[2].equalsIgnoreCase("maxplayers")) {
                                serverTemplate.setMaxPlayers(Integer.parseInt(args[3]));
                            } else if (args[2].equalsIgnoreCase("prefix")) {
                                serverTemplate.setPrefix(args[3]);
                            } else if (args[2].equalsIgnoreCase("off")) {
                                serverTemplate.setActive(false);
                            } else if (args[2].equalsIgnoreCase("on")) {
                                serverTemplate.setActive(true);
                            } else if (args[2].equalsIgnoreCase("forcewrapper")) {
                                if (args[3].equalsIgnoreCase("clear")) {
                                    serverTemplate.setForceSlave(null);
                                } else {
                                    serverTemplate.setForceSlave(args[3]);
                                }
                            } else {
                                sender.sendRawMessage("§cUnknown argument!");
                                return;
                            }
                            sender.sendRawMessage("§aValue set");
                            return;
                        }
                        MessageGroup templateGroup = new MessageGroup(sender);
                        int serverCount = 0;
                        int playerCount = 0;
                        for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
                            if (server.isCloudServer() && server.getServerTemplate() == serverTemplate) {
                                serverCount++;
                                playerCount += server.getOnlinePlayers();
                            }
                        }
                        templateGroup.addMessage("§7Information for §c" + serverTemplate.getName());
                        templateGroup.addMessage("§7Servers: §c" + serverCount);
                        templateGroup.addMessage("§7Players: §c" + playerCount);
                        templateGroup.send();
                    } else {
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cThere is no template with that name");
                    }
                } else {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud template <Name>");
                }
                break;
            case "rs":
                if (args.length != 2) {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud rs <daily|monthly>");
                    return;
                }
                if (args[1].equalsIgnoreCase("daily")) {
                    for (CloudPlayer cloudPlayer : CloudService.getCloudService().getManagerService().getStatsManager().getDailyStats()) {
                        cloudPlayer.getStats().getDailyStats().clear();
                        cloudPlayer.getStats().getDailyRanking().clear();
                        if (cloudPlayer.isOnline()) cloudPlayer.sendData(cloudPlayer.getServer(), DataUpdateType.UPDATE_VOTES, true);
                        cloudPlayer.updateToDatabase(false);
                    }
                    CloudService.getCloudService().getManagerService().getStatsManager().getDailyRanking().clear();
                    CloudService.getCloudService().getManagerService().getStatsManager().getDailyStats().clear();
                    CloudService.getCloudService().getManagerService().getStatsManager().getDailyTop().clear();
                    sender.sendRawMessage("daily cleared");
                } else if (args[1].equalsIgnoreCase("monthly")) {
                    for (CloudPlayer cloudPlayer : CloudService.getCloudService().getManagerService().getStatsManager().getMonthlyStats()) {
                        cloudPlayer.getStats().getMonthlyStats().clear();
                        cloudPlayer.getStats().getMonthlyRanking().clear();
                        if (cloudPlayer.isOnline()) cloudPlayer.sendData(cloudPlayer.getServer(), DataUpdateType.UPDATE_VOTES, true);
                        cloudPlayer.updateToDatabase(false);
                    }
                    CloudService.getCloudService().getManagerService().getStatsManager().getMonthlyRanking().clear();
                    CloudService.getCloudService().getManagerService().getStatsManager().getMonthlyStats().clear();
                    CloudService.getCloudService().getManagerService().getStatsManager().getMonthlyTop().clear();
                    sender.sendRawMessage("monthly cleared");
                }
                break;
            case "uc":
                CloudService.getCloudService().getManagerService().getProxyManager().setUserCountMode(Boolean.parseBoolean(args[1]));
                break;
            case "pc":
                if (args.length == 2) {
                    int playerCount = 0;
                    int serverCount = 0;
                    for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
                        if (server.isCloudServer() && server.getTemplateName().toLowerCase().startsWith(args[1].toLowerCase())) {
                            playerCount += server.getOnlinePlayers();
                            serverCount++;
                        }
                    }
                    sender.sendRawMessage("§aPlayers: §e" + playerCount + " §aServer: §e" + serverCount);
                } else {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud pc <Template>");
                }
                break;
            case "usr":
                if (args.length != 1) {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud usr <player>");
                    return;
                }
                CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(args[1]);
                if (cloudPlayer == null) {
                    sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cPlayer not found");
                    return;
                }
                cloudPlayer.getStats().getGlobalStats().clear();
                cloudPlayer.getStats().getGlobalRanking().clear();
                cloudPlayer.getStats().getMonthlyStats().clear();
                cloudPlayer.getStats().getMonthlyRanking().clear();
                cloudPlayer.getStats().getDailyStats().clear();
                cloudPlayer.getStats().getDailyRanking().clear();
                cloudPlayer.updateToDatabase(false);
                sender.sendRawMessage("Resettet");
                break;
            case "maxcpu":
                CloudService.getCloudService().getManagerService().getServerManager().setMaxCpuAverage(Double.parseDouble(args[1]));
                sender.sendRawMessage("Set!");
                break;
            case "check": {
                sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §6Checking all templates...");
                CloudService.getCloudService().getManagerService().getServerManager().checkServers();
                sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §aAll templates checked.");
                break;
            }
            case "slave": {
                if (args.length == 2) {
                    if (args[1].equalsIgnoreCase("restart")) {
                        sender.sendMessage(sender.getLanguageMessage("cloud-prefix")+" §6The slave system will be restarted in few seconds");
                        for (final UUID uuid : CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers()) {
                            final CloudPlayer getPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uuid);
                            if (getPlayer.getServer() == null || getPlayer.getServer().getServerType() == null) {
                                getPlayer.connectToServer("fallback");
                                return;
                            }
                            if (getPlayer.getServer().getServerType() == ServerType.STATIC) {
                                continue;
                            }
                            getPlayer.connectToServer("fallback");
                            try {
                                Thread.sleep(25L);
                            } catch (InterruptedException e4) {
                                e4.printStackTrace();
                            }
                        }
                        try {
                            Thread.sleep(5000L);
                        } catch (InterruptedException e5) {
                            e5.printStackTrace();
                        }
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §6The slave system will be restarted now");

                        //TODO: Restart System
                        //CloudService.restartWrapper();
                        return;
                    }
                    final MessageGroup wrapperGroup = new MessageGroup(sender);
                    wrapperGroup.addMessage("§aOnline-Slave:");
                    for (final Slave slave4 : CloudService.getCloudService().getManagerService().getSlaveManager().getSlaveList()) {
                        int serverCount3 = 0;
                        for (final Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
                            if (server.getSlave() == slave4) {
                                ++serverCount3;
                            }
                        }
                        wrapperGroup.addMessage("§a" + slave4.getName() + " §7(Host: §a" + slave4.getHost() + " §7Servers: §a" + serverCount3 + " §7UsedRam: §a" + slave4.getUsedMemory() + " §7MaxRam: §a" + slave4.getMaxRam() + "§7)");
                    }
                    wrapperGroup.send();
                } else {
                    if (args.length != 3) {
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cUse /cloud slave <Name/List> [Action(start/Stop)]");
                        return;
                    }
                    final Slave slave5 = CloudService.getCloudService().getManagerService().getSlaveManager().getSlaveByName(args[1]);
                    if (slave5 == null) {
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cThere is not a slave with this name.");
                        return;
                    }
                    if (args[2].equalsIgnoreCase("start")) {
                        slave5.setActive(true);
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §cThe slave "+slave5.getName()+" has been §7enabled§c.");
                        return;
                    }
                    if (args[2].equalsIgnoreCase("stop")) {
                        slave5.setActive(false);
                        sender.sendRawMessage(sender.getLanguageMessage("cloud-prefix")+" §slave "+slave5.getName()+" has been §7disabled§c.");
                    }
                }
            }
        }
    }
}