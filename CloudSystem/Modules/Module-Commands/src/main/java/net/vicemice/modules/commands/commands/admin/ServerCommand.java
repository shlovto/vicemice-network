package net.vicemice.modules.commands.commands.admin;

import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ServerCommand extends Command {

    private static final List<UUID> whitelistedPlayers = Arrays.asList(UUID.fromString("c80be485-b4f9-46a7-afa7-947a50f76739"));

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (!sender.getRank().isAdmin() && !(whitelistedPlayers.contains(sender.getUniqueId()))) {
            sender.sendMessage("command-no-permission");
            return;
        }
        if (args.length == 0) {
            sender.sendMessage("command-server-usage");
        } else {
            sender.sendMessage("command-server-success", args[0]);
            sender.connectToServer(args[0]);
        }
    }
}