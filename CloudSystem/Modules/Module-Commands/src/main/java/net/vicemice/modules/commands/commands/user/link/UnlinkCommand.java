package net.vicemice.modules.commands.commands.user.link;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.network.modules.api.event.discord.DiscordUnverifyEvent;
import net.vicemice.hector.network.modules.api.event.teamspeak.TeamSpeakUnverifyEvent;

public class UnlinkCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (args.length == 2 && args[0].equalsIgnoreCase("confirm") && args[1].equalsIgnoreCase("teamspeak")) {
            CloudService.getCloudService().getManagerService().getEventManager().callEvent(new TeamSpeakUnverifyEvent(sender, true));
        } else if (args.length == 2 && args[0].equalsIgnoreCase("confirm") && args[1].equalsIgnoreCase("discord")) {
            CloudService.getCloudService().getManagerService().getEventManager().callEvent(new DiscordUnverifyEvent(sender, true));
        } else if (args.length == 1 && args[0].equalsIgnoreCase("teamspeak")) {
            CloudService.getCloudService().getManagerService().getEventManager().callEvent(new TeamSpeakUnverifyEvent(sender, false));
        } else if (args.length == 1 && args[0].equalsIgnoreCase("discord")) {
            CloudService.getCloudService().getManagerService().getEventManager().callEvent(new DiscordUnverifyEvent(sender, false));
        } else {
            sender.sendMessage("command-unlink-usage");
        }
    }
}
