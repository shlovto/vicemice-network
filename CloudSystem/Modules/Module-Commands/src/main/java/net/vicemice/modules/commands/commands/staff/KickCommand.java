package net.vicemice.modules.commands.commands.staff;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;

public class KickCommand extends Command {

    public KickCommand() {
        super(Area.ADMINISTRATING);
    }

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (sender.getRank().isAdmin()) {
            if (args.length >= 2) {
                StringBuilder stringBuilder = new StringBuilder();
                int i = 1;
                while (i < args.length) {
                    if (i < args.length) {
                        stringBuilder.append(args[i] + " ");
                    } else {
                        stringBuilder.append(args[i]);
                    }
                    ++i;
                }
                String reason = stringBuilder.toString();

                String name = args[0];
                CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(name);
                if (cloudPlayer == null) {
                    sender.sendMessage("user-not-exist", sender.getLanguageMessage("team-prefix"));
                    return;
                }
                if (!cloudPlayer.isOnline()) {
                    sender.sendMessage("user-not-online", sender.getLanguageMessage("team-prefix"), (cloudPlayer.getRank().getRankColor()+cloudPlayer.getName()));
                    return;
                }
                if (cloudPlayer.getRank().isTeam() && !(sender.getRank().getAccessLevel() > 90)) {
                    sender.sendMessage("can-not-punish");
                    return;
                }

                sender.sendMessage("command-kick-success");
                CloudService.getCloudService().getManagerService().getAbuseManager().kickPlayer(cloudPlayer, reason, sender);
            } else {
                sender.sendMessage("command-kick-usage");
            }
        } else {
            sender.sendMessage("command-no-permission");
        }
    }
}