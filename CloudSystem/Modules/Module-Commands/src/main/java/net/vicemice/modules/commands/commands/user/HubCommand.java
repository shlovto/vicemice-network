package net.vicemice.modules.commands.commands.user;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.commands.Command;
import net.vicemice.hector.player.CloudPlayer;

public class HubCommand extends Command {

    @Override
    public void execute(String[] args, CloudPlayer sender) {
        if (sender.getServer() == null) CloudService.getCloudService().getManagerService().getLobbyManager().sendToLobby(sender.getName());
        if (!sender.getServer().getName().toLowerCase().startsWith("lobby") && !sender.getServer().getName().toLowerCase().startsWith("silent")) {
            CloudService.getCloudService().getManagerService().getLobbyManager().sendToLobby(sender.getName());
        } else {
            sender.sendMessage("command-hub-already-on-hub");
        }
    }
}