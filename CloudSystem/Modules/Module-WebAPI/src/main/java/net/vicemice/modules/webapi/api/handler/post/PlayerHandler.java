package net.vicemice.modules.webapi.api.handler.post;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.language.Language;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import org.eclipse.jetty.server.Request;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * Class created at 21:19 - 27.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (!token.hasPermission("player")) {
                    Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                    return;
                }

                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("add")) {
                        if (httpServletRequest.getHeader("data").equalsIgnoreCase("player")) {
                            if (httpServletRequest.getHeader("amount") != null) {
                                if (httpServletRequest.getHeader("target") != null) {
                                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(httpServletRequest.getHeader("target"));
                                    if (cloudPlayer != null) {
                                        if (httpServletRequest.getHeader("object").equalsIgnoreCase("coins")) {
                                            try {
                                                JSONObject jsonObject = new JSONObject();
                                                jsonObject.put("status", "SUCCESS");
                                                jsonObject.put("message", "COINS_ADDED");
                                                Main.getWebServer().message(httpServletResponse, jsonObject);

                                                cloudPlayer.addCoins(Integer.valueOf(httpServletRequest.getHeader("amount")));
                                                if (cloudPlayer.isOnline()) {
                                                    if (cloudPlayer.getLanguage() == Language.LanguageType.GERMAN) {
                                                        cloudPlayer.sendMessage("§7Jemand unbekanntes hat dir §6" + Integer.valueOf(httpServletRequest.getHeader("amount")) + " §7Coins geschenkt. Nutze diese Sinnvoll.");
                                                    } else {
                                                        cloudPlayer.sendMessage("§7Someone unknown has just given you §6" + Integer.valueOf(httpServletRequest.getHeader("amount")) + " §7Coins. Use this useful.");
                                                    }
                                                }
                                            } catch (Exception ex) {
                                                JSONObject jsonObject = new JSONObject();
                                                jsonObject.put("status", "ERROR");
                                                jsonObject.put("message", "INVALID_AMOUNT");
                                                jsonObject.put("code", ex.getCause().toString().toUpperCase());
                                                Main.getWebServer().message(httpServletResponse, jsonObject);
                                            }
                                        } else {
                                            JSONObject jsonObject = new JSONObject();
                                            jsonObject.put("status", "ERROR");
                                            jsonObject.put("message", "INVALID_OBJECT");
                                            Main.getWebServer().message(httpServletResponse, jsonObject);
                                        }
                                    } else {
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("status", "ERROR");
                                        jsonObject.put("message", "PLAYER_NOT_EXIST");
                                        Main.getWebServer().message(httpServletResponse, jsonObject);
                                    }
                                } else {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("status", "ERROR");
                                    jsonObject.put("message", "INVALID_REQUEST");
                                    Main.getWebServer().message(httpServletResponse, jsonObject);
                                }
                            } else {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "INVALID_REQUEST");
                                Main.getWebServer().message(httpServletResponse, jsonObject);
                            }
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_REQUEST");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                        }
                    } else if (httpServletRequest.getHeader("request").equalsIgnoreCase("remove")) {
                        if (httpServletRequest.getHeader("data").equalsIgnoreCase("player")) {
                            if (httpServletRequest.getHeader("amount") != null) {
                                if (httpServletRequest.getHeader("target") != null) {
                                    CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(httpServletRequest.getHeader("target"));
                                    if (cloudPlayer != null) {
                                        if (httpServletRequest.getHeader("object").equalsIgnoreCase("coins")) {
                                            try {
                                                JSONObject jsonObject = new JSONObject();
                                                jsonObject.put("status", "SUCCESS");
                                                jsonObject.put("message", "COINS_REMOVED");
                                                Main.getWebServer().message(httpServletResponse, jsonObject);

                                                cloudPlayer.removeCoins(Long.parseLong(httpServletRequest.getHeader("amount")));
                                                if (cloudPlayer.isOnline()) {
                                                    if (cloudPlayer.getLanguage() == Language.LanguageType.GERMAN) {
                                                        cloudPlayer.sendMessage("§7Jemand unbekanntes hat dir soeben §6" + Integer.valueOf(httpServletRequest.getHeader("amount")) + " §7Coins genommen. Wer das war, erfährst du wohl niemals...");
                                                    } else {
                                                        cloudPlayer.sendMessage("§7Someone unknown has just stealed you §6" + Integer.valueOf(httpServletRequest.getHeader("amount")) + " §7Coins. Who that was, you probably never know...");
                                                    }
                                                }
                                            } catch (Exception ex) {
                                                JSONObject jsonObject = new JSONObject();
                                                jsonObject.put("status", "ERROR");
                                                jsonObject.put("message", "INVALID_AMOUNT");
                                                jsonObject.put("code", ex.getCause().toString().toUpperCase());
                                                Main.getWebServer().message(httpServletResponse, jsonObject);
                                            }
                                        } else {
                                            JSONObject jsonObject = new JSONObject();
                                            jsonObject.put("status", "ERROR");
                                            jsonObject.put("message", "INVALID_OBJECT");
                                            Main.getWebServer().message(httpServletResponse, jsonObject);
                                        }
                                    } else {
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("status", "ERROR");
                                        jsonObject.put("message", "PLAYER_NOT_EXIST");
                                        Main.getWebServer().message(httpServletResponse, jsonObject);
                                    }
                                } else {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("status", "ERROR");
                                    jsonObject.put("message", "INVALID_REQUEST");
                                    Main.getWebServer().message(httpServletResponse, jsonObject);
                                }
                            } else {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "INVALID_REQUEST");
                                Main.getWebServer().message(httpServletResponse, jsonObject);
                            }
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_REQUEST");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                        }
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);
        }
    }
}