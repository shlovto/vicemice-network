package net.vicemice.modules.webapi.api.handler.get.acp;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.player.utils.ASNetwork;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import org.eclipse.jetty.server.Request;
import org.json.JSONArray;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * Class created at 15:52 - 28.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ASNetworkHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("asnetwork")) {
                        if (!token.hasPermission("acp")) {
                            Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                            return;
                        }

                        if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("registered")) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "SUCCESS");

                            JSONArray jsonArray = new JSONArray();
                            for (ASNetwork asNetwork : CloudService.getCloudService().getManagerService().getAutonomousManager().getNetworks().values()) {
                                JSONObject network = new JSONObject();
                                network.put("id", asNetwork.getId());
                                network.put("organisation", asNetwork.getName());
                                network.put("created", asNetwork.getCreated());
                                network.put("connections", asNetwork.getConnections());
                                network.put("firstPlayer", asNetwork.getFirstPlayer());
                                jsonArray.put(network);
                            }

                            jsonObject.put("networks", jsonArray);
                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_REQUEST");
                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                        }
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);;
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);;
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);;
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);;
        }
    }
}
