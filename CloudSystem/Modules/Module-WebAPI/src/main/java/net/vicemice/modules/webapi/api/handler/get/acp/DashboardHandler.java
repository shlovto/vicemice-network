package net.vicemice.modules.webapi.api.handler.get.acp;

import net.vicemice.hector.CloudService;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import net.vicemice.modules.webapi.api.utils.PlayerDate;
import org.bson.Document;
import org.eclipse.jetty.server.Request;
import org.json.JSONArray;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

/*
 * Class created at 13:31 - 27.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class DashboardHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("players")) {
                        if (!token.hasPermission("acp")) {
                            Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                            return;
                        }

                        if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("new_players_charts")) {
                            ArrayList<Document> charts = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("players");
                            ConcurrentHashMap<String, Integer> players = new ConcurrentHashMap<>();
                            ArrayList<PlayerDate> dates = new ArrayList<>();

                            for (Document document : charts) {
                                Date date = new Date(document.get("dates", Document.class).getLong("firstlogin"));
                                String da = new SimpleDateFormat("yyyy-MM-dd").format(date);
                                if (players.containsKey(da)) {
                                    players.put(da, (players.get(da)+1));
                                } else {
                                    players.put(da, 1);
                                }
                            }

                            for (String s1 : players.keySet()) {
                                PlayerDate playerDate = null;
                                try {
                                    playerDate = new PlayerDate(new SimpleDateFormat("yyyy-MM-dd").parse(s1), s1);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                dates.add(playerDate);
                            }

                            Collections.sort(dates);

                            JSONObject jsonObject = new JSONObject();

                            JSONArray jsonArray = new JSONArray();
                            for (PlayerDate playerDate : dates) {
                                JSONObject date = new JSONObject();
                                date.put("date", playerDate.getDate());
                                date.put("value", players.get(playerDate.getDate()));
                                jsonArray.put(date);
                            }

                            jsonObject.put("status", "SUCCESS");
                            jsonObject.put("dates", jsonArray);
                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                        } else if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("chatMessages")) {
                            ArrayList<Document> charts = CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("chat");
                            ConcurrentHashMap<String, Integer> players = new ConcurrentHashMap<>();
                            ArrayList<PlayerDate> dates = new ArrayList<>();

                            for (Document document : charts) {
                                Date date = new Date(document.getLong("time"));
                                String da = new SimpleDateFormat("yyyy-MM-dd").format(date);
                                if (players.containsKey(da)) {
                                    players.put(da, (players.get(da)+1));
                                } else {
                                    players.put(da, 1);
                                }
                            }

                            for (String s1 : players.keySet()) {
                                PlayerDate playerDate = null;
                                try {
                                    playerDate = new PlayerDate(new SimpleDateFormat("yyyy-MM-dd").parse(s1), s1);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                dates.add(playerDate);
                            }

                            Collections.sort(dates);

                            JSONObject jsonObject = new JSONObject();

                            JSONArray jsonArray = new JSONArray();
                            for (PlayerDate playerDate : dates) {
                                JSONObject date = new JSONObject();
                                date.put("date", playerDate.getDate());
                                date.put("value", players.get(playerDate.getDate()));
                                jsonArray.put(date);
                            }

                            jsonObject.put("status", "SUCCESS");
                            jsonObject.put("dates", jsonArray);
                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_REQUEST");
                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                        }
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);;
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);;
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);;
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);;
        }
    }
}
