package net.vicemice.modules.webapi;

import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.hector.network.modules.api.CoreModule;
import net.vicemice.hector.utils.terminal.CommandRegistry;
import net.vicemice.hector.utils.terminal.commands.CommandHelp;
import net.vicemice.modules.webapi.api.WebServer;
import net.vicemice.modules.webapi.api.utils.WebAPICommand;

public class Main extends CoreModule {

    @Getter
    private static WebServer webServer;

    public void onLoad() {
        webServer = new WebServer();
        CommandRegistry.registerCommand(new WebAPICommand(), "webapi");
        onBootstrap();
    }

    public void onBootstrap() {
        new Thread(() -> { webServer.startServer(); }).start(); //Brand new API (v3)
    }

    @Override
    public void onShutdown() {
        try {
            CommandRegistry.getCommands().forEach(command -> {
                if (!command.getAlias().contains("webapi")) return;
                CommandRegistry.getCommands().remove(command);
            });
            webServer.stopServer();
        } catch (Exception ex) {
            ex.printStackTrace();
            CloudService.getCloudService().getTerminal().writeMessage("§cAn error occurred while stopping the §e§lWebAPI");
        }
    }
}