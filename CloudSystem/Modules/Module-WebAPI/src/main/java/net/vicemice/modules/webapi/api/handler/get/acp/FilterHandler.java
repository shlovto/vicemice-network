package net.vicemice.modules.webapi.api.handler.get.acp;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.player.utils.Filter;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import org.eclipse.jetty.server.Request;
import org.json.JSONArray;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * Class created at 20:55 - 28.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class FilterHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("filter")) {
                        if (!token.hasPermission("acp")) {
                            Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                            return;
                        }

                        if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("registered")) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "SUCCESS");

                            JSONArray jsonArray = new JSONArray();
                            for (Filter filter : CloudService.getCloudService().getManagerService().getFilterManager().getEntries()) {
                                JSONObject network = new JSONObject();
                                network.put("word", filter.getWord());
                                network.put("creator", CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getPlayerName(filter.getCreator()));
                                network.put("created", filter.getCreated());
                                jsonArray.put(network);
                            }

                            jsonObject.put("filter", jsonArray);
                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_REQUEST");
                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                        }
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);;
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);;
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);;
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);;
        }
    }
}