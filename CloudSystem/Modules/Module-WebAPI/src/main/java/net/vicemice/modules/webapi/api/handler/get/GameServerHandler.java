package net.vicemice.modules.webapi.api.handler.get;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.network.gameserver.Server;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import org.eclipse.jetty.server.Request;
import org.json.JSONArray;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * Class created at 09:55 - 27.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GameServerHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("cloud")) {
                        if (!token.hasPermission("cloud")) {
                            Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                            return;
                        }

                        if (httpServletRequest.getHeader("service") == null) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_SERVICE");
                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                            return;
                        }

                        if (httpServletRequest.getHeader("service").equalsIgnoreCase("gameServers")) {
                            if (httpServletRequest.getHeader("template") != null) {
                                JSONObject serversObject = new JSONObject();
                                JSONArray serversArray = new JSONArray();
                                for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServersByTemplate(httpServletRequest.getHeader("template"))) {
                                    if (!server.isCloudServer()) continue;
                                    JSONObject serverObject = new JSONObject();
                                    serverObject.put("serverName", server.getName());
                                    serverObject.put("template", server.getTemplateName());
                                    serverObject.put("slave", server.getSlave().getName());
                                    serverObject.put("message", server.getMessage());
                                    serverObject.put("players", server.getOnlinePlayers());
                                    serverObject.put("maxPlayers", server.getMaxPlayers());
                                    serverObject.put("serverID", server.getServerID());
                                    serverObject.put("state", server.getServerState().name());
                                    serverObject.put("lobbyServer", server.isLobby());
                                    serverObject.put("cloudServer", server.isCloudServer());

                                    JSONObject proxys = new JSONObject();
                                    for (String proxy : server.getPlayers().keySet()) {
                                        proxys.put(proxy, server.getPlayers().get(proxy));
                                    }
                                    serverObject.put("proxys", proxys);

                                    serverObject.put("signs", server.getServerTemplate().getLobbySigns());
                                    serversArray.put(serverObject);
                                }

                                serversObject.put("status", "SUCCESS");
                                serversObject.put("servers", serversArray);
                                Main.getWebServer().message(httpServletResponse, serversObject);
                            } else {
                                JSONObject serversObject = new JSONObject();
                                JSONArray serversArray = new JSONArray();
                                for (Server server : CloudService.getCloudService().getManagerService().getServerManager().getServers()) {
                                    if (!server.isCloudServer()) continue;
                                    JSONObject serverObject = new JSONObject();
                                    serverObject.put("serverName", server.getName());
                                    serverObject.put("template", server.getTemplateName());
                                    serverObject.put("slave", server.getSlave().getName());
                                    serverObject.put("message", server.getMessage());
                                    serverObject.put("players", server.getOnlinePlayers());
                                    serverObject.put("maxPlayers", server.getMaxPlayers());
                                    serverObject.put("serverID", server.getServerID());
                                    serverObject.put("state", server.getServerState().name());
                                    serverObject.put("lobbyServer", server.isLobby());
                                    serverObject.put("cloudServer", server.isCloudServer());

                                    JSONObject proxys = new JSONObject();
                                    for (String proxy : server.getPlayers().keySet()) {
                                        proxys.put(proxy, server.getPlayers().get(proxy));
                                    }
                                    serverObject.put("proxys", proxys);

                                    serverObject.put("signs", server.getServerTemplate().getLobbySigns());
                                    serversArray.put(serverObject);
                                }

                                serversObject.put("status", "SUCCESS");
                                serversObject.put("servers", serversArray);
                                Main.getWebServer().message(httpServletResponse, serversObject);
                            }
                        } else if (httpServletRequest.getHeader("service").equalsIgnoreCase("gameServer")) {
                            if (httpServletRequest.getHeader("name") == null) {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "INVALID_NAME");
                                Main.getWebServer().message(httpServletResponse, jsonObject);;
                                return;
                            }

                            Server server = CloudService.getCloudService().getManagerService().getServerManager().getServerByName(httpServletRequest.getHeader("name"));
                            if (server != null) {

                                JSONObject serverObject = new JSONObject();
                                serverObject.put("name", server.getName());
                                serverObject.put("template", server.getTemplateName());
                                serverObject.put("slave", server.getSlave().getName());
                                serverObject.put("message", server.getMessage());
                                serverObject.put("players", server.getOnlinePlayers());
                                serverObject.put("maxPlayers", server.getMaxPlayers());
                                serverObject.put("serverID", server.getServerID());
                                serverObject.put("state", server.getServerState().name());
                                serverObject.put("lobbyServer", server.isLobby());
                                serverObject.put("cloudServer", server.isCloudServer());

                                JSONObject proxys = new JSONObject();
                                for (String proxy : server.getPlayers().keySet()) {
                                    proxys.put(proxy, server.getPlayers().get(proxy));
                                }
                                serverObject.put("proxys", proxys);

                                serverObject.put("signs", server.getServerTemplate().getLobbySigns());

                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "SUCCESS");
                                jsonObject.put("server", serverObject);
                                Main.getWebServer().message(httpServletResponse, jsonObject);
                            } else {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "SERVER_NOT_FOUND");
                                Main.getWebServer().message(httpServletResponse, jsonObject);;
                            }
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_REQUEST");
                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                        }
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);;
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);;
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);;
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);;
        }
    }
}
