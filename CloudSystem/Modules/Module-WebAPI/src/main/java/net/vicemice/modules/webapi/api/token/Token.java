package net.vicemice.modules.webapi.api.token;

import lombok.Getter;
import org.json.JSONObject;

import java.util.List;

public class Token {

    @Getter
    private String key;
    @Getter
    private List<String> permissions;

    public Token(String key, List<String> permissions) {
        this.key = key;
        this.permissions = permissions;
    }

    public boolean hasPermission(String s) {
        return permissions.contains(s);
    }

    public JSONObject noPermissionText() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status", "ERROR");
        jsonObject.put("message", "NO_PERMISSIONS");
        return jsonObject;
    }
}