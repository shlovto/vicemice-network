package net.vicemice.modules.webapi.api;

import lombok.Getter;
import net.vicemice.hector.CloudService;
import net.vicemice.modules.webapi.api.token.TokenManager;
import org.bson.Document;
import org.eclipse.jetty.server.Server;
import org.json.JSONObject;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WebServer {
    @Getter
    private Server server;
    @Getter
    private TokenManager tokenManager;
    @Getter
    private boolean started = false;

    public void startServer() {
        tokenManager = new TokenManager();

        CloudService.getCloudService().getTerminal().writeMessage("§3Starting §e§lAPI §8(§e§lWEB§8)");
        server = new Server(InetSocketAddress.createUnresolved(CloudService.getCloudService().getManagerService().getConfigManager().getCloudConfig().getHost(), 1910));
        server.setHandler(new WebHandler());
        try {
            server.start();
            server.join();
            started = true;
        } catch (Exception e) {
            e.printStackTrace();
            CloudService.getCloudService().getTerminal().writeMessage("§cError while starting §e§lAPI §8(§e§lWEB§8)");
            return;
        }

        CloudService.getCloudService().getTerminal().writeMessage("§3Successfully started §e§lAPI §8(§e§lWEB§8)");
    }

    public void stopServer() throws Exception {
        started = false;
        server.stop();
    }

    public boolean checkVersion() {
        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date(System.currentTimeMillis()));
        int year = Integer.parseInt(date.split("-")[0]);
        int month = Integer.parseInt(date.split("-")[1]);
        int day = Integer.parseInt(date.split("-")[2]);

        if (year >= 2020 && month >= 6 && day >= 10) {
            return true;
        }

        return false;
    }

    public void message(HttpServletResponse httpServletResponse, Object object) throws IOException {
        if (object instanceof JSONObject) {
            if (this.checkVersion()) {
                ((JSONObject) object).put("notification", "API_NEEDS_AN_UPDATE");
            }
        }
        httpServletResponse.setContentType("application/json");
        httpServletResponse.getWriter().println(object);
    }

    public double myRound(double wert, int stellen) {
        return Math.round(wert * Math.pow(10, stellen)) / Math.pow(10, stellen);
    }

    public Document getSpaceDocument(String collection, Document filter) {
        for (Document document : CloudService.getCloudService().getManagerService().getMongoManager().getClient().getDatabase("space").getCollection(collection).find(filter)) {
            return document;
        }

        return null;
    }

    public Document getRoleplayDocument(String collection, Document filter) {
        for (Document document : CloudService.getCloudService().getManagerService().getMongoManager().getClient().getDatabase("roleplay").getCollection(collection).find(filter)) {
            return document;
        }

        return null;
    }
}