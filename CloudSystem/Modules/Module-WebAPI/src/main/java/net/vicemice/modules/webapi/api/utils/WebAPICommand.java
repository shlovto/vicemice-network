package net.vicemice.modules.webapi.api.utils;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.utils.terminal.ArgumentList;
import net.vicemice.hector.utils.terminal.CommandExecutor;
import net.vicemice.hector.utils.terminal.Terminal;
import net.vicemice.modules.webapi.Main;

/*
 * Class created at 14:53 - 02.05.2020
 * Copyright (C) elrobtossohn
 */
public class WebAPICommand implements CommandExecutor {

    @Override
    public void onCommand(String command, Terminal writer, String[] args) {
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("toggle")) {
                if (Main.getWebServer().isStarted()) {
                    try {
                        Main.getWebServer().stopServer();
                        CloudService.getCloudService().getTerminal().writeMessage("§3Successfully shutdown the §e§lAPI §8(§e§lWEB§8)");
                    } catch (Exception e) {
                        e.printStackTrace();
                        CloudService.getCloudService().getTerminal().writeMessage("§cError while shutdown the §e§lAPI §8(§e§lWEB§8)");
                    }
                } else {
                    Main.getWebServer().startServer();
                }
            }
        } else {
            writer.write("§cSyntax: webapi <toggle>");
        }
    }

    @Override
    public ArgumentList getArguments() {
        return ArgumentList.builder().arg(new ArgumentList.Argument("/webapi", "Manage the WebAPI")).build();
    }
}