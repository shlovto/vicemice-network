package net.vicemice.modules.webapi.api.handler.get;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.packets.types.player.report.ReportPlayersPacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.language.Language;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import org.eclipse.jetty.server.Request;
import org.json.JSONArray;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * Class created at 09:25 - 27.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ReportHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("report")) {
                        if (!token.hasPermission("report")) {
                            Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                            return;
                        }

                        if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("all")) {
                            JSONObject reportsObject = new JSONObject();
                            JSONArray reportsArray = new JSONArray();

                            if (CloudService.getCloudService().getManagerService().getReportManager().getEntries().isEmpty()) {
                                reportsObject.put("status", "SUCCESS");
                                reportsObject.put("message", "NO_OPENED_REPORTS");
                                httpServletResponse.getWriter().println(reportsObject);
                                return;
                            }

                            for (ReportPlayersPacket.ReportEntry report : CloudService.getCloudService().getManagerService().getReportManager().getEntries()) {
                                if (report.getVisitor() != null && report.getState() != ReportPlayersPacket.ReportState.NOT_EDITED) {
                                    continue;
                                }
                                JSONObject reportObject = new JSONObject();
                                JSONArray reasonArray = new JSONArray();
                                reportObject.put("id", report.getUniqueID());
                                reportObject.put("suspect", report.getSuspect());

                                reportObject.put("status", report.getState().name());
                                if (report.getVisitor() != null) {
                                    reportObject.put("visitor", report.getVisitor());
                                }

                                for (ReportPlayersPacket.AccusationsReason reason : report.getAccusations()) {
                                    JSONObject reasonObject = new JSONObject();
                                    reasonObject.put("reporter", reason.getName());
                                    reasonObject.put("reason", reason.getReason());
                                    if (reason.getReplayId() != null) {
                                        reasonObject.put("replayId", reason.getReplayId());
                                    } else {
                                        reasonObject.put("replayId", "NO_REPLAYID");
                                    }
                                    if (reason.getChatLogId() != null) {
                                        reasonObject.put("chatLog", reason.getChatLogId());
                                    } else {
                                        reasonObject.put("chatLog", "NO_CHATLOG");
                                    }
                                    reasonObject.put("server", reason.getServer());
                                    reasonArray.put(reasonObject);
                                }
                                reportObject.put("reports", reasonArray);
                                reportsArray.put(reportObject);
                            }
                            if (reportsArray.length() != 0) {
                                reportsObject.put("status", "SUCCESS");
                                reportsObject.put("reports", reportsArray);
                            } else {
                                reportsObject.put("status", "ERROR");
                                reportsObject.put("message", "NO_OPENED_REPORTS");
                            }
                            Main.getWebServer().message(httpServletResponse, reportsObject);
                        } else if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("get")) {
                            if (httpServletRequest.getHeader("visitor") != null) {
                                CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(httpServletRequest.getHeader("visitor"));
                                if (!cloudPlayer.getRank().isModeration() && !cloudPlayer.getRank().isAdmin()) {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("status", "ERROR");
                                    jsonObject.put("message", "NOT_MODERATOR");
                                    Main.getWebServer().message(httpServletResponse, jsonObject);
                                    return;
                                }
                                if (!cloudPlayer.isOnline()) {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("status", "ERROR");
                                    jsonObject.put("message", "YOU_ARE_NOT_ONLINE");
                                    Main.getWebServer().message(httpServletResponse, jsonObject);
                                    return;
                                }
                                if (cloudPlayer.getEditReport() == null) {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("status", "ERROR");
                                    jsonObject.put("message", "NO_REPORT");
                                    Main.getWebServer().message(httpServletResponse, jsonObject);
                                    return;
                                }

                                JSONObject reportsObject = new JSONObject();
                                JSONArray reportsArray = new JSONArray();
                                if (CloudService.getCloudService().getManagerService().getReportManager().getEntries().isEmpty()) {
                                    reportsObject.put("status", "ERROR");
                                    reportsObject.put("message", "NO_OPENED_REPORTS");
                                    Main.getWebServer().message(httpServletResponse, reportsObject);
                                    return;
                                }

                                for (ReportPlayersPacket.ReportEntry report : CloudService.getCloudService().getManagerService().getReportManager().getEntries()) {
                                    if (report.getUniqueID() != cloudPlayer.getEditReport()) {
                                        continue;
                                    }
                                    JSONObject reportObject = new JSONObject();
                                    JSONArray reasonArray = new JSONArray();
                                    reportObject.put("id", report.getUniqueID());
                                    reportObject.put("suspect", report.getSuspect());

                                    reportObject.put("status", report.getState().name());
                                    if (report.getVisitor() != null) {
                                        reportObject.put("visitor", report.getVisitor());
                                    }

                                    for (ReportPlayersPacket.AccusationsReason reason : report.getAccusations()) {
                                        JSONObject reasonObject = new JSONObject();
                                        reasonObject.put("reporter", reason.getName());
                                        reasonObject.put("reason", reason.getReason());
                                        if (reason.getReplayId() != null) {
                                            reasonObject.put("replayId", reason.getReplayId());
                                        } else {
                                            reasonObject.put("replayId", "NO_REPLAYID");
                                        }
                                        if (reason.getChatLogId() != null) {
                                            reasonObject.put("chatLog", reason.getChatLogId());
                                        } else {
                                            reasonObject.put("chatLog", "NO_CHATLOG");
                                        }
                                        reasonObject.put("server", reason.getServer());
                                        reasonArray.put(reasonObject);
                                    }
                                    reportObject.put("reports", reasonArray);
                                    reportsArray.put(reportObject);
                                }

                                reportsObject.put("status", "SUCCESS");
                                if (reportsArray.length() >= 1) {
                                    reportsObject.put("report", reportsArray);
                                } else {
                                    reportsObject.put("report", "NO_REPORT");
                                }

                                httpServletResponse.getWriter().println(reportsObject);
                            } else {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "INVALID_REQUEST");
                                Main.getWebServer().message(httpServletResponse, jsonObject);
                            }
                        } else if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("opened")) {
                            if (httpServletRequest.getHeader("visitor") != null) {
                                CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(httpServletRequest.getHeader("visitor"));
                                if (!cloudPlayer.getRank().isModeration() && !cloudPlayer.getRank().isAdmin()) {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("status", "ERROR");
                                    jsonObject.put("message", "NOT_MODERATOR");
                                    Main.getWebServer().message(httpServletResponse, jsonObject);
                                    return;
                                }
                                if (!cloudPlayer.isOnline()) {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("status", "ERROR");
                                    jsonObject.put("message", "YOU_ARE_NOT_ONLINE");
                                    Main.getWebServer().message(httpServletResponse, jsonObject);
                                    return;
                                }

                                JSONObject reportsObject = new JSONObject();
                                JSONArray reportsArray = new JSONArray();
                                if (CloudService.getCloudService().getManagerService().getReportManager().getEntries().isEmpty()) {
                                    reportsObject.put("status", "ERROR");
                                    reportsObject.put("message", "NO_OPENED_REPORTS");
                                    Main.getWebServer().message(httpServletResponse, reportsObject);
                                    return;
                                }

                                for (ReportPlayersPacket.ReportEntry report : CloudService.getCloudService().getManagerService().getReportManager().getEntries()) {
                                    if (report.getVisitor() != null && report.getState() != ReportPlayersPacket.ReportState.NOT_EDITED) {
                                        continue;
                                    }
                                    JSONObject reportObject = new JSONObject();
                                    JSONArray reasonArray = new JSONArray();
                                    reportObject.put("id", report.getUniqueID());
                                    reportObject.put("suspect", report.getSuspect());

                                    reportObject.put("status", report.getState().name());
                                    if (report.getVisitor() != null) {
                                        reportObject.put("visitor", report.getVisitor());
                                    }

                                    for (ReportPlayersPacket.AccusationsReason reason : report.getAccusations()) {
                                        JSONObject reasonObject = new JSONObject();
                                        reasonObject.put("reporter", reason.getName());
                                        reasonObject.put("reason", reason.getReason());
                                        if (reason.getReplayId() != null) {
                                            reasonObject.put("replayId", reason.getReplayId());
                                        } else {
                                            reasonObject.put("replayId", "NO_REPLAYID");
                                        }
                                        if (reason.getChatLogId() != null) {
                                            reasonObject.put("chatLog", reason.getChatLogId());
                                        } else {
                                            reasonObject.put("chatLog", "NO_CHATLOG");
                                        }
                                        reasonObject.put("server", reason.getServer());
                                        reasonArray.put(reasonObject);
                                    }
                                    reportObject.put("reports", reasonArray);
                                    reportsArray.put(reportObject);
                                }
                                if (reportsArray.length() >= 1) {
                                    reportsObject.put("reports", reportsArray);
                                } else {
                                    reportsObject.put("error", "NO_OPENED_REPORTS");
                                }
                                Main.getWebServer().message(httpServletResponse, reportsObject);
                            } else {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "INVALID_REQUEST");
                                Main.getWebServer().message(httpServletResponse, jsonObject);
                            }
                        } else if (httpServletRequest.getHeader("get") != null && httpServletRequest.getHeader("get").equalsIgnoreCase("new")) {
                            if (request.getHeader("visitor") != null) {
                                if (CloudService.getCloudService().getManagerService().getReportManager().getEntries().isEmpty()) {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("error", "NO_OPENED_REPORTS");
                                    Main.getWebServer().message(httpServletResponse, jsonObject);
                                }

                                String visitor = request.getHeader("visitor");

                                ReportPlayersPacket.ReportEntry report = CloudService.getCloudService().getManagerService().getReportManager().getEntries().get(0);
                                for (ReportPlayersPacket.ReportEntry reportEntry : CloudService.getCloudService().getManagerService().getReportManager().getEntries()) {
                                    if (reportEntry.getVisitor() != null && reportEntry.getState() != ReportPlayersPacket.ReportState.NOT_EDITED) continue;
                                    report = reportEntry;
                                    break;
                                }

                                CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(visitor);

                                if (cloudPlayer.isOnline()) {
                                    if (!cloudPlayer.getRank().isModeration() && !cloudPlayer.getRank().isAdmin()) {
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("status", "ERROR");
                                        jsonObject.put("message", "NOT_MODERATOR");
                                        Main.getWebServer().message(httpServletResponse, jsonObject);
                                        return;
                                    }
                                    if (cloudPlayer.getEditReport() != null) {
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("error", "ALREADY_EDITING_A_REPORT");
                                        Main.getWebServer().message(httpServletResponse, jsonObject);
                                    }

                                    if (cloudPlayer.getLanguage() == Language.LanguageType.GERMAN) {
                                        cloudPlayer.sendRawMessage(cloudPlayer.getLanguageMessage("team-prefix")+" §7Du bearbeitest nun den Report für §c" + report.getSuspect());
                                    } else {
                                        cloudPlayer.sendRawMessage(cloudPlayer.getLanguageMessage("team-prefix")+" §7You are now editing the Report for §c" + report.getSuspect());
                                    }

                                    cloudPlayer.setEditReport(report.getUniqueID());
                                    report.setVisitor(visitor);
                                    report.setState(ReportPlayersPacket.ReportState.IN_PROCESS);

                                    JSONObject reportObject = new JSONObject();
                                    reportObject.put("status", "ERROR");
                                    JSONArray reasonArray = new JSONArray();
                                    reportObject.put("id", report.getUniqueID());
                                    reportObject.put("suspect", report.getSuspect());
                                    reportObject.put("status", report.getState().name());
                                    if (report.getVisitor() != null) {
                                        reportObject.put("visitor", report.getVisitor());
                                    }
                                    for (ReportPlayersPacket.AccusationsReason reason : report.getAccusations()) {
                                        JSONObject reasonObject = new JSONObject();
                                        reasonObject.put("reporter", reason.getName());
                                        reasonObject.put("reason", reason.getReason());
                                        if (reason.getReplayId() != null) {
                                            reasonObject.put("replayId", reason.getReplayId());
                                        } else {
                                            reasonObject.put("replayId", "NO_REPLAYID");
                                        }
                                        if (reason.getChatLogId() != null) {
                                            reasonObject.put("chatLog", reason.getChatLogId());
                                        } else {
                                            reasonObject.put("chatLog", "NO_CHATLOG");
                                        }
                                        reasonObject.put("server", reason.getServer());
                                        reasonArray.put(reasonObject);
                                    }
                                    reportObject.put("reports", reasonArray);
                                    Main.getWebServer().message(httpServletResponse, reportObject);
                                } else {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("status", "ERROR");
                                    jsonObject.put("message", "YOU_ARE_NOT_ONLINE");
                                    Main.getWebServer().message(httpServletResponse, jsonObject);
                                }
                            } else {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "NO_VISITOR_DEFINED");
                                Main.getWebServer().message(httpServletResponse, jsonObject);
                            }
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_REQUEST");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                        }
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);
        }
    }
}
