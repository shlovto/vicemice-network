package net.vicemice.modules.webapi.api.handler.get;

import net.vicemice.hector.CloudService;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import org.eclipse.jetty.server.Request;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

public class LocaleHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (!token.hasPermission("locales")) {
                        Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                        return;
                    }

                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("locales")) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "SUCCESS");
                        JSONArray jsonArray = new JSONArray();

                        for (Locale locale : CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager().getLocales().keySet()) {
                            JSONObject localeObject = new JSONObject();
                            localeObject.put("name", locale.getDisplayName());
                            localeObject.put("tag", locale.toLanguageTag());
                            jsonArray.put(localeObject);
                        }
                        jsonObject.put("locales", jsonArray);

                        Main.getWebServer().message(httpServletResponse, jsonObject);
                    } else if (httpServletRequest.getHeader("request").equalsIgnoreCase("localeMessages")) {
                        if (httpServletRequest.getHeader("locale") != null && Locale.forLanguageTag(httpServletRequest.getHeader("locale")) != null) {
                            Locale locale = Locale.forLanguageTag(httpServletRequest.getHeader("locale"));
                            if (CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager().getLocales().containsKey(locale)) {
                                Main.getWebServer().message(httpServletResponse, CloudService.getCloudService().getManagerService().getConfigManager().getLocaleManager().getLocales().get(locale));
                            } else {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "LOCALE_NOT_TRANSLATED");
                                Main.getWebServer().message(httpServletResponse, jsonObject);;
                            }
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_LOCALE");
                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                        }
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);;
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);;
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);;
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);;
        }
    }
}
