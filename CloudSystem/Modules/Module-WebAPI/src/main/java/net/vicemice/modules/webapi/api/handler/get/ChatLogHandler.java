package net.vicemice.modules.webapi.api.handler.get;

import net.vicemice.hector.CloudService;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import org.bson.Document;
import org.eclipse.jetty.server.Request;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/*
 * Class created at 09:50 - 27.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ChatLogHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("chatLog")) {
                        if (!token.hasPermission("chatLog")) {
                            Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                            return;
                        }

                        if (httpServletRequest.getHeader("chatLogID") == null) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_CHATLOGID");
                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                            return;
                        }

                        String chatLog = httpServletRequest.getHeader("chatLogID");
                        Document chatLogDocument = CloudService.getCloudService().getManagerService().getMongoManager().getDocument("chatLogs", new Document("id", chatLog));

                        if (chatLogDocument == null) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "CHATLOG_NOT_FOUND");
                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                            return;
                        }

                        Long timeStamp = chatLogDocument.getLong("time");

                        Long start = (timeStamp- TimeUnit.MINUTES.toMillis(10));
                        Long end = (timeStamp+TimeUnit.MINUTES.toMillis(10));

                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "SUCCESS");

                        JSONObject chatLogObject = new JSONObject();
                        chatLogObject.put("id", chatLog);
                        chatLogObject.put("creator", chatLogDocument.getString("creator"));
                        chatLogObject.put("tracked", chatLogDocument.getString("tracked"));
                        chatLogObject.put("server", chatLogDocument.getString("server"));
                        chatLogObject.put("report", chatLogDocument.getString("report"));
                        //JSONArray jsonArray = ChatLogAPI.getMessage(start, end, timeStamp, chatLogDocument.getString("server"));
                        //infoObject.put("messages", jsonArray);
                        //infoObject.put("messageSize", jsonArray.length());

                        JSONObject infoObject = new JSONObject();
                        infoObject.put("created", timeStamp);
                        infoObject.put("start", start);
                        infoObject.put("end", end);

                        chatLogObject.put("info", infoObject);

                        jsonObject.put("chatLog", chatLogObject);

                        Main.getWebServer().message(httpServletResponse, jsonObject);;
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);;
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);;
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);;
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);;
        }
    }
}
