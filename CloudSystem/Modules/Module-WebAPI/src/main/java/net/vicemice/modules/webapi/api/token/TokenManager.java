package net.vicemice.modules.webapi.api.token;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class TokenManager {

    @Getter
    public ConcurrentHashMap<String, Token> tokens;

    public TokenManager() {
        tokens = new ConcurrentHashMap<>();

        List<String> permissionsAdmin = new ArrayList<>();
        permissionsAdmin.add("admin");
        permissionsAdmin.add("acp");
        permissionsAdmin.add("player");
        permissionsAdmin.add("punishment");
        permissionsAdmin.add("cloud");
        permissionsAdmin.add("chatLog");
        permissionsAdmin.add("report");
        permissionsAdmin.add("reload");
        permissionsAdmin.add("locales");

        tokens.put("hhnBMvrepNbK2VVsNY3C68jNpkanwzDHZdc2RaZXFyut2yPbwFaPrfnAqXGCr9xM", new Token("hhnBMvrepNbK2VVsNY3C68jNpkanwzDHZdc2RaZXFyut2yPbwFaPrfnAqXGCr9xM", permissionsAdmin));
    }
}