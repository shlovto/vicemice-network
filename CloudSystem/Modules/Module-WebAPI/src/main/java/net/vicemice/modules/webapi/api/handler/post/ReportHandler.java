package net.vicemice.modules.webapi.api.handler.post;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.packets.types.player.report.ReportPlayersPacket;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.utils.PunishReason;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import org.eclipse.jetty.server.Request;
import org.json.JSONArray;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

/*
 * Class created at 21:20 - 27.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ReportHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("report")) {
                        if (!token.hasPermission("report")) {
                            Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                            return;
                        }

                        if (httpServletRequest.getHeader("action").equalsIgnoreCase("editReport")) {
                            if (httpServletRequest.getHeader("state").equalsIgnoreCase("edit")) {
                                String visitor = httpServletRequest.getHeader("visitor");
                                if (visitor != null) {
                                    String suspect = httpServletRequest.getHeader("suspect");
                                    if (suspect != null) {
                                        JSONObject reportsObject = new JSONObject();
                                        JSONArray reportsArray = new JSONArray();
                                        reportsObject.put("status", "SUCCESS");
                                        for (ReportPlayersPacket.ReportEntry report : CloudService.getCloudService().getManagerService().getReportManager().getEntries()) {
                                            if (!report.getSuspect().equalsIgnoreCase("suspect")) {
                                                continue;
                                            }

                                            report.setVisitor(visitor);
                                            report.setState(ReportPlayersPacket.ReportState.IN_PROCESS);

                                            JSONObject reportObject = new JSONObject();
                                            reportObject.put("name", report.getSuspect());
                                            reportObject.put("status", report.getState());
                                            reportObject.put("visitor", report.getVisitor());
                                            reportsArray.put(reportObject);
                                        }
                                        reportsObject.put("report", reportsArray);
                                        httpServletResponse.getWriter().println(reportsObject);
                                    } else {
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("status", "ERROR");
                                        jsonObject.put("message", "INVALID_SUSPECT");
                                        httpServletResponse.getWriter().println(jsonObject);
                                    }
                                } else {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("status", "ERROR");
                                    jsonObject.put("message", "INVALID_VISITOR");
                                    httpServletResponse.getWriter().println(jsonObject);
                                }
                            } else if (httpServletRequest.getHeader("state").equalsIgnoreCase("finish")) {
                                String visitor = httpServletRequest.getHeader("visitor");
                                String suspect = httpServletRequest.getHeader("suspect");
                                String type = httpServletRequest.getHeader("type");
                                if (visitor != null) {
                                    CloudPlayer visitorPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(visitor);
                                    if (visitorPlayer.getEditReport() != null) {
                                        if (suspect != null) {
                                            if (type != null) {
                                                CloudPlayer suspectPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(suspect);

                                                PunishReason.Points reason = PunishReason.Points.getPoint(type);

                                                if (reason != null && reason.isBan()) {
                                                    long newPoints = suspectPlayer.getAbuses().getBanPoints();
                                                    newPoints += reason.getBanPoints();
                                                    suspectPlayer.getAbuses().setBanPoints(newPoints);
                                                    suspectPlayer.updateToDatabase(false);
                                                    long length = getLength(newPoints);
                                                    if (length != 0) {
                                                        length += System.currentTimeMillis();
                                                    }

                                                    String replayId = (visitorPlayer.getServer().getGameID() != null ? "R:"+visitorPlayer.getServer().getGameID():"");
                                                    CloudService.getCloudService().getManagerService().getAbuseManager().punish(suspectPlayer, visitorPlayer, reason.name(), replayId, length, false, false);

                                                    visitorPlayer.setEditReport(null);

                                                    JSONObject jsonObject = new JSONObject();
                                                    jsonObject.put("status", "SUCCESS");
                                                    jsonObject.put("message", "REPORT_CLOSED_AND_FINISHED");
                                                    httpServletResponse.getWriter().println(jsonObject);

                                                    CloudService.getCloudService().getTerminal().write("§aReport Closed for " + suspectPlayer.getName() + "! The Player was banned! (Type: " + type + ")");
                                                } else if (reason != null && reason.isMute()) {
                                                    long newPoints = suspectPlayer.getAbuses().getMutePoints();
                                                    newPoints += reason.getMutePoints();
                                                    suspectPlayer.getAbuses().setMutePoints(newPoints);
                                                    suspectPlayer.updateToDatabase(false);
                                                    long length = getLength(newPoints);
                                                    if (length != 0) {
                                                        length += System.currentTimeMillis();
                                                    }

                                                    //TODO: Proof
                                                    CloudService.getCloudService().getManagerService().getAbuseManager().punish(suspectPlayer, visitorPlayer, reason.name(), "NO-PROOF", length, true, false);

                                                    visitorPlayer.setEditReport(null);

                                                    JSONObject jsonObject = new JSONObject();
                                                    jsonObject.put("status", "SUCCESS");
                                                    jsonObject.put("message", "REPORT_CLOSED_AND_FINISHED");
                                                    httpServletResponse.getWriter().println(jsonObject);

                                                    CloudService.getCloudService().getTerminal().write("§aReport Closed for " + suspectPlayer.getName() + "! The Player was muted! (Type: " + type + ")");
                                                } else {
                                                    try {
                                                        List<ReportPlayersPacket.AccusationsReason> auccusations = CloudService.getCloudService().getManagerService().getReportManager().getAccusationsFrom(suspectPlayer);
                                                        if (!auccusations.isEmpty()) {
                                                            for (ReportPlayersPacket.AccusationsReason auc : auccusations) {
                                                                CloudPlayer target = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(auc.getName());
                                                                if (target == null) continue;
                                                                if (target.isOnline()) target.sendMessage("report-processed-online", suspectPlayer.getRank().getRankColor()+suspectPlayer.getName());
                                                                if (!target.isOnline()) {
                                                                    target.setReportsProcessed((target.getReportsProcessed()+1));
                                                                    if (target.getReportProcessed() == null) target.setReportProcessed(suspectPlayer.getUniqueId());
                                                                }
                                                            }
                                                        }
                                                    }
                                                    catch (Exception exc) {
                                                        exc.printStackTrace();
                                                        CloudService.getCloudService().getTerminal().writeMessage(exc.getLocalizedMessage());
                                                    }

                                                    CloudService.getCloudService().getManagerService().getReportManager().removeReportFrom(suspectPlayer);

                                                    visitorPlayer.setEditReport(null);

                                                    JSONObject jsonObject = new JSONObject();
                                                    jsonObject.put("status", "SUCCESS");
                                                    jsonObject.put("message", "REPORT_CLOSED_AND_FINISHED");
                                                    httpServletResponse.getWriter().println(jsonObject);

                                                    CloudService.getCloudService().getTerminal().write("§aReport Closed for " + suspectPlayer.getName() + "! The Player not punished! (Type: " + type + ")");
                                                }
                                            } else {
                                                JSONObject jsonObject = new JSONObject();
                                                jsonObject.put("status", "ERROR");
                                                jsonObject.put("message", "INVALID_TYPE");
                                                httpServletResponse.getWriter().println(jsonObject);
                                            }
                                        } else {
                                            JSONObject jsonObject = new JSONObject();
                                            jsonObject.put("status", "ERROR");
                                            jsonObject.put("message", "INVALID_SUSPECT");
                                            httpServletResponse.getWriter().println(jsonObject);
                                        }
                                    } else {
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("status", "ERROR");
                                        jsonObject.put("message", "NO_REPORT");
                                        httpServletResponse.getWriter().println(jsonObject);
                                    }
                                } else {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("status", "ERROR");
                                    jsonObject.put("message", "INVALID_VISITOR");
                                    httpServletResponse.getWriter().println(jsonObject);
                                }
                            } else {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "INVALID_STATE");
                                httpServletResponse.getWriter().println(jsonObject);
                            }
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_ACTION");
                            httpServletResponse.getWriter().println(jsonObject);
                        }
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);
        }
    }

    public long getLength(long points) {
        if (points == 3) {
            return TimeUnit.MINUTES.toMillis(10);
        } else if (points <= 6) {
            return TimeUnit.MINUTES.toMillis(15);
        } else if (points <= 9) {
            return TimeUnit.MINUTES.toMillis(30);
        } else if (points <= 12) {
            return TimeUnit.HOURS.toMillis(1);
        } else if (points <= 15) {
            return TimeUnit.HOURS.toMillis(3);
        } else if (points <= 18) {
            return TimeUnit.HOURS.toMillis(6);
        } else if (points <= 21) {
            return TimeUnit.HOURS.toMillis(12);
        } else if (points <= 24) {
            return TimeUnit.DAYS.toMillis(1);
        } else if (points <= 27) {
            return TimeUnit.DAYS.toMillis(3);
        } else if (points <= 30) {
            return TimeUnit.DAYS.toMillis(6);
        } else if (points <= 33) {
            return TimeUnit.DAYS.toMillis(9);
        } else if (points <= 36) {
            return TimeUnit.DAYS.toMillis(12);
        } else if (points <= 39) {
            return TimeUnit.DAYS.toMillis(15);
        } else if (points <= 42) {
            return TimeUnit.DAYS.toMillis(20);
        } else if (points <= 45) {
            return TimeUnit.DAYS.toMillis(30);
        } else if (points <= 48) {
            return TimeUnit.DAYS.toMillis(60);
        } else if (points <= 51) {
            return TimeUnit.DAYS.toMillis(90);
        } else if (points >= 54) {
            return 0;
        } else {
            return -1;
        }
    }
}