package net.vicemice.modules.webapi.api.handler.post;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import org.eclipse.jetty.server.Request;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * Class created at 10:00 - 27.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ReplayHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("watch")) {
                        if (!token.hasPermission("player")) {
                            Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                            return;
                        }

                        if (httpServletRequest.getHeader("object").equalsIgnoreCase("replay")) {
                            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(httpServletRequest.getHeader("target"));
                            if (cloudPlayer != null) {
                                if (cloudPlayer.isOnline()) {
                                    String replay = httpServletRequest.getHeader("replayid");
                                    if (replay != null && !replay.equalsIgnoreCase("NO_REPLAYID")) {
                                        cloudPlayer.watchReplay(replay);

                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("status", "SUCCESS");
                                        jsonObject.put("message", "WATCHING_REPLAY");
                                        httpServletResponse.getWriter().println(jsonObject);
                                    } else if (replay != null && replay.equalsIgnoreCase("NO_REPLAYID")) {
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("status", "ERROR");
                                        jsonObject.put("message", "NO_REPLAY");
                                        httpServletResponse.getWriter().println(jsonObject);
                                    } else {
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("status", "ERROR");
                                        jsonObject.put("message", "REPLAY_NOT_FOUND");
                                        jsonObject.put("code", replay);
                                        httpServletResponse.getWriter().println(jsonObject);
                                    }
                                } else {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("status", "ERROR");
                                    jsonObject.put("message", "YOU_ARE_NOT_ONLINE");
                                    httpServletResponse.getWriter().println(jsonObject);
                                }
                            } else {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "PLAYER_NOT_EXIST");
                                httpServletResponse.getWriter().println(jsonObject);
                            }
                        } else if (httpServletRequest.getHeader("object").equalsIgnoreCase("live")) {
                            CloudPlayer visitorPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(httpServletRequest.getHeader("visitor"));
                            CloudPlayer targetPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(httpServletRequest.getHeader("target"));
                            if (visitorPlayer != null) {
                                if (visitorPlayer.isOnline()) {
                                    if (targetPlayer != null) {
                                        if (targetPlayer.isOnline()) {
                                            visitorPlayer.connectToServer(targetPlayer.getServer().getName());

                                            JSONObject jsonObject = new JSONObject();
                                            jsonObject.put("status", "SUCCESS");
                                            jsonObject.put("message", "WATCHING_LIVE");
                                            httpServletResponse.getWriter().println(jsonObject);
                                        } else {
                                            JSONObject jsonObject = new JSONObject();
                                            jsonObject.put("status", "ERROR");
                                            jsonObject.put("message", "TARGET_NOT_ONLINE");
                                            httpServletResponse.getWriter().println(jsonObject);
                                        }
                                    } else {
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("status", "ERROR");
                                        jsonObject.put("message", "TARGET_NOT_EXIST");
                                        httpServletResponse.getWriter().println(jsonObject);
                                    }
                                } else {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("status", "ERROR");
                                    jsonObject.put("message", "YOU_ARE_NOT_ONLINE");
                                    httpServletResponse.getWriter().println(jsonObject);
                                }
                            } else {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "VISITOR_NOT_EXIST");
                                httpServletResponse.getWriter().println(jsonObject);
                            }
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_OBJECT");
                            httpServletResponse.getWriter().println(jsonObject);
                        }
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);
        }
    }
}