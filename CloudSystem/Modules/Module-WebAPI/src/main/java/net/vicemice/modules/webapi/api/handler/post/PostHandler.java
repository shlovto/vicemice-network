package net.vicemice.modules.webapi.api.handler.post;

import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.handler.post.acp.FilterHandler;
import net.vicemice.modules.webapi.api.token.Token;
import org.eclipse.jetty.server.Request;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * Class created at 21:24 - 27.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PostHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("filter")) {
                        new FilterHandler().handle(s, request, httpServletRequest, httpServletResponse);
                    } else if (httpServletRequest.getHeader("request").equalsIgnoreCase("player")) {
                        new PlayerHandler().handle(s, request, httpServletRequest, httpServletResponse);
                    } else if (httpServletRequest.getHeader("request").equalsIgnoreCase("watch")) {
                        new ReplayHandler().handle(s, request, httpServletRequest, httpServletResponse);
                    } else if (httpServletRequest.getHeader("request").equalsIgnoreCase("report")) {
                        new ReportHandler().handle(s, request, httpServletRequest, httpServletResponse);
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "REQUEST_NOT_DEFINED");
                        Main.getWebServer().message(httpServletResponse, jsonObject);;
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);;
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);;
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);;
        }
    }
}
