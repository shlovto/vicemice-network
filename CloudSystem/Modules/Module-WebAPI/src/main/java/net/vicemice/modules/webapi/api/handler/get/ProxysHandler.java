package net.vicemice.modules.webapi.api.handler.get;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.network.proxy.Proxy;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import org.eclipse.jetty.server.Request;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * Class created at 09:54 - 27.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ProxysHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("cloud")) {
                        if (!token.hasPermission("cloud")) {
                            Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                            return;
                        }

                        if (httpServletRequest.getHeader("service") == null) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_SERVICE");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                            return;
                        }

                        if (httpServletRequest.getHeader("service").equalsIgnoreCase("proxys")) {
                            JSONObject proxyObject = new JSONObject();
                            JSONArray proxyArray = new JSONArray();
                            for (Proxy proxy : CloudService.getCloudService().getManagerService().getProxyManager().getProxies()) {
                                JSONObject object = new JSONObject();
                                object.put("name", proxy.getName());
                                object.put("host", proxy.getIpAddress());
                                object.put("port", proxy.getPort());
                                object.put("onlinePlayers", proxy.getOnlinePlayers());
                                proxyArray.put(object);
                            }

                            proxyObject.put("status", "SUCCESS");
                            proxyObject.put("proxys", proxyArray);
                            Main.getWebServer().message(httpServletResponse, proxyObject);
                        } else if (httpServletRequest.getHeader("service").equalsIgnoreCase("proxy")) {
                            if (httpServletRequest.getHeader("name") == null) {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "INVALID_NAME");
                                Main.getWebServer().message(httpServletResponse, jsonObject);
                                return;
                            }

                            Proxy proxy = CloudService.getCloudService().getManagerService().getProxyManager().getProxyByName(httpServletRequest.getHeader("name"));
                            if (proxy != null) {
                                JSONArray jsonArray = new JSONArray();
                                JSONObject proxyObject = new JSONObject();
                                proxyObject.put("name", proxy.getName());
                                proxyObject.put("host", proxy.getIpAddress());
                                proxyObject.put("port", proxy.getPort());
                                proxyObject.put("onlinePlayers", proxy.getOnlinePlayers());
                                proxyObject.put("maxPlayers", CloudService.getCloudService().getManagerService().getProxyManager().getProxySettingsPacket().getMaxPlayers());
                                jsonArray.put(proxyObject);

                                JSONObject finalObject = new JSONObject();
                                finalObject.put("status", "SUCCESS");
                                finalObject.put("proxy", jsonArray);
                                Main.getWebServer().message(httpServletResponse, finalObject);
                            } else {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "PROXY_NOT_FOUND");
                                Main.getWebServer().message(httpServletResponse, jsonObject);
                            }
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_REQUEST");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                        }
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);
        }
    }
}
