package net.vicemice.modules.webapi.api.handler.get;

import net.vicemice.hector.CloudService;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import org.eclipse.jetty.server.Request;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * Class created at 09:52 - 27.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class ReloadHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("reload")) {
                        if (!token.hasPermission("reload")) {
                            Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                            return;
                        }

                        if (!token.hasPermission("admin")) {
                            Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                            return;
                        }
                        if (request.getHeader("reload") != null && request.getHeader("reload").equalsIgnoreCase("as")) {
                            CloudService.getCloudService().getManagerService().getAutonomousManager().init();

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "SUCCESS");
                            jsonObject.put("message", "AS_NETWORKS_RELOADED");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                        } else if (request.getHeader("reload") != null && request.getHeader("reload").equalsIgnoreCase("templates")) {
                            CloudService.getCloudService().getManagerService().getServerManager().reloadTemplates();

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "SUCCESS");
                            jsonObject.put("message", "TEMPLATES_RELOADED");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                        } else if (request.getHeader("reload") != null && request.getHeader("reload").equalsIgnoreCase("permissions")) {
                            CloudService.getCloudService().getManagerService().getPermissionManager().reloadPermissions();

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "SUCCESS");
                            jsonObject.put("message", "PERMISSIONS_RELOADED");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                        } else if (request.getHeader("reload") != null && request.getHeader("reload").equalsIgnoreCase("signs")) {
                            CloudService.getCloudService().getManagerService().getSignManager().reloadSigns();

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "SUCCESS");
                            jsonObject.put("message", "GAMESIGNS_RELOADED");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                        } else if (request.getHeader("reload") != null && request.getHeader("reload").equalsIgnoreCase("settings")) {
                            CloudService.getCloudService().getManagerService().getProxyManager().loadSettings();

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "SUCCESS");
                            jsonObject.put("message", "SETTINGS_RELOADED");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                        } else if (request.getHeader("reload") != null && request.getHeader("reload").equalsIgnoreCase("games")) {
                            CloudService.getCloudService().getManagerService().getGameManager().loadGames();

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "SUCCESS");
                            jsonObject.put("message", "GAMES_RELOADED");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                        } else if (request.getHeader("reload") != null && request.getHeader("reload").equalsIgnoreCase("filter")) {
                            CloudService.getCloudService().getManagerService().getFilterManager().loadEntries();

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "SUCCESS");
                            jsonObject.put("message", "FILTER_RELOADED");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_REQUEST");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                        }
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);
        }
    }
}
