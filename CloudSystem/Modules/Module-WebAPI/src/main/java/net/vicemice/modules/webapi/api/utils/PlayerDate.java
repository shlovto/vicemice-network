package net.vicemice.modules.webapi.api.utils;

import lombok.Data;
import org.jetbrains.annotations.NotNull;

import java.util.Date;

/*
 * Class created at 14:37 - 27.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
@Data
public class PlayerDate implements Comparable<PlayerDate> {

    private Date dateTime;
    private String date;

    public PlayerDate(Date dateTime, String date) {
        this.dateTime = dateTime;
        this.date = date;
    }

    @Override
    public int compareTo(@NotNull PlayerDate o) {
        return getDateTime().compareTo(o.getDateTime());
    }
}