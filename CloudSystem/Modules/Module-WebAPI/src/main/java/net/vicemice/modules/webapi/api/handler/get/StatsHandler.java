package net.vicemice.modules.webapi.api.handler.get;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import org.eclipse.jetty.server.Request;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Locale;
import java.util.UUID;

/*
 * Class created at 09:42 - 27.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class StatsHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        httpServletRequest.setCharacterEncoding("UTF-8");
        httpServletResponse.setContentType("application/json");

        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("player")) {
                        if (!token.hasPermission("player")) {
                            Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                            return;
                        }

                        if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("stats")) {
                            CloudPlayer player = null;

                            if (httpServletRequest.getHeader("uniqueId") != null) {
                                player = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(UUID.fromString(httpServletRequest.getHeader("uniqueId")));
                            } else if (httpServletRequest.getHeader("name") != null) {
                                player = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(httpServletRequest.getHeader("name"));
                            }

                            if (player == null) {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "INVALID_PLAYER");
                                Main.getWebServer().message(httpServletResponse, jsonObject);
                                return;
                            }

                            if (httpServletRequest.getHeader("type") != null) {
                                if (httpServletRequest.getHeader("type").equalsIgnoreCase("all")) {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("status", "SUCCESS");
                                    JSONObject stats = new JSONObject();
                                    for (String key : player.getStats().getGlobalStats().keySet()) {
                                        stats.put(key, player.getStats().getGlobalStats().get(key));
                                    }
                                    jsonObject.put("stats", stats);
                                    Main.getWebServer().message(httpServletResponse, jsonObject);
                                } else if (httpServletRequest.getHeader("type").equalsIgnoreCase("gamemode")) {
                                    if (httpServletRequest.getHeader("gamemode") != null) {
                                        if (httpServletRequest.getHeader("ranking") != null) {
                                            JSONObject jsonObject = new JSONObject();
                                            jsonObject.put("status", "SUCCESS");

                                            HashMap<String, Object> objects = new HashMap<>();
                                            if (httpServletRequest.getHeader("gamemode").equalsIgnoreCase("bedwars")) {
                                                Long ranking = null;
                                                Long kills = null;
                                                Long deaths = null;
                                                Long destroyed_beds = null;
                                                Long wins = null;
                                                Long played_games = null;

                                                if (httpServletRequest.getHeader("ranking").equalsIgnoreCase("global")) {
                                                    ranking = player.getStats().getGlobalRanking().get("bw");
                                                    kills = player.getStats().getGlobalStats().get("bw_kills");
                                                    deaths = player.getStats().getGlobalStats().get("bw_deaths");
                                                    destroyed_beds = player.getStats().getGlobalStats().get("bw_destroyed_beds");
                                                    wins = player.getStats().getGlobalStats().get("bw_wins");
                                                    played_games = player.getStats().getGlobalStats().get("bw_played_games");
                                                } else if (httpServletRequest.getHeader("ranking").equalsIgnoreCase("monthly")) {
                                                    ranking = player.getStats().getMonthlyRanking().get("bw");
                                                    kills = player.getStats().getMonthlyStats().get("bw_kills");
                                                    deaths = player.getStats().getMonthlyStats().get("bw_deaths");
                                                    destroyed_beds = player.getStats().getMonthlyStats().get("bw_destroyed_beds");
                                                    wins = player.getStats().getMonthlyStats().get("bw_wins");
                                                    played_games = player.getStats().getMonthlyStats().get("bw_played_games");
                                                } else if (httpServletRequest.getHeader("ranking").equalsIgnoreCase("daily")) {
                                                    ranking = player.getStats().getDailyRanking().get("bw");
                                                    kills = player.getStats().getDailyStats().get("bw_kills");
                                                    deaths = player.getStats().getDailyStats().get("bw_deaths");
                                                    destroyed_beds = player.getStats().getDailyStats().get("bw_destroyed_beds");
                                                    wins = player.getStats().getDailyStats().get("bw_wins");
                                                    played_games = player.getStats().getDailyStats().get("bw_played_games");
                                                }

                                                if (kills == null && deaths == null & destroyed_beds == null & wins == null & played_games == null) {
                                                    JSONObject jsonObject1 = new JSONObject();
                                                    jsonObject1.put("status", "ERROR");
                                                    jsonObject1.put("message", "NOT_PLAYED");
                                                    httpServletResponse.getWriter().println(jsonObject1);
                                                    return;
                                                }

                                                if (ranking == null) {
                                                    ranking = 0L;
                                                }
                                                if (kills == null) {
                                                    kills = 0L;
                                                }
                                                if (deaths == null) {
                                                    deaths = 0L;
                                                }
                                                if (destroyed_beds == null) {
                                                    destroyed_beds = 0L;
                                                }
                                                if (wins == null) {
                                                    wins = 0L;
                                                }
                                                if (played_games == null) {
                                                    played_games = 0L;
                                                }

                                                DecimalFormat nf = new DecimalFormat();
                                                nf.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.GERMAN));

                                                objects.put("ranked", ranking);
                                                objects.put("kills", kills);
                                                objects.put("deaths", deaths);
                                                objects.put("destroyed_beds", destroyed_beds);
                                                objects.put("wins", wins);
                                                objects.put("played_games", played_games);
                                                objects.put("nf_kills", nf.format(kills));
                                                objects.put("nf_deaths", nf.format(deaths));
                                                objects.put("nf_destroyed_beds", nf.format(destroyed_beds));
                                                objects.put("nf_wins", nf.format(wins));
                                                objects.put("nf_played_games", nf.format(played_games));
                                                objects.put("available", "yes");
                                            }
                                            if (httpServletRequest.getHeader("gamemode").equalsIgnoreCase("qsg")) {
                                                Long ranking = null;
                                                Long kills = null;
                                                Long deaths = null;
                                                Long open_chests = null;
                                                Long projectile_kills = null;
                                                Long wins = null;
                                                Long played_games = null;

                                                if (httpServletRequest.getHeader("ranking").equalsIgnoreCase("global")) {
                                                    ranking = player.getStats().getGlobalRanking().get("qsg");
                                                    kills = player.getStats().getGlobalStats().get("qsg_kills");
                                                    deaths = player.getStats().getGlobalStats().get("qsg_deaths");
                                                    open_chests = player.getStats().getGlobalStats().get("qsg_open_chests");
                                                    projectile_kills = player.getStats().getGlobalStats().get("qsg_projectile_kills");
                                                    wins = player.getStats().getGlobalStats().get("qsg_wins");
                                                    played_games = player.getStats().getGlobalStats().get("qsg_played_games");
                                                } else if (httpServletRequest.getHeader("ranking").equalsIgnoreCase("monthly")) {
                                                    ranking = player.getStats().getMonthlyRanking().get("qsg");
                                                    kills = player.getStats().getMonthlyStats().get("qsg_kills");
                                                    deaths = player.getStats().getMonthlyStats().get("qsg_deaths");
                                                    open_chests = player.getStats().getMonthlyStats().get("qsg_open_chests");
                                                    projectile_kills = player.getStats().getMonthlyStats().get("qsg_projectile_kills");
                                                    wins = player.getStats().getMonthlyStats().get("qsg_wins");
                                                    played_games = player.getStats().getMonthlyStats().get("qsg_played_games");
                                                } else if (httpServletRequest.getHeader("ranking").equalsIgnoreCase("daily")) {
                                                    ranking = player.getStats().getDailyRanking().get("qsg");
                                                    kills = player.getStats().getDailyStats().get("qsg_kills");
                                                    deaths = player.getStats().getDailyStats().get("qsg_deaths");
                                                    open_chests = player.getStats().getDailyStats().get("qsg_open_chests");
                                                    projectile_kills = player.getStats().getDailyStats().get("qsg_projectile_kills");
                                                    wins = player.getStats().getDailyStats().get("qsg_wins");
                                                    played_games = player.getStats().getDailyStats().get("qsg_played_games");
                                                }

                                                if (kills == null && projectile_kills == null && deaths == null && open_chests == null && wins == null && played_games == null) {
                                                    JSONObject jsonObject1 = new JSONObject();
                                                    jsonObject1.put("status", "ERROR");
                                                    jsonObject1.put("message", "NOT_PLAYED");
                                                    httpServletResponse.getWriter().println(jsonObject1);
                                                    return;
                                                }

                                                if (ranking == null) {
                                                    ranking = 0L;
                                                }
                                                if (kills == null) {
                                                    kills = 0L;
                                                }
                                                if (projectile_kills == null) {
                                                    projectile_kills = 0L;
                                                }
                                                if (deaths == null) {
                                                    deaths = 0L;
                                                }
                                                if (open_chests == null) {
                                                    open_chests = 0L;
                                                }
                                                if (wins == null) {
                                                    wins = 0L;
                                                }
                                                if (played_games == null) {
                                                    played_games = 0L;
                                                }

                                                DecimalFormat nf = new DecimalFormat();
                                                nf.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.GERMAN));

                                                objects.put("ranked", ranking);
                                                objects.put("kills", kills);
                                                objects.put("projectile_kills", projectile_kills);
                                                objects.put("deaths", deaths);
                                                objects.put("open_chests", open_chests);
                                                objects.put("wins", wins);
                                                objects.put("played_games", played_games);
                                                objects.put("nf_kills", nf.format(kills));
                                                objects.put("nf_projectile_kills", nf.format(projectile_kills));
                                                objects.put("nf_deaths", nf.format(deaths));
                                                objects.put("nf_open_chests", nf.format(open_chests));
                                                objects.put("nf_wins", nf.format(wins));
                                                objects.put("nf_played_games", nf.format(played_games));
                                                objects.put("available", "yes");
                                            } else if (httpServletRequest.getHeader("gamemode").equalsIgnoreCase("ranking")) {
                                                for (String a : player.getStats().getGlobalRanking().keySet()) {
                                                    objects.put(a, player.getStats().getGlobalRanking().get(a));
                                                }
                                            } else if (httpServletRequest.getHeader("gamemode").equalsIgnoreCase("tttTop5")) {
                                                long i = 1;
                                                for (CloudPlayer cloudPlayer : CloudService.getCloudService().getManagerService().getPlayerManager().getNameCloudPlayers().values()) {
                                                    if (!cloudPlayer.getStats().getGlobalRanking().containsKey("ttt")) continue;
                                                    if (cloudPlayer.getStats().getGlobalRanking().get("ttt") != i) continue;
                                                    objects.put(cloudPlayer.getName(), i);
                                                    i++;
                                                }
                                                System.out.println(CloudService.getCloudService().getManagerService().getPlayerManager().getNameCloudPlayers().size());
                                            }

                                            JSONObject info = new JSONObject(objects);

                                            jsonObject.put("stats", info);

                                            Main.getWebServer().message(httpServletResponse, jsonObject);
                                        } else {
                                            JSONObject jsonObject = new JSONObject();
                                            jsonObject.put("status", "ERROR");
                                            jsonObject.put("message", "INVALID_RANKING");
                                            Main.getWebServer().message(httpServletResponse, jsonObject);
                                        }
                                    } else {
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("status", "ERROR");
                                        jsonObject.put("message", "INVALID_GAMEMODE");
                                        Main.getWebServer().message(httpServletResponse, jsonObject);
                                    }
                                } else {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("status", "ERROR");
                                    jsonObject.put("message", "INVALID_REQUEST");
                                    Main.getWebServer().message(httpServletResponse, jsonObject);
                                }
                            } else {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "INVALID_REQUEST");
                                Main.getWebServer().message(httpServletResponse, jsonObject);
                            }
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_REQUEST");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                        }
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);
        }
    }
}
