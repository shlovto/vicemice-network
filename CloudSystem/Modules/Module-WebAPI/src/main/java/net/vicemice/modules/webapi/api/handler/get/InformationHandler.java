package net.vicemice.modules.webapi.api.handler.get;

import net.vicemice.hector.CloudService;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import org.eclipse.jetty.server.Request;
import org.json.JSONArray;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * Class created at 09:53 - 27.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class InformationHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("permissions")) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "SUCCESS");
                        JSONArray jsonArray = new JSONArray();
                        for (String permission : token.getPermissions()) {
                            jsonArray.put(permission);
                        }
                        jsonObject.put("permissions", jsonArray);
                        Main.getWebServer().message(httpServletResponse, jsonObject);;
                    } else if (httpServletRequest.getHeader("request").equalsIgnoreCase("information")) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "SUCCESS");
                        jsonObject.put("registered", CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().size());
                        jsonObject.put("online", CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().size());
                        jsonObject.put("banned", CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithBan().size());
                        jsonObject.put("muted", CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithMute().size());
                        jsonObject.put("ranks", CloudService.getCloudService().getManagerService().getPlayerManager().getPlayersWithRank().size());
                        if (CloudService.getCloudService().getManagerService().getProxyManager().isMaintenanceActive()) {
                            jsonObject.put("maintenance", "ACTIVE");
                        } else if (!CloudService.getCloudService().getManagerService().getProxyManager().isMaintenanceActive()) {
                            jsonObject.put("maintenance", "INACTIVE");
                        }

                        Main.getWebServer().message(httpServletResponse, jsonObject);;
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);;
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);;
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);;
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);;
        }
    }
}
