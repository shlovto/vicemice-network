package net.vicemice.modules.webapi.api.handler.post.acp;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.utils.Filter;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import org.eclipse.jetty.server.Request;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

/*
 * Class created at 21:06 - 28.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class FilterHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("filter")) {
                        if (!token.hasPermission("acp")) {
                            Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                            return;
                        }
                        if (httpServletRequest.getHeader("word") == null) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_WORD");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                            return;
                        }

                        if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("add")) {
                            if (httpServletRequest.getHeader("creator") == null) {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "INVALID_CREATOR");
                                Main.getWebServer().message(httpServletResponse, jsonObject);
                                return;
                            }

                            String word = httpServletRequest.getHeader("word");
                            if (CloudService.getCloudService().getManagerService().getFilterManager().getEntriesWord().contains(word)) {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "WORD_ALREADY_CREATED");
                                Main.getWebServer().message(httpServletResponse, jsonObject);
                                return;
                            }

                            CloudPlayer cloudPlayer = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(httpServletRequest.getHeader("creator"));
                            UUID uniqueId = cloudPlayer.getUniqueId();

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "SUCCESS");
                            jsonObject.put("message", "WORD_ADDED");
                            Filter filter = new Filter(word, System.currentTimeMillis(), uniqueId);
                            filter.updateToDatabase(true);
                            CloudService.getCloudService().getManagerService().getFilterManager().getEntries().add(filter);
                            CloudService.getCloudService().getManagerService().getFilterManager().getEntriesWord().add(word);

                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                        } else if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("delete")) {
                            String word = httpServletRequest.getHeader("word");
                            if (!CloudService.getCloudService().getManagerService().getFilterManager().getEntriesWord().contains(word)) {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "WORD_NOT_EXIST");
                                Main.getWebServer().message(httpServletResponse, jsonObject);
                                return;
                            }

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "SUCCESS");
                            jsonObject.put("message", "WORD_REMOVED");
                            CloudService.getCloudService().getManagerService().getFilterManager().removeWord(word);

                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_REQUEST");
                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                        }
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);;
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);;
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);;
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);;
        }
    }
}
