package net.vicemice.modules.webapi.api.handler.get;

import net.vicemice.hector.CloudService;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.handler.get.acp.ASNetworkHandler;
import net.vicemice.modules.webapi.api.handler.get.acp.DashboardHandler;
import net.vicemice.modules.webapi.api.handler.get.acp.FilterHandler;
import net.vicemice.modules.webapi.api.token.Token;
import org.eclipse.jetty.server.Request;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * Class created at 10:01 - 27.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class GetHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("filter")) {
                        new FilterHandler().handle(s, request, httpServletRequest, httpServletResponse);
                    } else if (httpServletRequest.getHeader("request").equalsIgnoreCase("version")) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "SUCCESS");
                        jsonObject.put("author", "elrobtossohn");
                        jsonObject.put("version", CloudService.getVersion());
                        jsonObject.put("update_required", Main.getWebServer().checkVersion());
                        Main.getWebServer().message(httpServletResponse, jsonObject);
                    } else if (httpServletRequest.getHeader("request").equalsIgnoreCase("asnetwork")) {
                        new ASNetworkHandler().handle(s, request, httpServletRequest, httpServletResponse);
                    } else if (httpServletRequest.getHeader("request").equalsIgnoreCase("information") ||
                            httpServletRequest.getHeader("request").equalsIgnoreCase("permissions")) {
                        new InformationHandler().handle(s, request, httpServletRequest, httpServletResponse);
                    } else if (httpServletRequest.getHeader("request").equalsIgnoreCase("chatLog")) {
                        new ChatLogHandler().handle(s, request, httpServletRequest, httpServletResponse);
                    } else if (httpServletRequest.getHeader("request").equalsIgnoreCase("locales") ||
                            httpServletRequest.getHeader("request").equalsIgnoreCase("localeMessages")) {
                        new LocaleHandler().handle(s, request, httpServletRequest, httpServletResponse);
                    } else if (httpServletRequest.getHeader("request").equalsIgnoreCase("cloud")) {
                        if (httpServletRequest.getHeader("service") == null) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_SERVICE");
                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                            return;
                        }

                        if (httpServletRequest.getHeader("service").equalsIgnoreCase("proxys") || httpServletRequest.getHeader("service").equalsIgnoreCase("proxy")) {
                            new ProxysHandler().handle(s, request, httpServletRequest, httpServletResponse);
                        } else if (httpServletRequest.getHeader("service").equalsIgnoreCase("gameServers") || httpServletRequest.getHeader("service").equalsIgnoreCase("gameServer")) {
                            new GameServerHandler().handle(s, request, httpServletRequest, httpServletResponse);
                        } else if (httpServletRequest.getHeader("service").equalsIgnoreCase("slaves") || httpServletRequest.getHeader("service").equalsIgnoreCase("slave")) {
                            new SlavesHandler().handle(s, request, httpServletRequest, httpServletResponse);
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "SERVICE_NOT_FOUND");
                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                        }
                    } else if (httpServletRequest.getHeader("request").equalsIgnoreCase("reload")) {
                        new ReloadHandler().handle(s, request, httpServletRequest, httpServletResponse);
                    } else if (httpServletRequest.getHeader("request").equalsIgnoreCase("players")) {
                        new DashboardHandler().handle(s, request, httpServletRequest, httpServletResponse);
                    } else if (httpServletRequest.getHeader("request").equalsIgnoreCase("player")) {
                        if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("registered") ||
                                httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("punishments") ||
                                httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("online") ||
                                httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("discordVerify") ||
                                httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("playerwebdata") ||
                                httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("player")) {
                            new PlayerHandler().handle(s, request, httpServletRequest, httpServletResponse);
                        } else if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("punishment")) {
                            new PunishmentHandler().handle(s, request, httpServletRequest, httpServletResponse);
                        } else if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("stats")) {
                            new StatsHandler().handle(s, request, httpServletRequest, httpServletResponse);
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "REQUEST_NOT_DEFINED");
                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                        }
                    } else if (httpServletRequest.getHeader("request").equalsIgnoreCase("report")) {
                        new ReportHandler().handle(s, request, httpServletRequest, httpServletResponse);
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "REQUEST_NOT_DEFINED");
                        Main.getWebServer().message(httpServletResponse, jsonObject);;
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);;
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);;
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);;
        }
    }
}