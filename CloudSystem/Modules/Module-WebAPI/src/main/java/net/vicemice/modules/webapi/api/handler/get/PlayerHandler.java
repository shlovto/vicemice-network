package net.vicemice.modules.webapi.api.handler.get;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.hector.player.utils.Abuses;
import net.vicemice.hector.utils.players.FriendData;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import org.bson.Document;
import org.eclipse.jetty.server.Request;
import org.json.JSONArray;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/*
 * Class created at 09:43 - 27.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PlayerHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("player")) {
                        if (!token.hasPermission("player")) {
                            Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                            return;
                        }

                        if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("registered")) {
                            if (!token.hasPermission("player")) {
                                Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                                return;
                            }
                            if (httpServletRequest.getHeader("object") != null) {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "SUCCESS");

                                ArrayList<Object> objects = new ArrayList<>();

                                for (Document document : CloudService.getCloudService().getManagerService().getMongoManager().getDocuments("players")) {
                                    if (document == null) continue;
                                    HashMap<String, Object> o = new HashMap<>();
                                    o.put("id", document.getInteger("id"));
                                    o.put("uniqueId", document.getString("uniqueId"));
                                    o.put("name", document.getString("name"));
                                    o.put("firstlogin", document.getLong("firstlogin"));
                                    o.put("lastlogin", document.getLong("lastlogin"));
                                    o.put("rank", document.getString("rank"));

                                    JSONObject jsonArray = new JSONObject(o);
                                    objects.add(jsonArray);
                                }

                                JSONArray players = new JSONArray(objects);

                                jsonObject.put("totalplayers", CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().size());
                                jsonObject.put("players", players);

                                Main.getWebServer().message(httpServletResponse, jsonObject);
                                ;
                            } else if (httpServletRequest.getHeader("min") != null && httpServletRequest.getHeader("limit") != null) {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "SUCCESS");

                                ArrayList<Object> objects = new ArrayList<>();

                                for (int i = Integer.parseInt(httpServletRequest.getHeader("min")); i < (Integer.parseInt(httpServletRequest.getHeader("min")) + 10); i++) {
                                    Document l = CloudService.getCloudService().getManagerService().getMongoManager().getDocument("players", new Document("id", i));
                                    if (l == null) continue;
                                    HashMap<String, Object> o = new HashMap<>();
                                    o.put("id", l.getInteger("id"));
                                    o.put("uniqueId", l.getString("uniqueId"));
                                    o.put("name", l.getString("name"));
                                    o.put("firstlogin", l.getLong("firstlogin"));
                                    o.put("lastlogin", l.getLong("lastlogin"));

                                    JSONObject jsonArray = new JSONObject(o);
                                    objects.add(jsonArray);
                                }

                                JSONArray players = new JSONArray(objects);

                                jsonObject.put("totalplayers", CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().size());
                                jsonObject.put("players", players);

                                Main.getWebServer().message(httpServletResponse, jsonObject);
                                ;
                            } else if (httpServletRequest.getHeader("sort") != null) {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "SUCCESS");

                                ArrayList<Object> objects = new ArrayList<>();

                                ArrayList<Document> l = CloudService.getCloudService().getManagerService().getMongoManager().getDocumentsWithSort("players", new Document(httpServletRequest.getHeader("sort"), -1));

                                for (Document player : l) {
                                    HashMap<String, Object> o = new HashMap<>();
                                    o.put("uniqueId", player.getString("uniqueId"));
                                    o.put("name", player.getString("name"));
                                    o.put("firstlogin", player.getLong("firstlogin"));
                                    o.put("lastlogin", player.getLong("lastlogin"));

                                    JSONObject jsonArray = new JSONObject(o);
                                    objects.add(jsonArray);
                                }

                                JSONArray players = new JSONArray(objects);

                                jsonObject.put("players", players);

                                Main.getWebServer().message(httpServletResponse, jsonObject);
                                ;
                            } else {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "SUCCESS");

                                ArrayList<Object> objects = new ArrayList<>();

                                for (CloudPlayer player : CloudService.getCloudService().getManagerService().getPlayerManager().getUniqueIdPlayers().values()) {
                                    HashMap<String, Object> o = new HashMap<>();
                                    o.put("uniqueId", player.getUniqueId());
                                    o.put("name", player.getName());
                                    o.put("firstlogin", player.getFirstLogin());
                                    o.put("lastlogin", player.getLastLogin());

                                    JSONObject jsonArray = new JSONObject(o);
                                    objects.add(jsonArray);
                                }

                                JSONArray players = new JSONArray(objects);

                                jsonObject.put("players", players);

                                Main.getWebServer().message(httpServletResponse, jsonObject);
                                ;
                            }
                        } else if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("punishments")) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "SUCCESS");

                            ArrayList<Object> objects = new ArrayList<>();

                            for (Abuses.AbuseData abuseData : CloudService.getCloudService().getManagerService().getAbuseManager().getBanId().values()) {
                                HashMap<String, Object> o = new HashMap<>();
                                o.put("type", abuseData.getPunishType().name().toUpperCase());
                                o.put("name", CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(abuseData.getUser()).getName());
                                o.put("punisher", CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(abuseData.getAuthor()).getName());
                                o.put("reason", abuseData.getReason());
                                o.put("comment", abuseData.getComment());
                                o.put("shorted", (abuseData.isShorted() ? "Yes" : "No"));
                                o.put("removed", (abuseData.isRemoved() ? "Yes" : "No"));

                                JSONObject jsonArray = new JSONObject(o);
                                objects.add(jsonArray);
                            }

                            JSONArray players = new JSONArray(objects);

                            jsonObject.put("punishments", players);

                            Main.getWebServer().message(httpServletResponse, jsonObject);
                        } else if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("online")) {
                            if (!token.hasPermission("admin")) {
                                Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                                return;
                            }

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "SUCCESS");

                            ArrayList<Object> objects = new ArrayList<>();

                            for (UUID uniqueId : CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers()) {
                                CloudPlayer player = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(uniqueId);
                                HashMap<String, Object> o = new HashMap<>();
                                o.put("uniqueId", player.getUniqueId());
                                o.put("name", player.getName());
                                o.put("firstlogin", player.getFirstLogin());
                                o.put("lastlogin", player.getLastLogin());

                                JSONObject jsonArray = new JSONObject(o);
                                objects.add(jsonArray);
                            }

                            JSONArray players = new JSONArray(objects);

                            jsonObject.put("players", players);

                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                        } else if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("discordVerify")) {
                            if (httpServletRequest.getHeader("id") != null) {
                                CloudPlayer player = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByDiscordID(httpServletRequest.getHeader("id"));

                                if (player != null) {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("status", "SUCCESS");

                                    HashMap<String, Object> objects = new HashMap<>();
                                    objects.put("uniqueId", player.getUniqueId());
                                    objects.put("username", player.getName());

                                    JSONObject info = new JSONObject(objects);

                                    jsonObject.put("player", info);

                                    Main.getWebServer().message(httpServletResponse, jsonObject);;
                                } else {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("status", "ERROR");
                                    jsonObject.put("message", "PLAYER_NOT_FOUND");
                                    Main.getWebServer().message(httpServletResponse, jsonObject);;
                                }
                            } else {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "INVALID_REQUEST");
                                Main.getWebServer().message(httpServletResponse, jsonObject);;
                            }
                        } else if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("playerwebdata")) {
                            if (httpServletRequest.getHeader("name") != null) {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "SUCCESS");

                                CloudPlayer player = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(httpServletRequest.getHeader("name"));

                                HashMap<String, Object> objects = new HashMap<>();
                                objects.put("uniqueId", player.getUniqueId());
                                objects.put("username", player.getName());

                                JSONObject info = new JSONObject(objects);

                                jsonObject.put("player", info);

                                Main.getWebServer().message(httpServletResponse, jsonObject);;
                            } else if (httpServletRequest.getHeader("uniqueId") != null) {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "SUCCESS");

                                CloudPlayer player = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(UUID.fromString(httpServletRequest.getHeader("uniqueId")));

                                HashMap<String, Object> objects = new HashMap<>();
                                objects.put("uniqueId", player.getUniqueId());
                                objects.put("username", player.getName());

                                JSONObject info = new JSONObject(objects);

                                jsonObject.put("player", info);

                                Main.getWebServer().message(httpServletResponse, jsonObject);;
                            } else {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "INVALID_REQUEST");
                                Main.getWebServer().message(httpServletResponse, jsonObject);;
                            }
                        } else if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("player")) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "SUCCESS");

                            CloudPlayer player = null;

                            if (httpServletRequest.getHeader("uniqueId") != null) {
                                player = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(UUID.fromString(httpServletRequest.getHeader("uniqueId")));
                            } else if (httpServletRequest.getHeader("name") != null) {
                                player = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(httpServletRequest.getHeader("name"));
                            }

                            if (player == null) {
                                JSONObject jsonObject1 = new JSONObject();
                                jsonObject1.put("status", "ERROR");
                                jsonObject1.put("message", "INVALID_PLAYER");
                                Main.getWebServer().message(httpServletResponse, jsonObject1);
                                return;
                            }

                            HashMap<String, Object> objects = new HashMap<>();
                            objects.put("uniqueId", player.getUniqueId());
                            objects.put("username", player.getName());
                            if (token.hasPermission("admin")) {
                                if (player.getSession() != null) {
                                    objects.put("ip", player.getSession().getIp());
                                } else {
                                    objects.put("ip", player.getIp());
                                }
                            }
                            if (CloudService.getCloudService().getManagerService().getPlayerManager().getOnlineCloudPlayers().contains(player.getUniqueId()) && player.isOnline()) {
                                objects.put("status", "online");
                                objects.put("proxy", player.getProxy().getName());
                                objects.put("server", player.getServer().getName());
                                if (player.getLastServer() != null) {
                                    objects.put("lastServer", player.getLastServer().getName());
                                }
                            } else if (!player.isOnline()) {
                                objects.put("status", "offline");
                                if (player.getLastServer() != null) {
                                    objects.put("lastServer", player.getLastServer().getName());
                                }
                            }
                            objects.put("coins", String.valueOf(player.getCoins()));
                            objects.put("rank", player.getRank().name().toUpperCase());
                            objects.put("rankEnd", player.getRankEnd());
                            objects.put("firstLogin", player.getFirstLogin());
                            objects.put("lastLogin", player.getLastLogin());
                            objects.put("lastLogout", player.getLastLogout());
                            objects.put("playedTime", player.getPlayTime());
                            objects.put("banPoints", player.getAbuses().getBanPoints());
                            objects.put("mutePoints", player.getAbuses().getMutePoints());
                            if (player.getStats().getGlobalRanking().containsKey("points")) {
                                objects.put("globalPointsRanking", player.getStats().getGlobalRanking().get("points"));
                            } else {
                                objects.put("globalPointsRanking", 0);
                            }
                            if (player.getStats().getGlobalStats().containsKey("points")) {
                                objects.put("globalPoints", player.getStats().getGlobalStatsValue("points"));
                            } else {
                                objects.put("globalPoints", 0);
                            }
                            if (player.getStats().getGlobalStats().containsKey("played_games")) {
                                objects.put("globalPlayedGames", player.getStats().getGlobalStatsValue("played_games"));
                            } else {
                                objects.put("globalPlayedGames", 0);
                            }

                            JSONObject jsonObject1 = new JSONObject();
                            for (String s1 : player.getStats().getGlobalRanking().keySet()) {
                                jsonObject1.put(s1, player.getStats().getGlobalRanking().get(s1));
                            }
                            objects.put("globalRanking", jsonObject1);

                            if (player.getAbuses().getBan() != null) {
                                HashMap<String, Object> banData = new HashMap<>();
                                banData.put("reason", player.getAbuses().getBan().getReason());
                                banData.put("end", player.getAbuses().getBan().getLength());
                                banData.put("punisher", player.getAbuses().getBan().getAuthor());
                                banData.put("comment", player.getAbuses().getBan().getComment());

                                objects.put("banData", banData);
                            }

                            if (player.getAbuses().getMute() != null) {
                                HashMap<String, Object> muteData = new HashMap<>();
                                muteData.put("reason", player.getAbuses().getMute().getReason());
                                muteData.put("end", player.getAbuses().getMute().getLength());
                                muteData.put("punisher", player.getAbuses().getMute().getAuthor());
                                muteData.put("comment", player.getAbuses().getMute().getComment());

                                objects.put("muteData", muteData);
                            }

                            JSONObject connectedAccounts = new JSONObject();
                            if (player.getForumId() != null) {
                                connectedAccounts.put("forumId", player.getForumId());
                            }
                            if (player.getTeamSpeakId() != null) {
                                connectedAccounts.put("teamSpeakId", player.getTeamSpeakId());
                            }
                            if (player.getDiscordId() != null) {
                                connectedAccounts.put("discordId", player.getDiscordId());
                            }
                            objects.put("connectedAccounts", connectedAccounts);

                            JSONArray friends = new JSONArray();
                            for (FriendData friendData : player.getFriends().values()) {
                                friends.put(CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(friendData.getUniqueId()).getName());
                            }
                            objects.put("friends", friends);

                            ArrayList<Object> daObjects = new ArrayList<>();
                            for (CloudPlayer cloudPlayer : CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayersByIP(player.getIp())) {
                                if (cloudPlayer.getUniqueId() == player.getUniqueId()) continue;
                                HashMap<String, Object> daObject = new HashMap<>();
                                daObject.put("name", cloudPlayer.getName());
                                daObject.put("uniqueId", cloudPlayer.getUniqueId());
                                daObjects.add(new JSONObject(daObject));
                            }
                            objects.put("doubleAccounts", daObjects);

                            JSONObject info = new JSONObject(objects);

                            jsonObject.put("player", info);

                            Main.getWebServer().message(httpServletResponse, jsonObject);
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_REQUEST");
                            Main.getWebServer().message(httpServletResponse, jsonObject);;
                        }
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);;
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);;
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);;
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);;
        }
    }
}
