package net.vicemice.modules.webapi.api;

import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.get.GetHandler;
import net.vicemice.modules.webapi.api.handler.post.PostHandler;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class WebHandler extends AbstractHandler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        try {
            if (httpServletRequest.getHeader("token") != null) {
                if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                    if (httpServletRequest.getMethod().equals("GET")) {
                        new GetHandler().handle(s, request, httpServletRequest, httpServletResponse);
                    } else if (httpServletRequest.getMethod().equals("POST")) {
                        new PostHandler().handle(s, request, httpServletRequest, httpServletResponse);
                    } else if (httpServletRequest.getMethod().equals("PUT")) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "METHOD_NOT_AVAILABLE");
                        Main.getWebServer().message(httpServletResponse, jsonObject);
                    } else if (httpServletRequest.getMethod().equals("PATCH")) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "METHOD_NOT_AVAILABLE");
                        Main.getWebServer().message(httpServletResponse, jsonObject);
                    } else if (httpServletRequest.getMethod().equals("DELETE")) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "METHOD_NOT_AVAILABLE");
                        Main.getWebServer().message(httpServletResponse, jsonObject);
                    } else if (httpServletRequest.getMethod().equals("OPTIONS")) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "METHOD_NOT_AVAILABLE");
                        Main.getWebServer().message(httpServletResponse, jsonObject);
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "METHOD_UNKNOWN");
                        Main.getWebServer().message(httpServletResponse, jsonObject);
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "TOKEN_INVALID");
                    Main.getWebServer().message(httpServletResponse, jsonObject);
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_DEFINED");
                Main.getWebServer().message(httpServletResponse, jsonObject);
            }
            httpServletResponse.setContentType("text/html;charset=utf-8");
            httpServletResponse.setStatus(HttpServletResponse.SC_OK);
            request.setHandled(true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
