package net.vicemice.modules.webapi.api.handler.get;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.player.CloudPlayer;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import org.eclipse.jetty.server.Request;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

/*
 * Class created at 09:47 - 27.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class PunishmentHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("player")) {
                        if (!token.hasPermission("player")) {
                            Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                            return;
                        }

                        if (httpServletRequest.getHeader("info") != null && httpServletRequest.getHeader("info").equalsIgnoreCase("punishment")) {
                            if (!token.hasPermission("punishment")) {
                                Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                                return;
                            }

                            if (httpServletRequest.getHeader("name") != null) {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "SUCCESS");

                                CloudPlayer player = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByName(httpServletRequest.getHeader("name"));

                                HashMap<String, Object> objects = new HashMap<>();
                                objects.put("uniqueId", player.getUniqueId());
                                objects.put("username", player.getName());

                                if (player.getAbuses().getBan() != null || player.getAbuses().getMute() != null) {
                                    objects.put("has", true);

                                    if (player.getAbuses().getBan() != null) {
                                        objects.put("type", "BAN");
                                        objects.put("reason", player.getAbuses().getBan().getReason());
                                        objects.put("length", player.getAbuses().getBan().getLength());
                                    } else if (player.getAbuses().getMute() != null) {
                                        objects.put("type", "MUTE");
                                        objects.put("reason", player.getAbuses().getMute().getReason());
                                        objects.put("length", player.getAbuses().getMute().getLength());
                                    }
                                } else {
                                    objects.put("has", false);
                                }

                                JSONObject info = new JSONObject(objects);

                                jsonObject.put("player", info);

                                Main.getWebServer().message(httpServletResponse, jsonObject);
                            } else if (httpServletRequest.getHeader("uniqueId") != null) {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "SUCCESS");

                                CloudPlayer player = CloudService.getCloudService().getManagerService().getPlayerManager().getPlayerAPI().getCloudPlayerByUniqueId(UUID.fromString(httpServletRequest.getHeader("uniqueId")));

                                HashMap<String, Object> objects = new HashMap<>();
                                objects.put("uniqueId", player.getUniqueId());
                                objects.put("username", player.getName());

                                if (player.getAbuses().getBan() != null || player.getAbuses().getMute() != null) {
                                    objects.put("has", true);

                                    if (player.getAbuses().getBan() != null) {
                                        objects.put("type", "BAN");
                                        objects.put("reason", player.getAbuses().getBan().getReason());
                                        objects.put("length", player.getAbuses().getBan().getLength());
                                    } else if (player.getAbuses().getMute() != null) {
                                        objects.put("type", "MUTE");
                                        objects.put("reason", player.getAbuses().getMute().getReason());
                                        objects.put("length", player.getAbuses().getMute().getLength());
                                    }
                                } else {
                                    objects.put("has", false);
                                }

                                JSONObject info = new JSONObject(objects);

                                jsonObject.put("player", info);

                                Main.getWebServer().message(httpServletResponse, jsonObject);
                            } else {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "INVALID_REQUEST");
                                Main.getWebServer().message(httpServletResponse, jsonObject);
                            }
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_REQUEST");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                        }
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);
        }
    }
}