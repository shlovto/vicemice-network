package net.vicemice.modules.webapi.api.handler.get;

import net.vicemice.hector.CloudService;
import net.vicemice.hector.network.slave.Slave;
import net.vicemice.modules.webapi.Main;
import net.vicemice.modules.webapi.api.handler.Handler;
import net.vicemice.modules.webapi.api.token.Token;
import org.eclipse.jetty.server.Request;
import org.json.JSONArray;
import org.json.JSONObject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * Class created at 09:55 - 27.01.2020
 * Copyright (C) 2016-2020 Minetasia Solutions
 */
public class SlavesHandler implements Handler {

    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {
        if (httpServletRequest.getHeader("token") != null) {
            if (Main.getWebServer().getTokenManager().getTokens().containsKey(httpServletRequest.getHeader("token"))) {
                Token token = Main.getWebServer().getTokenManager().getTokens().get(httpServletRequest.getHeader("token"));
                if (httpServletRequest.getHeader("request") != null) {
                    if (httpServletRequest.getHeader("request").equalsIgnoreCase("cloud")) {
                        if (!token.hasPermission("cloud")) {
                            Main.getWebServer().message(httpServletResponse, token.noPermissionText());
                            return;
                        }

                        if (httpServletRequest.getHeader("service") == null) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_SERVICE");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                            return;
                        }

                        if (httpServletRequest.getHeader("service").equalsIgnoreCase("slaves")) {
                            JSONObject slavesObject = new JSONObject();
                            JSONArray slavesArray = new JSONArray();
                            for (Slave slave : CloudService.getCloudService().getManagerService().getSlaveManager().getSlaveList()) {
                                JSONObject object = new JSONObject();
                                object.put("name", slave.getName());
                                object.put("host", slave.getHost());
                                double cpu = Main.getWebServer().myRound(slave.getCpuUsage(), 2);
                                object.put("cpuUsage", cpu);
                                object.put("averageCPU", Main.getWebServer().myRound(slave.getAverageCPU(), 2));
                                object.put("forcedTemplate", slave.getForceTemplate());
                                object.put("usedMemory", slave.getUsedMemory());
                                object.put("freeMemory", slave.getFreeMemory());
                                object.put("totalMemory", slave.getTotalMemory());
                                object.put("allocatedMemory", slave.getMaxRam());
                                String startingServer = "none";
                                if (slave.getCurrentStartingServer() != null) {
                                    startingServer = slave.getCurrentStartingServer().getName();
                                }
                                object.put("startingServer", startingServer);
                                object.put("servers", CloudService.getCloudService().getManagerService().getServerManager().getServerCountOnSlave(slave));
                                slavesArray.put(object);
                            }

                            slavesObject.put("status", "SUCCESS");
                            slavesObject.put("slaves", slavesArray);
                            Main.getWebServer().message(httpServletResponse, slavesObject);
                        } else if (httpServletRequest.getHeader("service").equalsIgnoreCase("slave")) {
                            if (httpServletRequest.getHeader("name") == null) {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "INVALID_NAME");
                                Main.getWebServer().message(httpServletResponse, jsonObject);
                                return;
                            }

                            Slave slave = CloudService.getCloudService().getManagerService().getSlaveManager().getSlaveByName(httpServletRequest.getHeader("name"));
                            if (slave != null) {
                                JSONObject object = new JSONObject();
                                object.put("name", slave.getName());
                                object.put("host", slave.getHost());
                                double cpu = Main.getWebServer().myRound(slave.getCpuUsage(), 2);
                                object.put("cpuUsage", cpu);
                                object.put("averageCPU", Main.getWebServer().myRound(slave.getAverageCPU(), 2));
                                object.put("forcedTemplate", slave.getForceTemplate());
                                object.put("usedMemory", slave.getUsedMemory());
                                object.put("freeMemory", slave.getFreeMemory());
                                object.put("totalMemory", slave.getTotalMemory());
                                object.put("allocatedMemory", slave.getMaxRam());
                                String startingServer = "none";
                                if (slave.getCurrentStartingServer() != null) {
                                    startingServer = slave.getCurrentStartingServer().getName();
                                }
                                object.put("startingServer", startingServer);
                                object.put("servers", CloudService.getCloudService().getManagerService().getServerManager().getServerCountOnSlave(slave));


                                JSONObject slaveObject = new JSONObject();
                                slaveObject.put("status", "SUCCESS");
                                slaveObject.put("slave", object);
                                Main.getWebServer().message(httpServletResponse, slaveObject);
                            } else {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("status", "ERROR");
                                jsonObject.put("message", "SLAVE_NOT_FOUND");
                                Main.getWebServer().message(httpServletResponse, jsonObject);
                            }
                        } else {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("status", "ERROR");
                            jsonObject.put("message", "INVALID_REQUEST");
                            Main.getWebServer().message(httpServletResponse, jsonObject);
                        }
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("status", "ERROR");
                        jsonObject.put("message", "INVALID_REQUEST");
                        Main.getWebServer().message(httpServletResponse, jsonObject);
                    }
                } else {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("status", "ERROR");
                    jsonObject.put("message", "REQUEST_NOT_DEFINED");
                    Main.getWebServer().message(httpServletResponse, jsonObject);
                }
            } else {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("status", "ERROR");
                jsonObject.put("message", "TOKEN_NOT_FOUND");
                Main.getWebServer().message(httpServletResponse, jsonObject);
            }
        } else {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status", "ERROR");
            jsonObject.put("message", "TOKEN_NOT_DEFINED");
            Main.getWebServer().message(httpServletResponse, jsonObject);
        }
    }
}
